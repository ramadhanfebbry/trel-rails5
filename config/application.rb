require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Relevant
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(#{config.root}/lib #{config.root}/app/models/ckeditor)

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    config.active_record.observers = :chain_observer, :user_observer, :receipt_transaction_observer,:receipt_observer, :push_point_observer, :push_reward_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Hawaii'
    #config.time_zone = "Eastern Time (US & Canada)"

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Enable the asset pipeline
    # config.assets.enabled = true

    config.assets.compile = true
    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'

    # Devise Configuration Item:
    #
    config.action_mailer.default_url_options = {:host => "trelevant.herokuapp.com"}
    #
    # If you are deploying Rails 3.1 on Heroku, you may want to set:
    #
    config.assets.initialize_on_precompile = false

    #
    # On config/application.rb forcing your application to not access the DB
    # or load models when precompiling your assets.

    # config.logger = Logger.new(STDOUT)
    # config.logger.level = Logger::INFO
    
    config.i18n.load_path += Dir[Rails.root.join("config", "locales", "*.yml").to_s]
    config.i18n.default_locale = "en"
    config.assets.paths << "#{Rails.root}/tmp"
    config.assets.paths << "#{Rails.root}/app/assets/fonts"

    config.generators do |g|
     g.orm :active_record
    end

  end
end
