class TreatwareCheckParserV2
  attr_accessor :pos_check_upload_id, :user_id, :restaurant_id, :offer_id, :receipt_id, :parsed_receipt, :json_check

  def initialize(pos_check_upload_id, user_id, restaurant_id, offer_id, receipt_id)
    @pos_check_upload_id = pos_check_upload_id
    @user_id = user_id
    @restaurant_id = restaurant_id
    @offer_id = offer_id
    @receipt_id = receipt_id
    @parsed_receipt = nil
    @json_check = nil
  end

  def do_parse
    puts "start parse json checkk treatware data from pos check upload"
    puts "create receipt received record"

    p @receipt_id
    receipt = Receipt.find(@receipt_id)

    @pos_check_upload = PosCheckUpload.find(@pos_check_upload_id)
    @pos_check_upload = @pos_check_upload.reload
    json_check =  YAML.load(@pos_check_upload.xml_data)
    treatware_check = TreatwareCheck.new(json_check)

    @user = User.find(@user_id)
    chain = @user.chain
    location = Restaurant.find(@restaurant_id)
    restaurant_offer = RestaurantOffer.where(:restaurant_id => @restaurant_id, :offer_id => @offer_id).first rescue nil
    offer = Offer.find(@offer_id)

    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.restaurant_offer_id = (restaurant_offer.id rescue nil)
    receipt_transaction.subtotal = treatware_check.sub_total
    receipt_transaction.tax = treatware_check.tax
    receipt_transaction.restaurant_id = @restaurant_id

    valid_total = treatware_check.valid_total?(@offer_id,@pos_check_upload)
    valid_date = treatware_check.valid_date?

    p "validation total and date"
    p "valid_total #{valid_total}"
    p "valid_date #{valid_date}"
    p "--------------"

    receipt_transaction.issue_date = treatware_check.converted_date
    receipt_transaction.time_stamp = (treatware_check.converted_time.strip rescue nil)
    receipt_transaction.receipt_date = treatware_check.converted_date
    receipt_transaction.receipt_time = (treatware_check.converted_time.strip rescue nil)

    if !valid_total && treatware_check.total > 0 && treatware_check.sub_total > 0 && location && location.rest_tax > 0.0
      tax_sub_total = treatware_check.sub_total.to_f.eql?(0.0) ? 0.0 : (treatware_check.tax.to_f/treatware_check.sub_total.to_f).round(3)

      if (location.rest_tax * 0.98) <= tax_sub_total  && tax_sub_total <= (location.rest_tax * 1.02)
        valid_total = true
        treatware_check.error_messages.delete(:total)
      end
    elsif valid_total && location
      tax_sub_total = treatware_check.sub_total.to_f.eql?(0.0) ? 0.0 : (treatware_check.tax.to_f/treatware_check.sub_total.to_f).round(3)
      location.update_attribute(:rest_tax, tax_sub_total)
    end

    if valid_total and valid_date
      if unique_receipt?(chain, treatware_check)
        if !chain.blank? and chain.today_max_user_limit?(receipt, treatware_check.converted_date)
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          treatware_check.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
        elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, treatware_check.converted_date, offer)
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          treatware_check.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
        else
          receipt_transaction.status = Receipt::STATUS[:APPROVED]
          receipt.status = Receipt::STATUS[:APPROVED]
        end
      else
        if location
          if unique_receipt?(chain, treatware_check, location)
            if !chain.blank? and chain.today_max_user_limit?(receipt, treatware_check.converted_date)
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              treatware_check.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
            elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, treatware_check.converted_date, offer)
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              treatware_check.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
            else
              receipt_transaction.status = Receipt::STATUS[:APPROVED]
              receipt.status = Receipt::STATUS[:APPROVED]
            end
          elsif unique_receipt?(chain, treatware_check, location, treatware_check.receipt_number)
            if !chain.blank? and chain.today_max_user_limit?(receipt, treatware_check.converted_date)
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              treatware_check.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
            elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, treatware_check.converted_date, offer)
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              treatware_check.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
            else
              receipt_transaction.status = Receipt::STATUS[:APPROVED]
              receipt.status = Receipt::STATUS[:APPROVED]
            end
          else
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            treatware_check.error_messages[:receipt] = "Receipt should be uniq, this receipt have submitted before"
            ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, treatware_check.error_messages, receipt).deliver
          end
        else
          treatware_check.error_messages[:restaurant] = "Restaurant/Location is invalid."
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, treatware_check.error_messages, receipt).deliver
        end
      end
    else
      receipt_transaction.status = Receipt::STATUS[:REJECTED]
      receipt.status = Receipt::STATUS[:REJECTED]
      ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, treatware_check.error_messages, receipt).deliver
    end
    receipt_transaction.instructions = treatware_check.error_messages
    receipt_transaction.receipt_number = treatware_check.receipt_number
    receipt_transaction.restaurant_id = location ? location.id : nil
    p "this is receipt transaction object"
    p receipt_transaction

    ## end offer rule

    receipt.receipt_transactions << receipt_transaction
    ## offer rules validation not met criteria
    ## if the rules not met

    begin
      if receipt_transaction.offer and receipt_transaction.offer.offer_rules.size > 0 # doe not met the rules
        puts "OFFER RULE:: NCR ONLINE ORDER PROCESS JOB start ---" * 19

        off_rule = OfferRuleCalculation::CalculateOfferRule.new(receipt_transaction.offer, receipt_transaction)
        off_rule.result
        tmp_total_points_earned = off_rule.total_points_get
        if tmp_total_points_earned.to_f == 0.0 || tmp_total_points_earned == -1 || tmp_total_points_earned == -2
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt_transaction.instructions = {:offer_rules_not_met => "This receipt does not met any rules"}
          receipt_transaction.instructions = {:ignore_item_list => "This receipt get all the ignore items listed"} if tmp_total_points_earned == -1
          receipt_transaction.instructions = {:ignore_item_reject => "Rejected due the item"} if tmp_total_points_earned == -2
          receipt.status = Receipt::STATUS[:REJECTED]
        end
        puts "OFFER RULE:: Treatware check PROCESS JOB end ---" * 19
      end
    rescue => e
      puts "CALCULATE::OFFERRULES::treatware check ----> #{e.inspect}"
    end

    receipt.save
    ReceiptProcess::ReceiptSetIssuedate.setdatetime(receipt.id, receipt.created_at)
    p "this is receipt final status #{receipt.status}"
    if receipt.status == Receipt::STATUS[:APPROVED]
      "because receipt final status is approved, update pos check upload to approved"
      @pos_check_upload.update_attributes(
          :user_id => @user.id,
          :status => PosCheckUpload::STATUS[:PROCESSED]
      )
    end
    p "this is final pos check upload object"
    p @pos_check_upload
    puts "------ end ------"
  end

  def unique_receipt?(chain, ocr_xml, location = nil, receipt_number = nil)
    if location.blank? and receipt_number.blank?
      receipt_transaction_count = ReceiptApprovedDetail.select("id").where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ?", chain.id, ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip).count
      return true if receipt_transaction_count == 0
      return false
    elsif location and receipt_number.blank?
      receipt_transaction_count = ReceiptApprovedDetail.select("id").includes(:receipt).where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ?", chain.id, ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id).count
      return true if receipt_transaction_count == 0
      return false
    elsif receipt_number
      receipt_transaction_count = ReceiptApprovedDetail.select("id").includes(:receipt).where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ? AND receipt_number = ?", chain.id, ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id, receipt_number).count
      return true if receipt_transaction_count == 0
      return false
    else
      return false
    end
  end

end

