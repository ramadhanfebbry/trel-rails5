Paperclip.interpolates :user do |attachment, style|
  attachment.instance.user_id
end

Paperclip.interpolates :chain do |attachment, style|
  attachment.instance.chain_id
end

Paperclip.interpolates(:timestamp) do |attachment, style|
  attachment.instance_read(:updated_at).to_i
end

Paperclip.interpolates(:receipt) do |attachment, style|
  attachment.instance.id
end

Paperclip.interpolates(:year) do |attachment, style|
  attachment.instance.created_at.year rescue Time.current.year
end

Paperclip.interpolates(:month) do |attachment, style|
  attachment.instance.created_at.month rescue Time.current.month
end