class String

  def to_decimal
    self.delete(" ").gsub(',','.').to_f
  end

  def is_numeric?
    Float(self) != nil rescue false
  end

  def to_ean_13_format
    a = self
    b = a.to_i.to_s
    return a if a == b
    return b.chop
  end

  def remove_zero_padding(length)
    1.upto(length) do |i|
      self.sub!(/^0/, "")
    end
    self
  end

  def add_zero_padding(length)
    new_char = self
    1.upto(length) do |i|
      new_char = new_char.insert(0, '0')
    end
    new_char
  end

  def remove_prepostfix(prepostfix)
    self.upcase.sub(prepostfix.to_s.upcase, "")
  end

  def sanitize_restaurant_identifier_for_fishbowl
    self.gsub(/^0+(?!$)/,'').gsub(/\.[^.]*\d/, '')
  end

end