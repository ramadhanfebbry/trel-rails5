# encoding: utf-8
require 'nokogiri'
require 'date'
require 'time'

class PosReceiptXml
  attr_accessor :xml, :xml_in_nokogiri_format, :error_messages, :converted_date, :converted_time

  def initialize(xml)
    @xml = EscapeUtils.unescape_url(xml)
    @xml_in_nokogiri_format = Nokogiri::XML(@xml)
    @error_messages = {}
    @converted_date = nil
    @converted_time = nil
  end

  def check_id
    #@xml_in_nokogiri_format.xpath('//check').first.attributes["id"].value rescue nil
    @xml_in_nokogiri_format.root["id"] rescue nil
  end

  def seq_num
    #@xml_in_nokogiri_format.xpath('//check').first.attributes["seq_num"].value rescue nil
    @xml_in_nokogiri_format.root["seq_num"] rescue nil
  end

  def sub_total
    #self.total - self.tax
    total_amount = self.sales_total - self.discount_total
    #if total_amount == 0
    #  @xml_in_nokogiri_format.search("menu-item").each do |menu_item|
    #    amount = menu_item.search("total-amount").text.to_f
    #    qty = menu_item.search("quantity").text.to_decimal
    #    total_amount += amount*qty
    #  end
    #end
    total_amount
  end

  def sales_total
    sales_total_text = @xml_in_nokogiri_format.search("sales-total").text.to_s
    BigDecimal.new(sales_total_text)
  end

  def total
    #@xml_in_nokogiri_format.xpath("//total").text.to_decimal
    total_amount_text = @xml_in_nokogiri_format.search("payment-total").text.to_s
    BigDecimal.new(total_amount_text)
    #if total_amount == 0
    #  total_amount = self.sub_total + self.tax
    #end
  end

  def tax
    #@xml_in_nokogiri_format.xpath("//tax").text.to_decimal
    tax_text = @xml_in_nokogiri_format.search("tax-total").text.to_s
    BigDecimal.new(tax_text)
  end

  def discount_total
    discount_total_text = @xml_in_nokogiri_format.search("discount-total").text.to_s
    BigDecimal.new(discount_total_text)
  end

  def direct_tips
    direct_tips_text = @xml_in_nokogiri_format.search("direct-tips").text.to_s
    BigDecimal.new(direct_tips_text)
  end

  def receipt_date
    #@xml_in_nokogiri_format.xpath("//date").text
    selected_date = @xml_in_nokogiri_format.search("date").text
    if selected_date.blank?
      selected_date = Time.zone.now.strftime("%m/%d/%y %H:%M:%S")
    end
    selected_date
  end

  def receipt_number
    [self.check_id, self.seq_num].join("-")
  end

  def employee_num
    @xml_in_nokogiri_format.search("emp-num").text.to_s
  end

  def revenue_center
    @xml_in_nokogiri_format.search("rvc-num").text.to_s
  end

  def table_num
    @xml_in_nokogiri_format.search("tbl-num").text.to_s
  end

  def check_open_time
    @xml_in_nokogiri_format.search("open-time").text.to_s
  end

  def tip_total
    tip_total_text = @xml_in_nokogiri_format.search("tip-total").text.to_s
    BigDecimal.new(tip_total_text)
  end


  def  valid_total?(offer_id=nil,check_upload = nil)
    valid = true
    #unless self.total > 0
    #  valid = false
    #  @error_messages[:total] = "Total should greater than 0"
    #  return valid
    #end

    if self.sub_total.to_f == 0.0 # offer rule purpose forgot loyalty items
      offer = Offer.find(offer_id) rescue nil
      # check the rule first
      if !offer.blank? and offer.is_zero_subtotal?
        g_items = []
        unless offer.offer_rules_subtotal_zeros.blank?
          offer.offer_rules_subtotal_zeros.each do |zero_rules|
            g_items << zero_rules.offer_rule_menu_items
          end
        end
        g_items = g_items.flatten
        #item_numbers = g_items.pos_menu_items.map(&:item_number)
        item_numbers = []
        g_items.each do |g_item|
          item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number)
        end
        offer_item_parser = Parser::OfferItemsParser.new(check_upload)
        offer_item_parser.xml_data = @xml
        result_with_qty = offer_item_parser.find_item_number_and_quantity
        #purchased_item_match = []

        unless result_with_qty.blank?
        result_with_qty.each do |res|
          # if the purchase are correct
          if item_numbers.include?(res[:id].to_i)
            return valid # there are item match with the zero subtotal ( forgot loyalty item)
          end
        end
        end

      end
    end

    unless self.sub_total > 0
      valid = false
      @error_messages[:total] = "Sub Total less than 0"
      return valid
    end

    #unless (self.total.round(3)).eql?((self.tax + self.sub_total).round(3))
    #  valid = false
    #  @error_messages[:total] = "SubTotal + Tax not equal Total"
    #  return valid
    #end
    #unless self.total >= self.sub_total
    #  valid = false
    #  @error_messages[:total] = "Total less than subtotal"
    #  return valid
    #end
    return valid
  end

  def due_total
    due_total_text = @xml_in_nokogiri_format.search('due-total').text.to_s
    BigDecimal.new(due_total_text)
  end

  def valid_date?
    begin
      if /^(\d{1,2})\/(\d{1,2})\/(\d{2,4})/i =~ self.receipt_date
        @converted_date = DateTime.strptime(self.receipt_date, "%m/%d/%y %H:%M:%S")
        @converted_time = @converted_date.strftime("%I:%M%p")
        #if @converted_date < Date.current - Setting.ocr.expired_days.days
        #  @error_messages[:date] = "Receipt is expired"
        #  return false
        #end
        return true
      else
        @error_messages[:date] = "Invalid date"
        return false
      end
    rescue
      @error_messages[:date] = "Invalid date"
      return false
    end
  end

  def list_item
    items = []
    @xml_in_nokogiri_format.search("menu-item").each do |menu_item|
      item = {}
      item["id"] = menu_item.attr("id")
      item["name"] = menu_item.search("name").text
      item["amount"] = menu_item.search("total-amount").text.to_f
      item["qty"] = menu_item.search("quantity").text.to_decimal
      items << item
    end
    return items
  end

end