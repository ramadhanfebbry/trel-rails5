ActionMailer::Base.smtp_settings = {
    :address => Setting.smtp.host,
    :port => Setting.smtp.port,
    :user_name => Setting.smtp.username,
    :password => Setting.smtp.password,
    :authentication => :login
}