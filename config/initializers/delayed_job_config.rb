Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.max_attempts = Setting.delayed_job.max_attempts
Delayed::Worker.delay_jobs = !Rails.env.test?
Delayed::Worker.default_queue_name = ENV["JOB_QUEUE_NAME"].blank? ? "prel_job" : ENV["JOB_QUEUE_NAME"]

Delayed::Job.class_eval do
  establish_connection ActiveRecord::Base.configurations["#{Rails.env}"]
end

Delayed::Backend::Base::ClassMethods.module_eval do
  def enqueue(*args)
    options = {
        :priority => Delayed::Worker.default_priority,
        :queue => Delayed::Worker.default_queue_name
    }.merge!(args.extract_options!)

    options[:payload_object] ||= args.shift

    if args.size > 0
      warn "[DEPRECATION] Passing multiple arguments to `#enqueue` is deprecated. Pass a hash with :priority and :run_at."
      options[:priority] = args.first || options[:priority]
      options[:run_at]   = args[1]
    end

    unless options[:payload_object].respond_to?(:perform)
      raise ArgumentError, 'Cannot enqueue items which do not respond to perform'
    end

    if Delayed::Worker.delay_jobs
      self.new(options).tap do |job|
        Delayed::Worker.lifecycle.run_callbacks(:enqueue, job) do
          job.hook(:enqueue)
          job.delayable_type = options[:delayable_type] unless options[:delayable_type].blank?  rescue nil
          job.delayable_id = options[:delayable_id]  unless options[:delayable_id].blank? rescue nil
          job.run_at = options[:run_at] unless options[:run_at].blank? rescue Time.now
          job.save
        end
      end
    else
      Delayed::Job.new(:payload_object => options[:payload_object]).tap do |job|
        job.invoke_job
      end
    end
  end
end