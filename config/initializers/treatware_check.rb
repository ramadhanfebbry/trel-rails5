# encoding: utf-8
require 'nokogiri'
#require 'fuzzystringmatch'
require 'date'
require 'time'

class TreatwareCheck
  attr_accessor :json_check, :error_messages, :converted_date, :converted_time

  def initialize(json)
    @json_check = json
    @error_messages = {}
    @converted_date = nil
    @converted_time = nil
    @transaction_time = nil
  end

  def sub_total
    BigDecimal.new(self.json_check["order"]["subTotal"].to_s) rescue 0
  end

  def tax
    BigDecimal.new(self.json_check["order"]["tax"].to_s) rescue 0
  end

  def total
    BigDecimal.new(self.json_check["order"]["total"].to_s) rescue 0
  end

  def discount
    total_discount = 0
    discounts_list = self.json_check["order"]["discounts"]
    discounts_list.each do |disc|
      total_discount += (BigDecimal(disc["price"].to_s) rescue 0)
    end unless discounts_list.blank?
    total_discount
  end

  def store_id
    self.json_check["header"]["storeId"]
  end

  def pos_restaurant_id
    pos_location = PosLocation.where(:apikey => self.store_id).first
    pos_location.try(:restaurant_id)
  end

  def operator_id
    self.json_check["header"]["operatorId"]
  end

  def pos_group
    self.json_check["header"]["posGroup"]
  end

  def terminal_id
    self.json_check["header"]["terminalId"]
  end

  def pos_order_id
    (self.json_check["order"]["posOrderId"] || self.json_check["order"]["checkId"]).to_s
  end

  def destination
    self.json_check["order"]["destination"]
  end

  def transaction_time
    selected_date = self.json_check["order"]["posTransactionTime"]
    if selected_date.blank?
      selected_date = Time.zone.now.strftime("%m/%d/%y %H:%M:%S %Z")
    end
    selected_date
  end

  def short_code
    self.json_check["shortCode"]
  end

  def receipt_number
    [self.pos_order_id, self.store_id].join("-")
  end

  def receipt_identifier
    [self.pos_order_id, self.store_id].join("-")
  end

  def line_items
    self.json_check["order"]["lineItems"] rescue []
  end

  def payment_types
    self.json_check["order"]["payments"] rescue []
  end

  def gift_card_included?
    self.payment_types.map{|x| x["type"]}.include?("MOBILE_GIFT_CARD")
  end

  def build_item_discount(loyalty_discount_list)
    child_loyalty_discount_list = loyalty_discount_list
    response = []
    self.line_items.reverse.each do |line_item|
      existing_child_item = (line_item["childItems"].blank? ? [] : line_item["childItems"])
      return_loyalty_discount = loyalty_discount_list.find do |loyalty_discount|
        if loyalty_discount["id"] == line_item["id"]
          if existing_child_item.select{|a| a["type"] == "LOYALTY_DISCOUNT"}.blank?
            existing_child_item << {
                "type" => "LOYALTY_DISCOUNT",
                "price" => loyalty_discount["discount"],
                "quantity" => 1,
                "description" => loyalty_discount["description"],
                "reference" => loyalty_discount["reference"],
                "childItems" => []
            }
            loyalty_discount_list.delete_at(loyalty_discount_list.rindex(loyalty_discount))
          else
            existing_child_item.delete_if{|ci| ci["type"] == "LOYALTY_DISCOUNT"}
          end
        #else
        #  existing_child_item.delete_if{|ci| ci["type"] == "LOYALTY_DISCOUNT"}
        end
      end
      if return_loyalty_discount.nil? && !existing_child_item.select{|a| a["type"] == "LOYALTY_DISCOUNT"}.blank?
        existing_child_item.delete_if{|ci| ci["type"] == "LOYALTY_DISCOUNT"}
      end
      response << {
          "id" => line_item["id"],
          "type" => line_item["type"],
          "price" => line_item["price"].to_f,
          "quantity" => line_item["quantity"],
          "category" => line_item["categories"],
          "description" => line_item["description"],
          "reference" => line_item["reference"],
          "childItems" => existing_child_item
      }
    end
    #add child discount
    new_response = []
    response.each do |line_item|
      existing_child_item = (line_item["childItems"].blank? ? [] : line_item["childItems"])
      unless existing_child_item.select{|a| a["type"] == "LOYALTY_DISCOUNT"}.blank?
        child_loyalty_discount_list.each do |loyalty_discount|
          if line_item["id"] == loyalty_discount["parent_menu_item_id"]
            existing_child_item <<
                {
                    "type" => "LOYALTY_DISCOUNT",
                    "price" => loyalty_discount["discount"],
                    "quantity" => 1,
                    "description" => loyalty_discount["description"],
                    "reference" => loyalty_discount["reference"],
                    "childItems" => []
                }
          end
        end
      end

      new_response << {
          "id" => line_item["id"],
          "type" => line_item["type"],
          "price" => line_item["price"].to_f,
          "quantity" => line_item["quantity"],
          "category" => line_item["categories"],
          "description" => line_item["description"],
          "reference" => line_item["reference"],
          "childItems" => existing_child_item
      }
    end
    new_response.reverse
  end

  def build_response_for_discount_applied(loyalty_discount_list)
    return {"posLoyaltyApplyOffersResult" => {
        "statusCode" => 0,
        "order" => {
          "totalLoyaltyCost" => 1,
          "lineItems" => build_item_discount(loyalty_discount_list)
    }}}
  end

  def build_discount_for_discount_applied_treatware(request_order, loyalty_discount_list)
    result =  {"posLoyaltyApplyOffersResult" =>  request_order.merge!({"statusCode" => 0})}
    p "---build_discount_for_discount_applied_treatware---" * 10
    p result
    result["posLoyaltyApplyOffersResult"]["order"]["discounts"] = [] if result["posLoyaltyApplyOffersResult"]["order"]["discounts"].blank?
    loyalty_discount_list.each do |loyalty_discount|
      result["posLoyaltyApplyOffersResult"]["order"]["discounts"] << {
          "shortCode" => self.short_code.to_s,
          "type" => "LOYALTY_DISCOUNT",
          "price" => loyalty_discount["discount"],
          "quantity" => 1,
          "description" => loyalty_discount["description"],
          "reference" => loyalty_discount["reference"],
          "childItems" => []
      }
    end
    result
  end


  def valid_total?(offer_id = nil, check_upload=nil)
    valid = true
    #unless self.total > 0
    #  valid = false
    #  @error_messages[:total] = "Total should greater than 0"
    #  return valid
    #end

    if self.sub_total.to_f == 0.0 # offer rule purpose forgot loyalty items
      offer = Offer.find(offer_id) rescue nil
      # check the rule first
      if !offer.blank? and offer.is_zero_subtotal?
        g_items = []
        unless offer.offer_rules_subtotal_zeros.blank?
          offer.offer_rules_subtotal_zeros.each do |zero_rules|
            g_items << zero_rules.offer_rule_menu_items
          end
        end
        g_items = g_items.flatten
        #item_numbers = g_items.pos_menu_items.map(&:item_number)
        item_numbers = []
        g_items.each do |g_item|
          item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number)
        end
        offer_item_parser = Parser::OfferItemsParser.new(check_upload)
        offer_item_parser.xml_data = @xml
        result_with_qty = offer_item_parser.find_item_number_and_quantity
        #purchased_item_match = []
        unless result_with_qty.blank?
          result_with_qty.each do |res|
            # if the purchase are correct
            if item_numbers.include?(res[:id].to_i)
              return valid # there are item match with the zero subtotal ( forgot loyalty item)
            end
          end
        end

      end
    end

    unless self.sub_total > 0
      valid = false
      @error_messages[:total] = "Sub Total less than 0"
      return valid
    end

    #unless (self.total.round(3)).eql?((self.tax + self.sub_total).round(3))
    #  valid = false
    #  @error_messages[:total] = "SubTotal + Tax not equal Total"
    #  return valid
    #end
    #unless self.total >= self.sub_total
    #  valid = false
    #  @error_messages[:total] = "Total less than subtotal"
    #  return valid
    #end
    return valid
  end

  def valid_date?
    begin
      if /^(\d{1,2})\/(\d{1,2})\/(\d{2,4})/i =~ self.transaction_time
        @converted_date = DateTime.strptime(self.transaction_time, "%m/%d/%y %H:%M:%S %Z")
        @converted_time = @converted_date.strftime("%I:%M%p")
        if @converted_date < Date.current - Setting.ocr.expired_days.days
          @error_messages[:date] = "Receipt is expired"
          return false
        end
        return true
      else
        @error_messages[:date] = "Invalid date"
        return false
      end
    rescue
      @error_messages[:date] = "Invalid date"
      return false
    end
  end

  def self.get_line_items(chain, line_items)
    items_check_buy = []
    line_items.each do |menu_item|
      item = {}
      item[:description] = menu_item["description"] rescue ""
      item[:menu_item_id] = menu_item["id"].to_s.strip rescue nil
      item[:quantity] = menu_item["quantity"].to_i rescue 0
      item[:price] = menu_item["price"].to_f rescue 0
      arr_general_menu_item_id = []
      pos_menu_items = PosMenuItem.select("id").where(:item_number => item[:menu_item_id], :chain_id => chain.id)
      pos_menu_items.each do |pos_menu_item|
        arr_general_menu_item_id = (pos_menu_item.general_menu_items.map(&:id) rescue [])
        break unless arr_general_menu_item_id.blank?
      end
      item[:general_menu_item_id] = arr_general_menu_item_id
      unless  menu_item["childItems"].blank?
        item[:child_items] = []
        item[:child_items] = TreatwareCheck.get_line_items(chain, menu_item["childItems"])
      else
        item[:child_items] = []
      end
      items_check_buy << item
    end unless line_items.blank?
    items_check_buy
  end

  def self.sort_items_by_price(items, lowest = true)
    return items if items.blank?
    if lowest
      items = items.sort_by{|a| a[:price]}
    else
      items = items.sort_by{|a| -a[:price]}
    end
    items.each do |item|
      tmp = self.sort_items_by_price(item[:child_items], lowest)
      item[:child_items] = []
      item[:child_items] = tmp
    end
    items
  end

  def check_discount(pos_location, pos_discount_type, reward_menu_items, items)
    return [] if pos_discount_type.blank?
    items_discount = []
    error_message = nil
    required_menu_items = reward_menu_items.select{|a| a.required }
    optional_menu_items = reward_menu_items.select{|a| !a.required }

    #check min no items ordered
    qualified_items_ordered_validation = items.select{|a| !(a[:general_menu_item_id] & reward_menu_items.map(&:general_menu_item_id)).blank?}
    return [items_discount, "Discount is not activated, because minimum number of items ordered validation."] if pos_discount_type.min_no_items_ordered < 1
    return [items_discount, "Discount is not activated, because number of items to be discounted validation."] if pos_discount_type.min_no_items_discounted < 1

    return [items_discount, "The Items that you bought is not met with the requirement minimum number of items ordered."] if qualified_items_ordered_validation.count < pos_discount_type.min_no_items_ordered && pos_discount_type.min_no_items_ordered > 1

    item_discounted = 0
    items.each do |filtered_item|
      unless (filtered_item[:general_menu_item_id] & reward_menu_items.map(&:general_menu_item_id)).blank?
        reward_discount_count = RewardDiscount.where(:check_id => self.pos_order_id, :restaurant_id => pos_location.restaurant_id).count
        if reward_discount_count < 1
          if pos_discount_type.blank?
            calculated_discount_from_reward = filtered_item[:price]
          elsif pos_discount_type.is_discount_percentage?
            calculated_discount_from_reward = ((pos_discount_type.discount_amount.to_f/100) * filtered_item[:price])
          elsif pos_discount_type.is_discount_price?
            #calculated_discount_from_reward = (filtered_item[:price_per_item] - pos_discount_type.discount_amount).round(2)
            calculated_discount_from_reward = (filtered_item[:price] > pos_discount_type.discount_amount ? (filtered_item[:price] - pos_discount_type.discount_amount) : filtered_item[:price])
          elsif pos_discount_type.is_discount_fixed_price?
            calculated_discount_from_reward = (filtered_item[:price] > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : filtered_item[:price])
          end
          item_discounted += 1
          discount_item = {:menu_item_id => filtered_item[:menu_item_id], :description => filtered_item[:description],  :discount => calculated_discount_from_reward}
          unless filtered_item[:child_items].blank?
            discount_item[:child_discount] = []
            discount_item[:child_discount] << check_child_discount(pos_location, pos_discount_type, optional_menu_items, filtered_item[:child_items], item_discounted)
          else
            discount_item[:child_discount] = []
          end
          discount_item[:child_discount] = discount_item[:child_discount].flatten
          items_discount << discount_item
          break if item_discounted >= pos_discount_type.min_no_items_discounted
        else
          error_message = "Reward code invalid, one reward per check."
        end
      end
    end
    p "THE DISCOUNT IS"
    p items_discount
    p "---------------------"
    sum_of_discount = 0
    unless items_discount.blank?
      items_discount.each do |disc|
        sum_of_discount += disc.find_all_values_for(:discount).sum
      end
      sum_of_discount = '%.2f' % sum_of_discount
      return [[{:description=>"Loyalty Discount", :discount => sum_of_discount.to_f, :child_discount=> [] }], error_message ]
    else
      return [[], error_message]
    end
  end

  def check_child_discount(pos_location, pos_discount_type, optional_reward_menu_items, items, item_discounted)
    items_discount = []
      items.each do |filtered_item|
        unless (filtered_item[:general_menu_item_id] & optional_reward_menu_items.map(&:general_menu_item_id)).blank?
          reward_discount_count = RewardDiscount.where(:check_id => self.pos_order_id, :restaurant_id => pos_location.restaurant_id, :menu_item_id => filtered_item[:menu_item_id]).count
          if reward_discount_count < filtered_item[:quantity]
            if pos_discount_type.blank?
              calculated_discount_from_reward = filtered_item[:price]
            elsif pos_discount_type.is_discount_percentage?
              calculated_discount_from_reward = ((pos_discount_type.discount_amount.to_f/100) * filtered_item[:price])
            elsif pos_discount_type.is_discount_price?
              #calculated_discount_from_reward = (filtered_item[:price_per_item] - pos_discount_type.discount_amount).round(2)
              calculated_discount_from_reward = (filtered_item[:price] > pos_discount_type.discount_amount ? (filtered_item[:price] - pos_discount_type.discount_amount) : filtered_item[:price])
            elsif pos_discount_type.is_discount_fixed_price?
              calculated_discount_from_reward = (filtered_item[:price] > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : filtered_item[:price])
            end
            item_discounted += 1
            discount_item = {:menu_item_id => filtered_item[:menu_item_id], :description => filtered_item[:description],  :discount => calculated_discount_from_reward}
            unless filtered_item[:child_items].blank?
              discount_item[:child_discount] = []
              discount_item[:child_discount] << check_child_discount(pos_location, pos_discount_type, optional_reward_menu_items, filtered_item[:child_items], item_discounted)
            else
              discount_item[:child_discount] = []
            end
            discount_item[:child_discount] = discount_item[:child_discount].flatten
            items_discount << discount_item
            break if item_discounted >= pos_discount_type.min_no_items_discounted
          end
        end
      end
    items_discount
  end

  def parent_line_item_discount(pos_location, items_discount, reward_transaction)
    child_item_for_treatware_discount = []
    items_discount.each do |item|
      RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => self.pos_order_id, :restaurant_id => pos_location.restaurant_id, :discount => item[:discount], :menu_item_id => item[:menu_item_id])
      child_item_for_treatware_discount << {
          "id" => item[:menu_item_id],
          "discount" => item[:discount],
          "description" => "Free #{item[:description]}",
          "reference" => "reward_transaction_#{reward_transaction.id}",
          "parent_menu_item_id" => nil
      }
      child_item_for_treatware_discount << child_line_item_discount(item[:menu_item_id], pos_location, item[:child_discount], reward_transaction)
    end
    child_item_for_treatware_discount
  end

  def child_line_item_discount(parent_menu_item, pos_location, child_discount, reward_transaction)
    child_item_for_treatware_discount = []
    child_discount.each do |item|
      RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => self.pos_order_id, :restaurant_id => pos_location.restaurant_id, :discount => item[:discount], :menu_item_id => item[:menu_item_id])
      child_item_for_treatware_discount << {
          "id" => item[:menu_item_id],
          "discount" => item[:discount],
          "description" => "Free #{item[:description]}",
          "reference" => "reward_transaction_#{reward_transaction.id}",
          "parent_menu_item_id" => parent_menu_item
      }
      child_item_for_treatware_discount << child_line_item_discount(parent_menu_item, pos_location, item[:child_discount], reward_transaction)
    end
    child_item_for_treatware_discount
  end

end
