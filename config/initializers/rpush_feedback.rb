module Rpush
  module Apns
    class Feedback < ActiveRecord::Base
      self.table_name = 'rpush_feedback'

      # if Rpush.attr_accessible_available?
      attr_accessor :device_token, :failed_at, :app
      # end

      validates :device_token, :presence => true
      validates :failed_at, :presence => true
      #masih_salah
      # validates_with Rpush::Apns::DeviceTokenFormatValidator

      def self.clear_feedback
        Rpush::Apns::Feedback.all.each do |feedback|
          User.where(:device_token => feedback.device_token).each do |user|
            user.update_attributes(:device_token => nil, :sign_in_device_type => nil, :device_id => nil)
          end
          feedback.destroy
        end
      end

    end
  end
end
