class Time
  
  def custom_beginning_of_hour
    change(:min => 0, :sec => 0, :usec => 0)
  end

  def custom_end_of_hour
    change(:min => 59, :sec => 59, :usec => 999999)
  end

  def custom_beginning_of_10minutes
    change(:min => self.min-(self.min%10), :sec => 0, :usec => 0)
  end
  
  def custom_end_of_10minutes
    change(:min => self.min+(9-(self.min%10)), :sec => 59, :usec => 999999)
  end

  def custom_beginning_of_week
    self.beginning_of_week(:sunday)
  end

  def custom_end_of_week
    self.end_of_week(:sunday)
  end
  
  def beginning_of_week(start_day = :monday)
    days_to_start = days_to_week_start(start_day)
    (self - days_to_start.days).midnight
  end

  def end_of_week(start_day = :monday)
    days_to_end =  6 - days_to_week_start(start_day)
    (self + days_to_end.days).end_of_day
  end
  
  def days_to_week_start(start_day = :monday)
    start_day_number = DAYS_INTO_WEEK[start_day]
    current_day_number = wday != 0 ? wday - 1 : 6
    days_span = current_day_number - start_day_number
    days_span >= 0 ? days_span : 7 + days_span
  end
  
end