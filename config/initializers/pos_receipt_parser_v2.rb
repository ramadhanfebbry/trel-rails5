require "#{Rails.root}/lib/net_pos/net_pos_receipt"

class PosReceiptParserV2
  attr_accessor :pos_check_upload_id, :user_id, :restaurant_id, :offer_id, :receipt_id, :parsed_receipt, :pos_xml

  def initialize(pos_check_upload_id, user_id, restaurant_id, offer_id, receipt_id)
    @pos_check_upload_id = pos_check_upload_id
    @user_id = user_id
    @restaurant_id = restaurant_id
    @offer_id = offer_id
    @receipt_id = receipt_id
    @parsed_receipt = nil
    @pos_xml = nil
  end

  def do_parse
    puts "start parse xml data from pos check upload"

    p @receipt_id
    receipt = Receipt.find(@receipt_id)
    @pos_check_upload = PosCheckUpload.find(@pos_check_upload_id)
    @pos_check_upload = @pos_check_upload.reload

    @user = User.find(@user_id)
    chain = @user.chain

    json_check = YAML.load(@pos_check_upload.xml_data) rescue nil

    if json_check.class == ActiveSupport::HashWithIndifferentAccess
      @pos_xml = PosReceiptJson.new(@pos_check_upload.xml_data)
    elsif json_check.class == Hash && chain.pos_used_type == Chain::POS_USED_TYPE["NETPOS"]
      @pos_xml = NetPosReceipt.new(@pos_check_upload.xml_data, @pos_check_upload.try(:pos_location).try(:netpos_timezone))
    else
      @pos_xml = PosReceiptXml.new(@pos_check_upload.xml_data)
    end

    location = Restaurant.find(@restaurant_id)
    restaurant_offer = RestaurantOffer.where(:restaurant_id => @restaurant_id, :offer_id => @offer_id).first rescue nil
    @offer = Offer.find(@offer_id)

    "**************--------- get reward redeem discount ---------- ***********"
    check_id = @pos_xml.check_id
    seq_num = @pos_xml.seq_num
    p "-- check id is #{check_id}"
    p "--- sequence number is #{seq_num}"
    total_reward_discount = RewardDiscount.sum(:discount, :conditions => {:check_id => check_id, :seq_num => seq_num, :restaurant_id => @restaurant_id})
    p total_reward_discount
    p "----- total discount selected is #{total_reward_discount}"

    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.restaurant_offer_id = (restaurant_offer.id rescue nil)
    receipt_transaction.subtotal = @pos_xml.sub_total
    receipt_transaction.tax = @pos_xml.tax
    receipt_transaction.restaurant_id = @restaurant_id
    receipt_transaction.total_discount = total_reward_discount

    valid_total = @pos_xml.valid_total?(@offer_id,@pos_check_upload)
    valid_date = @pos_xml.valid_date?

    p "validation total and date"
    p "valid_total #{valid_total}"
    p "valid_date #{valid_date}"
    p "--------------"
    p receipt_transaction.subtotal

    receipt_transaction.issue_date = @pos_xml.converted_date
    receipt_transaction.time_stamp = (@pos_xml.converted_time.strip rescue nil)
    receipt_transaction.receipt_date = @pos_xml.converted_date
    receipt_transaction.receipt_time = (@pos_xml.converted_time.strip rescue nil)

    if !valid_total && @pos_xml.total > 0 && @pos_xml.sub_total > 0 && location && location.rest_tax > 0.0
      tax_sub_total = @pos_xml.sub_total.to_f.eql?(0.0) ? 0.0 : (@pos_xml.tax.to_f/@pos_xml.sub_total.to_f).round(3)

      if (location.rest_tax * 0.98) <= tax_sub_total  && tax_sub_total <= (location.rest_tax * 1.02)
        valid_total = true
        @pos_xml.error_messages.delete(:total)
      end
    elsif valid_total && location
      tax_sub_total = @pos_xml.sub_total.to_f.eql?(0.0) ? 0.0 : (@pos_xml.tax.to_f/@pos_xml.sub_total.to_f).round(3)
      location.update_attribute(:rest_tax, tax_sub_total)
    end

    if valid_total and valid_date
      if unique_receipt?(chain, @pos_xml)
        p "----------UNIQUE RECEIPT_-------------"
        if !chain.blank? and chain.today_max_user_limit?(receipt, @pos_xml.converted_date)
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          @pos_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
        elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, @pos_xml.converted_date, @offer)
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          @pos_xml.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
        else
          receipt_transaction.status = Receipt::STATUS[:APPROVED]
          receipt.status = Receipt::STATUS[:APPROVED]
        end
      else
        if location
          if unique_receipt?(chain, @pos_xml, location)
            p "-------- UNIQUE RECEIPT WITH LOCATION----------"
            if !chain.blank? and chain.today_max_user_limit?(receipt, @pos_xml.converted_date)
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              @pos_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
            elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, @pos_xml.converted_date, @offer)
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              @pos_xml.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
            else
              receipt_transaction.status = Receipt::STATUS[:APPROVED]
              receipt.status = Receipt::STATUS[:APPROVED]
            end
          elsif unique_receipt?(chain, @pos_xml, location, @pos_xml.receipt_number)
            p "-----UNIQUE RECEIPT 3------"
            if !chain.blank? and chain.today_max_user_limit?(receipt, @pos_xml.converted_date)
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              @pos_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
            elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, @pos_xml.converted_date, @offer)
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              @pos_xml.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
            else
              receipt_transaction.status = Receipt::STATUS[:APPROVED]
              receipt.status = Receipt::STATUS[:APPROVED]
            end
          else
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            @pos_xml.error_messages[:receipt] = "Receipt should be uniq, this receipt have submitted before"
            ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, @pos_xml.error_messages, receipt).deliver
          end
        else
          @pos_xml.error_messages[:restaurant] = "Restaurant/Location is invalid."
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, @pos_xml.error_messages, receipt).deliver
        end
      end
    else
      receipt_transaction.status = Receipt::STATUS[:REJECTED]
      receipt.status = Receipt::STATUS[:REJECTED]
      ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, @pos_xml.error_messages, receipt).deliver
    end
    receipt_transaction.instructions = @pos_xml.error_messages
    receipt_transaction.receipt_number = @pos_xml.receipt_number
    receipt_transaction.restaurant_id = location ? location.id : nil
    p "this is receipt transaction object"
    p receipt_transaction

    receipt.receipt_transactions << receipt_transaction
    ## offer rules validation not met criteria
    ## if the rules not met

    begin
      if receipt_transaction.offer and receipt_transaction.offer.offer_rules.size > 0 # doe not met the rules
        puts "OFFER RULE:: NCR ONLINE ORDER PROCESS JOB start ---" * 19

        off_rule = OfferRuleCalculation::CalculateOfferRule.new(receipt_transaction.offer, receipt_transaction)
        off_rule.result
        tmp_total_points_earned = off_rule.total_points_get
        if tmp_total_points_earned.to_f == 0.0 || tmp_total_points_earned == -1 || tmp_total_points_earned == -2
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt_transaction.instructions = {:offer_rules_not_met => "This receipt does not met any rules"}
          receipt_transaction.instructions = {:ignore_item_list => "This receipt get all the ignore items listed"} if tmp_total_points_earned == -1
          receipt_transaction.instructions = {:ignore_item_reject => "Rejected due the item"} if tmp_total_points_earned == -2
          receipt.status = Receipt::STATUS[:REJECTED]
        end
        puts "OFFER RULE:: NCR ONLINE ORDER PROCESS JOB end ---" * 19
      end
    rescue => e
      puts "CALCULATE::OFFERRULES ----> #{e.inspect}"
    end
    ## end offer rule

    receipt.save
    p "this is receipt final status #{receipt.status}"
    if receipt.status == Receipt::STATUS[:APPROVED]
      "because receipt final status is approved, update pos check upload to approved"
      @pos_check_upload.update_attributes(
          :user_id => @user.id,
          :status => PosCheckUpload::STATUS[:PROCESSED]
      )
    end
    p "this is final pos check upload object"
    p @pos_check_upload
    puts "------ end ------"
  end

  def unique_receipt?(chain, ocr_xml, location = nil, receipt_number = nil)
    if location.blank? and receipt_number.blank?
      receipt_transaction_count = ReceiptApprovedDetail.select("id").where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ?", chain.id, ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip).count
      return true if receipt_transaction_count == 0
      return false
    elsif location and receipt_number.blank?
      receipt_transaction_count = ReceiptApprovedDetail.select("id").includes(:receipt).where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ?", chain.id, ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id).count
      return true if receipt_transaction_count == 0
      return false
    elsif receipt_number
      receipt_transaction_count = ReceiptApprovedDetail.select("id").includes(:receipt).where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ? AND receipt_number = ?", chain.id, ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id, receipt_number).count
      return true if receipt_transaction_count == 0
      return false
    else
      return false
    end
  end

end

