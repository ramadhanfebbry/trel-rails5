module GcmHelper
  class Sender
    def update_status(unsent_reg_ids, all_results, multicast_result)
      results = multicast_result.results
      raise RuntimeError, "Internal error: sizes do not match. currentResults: #{results}; unsentRegIds: #{unsent_reg_ids}" unless results.size==unsent_reg_ids.size

      new_unsent_reg_ids = []
      unsent_reg_ids.each_with_index {|reg_id, index|
        result = results[index]
        all_results[reg_id]= result

        unless (result.error_code.nil? || result.error_code.eql?(ERROR_UNAVAILABLE))
          if result.error_code.eql? ERROR_NOT_REGISTERED or result.error_code.eql? ERROR_INVALID_REGISTRATION
            GcmUnsentHistory.create(:reg_id => reg_id, :error_code => result.error_code, :failed_at => Time.now)
          else
            new_unsent_reg_ids << reg_id
          end
        end

      }
      new_unsent_reg_ids
    end

  end
end

