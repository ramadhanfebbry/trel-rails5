DEVICE_TYPE_SELECT = {"Android" => 1, "Iphone" => 2, "Browser" => 3, "Browser Compatible" => 4}

# Indicating if an app is available or not
APPLICATION_STATUS = {ACTIVE: "active",
    INACTIVE: "inactive",
    DISABLED: "disabled"}

APPLICATION_KEY_STATUS = {ACTIVE: "active",
    INACTIVE: "inactive",
    DISCONTINUED: "discontinued",
    DISABLED: "disabled"}

ADMIN_ROLES = {ADMIN: "admin",
    REVIEWER: "reviewer",
    BANNED: "banned",
    TESTER: "tester",
    CS: "customer support"}

OWNER_ROLES = {CREATE: "create",
               PROCESS: "process",
               CREATE_PROCESS: "create_process",
               EMAIL: "email",
               CREATE_EMAIL: "create_email",
               EMAIL_PROCESS: "email_process",
               ALL_SETTING: "create_email_process"
               }

RAILS_ENV = Rails.env

PHP_DB =  {
    :production => {
        :adapter  => "mysql2",
        :host     => "192.254.213.52",
        :username => "railuser",
        :password => "relevantmobile",
        :database => "slickeat_slickeat"
    },

    :development => {
        :adapter  => "mysql2",
        :host     => "127.0.0.1",
        :username => "root",
        :password => "password",
        :database => "slickeat_slickeat"
    }

}

AROMA_CHAIN_ID = 1
ZOES_CHAIN_ID = 5