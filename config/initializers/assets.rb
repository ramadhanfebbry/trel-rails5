# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
Rails.application.config.assets.precompile += 
  %w(admin/*.css api/v1/*.css api_sign/*.css bootstrap/*.css d2_css/**/*.css d2_css/*.css d2_js/*.js *.css *.js admin/*.js
  api_sign/*.js owner/*.js api_sign/**/*.png api_sign/**/*.ico d2_img/*.jpg d2_img/*.png d2_img/*.gif d2_img/*.ico 
  d2_img/**/*.png d2_img/**/*.gif d2_img/**/*.ico )