ENV["REDISTOGO_URL"] ||= "redis://localhost:6379"

Sidekiq.configure_server do |config|
  config.redis = { url: ENV["REDISTOGO_URL"] }

  database_url = ENV['DATABASE_URL']
  if database_url
    ENV['DATABASE_URL'] = "#{database_url}?pool=50"
    ActiveRecord::Base.establish_connection
  end
end


Sidekiq.configure_client do |config|
  config.redis = { url: ENV["REDISTOGO_URL"] }
end

#masih_salah
# module Sidekiq::Extensions::Klass
#   alias :sidekiq_delay :delay
#   remove_method :delay
#   alias :sidekiq_delay_for :delay_for
#   remove_method :delay_for
#   alias :sidekiq_delay_until :delay_until
#   remove_method :delay_until
# end

# module Sidekiq::Extensions::ActiveRecord
#   alias :sidekiq_delay :delay
#   remove_method :delay
#   alias :sidekiq_delay_for :delay_for
#   remove_method :delay_for
#   alias :sidekiq_delay_until :delay_until
#   remove_method :delay_until
# end

# module Sidekiq::Extensions::ActionMailer
#   alias :sidekiq_delay :delay
#   remove_method :delay
#   alias :sidekiq_delay_for :delay_for
#   remove_method :delay_for
#   alias :sidekiq_delay_until :delay_until
#   remove_method :delay_until
# end
