class Date
  class <<self
    alias_method :broke_ass_today, :today
  end

  def self.today
     Time.zone.now.to_date
  end
end