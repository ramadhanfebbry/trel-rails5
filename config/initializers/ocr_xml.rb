# encoding: utf-8
require 'nokogiri'
#require 'fuzzystringmatch'
require 'date'
require 'time'

class OcrXml
  attr_accessor :xml, :xml_in_nokogiri_format, :error_messages, :converted_date, :converted_time

  def initialize(xml)
    @xml = EscapeUtils.unescape_url(xml)
    @xml_in_nokogiri_format = Nokogiri::XML(@xml)
    @error_messages = {}
    @converted_date = nil
    @converted_time = nil
  end

  def sub_total
    @xml_in_nokogiri_format.xpath("//_SubTotal").text.to_decimal
  end

  def total
    @xml_in_nokogiri_format.xpath("//_Total").text.to_decimal
  end

  def tax
    @xml_in_nokogiri_format.xpath("//_Tax").text.to_decimal
  end

  def chain_name
    @xml_in_nokogiri_format.xpath("//_ChainName").text
  end

  def receipt_date
    @xml_in_nokogiri_format.xpath("//_ReceiptDate").text
  end

  def receipt_time
    @xml_in_nokogiri_format.xpath("//_ReceiptTime").text
  end

  def receipt_number
    @xml_in_nokogiri_format.xpath("//_ReceiptNumber").text.gsub("—", "-").gsub("â€”", "-")
  end

  def server_name
    @xml_in_nokogiri_format.xpath("//_ServerName").text
  end

  def cashier_number
    @xml_in_nokogiri_format.xpath("//_CashierNumber").text
  end

  def cashier_name
    @xml_in_nokogiri_format.xpath("//_CashierName").text
  end

  def payment_method
    @xml_in_nokogiri_format.xpath("//_PaidMethod").text
  end

  def amount_paid
    @xml_in_nokogiri_format.xpath("//_AmountPaid").text
  end

  def card_name
    @xml_in_nokogiri_format.xpath("//_CreditCardName").text
  end

  def card_number
    @xml_in_nokogiri_format.xpath("//_CreditCardNumber").text
  end

  def address_line
    @xml_in_nokogiri_format.xpath("//_AddressLine1").text
  end

  def city
    @xml_in_nokogiri_format.xpath("//_City").text
  end

  def state
    @xml_in_nokogiri_format.xpath("//_State").text
  end

  def zip
    @xml_in_nokogiri_format.xpath("//_Zip").text
  end

  def order_type
    @xml_in_nokogiri_format.xpath("//_OrderType").text
  end

  def line_items
    line_items = []
    @xml_in_nokogiri_format.xpath("//_LineItems").each do |line_item|
      item = {}
      item["qty"] = line_item.xpath("_Quantity").text.to_i
      item["description"] = line_item.xpath("_Description").text
      item["amount"] = line_item.xpath("_Amount").text.to_f
      line_items <<  item
    end
    line_items
  end


#  def replace_charater(text)
#    replacements = []
#    replacements << ['–', '-']           # long hyphen
#    replacements << ['â€“', '-']           # long hyphen
#
#    replacements.each{ |set| text = text.gsub(set[0], set[1]) }
#    text
#  end

  def payment_method_type
    return nil if payment_method.blank?
    return 1 if payment_method.downcase.eql?("cash")
    return 2
  end

  def full_address
    "#{self.address_line} #{self.city} #{self.state} #{self.zip}"
  end

  def valid_total?
    valid = true
    if self.total.eql?(0.0)
      valid = false
      @error_messages[:total] = "Total is 0"
      return valid
    end

    unless self.sub_total > 0
      valid = false
      @error_messages[:total] = "Sub Total less than 0"
      return valid
    end

    unless (self.total.round(3)).eql?((self.tax + self.sub_total).round(3))
      valid = false
      @error_messages[:total] = "SubTotal + Tax not equal Total"
      return valid
    end
    return valid
  end


  def valid_date?(receipt = nil)
    begin
      if /^(\d{1,2})\/(\d{1,2})\/(\d{2,4})/i =~ self.receipt_date
        current_date = Date.current
        format = $3.size.eql?(2) ? '%m/%d/%y' : '%m/%d/%Y'
        @converted_date = Date.strptime self.receipt_date, format
        p "converted Date : #{@converted_date}"
        p  "date today : #{current_date}"
        if @converted_date.year > current_date.year
          p "enter to this if block since converted date year > date today year"
          if @converted_date.beginning_of_month == @converted_date.beginning_of_year and receipt
            p "replace with receipt created at year"
            @converted_date = Date.strptime "#{$1}/#{$2}/#{receipt.created_at.year}", '%m/%d/%Y'
          else
            p "replace with current year"
            @converted_date = Date.strptime "#{$1}/#{$2}/#{current_date.year}", '%m/%d/%Y'
          end
        end
        p "converted date last result : #{@converted_date}"
        p "checking expired date : #{current_date - Setting.ocr.expired_days.days}"
        if @converted_date < current_date - Setting.ocr.expired_days.days
          @error_messages[:date] = "Receipt is expired"
          return false
        end
        return true
      else
        @error_messages[:date] = "Invalid date"
        return false
      end
    rescue
      @error_messages[:date] = "Invalid date"
      return false
    end
  end

  def valid_time?(restaurant_hours = [])
    begin
      if /(\d{1,2})[\:\.](\d{1,2})\s?([ap])/i =~ self.receipt_time
        time_format = "#{$1}:#{$2}#{$3}m".downcase #self.receipt_time.gsub(/[^apAP[0-12][:\.]\d]/, '').gsub(".", ":").gsub(/(p|a|P|A)+(.|\z)/){|x|  x.include?('m') ? x.downcase : x.downcase+'m'}
        @converted_time = Time.parse(time_format).strftime('%l:%M%p')
        return true
      end
      if /(\d{1,2})[\:\.](\d{1,2})/ =~ self.receipt_time
        restaurant_hours.each do |hour|
          if !hour.open_at.blank? and !hour.close_at.blank?
            converted_open_at = Time.parse(hour.open_at)
            converted_close_at = Time.parse(hour.close_at)

            am_time_converted = Time.parse("#{$1}:#{$2}am")
            pm_time_converted = Time.parse("#{$1}:#{$2}pm")

            if converted_open_at <= pm_time_converted and pm_time_converted <= converted_close_at
              @converted_time = pm_time_converted.strftime('%l:%M%p')
              return true
            end

            if converted_open_at <= am_time_converted and am_time_converted <= converted_close_at
              @converted_time = am_time_converted.strftime('%l:%M%p')
              return true
            end
          end
        end
      end
      @error_messages[:time] = "Invalid time"
      false
    rescue
      @error_messages[:time] = "Invalid time"
      false
    end
  end

  def custom_valid_time?
    begin
      if /(\d{1,2})[\:\.](\d{1,2})\s?(a|p|am|pm)$/i =~ self.receipt_time
        time_format = "#{$1}:#{$2}#{$3}m".downcase #self.receipt_time.gsub(/[^apAP[0-12][:\.]\d]/, '').gsub(".", ":").gsub(/(p|a|P|A)+(.|\z)/){|x|  x.include?('m') ? x.downcase : x.downcase+'m'}
        @converted_time = Time.parse(time_format).strftime('%l:%M%p').strip rescue nil
        return true
      end
      if /(\d{1,2})[\:\.](\d{1,2})/ =~ self.receipt_time
        time_format = "#{$1}:#{$2}#{$3}pm".downcase
        @converted_time = Time.parse(time_format).strftime('%l:%M%p').strip rescue nil
        return true
      end
      @error_messages[:time] = "Invalid time"
      false
    rescue
      @error_messages[:time] = "Invalid time"
      false
    end
  end

  def roti_valid_time?
    begin
      if /(\d{1,2})[\:\.](\d{1,2})[\:\.](\d{1,2})\s?([ap])/i =~ self.receipt_time
        time_format = "#{$1}:#{$2}:#{$3}#{$4}m".downcase #self.receipt_time.gsub(/[^apAP[0-12][:\.]\d]/, '').gsub(".", ":").gsub(/(p|a|P|A)+(.|\z)/){|x|  x.include?('m') ? x.downcase : x.downcase+'m'}
        @converted_time = Time.parse(time_format).strftime('%l:%M:%S%p').strip rescue nil
        return true
      end
      if /(\d{1,2})[\:\.](\d{1,2})[\:\.](\d{1,2})/ =~ self.receipt_time
        time_format = "#{$1}:#{$2}:#{$3}pm".downcase
        @converted_time = Time.parse(time_format).strftime('%l:%M:%S%p').strip rescue nil
        return true
      end
      @error_messages[:time] = "Invalid time"
      false
    rescue
      @error_messages[:time] = "Invalid time"
      false
    end
  end



  def should_check_restaurant_hour?
    !!(/\d{1,2}[\:\.]\d{1,2}/ =~ self.receipt_time)
  end

  def valid_receipt_number?(default = true)
    if default
      return true unless /[A-Za-z]+-[0-9]{4,6}$/.match(self.receipt_number.gsub("—", "-").gsub("â€”", "-")).blank?
    else
      return true unless /^[0-9]{2,7}\z/.match(self.receipt_number.gsub("—", "-").gsub("â€”", "-")).blank?
    end
    @error_messages[:receipt_number] = "Invalid receipt number"
    return false
  end

end