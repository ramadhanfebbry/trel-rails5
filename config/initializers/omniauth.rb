module OmniAuth::Strategies

  class FacebookNekter < Facebook
    def name
      :facebook_nekter
    end
  end

  class FacebookLpq < Facebook
    def name
      :facebook_lpq
    end
  end

end