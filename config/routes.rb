Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  require 'sidekiq/web'

  # omniauth client stuff
  match '/auth/:provider/callback' => 'authentications#create', via: [:get, :post]
  match '/auth/failure' => 'authentications#failure', via: [:get, :post]

  # Provider stuff
  match '/auth/olo/authorize' => 'auth#authorize', via: [:get, :post]
  match '/auth/olo/authorize_mobile_app' => 'auth#authorize_mobile_app', via: [:get, :post]
  match '/auth/olo/access_token' => 'auth#access_token', via: [:get, :post]
  match '/auth/olo/user' => 'auth#user', via: [:get, :post]
  match '/auth/olo/logout' => 'auth#session_destroy', via: [:get, :post]
  match '/oauth/token' => 'auth#access_token', via: [:get, :post]

  # Account linking
  match 'authentications/:user_id/link' => 'authentications#link', :as => :link_accounts, via: [:get, :post]
  match 'authentications/:user_id/add' => 'authentications#add', :as => :add_account, via: [:get, :post]

  #signuploginoauth
  match "login_oauth" => "authentications#login_oauth", :as => :login_oauth, via: [:get, :post]

  # signup confirmation
  get "users/:id/email_confirm" => "menus#confirm_email", :as => :confirm_email
  get "terms_of_use" => "menus#terms_of_use", :as => :tou
  get "privacy_policy" => "menus#privacy_policy", :as => :privacy_policy
  get "faq" => "menus#faq", :as => :faq
  get "faqs/:faq_data" => "menus#generated_faq", :as => :generated_faq
  get "about_us" => "menus#about_us", :as => :about_us
  get "contact_us_faq" => "menus#contact_us_faq", :as => :contact_us_faq
  get "game_faq" => "menus#game_faq", :as => :game_faq
  get "url/:chain_id/:url_id" => "menus#url", :as => :chain_url
  get "download_reward_transactions" => "menus#download_reward_transactions", :as => :download_reward_transactions
  get 'receipt/:url_id' => "menus#show_receipt", :as => :show_receipt
  post "status_url_skrill_payment" => "menus#status_url_skrill_payment", :as => :status_url_skrill_payment

  #post "receive", :to => "api/v1/olo_webhooks#create"
  post "olo_webhook/:chain_name", :to => "api/v1/olo_webhooks#create_olo"
  get "receive", :to => "api/v1/olo_webhooks#index"
  #resources :translations
  #get "translations/index"

  devise_for :owners, :controllers => { :sessions => "owner/sessions" }
  devise_scope :owner do
   get "owner/log_in", :to => "owner/sessions#new"
   post "owner/log_in", :to => "owner/sessions#create"
   get "owner/log_out", :to => "owner/sessions#destroy"
 end


  # devise_for :owners, :controllers => {:passwords => "owner/passwords", :registrations => "owner/registrations", :sessions => "owner/sessions", :omniauth_callbacks => 'owner/omniauth_callbacks'} #:sessions => "owner/sessions" }
  # devise_for :owners, :controllers => {:registrations => "owner/registrations", :sessions => "owner/sessions"} #:sessions => "owner/sessions" }
  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end

  devise_scope :owner do
    get "owner/log_in", :to => "owner/sessions#new"
    post "owner/log_in", :to => "owner/sessions#create"
    get "owner/log_out", :to => "owner/sessions#destroy"
    put "owner/:id" => 'owner/registrations#update', :as => 'owner_registration_update'
  end

  namespace :owner do
    resources :greetings, :path => "/summaries" do
      collection do
        get :search_summaries
        get :export_summaries
      end
    end




    resources :guest_relations, :path => "/request" do
      collection do
        get :search_status_relation
      end

      member do
        get "check_status_relation"
        post "update_status_relation"
        get "approve_or_reject"
      end
    end

    resources :owner_jobs, :path => "/jobs" do
      collection do
        get :search_users
        get :miss_you
        get :new_miss_you
        get :individual
        post :create_individual
        post :create_miss_job
        get :search_job_type
      end
      member do
        get 'completed_job'
      end
    end
    resources :members do
      collection do
        post :send_message
        post :search
      end
    end
    resources :owner_users do
      collection do
        get :export_pdf, :format => :pdf
      end
    end
    resources :surveys do
      collection do
        ##dashboard 2.3
        #match '/all_survey' => "surveys#new_all_survey"
        match '/all_survey' => "surveys#all_survey", via: [:get, :post]
        get :export_survey_dashboard
        get :search_location_survey
        #get :all_survey
        get :search_all_survey
        get :search
        get :search_chart
        post :process_request
        get :request_csv
        post :submit_request_csv
      end

      member do
        get "request_guest_relation"
        post "request_guest"

      end
    end
    resources :rewards
    resources :activities do
      collection do
        get :search_activities
        post :generate_restaurant
        post :generate_restaurant_rewards
        post :print_row_tables
        post :print_row_tables_rewards
        get :rewards
        post :export_rewards_redeemed
        post :export_activities
      end
      member do
        get :show_detail_receipt
        get :show_detail_reward
        get :show_popup_reward_activities
        get :show_receipt_barcode
        get :show_online_order
        get :show_olo_order
      end
    end
  end

  resource :charts do
    collection do
      get :active_members
      get :members
      get :average_frequency
      get :average_check_frequency
      get :frequency
      get :top5_surveys
      get :average_surveys
      get :avg_surveys
      get :survey_question1
      get :survey_question2
      get :survey_question3
      get :survey_question4
    end
  end

  # scope '(:locale)' do
  #   devise_for :admin
  # end
  scope '(:locale)' do
    devise_for :admin, skip: :registrations, controllers: {sessions: 'admin/sessions'} do
      # devise_scope :admin do
      get "sign_in" , to: "admin/sessions#new", as: :admin_sign_in
      get "log_in" , to: "admin/sessions#new", as: :new_admin_log_in
      post "log_in" , to: "admin/sessions#create", as: :admin_log_in
      get "log_out" , to: "admin/sessions#destroy", as: :admin_log_out
    end
  end
  devise_for :users, :controllers => {
    :sessions => "user/sessions", :registrations => "user/registrations", :omniauth_callbacks=>'user/omniauth_callbacks',
    :passwords => "user/passwords"
  }
  devise_scope :user do
    get "user/log_in", :to => "user/sessions#new"
    post "user/log_in", :to => "user/sessions#create"
    get "user/log_out", :to => "user/sessions#destroy"
  end

  namespace :admin do
    # scope '(:locale)' do
    #resources :user_locations, :only => :index
    mount Sidekiq::Web => '/sidekiq_redis'

    resources :partner_rewards do
      collection do
        post :search
        get :search
        get :load_categories
        get :load_restaurants
      end
    end
    resources :partner_categories do
      resources :partner_sub_categories
    end
    resources :partner_sub_categories do
      collection do
        post :load_restaurants
        get :load_menu_items
      end
      member do
        post :update_participation
        post :add_item
        post :remove_item
        get :search_menu_items
      end
    end
    resources :partner_menu_items do
      collection do
        post :load_restaurants
      end
    end
    resources :partner_code_settings do
      collection do
        post 'search', :to => "partner_code_settings#search"
      end
    end
    resources :partner_webhook_settings do
      collection do
        post 'search', :to => "partner_webhook_settings#search"
      end
    end

    resources :vantiv_gift_card_email_sent_logs, except: [:edit, :update, :destroy, :new, :create] do
      collection do
        match "search", via: [:get, :post]
      end

      member do
        post :resend_mail
      end
    end
    resources :push_service_notifications, except: [:edit, :update, :destroy], path: "push_services" do
      member do
        get "progress"
        get "cancel"
        get "resume"
        get "pause"
        get 'load_user_list'
      end
      collection do
        get 'sync'
        get 'get_time'
        get 'dashboard'
        get 'ipost'
      end
    end
    resources :push_service_points, except: [:edit, :update, :destroy] do
      member do
        get "progress"
        get "cancel"
        get "resume"
        get "pause"
      end
      collection do
        get 'sync'
      end
    end

    resources :papa_murphys, as: :papa_murphy_reports
    resources :webhooks do
      member do
        get "process_receipt_olo"
      end
    end

    resources :olo_orders do
      collection do
        post 'search'
        match 'olo_receipt_detail', :as => :olo_receipt_detail, via: :get
      end
      member do
        get "order_details"
        match "/order_details/:detail_id" ,:to => "olo_orders#olo_order_show", :via => :get , :as => :olo_order_show
        match "/order_detail_edit/:detail_id" ,:to => "olo_orders#olo_order_edit", :via => :get , :as => :olo_order_edit
        get "process_olo_detail"
        put "post_detail_olo"
        post "batch_process"
      end
    end

    resources :comments, :path => 'email_list' do
      member do
        get "update_email_process" => "comments#update_email_process"
        get "process_receipt" => "comments#process_receipt"
        get "nokogiri_parse_email" => "comments#nokogiri_parse_email"
      end
    end

    resources :miss_you_tasks do
      collection do
        get 'search'
        get 'testing_activation'
      end
      member do
        get 'sending_report'
        post 'post_sending_report_miss_you'
        get 'completed_job'
        put 'update_from_admin'
        get 'update_step',:format => :js
        post 'update_step_ajax',:format => :js
      end
    end

    resources :no_scan_tasks do
      collection do
        get 'search'
        get 'testing_activation'
      end
      member do
        get 'completed_job'
        put 'update_from_admin'
        get 'update_step',:format => :js
        post 'update_step_ajax',:format => :js
      end
    end

    resources :owner_jobs do
      collection do
        get 'search'
        get 'testing_activation'
      end
      member do
        get 'completed_job'
        put 'update_from_admin'
        get 'update_step',:format => :js
        post 'update_step_ajax',:format => :js
      end
    end
    resources :jobs
    resources :admin_jobs,:except => [:show, :edit, :destroy, :update]

    resources :report_subscriptions do
      collection do
        get "search_chain"
        get "search_report_description"
        get "report_settings"
        post "post_test_report"
      end

      member do
        get "subscription_filename"
        get "test_report"
        post "post_subscrition_filename"
        put  "post_subscrition_filename"
        get "get_filename_subscription"
      end
    end
    resources :report_definitions do
      member do
        post "activate_deactivate" => "report_definitions#activate_deactivate", :as => :activate_deactivate
        get "edit_parameter"
      end
    end

    resources :reports do
      collection do
        post "search_reports"
      end
    end
    resources :admins
    resources :search, :questions, :choices, :translations, :perks, :promo_codes, :notification_locales

    resources :surveys do
      member do
        get :edit_max_question
        get :set_rotating_position
        post :submit_position_rotating
        put :update_max_question
        post :update_order_number
        get 'set_choice_for_negative_response/:value', :to => "surveys#set_choice_for_negative_response", :as => :set_choice_for_negative_response
        get :pick_incentive
        get :load_onetime_reward
        get :load_points
        get :assign_incentive
        put :add_point
        get 'assign_with_existing_reward/:reward_id', :to => "surveys#assign_with_existing_reward", :as => :assign_with_existing_reward
        get 'edit_point'
        get 'remove_incentive/:type', :to => "surveys#remove_incentive", :as => :remove_incentive
        post :create_reward_incentive
      end

      collection do
        post :group_chain_list
      end
    end

    resources :managers, :except => [:show, :index,:edit, :destroy, :update], :controller => 'owners' do
      collection do
        get '', :to => "owners#managers"
        get 'new', :to => "owners#new_manager"
        get "restaurant_selection_manager", :to => 'owners#restaurant_selection_manager'
        post "create_manager", :to => 'owners#create_manager'
      end
      member do
        get 'list_manager', :to => "owners#show_managers"
        get 'edit', :to => "owners#edit_manager"
        put 'update', :to => "owners#update_manager"
        get "detail",  :to => "owners#detail_manager"
      end
    end


    resources :owners do
      collection do
        get "unknown_owner"
        get "question_to"
        get "restaurant_selection"
        post "set_email_to"
        post "select_chain"
        post "set_email_confirmation"
      end
      member do
        get "detail"
        get "dashboard_setting"
        get "set_user_owner_status"
        get "edit_permission"
        get "set_take_action_setting"
        get "set_take_action_setting_new"
        post "post_setting"
      end
    end

    resources :offers do
      collection do
        post "group_chain_list"
      end
      member do
      end
      resources :offer_rules do
        member do
          get 'general_rule_items'
          get "update_rule_status"
          get 'add_general_rule_item/:menu_item_id' => 'offer_rules#add_general_rule_item', :as => 'add_general_rule_item'
          get 'remove_general_rule_item/:menu_item_id' => 'offer_rules#remove_general_rule_item', :as => 'remove_general_rule_item'
          get 'set_type/:menu_item_id/:type' => 'offer_rules#set_type', :as => 'set_type'
        end
      end
    end

    resources :online_offers do
      collection do
        post "group_chain_list"
      end
      member do

      end
    end

    resources :restaurants do
      collection do
        get "deleted" => 'restaurants#deleted', :as => :deleted
        get "new_owner"
        get "owner_restaurant_list"
        post "create_owner"
        get "by_chain"
        post "group_chain_list"
        get "import_restaurants"
        post "import_preview"
        post "create_import"
        get "upload_restaurants"
        post "do_upload_restaurants"
        get "ftp_form"
        post "import_by_ftp"
        get "get_list_file_ftp"
        get "import_restaurants_histories"
        get "import_restaurants_status"
      end
      member do
        get 'change_status_restaurant'
        put 'save_status_restaurant'
        get 'revive' => 'restaurants#revive', :as => :revive
        get "avg_visit"
        match "/:user_id/user_visits" ,:to => "restaurants#user_visits", :via => :get , :as => :user_visits
      end
    end

    resources :milestones do
      member do
        get 'new' => 'milestones#new', :as => :new
        get "milestones_by_chain" => 'milestones#milestones_by_chain', :as => :milestones_by_chain
        get 'add_reward' => "milestones#add_reward", :as => "add_reward"
        post 'create_milestone_reward' => 'milestones#create_milestone_reward', :as => "create_milestone_reward"
        get 'edit_reward/:reward_id' => "milestones#edit_reward", :as => 'edit_reward'
        put 'update_milestone_reward/:reward_id' => "milestones#update_milestone_reward", :as => 'update_milestone_reward'
      end
    end

    resources :unsent_android_notifications do
      member do
        get 'remove_regid' => 'unsent_android_notifications#remove_regid', :as => :remove_regid
      end
    end

    resources :queue_notif_settings do

    end

    resources :push_service_notification_certificates do
      collection do
        get "search", :to => "push_service_notification_certificates#index", :as => :search
      end
    end

    resources :applications do
      member do
        get 'set_key' => "applications#set_key", :as =>  :set_key
        get 'change_status/:status' => 'applications#change_status', :as => :change_status
        get 'test_send_apn' => 'applications#test_send_apn', :as => :test_send_apn
        post 'send_apn' => 'applications#send_apn', :as => :send_apn
      end
      resources :application_keys do
        member do
          get 'update_status/:status' => 'application_keys#update_status'
          get 'set_newest_version/:newest' => 'application_keys#set_newest_version'
        end
      end

      resources :certificates do
        collection do
          get 'download'
        end
      end
    end

    resources :referral_programs do
      resources :referral_codes do
        resources :referral_users

        collection do
          get "generate_codes", :to => "referral_codes#generate_codes"
          get "search", :to => "referral_codes#search", :as => :search
        end
      end

      collection do
        get "notifications", :to => "referral_programs#notifications"
        post "search", :to => "referral_programs#search", :as => :search
        match '/:chain_id/new_ref', :to => "referral_programs#new_ref", :via => :get , :as => :new_ref, :format => :js
        match '/:id/:chain_id/edit_ref', :to => "referral_programs#edit_ref", :via => :get , :as => :edit_ref, :format => :js
        match '/:id/:chain_id/show_ref', :to => "referral_programs#show_ref", :via => :get , :as => :show_ref, :format => :js
      end

      member do
        get :pick_incentive, :format => :js
        get :assign_incentive , :format => :js
        get :load_onetime_reward, :format => :js
        get :load_points, :format => :js
        get 'assign_with_existing_reward/:reward_id' => 'referral_programs#assign_with_existing_reward', :as => :assign_with_existing_reward
        post 'create_reward_incentive' => 'referral_programs#create_reward_incentive', :as => :create_reward_incentive
        post 'create_offer_incentive' => 'referral_programs#create_offer_incentive', :as => :create_offer_incentive
        get 'remove_incentive/:incentive_id' => 'referral_programs#remove_incentive', :as => :remove_incentive
        put 'update_incentive' => 'referral_programs#update_incentive', :as => :update_incentive
        get 'edit_incentive' => 'referral_programs#edit_incentive', :as => :edit_incentive
        get "activate" => 'referral_programs#activate', :as => :activate
      end
    end

    resources :promotions do
      collection do
        get 'search', :to => "promotions#search", :as => :search_promotion
        get "reward_lists", :to => "promotions#reward_list", :as => :reward_list
        match '/search_rewards',:to => "promotions#search_rewards", :via => :any,:as => :search_rewards
        match '/search_perks',:to => "promotions#search_perks", :via => :any,:as => :search_perks
        match '/search_offers',:to => "promotions#search_offers", :via => :any,:as => :search_offers
        match '/perk_lists',:to => "promotions#perk_list", :via => :any,:as => :perk_list
        match '/promo_offer_lists',:to => "promotions#promo_offer", :via => :any,:as => :promo_offer
        match '/new_offer',:to => "promotions#new_offer", :via => :any,:as => :new_offer
        match '/create_promo_offer',:to => "promotions#create_offer", :via => :any,:as => :create_promo_offer
        match "/generate_promo_code", :to => "promotions#generate_promo_code", :via => :get, :as => :generate_promo_code
        post "preview_xls_data"
        get 'promotion_notification/:id', :to => "promotions#promotion_notification", :as => :promotion_notification
      end



      member do
        get 'promo-codes', :to => "promotions#show_promo_codes", :as => :show_promo_codes
        get 'code-histories', :to => "promotions#code_histories", :as => :code_histories
        get "search_code_by_email_and_code", :to => "promotions#search_code_by_email_and_code", :as => :search_code_by_email_and_code
      end
    end

    resources :rewards do
      collection do
        match '/:chain_id/search_rewards', :to => "rewards#search_rewards", :via => :get , :as => :search_rewards
        match '/:chain_id/new_push_reward', :to => "rewards#new_push_reward", :via => :get , :as => :new_push_reward
        match '/:chain_id/new_birthday_reward', :to => "rewards#new_birthday_reward", :via => :get , :as => :new_birthday_reward
        match '/:chain_id/new_promo_reward', :to => "rewards#new_promo_reward", :via => :get , :as => :new_promo_reward
        match '/:chain_id/new_regular_reward', :to => "rewards#new_regular_reward", :via => :get , :as => :new_regular_reward
        match '/:chain_id/new_dashboard_reward', :to => "rewards#new_dashboard_reward", :via => :get , :as => :new_dashboard_reward
        match '/:chain_id/new_gifting_reward', :to => "rewards#new_gifting_reward", :via => :get, :as => :new_gifting_reward
        match '/:chain_id/new_anniversary_reward', :to => "rewards#new_anniversary_reward", :via => :get , :as => :new_anniversary_reward
        match '/:chain_id/list', :to => "rewards#collection_list", :via => :any, :as => :collection_list
        match '/search',:to => "rewards#search", :via => :get
        match '/search_users',:to => "rewards#search_users",:via => :any, :as => :search_users
        match '/:chain_id/one_time_reward',:to => "rewards#one_time_reward",:via => :get, :as => :one_time_reward
        get ":reward_id/:event_id/assign_reward", :to => "rewards#assign_reward", :via => :any, :as => :assign_reward
        get ":reward_id/:event_id/remove_reward", :to => "rewards#remove_reward", :via => :any, :as => :remove_reward
        post 'send_test_email' => "rewards#send_test_email", :as => :send_test_email
        get 'push_reward'
        get 'push_reward_jobs'
        post 'push_reward_to_users'
        post "preview_xls_data"
        post :upload_file_reward
        get :get_uploaded_file
        post :upload_file_thumbnail_reward
        get :get_uploaded_file_thumbnail
      end

      member do
        get 'milestones' => "rewards#milestones", :as => :milestones
        get 'add_milestone' => "rewards#add_milestone", :as => :add_milestone
        get 'edit_milestone/:chain_id' => "rewards#edit_milestone", :as => :edit_milestone
        get 'add_reward' => "rewards#add_reward", :as => :add_reward
        put 'update_milestone/:chain_id' => "rewards#update_milestone", :as => :update_milestone
        post 'create_milestone' => 'rewards#create_milestone', :as => :create_milestone
        get 'remove_milestone_reward/:reward_id(/:from)' => 'rewards#remove_milestone_reward', :as => :remove_milestone_reward
        get 'add_milestone_reward/:reward_id' => 'rewards#add_milestone_reward', :as => :add_milestone_reward
        get 'add_new_milestone_reward' => 'rewards#add_new_milestone_reward', :as => "add_new_milestone_reward"
        post 'create_new_milestone_reward' => 'rewards#create_new_milestone_reward', :as => "create_new_milestone_reward"
        get 'reward_sent_histories'
        get 'load_user_list'
        get 'view_claim_history'
        get 'claim_history_job'
        get 'unused_pos_codes'
        get 'general_items'
        get 'add_general_item/:menu_item_id' => 'rewards#add_general_item', :as => 'add_general_item'
        get 'remove_general_item/:menu_item_id' => 'rewards#remove_general_item', :as => 'remove_general_item'
        get 'discount_type'
        post 'create_pos_discount_type'
        get 'set_type/:menu_item_id/:type' => 'rewards#set_type', :as => 'set_type'
        get 'fishbowl_promotion_setting'
        post 'create_fishbowl_promotion_setting'
        get 'fishbowl_promotion_codes'
      end


      resources :reward_pos_codes

    end

    resources :answers do
      collection do
        get 'search'
        get "export_answer_csv"
      end
    end
    resources :cities do
      collection do
        get :load_states, :as => :js
        get :lookup
        post :search_cities
      end
      member do
        post :update_status
      end
    end
    resources :regions do
      collection do
        post :search_region
      end

      member do
        post :update_status
      end
    end
    resources :countries do
      collection do
        post :search_country
      end

      member do
        post :update_status
      end

      resources :regions do
        resources :cities
      end
    end
    resources :receipts do
      resources :receipt_transactions
      collection do
        get 'search'
        get 'export_status'
        get 'review'
        get 'review_search'
        post "search_users"
      end
      member do
        get 'blocked'
        get 'url'
        get 'goto'
      end
    end

    resources :push_points do
      collection do
        post "preview_xls_data"
        get "all_user"
        get "filter_push_points"
      end
      member do
        get :summary_success
        get :summary_failed
      end
    end

    resources :remove_points do
      member do
        get :summary_success
        get :summary_failed
      end
    end

    resources :push_rewards do
      collection do
        get "all_user"
        get "filter_push_rewards"
      end
      member do
        get :summary_success
        get :summary_failed
        put :execute_again
      end

      collection do
        get :push_reward_new
      end
    end

    resources :plain_push_notifications do
      resources :history_notifications
      collection do
        post "preview_xls_data"
        get "get_email_for_chains"
        get "filter_plain_push_notifications"
      end

      member do
        get "email_list"
        get "show_email_list"
        get "show_log_notifications"
      end
    end

    resources :chains do
      resources :giftcard_skins
      resources :chain_setting_alerts do
        member do
          put "test_no_scan_alert_chain"
        end
      end
      resources :geo_data_settings
      resources :appcode_settings
      get 'new_no_scan' => "task_definitions#new_no_scan" , :as => "new_no_scan"
      post "post_no_scan" => "task_definitions#post_no_scan", :as => "post_no_scan"
      put "update_no_scan/:no_scan_id" => "task_definitions#update_no_scan", :as => "update_no_scan"
      get 'edit_no_scan/:no_scan_id' => "task_definitions#edit_no_scan" , :as => "edit_no_scan"
      get 'notifications' => "chains#group_notification", :as => "group_notifications"

      resources :notification_settings do
        member do
          put "update_we_miss/:task_definition_id" => "notification_settings#update_we_miss", as: "update_we_miss"
          put "update_no_scan/:task_definition_id" => "notification_settings#update_no_scan", as: "update_no_scan"
          post :create_we_miss
          post :create_no_scan
        end
      end

      resources :task_definitions  do
        member do
          get :testing_fishbowl_miss_you
          post :post_testing_fishbowl_miss_you
          get :generate_job
          get "update_job/:job_id" => 'task_definitions#update_job', :as => 'update_job_admin'
        end
      end

      get 'no_scan_tasks' => "task_definitions#no_scan_tasks" ,:as => 'no_scan_task'
      member do
        get 'itunes_play_store_config'
        post 'post_itunes_play_store_config'
        get 'surveys'
        get 'restaurants'
        get 'edit_email_template/:key' => 'chains#edit_email_template', :as => 'edit_email_template'
        get "set_take_action_setting_chain"
        delete 'delete_email_template/:key' => 'chains#delete_email_template', :as => 'delete_email_template'
        post 'create_email_template' => 'chains#create_email_template', :as => 'create_email_template'
        post 'add_email_template' => 'chains#add_email_template', :as => 'add_email_template'
        #get 'new_email_template' => 'chains#new_email_template', :as => 'new_email_template'
        get 'change_status/:status' => 'chains#change_status', :as => :change_status

        get 'new_dashboard_signature' => 'chains#new_dashboard_signature', :as => 'new_dashboard_signature'
        get 'dashboard_setting' => 'chains#dashboard_setting', :as => 'dashboard_setting'
        patch 'create_dashboard_signature' => 'chains#create_dashboard_signature', :as => 'create_dashboard_signature'

        get 'new_warning_template' => 'chains#new_warning_template', :as => 'new_warning_template'
        get 'new_reward_notif' => 'chains#new_reward_notif', :as => 'new_reward_notif'
        get 'generate_warning_template' => 'chains#generate_warning_template', :as => 'generate_warning_template'
        get 'generate_reward_notif' => 'chains#generate_reward_notif', :as => 'generate_reward_notif'
        post 'create_warning_template' => 'chains#create_warning_template', :as => 'create_warning_template'
        delete 'delete_warning_template/:key' => 'chains#delete_warning_template', :as => 'delete_warning_template'
        delete 'delete_reward_notif/:key' => 'chains#delete_reward_notif', :as => 'delete_reward_notif'
        get 'edit_warning_template/:key' => 'chains#edit_warning_template', :as => 'edit_warning_template'
        get 'edit_reward_notif/:key' => 'chains#edit_reward_notif', :as => 'edit_reward_notif'
        post 'add_warning_template' => 'chains#add_warning_template', :as => 'add_warning_template'
        post 'add_reward_notif' => 'chains#add_reward_notif', :as => 'add_reward_notif'
        post 'update_warning_template' => 'chains#update_warning_template', :as => 'update_warning_template'
        get 'new_reward_notification'
        get 'new_birthday_reward_notification'
        get 'new_referral_notification'
        get 'new_gift_notification'        
        patch 'create_referral_notification'
        patch 'create_gift_notification'
        get "daily_report"
        get "weekly_report"
        get 'new_point_notification'
        patch 'create_point_notification'
        get 'new_milestone_notification'
        patch 'create_reward_notification'
        patch 'create_birthday_reward_notification'
        get 'new_email_parse'
        patch 'create_email_parse'
        patch 'create_milestone_notification'
        get 'new_account_notification'
        patch 'create_account_notification'
        get 'new_reward_share_text'
        patch 'create_reward_share_text'
        get 'new_survey_response_alert'
        patch 'create_survey_response_alert'
        get 'new_game_notification'
        patch 'create_game_notification'
        get 'new_reset_email_notification'
        patch 'create_reset_email_notification'
        get 'new_steppingstone_notification'
        patch 'create_steppingstone_notification'
        get 'rewards_pos_codes'
        get 'new_game_point_notification'
        patch 'create_game_point_notification'
        get 'new_admin_push_reward_notification'
        patch 'create_admin_push_reward_notification'
        get 'new_admin_push_point_notification'
        patch 'create_admin_push_point_notification'
        get 'new_admin_rounding_setting'
        patch 'create_admin_rounding_setting'
        get "new_mailchimp_setting"
        patch "create_mailchimp_setting"
        get "new_emma_setting"
        post "create_emma_setting"
        put "create_emma_setting"
        get "sync_now"
        get "sync_now_emma"
        get 'new_launch_text'
        patch 'create_new_launch_text'
        patch 'create_api_response_text'
        patch 'create_faq'
        get "fishbowl_sync_now"
        get "new_survey_incentive_threshold"
        patch "create_survey_incentive_threshold"
        get 'new_reward_expiration_notification'
        patch 'create_reward_expiration_notification'
        get 'new_active_reward_notification'
        patch 'create_active_reward_notification'
        get "new_giftcard_notification_email"
        patch "create_giftcard_notification_email"
        get "revel_setting"

        # momentfeed routes
        get "new_momentfeed_setting"
        post "create_momentfeed_setting"
        put "create_momentfeed_setting"
        get "manual_sync_momentfeed_settings"

        ## dashboard 2.3
        get "new_guest_relation_setting"
        get "update_guest_relation_setting"
        get 'load_survey_question'
        get 'load_survey_rotating_question'
        get 'new_avg_survey_setting'
        post 'avg_survey_setting'
        get 'set_considered_question/:type/:value' => "chains#set_considered_question"
        get 'set_considered_rotating_question/:type/:value' => "chains#set_considered_rotating_question"

        post 'post_push_reward'
        get 'new_push_reward'
        get 'reward_timer_setting'

        #FISHBOWL
        get 'new_fishbowl_setting'
        patch 'create_fishbowl_setting'
        get 'remove_fishbowl_setting'
        get "fishbowl_initial_sync"
        get "new_pay_code_setting"
        patch "create_pay_code_setting"
        get "new_user_code_setting"
        patch "create_user_code_setting"
        patch "new_receipt_code_setting"
        patch "create_receipt_code_setting"
        get "new_reward_code_setting"
        patch "create_reward_code_setting"
        get "new_gift_code_setting"
        patch "create_gift_code_setting"

        get "new_btree_setting"
        patch "create_btree_setting"
        get "olo_setting"
        patch "olo_update"
        get "new_netpos_setting"
        patch "create_netpos_setting"
        get "hybrid_reward_redemption"
        patch "update_hybrid_reward_redemption"
        get "location_update_json"
        patch "save_location_json"
        get "manual_sync_restaurant_json"
      end
      collection do
        get 'deactivated' => 'chains#deactivated', :as => :deactivated
        get "import_chain"
        post "import_preview"
        post "create_import"
        post "search_chain"
        get "region_report"
        get "grouping_region_report"
        post "post_grouping_region_report"
        post "post_region_report"
        get "get_file_region"
        post "region_match"
        post "grouping_region_match"
        # report LPQ
        get "user_email_list"
        post "post_user_email_list"

        get "survey_check_report"
        post "pos_survey_check_report"

        get "reward_count_list"
        post "post_reward_count_list"

        get "lapsed_list"
        post "post_lapsed_list"

        get "geodata_report"
        post "post_geodata_report"
        # end report LPQ
      end

      resources :dashboard_questions do
        resources :dashboard_choices
      end
      resources :promotion_locales
      resources :chain_urls
      resources :image_text_shares
      resources :steppingstones do
        resources :steps do
          ## points earned steps
          collection do
            get :new_points_earned_step
            post :create_step_points_earned
          end
          member do
            get :add_one_time_reward, :format => :js
            get :add_reward
            get :add_point_reward
            get :add_point
            put :set_point
            get :remove_reward
            get :remove_one_time_reward, :format => :js
            get :remove_point
            get 'add_milestone_reward/:reward_id' => "steps#add_milestone_reward", :as => :add_milestone_reward
            get 'add_the_one_reward/:reward_id' => "steps#add_the_one_reward", :as => :add_the_one_reward, :format => :js
            get "edit_milestone_point" => "steps#edit_milestone_point", :as => :edit_milestone_point
            put 'update_milestone_point' => "steps#update_milestone_point", :as => :update_milestone_point
            get :new_milestone_reward
            get :new_one_time_reward
            post :create_milestone_reward
            post :create_one_time_reward
          end
        end
      end

      resources :bucket_paths do
        scope ":type" do
          resources :steps do
            member do
              get :add_reward
              get :remove_reward
              get 'add_milestone_reward/:reward_id' => "steps#add_milestone_reward", :as => :add_milestone_reward
              get :new_milestone_reward
              post :create_milestone_reward
            end
          end
        end
      end

      resources :api_response_texts do
        member do
          get "show_form/:type" => "api_response_texts#show_form", :as => :show_form
          post "update_content" => "api_response_texts#update_content", :as => :update_content
          get "set_to_default_message/:message_for/:locale" => "api_response_texts#set_to_default_message", :as => :set_to_default_message
        end
      end

      resource :faqs do
        member do
          get "edit/:id/" => "faqs#edit", :as => :edit
          get "preview/:id" => "faqs#preview", :as => :preview
          get "destroy/:id" => "faqs#destroy", :as => :destroy
          patch "/update/:id" => "faqs#update", :as => :update
          get "generate_url/:id" => "faqs#generate_url", :as => :generate_url
        end
      end

      resource :faqimages do
        member do
          get "edit/:id/" => "faqimages#edit", :as => :edit
          get "destroy/:id" => "faqimages#destroy", :as => :destroy
          patch "/update/:id" => "faqimages#update", :as => :update
        end
      end

      scope ":content_type" do
        resources :text_tile_contents
      end

      resources :email_templates do
        mount MailPreview => 'mail_view'
      end

      resources :user_notifications

    end

    resources :terms do
      collection do
        match "/preview", :to => "terms#preview", :via => :get
      end
    end

    resources :privacy_policies do
      collection do
        match "/preview", :to => "privacy_policies#preview", :via => :get
      end
    end

    resources :olorewards_settings do
      collection do
        match '/search', :to => "olorewards_settings#search", :via => :any
      end
      member do
        get 'edit_aoo_cloud_connect_credential'
        post 'post_aoo_cloud_connect_credential'
      end
    end

    resources :nuorder_connect_settings do
      collection do
        match '/search',:to => "nuorder_connect_settings#search", :via => :any
      end
      member do
        get 'edit_nuorder_connect_setting'
        post 'post_nuorder_connect_setting'
      end
    end

    resources :olo_connect_settings do
      collection do
        match '/search',:to => "olo_connect_settings#search", :via => :any
      end
      member do
        get 'edit_aoo_cloud_connect_credential'
        post 'post_aoo_cloud_connect_credential'
      end
    end

    resources :onosys_connect_settings do
      collection do
        match '/search',:to => "onosys_connect_settings#search", :via => :any
      end
      member do
        get 'edit_onosys_connect_setting'
        post 'post_onosys_connect_setting'
      end
    end

    resources :questions do
      member do
        post 'edit'
        get 'question_with_type'
      end
    end

    resources :users do
      collection do
        get 'search'
        get 'deleted' => "users#deleted", :as => "deleted"
      end
      member do
        get 'revive' => 'users#revive', :as => :revive
        get "show_rewards" => "users#show_rewards", :as => :show_reward
        get "show_perks" => "users#show_perks", :as => :show_perks
        get "available_rewards" => "users#available_rewards", :as => :available_rewards
        get "earned_deal_points" => "users#earned_deal_points", :as => :earned_deal_points
        get "offline_deal_reward" => "users#offline_deal_reward", :as => :offline_deal_reward
        post "activate_deactivate" => "users#activate_deactivate", :as => :activate_deactivate
        get "deal_participation" => "users#deal_participation", :as => :deal_participation
        post "update_wallet_status" => "users#update_wallet_status", :as => :update_wallet_status
        post "update_reward_status" => "users#update_reward_status", :as => :update_reward_status
        get "receipts"
        get "giftcards"
        get "receipts_online"
        get "point_histories"
        get "gift_card_redeem_histories"

        post "update_notification_email"
        post "update_notification_device"
        post "update_marketing_optin"

        get 'game_activity'
        scope(:path => '/last_active') do
          get "miss_you_date"
        end
        put "update_miss_you"
        put "update_signup"
        get 'show_accosiated_devices'
        get 'payments'
        get 'nuorder'
      end

      resources :reward_transactions, :only => :index
    end

    resources :deals do
      collection do
        get 'deal_by_chain/:chain_id' => 'deals#deal_by_chain', :as => :deal_by_chain
        get 'new_deal/:chain_id/:form_type' => 'deals#new_deal', :as => :new_deal
        post 'create_activity/:chain_id' => 'deals#create_activity', :as => :create_activity
        get 'deactivated' => 'deals#deactivated', :as => :deactivated
      end

      member do
        get :pick_incentive
        get :assign_incentive
        put 'update_incentive' => 'deals#update_incentive', :as => :update_incentive
        get 'edit_incentive' => 'deals#edit_incentive', :as => :edit_incentive
        post 'create_reward_incentive' => 'deals#create_reward_incentive', :as => :create_reward_incentive
        post 'create_offer_incentive' => 'deals#create_offer_incentive', :as => :create_offer_incentive
        post 'create_offline_incentive' => 'deals#create_offline_incentive', :as => :create_offline_incentive
        put 'update_activity/:chain_id' => 'deals#update_activity', :as => :update_activity
        get 'revive' => "deals#revive", :as => :revive
        get 'show_participants' => 'deals#show_participants', :as => :show_participants
        get :load_ontime_reward
        get :load_points
        get :load_offline
        get 'assign_with_existing_reward/:reward_id' => 'deals#assign_with_existing_reward', :as => :assign_with_existing_reward
        get 'assign_with_existing_deal_offer/:deal_offer_id' => 'deals#assign_with_existing_deal_offer', :as => :assign_with_existing_deal_offer
        get 'remove_incentive/:incentive_id' => 'deals#remove_incentive', :as => :remove_incentive
      end
    end

    #goal and categories stuff
    resources :goal_categories do
      member do
        get 'categories_by_chain'
        get 'deactivate'
        get 'activate'
      end
    end

    resources :goals do
      member do
        get 'goals_by_chain'
        get 'pick_incentive'
        get 'assign_incentive'
        post 'create_reward_incentive' => 'goals#create_reward_incentive', :as => :create_reward_incentive
        post 'create_offer_incentive' => 'goals#create_offer_incentive', :as => :create_offer_incentive

        put 'update_incentive' => 'goals#update_incentive', :as => :update_incentive
        get 'edit_incentive' => 'goals#edit_incentive', :as => :edit_incentive

        get 'deactivate'
        get 'activate'
        get 'remove_incentive/:incentive_id' => 'goals#remove_incentive', :as => :remove_incentive
        get 'add_message_incentive'
        patch 'create_message_incentive'
        get 'load_ontime_reward'
        get 'load_points'
        get 'assign_with_existing_reward/:reward_id' => 'goals#assign_with_existing_reward', :as => :assign_with_existing_reward
        get 'assign_with_existing_point/:point_id' => 'goals#assign_with_existing_point', :as => :assign_with_existing_point
      end
    end

    resources :user_goal_categories do
      member do
        get 'goal_categories_by_chain'
        get 'show_goals'
      end
    end

    resources :rotating_groups do
      member do
        get "search_by_chain"
      end

      resources :rotating_questions do
        member do
          get :new_choices
          put :save_choices
          get 'question_with_type'
          get 'remove'
        end
      end
    end

    resources :social_shares do
      collection do
        post :group_chain_list
      end

      member do
        resources :social_share_participants
        get :pick_incentive, :format => :js
        get :assign_incentive , :format => :js
        get :load_onetime_reward, :format => :js
        get :load_points, :format => :js

        get 'assign_with_existing_reward/:reward_id' => 'social_shares#assign_with_existing_reward', :as => :assign_with_existing_reward
        post 'create_reward_incentive' => 'social_shares#create_reward_incentive', :as => :create_reward_incentive
        post 'create_offer_incentive' => 'social_shares#create_offer_incentive', :as => :create_offer_incentive
        get 'remove_incentive/:incentive_id' => 'social_shares#remove_incentive', :as => :remove_incentive
        put 'update_incentive' => 'social_shares#update_incentive', :as => :update_incentive
        get 'edit_incentive' => 'social_shares#edit_incentive', :as => :edit_incentive
        get "activate" => 'social_shares#activate', :as => :activate
      end
    end

    resources :games do
      resources :levels
    end
    resources :tags
    resources :system_tags

    resources :vantiv_credentials do
      collection do
        match '/search',:to => "vantiv_credentials#search", :via => :any
      end
      member do
        get 'edit_vantiv_credential'
        post 'post_vantiv_credential'
      end
    end

    resources :vantiv_gift_cards do
      collection do
        match '/search',:to => "vantiv_gift_cards#search", :via => :any
        delete :unlink_data
      end
      member do
        get 'edit_vantiv_gift_card'
        post 'post_vantiv_gift_card'
      end
    end

    resources :vantiv_transaction_logs do
      collection do
        match '/search',:to => "vantiv_transaction_logs#search", :via => :any
        match '/search_query/:chain_id',:to => "vantiv_transaction_logs#search_query", :via => :post, as: :search_query
      end
    end

    resources :mailchimp_logs do
      member do
        get :success_users
        get :failed_users
      end

      collection do
        get :filter_log
      end
    end

    #fishbowl
    resources :fishbowl_logs do
      member do
        get :success_users
        get :failed_users
      end

      collection do
        get :filter_log
      end
    end

    resources :emma_logs do
      member do
        get :success_users
        get :failed_users
      end

      collection do
        get :filter_log
      end
    end

    resources :revel_logs do
      member do
        get :success_users
        get :failed_users
      end

      collection do
        get :filter_log
        get :manual_sync
        post :do_manual_sync
      end
    end

    resources :receipt_line_items

    #POS INTEGRATION STUFF ADMIN PAGE ROUTING-------------------
    resources :general_menu_items do
      member do
        get :rewards
        get :add_reward
        get :menu_items
        get 'remove_menu_item/:menu_item_id' => "general_menu_items#remove_menu_item", :as => :remove_menu_item
        get 'add_menu_item/:menu_item_id' => "general_menu_items#add_menu_item", :as => :add_menu_item
        get 'remove_olo_menu_item/:menu_item_id' => "general_menu_items#remove_menu_item", :as => :remove_olo_menu_item
        get 'add_olo_menu_item/:menu_item_id' => "general_menu_items#add_olo_menu_item", :as => :add_olo_menu_item
        get "assign_reward/:reward_id" => "general_menu_items#assign_reward", :as => :assign_reward
        get "remove_reward/:reward_id" => "general_menu_items#remove_reward", :as => :remove_reward
        get 'add_range_start_end' => "general_menu_items#add_range_start_end", :as => :add_range_start_end
        get 'olo_menu_items' => "general_menu_items#olo_menu_items", :as => :olo_menu_items
        put 'submit_range_start_end' => "general_menu_items#submit_range_start_end", :as => :submit_range_start_end
      end
    end

    resources :pos_menu_items do
      collection do
        post :load_restaurants
        get :step_2
      end
    end

    resources :pos_menu_uploads, :pos_check_uploads, :pos_reward_checks, :focus_check_uploads, :focus_reward_checks do
      collection do
        post :load_restaurants
        get :search
      end
    end
    resources :pos_locations do
      collection do
        get :new_group_of_pos
        post :create_group
        post :load_restaurants
      end
      member do
        get 'chain_restaurants'
      end
    end

    resources :pos_receipts
    resources :olo_receipts do
      collection do
        get 'search'
      end
    end
    resources :online_orders do
      collection do
        get 'search'
      end
    end

    resources :barcode_batchs
    resources :receipt_barcodes do
      resources :receipt_transactions
      collection do
        get 'search'
        get 'export_status'
        get 'list'
        get 'new_search'
      end
      member do
        get 'url'
        get 'menu_receipt'
        get 'goto'
      end
    end
    #END POS INTEGRATION STUFF ADMIN PAGE ROUTING --------

    resources :menu_item_reports do
      member do
        get :load_user_list
      end
    end

    resources :relevant_auth_apis do
      member do
        get "inactive"
        get "active"
      end
    end

    resources :stream_galleries do
      collection do
        post "search"
        post 'upload_file_embed'
      end
      member do
        get 'add_xml_feed_link'
        post 'create_xml_feed_link'
        get 'edit_xml_embed'
        get 'remove_xml_embed'
        put 'update_xml_embed'

        get 'add_image_upload'
        post 'create_image_upload'
        get 'edit_image_upload'
        get 'remove_image_upload'
        put 'update_image_upload'
        get 'reload_image_upload'
      end
    end

    resources :raffles do
      collection do
        post "search"
        post 'activate_deactivate'
      end
      member do
        get 'additional_informations'
        get 'add_additional_info'
        post 'create_additional_info'
        get 'edit_additional_info'
        delete 'remove_additional_info'
        put 'update_additional_info'

        get 'steps'
        get 'new_step'
        post 'create_step'
        get 'edit_step'
        put 'update_step'
        delete 'remove_step'
        post 'activate_deactivate_step'

        get "participation_histories"
        get "step_histories"
        get 'edit_step_users'
        put 'update_step_users'
        get 'admin_update'
      end
    end

    #XPIENT CHECKS
    resources :xpient_checks do
      collection do
        post :load_restaurants
      end
    end

    #onosys admin page
    namespace :onosys do
      resources :online_orders do
        collection do
          get 'search'
        end
      end
    end

    #nuorder admin page
    namespace :vext do
      resources :online_orders do
        collection do
          get 'search'
        end
      end
    end

    get "chain_share_images" => 'chains#chain_share_images', :as => :chain_share_images
    resources :external_settings do
      collection do
        match '/search', :to => "external_settings#search", :via => :any
      end
    end
    # end
  end
  resources :rewards, :offers, :restaurants, :owners, :search, :surveys, :questions, :choices, :applications, :translations

  namespace :user do
    resources :rewards, :receipts, :surveys, :users
  end

  match "/report_notifications/" => "admin/chains#report_notifications", :via => :get, :format => :js
  match "/update_hours_delay_chain" => 'admin/chains#update_hours' ,:via => :post
  get 'admin/test_api' => 'admin/home#test_api', :as =>  :admin_test_api
  get 'admin/test_nuorder' => 'admin/home#test_nuorder', :as =>  :admin_test_nuorder_api
  get 'admin/test_vext' => 'admin/home#test_vext', :as =>  :admin_test_vext_api
  get 'admin/test_onosys' => 'admin/home#test_onosys', :as =>  :admin_test_onosys_api
  get 'admin' => 'admin/home#index'
  get 'admin/home' => 'admin/home#index'
  match '/admin/load_restaurants' => 'admin/home#load_restaurants', :as =>  :admin_load_restaurants, via: [:get, :post]
  match '/admin/load_offers' => 'admin/home#load_offers', :as =>  :admin_load_offers, via: [:get, :post]
  match '/admin/load_rewards' => 'admin/home#load_rewards', :as =>  :admin_load_rewards, via: [:get, :post]
  match '/admin/search/:action' => 'admin/search#:action', via: [:get, :post]
  match '/admin/searchs/:action' => 'admin/search#:action', via: [:get, :post]
  #match '/check_custom_image_uploader' => 'admin/recelocalipts#check_custom_image_uploader'
  match '/admin/update_offer_participation' => 'admin/offers#update_participation', via: [:get, :post]
  match '/admin/update_online_offer_participation' => 'admin/online_offers#update_participation', via: [:get, :post]
  match '/admin/update_deal_participation/:id' => 'admin/deals#update_participation', via: [:get, :post]
  match '/admin/update_reward_availability' => 'admin/rewards#update_participation', via: [:get, :post]
  match '/admin/timeout_traces' => "admin/timeout_errors#index", via: [:get, :post]

  match '/admin/receipts/reject_reasons/:id' => 'admin/receipts#reject_reasons', via: [:get, :post]
  match '/admin/reject_reasons/:id' => 'admin/receipts#reject_reasons', via: [:get, :post]

  match '/admin/receipts/offer_restaurants/:id' => 'admin/offers#getOfferRestaurants', via: [:get, :post]
  match '/admin/offer_restaurants/:id' => 'admin/offers#getOfferRestaurants', via: [:get, :post]
  match '/admin/chain_offers/:id' => 'admin/offers#chain_offers', via: [:get, :post]
  match '/admin/chain_users/:id' => 'admin/users#chain_users', via: [:get, :post]
  match '/admin/chain_appkeys/:id' => 'admin/users#chain_appkeys', via: [:get, :post]

  match '/admin/regions_by_country/:id' => 'admin/cities#getCountryRegions', via: [:get, :post]

  match '/admin/manage_one_time_rewards' => 'admin/rewards#one_time_rewards', via: [:get, :post]
  match '/admin/manage_milestone_rewards' => 'admin/rewards#milestone_rewards', via: [:get, :post]
  match '/admin/save_chain_reward_event' => 'admin/rewards#save_chain_reward_event', :via => :post

  #API AJAX

  ##test api page for partner
  match "/api/test/partner_categories" => "api/v1/tests#partner_categories", :via => :get
  match "/api/test/partner_rewards" => "api/v1/tests#partner_rewards", :via => :get
  match "/api/test/get_partner_rewards" => "api/v1/tests#get_partner_rewards", :via => :get

## test api
#post 'email_processor' => 'griddler#create'
mount_griddler
match "/api/test/reward_list" => "api/v1/tests#reward_list", :via => :get
match "/api/test/test_timeout" => "api/v1/tests#test_timeout", :via => :get
match "/api/test/olo_list_summaries" => "api/v1/tests#olo_list_summaries", :via => :get
match "/api/test/olo_orders" => "api/v1/tests#olo_orderexports", :via => :get
match "/api/test/olo_export" => "api/v1/tests#olo_export", :via => :get
match "/api/test/get_olo_order_batch" => "api/v1/tests#get_olo_order_batch", :via => :get
match "/api/test/get_olo_orders" => "api/v1/tests#get_olo_orderexports", :via => :get
match "/api/test/get_olo_list_summaries" => "api/v1/tests#get_olo_list_summaries", :via => :get
match "/api/test/geo_data_setting" => "api/v1/tests#geo_data_testing", :via => :get
match "/api/test/social_shares" => "api/v1/tests#social_shares", :via => :get
match "/api/test/post_social_shares" => "api/v1/tests#post_social_shares", :via => :get
match "/api/test/test_fb_reactivate_account" => "api/v1/tests#test_fb_reactivate_account", :via => :get
match "/api/test/sign_up_with_referral" => "api/v1/tests#sign_up_with_referral", :via => :get
match "/api/test/referral" => "api/v1/tests#referral", :via => :get
match "/api/test/referral_email" => "api/v1/tests#referral_email", :via => :get
match "/api/test/chain_image_share" => "api/v1/tests#chain_image_share", :via => :get
match "/api/test/form_chain_image" => "api/v1/tests#form_chain_image", :via => :get
match "/api/test/:survey_id/answer_surveys" => "api/v1/tests#test_answer_survey", :via => :get
match "/api/test/answer_deal_survey" => "api/v1/tests#test_answer_deal_survey", :via => :get
match "/api/test/post_answer_deal_survey" => "api/v1/tests#test_post_answer_deal_survey", :via => :post, :as => :test_post_answer_deal_survey
match "/api/test/get_deal_survey" => "api/v1/tests#test_get_deal_survey", :via => :get
match "/api/test/get_data_survey" => "api/v1/tests#test_post_deal_survey", :via => :post,:as => :test_post_deal_survey
match "/api/test/claim" => "api/v1/tests#test_claim", :via => :get
match "/api/test/locate" => "api/v1/tests#test_locate", :via => :get
match "/api/test/receipt" => "api/v1/tests#test_submit_receipt", :via => :get
match "/api/test/offer_restaurants/:id" => "api/v1/tests#getOfferRestaurants", :via => :get
match "/api/test/promocode", :to =>"api/v1/tests#test_promocode", :via => :get
get "api/test/forgot_password" => "api/v1/tests#forgot_password"
get "api/test/submit_receipt" => "api/v1/tests#submit_receipt"
get "api/test/update_password" => "api/v1/tests#update_password"
get "api/test/user_signin" => "api/v1/tests#user_signin"
get "api/test/test_list_deal" => "api/v1/tests#test_list_deals", :as => :test_list_deals
get "api/test/test_detail_deal" => "api/v1/tests#test_detail_deals", :as => :test_detail_deals
get "api/test/test_deal_location" => "api/v1/tests#test_deal_location", :as => :test_deal_location
get "api/test/test_post_checkin" => "api/v1/tests#test_post_checkin", :as => :test_post_checkin
match "/api/test/test_user_signup" => "api/v1/tests#test_user_signup", :as => 'test_signup', via: [:get, :post]
match "/api/test/claim_message" => "api/v1/tests#claim_message", :as => 'claim_message', via: [:get, :post]
match "/api/test/test_submit_survey" => "api/v1/tests#test_submit_survey", :as => 'test_submit_survey', via: [:get, :post]
match "/api/test/test_submit_survey_step2" => "api/v1/tests#test_submit_survey_step2", :as => 'test_submit_survey_step2', via: [:get, :post]
match "/api/test/test_post_submit_survey" => "api/v1/tests#test_post_submit_survey", :as => :test_post_submit_survey, via: [:get, :post]
get "api/test/test_get_categories" => "api/v1/tests#test_get_categories", :as => 'test_get_categories'
get "api/test/test_submit_categories" => "api/v1/tests#test_submit_categories", :as => 'test_submit_categories'
get "api/test/load_categories/:appkey" => "api/v1/tests#load_categories", :as => 'load_categories'
get "api/test/test_weekly_user_goal" => "api/v1/tests#test_weekly_user_goal", :as => 'test_weekly_user_goal'
get "api/test/test_complete_goal" => "api/v1/tests#test_complete_goal", :as => 'test_complete_goal'
get "api/test/load_user_goals/:auth_token" => "api/v1/tests#load_user_goals", :as => 'load_user_goals'
get "api/test/test_deadline" => "api/v1/tests#test_deadline", :as => 'test_deadline'
get "api/test/test_history" => "api/v1/tests#test_history", :as => 'test_history'
get "api/test/test_reward_activity" => "api/v1/tests#test_reward_activity", :as => 'test_reward_activity'
get "api/test/test_deactivate_account" => "api/v1/tests#test_deactivate_account", :as => 'test_deactivate_account'
get "api/test/test_reactivate_account" => "api/v1/tests#test_reactivate_account", :as => 'test_reactivate_account'
get "api/test/test_confirm_reactivate_account" => "api/v1/tests#test_confirm_reactivate_account", :as => 'test_confirm_reactivate_account'




get "api/test/test_claim_reward" => "api/v1/tests#test_claim_reward", :as => 'test_claim_reward'
get "api/test/test_claim_reward_get_info" => "api/v1/tests#test_claim_reward_get_info", :as => 'test_claim_reward_get_info'
get "api/test/load_rewards" => "api/v1/tests#load_rewards", :as => 'test_load_rewards'
get "api/test/load_restaurants_and_users" => "api/v1/tests#load_restaurants_and_users", :as => 'test_load_restaurants_and_users'
get "api/test/test_share_text_list" => "api/v1/tests#test_share_text_list", :as => 'test_share_text_list'
get "api/test/load_users" => "api/v1/tests#load_users", :as => 'load_users'
get "api/test/load_raffle_additional_info" => "api/v1/tests#load_raffle_additional_info", :as => 'load_raffle_additional_info'
get "api/test/load_giftable_rewards" => "api/v1/tests#load_giftable_rewards", :as => 'load_giftable_rewards'
get "api/test/load_survey" => "api/v1/tests#load_survey", :as => 'load_survey'
get "api/test/test_active_reward" => "api/v1/tests#test_active_reward", :as => "test_active_reward"
get "api/test/test_module_accessed" => "api/v1/tests#test_module_accessed", :as => "test_module_accessed"
get "api/test/test_facebook_data" => "api/v1/tests#test_facebook_data", :as => "test_facebook_data"
get "api/test/test_user_update_profile" => "api/v1/tests#test_user_update_profile", :as => "test_user_update_profile"
get "api/test/test_birthday_reward" => "api/v1/tests#test_birthday_reward", :as => "test_birthday_reward"
post "api/test/post_birthday_reward" => "api/v1/tests#post_birthday_reward", :as => "post_birthday_reward"
get "api/test/test_user_get_oauth_token" => "api/v1/tests#test_user_get_oauth_token", :as => "test_user_get_oauth_token"
get "api/test/test_get_olo_mobile_uri" => "api/v1/tests#test_get_olo_mobile_uri", :as => "test_get_olo_mobile_uri"

#gifting reward api test
get "api/test/test_add_gift_to_user" => "api/v1/tests#test_add_gift_to_user", :as => "test_add_gift_to_user"
get "api/test/test_tap_to_gift" => "api/v1/tests#test_tap_to_gift", :as => "test_tap_to_gift"
get "api/test/test_giftcode" => "api/v1/tests#test_giftcode", :as => "test_giftcode"
get "api/test/test_game_points" => "api/v1/tests#test_game_points", :as => "test_game_points"
get "api/test/test_game_activity" => "api/v1/tests#test_game_activity", :as => "test_game_activity"
get "api/test/test_php_migration/:type" => "api/v1/tests#test_php_migration", :as => "test_php_migration"
get "api/test/test_user_activity" => "api/v1/tests#test_user_activity", :as => "test_user_activity"
get "api/test/test_user_profile" => "api/v1/tests#test_user_profile", :as => "test_user_profile"
get "api/test/test_launch_text" => "api/v1/tests#test_launch_text", :as =>  "test_launch_text"
get "api/test/test_generate_keychain_value" => "api/v1/tests#test_generate_keychain_value", :as =>  "test_generate_keychain_value"
get "api/test/test_braintree_client_token" => "api/v1/tests#test_braintree_client_token", :as =>  "test_braintree_client_token"

#TEST API PAGE FOR POS STUFF ----------
get "api/test/test_pos_reward" => "api/v1/tests#test_pos_reward", :as => 'test_pos_reward'
get "api/test/test_pos_check_upload_status" => "api/v1/tests#test_pos_check_upload_status", :as => 'test_pos_check_upload_status'
get "api/test/test_pos_check_upload" => "api/v1/tests#test_pos_check_upload", :as => 'test_pos_check_upload'
get "api/test/test_pos_menu_upload_status" => "api/v1/tests#test_pos_menu_upload_status", :as => 'test_pos_menu_upload_status'
get "api/test/test_pos_menu_upload" => "api/v1/tests#test_pos_menu_upload", :as => 'test_pos_menu_upload'
get "api/test/test_pos_receipt_upload" => "api/v1/tests#test_pos_receipt_upload", :as => 'test_pos_receipt_upload'
get "api/test/test_pos_receipt_barcode" => "api/v1/tests#test_pos_receipt_barcode", :as => 'test_pos_receipt_barcode'
get "api/test/test_submit_barcode" => "api/v1/tests#test_submit_barcode", :as => 'test_submit_barcode'
get "api/test/test_pos_send_receipt" => "api/v1/tests#test_pos_send_receipt", :as => 'test_pos_send_receipt'
get "api/test/test_get_user_code" => "api/v1/tests#test_get_user_code", :as => "test_get_user_code"
get "api/test/test_get_payment_code" => "api/v1/tests#test_get_payment_code", :as => "test_get_payment_code"
get "api/test/test_get_gift_code" => "api/v1/tests#test_get_gift_code", :as => "test_get_gift_code"
get "api/test/test_get_giftcard_skin" => "api/v1/tests#test_get_giftcard_skin", :as => "test_get_giftcard_skin"
get "api/test/test_submit_usercode" => "api/v1/tests#test_submit_usercode", :as => 'test_submit_usercode'
get "api/test/test_get_code_type" => "api/v1/tests#test_get_code_type", :as => 'test_get_code_type'
get "api/test/test_gallery_stream" => "api/v1/tests#test_gallery_stream", :as => 'test_gallery_stream'
get "api/test/test_raffle" => "api/v1/tests#test_raffle", :as => 'test_raffle'
get "api/test/test_raffle_submit_code" => "api/v1/tests#test_raffle_submit_code", :as => 'test_raffle_submit_code'
get "api/test/test_raffle_submit_info" => "api/v1/tests#test_raffle_submit_info", :as => 'test_raffle_submit_info'

get 'api/test/test_micros_check_detail' => "api/v1/tests#test_micros_check_detail", :as => :test_micros_check_detail
get 'api/test/test_pay_table_service' => "api/v1/tests#test_pay_table_service", :as => :test_pay_table_service
get 'api/test/test_pull_check_payment' => "api/v1/tests#test_pull_check_payment", :as => :test_pull_check_payment
get 'api/test/test_get_check' => "api/v1/tests#test_get_check_detail", :as => :test_get_check
get 'api/test/test_pull_payment_alert' => "api/v1/tests#test_pull_payment_alert", :as => :test_pull_payment_alert

#END API PAGE POS STUFF -------------

#test xpient pos
get "api/test/test_xpient_submit_order" => "api/v1/tests#test_xpient_submit_order", :as => 'test_xpient_submit_order'
#end-----

#pull surveys
get "api/test/test_pull_survey" => "api/v1/tests#test_pull_survey", :as => 'test_pull_survey'
get "api/test/test_skip_survey" => "api/v1/tests#test_skip_survey", :as => 'test_skip_survey'

get "api/test/test_get_restaurant_aoid" => "api/v1/tests#test_get_restaurant_aoid", :as => "test_get_restaurant_aoid"

#automatic test
post "api/test/automatic_test" => "api/v1/tests#automatic_test", format: "js"
# end --

#test pos LOyalty Treatware
#POS LOYALTY get info
get "api/test/test_tw_get_info" => "api/v1/tests#test_tw_get_info", :as => 'test_tw_get_info'
get "api/test/test_tw_apply_offers" => "api/v1/tests#test_tw_apply_offers", :as => 'test_tw_apply_offers'
get "api/test/test_tw_submit_orders" => "api/v1/tests#test_tw_submit_orders", :as => 'test_tw_submit_orders'
#end

#TEST VANTIV STUFF
get "api/test/test_vantiv_activate_card" => "api/v1/tests#test_vantiv_activate_card", :as => 'test_vantiv_activate_card'
get "api/test/test_vantiv_deposit_card" => "api/v1/tests#test_vantiv_deposit_card", :as => 'test_vantiv_deposit_card'
get "api/test/test_vantiv_get_card_balance" => "api/v1/tests#test_vantiv_get_card_balance", :as => 'test_vantiv_get_card_balance'
get "api/test/test_vantiv_gift" => "api/v1/tests#test_vantiv_gift", :as => 'test_vantiv_gift'
get "api/test/test_vantiv_card_unload" => "api/v1/tests#test_vantiv_card_unload", :as => 'test_vantiv_card_unload'
get "api/test/test_vantiv_api_call" => "api/v1/tests#test_vantiv_api_call", :as => 'test_vantiv_api_call'

#END VANTIV STUFF

#TEST OLO STUFF
get "api/test/test_olo_settings" => "api/v1/tests#test_olo_settings", :as => 'test_olo_settings'
#END OLO STUFF



devise_scope :user do
  get "api/v1/user/login", :to => "api/v1/sessions#create"
  post "api/v1/user/login", :to => "api/v1/sessions#create"

  get "api/v1/user/signup", :to => "api/v1/registrations#create"
  post "api/v1/user/signup", :to => "api/v1/registrations#create"
  get "api/v1/user/migrate", :to => "api/v1/registrations#migrate"

  get "api/v1/user/logout", :to => "api/v1/sessions#destroy"

  get "api/v1/user/deactivate" => "api/v1/sessions#deactivate_account", :as => :api_v1_deactivate_account
  get "api/v1/user/reactivate" => "api/v1/sessions#reactivate_account", :as => :api_v1_reactivate_account
  post "api/v1/user/validate_token", :to => "api/v1/sessions#validate_token"

end

namespace :api do
  namespace :v1 do
    #PARTNER
    get "category", :to => "partner#category"
    post "partner_reward", :to => "partner#partner_reward"
    get "partner_reward", :to => "partner#get_partner_reward"
    post "partner_reward_code", :to => "partner#partner_reward_code"
    post "bridg_webhook", :to => "partner#webhook"

    get "olo/settings", :to => "ololehos#settings"

    match 'version/latest' => 'application_keys#latest', via: [:get, :post]
    match 'version/last' => 'application_keys#last', via: [:get, :post]
    match 'checkin/locations' => 'deals#deal_locations', :as => :deal_location, via: [:get, :post]
    #post 'get_email_sendgrid' => 'webhook_sendgrid#index', :as => :webhook_sendgrid
    #post Gridhook.config.event_receive_path => 'api/v1/webhook_sendgrid#create'
    post 'checkin/submit' => 'deals#checkin_submit', :as => :post_checkin
    get 'checkin/submit' => 'deals#checkin_submit'
    match '/share/list' => 'image_shares#get_image_info', :as => :get_image_info, via: :get
    match '/referral/email' => 'referrals#email', :as => :referral_email, via: :get
    match '/referral/mail_to_referer' => 'referrals#mail_to_referer', :as => :mail_to_referer, via: :get
    match '/referral' => 'referrals#index', :as => :referral, via: :get
    match '/launch/text' => 'application_keys#launch_text', :as => "launch_text", :via  => :get
    match 'keychain/generate' => "user#generate_keychain", :as => :generate_keychain, via: :get
    get 'gallery_stream' => "application_keys#gallery_stream", :as => :gallery_stream
    match '/user/code' => "user#get_user_id", :as => :get_user_id, via: [:get, :post]
    match '/user/pay_code' => "user#pay_code", :as => :pay_code, via: [:get, :post]
    get 'user/giftcard_code' => "user#gift_code", :as => :gift_code
    get 'user/giftcard_skin' => "user#giftcard_skin", :as => :giftcard_skin
    post "analytics/module_accessed" => "user#module_accessed", :as => :module_accessed
    post "analytics/geodata" => "user#geodata", :as => :geodata
    get 'braintree/client_token' => "user#braintree_client_token", :as => "braintree_client_token"
    post "user/profile/update" => "user#update_profile", :as => "user_update_profile"
    post "pay/pos/table_service" => "receipts#pay_table_service", :as => :pay_table_service
    get "user/oauth_token" => "user#get_oauth_token", :as => :get_oauth_token
    get "user/get_olo_mobile_uri" => "user#get_olo_mobile_uri", :as => :get_olo_mobile_uri
    post "user/bt/remove_old_card" => "user#delete_old_bt_credit_card", :as => :delete_old_bt_credit_card

    #VANTIV
    post "giftcard/activate" => "gift_cards#activate", :as => :vantiv_activate_card
    post "giftcard/deposit" => "gift_cards#deposit", :as => :vantiv_deposit_card
    post "giftcard/balance" => "gift_cards#balance", :as => :vantiv_balance_card
    post "giftcard/unload" => "gift_cards#unload", :as => :vantiv_unload_card
    post "giftcard/gift" => "gift_cards#gift", :as => :vantiv_gift
    post "vantiv/test_call" => "gift_cards#test_call", :as => :vantiv_test_call

    post 'notification' => "user#notifications", :as => :user_notifications
    #END PUSH SERVICES API

    resources :receipts do
      collection do
        get "upload"
        match "code" => "receipts#upload", :as => :code, via: [:get, :post]
        get 'pos/code' => "receipts#check_detail", :as => :check_detail
      end
    end
    resources :rewards do
      collection do
        get 'locate'
        post 'claim'
        get 'activity'
        get "share"
        get "add_giftable_to_user"
        post "gift" => "rewards#tap_to_gift", :as => "tap_to_gift"
        get "active" => "rewards#active", :as => "active"
      end
    end
    resources :offers do
      collection do
        get 'nearby'
        get 'restaurants'
      end
    end

    resources :restaurants do
      collection do
        get 'nearby'
      end
    end

    resources :survey do
      member do
        #match 'answer' => "survey", via: :any
        match 'answer' , via: :get
      end
      collection do
        get 'getSurvey'
        get 'pull'
        post 'skip'
      end
    end

    resources :deals, :only => [] do
      get 'get_survey'
      post 'answer_survey'
      collection do
        get "detail" => "deals#deal_detail", :as => :detail_deal
        get '' => "deals#list_deals", :as => :list_deals
      end
    end
    #resources :cities
    resources :user do
      collection do
        post 'update_password'
        get 'activity'
        post 'forgot_password'
        get 'profile'
        post 'facebook_data'
        #post 'collect_geo_data'
      end
    end

    resources :social_shares do
      collection do
        post "user_interaction"
      end
    end

    resources :goals, :only => :index do
      collection do
        post 'complete'
        get 'deadline'
        get 'history'
      end
    end
    resources :categories, :only => :index do
      collection do
        post 'select'
        get 'select'
      end
    end

    resource :game do
      collection do
        post 'points'
        get 'activity'
      end
    end

    resource :raffle do
      collection do
        post "submit_code"
        post "submit_info"
      end
    end

  end

  namespace :v2 do
    #PARTNER
    get "category", :to => "partner#category"
    post "partner_reward", :to => "partner#partner_reward"
    get "partner_reward", :to => "partner#get_partner_reward"

    get "olo/settings", :to => "ololehos#settings"
    match 'version/latest' => 'application_keys#latest', via: [:get, :post]
    match 'version/last' => 'application_keys#last', via: [:get, :post]
    match 'checkin/locations' => 'deals#deal_locations', :as => :deal_location, via: [:get, :post]
    post 'checkin/submit' => 'deals#checkin_submit', :as => :post_checkin
    get 'checkin/submit' => 'deals#checkin_submit', via: [:get, :post]
    match '/share/list' => 'image_shares#get_image_info', :as => :get_image_info, via: :get
    get 'referral/email' => 'referrals#email', :as => :referral_email
    match '/referral/mail_to_referer' => 'referrals#mail_to_referer', :as => :mail_to_referer, via: :get
    match '/referral' => 'referrals#index', :as => :referral, via: :get
    match '/launch/text' => 'application_keys#launch_text', :as => "launch_text", :via => :get
    match 'keychain/generate' => "user#generate_keychain", :as => :generate_keychain, via: :get
    get 'gallery_stream' => "application_keys#gallery_stream", :as => :gallery_stream
    match '/user/code' => "user#get_user_id", :as => :get_user_id, via: [:get, :post]
    match '/user/pay_code' => "user#pay_code", :as => :pay_code, via: [:get, :post]
    get 'user/giftcard_code' => "user#gift_code", :as => :gift_code
    get 'user/giftcard_skin' => "user#giftcard_skin", :as => :giftcard_skin
    post "analytics/module_accessed" => "user#module_accessed", :as => :module_accessed
    post "analytics/geodata" => "user#geodata", :as => :geodata
    get 'braintree/client_token' => "user#braintree_client_token", :as => "braintree_client_token"
    post "user/profile/update" => "user#update_profile", :as => "user_update_profile"
    post "pay/pos/table_service" => "receipts#pay_table_service", :as => :pay_table_service
    get "user/oauth_token" => "user#get_oauth_token", :as => :get_oauth_token
    get "user/get_olo_mobile_uri" => "user#get_olo_mobile_uri", :as => :get_olo_mobile_uri
    post "user/bt/remove_old_card" => "user#delete_old_bt_credit_card", :as => :delete_old_bt_credit_card

    #VANTIV
    post "giftcard/activate" => "gift_cards#activate", :as => :vantiv_activate_card
    post "giftcard/deposit" => "gift_cards#deposit", :as => :vantiv_deposit_card
    get "giftcard/balance" => "gift_cards#balance", :as => :vantiv_balance_card
    post "giftcard/unload" => "gift_cards#unload", :as => :vantiv_unload_card
    post "giftcard/gift" => "gift_cards#gift", :as => :vantiv_gift
    post "vantiv/test_call" => "gift_cards#test_call", :as => :vantiv_test_call

    post 'notification' => "user#notifications", :as => :user_notifications
    #END PUSH SERVICES API

    resources :receipts do
      collection do
        get "upload"
        match "code" => "receipts#upload", :as => :code, via: [:get, :post]
        get 'pos/code' => "receipts#check_detail", :as => :check_detail
      end
    end
    resources :rewards do
      collection do
        get 'locate'
        post 'claim'
        get 'activity'
        get "share"
        get "add_giftable_to_user"
        post "gift" => "rewards#tap_to_gift", :as => "tap_to_gift"
        get "active" => "rewards#active", :as => "active"
      end
    end
    resources :offers do
      collection do
        get 'nearby'
        get 'restaurants'
      end
    end

    resources :restaurants do
      collection do
        get 'nearby'
      end
    end

    resources :survey do
      member do
        #match 'answer' => "survey", via: :any
        match 'answer' , via: :get
      end
      collection do
        get 'getSurvey'
        get 'pull'
        post 'skip'
      end
    end

    resources :deals, :only => [] do
      get 'get_survey'
      post 'answer_survey'
      collection do
        get "detail" => "deals#deal_detail", :as => :detail_deal
        get '' => "deals#list_deals", :as => :list_deals
      end
    end
    #resources :cities
    resources :user do
      collection do
        post 'update_password'
        get 'activity'
        post 'forgot_password'
        get 'profile'
        post 'facebook_data'
        #post 'collect_geo_data'
      end
    end

    resources :social_shares do
      collection do
        post "user_interaction"
      end
    end

    resources :goals, :only => :index do
      collection do
        post 'complete'
        get 'deadline'
        get 'history'
      end
    end
    resources :categories, :only => :index do
      collection do
        post 'select'
        get 'select'
      end
    end

    resource :game do
      collection do
        post 'points'
        get 'activity'
      end
    end

    resource :raffle do
      collection do
        post "submit_code"
        post "submit_info"
      end
    end

  end
end

scope "/api/v1" do
  scope "/receipts" do
    post "" => "receipts#create"
    get ":receipt_id" => "receipts#show"
    get ":receipt_id/transactions" => "receipts#transactions"
    put ":receipt_id" => "receipts#update"
  end

  scope "/promocode" do
    post "" ,:to =>"api/v1/promo_codes#create"
  end
end



devise_scope :user do
  get "api/v2/user/login", :to => "api/v2/sessions#create"
  post "api/v2/user/login", :to => "api/v2/sessions#create"

  post "api/v2/user/signup", :to => "api/v2/registrations#create"
  get "api/v2/user/migrate", :to => "api/v2/registrations#migrate"

  post "api/v2/user/logout", :to => "api/v2/sessions#destroy"

  get "api/v2/user/deactivate" => "api/v2/sessions#deactivate_account", :as => :api_v2_deactivate_account
  get "api/v2/user/reactivate" => "api/v2/sessions#reactivate_account", :as => :api_v2_reactivate_account
  post "api/v2/user/validate_token", :to => "api/v2/sessions#validate_token"
end



scope "/api/v2" do
  scope "/receipts" do
    post "" => "receipts#create"
    get ":receipt_id" => "receipts#show"
    get ":receipt_id/transactions" => "receipts#transactions"
    put ":receipt_id" => "receipts#update"
  end

  scope "/promocode" do
    post "" ,:to =>"api/v1/promo_codes#create"
  end
end


  #POS INTEGRATION STUFF API ROUTES
  namespace :pos do
    namespace :v1 do
      post "reward" => "rewards#process_code", :as => :pos_process_reward_code
      get "check/status" => "upload#check_status", :as => :pos_check_upload_status
      post "check/upload" => "upload#check_upload", :as => :pos_check_upload
      get "menu/status" => "upload#menu_status", :as => :pos_menu_upload_status
      post "menu/upload" => "upload#menu_upload", :as => :pos_menu_upload
      get "receipts/barcode" => "upload#barcode", :as => :pos_receipt_barcode
      get "check/barcode" => "upload#barcode", :as => :pos_check_barcode
      post "check/submit" => "upload#barcode_check_submit", :as => :pos_check_submit
      post "receipts/send" => "upload#send_receipt", :as => :pos_send_receipt
      match "check/usercode" => "upload#usercode", :as => :pos_usercode, via: [:get, :post]
      get "code/:code/type" => "upload#get_code_type", :as => :get_code_type
      get "check/detail" => "upload#get_check_detail", :as => :get_check_detail
      post "check/payment" => "upload#pull_check_payment", :as => :pull_check_payment
      get "mp/paycode/update/:apikey/code/:code" => "upload#update_mp_processed_paycode", :as => :update_mp_processed_paycode
      get "check/payments/:emp_num/:rvc" => "upload#pull_payment_alert", :as => :pull_payment_alert
      post 'check/do_pull_payment_alert' => "upload#do_pull_payment_alert", :as => :do_pull_payment_alert
      match "xpient/getInfo" => "xpient#get_info", :as => :get_info_xpient, via: [:get, :post]
      match "xpient/applyOffers" => "xpient#apply_offers", :as => :apply_offers_xpient, via: [:get, :post]
      match "xpient/submitOrder" => "xpient#submit_order", :as => :submit_order_xpient, via: [:get, :post]
      match "xpient/voidOrder" => "xpient#void_order", :as => :void_order_xpient, via: [:get, :post]

      #POS LOYALTY TREATWARE get info
      #post 'posLoyalty/getInfo' => "treatware#tw_get_info", :as => :tw_get_info
      get 'posLoyalty/getInfo' => "treatware#tw_get_info", :as => :tw_get_info
      post 'posLoyalty/applyOffers' => "treatware#tw_apply_offers", :as => :tw_apply_offers
      post 'posLoyalty/submitOrder' => "treatware#tw_submit_orders", :as => :tw_submit_orders
      delete 'posLoyalty/applyOffers' => "treatware#tw_void_offers", :as => :tw_void_offers

      resources :net_pos , :path => 'posLoyalty/netpos' do

      end

      #FOCUS POS
      get "focus/GetProgramDescription" => "focus#get_program_description"
      get "focus/GetStatus" => "focus#get_status"
      get "focus/AddCredits" => "focus#add_credits"
      get "focus/GetCoupon" => "focus#get_coupon"
      post "focus/CloseTransactionWithSKU" => "focus#close_transaction_with_sku"
      post "focus/RedeemCoupon" => "focus#redeem_coupon"


    end
  end

  #END POS INTEGRATION STUFF API ROUTES

  #POS INTEGRATION STUFF API ROUTES V2
  namespace :pos do
    namespace :v2 do
      post "reward" => "rewards#process_code", :as => :pos_process_reward_code
      get "check/status" => "upload#check_status", :as => :pos_check_upload_status
      post "check/upload" => "upload#check_upload", :as => :pos_check_upload
      get "menu/status" => "upload#menu_status", :as => :pos_menu_upload_status
      post "menu/upload" => "upload#menu_upload", :as => :pos_menu_upload
      get "receipts/barcode" => "upload#barcode", :as => :pos_receipt_barcode
      get "check/barcode" => "upload#barcode", :as => :pos_check_barcode
      post "check/submit" => "upload#barcode_check_submit", :as => :pos_check_submit
      post "receipts/send" => "upload#send_receipt", :as => :pos_send_receipt
      match "check/usercode" => "upload#usercode", :as => :pos_usercode, via: [:get, :post]
      get "code/:code/type" => "upload#get_code_type", :as => :get_code_type
      get "check/detail" => "upload#get_check_detail", :as => :get_check_detail
      post "check/payment" => "upload#pull_check_payment", :as => :pull_check_payment
      get "mp/paycode/update/:apikey/code/:code" => "upload#update_mp_processed_paycode", :as => :update_mp_processed_paycode
      get "check/payments/:emp_num/:rvc" => "upload#pull_payment_alert", :as => :pull_payment_alert
      post 'check/do_pull_payment_alert' => "upload#do_pull_payment_alert", :as => :do_pull_payment_alert
      match "xpient/getInfo" => "xpient#get_info", :as => :get_info_xpient, via: [:get, :post]
      match "xpient/applyOffers" => "xpient#apply_offers", :as => :apply_offers_xpient, via: [:get, :post]
      match "xpient/submitOrder" => "xpient#submit_order", :as => :submit_order_xpient, via: [:get, :post]
      match "xpient/voidOrder" => "xpient#void_order", :as => :void_order_xpient, via: [:get, :post]

      #POS LOYALTY TREATWARE get info
      #post 'posLoyalty/getInfo' => "treatware#tw_get_info", :as => :tw_get_info
      get 'posLoyalty/getInfo' => "treatware#tw_get_info", :as => :tw_get_info
      post 'posLoyalty/applyOffers' => "treatware#tw_apply_offers", :as => :tw_apply_offers
      post 'posLoyalty/submitOrder' => "treatware#tw_submit_orders", :as => :tw_submit_orders
      delete 'posLoyalty/applyOffers' => "treatware#tw_void_offers", :as => :tw_void_offers

      resources :net_pos , :path => 'posLoyalty/netpos' do

      end

      #FOCUS POS
      get "focus/GetProgramDescription" => "focus#get_program_description"
      get "focus/GetStatus" => "focus#get_status"
      get "focus/AddCredits" => "focus#add_credits"
      get "focus/GetCoupon" => "focus#get_coupon"
      post "focus/CloseTransactionWithSKU" => "focus#close_transaction_with_sku"
      post "focus/RedeemCoupon" => "focus#redeem_coupon"
    end
  end
  #END POS INTEGRATION STUFF API ROUTES V2


  #IFRAME Mobile App Connect To Tokenex
  get 'page/:chain_id/tokenex_htp' => "page#tokenex_htp"
  post 'page/:chain_id/submit_tokenex_htp/:appkey/:auth_token' => "page#submit_tokenex_htp"
  #end IFRAME
  #
  #IFRAME Mobile App Contact Page
  get 'page/contact/:appkey' => "page#contact"
  post 'page/contact_submit/:appkey' => "page#contact_submit"
  # config/routes.rb

  #NuOrder INTEGRATION STUFF API ROUTES
  namespace :api do
    namespace :vnuor do
      get "connect/setting" => "user#connect_setting", :as => :connect_setting
      post "user/onlineorder/customer_connect" => "user#customer_connect", :as => :customer_connect
      get "user/profile" => "user#get_profile", :as => :get_profile
      get "user/onlineorder/search_profile" => "user#search_profile", :as => :search_profile
      post "user/forgot_password" => "user#forgot_password", :as => :forgot_password
      post "user/update_password" => "user#update_password", :as => :update_password
      post "user/profile" => "user#profile", :as => :profile
      get "user/onlineorder/password_hint" => "user#password_hint", :as => :password_hint
    end
  end

  namespace :api do
    namespace :vext do
      get 'rewards' => "rewards#index", :as => :vext_rewards
      post 'rewards/claim' => "rewards#olo_claim", :as => :vext_claim_rewards
      post 'rewards/validate' => "rewards#validate_reward", :as => :vext_validate_reward
      post 'rewards/redeem' => "rewards#redeem", :as => :vext_redeem_rewards
      post 'user/onlineorder/external_customer_connect/' => "users#external_customer_connect", :as => :vext_external_customer_connect
      post 'user/receipts/onlineorder' => "users#onlineorder", :as => :vext_online_order
    end
  end

  #TEST API PAGE FOR NuOrder STUFF ----------
  get "api/vnuor/test/test_nuorder_settings" => "api/vnuor/tests#test_nuorder_settings", as: "test_nuorder_connect_settings"
  get "api/vnuor/test/test_nuorder_signup_login" => "api/vnuor/tests#test_nuorder_signup_login", as: "test_nuorder_signup_login"
  get "api/vnuor/test/test_nuorder_get_profile" => "api/vnuor/tests#test_nuorder_get_profile", as: "test_nuorder_get_profile"
  get "api/vnuor/test/test_nuorder_search_profile" => "api/vnuor/tests#test_nuorder_search_profile", as: "test_nuorder_search_profile"
  get "api/vnuor/test/test_nuorder_forgot_password" => "api/vnuor/tests#test_nuorder_forgot_password", as: "test_nuorder_forgot_password"
  get "api/vnuor/test/test_nuorder_update_password" => "api/vnuor/tests#test_nuorder_update_password", as: "test_nuorder_update_password"
  get "api/vnuor/test/test_nuorder_update_profile" => "api/vnuor/tests#test_nuorder_update_profile", as: "test_nuorder_update_profile"
  get "api/vnuor/test/test_nuorder_password_hint" => "api/vnuor/tests#test_nuorder_password_hint", as: "test_nuorder_password_hint"
  #END TEST API PAGE FOR NuOrder STUFF ----------


  #test api VEXT
  get "api/vext/test/test_customer_connect" => "api/vext/tests#test_customer_connect", as: "test_vext_connect_settings"


  #TEST API PAGE FOR USING APPKEY AS REQUEST HEADER NuOrder STUFF ----------
  post "api/vnuor/test/request_header_for_signup_login" => "api/vnuor/tests#request_header_for_signup_login", as: "request_header_for_signup_login"
  get "api/vnuor/test/request_header_for_get_nuorder_settings" => "api/vnuor/tests#request_header_for_get_nuorder_settings", as: "request_header_for_get_nuorder_settings"
  get "api/vnuor/test/request_header_for_get_user_profile" => "api/vnuor/tests#request_header_for_get_user_profile", as: "request_header_for_get_user_profile"
  get "api/vnuor/test/request_header_for_search_profile" => "api/vnuor/tests#request_header_for_search_profile", as: "request_header_for_search_profile"
  post "api/vnuor/test/request_header_for_forgot_password" => "api/vnuor/tests#request_header_for_forgot_password", as: "request_header_for_forgot_password"
  post "api/vnuor/test/request_header_for_update_password" => "api/vnuor/tests#request_header_for_update_password", as: "request_header_for_update_password"
  post "api/vnuor/test/request_header_for_update_profile" => "api/vnuor/tests#request_header_for_update_profile", as: "request_header_for_update_profile"
  get "api/vnuor/test/request_header_for_password_hint" => "api/vnuor/tests#request_header_for_password_hint", as: "request_header_for_password_hint"
  #END TEST API PAGE FOR USING APPKEY AS REQUEST HEADER NuOrder STUFF ----------

  #ONOSYS INTEGRATION STUFF API ROUTES
  namespace :api do
    namespace :vono do
      get "connect/setting" => "user#connect_setting", :as => :connect_setting
      get "user/register" => "user#register", :as => :register
      get "user/login" => "user#login", :as => :login
      post "user/forgot_password" => "user#forgot_password", :as => :forgot_password
      post "user/update_password" => "user#update_password", :as => :update_password
      get "user/profile" => "user#get_profile", :as => :get_profile
      put "user/profile" => "user#update_profile", :as => :update_profile
      post "user/connect" => "user#connect", :as => :connect

      post "user/receipts/onlineorder" => "receipts#online_order", :as => :online_order_upload
      get "user/refresh_token" => "user#refresh_token", :as => :refresh_token
      get "rewards" => "user#get_rewards", :as => :get_rewards
      post "rewards/claim" => "user#reward_claim", :as => :reward_claim
      post "rewards/redeem" => "user#reward_redeem", :as => :reward_redeem
    end
  end

  #OLO INTEGRATION STUFF API ROUTES
  namespace :api do
    namespace :volo do
      post "user/connect" => "user#connect", :as => :connect
      post "user/profile/update" => "user#update_profile", :as => :update_profile
      get "user/profile" => "user#merge_profile", :as => :merge_profile
    end
  end

  #TEST API PAGE FOR ONOSYS STUFF ----------
  post "api/vono/test/request_header_for_signup_login" => "api/vnuor/tests#request_header_for_signup_login"
  get "api/vono/test/test_onosys_settings" => "api/vono/tests#test_onosys_settings"
  get "api/vono/test/test_onosys_signup_login" => "api/vono/tests#test_onosys_signup_login"
  get "api/vono/test/test_onosys_get_profile" => "api/vono/tests#test_onosys_get_profile"
  get "api/vono/test/test_onosys_forgot_password" => "api/vono/tests#test_onosys_forgot_password"
  get "api/vono/test/test_onosys_update_password" => "api/vono/tests#test_onosys_update_password"
  get "api/vono/test/test_onosys_update_profile" => "api/vono/tests#test_onosys_update_profile"
  get "api/vono/test/test_onosys_get_refresh_token" => "api/vono/tests#test_onosys_get_refresh_token"
  get "api/vono/test/test_onosys_get_rewards" => "api/vono/tests#test_onosys_get_rewards"
  get "api/vono/test/test_onosys_reward_claim" => "api/vono/tests#test_onosys_reward_claim"
  get "api/vono/test/test_onosys_reward_redeem" => "api/vono/tests#test_onosys_reward_redeem"
  #END TEST API PAGE FOR ONOSYS STUFF ----------

  #TEST API PAGE FOR USING APPKEY AS REQUEST HEADER ONOSYS STUFF ----------
  get "api/vono/test/request_header_for_get_onosys_settings" => "api/vono/tests#request_header_for_get_onosys_settings"
  post "api/vnuor/test/request_header_for_signup_login" => "api/vnuor/tests#request_header_for_signup_login"
  get "api/vono/test/request_header_for_get_user_profile" => "api/vono/tests#request_header_for_get_user_profile"
  post "api/vono/test/request_header_for_forgot_password" => "api/vono/tests#request_header_for_forgot_password"
  post "api/vono/test/request_header_for_update_password" => "api/vono/tests#request_header_for_update_password"
  post "api/vono/test/request_header_for_update_profile" => "api/vono/tests#request_header_for_update_profile"
  get "api/vono/test/request_header_for_get_refresh_token" => "api/vono/tests#request_header_for_get_refresh_token"
  get "api/vono/test/request_header_for_get_rewards" => "api/vono/tests#request_header_for_get_rewards"
  post "api/vono/test/request_header_for_reward_claim" => "api/vono/tests#request_header_for_reward_claim"
  post "api/vono/test/request_header_for_reward_redeem" => "api/vono/tests#request_header_for_reward_redeem"
  #END TEST API PAGE FOR USING APPKEY AS REQUEST HEADER ONOSYS STUFF ----------


  # config/routes.rb


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # map.namespace :admin do |admin|
  #   admin.resources :translations
  # end
  root :to => 'owner/members#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end

