
devise_for :users, :controllers => {:sessions => "api/v2/sessions", :registrations => "api/v2/registrations", :omniauth_callbacks=>'api/v2/omniauth_callbacks'}
devise_scope :user do
  get "api/v2/user/login", :to => "api/v2/sessions#create"
  post "api/v2/user/login", :to => "api/v2/sessions#create"

  post "api/v2/user/signup", :to => "api/v2/registrations#create"
  get "api/v2/user/migrate", :to => "api/v2/registrations#migrate"

  post "api/v2/user/logout", :to => "api/v2/sessions#destroy"

  get "api/v2/user/deactivate" => "api/v2/sessions#deactivate_account", :as => :api_v2_deactivate_account
  get "api/v2/user/reactivate" => "api/v2/sessions#reactivate_account", :as => :api_v2_reactivate_account
  post "api/v2/user/validate_token", :to => "api/v2/sessions#validate_token"
end



scope "/api/v2" do
  scope "/receipts" do
    post "/" => "receipts#create"
    get ":receipt_id" => "receipts#show"
    get ":receipt_id/transactions" => "receipts#transactions"
    put ":receipt_id" => "receipts#update"
  end

  scope "/promocode" do
    post "/" ,:to =>"api/v1/promo_codes#create"
  end
end