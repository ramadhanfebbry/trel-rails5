##test api page for partner
match "/api/test/partner_categories" => "api/v1/tests#partner_categories", :via => :get
match "/api/test/partner_rewards" => "api/v1/tests#partner_rewards", :via => :get
match "/api/test/get_partner_rewards" => "api/v1/tests#get_partner_rewards", :via => :get

## test api
#post '/email_processor' => 'griddler#create'
mount_griddler
match "/api/test/reward_list" => "api/v1/tests#reward_list", :via => :get
match "/api/test/test_timeout" => "api/v1/tests#test_timeout", :via => :get
match "/api/test/olo_list_summaries" => "api/v1/tests#olo_list_summaries", :via => :get
match "/api/test/olo_orders" => "api/v1/tests#olo_orderexports", :via => :get
match "/api/test/olo_export" => "api/v1/tests#olo_export", :via => :get
match "/api/test/get_olo_order_batch" => "api/v1/tests#get_olo_order_batch", :via => :get
match "/api/test/get_olo_orders" => "api/v1/tests#get_olo_orderexports", :via => :get
match "/api/test/get_olo_list_summaries" => "api/v1/tests#get_olo_list_summaries", :via => :get
match "/api/test/geo_data_setting" => "api/v1/tests#geo_data_testing", :via => :get
match "/api/test/social_shares" => "api/v1/tests#social_shares", :via => :get
match "/api/test/post_social_shares" => "api/v1/tests#post_social_shares", :via => :get
match "/api/test/test_fb_reactivate_account" => "api/v1/tests#test_fb_reactivate_account", :via => :get
match "/api/test/sign_up_with_referral" => "api/v1/tests#sign_up_with_referral", :via => :get
match "/api/test/referral" => "api/v1/tests#referral", :via => :get
match "/api/test/referral_email" => "api/v1/tests#referral_email", :via => :get
match "/api/test/chain_image_share" => "api/v1/tests#chain_image_share", :via => :get
match "/api/test/form_chain_image" => "api/v1/tests#form_chain_image", :via => :get
match "/api/test/:survey_id/answer_surveys" => "api/v1/tests#test_answer_survey", :via => :get
match "/api/test/answer_deal_survey" => "api/v1/tests#test_answer_deal_survey", :via => :get
match "/api/test/post_answer_deal_survey" => "api/v1/tests#test_post_answer_deal_survey", :via => :post, :as => :test_post_answer_deal_survey
match "/api/test/get_deal_survey" => "api/v1/tests#test_get_deal_survey", :via => :get
match "/api/test/get_data_survey" => "api/v1/tests#test_post_deal_survey", :via => :post,:as => :test_post_deal_survey
match "/api/test/claim" => "api/v1/tests#test_claim", :via => :get
match "/api/test/locate" => "api/v1/tests#test_locate", :via => :get
match "/api/test/receipt" => "api/v1/tests#test_submit_receipt", :via => :get
match "/api/test/offer_restaurants/:id" => "api/v1/tests#getOfferRestaurants", :via => :get
match "/api/test/promocode", :to =>"api/v1/tests#test_promocode", :via => :get
get "api/test/forgot_password" => "api/v1/tests#forgot_password"
get "api/test/submit_receipt" => "api/v1/tests#submit_receipt"
get "api/test/update_password" => "api/v1/tests#update_password"
get "api/test/user_signin" => "api/v1/tests#user_signin"
get "api/test/test_list_deal" => "api/v1/tests#test_list_deals", :as => :test_list_deals
get "api/test/test_detail_deal" => "api/v1/tests#test_detail_deals", :as => :test_detail_deals
get "api/test/test_deal_location" => "api/v1/tests#test_deal_location", :as => :test_deal_location
get "api/test/test_post_checkin" => "api/v1/tests#test_post_checkin", :as => :test_post_checkin
match "/api/test/test_user_signup" => "api/v1/tests#test_user_signup", :as => 'test_signup'
match "/api/test/claim_message" => "api/v1/tests#claim_message", :as => 'claim_message'
match "/api/test/test_submit_survey" => "api/v1/tests#test_submit_survey", :as => 'test_submit_survey'
match "/api/test/test_submit_survey_step2" => "api/v1/tests#test_submit_survey_step2", :as => 'test_submit_survey_step2'
match "/api/test/test_post_submit_survey" => "api/v1/tests#test_post_submit_survey", :as => :test_post_submit_survey
get "/api/test/test_get_categories" => "api/v1/tests#test_get_categories", :as => 'test_get_categories'
get "/api/test/test_submit_categories" => "api/v1/tests#test_submit_categories", :as => 'test_submit_categories'
get "/api/test/load_categories/:appkey" => "api/v1/tests#load_categories", :as => 'load_categories'
get "/api/test/test_weekly_user_goal" => "api/v1/tests#test_weekly_user_goal", :as => 'test_weekly_user_goal'
get "/api/test/test_complete_goal" => "api/v1/tests#test_complete_goal", :as => 'test_complete_goal'
get "/api/test/load_user_goals/:auth_token" => "api/v1/tests#load_user_goals", :as => 'load_user_goals'
get "/api/test/test_deadline" => "api/v1/tests#test_deadline", :as => 'test_deadline'
get "/api/test/test_history" => "api/v1/tests#test_history", :as => 'test_history'
get "/api/test/test_reward_activity" => "api/v1/tests#test_reward_activity", :as => 'test_reward_activity'
get "/api/test/test_deactivate_account" => "api/v1/tests#test_deactivate_account", :as => 'test_deactivate_account'
get "/api/test/test_reactivate_account" => "api/v1/tests#test_reactivate_account", :as => 'test_reactivate_account'
get "/api/test/test_confirm_reactivate_account" => "api/v1/tests#test_confirm_reactivate_account", :as => 'test_confirm_reactivate_account'
get "/api/test/test_pos_reward" => "api/v1/tests#test_pos_reward", :as => 'test_pos_reward'
get "/api/test/test_pos_check_upload_status" => "api/v1/tests#test_pos_check_upload_status", :as => 'test_pos_check_upload_status'
get "/api/test/test_pos_check_upload" => "api/v1/tests#test_pos_check_upload", :as => 'test_pos_check_upload'
get "/api/test/test_pos_menu_upload_status" => "api/v1/tests#test_pos_menu_upload_status", :as => 'test_pos_menu_upload_status'
get "/api/test/test_pos_menu_upload" => "api/v1/tests#test_pos_menu_upload", :as => 'test_pos_menu_upload'
get "/api/test/test_claim_reward" => "api/v1/tests#test_claim_reward", :as => 'test_claim_reward'
get "/api/test/test_claim_reward_get_info" => "api/v1/tests#test_claim_reward_get_info", :as => 'test_claim_reward_get_info'
get "/api/test/load_rewards" => "api/v1/tests#load_rewards", :as => 'test_load_rewards'
get "/api/test/load_restaurants_and_users" => "api/v1/tests#load_restaurants_and_users", :as => 'test_load_restaurants_and_users'
get "/api/test/test_share_text_list" => "api/v1/tests#test_share_text_list", :as => 'test_share_text_list'
get "/api/test/load_users" => "api/v1/tests#load_users", :as => 'load_users'
get "/api/test/load_raffle_additional_info" => "api/v1/tests#load_raffle_additional_info", :as => 'load_raffle_additional_info'
get "/api/test/load_giftable_rewards" => "api/v1/tests#load_giftable_rewards", :as => 'load_giftable_rewards'
get "/api/test/load_survey" => "api/v1/tests#load_survey", :as => 'load_survey'
get "/api/test/test_active_reward" => "api/v1/tests#test_active_reward", :as => "test_active_reward"
get "/api/test/test_module_accessed" => "api/v1/tests#test_module_accessed", :as => "test_module_accessed"
get "/api/test/test_facebook_data" => "api/v1/tests#test_facebook_data", :as => "test_facebook_data"
get "/api/test/test_user_update_profile" => "api/v1/tests#test_user_update_profile", :as => "test_user_update_profile"
get "/api/test/test_birthday_reward" => "api/v1/tests#test_birthday_reward", :as => "test_birthday_reward"
post "/api/test/post_birthday_reward" => "api/v1/tests#post_birthday_reward", :as => "post_birthday_reward"
get "/api/test/test_user_get_oauth_token" => "api/v1/tests#test_user_get_oauth_token", :as => "test_user_get_oauth_token"
get "/api/test/test_get_olo_mobile_uri" => "api/v1/tests#test_get_olo_mobile_uri", :as => "test_get_olo_mobile_uri"

#gifting reward api test
get "/api/test/test_add_gift_to_user" => "api/v1/tests#test_add_gift_to_user", :as => "test_add_gift_to_user"
get "/api/test/test_tap_to_gift" => "api/v1/tests#test_tap_to_gift", :as => "test_tap_to_gift"
get "/api/test/test_giftcode" => "api/v1/tests#test_giftcode", :as => "test_giftcode"
get "/api/test/test_game_points" => "api/v1/tests#test_game_points", :as => "test_game_points"
get "/api/test/test_game_activity" => "api/v1/tests#test_game_activity", :as => "test_game_activity"
get "/api/test/test_php_migration/:type" => "api/v1/tests#test_php_migration", :as => "test_php_migration"
get "/api/test/test_user_activity" => "api/v1/tests#test_user_activity", :as => "test_user_activity"
get "/api/test/test_user_profile" => "api/v1/tests#test_user_profile", :as => "test_user_profile"
get "/api/test/test_launch_text" => "api/v1/tests#test_launch_text", :as =>  "test_launch_text"
get "/api/test/test_generate_keychain_value" => "api/v1/tests#test_generate_keychain_value", :as =>  "test_generate_keychain_value"
get "/api/test/test_braintree_client_token" => "api/v1/tests#test_braintree_client_token", :as =>  "test_braintree_client_token"

#TEST API PAGE FOR POS STUFF ----------
get "/api/test/test_pos_reward" => "api/v1/tests#test_pos_reward", :as => 'test_pos_reward'
get "/api/test/test_pos_check_upload_status" => "api/v1/tests#test_pos_check_upload_status", :as => 'test_pos_check_upload_status'
get "/api/test/test_pos_check_upload" => "api/v1/tests#test_pos_check_upload", :as => 'test_pos_check_upload'
get "/api/test/test_pos_menu_upload_status" => "api/v1/tests#test_pos_menu_upload_status", :as => 'test_pos_menu_upload_status'
get "/api/test/test_pos_menu_upload" => "api/v1/tests#test_pos_menu_upload", :as => 'test_pos_menu_upload'
get "/api/test/test_pos_receipt_upload" => "api/v1/tests#test_pos_receipt_upload", :as => 'test_pos_receipt_upload'
get "/api/test/test_pos_receipt_barcode" => "api/v1/tests#test_pos_receipt_barcode", :as => 'test_pos_receipt_barcode'
get "/api/test/test_submit_barcode" => "api/v1/tests#test_submit_barcode", :as => 'test_submit_barcode'
get "/api/test/test_pos_send_receipt" => "api/v1/tests#test_pos_send_receipt", :as => 'test_pos_send_receipt'
get "/api/test/test_get_user_code" => "api/v1/tests#test_get_user_code", :as => "test_get_user_code"
get "/api/test/test_get_payment_code" => "api/v1/tests#test_get_payment_code", :as => "test_get_payment_code"
get "/api/test/test_get_gift_code" => "api/v1/tests#test_get_gift_code", :as => "test_get_gift_code"
get "/api/test/test_get_giftcard_skin" => "api/v1/tests#test_get_giftcard_skin", :as => "test_get_giftcard_skin"
get "/api/test/test_submit_usercode" => "api/v1/tests#test_submit_usercode", :as => 'test_submit_usercode'
get "/api/test/test_get_code_type" => "api/v1/tests#test_get_code_type", :as => 'test_get_code_type'
get "/api/test/test_gallery_stream" => "api/v1/tests#test_gallery_stream", :as => 'test_gallery_stream'
get "/api/test/test_raffle" => "api/v1/tests#test_raffle", :as => 'test_raffle'
get "/api/test/test_raffle_submit_code" => "api/v1/tests#test_raffle_submit_code", :as => 'test_raffle_submit_code'
get "/api/test/test_raffle_submit_info" => "api/v1/tests#test_raffle_submit_info", :as => 'test_raffle_submit_info'

get '/api/test/test_micros_check_detail' => "api/v1/tests#test_micros_check_detail", :as => :test_micros_check_detail
get '/api/test/test_pay_table_service' => "api/v1/tests#test_pay_table_service", :as => :test_pay_table_service
get '/api/test/test_pull_check_payment' => "api/v1/tests#test_pull_check_payment", :as => :test_pull_check_payment
get '/api/test/test_get_check' => "api/v1/tests#test_get_check_detail", :as => :test_get_check
get '/api/test/test_pull_payment_alert' => "api/v1/tests#test_pull_payment_alert", :as => :test_pull_payment_alert

#END API PAGE POS STUFF -------------

#test xpient pos
get "/api/test/test_xpient_submit_order" => "api/v1/tests#test_xpient_submit_order", :as => 'test_xpient_submit_order'
#end-----

#pull surveys
get "/api/test/test_pull_survey" => "api/v1/tests#test_pull_survey", :as => 'test_pull_survey'
get "/api/test/test_skip_survey" => "api/v1/tests#test_skip_survey", :as => 'test_skip_survey'

get "/api/test/test_get_restaurant_aoid" => "api/v1/tests#test_get_restaurant_aoid", :as => "test_get_restaurant_aoid"

#automatic test
post "/api/test/automatic_test" => "api/v1/tests#automatic_test", format: "js"
# end --

#test pos LOyalty Treatware
#POS LOYALTY get info
get "/api/test/test_tw_get_info" => "api/v1/tests#test_tw_get_info", :as => 'test_tw_get_info'
get "/api/test/test_tw_apply_offers" => "api/v1/tests#test_tw_apply_offers", :as => 'test_tw_apply_offers'
get "/api/test/test_tw_submit_orders" => "api/v1/tests#test_tw_submit_orders", :as => 'test_tw_submit_orders'
#end

#TEST VANTIV STUFF
get "/api/test/test_vantiv_activate_card" => "api/v1/tests#test_vantiv_activate_card", :as => 'test_vantiv_activate_card'
get "/api/test/test_vantiv_deposit_card" => "api/v1/tests#test_vantiv_deposit_card", :as => 'test_vantiv_deposit_card'
get "/api/test/test_vantiv_get_card_balance" => "api/v1/tests#test_vantiv_get_card_balance", :as => 'test_vantiv_get_card_balance'
get "/api/test/test_vantiv_gift" => "api/v1/tests#test_vantiv_gift", :as => 'test_vantiv_gift'
get "/api/test/test_vantiv_card_unload" => "api/v1/tests#test_vantiv_card_unload", :as => 'test_vantiv_card_unload'
get "/api/test/test_vantiv_api_call" => "api/v1/tests#test_vantiv_api_call", :as => 'test_vantiv_api_call'

#END VANTIV STUFF

#TEST OLO STUFF
  get "/api/test/test_olo_settings" => "api/v1/tests#test_olo_settings", :as => 'test_olo_settings'
#END OLO STUFF


devise_for :users, :controllers => {:sessions => "api/v1/sessions", :registrations => "api/v1/registrations", :omniauth_callbacks=>'api/v1/omniauth_callbacks'}
devise_scope :user do
  get "api/v1/user/login", :to => "api/v1/sessions#create"
  post "api/v1/user/login", :to => "api/v1/sessions#create"

  get "api/v1/user/signup", :to => "api/v1/registrations#create"
  post "api/v1/user/signup", :to => "api/v1/registrations#create"
  get "api/v1/user/migrate", :to => "api/v1/registrations#migrate"

  get "api/v1/user/logout", :to => "api/v1/sessions#destroy"

  get "api/v1/user/deactivate" => "api/v1/sessions#deactivate_account", :as => :api_v1_deactivate_account
  get "api/v1/user/reactivate" => "api/v1/sessions#reactivate_account", :as => :api_v1_reactivate_account
  post "api/v1/user/validate_token", :to => "api/v1/sessions#validate_token"

end

namespace :api do
  namespace :v1 do
    #PARTNER
    get "/category", :to => "partner#category"
    post "/partner_reward", :to => "partner#partner_reward"
    get "/partner_reward", :to => "partner#get_partner_reward"
    post "/partner_reward_code", :to => "partner#partner_reward_code"
    post "/bridg_webhook", :to => "partner#webhook"

    get "/olo/settings", :to => "ololehos#settings"

    match 'version/latest' => 'application_keys#latest'
    match 'version/last' => 'application_keys#last'
    match 'checkin/locations' => 'deals#deal_locations', :as => :deal_location
    #post 'get_email_sendgrid' => 'webhook_sendgrid#index', :as => :webhook_sendgrid
    #post Gridhook.config.event_receive_path => 'api/v1/webhook_sendgrid#create'
    post 'checkin/submit' => 'deals#checkin_submit', :as => :post_checkin
    get 'checkin/submit' => 'deals#checkin_submit'
    match '/share/list' => 'image_shares#get_image_info', :as => :get_image_info, :method => :get
    match '/referral/email' => 'referrals#email', :as => :referral_email, :method => :get
    match '/referral/mail_to_referer' => 'referrals#mail_to_referer', :as => :mail_to_referer, :method => :get
    match '/referral' => 'referrals#index', :as => :referral, :method => :get
    match '/launch/text' => 'application_keys#launch_text', :as => "launch_text", :method  => :get
    match 'keychain/generate' => "user#generate_keychain", :as => :generate_keychain, :method => :get
    get 'gallery_stream' => "application_keys#gallery_stream", :as => :gallery_stream
    match '/user/code' => "user#get_user_id", :as => :get_user_id
    match '/user/pay_code' => "user#pay_code", :as => :pay_code
    get '/user/giftcard_code' => "user#gift_code", :as => :gift_code
    get '/user/giftcard_skin' => "user#giftcard_skin", :as => :giftcard_skin
    post "/analytics/module_accessed" => "user#module_accessed", :as => :module_accessed
    post "/analytics/geodata" => "user#geodata", :as => :geodata
    get '/braintree/client_token' => "user#braintree_client_token", :as => "braintree_client_token"
    post "/user/profile/update" => "user#update_profile", :as => "user_update_profile"
    post "/pay/pos/table_service" => "receipts#pay_table_service", :as => :pay_table_service
    get "/user/oauth_token" => "user#get_oauth_token", :as => :get_oauth_token
    get "/user/get_olo_mobile_uri" => "user#get_olo_mobile_uri", :as => :get_olo_mobile_uri
    post "/user/bt/remove_old_card" => "user#delete_old_bt_credit_card", :as => :delete_old_bt_credit_card

    #VANTIV
    post "/giftcard/activate" => "gift_cards#activate", :as => :vantiv_activate_card
    post "/giftcard/deposit" => "gift_cards#deposit", :as => :vantiv_deposit_card
    post "/giftcard/balance" => "gift_cards#balance", :as => :vantiv_balance_card
    post "/giftcard/unload" => "gift_cards#unload", :as => :vantiv_unload_card
    post "/giftcard/gift" => "gift_cards#gift", :as => :vantiv_gift
    post "/vantiv/test_call" => "gift_cards#test_call", :as => :vantiv_test_call

    post '/notification' => "user#notifications", :as => :user_notifications
    #END PUSH SERVICES API

    resources :receipts do
      collection do
        get "upload"
        match "code" => "receipts#upload", :as => :code
        get 'pos/code' => "receipts#check_detail", :as => :check_detail
      end
    end
    resources :rewards do
      collection do
        get 'locate'
        post 'claim'
        get 'activity'
        get "share"
        get "add_giftable_to_user"
        post "gift" => "rewards#tap_to_gift", :as => "tap_to_gift"
        get "active" => "rewards#active", :as => "active"
      end
    end
    resources :offers do
      collection do
        get 'nearby'
        get 'restaurants'
      end
    end

    resources :restaurants do
      collection do
        get 'nearby'
      end
    end

    resources :survey do
      member do
        #match 'answer' => "survey", :method => :any
        match 'answer' , :method => :get
      end
      collection do
        get 'getSurvey'
        get 'pull'
        post 'skip'
      end
    end

    resources :deals, :only => [] do
      get 'get_survey'
      post 'answer_survey'
      collection do
        get "detail" => "deals#deal_detail", :as => :detail_deal
        get '/' => "deals#list_deals", :as => :list_deals
      end
    end
    #resources :cities
    resources :user do
      collection do
        post 'update_password'
        get 'activity'
        post 'forgot_password'
        get 'profile'
        post 'facebook_data'
        #post 'collect_geo_data'
      end
    end

    resources :social_shares do
      collection do
        post "user_interaction"
      end
    end

    resources :goals, :only => :index do
      collection do
        post 'complete'
        get 'deadline'
        get 'history'
      end
    end
    resources :categories, :only => :index do
      collection do
        post 'select'
        get 'select'
      end
    end

    resource :game do
      collection do
        post 'points'
        get 'activity'
      end
    end

    resource :raffle do
      collection do
        post "submit_code"
        post "submit_info"
      end
    end

  end

  namespace :v2 do
    #PARTNER
    get "/category", :to => "partner#category"
    post "/partner_reward", :to => "partner#partner_reward"
    get "/partner_reward", :to => "partner#get_partner_reward"

    get "/olo/settings", :to => "ololehos#settings"
    match 'version/latest' => 'application_keys#latest'
    match 'version/last' => 'application_keys#last'
    match 'checkin/locations' => 'deals#deal_locations', :as => :deal_location
    post 'checkin/submit' => 'deals#checkin_submit', :as => :post_checkin
    get 'checkin/submit' => 'deals#checkin_submit'
    match '/share/list' => 'image_shares#get_image_info', :as => :get_image_info, :method => :get
    get 'referral/email' => 'referrals#email', :as => :referral_email
    match '/referral/mail_to_referer' => 'referrals#mail_to_referer', :as => :mail_to_referer, :method => :get
    match '/referral' => 'referrals#index', :as => :referral, :method => :get
    match '/launch/text' => 'application_keys#launch_text', :as => "launch_text", :method  => :get
    match 'keychain/generate' => "user#generate_keychain", :as => :generate_keychain, :method => :get
    get 'gallery_stream' => "application_keys#gallery_stream", :as => :gallery_stream
    match '/user/code' => "user#get_user_id", :as => :get_user_id, via: [:get, :post]
    match '/user/pay_code' => "user#pay_code", :as => :pay_code, via: [:get, :post]
    get '/user/giftcard_code' => "user#gift_code", :as => :gift_code
    get '/user/giftcard_skin' => "user#giftcard_skin", :as => :giftcard_skin
    post "/analytics/module_accessed" => "user#module_accessed", :as => :module_accessed
    post "/analytics/geodata" => "user#geodata", :as => :geodata
    get '/braintree/client_token' => "user#braintree_client_token", :as => "braintree_client_token"
    post "/user/profile/update" => "user#update_profile", :as => "user_update_profile"
    post "/pay/pos/table_service" => "receipts#pay_table_service", :as => :pay_table_service
    get "/user/oauth_token" => "user#get_oauth_token", :as => :get_oauth_token
    get "/user/get_olo_mobile_uri" => "user#get_olo_mobile_uri", :as => :get_olo_mobile_uri
    post "/user/bt/remove_old_card" => "user#delete_old_bt_credit_card", :as => :delete_old_bt_credit_card

    #VANTIV
    post "/giftcard/activate" => "gift_cards#activate", :as => :vantiv_activate_card
    post "/giftcard/deposit" => "gift_cards#deposit", :as => :vantiv_deposit_card
    get "/giftcard/balance" => "gift_cards#balance", :as => :vantiv_balance_card
    post "/giftcard/unload" => "gift_cards#unload", :as => :vantiv_unload_card
    post "/giftcard/gift" => "gift_cards#gift", :as => :vantiv_gift
    post "/vantiv/test_call" => "gift_cards#test_call", :as => :vantiv_test_call

    post '/notification' => "user#notifications", :as => :user_notifications
    #END PUSH SERVICES API

    resources :receipts do
      collection do
        get "upload"
        match "code" => "receipts#upload", :as => :code, via: [:get, :post]
        get 'pos/code' => "receipts#check_detail", :as => :check_detail
      end
    end
    resources :rewards do
      collection do
        get 'locate'
        post 'claim'
        get 'activity'
        get "share"
        get "add_giftable_to_user"
        post "gift" => "rewards#tap_to_gift", :as => "tap_to_gift"
        get "active" => "rewards#active", :as => "active"
      end
    end
    resources :offers do
      collection do
        get 'nearby'
        get 'restaurants'
      end
    end

    resources :restaurants do
      collection do
        get 'nearby'
      end
    end

    resources :survey do
      member do
        #match 'answer' => "survey", :method => :any
        match 'answer' , :method => :get
      end
      collection do
        get 'getSurvey'
        get 'pull'
        post 'skip'
      end
    end

    resources :deals, :only => [] do
      get 'get_survey'
      post 'answer_survey'
      collection do
        get "detail" => "deals#deal_detail", :as => :detail_deal
        get '/' => "deals#list_deals", :as => :list_deals
      end
    end
    #resources :cities
    resources :user do
      collection do
        post 'update_password'
        get 'activity'
        post 'forgot_password'
        get 'profile'
        post 'facebook_data'
        #post 'collect_geo_data'
      end
    end

    resources :social_shares do
      collection do
        post "user_interaction"
      end
    end

    resources :goals, :only => :index do
      collection do
        post 'complete'
        get 'deadline'
        get 'history'
      end
    end
    resources :categories, :only => :index do
      collection do
        post 'select'
        get 'select'
      end
    end

    resource :game do
      collection do
        post 'points'
        get 'activity'
      end
    end

    resource :raffle do
      collection do
        post "submit_code"
        post "submit_info"
      end
    end

  end
end

scope "/api/v1" do
  scope "/receipts" do
    post "/" => "receipts#create"
    get ":receipt_id" => "receipts#show"
    get ":receipt_id/transactions" => "receipts#transactions"
    put ":receipt_id" => "receipts#update"
  end

  scope "/promocode" do
    post "/" ,:to =>"api/v1/promo_codes#create"
  end
end