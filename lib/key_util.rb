module KeyUtil
  def generate_random_string(size, chars)
    (1..size).map{ chars[rand(chars.length)] }.join
  end
end