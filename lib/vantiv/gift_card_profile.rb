module Vantiv
  class GiftCardProfile < ActiveRecord::Base
    self.table_name = "gift_card_profiles"
    attr_accessible :activate_at, :amount, :gift_card_number, :user_id, :expiration_month, :expiration_year, :cvv, :cvv2,
                    :security_code, :vantiv_gift_card_id

    belongs_to :user

  end
end
