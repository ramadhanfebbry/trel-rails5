module Vantiv
  class TransactionLog < ActiveRecord::Base
    self.table_name = "vantiv_transaction_logs"

    belongs_to :chain
    belongs_to :user

    ### paperclip
    has_attached_file :barcode,
                      :styles => { :small => Setting.paperclip.styles.small,
                                   :large => Setting.paperclip.styles.large },
                      :storage => :s3,
                      :bucket => Setting.storage.s3_bucket,
                      :s3_credentials => {
                          :access_key_id => Setting.storage.s3_access_key_id,
                          :secret_access_key => Setting.storage.s3_secret_access_key
                      }

    def self.add(transaction_type, user, card_number, ref_num, transaction_id, amount, relevant_response, vantiv_response, mp_response, success)
      selected_user = user.class.to_s == "User" ? user : nil
      selected_chain = user.class.to_s == "Chain" ? user : nil
      vantiv_log = Vantiv::TransactionLog.create(
        :transaction_type => transaction_type,
        :chain_id => selected_user ? selected_user.chain_id : selected_chain.try(:id),
        :user_id => selected_user.try(:id),
        :card_number => card_number,
        :ref_num => ref_num,
        :transaction_id => transaction_id,
        :amount => amount,
        :relevant_response => relevant_response,
        :vantiv_response => vantiv_response,
        :mp_response => mp_response,
        :success => success
      )
      return vantiv_log.try(:id)
    end

    def transaction_type_description
      case self.transaction_type
        when 1
           "ACTIVATE"
        when 2
           "RELOAD"
        when 3
          "GET BALANCE"
        when 4
          "GIFTING"
        when 5
          "UNLOAD"
        when 6
          "REVERSAL"
      end

    end
  end
end
