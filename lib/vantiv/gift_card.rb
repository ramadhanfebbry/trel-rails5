module Vantiv
  class GiftCard
    attr_accessor :url, :account_id, :account_token, :application_id, :acceptor_id

    def initialize(url, account_id, account_token, application_id, acceptor_id)
      @url = url
      @account_id = account_id
      @account_token = account_token
      @application_id = application_id
      @acceptor_id = acceptor_id
    end

    def time_check_connection
      xml_data = <<-EOF
      <?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <TimeCheck xmlns="https://transaction.elementexpress.com">
      <credentials>
        <AccountID>#{@account_id}</AccountID>
        <AccountToken>#{@account_token}</AccountToken>
        <AcceptorID>#{@acceptor_id}</AcceptorID>
      </credentials>
      <application>
        <ApplicationID>#{@application_id}</ApplicationID>
        <ApplicationName>RelevantMobile</ApplicationName>
        <ApplicationVersion>1.0</ApplicationVersion>
      </application>
    </TimeCheck>
  </soap:Body>
</soap:Envelope>
      EOF

      begin
        uri = URI.parse(@url)
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = xml_data
        request["Content-Type"] = "text/xml; charset=utf-8"
        request["Content-Length"] =  "length"
        request["SOAPAction"] = "https://transaction.elementexpress.com/TimeCheck"
        response = http.request(request)
        if response.is_a?(Net::HTTPOK)
          p "SUCCESS REQUEST ON time_check_connection"
          p response.body
        end
      rescue => e
        p "------------- error request time_check_connection ------"
        p e.message
      end
    end

    def activate_card(card,exp_month, exp_year, amount)
      ref_num = DateTime.current.strftime("%Q")
      xml_data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GiftCardActivate xmlns="https://transaction.elementexpress.com">
      <credentials>
        <AccountID>#{@account_id}</AccountID>
        <AccountToken>#{@account_token}</AccountToken>
        <AcceptorID>#{@acceptor_id}</AcceptorID>
        <NewAccountToken>#{@account_token}</NewAccountToken>
      </credentials>
      <application>
        <ApplicationID>#{@application_id}</ApplicationID>
        <ApplicationName>RelevantMobile</ApplicationName>
        <ApplicationVersion>1.0</ApplicationVersion>
      </application>
      <terminal>
        <TerminalID>001</TerminalID>
        <CardPresentCode>NotPresent</CardPresentCode>
        <CardholderPresentCode>ECommerce</CardholderPresentCode>
        <CardInputCode>ManualKeyed</CardInputCode>
        <CVVPresenceCode>NotProvided</CVVPresenceCode>
        <TerminalCapabilityCode>KeyEntered</TerminalCapabilityCode>
        <TerminalEnvironmentCode>ECommerce</TerminalEnvironmentCode>
        <MotoECICode>NonAuthenticatedSecureECommerceTransaction</MotoECICode>
      </terminal>
      <card>
        <CardNumber>#{card}</CardNumber>
        <ExpirationMonth>#{exp_month}</ExpirationMonth>
        <ExpirationYear>#{exp_year}</ExpirationYear>
      </card>
      <transaction>
        <TransactionAmount>#{amount}</TransactionAmount>
        <ReferenceNumber>#{ref_num}</ReferenceNumber>
        <MarketCode>ECommerce</MarketCode>
        <PartialApprovedFlag>True</PartialApprovedFlag>
      </transaction>
    </GiftCardActivate>
  </soap:Body>
</soap:Envelope>
      EOF
      p "XML API : #{xml_data}"
      begin
        uri = URI.parse(@url)
        p "URI IS #{uri}"
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = xml_data
        request["Content-Type"] = "text/xml; charset=utf-8"
        request["Content-Length"] =  "length"
        request["SOAPAction"] = "https://transaction.elementexpress.com/GiftCardActivate"
        response = http.request(request)
        p "RESPONSE activate_card IS #{response}"
        if response.is_a?(Net::HTTPOK)
          response_hash = Hash.from_xml(response.body) rescue nil
          unless response_hash.blank?
            response_code = response_hash["Envelope"]["Body"]["GiftCardActivateResponse"]["response"]["ExpressResponseCode"]
            response_message = response_hash["Envelope"]["Body"]["GiftCardActivateResponse"]["response"]["ExpressResponseMessage"]
            transaction_id = response_hash["Envelope"]["Body"]["GiftCardActivateResponse"]["response"]["Transaction"]["TransactionID"] rescue nil
            p "RESPONSE ACTIVATE CARD #{response_code} - #{response_message}"
            if response_code == "0" && response_message == "Success"
              p "VANTIV API: ACTIVATE CARD #{card} SUCCESS, AMOUNT #{amount} REF NUM = #{ref_num} TRANSACTION ID = #{transaction_id}"
              return [true, response_code, response_message, ref_num, transaction_id]
            else
              p "VANTIV API: ACTIVATE CARD #{card} FAILED, AMOUNT #{amount} REF NUM = #{ref_num}  TRANSACTION ID = #{transaction_id}"
              return [false, response_code, response_message, ref_num, transaction_id]
            end
          else
            p "VANTIV API: ACTIVATE CARD #{card} FAILED, AMOUNT #{amount} REF NUM = #{ref_num}"
            return [false, "FAILED CONVERT RESPONSE BODY", response.body]
          end
        end
      rescue => e
        p "------------- error request activate card ------"
        p "VANTIV API: ACTIVATE CARD #{card} FAILED, AMOUNT #{amount} REF NUM = #{ref_num}"
        p e.message
        return [false, e.message, ""]
      end
    end

    def card_reload(card,exp_month, exp_year, amount)
      ref_num = DateTime.current.strftime("%Q")
      xml_data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GiftCardReload xmlns="https://transaction.elementexpress.com">
      <credentials>
        <AccountID>#{@account_id}</AccountID>
        <AccountToken>#{@account_token}</AccountToken>
        <AcceptorID>#{@acceptor_id}</AcceptorID>
        <NewAccountToken>#{@account_token}</NewAccountToken>
      </credentials>
      <application>
        <ApplicationID>#{@application_id}</ApplicationID>
        <ApplicationName>RelevantMobile</ApplicationName>
        <ApplicationVersion>1.0</ApplicationVersion>
      </application>
      <terminal>
        <TerminalID>001</TerminalID>
        <CardPresentCode>NotPresent</CardPresentCode>
        <CardholderPresentCode>ECommerce</CardholderPresentCode>
        <CardInputCode>ManualKeyed</CardInputCode>
        <CVVPresenceCode>NotProvided</CVVPresenceCode>
        <TerminalCapabilityCode>KeyEntered</TerminalCapabilityCode>
        <TerminalEnvironmentCode>ECommerce</TerminalEnvironmentCode>
        <MotoECICode>NonAuthenticatedSecureECommerceTransaction</MotoECICode>
      </terminal>
      <card>
        <CardNumber>#{card}</CardNumber>
        <ExpirationMonth>#{exp_month}</ExpirationMonth>
        <ExpirationYear>#{exp_year}</ExpirationYear>
      </card>
      <transaction>
        <TransactionAmount>#{amount}</TransactionAmount>
        <ReferenceNumber>#{ref_num}</ReferenceNumber>
        <MarketCode>ECommerce</MarketCode>
        <PartialApprovedFlag>True</PartialApprovedFlag>
      </transaction>
    </GiftCardReload>
  </soap:Body>
</soap:Envelope>
      EOF
      p "XML API : #{xml_data}"
      begin
        uri = URI.parse(@url)
        p "URI IS #{uri}"
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = xml_data
        request["Content-Type"] = "text/xml; charset=utf-8"
        request["Content-Length"] =  "length"
        request["SOAPAction"] = "https://transaction.elementexpress.com/GiftCardReload"
        response = http.request(request)
        p "RESPONSE card_reload IS #{response}"
        if response.is_a?(Net::HTTPOK)
          response_hash = Hash.from_xml(response.body) rescue nil
          unless response_hash.blank?
            response_code = response_hash["Envelope"]["Body"]["GiftCardReloadResponse"]["response"]["ExpressResponseCode"]
            response_message = response_hash["Envelope"]["Body"]["GiftCardReloadResponse"]["response"]["ExpressResponseMessage"]
            transaction_id = response_hash["Envelope"]["Body"]["GiftCardReloadResponse"]["response"]["Transaction"]["TransactionID"] rescue nil
            p "RESPONSE RELOAD CARD #{response_code} - #{response_message}"
            if response_code == "0" && response_message == "Success"
              p "VANTIV API: RELOAD CARD #{card} SUCCESS, AMOUNT #{amount} REF NUM = #{ref_num} TRANSACTION ID = #{transaction_id}"
              return [true, response_code, response_message, ref_num, transaction_id]
            else
              p "VANTIV API: RELOAD CARD #{card} FAILED, AMOUNT #{amount} REF NUM = #{ref_num} TRANSACTION ID = #{transaction_id}"
              return [false, response_code, response_message, ref_num, transaction_id]
            end
          else
            p "VANTIV API: RELOAD CARD #{card} FAILED, AMOUNT #{amount} REF NUM = #{ref_num}"
            return [false, "FAILED CONVERT RESPONSE BODY", response.body]
          end
        end
      rescue => e
        p "------------- error request activate card ------"
        p "VANTIV API: RELOAD CARD #{card} FAILED, AMOUNT #{amount} REF NUM = #{ref_num}"
        p e.message
        return [false, e.message, ""]
      end
    end

    def get_card_balance(card,exp_month, exp_year)
      ref_num = DateTime.current.strftime("%Q")
      xml_data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GiftCardBalanceInquiry xmlns="https://transaction.elementexpress.com">
      <credentials>
        <AccountID>#{@account_id}</AccountID>
        <AccountToken>#{@account_token}</AccountToken>
        <AcceptorID>#{@acceptor_id}</AcceptorID>
        <NewAccountToken>#{@account_token}</NewAccountToken>
      </credentials>
      <application>
        <ApplicationID>#{@application_id}</ApplicationID>
        <ApplicationName>RelevantMobile</ApplicationName>
        <ApplicationVersion>1.0</ApplicationVersion>
      </application>
      <terminal>
        <TerminalID>001</TerminalID>
        <CardPresentCode>NotPresent</CardPresentCode>
        <CardholderPresentCode>ECommerce</CardholderPresentCode>
        <CardInputCode>ManualKeyed</CardInputCode>
        <CVVPresenceCode>NotProvided</CVVPresenceCode>
        <TerminalCapabilityCode>KeyEntered</TerminalCapabilityCode>
        <TerminalEnvironmentCode>ECommerce</TerminalEnvironmentCode>
        <MotoECICode>NonAuthenticatedSecureECommerceTransaction</MotoECICode>
      </terminal>
      <card>
        <CardNumber>#{card}</CardNumber>
        <ExpirationMonth>#{exp_month}</ExpirationMonth>
        <ExpirationYear>#{exp_year}</ExpirationYear>
      </card>
      <transaction>
        <TransactionAmount>0</TransactionAmount>
        <ReferenceNumber>#{ref_num}</ReferenceNumber>
        <MarketCode>ECommerce</MarketCode>
        <PartialApprovedFlag>True</PartialApprovedFlag>
      </transaction>
    </GiftCardBalanceInquiry>
  </soap:Body>
</soap:Envelope>
      EOF
      p "XML API : #{xml_data}"
      begin
        uri = URI.parse(@url)
        p "URI IS #{uri}"
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = xml_data
        request["Content-Type"] = "text/xml; charset=utf-8"
        request["Content-Length"] =  "length"
        request["SOAPAction"] = "https://transaction.elementexpress.com/GiftCardBalanceInquiry"
        response = http.request(request)
        p "RESPONSE get_card_balance IS #{response}"
        if response.is_a?(Net::HTTPOK)
          response_hash = Hash.from_xml(response.body) rescue nil
          unless response_hash.blank?
            response_code = response_hash["Envelope"]["Body"]["GiftCardBalanceInquiryResponse"]["response"]["ExpressResponseCode"]
            response_message = response_hash["Envelope"]["Body"]["GiftCardBalanceInquiryResponse"]["response"]["ExpressResponseMessage"]
            balance = response_hash["Envelope"]["Body"]["GiftCardBalanceInquiryResponse"]["response"]["Transaction"]["BalanceAmount"]
            transaction_id = response_hash["Envelope"]["Body"]["GiftCardBalanceInquiryResponse"]["response"]["Transaction"]["TransactionID"] rescue nil
            p "RESPONSE GET BALANCE CARD #{response_code} - #{response_message}"
            if response_code == "0" && response_message == "Success"
              p "VANTIV API: GET CARD BALANCE #{card} SUCCESS, BALANCE RESULT #{balance} REF NUM = #{ref_num} TRANSACTION ID = #{transaction_id}"
              return [true, response_code, response_message, balance, ref_num, transaction_id]
            else
              p "VANTIV API: GET CARD BALANCE #{card} FAILED, REF NUM = #{ref_num} TRANSACTION ID = #{transaction_id}"
              return [false, response_code, response_message, ref_num, transaction_id]
            end
          else
            p "VANTIV API: GET CARD BALANCE #{card} FAILED, REF NUM = #{ref_num}"
            return [false, "FAILED CONVERT RESPONSE BODY", response.body]
          end
        end
      rescue => e
        p "------------- error request get balance card ------"
        p "VANTIV API: GET CARD BALANCE #{card} FAILED, REF NUM = #{ref_num}"
        p e.message
        return [false, e.message, ""]
      end
    end

    def card_unload(card, exp_month, exp_year, amount = 0)
      ref_num = DateTime.current.strftime("%Q")
      xml_data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GiftCardUnload xmlns="https://transaction.elementexpress.com">
      <credentials>
        <AccountID>#{@account_id}</AccountID>
        <AccountToken>#{@account_token}</AccountToken>
        <AcceptorID>#{@acceptor_id}</AcceptorID>
        <NewAccountToken>#{@account_token}</NewAccountToken>
      </credentials>
      <application>
        <ApplicationID>#{@application_id}</ApplicationID>
        <ApplicationName>RelevantMobile</ApplicationName>
        <ApplicationVersion>1.0</ApplicationVersion>
      </application>
      <terminal>
        <TerminalID>001</TerminalID>
        <CardPresentCode>NotPresent</CardPresentCode>
        <CardholderPresentCode>ECommerce</CardholderPresentCode>
        <CardInputCode>ManualKeyed</CardInputCode>
        <CVVPresenceCode>NotProvided</CVVPresenceCode>
        <TerminalCapabilityCode>KeyEntered</TerminalCapabilityCode>
        <TerminalEnvironmentCode>ECommerce</TerminalEnvironmentCode>
        <MotoECICode>NonAuthenticatedSecureECommerceTransaction</MotoECICode>
      </terminal>
      <card>
        <CardNumber>#{card}</CardNumber>
        <ExpirationMonth>#{exp_month}</ExpirationMonth>
        <ExpirationYear>#{exp_year}</ExpirationYear>
      </card>
      <transaction>
        <TransactionAmount>#{amount}</TransactionAmount>
        <ReferenceNumber>#{ref_num}</ReferenceNumber>
        <MarketCode>ECommerce</MarketCode>
        <PartialApprovedFlag>True</PartialApprovedFlag>
      </transaction>
    </GiftCardUnload>
  </soap:Body>
</soap:Envelope>
      EOF
      p "XML API : #{xml_data}"
      begin
        uri = URI.parse(@url)
        p "URI IS #{uri}"
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = xml_data
        request["Content-Type"] = "text/xml; charset=utf-8"
        request["Content-Length"] =  "length"
        request["SOAPAction"] = "https://transaction.elementexpress.com/GiftCardUnload"
        response = http.request(request)
        p "RESPONSE card_unload IS #{response}"
        if response.is_a?(Net::HTTPOK)
          response_hash = Hash.from_xml(response.body) rescue nil
          unless response_hash.blank?
            response_code = response_hash["Envelope"]["Body"]["GiftCardUnloadResponse"]["response"]["ExpressResponseCode"]
            response_message = response_hash["Envelope"]["Body"]["GiftCardUnloadResponse"]["response"]["ExpressResponseMessage"]
            balance = response_hash["Envelope"]["Body"]["GiftCardUnloadResponse"]["response"]["Transaction"]["BalanceAmount"]
            transaction_id = response_hash["Envelope"]["Body"]["GiftCardUnloadResponse"]["response"]["Transaction"]["TransactionID"] rescue nil
            p "RESPONSE UNLOAD CARD #{response_code} - #{response_message}"
            if response_code == "0" && response_message == "Success"
              p "VANTIV API: GET CARD UNLOAD #{card} SUCCESS, REF NUM = #{ref_num} TRANSACTION ID = #{transaction_id}"
              return [true, response_code, response_message, balance, ref_num, transaction_id]
            else
              p "VANTIV API: GET CARD UNLOAD #{card} FAILED, REF NUM = #{ref_num} TRANSACTION ID = #{transaction_id}"
              return [false, response_code, response_message, balance, ref_num, transaction_id]
            end
          else
            p "VANTIV API: GET CARD UNLOAD #{card} FAILED, REF NUM = #{ref_num}"
            return [false, "FAILED CONVERT RESPONSE BODY", response.body]
          end
        end
      rescue => e
        p "------------- error request Unload card ------"
        p "VANTIV API: GET CARD UNLOAD #{card} FAILED, REF NUM = #{ref_num}"
        p e.message
        return [false, e.message, ""]
      end
    end

    def reversal(card,exp_month, exp_year, amount)
      xml_data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <GiftCardReversal xmlns="https://transaction.elementexpress.com">
      <credentials>
        <AccountID>#{@account_id}</AccountID>
        <AccountToken>#{@account_token}</AccountToken>
        <AcceptorID>#{@acceptor_id}</AcceptorID>
        <NewAccountToken>#{@account_token}</NewAccountToken>
      </credentials>
      <application>
        <ApplicationID>#{@application_id}</ApplicationID>
        <ApplicationName>RelevantMobile</ApplicationName>
        <ApplicationVersion>1.0</ApplicationVersion>
      </application>
      <terminal>
        <TerminalID>001</TerminalID>
        <CardPresentCode>NotPresent</CardPresentCode>
        <CardholderPresentCode>ECommerce</CardholderPresentCode>
        <CardInputCode>ManualKeyed</CardInputCode>
        <CVVPresenceCode>NotProvided</CVVPresenceCode>
        <TerminalCapabilityCode>KeyEntered</TerminalCapabilityCode>
        <TerminalEnvironmentCode>ECommerce</TerminalEnvironmentCode>
        <MotoECICode>NonAuthenticatedSecureECommerceTransaction</MotoECICode>
      </terminal>
      <card>
        <CardNumber>#{card}</CardNumber>
        <ExpirationMonth>#{exp_month}</ExpirationMonth>
        <ExpirationYear>#{exp_year}</ExpirationYear>
      </card>
      <transaction>
        <TransactionAmount>#{amount}</TransactionAmount>
        <MarketCode>ECommerce</MarketCode>
        <PartialApprovedFlag>True</PartialApprovedFlag>
        <ReversalType>System</ReversalType>
      </transaction>
    </GiftCardReversal>
  </soap:Body>
</soap:Envelope>
      EOF
      p "XML API : #{xml_data}"
      begin
        uri = URI.parse(@url)
        p "URI IS #{uri}"
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = xml_data
        request["Content-Type"] = "text/xml; charset=utf-8"
        request["Content-Length"] =  "length"
        request["SOAPAction"] = "https://transaction.elementexpress.com/GiftCardReversal"
        response = http.request(request)
        p "RESPONSE card_reversal IS #{response}"
        if response.is_a?(Net::HTTPOK)
          response_hash = Hash.from_xml(response.body) rescue nil
          unless response_hash.blank?
            response_code = response_hash["Envelope"]["Body"]["GiftCardReversalResponse"]["response"]["ExpressResponseCode"]
            response_message = response_hash["Envelope"]["Body"]["GiftCardReversalResponse"]["response"]["ExpressResponseMessage"]
            p "RESPONSE REVERSAL CARD #{response_code} - #{response_message}"
            if response_code == "0" && response_message == "Success"
              p "VANTIV API: GET CARD REVERSAL #{card} SUCCESS"
              return [true, response_code, response_message]
            else
              p "VANTIV API: GET CARD REVERSAL #{card} FAILED"
              return [false, response_code, response_message]
            end
          else
            p "VANTIV API: GET CARD REVERSAL #{card} FAILED"
            return [false, "FAILED CONVERT RESPONSE BODY", response.body]
          end
        end
      rescue => e
        p "------------- error request REVERSAL card ------"
        p "VANTIV API: GET CARD REVERSAL #{card} FAILED, REF NUM = #{ref_num}"
        p e.message
        return [false, e.message, ""]
      end
    end

  end
end