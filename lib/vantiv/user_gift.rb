module Vantiv
  module UserGift
    def has_vantiv_gift_card?
      Vantiv::GiftCardProfile.where(:user_id => self.id).count > 0
    end

    def vantiv_gift_card_profile
      Vantiv::GiftCardProfile.where(:user_id => self.id).first
    end
  end
end
