class OfferRuleCalculation::CalculateOfferRule

  attr_accessor :offer, :rt, :offer_rules, :dolar_spent, :offer_result, :total_points_get
=begin how to test on console
     receipt_id = 32955
     offer_id = 174
     receipt =Receipt.find(receipt_id)
     offer = Offer.find 174
     rt = receipt.last_transaction
    receipt_transaction = rt
     x = OfferRuleCalculation::CalculateOfferRule.new(offer,rt,receipt)
     x.result
=end

  def initialize(offer, rt, receipt = nil)
    @offer = offer
    @rt = rt # rt means receipt_transactions
    @dolar_spent = @rt.subtotal.to_f * @rt.receipt.chain.points.to_f if receipt.blank? and !@rt.receipt.blank?
    @dolar_spent = @rt.subtotal.to_f * receipt.chain.points.to_f unless receipt.blank?
    @receipt = receipt
  end

  def check_ignore(subtotal)
    @ignore_price = 0
    if get_all_rules.map(&:rule_type).include?(6)
      get_all_rules.each do |x|
      # check ignore item and substract the subtotal
        multiplier = x.multiplier_constant_value # get offer multiplier
        @ignore_price = ignore_calculate_item(x,multiplier) if x.rule_type == 6
      end
    end
    return subtotal + @ignore_price.to_f
  end

  def process_offer
    if is_offer_rules? # there rules on the offer
      rules = get_all_rules
      result = []
      rules.each do |rule|
        val = switch_rule_case(rule)
        result <<  {:rule_id => rule.id , :value => val}
      end
    end
    @offer_result = result
  end

  def result
    chain_points = @rt.receipt.chain.points.to_f rescue @receipt.chain.points.to_f
    @dolar_spent = check_ignore(@rt.subtotal.to_f) * chain_points
    process_offer
    @total_points_get = 0
    unless @offer_result.blank?



      @offer_result.each do |x|
        @total_points_get += x[:value].to_f if x[:value].to_f >= 0
      end

      # check ignore first to reject

      #if @offer_result.size == 1
        @offer_result.each do |x|
          if x[:value] == "IGNORE"
            @total_points_get = -1
            break
          end
        end
      #end

      # check reject item
      @offer_result.each do |x|
        if x[:value] == "REJECTITEM"
          @total_points_get = -2
          break
        end
      end
    end
  end
  # def result
  #   @dolar_spent = check_ignore(@rt.subtotal.to_f) * @rt.receipt.chain.points.to_f
  #   process_offer
  #   @total_points_get = 0
  #   unless @offer_result.blank?
  #     @offer_result.each do |x|
  #       if x[:value] != "IGNORE" # if not ignore
  #         if x[:value] != "REJECTITEM" # if not reject
  #           @total_points_get += x[:value].to_f if x[:value].to_f >= 0# handle for the ignore items value >= 0
  #         else
  #           @total_points_get = -2 # for rejected menu item
  #           break
  #         end
  #       else
  #         @total_points_get = -1
  #         break
  #       end
  #     end
  #   end
  # end

  def get_all_rules
    return  if !is_offer_rules?
    @offer_rules = @offer.offer_rules.active
  end

  def is_offer_rules?
    unless @offer.offer_rules.active.blank?
      return true
    else
      return false
    end
  end

  def switch_rule_case(offer_rule)
    # TYPE = {
    # {
    #     "POINTS MULTIPLIER" =>  1,"CONSTANT POINTS" => 2, "MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER" =>  3, "MENU ITEM BASED- CONSTANT POINTS" => 4,
    #     "MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER, IGNORE SUBTOTAL" => 5, "MENU ITEM BASED - IGNORE MENU ITEMS" => 6
    # }

    ## check the menu item, then reject if type ="MENU ITEM BASED - IGNORE/REJECT RECEIPT" => 7
    if offer_rule.rule_type != 5
      # check subtotal min / max, except the forgot loyalty
      return 0 if check_subtotal_criteria(offer_rule) == false
    end

    if offer_rule.rule_type == 7
      # check subtotal min / max, except the forgot loyalty
      return "REJECTITEM" if reject_item(offer_rule) == true
    end

    multiplier = offer_rule.multiplier_constant_value # get offer multiplier
    case offer_rule.rule_type
      when 1
        value = calculate_points_multiplier(offer_rule,multiplier)
        value = 0 if @rt.subtotal.to_f == 0.0 # if subtotal are zero
      when 2
        value = calculate_points_multiplier(offer_rule,multiplier)
        value = 0 if @rt.subtotal.to_f == 0.0 # if subtotal are zero
      when 3
        value = calculate_item_point(offer_rule,multiplier)
        value = 0 if @rt.subtotal.to_f == 0.0 # if subtotal are zero
      when 4
        value = calculate_item_point(offer_rule,multiplier)
        value = 0 if @rt.subtotal.to_f == 0.0 # if subtotal are zero
      when 5
        value = calculate_item_point(offer_rule,multiplier)
        value = 0 if @rt.subtotal.to_f > 0.0
      when 6
        value = ignore_calculate_item(offer_rule,multiplier)
        value = 0 if @rt.subtotal.to_f == 0.0
      when 8 # MENU ITEM BASED - QUALIFYING ORDERED ITEMS SUBTOTAL, MULTIPLIER
        value = qualify_item_multiply(offer_rule,multiplier)
        value = 0 if @rt.subtotal.to_f == 0.0
    end

    return value
  end

  def qualify_item_multiply(offer_rule,multiplier)
    ### week days conditions start
    unless offer_rule.days_of_week.blank?
      ## check the days
      if offer_rule.include_day_of_week(@rt.issue_date.in_time_zone('UTC').wday.to_i) == false
        return nil
      end

      # start_hour, end_hour = hour_format_date(offer_rule)
      # issue_hour = @rt.issue_date.in_time_zone('UTC').hour.to_i
      # puts "START HOUR AND END HOUR = #{start_hour} END HOUR = #{end_hour} #{issue_hour}"
      #
      # if !start_hour.blank? and !end_hour.blank?
      #   if issue_hour >= start_hour and end_hour >= issue_hour
      #     puts "in"
      #   else
      #     return nil
      #   end
      # end

      is_qualify = included_in_hours?(offer_rule)
      return nil if is_qualify == false
    else
      return nil
    end

    ### week days conditions check above

    # case when there are items for offer
    items = offer_rule.offer_rule_menu_items rescue nil
    unless items.blank?
      # gather the menu item,
      g_items = offer_rule.offer_rule_menu_items
      #item_numbers = g_items.pos_menu_items.map(&:item_number)
      item_numbers = collect_item_numbers(g_items,@rt)
      # g_items.each do |g_item|
      #   item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number) rescue []
      #   item_numbers += Array(g_item.general_menu_item.general_menu_item_range_value.start_value .. g_item.general_menu_item.general_menu_item_range_value.end_value) rescue []
      # end
      rt = @rt
      ## get the barcode from pos check upload if receipt barcode
      if rt.receipt #and rt.receipt.is_receipt_barcode == true
        xml_data = rt.receipt.pos_check_upload.xml_data rescue nil
        check_upload = rt.receipt.pos_check_upload rescue nil
        check_upload = olo_check_order(rt.receipt,check_upload)
        offer_item_parser = Parser::OfferItemsParser.new(check_upload)
        offer_item_parser.xml_data = xml_data
        result_with_qty  = offer_item_parser.find_item_number_and_quantity
        purchased_item_match = []
        unless result_with_qty.blank?
          result_with_qty.each do |res|
            # if the purchase are correct
            if item_numbers.include?(res[:id].to_i)
              purchased_item_match << res
            end
          end
        end

        purchased_item_match = purchased_item_match.flatten

        # calculate the price and multiply it on the quantity
        if offer_rule.is_multiplier?# if multiply
          total = 0
          purchased_item_match.each do |pur|
            total += pur[:price].to_f * pur[:quantity].to_f
          end
          if total == 0
            return nil
          else
            total = multiplier.to_f * total
          end
          return total.to_f
        elsif offer_rule.is_constant_value?
          return offer_rule.multiplier_constant_value
        end
      end


    end
  end

  def reject_item(offer_rule)
    unless offer_rule.days_of_week.blank?
      ## check the days
      issue_date_tmp = @rt.issue_date.blank? ? @rt.created_at : @rt.issue_date

      if !issue_date_tmp.blank?
        issue_date_tmp = issue_date_tmp.in_time_zone('UTC')
        issue_date_tmp = issue_date_tmp.in_time_zone('EST') if @rt.issue_date.blank?
      else
        issue_date_tmp = @receipt.created_at.in_time_zone('EST') rescue Time.zone.now.in_time_zone('EST')
      end

      if issue_date_tmp.blank?
        issue_date_tmp = @receipt.created_at.in_time_zone('EST') rescue Time.zone.now.in_time_zone('EST')
      end
      if offer_rule.include_day_of_week(issue_date_tmp.wday.to_i) == false
        return nil
      end

      is_qualify = included_in_hours?(offer_rule)
      return nil if is_qualify == false
      # start_hour, end_hour = hour_format_date(offer_rule)
      # issue_hour = issue_date_tmp.hour.to_i
      # puts "START HOUR AND END HOUR = #{start_hour} END HOUR = #{end_hour} #{issue_hour}"
      #
      # if !start_hour.blank? and !end_hour.blank?
      #   if issue_hour >= start_hour and end_hour >= issue_hour
      #     puts "in"
      #   else
      #     return nil
      #   end
      # end
    else
      return nil
    end

    ### week days conditions check above

    # case when there are items for offer
    items = offer_rule.offer_rule_menu_items rescue nil
    unless items.blank?
      # gather the menu item,
      g_items = offer_rule.offer_rule_menu_items
      #item_numbers = g_items.pos_menu_items.map(&:item_number)
      item_numbers = collect_item_numbers(g_items,@rt)
      # g_items.each do |g_item|
      #   item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number) rescue []
      #   item_numbers += Array(g_item.general_menu_item.general_menu_item_range_value.start_value .. g_item.general_menu_item.general_menu_item_range_value.end_value) rescue []
      # end
      rt = @rt
      ## get the barcode from pos check upload if receipt barcode
      if rt.receipt #and rt.receipt.is_receipt_barcode == true
        xml_data = rt.receipt.pos_check_upload.xml_data rescue nil
        check_upload = rt.receipt.pos_check_upload rescue nil
        check_upload = olo_check_order(rt.receipt,check_upload)
        # check_upload = rt.receipt.ncr_data_receipt rescue nil
        # if check_upload.blank?
        #   # online order
        #   check_upload = rt.receipt.ncr_online_order.ncr_online_order_detail rescue nil
        # end
        offer_item_parser = Parser::OfferItemsParser.new(check_upload)
        offer_item_parser.xml_data = xml_data
        result_with_qty  = offer_item_parser.find_item_number_and_quantity
        purchased_item_match = []
        unless result_with_qty.blank?
          result_with_qty.each do |res|
            # if the purchase are correct
            if item_numbers.include?(res[:id].to_i)
              purchased_item_match << res
            end
          end
        end

        purchased_item_match = purchased_item_match.flatten
        # item match, reject it
        if purchased_item_match.size > 0
          return true
        else
          return false
        end
      end
    end
  end

  def check_subtotal_criteria(offer_rule)
      sub = @rt.subtotal

      # no limit
      return true if offer_rule.min_amount.to_f == 0.0 and  offer_rule.max_amount.to_f == 0.0

      # no minimum but having maximum
      if offer_rule.min_amount.to_f == 0.0 and offer_rule.max_amount.to_f  != 0.0
        if sub.to_f <= offer_rule.max_amount.to_f
          return true
        else
          ## subtotal is greater than the max amount set
          return false
        end
      end

      # no maximum but minimum added
      if offer_rule.min_amount.to_f != 0.0 and offer_rule.max_amount.to_f  == 0.0
        if sub.to_f >= offer_rule.min_amount.to_f
          return true
        else
          ## subtotal is lesser than the min amount set
          return false
        end
      end


      # maximum and minimum set
      if offer_rule.min_amount.to_f != 0.0 and offer_rule.max_amount.to_f  != 0.0
        if sub.to_f >= offer_rule.min_amount.to_f and sub.to_f <= offer_rule.max_amount.to_f
          return true
        else
          # subtotal does not meet the criteria
          return false
        end
      end
  end

  def calculate_points_multiplier(offer_rule,multiplier)
    week_and_hour_consider(offer_rule,multiplier)
  end

  def calculate_item_point(offer_rule,multiplier)
    return general_item_consider(offer_rule,multiplier)
  end

  def week_and_hour_consider(offer_rule,multiplier)
    unless offer_rule.days_of_week.blank?

      ## same day as the offer rule
      #if self.issue_date.in_time_zone('UTC').wday.to_i == offer_rule.daysOfWeek.to_i
      if offer_rule.include_day_of_week(@rt.issue_date.in_time_zone('UTC').wday.to_i)
        ## time start and end
        if offer_rule.is_multiplier?# if multiply
          tmp_total_points_earned = @dolar_spent * multiplier
        elsif offer_rule.is_constant_value?
          tmp_total_points_earned = offer_rule.multiplier_constant_value
        end
        # start_hour, end_hour = hour_format_date(offer_rule)
        # issue_hour = @rt.issue_date.in_time_zone('UTC').hour.to_i
        is_qualify = included_in_hours?(offer_rule)


          if is_qualify == true
            tmp_total_points_earned = @dolar_spent * multiplier if offer_rule.is_multiplier?# if multiply
            tmp_total_points_earned = offer_rule.multiplier_constant_value if offer_rule.is_constant_value?
          else
            #tmp_total_points_earned = @dolar_spent if offer_rule.is_multiplier?# if multiply
            tmp_total_points_earned = 0 #if offer_rule.is_constant_value?
          end

      else
        ## not same day as the offer
        # tmp_total_points_earned = @dolar_spent if offer_rule.is_multiplier?# if multiply
        # tmp_total_points_earned = offer_rule.multiplier_constant_value if offer_rule.is_constant_value?
        # didnt get nothing
        tmp_total_points_earned = 0
      end
    else
      #tmp_total_points_earned = dolar_spent * multiplier
      #if offer_rule.is_multiplier?# if multiply
      #  tmp_total_points_earned = @dolar_spent * multiplier
      #elsif offer_rule.is_constant_value?
      #  tmp_total_points_earned = offer_rule.multiplier_constant_value
      #end
      tmp_total_points_earned = 0
    end
    return tmp_total_points_earned
  end


  def general_item_consider(offer_rule,multiplier)
    ### week days conditions start
    unless offer_rule.days_of_week.blank?
      ## check the days
      if offer_rule.include_day_of_week(@rt.issue_date.in_time_zone('UTC').wday.to_i) == false
        return nil
      end

      # start_hour, end_hour = hour_format_date(offer_rule)
      # issue_hour = @rt.issue_date.in_time_zone('UTC').hour.to_i
      # puts "START HOUR AND END HOUR = #{start_hour} END HOUR = #{end_hour} #{issue_hour}"
      #
      # if !start_hour.blank? and !end_hour.blank?
      #   if issue_hour >= start_hour and end_hour >= issue_hour
      #     puts "in"
      #   else
      #     return nil
      #   end
      # end
      is_qualify = included_in_hours?(offer_rule)
      return nil if is_qualify == false
    else
      return nil
    end

    ### week days conditions check above

    # case when there are items for offer
    items = offer_rule.offer_rule_menu_items rescue nil
    unless items.blank?
      # gather the menu item,
      g_items = offer_rule.offer_rule_menu_items
      #item_numbers = g_items.pos_menu_items.map(&:item_number)
      item_numbers = collect_item_numbers(g_items,@rt)
      # g_items.each do |g_item|
      #   item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number) rescue []
      #   item_numbers += Array(g_item.general_menu_item.general_menu_item_range_value.start_value .. g_item.general_menu_item.general_menu_item_range_value.end_value) rescue []
      # end
      rt = @rt
      ## get the barcode from pos check upload if receipt barcode
      if rt.receipt #and rt.receipt.is_receipt_barcode == true
        xml_data = rt.receipt.pos_check_upload.xml_data rescue nil
        check_upload = rt.receipt.pos_check_upload rescue nil
        check_upload = olo_check_order(rt.receipt,check_upload)
        offer_item_parser = Parser::OfferItemsParser.new(check_upload)
        offer_item_parser.xml_data = xml_data
        result_with_qty  = offer_item_parser.find_item_number_and_quantity
        purchased_item_match = []
        unless result_with_qty.blank?
          result_with_qty.each do |res|
            # if the purchase are correct
            if item_numbers.include?(res[:id].to_i)
              purchased_item_match << res
            end
          end
        end

        purchased_item_match = purchased_item_match.flatten

        # calculate the price and multiply it on the quantity
        total = 0
        purchased_item_match.each do |pur|
          total += pur[:quantity]
        end
        return nil if total == 0
        if offer_rule.is_multiplier?# if multiply
          return total.to_f *  multiplier.to_f
        elsif offer_rule.is_constant_value?
          return offer_rule.multiplier_constant_value
        end
      end


    end
  end

  def ignore_calculate_item(offer_rule,multiplier)
    unless offer_rule.days_of_week.blank?
      ## check the days
      issue_date_tmp = @rt.issue_date.blank? ? @rt.created_at : @rt.issue_date
      if !issue_date_tmp.blank?
        issue_date_tmp = issue_date_tmp.in_time_zone('UTC')
        issue_date_tmp = issue_date_tmp.in_time_zone('EST') if @rt.issue_date.blank?
      else
        issue_date_tmp = @receipt.created_at.in_time_zone('EST') rescue Time.zone.now.in_time_zone('EST')
      end

      if offer_rule.include_day_of_week(issue_date_tmp.wday.to_i) == false
        return nil
      end

      # start_hour, end_hour = hour_format_date(offer_rule)
      # issue_hour = issue_date_tmp.hour.to_i
      # puts "START HOUR AND END HOUR = #{start_hour} END HOUR = #{end_hour} #{issue_hour}"
      #
      # if !start_hour.blank? and !end_hour.blank?
      #   if issue_hour >= start_hour and end_hour >= issue_hour
      #     puts "in"
      #   else
      #     return nil
      #   end
      # end
      is_qualify = included_in_hours?(offer_rule)
      return nil if is_qualify == false
    else
      return nil
    end

    ### week days conditions check above

    # case when there are items for offer
    items = offer_rule.offer_rule_menu_items rescue nil
    unless items.blank?
      # gather the menu item,
      g_items = offer_rule.offer_rule_menu_items
      #item_numbers = g_items.pos_menu_items.map(&:item_number)
      item_numbers = collect_item_numbers(g_items,@rt)
      # g_items.each do |g_item|
      #   item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number) rescue []
      #   item_numbers += Array(g_item.general_menu_item.general_menu_item_range_value.start_value .. g_item.general_menu_item.general_menu_item_range_value.end_value) rescue []
      # end
      rt = @rt
      ## get the barcode from pos check upload if receipt barcode
      if rt.receipt #and rt.receipt.is_receipt_barcode == true
        xml_data = rt.receipt.pos_check_upload.xml_data rescue nil
        check_upload = rt.receipt.pos_check_upload rescue nil
        check_upload = olo_check_order(rt.receipt,check_upload)
        #check_upload = rt.receipt.ncr_data_receipt rescue nil
        #if check_upload.blank?
          # online order
        #  check_upload = rt.receipt.ncr_online_order.ncr_online_order_detail rescue nil
        #end
        offer_item_parser = Parser::OfferItemsParser.new(check_upload)
        offer_item_parser.xml_data = xml_data
        result_with_qty  = offer_item_parser.find_item_number_and_quantity
        purchased_item_match = []
        unless result_with_qty.blank?
          result_with_qty.each do |res|
            # if the purchase are correct
            if item_numbers.include?(res[:id].to_i)
              purchased_item_match << res
            end
          end
        end

        purchased_item_match = purchased_item_match.flatten

        # calculate the price and multiply it on the quantity
        total = 0
        # it means all the items are on the ignore list
        if purchased_item_match.size == result_with_qty.size
          return "IGNORE"
        else
          if purchased_item_match.size > 0
            purchased_item_match.each do |x|
              puts x[:price].to_f * -1 #* @rt.receipt.chain.points.to_f
              total += x[:price].to_f * -1 #* @rt.receipt.chain.points.to_f
            end
          end
          return total
        end
      end


    end
  end

  def hour_format_date(offer)
    hour_start = nil
    hour_end = nil
    unless offer.time_start.blank?
      a = offer.time_start
      if a.downcase.include?("am")
        hour_start = a.to_i
        hour_start = 0 if a.to_i == 12
      else
        hour_start = a.to_i + 12
      end
    end

    unless offer.time_end.blank?
      a = offer.time_end
      if a.downcase.include?("am")
        hour_end = a.to_i
        hour_start = 0 if a.to_i == 12
      else
        hour_end = a.to_i + 12
      end
    end
    return hour_start, hour_end
  end

  def hour_format_date_utc(offer)
    hour_start = nil
    hour_end = nil
    unless offer.time_start.blank?
      a = offer.time_start
      if a.downcase.include?("am")
        hour_start = a.to_i
        hour_start = 0 if a.to_i == 12
      else
        hour_start = a.to_i + 12
      end
    end

    unless offer.time_end.blank?
      a = offer.time_end
      if a.downcase.include?("am")
        hour_end = a.to_i
        hour_start = 0 if a.to_i == 12
      else
        hour_end = a.to_i + 12
      end
    end

#    hour_start = 1
#    hour_end = 9
    current_time_zone = Time.now.in_time_zone("Eastern Time (US & Canada)")
    if current_time_zone.dst?
      zone = "EDT"
    else
      zone = "EST"
    end
    hour_start = (Time.parse("2010-01-01 #{hour_start}:00 #{zone}")).utc
    hour_end  = (Time.parse("2010-01-01 #{hour_end}:00 #{zone}")).utc

    hours = []
    (hour_start.to_datetime.to_i .. hour_end.to_datetime.to_i).step(1.hour) do |date|
      hours <<  Time.at(date).utc.hour
    end
    return hours
  end

  def included_in_hours?(offer_rule)
    issue_date_tmp = @rt.issue_date.blank? ? @rt.created_at : @rt.issue_date
    if !issue_date_tmp.blank?
      issue_date_tmp = issue_date_tmp.in_time_zone('UTC')
      issue_date_tmp = issue_date_tmp.in_time_zone('EST') if @rt.issue_date.blank?
    else
      issue_date_tmp = @receipt.created_at.in_time_zone('EST') rescue Time.zone.now.in_time_zone('EST')
    end

    hours = hour_format_date_utc(offer_rule)
    issue_hour = issue_date_tmp.hour.to_i
    issue_hour = 1 if issue_hour == 0
    puts "START HOUR AND END HOUR = #{hours} : ISSUE HOUR: #{issue_hour}"

    if !issue_hour.blank?
      if hours.include?(issue_hour)
        return true
      else
        return false
      end
    end
  end

  def olo_check_order(receipt, check_upload)
    begin
      if receipt.is_olo == true
        p "Olo Check order true"
        unless  OloWebhook.where(:receipt_id => receipt.id).blank?
          check_upload = OloWebhook.where(:receipt_id => receipt.id).first
          p check_upload
        end

      end
      return check_upload
    rescue
      return check_upload
    end
  end

  def collect_item_numbers(g_items, rt)
    is_olo = false
    is_olo = rt.receipt.is_olo rescue false
    item_numbers = []

    g_items.each do |g_item|
      if is_olo == false
        item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number) rescue []
        item_numbers += Array(g_item.general_menu_item.general_menu_item_range_value.start_value .. g_item.general_menu_item.general_menu_item_range_value.end_value) rescue []
      else
        item_numbers += g_item.general_menu_item.pos_menu_items.where("item_number_olo IS NOT NULL").map(&:item_number_olo) rescue []
      end
    end

    return item_numbers
  end
end