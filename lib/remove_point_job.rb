class RemovePointJob < Struct.new(:remove_point)

  def perform
    puts "perform"
    p remove_point
    
    success_logs = []
    success_users = []
    error_logs = []
    remove_point.user_ids[:user_ids].each do |user_id|
      begin
        user = User.find(user_id)
        if user.chain.eql?(remove_point.chain)
          user.burn(remove_point.points)
          PointHistory.add_to_history(user.id, "Admin Remove Point", remove_point.points)
          success_logs << { :user_id => user_id, :email => user.email, :log => "Removed #{remove_point.points} point" }
          success_users << user_id
        else
          error_logs << {:user_id => user_id, :email => (user.email rescue ""), :log => "Failed, #{user_id} not included in #{remove_point.chain.name}"}
        end
      rescue => e
        error_logs << {:user_id => user_id, :email => (user.email rescue ""), :log => "Failed, Could not find user with id #{user_id}"}
      end
    end unless remove_point.user_ids.blank?

    p "*"*100
    remove_point.error_logs = error_logs
    remove_point.success_logs = success_logs
    remove_point.save(:validate => false)
    p remove_point.errors

  end

end
