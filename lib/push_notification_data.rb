require 'mandrill'

module PushNotificationData

  class PointNotification

    def self.group_emails_and_device_token_by_locale(user_ids)
      hash_map = {}
      if !user_ids.blank?
        users = User.where("id in (?)", user_ids)
        users.each do |user|
          hash_map[user.locale.id] = {} if hash_map[user.locale.id].blank?
          hash_map[user.locale.id][:emails] = [] if hash_map[user.locale.id][:emails].blank?
          hash_map[user.locale.id][:android_device_tokens] = [] if hash_map[user.locale.id][:android_device_tokens].blank?
          hash_map[user.locale.id][:iphone_device_tokens] = [] if hash_map[user.locale.id][:iphone_device_tokens].blank?
          hash_map[user.locale.id][:emails] << user.email  if user.push_to_email?
          hash_map[user.locale.id][:android_device_tokens] << user.device_token if user.push_to_device? && user.sign_in_device_type.to_s.downcase.eql?("android") && !user.device_token.blank?
          hash_map[user.locale.id][:iphone_device_tokens] << user.device_token if user.push_to_device? &&  user.sign_in_device_type.to_s.downcase.eql?("iphone") && !user.device_token.blank?
        end
        hash_map
      end
    end

    def self.group_message_pn_by_locale(chain)
      hash_map = {}
      chain.locales.each do |locale|
        notification = chain.admin_push_point_notifications.where("locale_id = ?", locale.id).first
        hash_map[locale.id] = {} if hash_map[locale.id].blank?
        hash_map[locale.id]["email_subject"] = notification.blank? ? "" : notification.email_subject
        hash_map[locale.id]["email_content"] = notification.blank? ? "" : notification.email_content
        hash_map[locale.id]["email_content_html"] = notification.blank? ? "" : notification.email_content_html
        hash_map[locale.id]["notification"] = notification.blank? ? "" : notification.notification
      end
      return hash_map
    end

    def self.gcm_push(chain, reg_ids_by_locale, hash_notification, point, image_url = nil)
      p reg_ids_by_locale
      p hash_notification
      reg_ids_by_locale.each do |key, value|
        unless value[:android_device_tokens].blank?
          puts "---Started sending PN---"
          p key
          p value
          p hash_notification[key.to_i]["notification"]
          msg = key.blank? ? "" : hash_notification[key.to_i]["notification"]
          msg = msg.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
          msg = msg.gsub('__','')
          push_to_android(chain, value[:android_device_tokens], (msg % {
              :chain_name => chain.name,
              :point_added => point,
              :receipt_id_link => image_url}))
          puts "---SENT---"
        end
      end
    end

    def self.push_to_android(chain, reg_ids, notification)
      if application = chain.applications.by_device_type(1).first
        return if application.key.blank?
        message = GcmHelper::Message.new
        message.delay_while_idle = true
        message.add_data('alert', notification)
        message.add_data('timestamp', "#{Time.now}")
        #      key="AIzaSyCHAQW9i1vGKNUD83jSsSn8Jfgb2TPFGKc"
        sender = GcmHelper::Sender.new(application.key)
        response = sender.multicast_with_retry(message, reg_ids, 3)
      end
    end

    def self.push_to_iphone2(chain, reg_ids_by_locale, hash_notification, point, image_url=nil)
      if application = chain.applications.by_device_type(2).first
        reg_ids_by_locale.each do |key, value|
          unless value[:iphone_device_tokens].blank?
            msg = key.blank? ? "" : hash_notification[key]["notification"]
            msg = msg.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
            msg = msg.gsub('__','')

            value[:iphone_device_tokens].each do |device_token|
              rapns_app = application.rapns_app
              return if rapns_app.blank?
              n = Rpush::Apns::Notification.new
              n.app = rapns_app
              n.device_token = device_token
              n.alert = msg % {
                  :chain_name => chain.name,
                  :point_added => point,
                  :receipt_id_link => image_url
              }

              n.sound = "1.aiff"
              n.badge = 1
              n.save
            end
          end
        end
      end
    end

    def self.push_email_group(chain, reg_ids_by_locale, hash_notification, point, image_url = nil)
      reg_ids_by_locale.each do |key, value|
        unless value[:emails].blank?
          email_subject = key.blank? ? "" : hash_notification[key]["email_subject"]
          email_content = key.blank? ? "" : hash_notification[key]["email_content"]
          email_content_html = key.blank? ? "" : hash_notification[key]["email_content_html"]
          email_content = email_content.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
          email_content = email_content.gsub('__','')
          email_content_html = email_content_html.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
          email_content_html = email_content_html.gsub('__','')

          value[:emails].each do |email|
            UserMailer.push_point_notice_email(chain, email, email_subject,
                                               (email_content % {
                                                   :chain_name => chain.name,
                                                   :point_added => point, :receipt_id_link => image_url}),
                                               (email_content_html % {
                                                   :chain_name => chain.name,
                                                   :point_added => point, :receipt_id_link => image_url} rescue email_content_html)
            ).deliver
          end
        end
      end
    end

    def self.push_email_group_using_mandrill(chain, reg_ids_by_locale, hash_notification, point, image_url = nil, notif_setting)
      reg_ids_by_locale.each do |key, value|
        unless value[:emails].blank?
          email_subject = key.blank? ? "" : hash_notification[key]["email_subject"]
          email_content = key.blank? ? "" : hash_notification[key]["email_content"]
          email_content_html = key.blank? ? "" : hash_notification[key]["email_content_html"]
          email_content = email_content.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
          email_content = email_content.gsub('__','')
          email_content_html = email_content_html.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
          email_content_html = email_content_html.gsub('__','')

          mandrill_api = notif_setting.apikey rescue nil
          value[:emails].each do |email|
            p "push_email_group_using_mandrill"
            unless mandrill_api.blank?
              begin
                mandrill = Mandrill::API.new(mandrill_api)
                #"ADMIN PUSH POINTS NOTIFICATION"
                template_content = [{"content"=>"example content", "name"=>"example name"}]
                message = {
                    "merge_vars"=> [{"vars"=>[{"content"=>"merge2 content", "name"=>"merge2"}], "rcpt"=>"recipient.email@example.com"}],
                    "to"=> [{"type"=>"to", "email"=> email, "name"=> email}],
                    "global_merge_vars" => [
                        { "name" => "APPNAME", "content" => chain.name },
                        { "name" => "CH_ADDR", "content" => "35 Hugus Aly Suite 300 Pasadena, USA" },
                        { "name" => "CH_PHONE", "content" => "411-111-111" },
                        { "name" => "FNAME", "content" => "" },
                        { "name" => "LNAME", "content" => "" },
                        { "name" => "TEL_NUM", "content" => "" },
                        { "name" => "EMAIL", "content" =>  ""},
                        { "name" => "POINTS", "content" =>  point},
                        { "name" => "NEXT_POINT", "content" =>  ""},
                        { "name" => "PROMO", "content" => "" }
                    ],
                    "tracking_domain"=>nil,
                    "subject"=> email_subject}
                async = false
                ip_pool = nil
                send_at = Time.now.utc
                mandrill.messages.send_template notif_setting.name, template_content, message, async, ip_pool, send_at
              rescue Mandrill::InvalidKeyError => e
                puts "A mandrill error occurred: #{e.class} - #{e.message}"
                UserMailer.push_point_notice_email(chain, email, email_subject,
                                                   (email_content % {
                                                       :chain_name => chain.name,
                                                       :point_added => point, :receipt_id_link => image_url}),
                                                   (email_content_html % {
                                                       :chain_name => chain.name,
                                                       :point_added => point, :receipt_id_link => image_url})
                ).deliver
              end
            else
              UserMailer.push_point_notice_email(chain, email, email_subject,
                                                 (email_content % {
                                                     :chain_name => chain.name,
                                                     :point_added => point, :receipt_id_link => image_url}),
                                                 (email_content_html % {
                                                     :chain_name => chain.name,
                                                     :point_added => point, :receipt_id_link => image_url})
              ).deliver
            end

          end
        end
      end
    end

    def self.set_valid_pushed_for_each_user(success_logs)
      success_logs.each do |success_log|
        user = User.find(success_log[:user_id])
        if user.push_to_email?
          success_log[:email] = "Success"
        else
          #success_log[:email] = "Not Pushed"
          success_log[:email] = "N/A"
        end

        if user.push_to_device? #&& !user.device_token.blank?
          if user.device_token.blank?
            success_log[:notification] = "Failed"
          else
            success_log[:notification] = "Success"
          end
        else
          success_log[:notification] = "N/A"
        end
      end
      success_logs
    end

    def self.set_status_notif_when_reward_notif_not_set(success_logs, group_message_pn)
      success_logs.each do |succ_log|
        user = User.find(succ_log[:user_id])
        notif =  group_message_pn[user.locale_id]
        unless notif.blank?
          if notif["email_content"].blank?
            succ_log[:email] = "not set"
          end
          if notif["notification"].blank?
            succ_log[:notification] = "not set"
          end
        end
      end
      success_logs
    end

  end

  class RewardNotification

    def self.group_emails_and_device_token_by_locale(user_ids)
      hash_map = {}
      if !user_ids.blank?
        users = User.where("id in (?)", user_ids)
        users.each do |user|
          hash_map[user.locale.id] = {} if hash_map[user.locale.id].blank?
          hash_map[user.locale.id][:emails] = [] if hash_map[user.locale.id][:emails].blank?
          hash_map[user.locale.id][:android_device_tokens] = [] if hash_map[user.locale.id][:android_device_tokens].blank?
          hash_map[user.locale.id][:iphone_device_tokens] = [] if hash_map[user.locale.id][:iphone_device_tokens].blank?
          hash_map[user.locale.id][:emails] << user.email  if user.push_to_email?
          hash_map[user.locale.id][:android_device_tokens] << user.device_token if user.push_to_device? && user.sign_in_device_type.to_s.downcase.eql?("android") && !user.device_token.blank?
          hash_map[user.locale.id][:iphone_device_tokens] << user.device_token if user.push_to_device? &&  user.sign_in_device_type.to_s.downcase.eql?("iphone") && !user.device_token.blank?
        end
        hash_map
      end
    end

    def self.group_message_pn_by_locale(reward)
      hash_map = {}
      reward.chain.locales.each do |locale|
        notification = reward.chain.admin_push_reward_notifications.where("locale_id = ?", locale.id).first
        hash_map[locale.id] = {} if hash_map[locale.id].blank?
        hash_map[locale.id]["email_subject"] = notification.blank? ? "" : notification.email_subject
        hash_map[locale.id]["email_content"] = notification.blank? ? "" : notification.email_content
        hash_map[locale.id]["email_content_html"] = notification.blank? ? "" : notification.email_content_html
        hash_map[locale.id]["notification"] = notification.blank? ? "" : notification.notification
      end
      return hash_map
    end

    def self.set_valid_pushed_for_each_user(success_logs)
      success_logs.each do |success_log|
        user = User.find(success_log[:user_id])
        if user.push_to_email?
           success_log[:email] = "Success"
        else
          success_log[:email] = "N/A"
        end

        if user.push_to_device? #&& !user.device_token.blank?
          if user.device_token.blank?
            success_log[:notification] = "Failed"
          else
            success_log[:notification] = "Success"
          end
        else
          success_log[:notification] = "N/A"
        end
      end
      success_logs
    end

    def self.set_status_notif_when_reward_notif_not_set(success_logs, group_message_pn)
      success_logs.each do |succ_log|
        user = User.find(succ_log[:user_id])
        notif =  group_message_pn[user.locale_id]
        unless notif.blank?
          if notif["email_content"].blank?
            succ_log[:email] = "not set"
          end
          if notif["notification"].blank?
            succ_log[:notification] = "not set"
          end
        end
      end
      success_logs
    end

  end


end
