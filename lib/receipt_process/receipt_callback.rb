class ReceiptProcess::ReceiptCallback < Struct.new(:receipt_id)

  def perform
    retry_count = 0
    begin
      receipt = Receipt.find(receipt_id)
      UserMailer.receipt_email(Setting.email.reviewer, receipt).deliver
    rescue
      puts "Retrying::RECEIPTCALLBACK STEP #{retry_count}"
      retry_count = retry_count + 1
      if retry_count <= 3
        retry
      end
    end
  end
end