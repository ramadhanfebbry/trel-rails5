class ReceiptProcess::ReceiptSetIssuedate
  # set receipt date based on each check

  # sample
  # ReceiptProcess::ReceiptSetIssuedate.set_date(1,params)
  # ReceiptProcess::ReceiptSetIssuedate.setdatetime(1,Time.zone.now)

  def self.set_date(receipt_id,params)
    rd = ReceiptDetail.where(:receipt_id => receipt_id)
    if rd.size > 0
      rd.first.update_attributes(:receipt_id => receipt_id,
                           :issue_date => params[:issue_date],
                           :issue_time =>params[:issue_time],
                           :timezone => params[:timezone])
    else
      ReceiptDetail.create(
          :receipt_id => receipt_id,
          :issue_date => params[:issue_date],
          :issue_time =>params[:issue_time],
          :timezone => params[:timezone]
      )
    end
  end

  def self.setdatetime(receipt_id, datetime)
    params ={}
    params[:issue_date] = datetime.to_date
    params[:issue_time] = datetime.strftime('%H:%M:%S')
    params[:timezone] = datetime.zone
    set_date(receipt_id,params)
  end
end