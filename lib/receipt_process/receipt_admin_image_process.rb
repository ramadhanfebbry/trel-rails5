class ReceiptProcess::ReceiptAdminImageProcess < Struct.new(:receipt, :params)

  def perform
    unless params[:restaurant].blank?
      ru = RestaurantUser.new(:user_id => params[:receipt][:user_id], :restaurant_id => params[:restaurant])
      ru.save
      EmailMarketing.update_email_marketing_location((User.find(params[:receipt][:user_id]) rescue nil), params[:restaurant])
      Chain.update_user_fishbowl_location((User.find(params[:receipt][:user_id]) rescue nil), params[:restaurant])
    end
    receipt.push_referral_code_reward ## push referral reward
    if receipt.status == Receipt::STATUS[:APPROVED]
      receipt.push_one_time_reward(receipt.user)
    end
    receipt.sqs_send_message(params[:appkey])
  end

end
