class ReceiptProcess::ReceiptTransactionCallback < Struct.new(:record)

  def perform
    retry_count = 0
    begin
      #record = ReceiptTransaction.find(receipt_transaction_id)
      receipt = record.receipt
      status = ReceiptStatus.where("receipt_transaction_id = ? and status = ?", record.id, record.status).last
      status.update_attributes(:delayed_status => ReceiptStatus::DJ_STATUS[:ONPROGRESS])
      ## for rejected receipt
      if Setting.email.turned_on and record.status.eql?(4) and !record.reject_reason_id.blank?
        user, receipt, reject_reason = setup_params_for_reject_receipt(record)

        ## get the receipt transactions, handle if the duplicate receipts (approved first) and then rejected after
        # that, dont send an email reject, because the receipt is already approved
        #list_transactions = record.receipt.receipt_transactions

        list_transactions = ReceiptTransaction.where(
            "receipt_id != ? AND DATE(issue_date) = ? AND
          restaurant_id = ? AND receipt_number = ? AND status = ?",
            receipt.id, record.issue_date.to_date,
            record.restaurant_id, record.receipt_number, Receipt::STATUS[:APPROVED]
        ) rescue []

        send_reject_email = true
        from_same_user = true
        ## if this receipt is already been approved, and then user send the receipt again
        ## this will rejected ( duplicate ) and we should not send the user an email for this reject
        ## unless the receipt is come from another user
        unless list_transactions.blank? ## there are record already approved,
          send_reject_email = false
          ## if the transactions is came from different user, then we should send them email
          from_same_user = false if !list_transactions.map(&:receipt).map(&:user_id).include?(receipt.user_id)
        end
        ####

        if Setting.email.delayed and user.push_to_email?
          user.reject_receipt_email(receipt, reject_reason, true) if send_reject_email or !from_same_user
        elsif user.push_to_email?
          user.reject_receipt_email(receipt, reject_reason, false) if send_reject_email or !from_same_user
        end
      end


      ## insert to restaurant user if user has submit one receipt to restaurant"
      puts "*************************************** Restaurant User process ************************"
      RestaurantUser.update_member(record)
      puts "*************************************** END Restaurant User process ************************"

      if record.status.eql?(Receipt::STATUS[:APPROVED])
        puts "---------------------- start observer receipt transaction ---------------------"

        ## move to dj
        # if record.updated_manually
        #   receipt_xml_data = Receipt.get_associated_xml_receipt_data(receipt)
        #   if receipt_xml_data
        #     ocr_xml = OcrXml.new(receipt_xml_data)
        #     record.line_items = ocr_xml.line_items
        #   end
        # end

        record.save_line_items


        ## move to dj
        puts "--- 5. update day between visit record"
        DayVisit.update_day_visit(record) ## update day between visit record
        puts "-- end update day between visit record"
        puts "*****************************************"

        puts "--- 6. average chart table process"

        # comment out , since it s make process heavier
        # begin
        #   #AverageChartTable.update_average(record)
        # rescue => e
        #   #puts "average chart table update #{e.inspect}"
        # end
        puts "-- end average chart table"
        #puts "--- 7. activity summary process"
        #ActivitySummary.update_data(record) rescue nil
        puts "*****************************************"

        puts "---------------------- after save observer receipt transaction ---------------------"

        puts "record.subtotal #{record.subtotal}"
        oj = OwnerJob::RestaurantUserJob.new(receipt.user_id,
                                             record.restaurant_id,
                                             record.created_at,
                                             record.subtotal,
                                             receipt.get_date
        )
        oj.update_restaurant_user
      end

      # move to dj
      if record.status.eql?(Receipt::STATUS[:OCRED])
        Receipt.store_ocred_receipt_xml_data_to_s3(receipt, record.xml_data) unless record.xml_data.blank?
      end

    rescue
      retry_count = retry_count + 1
      if retry_count <= 3
        retry
      end
    end
  end


  def setup_params_for_reject_receipt(receipt_transaction)
    reject_reason = receipt_transaction.reject_reason
    user = receipt_transaction.receipt.user
    locale = user.locale
    reject_reason_content = get_reject_reason_by_locale(reject_reason.reason_key, locale.key)
    return user, receipt_transaction.receipt, reject_reason_content
  end

  def get_reject_reason_by_locale(key, locale = "en")
    REDIS.get "#{locale}.receipt_reject_reason.#{key}" unless key.blank?
  end

  def success(job)
    status = ReceiptStatus.where("receipt_transaction_id = ? and status = ?", record.id, record.status).last
    status.update_attributes(:delayed_status => ReceiptStatus::DJ_STATUS[:COMPLETED], :delayed_id => job.id)
  end

  def error(job, exception)
    status = ReceiptStatus.where("receipt_transaction_id = ? and status = ?", record.id, record.status).last
    status.update_attributes(:delayed_status => ReceiptStatus::DJ_STATUS[:FAILED], :delayed_id => job.id)
  end

end