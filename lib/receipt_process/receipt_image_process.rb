class ReceiptProcess::ReceiptImageProcess < Struct.new(:receipt, :receipt_transaction, :params, :user)
  def perform
  #if receipt.save and receipt_transaction.save
    unless params[:restaurant].blank?
      ru = RestaurantUser.new(:user_id => user.id, :restaurant_id => params[:restaurant])
      ru.save
      EmailMarketing.update_email_marketing_location(user, params[:restaurant])
      Chain.update_user_fishbowl_location(user, params[:restaurant])
    end
    receipt.sqs_send_message(params[:appkey])
  #end
  end
end