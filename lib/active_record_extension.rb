module ActiveRecordExtension

  extend ActiveSupport::Concern

  # add your instance methods here

  def verify_readonly_attribute(name)
    raise ActiveRecordError, "#{name} is marked as readonly" if self.class.readonly_attributes.include?(name)
  end

  def update_columns(attributes)
    raise ActiveRecordError, "cannot update on a new record object" unless persisted?

    attributes.each_key do |key|
      verify_readonly_attribute(key.to_s)
    end

    updated_count = self.class.unscoped.where(self.class.primary_key => id).update_all(attributes)

    attributes.each do |k, v|
      _write_attribute(k, v)
    end

    updated_count == 1
  end

end

# include the extension
ActiveRecord::Base.send(:include, ActiveRecordExtension)