# encoding: utf-8
require 'uri'
require 'fuzzystringmatch'
require 'mail'
module Additions
  module Ocr
    extend ActiveSupport::Concern

    module ClassMethods
      def create_ocr_transaction_from_sqs
        #begin
        p "-----------  scheduler job read xml data from sqs ------------"
        sqs = AWS::SQS.new(:access_key_id => Setting.sqs.access_key_id, :secret_access_key => Setting.sqs.secret_access_key)
        output_queue = sqs.queues.named(Setting.sqs.ocr_output_queue)
        p "total data in queue = #{output_queue.visible_messages}"
        message_collection = []
        continue_read_message = true
        while continue_read_message == true do
          message = output_queue.receive_message
          if message.blank?
            continue_read_message = false
          else
            message_collection << message.body.to_s
            if message_collection.size == 10
              p "---- 10 xml data added into delayed job -----"
              Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
              message_collection = []
            end
            message.delete
          end
        end
        unless message_collection.blank?
          p "#{message_collection.count} xml data added into delayed job"
          Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
        end

        #output_queue.poll(:idle_timeout => 30) do |message|
        #  message_collection << message.body.to_s
        #  if message_collection.size == 10
        #    p "---- 10 xml data added into delayed job -----"
        #    Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
        #    message_collection = []
        #  end
        #  message.delete
        #  #begin
        #  #  xml_format = ActiveSupport::JSON.decode(message.body.to_s)
        #  #  read_xml_response_ocr(xml_format)
        #  #rescue => e
        #  #  receipt = Receipt.find(xml_format["ReceiptId"]) rescue nil
        #  #  [Setting.email.sys_admin, Setting.email.reviewer].each do |recipient|
        #  #    if recipient.eql?(Setting.email.reviewer)
        #  #      ReceiptMailer.receipt_validation_failed(recipient, e.message, receipt).deliver
        #  #    else
        #  #      ReceiptMailer.receipt_validation_failed(recipient, e.message, receipt, message.body.to_s ).deliver
        #  #    end
        #  #  end
        #  #  message.delete
        #  #end
        #end
        #unless message_collection.blank?
        #  p "#{message_collection.count} xml data added into delayed job"
        #  Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
        #end
        p "------- end scheduler job read xml data from sqs ------------"
      end

      def read_from_xml_file_s3_and_store_to_mongo
        s3 = AWS::S3.new(:access_key_id => Setting.storage.s3_access_key_id, :secret_access_key => Setting.storage.s3_secret_access_key)
        object = s3.buckets[Setting.storage.ocr_archived_s3_bucket]
        object.objects.to_a.each do |s3_obj|
          if s3_obj.content_type == "application/xml"
             begin
               receipt_id = s3_obj.key.split("/").last.split("_").first
               url_file = s3_obj.url_for(:get, { :expires => 20.minutes.from_now, :secure => true }).to_s
               file_read = open(url_file).read
               receipt = Receipt.find receipt_id
               last_transaction = receipt.last_transaction
               if last_transaction && last_transaction.status == 3
                 ocr_xml = OcrXml.new(file_read)
                 puts "save to mongo db --- receipt #{receipt_id}"
                 last_transaction.line_items = ocr_xml.line_items
                 last_transaction.save_line_items
               else
                 puts "failed save to  mongo db -- receipt #{receipt_id}"
               end
             rescue
               puts "error reading xml for receipt #{receipt_id}"
             end
          end
        end
      end

      def read_data_xml_from_receipt_object(year, month)
        s3 = AWS::S3.new(:access_key_id => Setting.storage.s3_access_key_id, :secret_access_key => Setting.storage.s3_secret_access_key)
        bucket = s3.buckets['ocr-archive']
        date_selected = Date.parse("#{year}-#{month}-10")
        start_date = date_selected.beginning_of_month
        end_date = date_selected.end_of_month
        Receipt.where("id >= ? AND status = ? AND created_at BETWEEN ? AND ?", 35729,3, start_date, end_date).each do |receipt|
          file_receipt = File.basename(URI.parse(receipt.image.url).path)
          file_name = file_receipt.split(".").first
          folder_selected = receipt.created_at.utc.strftime("%Y-%m-%d")
          file = "#{folder_selected}/#{file_name}.xml"
          s3_file_xml = bucket.objects[file]
          if s3_file_xml.exists?
            last_transaction = receipt.last_transaction
            if last_transaction && last_transaction.status == 3
              ocr_xml = OcrXml.new(s3_file_xml.read)
              puts "save to mongo db --- receipt #{receipt.id}"
              last_transaction.line_items = ocr_xml.line_items
              last_transaction.save_line_items
            else
              puts "failed save to  mongo db -- receipt #{receipt.id}"
            end
          end
        end
      end

      def process_pending_ocr_receipt_from_xml_data_archived_s3(date)
        s3 = AWS::S3.new(:access_key_id => Setting.storage.s3_access_key_id, :secret_access_key => Setting.storage.s3_secret_access_key)
        bucket = s3.buckets['ocr-archive']
        message_collection = []
        Receipt.where("status = ? AND DATE(created_at) = ?", 5, date).limit(50).each do |receipt|
          p "RECEIPT ID EXECUTING ... #{receipt.id}"
          file_receipt = File.basename(URI.parse(receipt.image.url).path)
          file_name = file_receipt.split(".").first
          folder_selected = receipt.created_at.utc.strftime("%Y-%m-%d")
          file = "#{folder_selected}/#{file_name}.xml"
          s3_file_xml = bucket.objects[file]
          if s3_file_xml.exists?
             message = {
                 Xml: s3_file_xml.read.force_encoding("utf-8"),
                 ChainId:  receipt.chain_id,
                 ReceiptId: receipt.id
             }
             message = message.to_json
             p message
             p "---"*9
             message_collection << message
             if message_collection.size == 10
               p "---- 10 xml data added into delayed job -----"
               Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
               message_collection = []
             end
          end
        end
        unless message_collection.blank?
          p "#{message_collection.count} xml data added into delayed job"
          Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
        end
      end

      def process_pending_ocr_receipt_from_xml_data_archived_db
        message_collection = []
        sql_date =  "TIMEZONE('UTC', receipts.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}'"
        time_now = Time.current
        end_time = time_now - 8.hours
        start_time = Time.now - 7.days
        Receipt.where("receipts.status = ? AND #{sql_date} BETWEEN ? AND ?", 5, start_time, end_time).each do |receipt|
          p "RECEIPT ID EXECUTING ... #{receipt.id}"
          archived_xml = ArchivedXmlReceipt.where(:receipt_id => receipt.id).first
          unless  archived_xml.blank?
            message = {
                Xml: archived_xml.xml_data_receipt.force_encoding("utf-8"),
                ChainId:  receipt.chain_id,
                ReceiptId: receipt.id
            }
            message = message.to_json
            p message
            p "---"*9
            message_collection << message
            if message_collection.size == 10
              p "---- 10 xml data added into delayed job -----"
              Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
              message_collection = []
            end
          else
            chain = receipt.chain
            user = receipt.user
            receipt.sqs_send_message(user.chain_app_key)
          end
        end
        unless message_collection.blank?
          p "#{message_collection.count} xml data added into delayed job"
          Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
        end
      end

      def store_ocred_receipt_xml_data_to_s3(receipt, xml)
        s3 = AWS::S3.new(:access_key_id => Setting.storage.s3_access_key_id, :secret_access_key => Setting.storage.s3_secret_access_key)
        bucket = s3.buckets[Setting.storage.ocred_xml_s3_bucket]
        unless bucket.exists?
          bucket = s3.buckets.create(Setting.storage.ocred_xml_s3_bucket)
        end
        new_file = bucket.objects["ocred_xml_#{receipt.id}.xml"]
        new_file.write(xml)
      end

      def process_pending_ocr_receipt_from_xml_data_archived_db
        message_collection = []
        Receipt.where("status = ?", 5).each do |receipt|
          p "RECEIPT ID EXECUTING ... #{receipt.id}"
          archived_xml = ArchivedXmlReceipt.where(:receipt_id => receipt.id).first
          unless  archived_xml.blank?
            message = {
                Xml: archived_xml.xml_data_receipt.force_encoding("utf-8"),
                ChainId:  receipt.chain_id,
                ReceiptId: receipt.id
            }
            message = message.to_json
            p message
            p "---"*9
            message_collection << message
            if message_collection.size == 10
              p "---- 10 xml data added into delayed job -----"
              Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
              message_collection = []
            end
          end
        end
        unless message_collection.blank?
          p "#{message_collection.count} xml data added into delayed job"
          Delayed::Job.enqueue(OcrReceiptJob.new(message_collection))
        end
      end

      def get_associated_xml_receipt_data(receipt)
        s3 = AWS::S3.new(:access_key_id => Setting.storage.s3_access_key_id, :secret_access_key => Setting.storage.s3_secret_access_key)
        bucket = s3.buckets[Setting.storage.ocred_xml_s3_bucket]
        if bucket.exists?
          file = s3.buckets[bucket.name].objects["ocred_xml_#{receipt.id}.xml"]
          if file.exists?
            return file.read
          else
            nil
          end
        end
        nil
      end

      def read_xml_response_ocr(message_hash)
        ActiveRecord::Base.transaction do
          ocr_xml = OcrXml.new(message_hash["Xml"])
          p "START PROCESS XML FROM RECEIPT ID #{message_hash["ReceiptId"]}"
          receipt = Receipt.find(message_hash["ReceiptId"])
          chain = Chain.find(message_hash["ChainId"]) rescue nil
          last_transaction = receipt.last_transaction

          send_email_due_to_invalid_subtotal = false

          receipt_transaction = ReceiptTransaction.new
          receipt_transaction.xml_data =  message_hash["Xml"]
          receipt_transaction.restaurant_offer_id = last_transaction.restaurant_offer_id
          receipt_transaction.subtotal = ocr_xml.sub_total
          receipt_transaction.tax = ocr_xml.tax
          receipt_restaurant = receipt_transaction.restaurant

          valid_total = ocr_xml.valid_total?
          valid_date = ocr_xml.valid_date?(receipt)
          chain ||= receipt.chain
          unless chain.blank?
            if chain.id.eql?(Setting.chain_id.aroma)
              valid_time = ocr_xml.custom_valid_time?
            elsif chain.id.eql?(Setting.chain_id.roti)
              valid_time = ocr_xml.roti_valid_time?
            else
              valid_time = ocr_xml.valid_time?(receipt_restaurant.restaurant_hours)
            end
          else
            valid_time = ocr_xml.valid_time?(receipt_restaurant.restaurant_hours)
          end


          p "----------- DATA GOT FROM XML -----------------"
          p "this is chain_name : #{ocr_xml.chain_name}"
          p "this is receipt number : #{ocr_xml.receipt_number.gsub("—", "-").gsub("â€”", "-")}"
          p "this is receipt tax : #{ocr_xml.tax}"
          p "this is receipt subtotal : #{ocr_xml.sub_total}"
          p "this is receipt total : #{ocr_xml.total}"
          p "this is original time : #{ocr_xml.receipt_time}"
          p "this is time converted : #{ocr_xml.converted_time}"
          p "this is original date : #{ocr_xml.receipt_date}"
          p "this is original date : #{ocr_xml.converted_date}"
          p "Full Address is : #{ocr_xml.full_address}"
          p "Order Type is : #{ocr_xml.order_type}"

          location = get_location(receipt.chain, ocr_xml.address_line.strip)
          p "location from name comparation = #{location}"
          if location.blank?  && !ocr_xml.address_line.strip.blank?
            location = (Restaurant.active.within(1, :units => :km, :origin => ocr_xml.address_line.strip).where("chain_id = ?", receipt.chain_id).first rescue nil)
            p "location from geocoder = #{location}"
          end
          if location.blank?
            location = (receipt.last_transaction.restaurant_offer.restaurant rescue nil) if location.blank?
            p "location from restaurant transaction = #{location}"
          end
          p "location selected is"
          p location

          if !valid_total && ocr_xml.total > 0 && ocr_xml.sub_total > 0 && location && location.rest_tax > 0.0
            p " total not valid check rest tax process --------------"
            p ocr_xml.sub_total.to_f
            p ocr_xml.tax
            tax_sub_total = ocr_xml.sub_total.to_f.eql?(0.0) ? 0.0 : (ocr_xml.tax.to_f/ocr_xml.sub_total.to_f).round(5)
            p tax_sub_total
            p location.rest_tax
            if ((location.rest_tax * 0.98) <= tax_sub_total) and (tax_sub_total <= (location.rest_tax * 1.02))
              valid_total = true
              ocr_xml.error_messages.delete(:total)
            end
          elsif valid_total && location
            p "total is valid so update the rest tax for restaurant"
            p ocr_xml.sub_total.to_f
            p ocr_xml.tax
            tax_sub_total = ocr_xml.sub_total.to_f.eql?(0.0) ? 0.0 : (ocr_xml.tax.to_f/ocr_xml.sub_total.to_f).round(5)
            p tax_sub_total
            p location.update_attribute(:rest_tax, tax_sub_total)
            p location.errors
          end

          if valid_total and valid_date and valid_time
            if unique_receipt?(ocr_xml)
              valid_receipt =  true
              if !chain.blank? and chain.today_max_user_limit?(receipt, ocr_xml.converted_date)
                receipt_transaction.status = Receipt::STATUS[:OCRED]
                receipt.status = Receipt::STATUS[:OCRED]
                ocr_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
                valid_receipt = false
              end
              if chain.max_subtotal > 0 && chain.max_subtotal < receipt_transaction.subtotal.to_f
                receipt_transaction.status = Receipt::STATUS[:OCRED]
                receipt.status = Receipt::STATUS[:OCRED]
                ocr_xml.error_messages[:max_subtotal] = "MAX SUBTOTAL LIMIT CROSSED"
                valid_receipt = false
                send_email_due_to_invalid_subtotal = true
              end
              if valid_receipt
                receipt_transaction.status = Receipt::STATUS[:APPROVED]
                receipt.status = Receipt::STATUS[:APPROVED]
              end
            else
              if location
                if unique_receipt?(ocr_xml, location)
                  valid_receipt = true
                  if !chain.blank? and chain.today_max_user_limit?(receipt, ocr_xml.converted_date)
                    receipt_transaction.status = Receipt::STATUS[:OCRED]
                    receipt.status = Receipt::STATUS[:OCRED]
                    ocr_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
                    valid_receipt = false
                  end
                  if chain.max_subtotal > 0 && chain.max_subtotal < receipt_transaction.subtotal.to_f
                    receipt_transaction.status = Receipt::STATUS[:OCRED]
                    receipt.status = Receipt::STATUS[:OCRED]
                    ocr_xml.error_messages[:max_subtotal] = "MAX SUBTOTAL LIMIT CROSSED"
                    valid_receipt = false
                    send_email_due_to_invalid_subtotal = true
                  end
                  if valid_receipt
                    receipt_transaction.status = Receipt::STATUS[:APPROVED]
                    receipt.status = Receipt::STATUS[:APPROVED]
                  end
                elsif ocr_xml.valid_receipt_number?((chain and chain.id.eql?(Setting.chain_id.zoes) ? false : true))
                  if unique_receipt?(ocr_xml, location, ocr_xml.receipt_number)
                    valid_receipt = true
                    if !chain.blank? and chain.today_max_user_limit?(receipt, ocr_xml.converted_date)
                      receipt_transaction.status = Receipt::STATUS[:OCRED]
                      receipt.status = Receipt::STATUS[:OCRED]
                      ocr_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
                      valid_receipt = false
                    end
                    if chain.max_subtotal > 0 && chain.max_subtotal < receipt_transaction.subtotal.to_f
                      receipt_transaction.status = Receipt::STATUS[:OCRED]
                      receipt.status = Receipt::STATUS[:OCRED]
                      ocr_xml.error_messages[:max_subtotal] = "MAX SUBTOTAL LIMIT CROSSED"
                      valid_receipt = false
                      send_email_due_to_invalid_subtotal = true
                    end
                    if valid_receipt
                      receipt_transaction.status = Receipt::STATUS[:APPROVED]
                      receipt.status = Receipt::STATUS[:APPROVED]
                    end
                  else
                    receipt_transaction.status = Receipt::STATUS[:OCRED]
                    receipt.status = Receipt::STATUS[:OCRED]
                    ocr_xml.error_messages[:receipt] = "Receipt should be uniq, this receipt have submitted before"
                    ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, ocr_xml.error_messages, receipt).deliver
                  end
                else
                  receipt_transaction.status = Receipt::STATUS[:OCRED]
                  receipt.status = Receipt::STATUS[:OCRED]
                  p "-----ocred 1------"
                  ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, ocr_xml.error_messages, receipt).deliver
                  p "--- sent ocred mail 1---"
                end
              else
                ocr_xml.error_messages[:restaurant] = "Restaurant/Location is invalid."
                receipt_transaction.status = Receipt::STATUS[:OCRED]
                receipt.status = Receipt::STATUS[:OCRED]
                p "-----ocred 2------"
                ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, ocr_xml.error_messages, receipt).deliver
                p "--- sent ocred mail 2---"
              end
            end
          else
            receipt_transaction.status = Receipt::STATUS[:OCRED]
            receipt.status = Receipt::STATUS[:OCRED]
            p "-----ocred 3------"
            ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, ocr_xml.error_messages, receipt).deliver
            p "--- sent ocred mail 3---"
          end
          receipt_transaction.instructions = ocr_xml.error_messages
          receipt_transaction.receipt_number = ocr_xml.receipt_number
          receipt_transaction.card_number = ocr_xml.card_number
          receipt_transaction.cashier = ocr_xml.cashier_name
          receipt_transaction.server_name = ocr_xml.server_name
          receipt_transaction.payment_method = ocr_xml.payment_method_type
          receipt_transaction.card_number = ocr_xml.card_number
          receipt_transaction.total_payment = ocr_xml.amount_paid
          receipt_transaction.issue_date = ocr_xml.converted_date
          receipt_transaction.time_stamp = (ocr_xml.converted_time.strip rescue nil)
          receipt_transaction.receipt_date = ocr_xml.converted_date
          receipt_transaction.receipt_time = (ocr_xml.converted_time.strip rescue nil)
          receipt_transaction.restaurant_id = location ? location.id : nil
          receipt_transaction.admin_id = 1
          receipt_transaction.line_items = ocr_xml.line_items
          receipt_transaction.receipt_id = receipt.id
          p "this is receipt transaction object"
          p receipt_transaction
          if receipt_transaction.save
            p "receipt transaction saved"
            receipt.set_order_type(ocr_xml.order_type)
            p "----- receipt update process ----"
            receipt.save
            p receipt.errors
          end
          ReceiptMailer.receipt_subtotal_maxed(Setting.email.reviewer, receipt).deliver if send_email_due_to_invalid_subtotal
          unless receipt_transaction.restaurant_id.blank?
            ru = RestaurantUser.new(:user_id => receipt.user_id, :restaurant_id => receipt_transaction.restaurant_id)
            ru.save
            p ru.errors
          end
          p "----------------  END PROCESS XML FROM RECEIPT ID #{message_hash["ReceiptId"]} *************************"
        end
      end

      def unique_receipt?(ocr_xml, location = nil, receipt_number = nil)
        if location.blank? and receipt_number.blank?
          receipt_transactions = ReceiptTransaction.where("subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND status = ?", ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, Receipt::STATUS[:APPROVED])
          return true if receipt_transactions.blank?
          return false
        elsif location and receipt_number.blank?
          receipt_transactions = ReceiptTransaction.where("subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ? AND status = ?", ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id, Receipt::STATUS[:APPROVED])
          return true if receipt_transactions.blank?
          return false
        elsif receipt_number
          receipt_transactions = ReceiptTransaction.where("subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ? AND receipt_number = ? AND status = ?", ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id, receipt_number, Receipt::STATUS[:APPROVED])
          return true if receipt_transactions.blank?
          return false
        else
          return false
        end
      end

      def get_location(chain, address)
        restaurants_distances = []
        jarow = FuzzyStringMatch::JaroWinkler.create(:native)
        chain.restaurants.active.each do |restaurant|
          distance = jarow.getDistance(restaurant.address, address)
          if distance >= 0.7
            restaurants_distances << {:restaurant => restaurant, :distance => distance}
          end
        end
        sort_by_distance = restaurants_distances.sort{|a, b| b[:distance] <=> a[:distance]}
        return (sort_by_distance.first[:restaurant] rescue nil)
      end


      def test_send_message_to_sqs(chainId, userId, receiptId, xml)
        # begin
        p "-------- send message to SQS Amazon-----------"
        message = {receiptId: receiptId, userId: userId, chainId: chainId, xml: EscapeUtils.escape_url(xml) }
        sqs = AWS::SQS.new(:access_key_id => Setting.sqs.access_key_id, :secret_access_key => Setting.sqs.secret_access_key)
        output_queue = sqs.queues.named(Setting.sqs.ocr_output_queue)
        output_queue.send_message(message)
        p "---------- sent-----------------"
        # rescue
        # p "Message sent failed"
        # end
      end

      def x
        %q[<?xml version=\"1.0\" encoding=\"UTF-8\"?><form:Documents xmlns:form=\"http:\/\/www.abbyy.com\/FlexiCapture\/Schemas\/Export\/FormData.xsd\" xmlns:addData=\"http:\/\/www.abbyy.com\/FlexiCapture\/Schemas\/Export\/AdditionalFormData.xsd\"><_Aroma_03_09_27_2012:_Aroma_03_09_27_2012 xmlns:_Aroma_03_09_27_2012=\"http:\/\/www.abbyy.com\/FlexiCapture\/Schemas\/Export\/Aroma_03_09_27_2012.xsd\"><_Document_Section_1><_ChainName>AROMA ESPRESSO BAR<\/_ChainName><_ReceiptNumber \/><_TableNumber>2<\/_TableNumber><_NumberPeopleTable>1<\/_NumberPeopleTable><_ServerName>v\\MICHAEL Tâ€¨M !â– ^ rvT Â»IT t lA<\/_ServerName><_ServerNumber \/><_ServerCheckNumber>160<\/_ServerCheckNumber><_ReceiptDate>10\/09\/12<\/_ReceiptDate><_ReceiptTime>9:24o<\/_ReceiptTime><_CashierNumber>T7<\/_CashierNumber><_CashierName \/><_OrderType>DINING<\/_OrderType><_SubTotal>8.750<\/_SubTotal><_Tax>0.780<\/_Tax><_Total>9.530<\/_Total><_Gratuity \/><_AddressLine1>145 GREENE STREET<\/_AddressLine1><_AddressLine2 \/><_City>NEW YORK,<\/_City><_State>NY<\/_State><_Zip>10012<\/_Zip><_Phone>212-533-1094<\/_Phone><_PaidMethod>MASTERCARD<\/_PaidMethod><_AmountPaid>9.53<\/_AmountPaid><_AmountChangeDue \/><_CreditCardName>MASTERCARD<\/_CreditCardName><_CreditCardNumber>7515<\/_CreditCardNumber><_DiscountCode \/><_DiscountAmount \/><_DiscountPercent \/><_LineItems><_Quantity \/><_Description>Bureka Treat<\/_Description><_Amount>6.25<\/_Amount><\/_LineItems><_LineItems><_Quantity \/><_Description>Es. Macciato<\/_Description><_Amount>2.50<\/_Amount><\/_LineItems><_LineItems><_Quantity \/><_Description>RAF<\/_Description><_Amount>0.00<\/_Amount><\/_LineItems><\/_Document_Section_1><\/_Aroma_03_09_27_2012:_Aroma_03_09_27_2012><\/form:Documents>]
      end

      def check_no_transaction_receipt
        Receipt.all.each do |receipt|
          last_transaction = receipt.last_transaction
          if last_transaction.is_received?
            if ((Time.now - 1.hours) - last_transaction.created_at)/3600 > 1
              ReceiptMailer.no_transaction_receipt(Setting.email.reviewer, last_transaction.receipt).deliver
            end
          end
        end
      end

      def check_sqs_output_queue_messages
        sqs = AWS::SQS.new(:access_key_id => Setting.sqs.access_key_id, :secret_access_key => Setting.sqs.secret_access_key)
        output_queue = sqs.queues.named(Setting.sqs.ocr_output_queue)
        if output_queue.visible_messages >= Setting.ocr.max_message_to_notify_down
          Mail.defaults do
            delivery_method :smtp, {
                :address => Setting.smtp.host,
                :port => Setting.smtp.port,
                :user_name => Setting.smtp.username,
                :password => Setting.smtp.password,
                :authentication => 'plain',
                :enable_starttls_auto => true
            }
          end
          mail = Mail.new do
            from Setting.email.default_from
            to "support@relevantmobile.com"
            subject "OCR Automated Module Down Notification"
            html_part do
              content_type 'text/html; charset=UTF-8'
              body "Something goes wrong with OCR Automated Module. There are a lot receipts in output SQS Queue.".html_safe
            end
          end
          mail.deliver!
        end
      end

      def check_sqs_input_queue_messages
        sqs = AWS::SQS.new(:access_key_id => Setting.sqs.access_key_id, :secret_access_key => Setting.sqs.secret_access_key)
        input_queue = sqs.queues.named(Setting.sqs.ocr_input_queue)
        if input_queue.visible_messages >=  Setting.ocr.max_message_ocr_service_down
          Mail.defaults do
            delivery_method :smtp, {
                :address => Setting.smtp.host,
                :port => Setting.smtp.port,
                :user_name => Setting.smtp.username,
                :password => Setting.smtp.password,
                :authentication => 'plain',
                :enable_starttls_auto => true
            }
          end
          mail = Mail.new do
            from Setting.email.default_from
            to "sr@dailygobble.com, xd@dailygobble.com, dc@dailygobble.com, sm@dailygobble.com, bb@dailygobble.com, tb@dailygobble.com"
            #to " sm@dailygobble.com, syafiklijtk04@gmail.com"
            subject "OCR Service Down Notification"
            html_part do
              content_type 'text/html; charset=UTF-8'
              body "Something goes wrong with OCR Service. Please restart the server to get it up. There are a lot receipts in input SQS Queue.".html_safe
            end
          end
          mail.deliver!
        end
      end
    end

    module InstanceMethods
      def sqs_send_message(appkey)
        application_key = ApplicationKey.where(:appkey => appkey).first
        p "------appkey = #{appkey}----------"
        if application_key and application_key.ocr_send_message
          begin
            p "-------- send message to SQS Amazon-----------"
            content_message = {
                Url: self.image.url,
                AppKey: appkey,
                FileName: (File.basename(URI.parse(self.image.url).path) rescue nil),
                ChainId: "#{self.chain_id}",
                RestaurantId: "#{self.last_transaction.restaurant_offer.restaurant_id}",
                ReceiptId: "#{self.id}",
                OfferId: "#{self.last_transaction.restaurant_offer.offer_id}"
            }
            p "MESSAGE : #{content_message.to_json}"
            sqs = AWS::SQS.new(:access_key_id => Setting.sqs.access_key_id, :secret_access_key => Setting.sqs.secret_access_key)
            input_queue = sqs.queues.named(Setting.sqs.ocr_input_queue)
            input_queue.send_message(content_message.to_json)
            p "---------- sent-----------------"
            p input_queue.visible_messages
          rescue
            p "Message sent failed"
          end
        end
      end
    end

  end
end