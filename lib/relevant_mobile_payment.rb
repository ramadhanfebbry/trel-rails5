class RelevantMobilePayment

  attr_accessor :root_uri, :api_path
  def initialize(root_uri = nil, api_path = nil)
    @root_uri = root_uri || ENV["RELEVANT_MP_PAYMENT_ROOT"] || "http://fundy.relevantmobile.com:8090"
    @api_path = api_path || "/mobilepay/api/v2"
  end


  def create_payment(appkey, auth_token, amount, cvv = nil)
    begin
      action_path = "/user/app/payment"
      url = @root_uri + @api_path + action_path
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      if @root_uri.include?("https://")
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      request = Net::HTTP::Post.new(uri.request_uri)
      request["Content-Type"] = "application/json; charset=utf-8"
      request["appkey"] = appkey
      request["auth_token"] = auth_token
      body_params = {"amount" => amount}
      body_params["cvv"] = cvv unless cvv.blank?
      request.body = body_params.to_json
      response = http.request(request)
      p "response for create_payment-------------------"
      p response.body
      p "---------"
      if response.kind_of? Net::HTTPSuccess
        response_json = JSON.load(response.body)
        if response_json["code"] == 0
          return [true, response_json["code"], response_json["message"], response_json["authorization"]]
        else
          return [false, response_json["code"], response_json["message"]]
        end
      else
        return [false, nil, nil]
      end
    rescue
      return [false, nil, nil]
    end
  end

  def void_payment(appkey, auth_token, amount, authorization)
    begin
      action_path = "/user/app/payment"
      url = @root_uri + @api_path + action_path
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      if @root_uri.include?("https://")
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      request = Net::HTTP::Put.new(uri.request_uri)
      request["Content-Type"] = "application/json; charset=utf-8"
      request["appkey"] = appkey
      request["auth_token"] = auth_token
      request.body = {"amount" => amount, "authorization" => authorization }.to_json
      response = http.request(request)
      p "response for create_payment-------------------"
      p response.body
      p "---------"
      if response.kind_of? Net::HTTPSuccess
        response_json = JSON.load(response.body)
        if response_json["code"] == 0
          return [true, response_json["code"], response_json["message"]]
        else
          return [false, response_json["code"], response_json["message"]]
        end
      else
        return [false, nil, nil]
      end
    rescue
      return [false, nil, nil]
    end
  end

  def create_user(appkey, auth_token)
    begin
      action_path = "/user"
      url = @root_uri + @api_path + action_path
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      if @root_uri.include?("https://")
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      request = Net::HTTP::Post.new(uri.request_uri)
      request["Content-Type"] = "application/json; charset=utf-8"
      request["appkey"] = appkey
      request["auth_token"] = auth_token
      response = http.request(request)
      p "response for create_user-------------------"
      p response.body
      p "---------"
      if response.kind_of? Net::HTTPSuccess
        response_json = JSON.load(response.body)
        if response_json["code"] == 0
          return [true, response_json["code"], response_json["message"]]
        else
          return [false, response_json["code"], response_json["message"]]
        end
      else
        return [false, nil, nil]
      end
    rescue
      return [false, nil, nil]
    end
  end

  def create_payment_method(appkey, auth_token, card_number, exp_month, exp_year, cvv, card_holder_name=nil, street_address=nil, city=nil, state=nil, country=nil, postal_code=nil)
    begin
      query_params = {
          "cardNumber" => card_number,
          "expMonth" => exp_month,
          "expYear" => exp_year,
          "cvv" => cvv
      }
      query_params["cardholderName"] = card_holder_name unless card_holder_name.blank?
      query_params["streetAddress"] = street_address unless street_address.blank?
      query_params["city"] = city unless city.blank?
      query_params["state"] = state unless state.blank?
      query_params["country"] = state unless country.blank?
      query_params["postalCode"] = postal_code unless postal_code.blank?

      action_path = "/user/method?#{query_params.to_query}"
      url = @root_uri + @api_path + action_path
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      if @root_uri.include?("https://")
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end
      request = Net::HTTP::Put.new(uri.request_uri)
      request["Content-Type"] = "application/json; charset=utf-8"
      request["appkey"] = appkey
      request["auth_token"] = auth_token
      response = http.request(request)
      p "response for create_payment_method-------------------"
      p response.body
      p "---------"
      if response.kind_of? Net::HTTPSuccess
        response_json = JSON.load(response.body)
        if response_json["code"] == 0
          return [true, response_json["code"], response_json["message"]]
        else
          return [false, response_json["code"], response_json["message"]]
        end
      else
        return [false, nil, nil]
      end
    rescue
      return [false, nil, nil]
    end
  end

end