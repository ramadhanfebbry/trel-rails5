class MomentfeedImportRestaurantsJob < Struct.new(:chain)
  require 'geokit'
  require 'geokit-rails'
  include Geokit::Geocoders
  require 'net/ftp'
  def perform
    p "***** RUN MomentfeedImportRestaurantsJob *****"
    @rih = RestaurantImportHistory.create(chain_id: chain.id, status: "processing", log_type: "#{chain.try(:name)} JSON", file_name: "#{chain.momentfeed_setting.base_url}?api_token=#{chain.momentfeed_setting.api_key}_#{chain.id}")
    begin
      @res_success = []
      @res_fail = []
      @pos = 0
      uri = URI.parse("#{chain.momentfeed_setting.base_url}?api_token=#{chain.momentfeed_setting.api_key}")
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      if response.kind_of? Net::HTTPSuccess
        restaurants = JSON.parse(response.body)
        restaurants.each_with_index do |rest,index|
          p "#{chain.try(:name)}: ImportRestaurantsJob processing #{rest["momentfeed_venue_id"]} ---- #{index+1}"
          p "= " * 100
          processing_file(rest, chain)
           #if index == 10
           #  break
           #end
        end
        @rih.update_attributes(:status => "completed", :success_restaurants => @res_success, :failed_restaurants => @res_fail )
      else
        p "= " * 100
        p "#{chain.try(:name)}: ImportRestaurantsJob failed"
        @rih.update_attributes(status: "failed")
      end
    rescue
      p "= " * 100
      p "#{chain.try(:name)}: ImportRestaurantsJob failed"
      @rih.update_attributes(status: "failed")
    end
  end

  def processing_file(rest, chain)
    @res = chain.restaurants.find_or_initialize_by_name("#{rest["store_info"]["corporate_id"]}") rescue nil
    if @res
      begin
        # debugger
        # Get the store info in one variable
        store_info = rest["store_info"]

        # App Display Text
        @res.app_display_text = "#{store_info["name"].present? ? store_info["name"] : '-' }"

        # DashboardDisplayName
        @res.dashboard_display_text = "#{store_info["name"].present? ? store_info["name"] : '-' }"

        # Address
        @res.address = "#{store_info["address"]}"

        # City
        @res.city_import = "#{store_info["locality"]}".strip.downcase if store_info["locality"].present?

        # State
        @res.state_import = "#{store_info["region"]}".strip.downcase if store_info["region"].present?

        # Country
        @res.country_import = "#{store_info["country"]}".strip.downcase if store_info["country"].present?

        # ZipCode
        @res.ignored_zipcode = true
        @res.zipcode = "#{store_info["postcode"]}"

        # PhoneNumber
        @res.ignored_phone = true
        @res.phone_number = "#{store_info["phone"]}"

        if store_info["latitude"].present? && store_info["longitude"].present?
          @res.latitude = "#{store_info["latitude"]}"
          @res.longitude = "#{store_info["longitude"]}"
        else
          loc = MultiGeocoder.geocode("#{store_info["address"]}, #{store_info["locality"]}, #{store_info["postcode"]}")
          if loc.success
            @res.latitude = loc.lat
            @res.longitude = loc.lng
          else
            @res.latitude = store_info["latitude"].to_i
            @res.longitude = store_info["longitude"].to_i
          end
        end

        # Tags
        tags = []
        tags << store_info["locality"]
        @res.tag_list = tags

        # SiteLive
        # open, closed, coming soon, temp closed
        @res.status = rest["status"].downcase.eql?("open") || rest["status"].downcase.eql?("coming soon")  #CHECK THIS
        @res.skip_participate_to_rewards_and_offers = ["closed", "coming soon", "temp closed"].include?(rest["status"].downcase)  #CHECK THIS
        @res.skip_set_hybrid_reward_redemption = ["closed", "coming soon", "temp closed"].include?(rest["status"].downcase)  #CHECK THIS

        # Fishbowl ID
        # @res.fishbowl_restaurant_identifier = "#{rest["storeid"]}" #CHECK THIS

        # City assignment to city_id
        @res.city_id = check_city_state_country(@res)

        @res.save!
        
        # restaurant status detail
        # open, closed, coming soon, temp closed
        @res_detail = RestaurantDetail.find_or_initialize_by_restaurant_id(@res.id)
        @res_detail.status = ["open", "closed", "coming soon", "temp closed", "testing"].each_with_index.select{|x,i| x.eql?("#{rest["status"].downcase}")}.first.last rescue 1

        if [Restaurant::TYPES["OPEN"], Restaurant::TYPES["CLOSED"], Restaurant::TYPES["TESTING"]].include?(@res_detail.status)
          @res.update_column(:app_display_text, "#{@res.app_display_text.chomp("- Coming Soon")}") if @res.app_display_text.include?("- Coming Soon")
          @res.update_column(:app_display_text, "#{@res.app_display_text.chomp("- Temporary Closed")}") if @res.app_display_text.include?("- Temporary Closed")
        elsif @res_detail.status.eql?(Restaurant::TYPES["COMING_SOON"])
          @res.update_column(:app_display_text, "#{@res.app_display_text.chomp("- Temporary Closed")}") if @res.app_display_text.include?("- Temporary Closed")
          @res.update_column(:app_display_text, "#{@res.app_display_text}- Coming Soon") unless @res.app_display_text.include?("- Coming Soon")
        elsif @res_detail.status.eql?(Restaurant::TYPES["TEMP_CLOSED"])
          @res.update_column(:app_display_text, "#{@res.app_display_text.chomp("- Coming Soon")}") if @res.app_display_text.include?("- Coming Soon")
          @res.update_column(:app_display_text, "#{@res.app_display_text}- Temporary Closed") unless @res.app_display_text.include?("- Temporary Closed")
        end

        @res_detail.save!

        if store_info["operatingHours"].present?
          Date::DAYNAMES.each_with_index do |dayname,i|
            store_info["hours"].split(';').each do |day|
              date_hours = day.split(',')
              day = date_hours[0] - 1
              open_at = date_hours[1].chars.each_slice(2).map(&:join).join(':')
              close_at = date_hours[2].chars.each_slice(2).map(&:join).join(':')

              @res_hours = @res.restaurant_hours.find_or_initialize_by_day_of_week(i)
              @res_hours.open_at = "#{open_at}"
              @res_hours.close_at = "#{close_at}"
              @res_hours.save!
            end
          end
        end

        @res_success << "#{rest["momentfeed_venue_id"]}"
        p "= " * 100
        p "#{chain.try(:name)}: ImportRestaurantsJob EXECUTE #{rest["momentfeed_venue_id"]} success"

      rescue => e
        p "= " * 100
        p "#{chain.try(:name)}: ImportRestaurantsJob EXECUTE #{rest["momentfeed_venue_id"]} fail"
        p e
        fail_row = []
        fail_row << "#{rest["momentfeed_venue_id"]}"
        fail_row << e.to_s
        @res_fail << fail_row
      end
    end
  end

  def check_city_state_country(res)
    country_import = res.country_import.upcase == "US" ? "USA" : res.country_import
    country_exist = Country.where(:ISO => "#{res.country_import.upcase}").first rescue nil
    country_exist = Country.where("lower(abbreviation) = '#{res.country_import.downcase}'").first rescue nil if country_exist.blank?
    if country_exist.blank?
      country_exist = Country.where("lower(name) like '#{res.country_import.gsub("'", "''").downcase}'").first rescue nil
    end 
    if country_exist.blank?
      country_exist = Country.create(
        :name => country_import.titleize,
        :abbreviation => (CS.countries.key(country_import.titleize) rescue nil)
      )
    end

    state_exist = nil
    if res.state_import.size <= 2
      state_exist = country_exist.regions.where("lower(abbreviation) = '#{res.state_import.downcase}'").first rescue nil

      if state_exist.blank?
        state_exist = Region.create(
            :name => ( CS.states(country_exist.abbreviation)["#{res.state_import}".upcase.to_sym] rescue res.state_import.titleize ),
            :abbreviation => res.state_import.upcase,
            :country_id => country_exist.id
        )
      end
    else
      state_exist = country_exist.regions.where("lower(name) like '#{res.state_import.gsub("'", "''").downcase}' ").first rescue nil

      if state_exist.blank?
        state_exist = Region.create(
            :name => res.state_import.titleize,
            :abbreviation => (CS.states(country_exist.abbreviation).key(res.state_import.titleize) rescue nil),
            :country_id => country_exist.id
        )
      end
    end

    city_exist = state_exist.cities.where("lower(name) like '#{res.city_import.gsub("'", "''").downcase}'").first rescue nil
    if city_exist.blank?
      city_exist = City.create(
          :name => res.city_import.titleize,
          :region_id => state_exist.id
      )
    end

    return city_exist.id
  end
end