module Onosys
  class OnlineOrderDetail < ActiveRecord::Base

    # attr_accessible :title, :body
    self.table_name = "onosys_online_order_details"

    belongs_to :online_order, :class_name => "Onosys::OnlineOrder"

  end

end
