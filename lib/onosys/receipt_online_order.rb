module Onosys
  class ReceiptOnlineOrder< ActiveRecord::Base

    self.table_name = "onosys_receipt_online_orders"
    attr_accessible :onosys_online_order_id, :receipt_id

    belongs_to :receipt
    belongs_to :online_order, :class_name => "Onosys::OnlineOrder"

  end

end

