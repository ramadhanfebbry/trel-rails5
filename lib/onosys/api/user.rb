require "net/http"
require "uri"

module Onosys
  module Api
    class User
      attr_accessor :api_root, :api_key, :client_id, :secret_id

      def initialize(onosys_connect_setting)
        @api_root = onosys_connect_setting["api_root"]
        @api_key = onosys_connect_setting["api_key"]
        @client_id = onosys_connect_setting["client_id"]
        @client_secret = onosys_connect_setting["client_secret"]
      end

      def create(params)
        p "ONOSYS API REGISTRATION #{params[:email]}"
        begin
          birth_date = "#{params[:dob_day]}/#{params[:dob_month]}/#{params[:dob_year]}".to_date rescue nil
          data = {
              "first_name" => params[:first_name],
              "last_name" => params[:last_name],
              "email" =>  params[:email],
              "username" => params[:email],
              "phone" => params[:phone_number],
              "country" => "US",
              "password" => params[:password],
              "password_confirm" => params[:password],
              "over_thirteen" => true,
              "email_as_username" => true
          }
          p uri = URI.parse("#{@api_root}/account/register")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request["Content-Type"] = "application/json; charset=utf-8"
          request["APIKEY"] = @api_key
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK) || response.code.to_s == "204"
            return [true, nil]
          else
            p response
            p "RESPONSE ONOSYS API REGISTRATION FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body)
            return [false, (response_json["ModelState"].values.first rescue response_json["Message"])]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

      def login(params)
        p "ONOSYS API LOGIN #{params[:email]}"
        begin
          p uri = URI.parse("#{@api_root}/account/login")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request["Content-Type"] = "application/x-www-form-urlencoded"
          request["apikey"] = @api_key
          request.body = "grant_type=password&username=#{params[:email]}&password=#{params[:password]}"
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            p "RESPONSE ONOSYS API LOGIN FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body)
            return [true, response_json, nil]
          else
            p "RESPONSE ONOSYS API LOGIN FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body)
            return [false, nil, response_json["error"]]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, nil, e.message]
        end
      end

      def get_profile(account_id, access_token)
        p "ONOSYS API GET PROFILE #{account_id}"
        begin
          uri = URI.parse("#{@api_root}/customer/#{account_id}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Get.new(uri.request_uri)
          request["apikey"] = @api_key
          request["Authorization"] = "Bearer #{access_token}"
          request["content-type"] = "application/json; charset=utf-8"
          response = http.request(request)
          if response.is_a?(Net::HTTPOK) || response.code.to_s == "204"
            p "RESPONSE ONOSYS API GET PROFILE FOR #{account_id} - #{response.body}"
            return [true, response]
          else
            p "RESPONSE ONOSYS API GET PROFILE FOR #{account_id} - #{response.body}"
            response_json = JSON.parse(response.body) rescue ""
            return [false, (response_json["Message"] rescue "")]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

      def update_profile(account_id, access_token, params)
        p "ONOSYS API UPDATE PROFILE #{params}"
        begin
          data = {
              "id" => account_id,
              "get_news_letters" => false,
              "country" => "US"
          }
          data["email"] = params[:email] if params[:email].present?
          data["first_name"] = params[:first_name] if params[:first_name].present?
          data["last_name"] = params[:last_name] if params[:last_name].present?
          data["phone"] = params[:phone_number] if params[:phone_number].present?

          uri = URI.parse("#{@api_root}/customer/#{account_id}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Put.new(uri.request_uri)
          request["apikey"] = @api_key
          request["Authorization"] = "Bearer #{access_token}"
          request["content-type"] = "application/json; charset=utf-8"
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK) || response.code.to_s == "204"
            p "RESPONSE ONOSYS API UPDATE PROFILE FOR #{account_id} - #{response.body}"
            return [true, "Your profile information has been updated."]
          else
            p "RESPONSE ONOSYS API UPDATE PROFILE FOR #{account_id} - #{response.body}"
            response_json = JSON.parse(response.body) rescue ""
            return [false, (response_json["Message"] rescue "")]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

      def update_password(params, acces_token)
        p "ONOSYS API UPDATE PASSWORD #{params}"
        begin
          data = {
              "old_password" => params[:old_password],
              "new_password" => params[:new_password],
              "password_confirm" => params[:confirm_password]

          }
          uri = URI.parse("#{@api_root}/account/change_password")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request["apikey"] = @api_key
          request["Authorization"] = "Bearer #{acces_token}"
          request["content-type"] = "application/json; charset=utf-8"
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK) || response.code.to_s == "204"
            p "RESPONSE ONOSYS API UPDATE PASSWORD FOR #{params[:email]} - #{response.body}"
            return [true, "Your password has been updated."]
          else
            p "RESPONSE ONOSYS API UPDATE PASSWORD FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body) rescue ""
            return [false, (response_json["Message"] rescue "")]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

      def forgot_password(params)
        p "ONOSYS API FORGOT PASSWORD #{params}"
        begin
          data = { "user_name" => params[:email]}
          uri = URI.parse("#{@api_root}/account/forgot_password?user_name=#{params[:email]}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request["apikey"] = @api_key
          request["content-type"] =  "application/x-www-form-urlencoded"
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK) || response.code.to_s == "204"
            p "RESPONSE ONOSYS API FORGOT PASSWORD FOR #{params[:email]} - #{response.body}"
            return [true, "An email with password reset link has been sent. Please check your inbox in sometime and follow the instructions to reset your password. If you have any concerns, please contact support."]
          else
            p "RESPONSE ONOSYS API FORGOT PASSWORD FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body) rescue ""
            return [false, (response_json["Message"] rescue "")]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

      def get_refresh_token(email, refresh_token)
        p "ONOSYS API GET REFRESH TOKEN #{email}"
        begin
          uri = URI.parse("#{@api_root}/account/login")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Get.new(uri.request_uri)
          request["apikey"] = @api_key
          request["content-type"] = "application/x-www-form-urlencoded"
          request.body = "grant_type=refresh_token&refresh_token=#{refresh_token}"
          response = http.request(request)
          if response.is_a?(Net::HTTPOK) || response.code.to_s == "204"
            p "RESPONSE ONOSYS API GET REFRESH TOKEN FOR #{email} - #{response.body}"
            return [true, response]
          else
            p "RESPONSE ONOSYS API GET REFRESH TOKEN FOR #{email} - #{response.body}"
            return [false, "User need login or sign up to proceed"]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

    end
  end
end