module Onosys
  class OnlineOrder < ActiveRecord::Base
    # attr_accessible :title, :body
    self.table_name = "onosys_online_orders"
    belongs_to :chain
    belongs_to :user
    belongs_to :restaurant

    has_one :online_order_detail, :class_name => "Onosys::OnlineOrderDetail", :foreign_key => :onosys_online_order_id
    has_one :receipt_online_order, :class_name => "Onosys::ReceiptOnlineOrder", :foreign_key => :onosys_online_order_id
    has_one :receipt, :through => :receipt_online_order

    STATUS = {
        :NEW => 1,
        :CONVERTED => 2
    }

    PLATFORM = {
        "Android" => 1, "Iphone" => 2, "Browser" => 3, "Browser" => 4
    }

    scope :custom_search, lambda{ |params|
      conditions = []
      conditions << "chains.status = 'active'"
      conditions << "restaurants.id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
      conditions << "chains.id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
      conditions << "lower(users.email) LIKE '%#{params[:q].downcase}%' OR (lower(restaurants.name) LIKE '%#{params[:q].downcase}%')" unless params[:q].blank?
      #conditions << "lower(users.email) LIKE '%#{params[:q].downcase}%' " unless params[:q].blank?
      conditions = conditions.join(" AND ")
      joins("INNER JOIN restaurants ON restaurants.id=onosys_online_orders.restaurant_id INNER JOIN chains ON chains.id=restaurants.chain_id INNER JOIN users ON users.id=onosys_online_orders.user_id").where(conditions)
    }


    def save_detail(order_detail)
      ood = Onosys::OnlineOrderDetail.new(:onosys_online_order_id => self.id)
      ood.order_detail = order_detail
      ood.sub_total = order_detail["totals"]["subtotal"] rescue 0
      ood.tax = order_detail["totals"]["total_taxes"] rescue 0
      ood.total = order_detail["totals"]["total_price"] rescue 0
      ood.discount = order_detail["totals"]["discount_amount"] rescue 0
      ood.save
    end

    def as_received_receipt(chain_id, user_id, order_detail)
      receipt = Receipt.new(:chain_id => chain_id, :user_id => user_id, :status => Receipt::STATUS[:RECEIVED], :is_online_order => true)
      receipt_transaction = ReceiptTransaction.new
      receipt_transaction.status = Receipt::STATUS[:RECEIVED]
      receipt_transaction.subtotal = order_detail["totals"]["subtotal"]
      receipt_transaction.tax = order_detail["totals"]["total_taxes"]
      receipt.receipt_transactions << receipt_transaction
      receipt.save
      p receipt.errors
      return receipt.id
    end

  end
end
