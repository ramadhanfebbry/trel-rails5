class OwnerJob::SurveyExportJob < Struct.new(:survey, :owner)
  include ActionView::Helpers::NumberHelper

  def perform
    current_owner = owner
    puts "OwnerJob::SurveyExportJob Executing"
    email = owner.email || 'inoe.bainur@gmail.com' #|| owner.email
    file_name = "survey_export.csv"
    chain = owner.chain

    @is_avg = REDIS.get "avg_survey_setting_#{chain.id}"
    @is_avg = @is_avg.blank?
    puts "performing" * 100
    question_ids = survey.all_questions_collection(nil, nil).map(&:id)
    questions = survey.all_questions_collection(nil, nil).map(&:text)
    question1 = questions[0] rescue ""
    question2 = questions[1] rescue ""
    question3 = questions[2] rescue ""
    question4 = questions[3] rescue ""
    question5 = questions[4] rescue ""


    conditions_for_res_owner = nil
    conditions_for_res_owner = " AND restaurant_id in (#{owner.restaurant_list.map(&:id).
        join(',')})" if !owner.chain_owner?

    query = []
    question_ids.each do |q|
      query << "question_id = #{q.to_s} desc"
    end

    query = query.join(" , ")

    @survey_users = SurveysUser.where("survey_id = ? #{conditions_for_res_owner}", survey.id).
        order("surveys_users.guest_count desc,surveys_users.created_at desc").
        includes(:survey, :answers, :receipt => :receipt_transactions).
        paginate(:page => 1, :per_page => Setting.pagination.per_page)

    total_pages = @survey_users.total_pages
    puts "total pages = #{total_pages}"
    reward_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      avg_show = "AVG" if @is_avg
      csv << ["Date", "Time", "Location", "Member", avg_show, "#{question1}", "#{question2}",
              "#{question3}", "#{question4}", "#{question5}"].compact
      #averages

      1.upto(total_pages) do |pg|
        m = SurveysUser.where("survey_id = ? #{conditions_for_res_owner}", survey.id).
            order("surveys_users.guest_count desc,surveys_users.created_at desc").
            includes(:survey, :answers, :receipt => :receipt_transactions).
            paginate(:page => pg, :per_page => Setting.pagination.per_page)
        m.each do |survey_user|

          begin
            res_name = survey_user.receipt.last_transaction.restaurant.name
          rescue
            res_name = Restaurant.find(survey_user.receipt.last_transaction.restaurant_id).name rescue "-"
          end

          if current_owner.role and current_owner.role.to_s.include?('email')
            user_email = string_format(survey_user.user.email.gsub("@", "@"), :slice_limit => 10) rescue '-'
          else
            user_email = survey_user.user.id rescue '-'
          end

          avg = survey_user.show_answers_new(question_ids,
                                             survey_user.answers.includes(:question).order(query))

          show_avg_value = float_format(avg[:all]) if @is_avg
          csv << [
              date_only(survey_user.created_at),
              hour_only(survey_user.created_at),
              res_name,
              user_email,
              show_avg_value,
              avg[:q1],
              avg[:q2],
              avg[:q3],
              avg[:q4],
              avg[:q5]
          ].compact

        end
      end

    end

    OwnerMailer.send_reward_redeemed(chain, email, "tmp/#{file_name}.csv", file_name, "Survey Export").deliver!
    File.delete("tmp/#{file_name}.csv")
    puts "OwnerJob::SurveyExportJob Executed"
  end

  def date_only(date)
    date.to_datetime.strftime("%Y-%m-%d") if date
  end

  def hour_only(date)
    date.strftime("%H:%M %p")
  end

  def string_format(str, opts = {})
    if is_number?(str)
      #slice_size = opts[:slice_limit] || 10
      #ln = str.length
      #res = []
      #
      #if ln > slice_size
      #  slice = ln / slice_size
      #
      #  to = slice_size
      #  from = 0
      #  0.upto(slice) do |x|
      #    res << str.slice(from..to)
      #    from += slice_size + 1
      #    to += slice_size + 1
      #  end
      #else
      #   return str
      # end
      return "" if str.to_i == 0
      return str
    else
      return str
    end
  end

  def is_number?(var)
    true if Float(var) rescue false
  end

  def float_format(number,precision = 2)
    begin
      if number.nan?
        return 0
      else
        return number_with_precision(number, :precision => precision)
      end
    rescue
      return number_with_precision(number, :precision => precision)
    end
    return number_with_precision(number, :precision => precision) if number.class == Float
  end


end