class OwnerJob::ActivitiesNumberExportJob < Struct.new(:chain, :owner, :params)
  def perform
    email = owner.email

    if params['restaurant_id'].blank?
      if owner.is_chain_owner?
        restaurants = chain.restaurants
      else
        restaurants = owner.restaurant_list
      end
    end

    # prepopulate the data first
    ActivitySummary.scheduler_daily_by_chain(chain)


    start_date = params['start_date']  || Date.today
    end_date = params['end_date'] || (Date.today + 31.days).to_date
    file_name = "activities_#{chain.name}_#{start_date}_#{end_date}.csv"

    act_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      head = table_generate_receipts_search(start_date,end_date,restaurants)
      csv << head



      restaurants.each do |res|
        csv << table_generate_body_receipts_ajax_search(start_date,end_date, res)
      end
    end
    OwnerMailer.send_reward_redeemed(chain, email, "tmp/#{file_name}.csv", file_name,"Activities Export").deliver!
    File.delete("tmp/#{file_name}.csv")
    puts "Success EXECUTING OwnerJob::ActivitiesJob"
  end

  def success

  end

  def error(job, exception)
    #job.update_column(:status,4)
  end

  def table_generate_receipts_search(start_date,end_date,restaurants)
    end_date = end_date.to_date
    start_date = start_date.to_date
    str = []
    loop = (end_date - start_date).to_i

    str << "Location"
    loop.downto(0).each do |week|
      str << "#{(end_date - (week).days).strftime("%d %b")}"
    end

    return str.flatten
  end

  def table_generate_body_receipts_ajax_search(start_date,end_date, res)

    start_date = start_date.to_date
    end_date = end_date.to_date
    str = []
    loop = (end_date - start_date).to_i

    str << "#{res.dashboard_display_text}"
    approved_receipt = approved_receipt_prepopulate_search(start_date,end_date,res.id)
    loop.downto(0).each do |week|
      approved = approved_receipt.select{|x| x.transaction_date.to_s == (end_date - week.days).to_date.to_s}
      approved = approved.first
      if !approved.blank? and approved.total.to_i > 0
        str << "#{approved.total.to_i}"
      else
        unless approved.blank?
          str << "#{approved.total.to_i}"
        else
          str << "0"
        end
      end
    end

    result = str.flatten

    return result
  end

  def approved_receipt_prepopulate_search(start_date,end_date,res_id)
    #total = 0
    #res = ActivitySummary.where("restaurant_id = ? and transaction_date = date(?)", res_id,(Time.zone.now - week.days)).first
    #total = res.total.to_i unless res.blank?
    #return total
    result = ActivitySummary.where('transaction_date between date(?) and date(?) and restaurant_id = ?',
                                   (start_date - 2.days),end_date,res_id)
    return result
  end
end