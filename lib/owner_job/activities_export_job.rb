class OwnerJob::ActivitiesExportJob < Struct.new(:chain, :owner, :params)
  def perform
    email = owner.email

    if params['restaurant_id'].blank?
      if owner.is_chain_owner?
        params['restaurant_id'] = chain.restaurants.map(&:id)
      else
        params['restaurant_id'] = owner.restaurant_list
      end
    end

    if params['reward_id'].blank?
      reward_conditions  = nil
    else
      reward_conditions = "AND reward_id = #{params['reward_id']}"
    end
    puts "EXECUTING OwnerJob::ActivitiesExportJob"
    puts params
    start_date = params['start_date']  || Date.today
    end_date = params['end_date'] || (Date.today + 31.days).to_date
    restaurant_id_sql = params['restaurant_id'].join(',')
    puts "============== #{start_date} ============="
    puts "============== #{end_date} ============="

    file_name = "activities_#{chain.name}_#{start_date}_#{end_date}.csv"
    date_zone = "date(TIMEZONE('UTC', reward_transactions.created_at) AT TIME ZONE 'EST')"

    time_hour_scale = ''#"AND date_part('HOUR',reward_transactions.created_at - interval '5 hour') between #{params["hour_start_date"]["hours(4i)"]} AND #{params["hour_end_date"]["hours(4i)"]}"
    time_min_scale =  ''#"AND date_part('MINUTE',reward_transactions.created_at - interval '5 hour') between #{params["hour_start_date"]["hours(5i)"]} AND #{params["hour_end_date"]["hours(5i)"]}"


    activities_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      #results = RewardTransaction.where("#{date_zone} between date(?) and date(?) and restaurant_id in (?) #{reward_conditions} #{time_hour_scale} #{time_min_scale}",
      #                                  start_date, end_date, params['restaurant_id']).order('created_at desc')#.paginate(page: page, per_page: Setting.pagination.per_page)
      query = "select distinct on (r.receipt_id) receipt_id,subtotal,restaurant_id,case when exists(select r.issue_date)
      then r.issue_date
      else r.created_at
      end as created_at,r.id from receipt_transactions r
      where  date(
      case when exists(select r.issue_date)
      then r.issue_date
      else r.created_at
      end
      ) between date('#{start_date}')
      and date('#{end_date}') and r.status=3
      and r.restaurant_id
      in(#{restaurant_id_sql})
      order by r.receipt_id,r.created_at desc limit 100"

      results = ReceiptTransaction.find_by_sql(query)

      csv << [
          "Receipt id",
          "Check Total",
          "Date",
          "Time",
          "Restaurant Name"
      ]

      results.each do |receipt_transaction|

        csv << [
            receipt_transaction.receipt_id,
            receipt_transaction.subtotal,
            receipt_transaction.created_at.strftime('%D'),
            receipt_transaction.created_at.strftime('%H:%M %Z'),
            (Restaurant.find(receipt_transaction.restaurant_id).dashboard_display_text rescue nil)
        ]
      end
    end
    OwnerMailer.send_reward_redeemed(chain, email, "tmp/#{file_name}.csv", file_name,
                                     "Acitities summary report").deliver!
    File.delete("tmp/#{file_name}.csv")
    puts "Success EXECUTING OwnerJob::ActivitiesExportJob"
  end

  def success

  end

  def error(job, exception)
    #job.update_column(:status,4)
  end
end