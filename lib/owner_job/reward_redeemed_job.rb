class OwnerJob::RewardRedeemedJob < Struct.new(:chain, :owner, :params)
  def perform
    email = owner.email

    if params['restaurant_id'].blank?
      if owner.is_chain_owner?
        params['restaurant_id'] = chain.restaurants.map(&:id)
      else
        params['restaurant_id'] = owner.restaurant_list
        end
    end

    if params['reward_id'].blank?
      reward_conditions  = nil
    else
      reward_conditions = "AND reward_id = #{params['reward_id']}"
    end
    puts "EXECUTING OwnerJob::RewardRedeemedJob"
    puts params
    start_date = params['start_date']  || Date.today
    end_date = params['end_date'] || (Date.today + 31.days).to_date

    puts "============== #{start_date} ============="
    puts "============== #{end_date} ============="
    ## if all the rewards are exported
    if params['reward_id'].blank?
      file_name = "reward_redeemed_#{chain.name}_#{start_date}_#{end_date}.csv"
    else
    ## if all the rewards are only one which selected
      file_name = "#{Reward.find(params['reward_id']).name}_redeemed_#{chain.name}_#{start_date}_#{end_date}.csv"
    end
    date_zone = "date(TIMEZONE('UTC', reward_transactions.created_at) AT TIME ZONE 'EST')"
    time_hour_scale = ''#"AND date_part('HOUR',reward_transactions.created_at - interval '5 hour') between #{params["hour_start_date"]["hours(4i)"]} AND #{params["hour_end_date"]["hours(4i)"]}"
    time_min_scale =  ''#"AND date_part('MINUTE',reward_transactions.created_at - interval '5 hour') between #{params["hour_start_date"]["hours(5i)"]} AND #{params["hour_end_date"]["hours(5i)"]}"

   # rt_count = RewardTransaction.where("#{date_zone} between date(?) and date(?) and restaurant_id in(?) #{reward_conditions} #{time_hour_scale} #{time_min_scale}",
  #                                     start_date, end_date, params['restaurant_id']).count
    puts file_name

   # loop = (rt_count.to_f / Setting.pagination.per_page.to_f).ceil


    reward_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << [
          "Reward id",
          "Code",
          "Name",
          "Date",
          "Time",
          "Restaurant Name",
          "Description"
          #"Latitude",
          #"Longitude"
      ]
      #1.upto(loop) do |page|
       # puts page
        results = RewardTransaction.where("#{date_zone} between date(?) and date(?) and restaurant_id in (?) #{reward_conditions} #{time_hour_scale} #{time_min_scale}",
                                start_date, end_date, params['restaurant_id']).order('created_at desc')#.paginate(page: page, per_page: Setting.pagination.per_page)
        #puts results
        results.each do |reward_transaction|

          csv << [
              reward_transaction.reward_id,
              reward_transaction.staffcode,
              reward_transaction.reward.name,
              reward_transaction.created_at.strftime('%D'),
              reward_transaction.created_at.strftime('%H:%M %Z'),
              (reward_transaction.restaurant.dashboard_display_text rescue nil),
              (reward_transaction.reward.description rescue nil)
              #reward_transaction.latitude,
              #reward_transaction.longitude
          ]
        end
      #end

    end
    OwnerMailer.send_reward_redeemed(chain, email, "tmp/#{file_name}.csv", file_name).deliver!
    File.delete("tmp/#{file_name}.csv")
    puts "Success EXECUTING OwnerJob::RewardRedeemedJob"
  end

  def success

  end

  def error(job, exception)
    #job.update_column(:status,4)
  end
end