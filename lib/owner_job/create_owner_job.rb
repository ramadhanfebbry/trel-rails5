class OwnerJob::CreateOwnerJob < Struct.new(:chain, :owner, :pwd_generated)
  def perform
    owner.participate_restaurant(chain.restaurants.map(&:id))
    Delayed::Job.enqueue(LdapJob.new(owner.chain, owner, pwd_generated), :run_at => Time.zone.now + 30.seconds)
    OwnerMailer.confirmation_email(owner, pwd_generated).deliver
  end
end