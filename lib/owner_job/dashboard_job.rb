class OwnerJob::DashboardJob < Struct.new(:job)
  def perform
    ## locked the job
    job.update_column(:status, 2)
    # last method before editted
    #job.execute_job

    execute_job(job)
  end

  def success_callback(job)
    if job.schedule_type == 1
      job.update_column(:status, 3)
    else
      complete = job.is_job_complete?
      if complete
        job.update_column(:status, 3)
      else
        job.update_column(:status, 0)
      end
    end
  end

  #def error(job, exception)
    #job.update_column(:status,4)
    #OwnerJob.where("id = ?", job.delayable_id).first.update_column(:status, 4)
    #path = "#{Rails.root}/tmp/error.csv"
    #transaction_csv = CSV.open(path, "wb") do |csv|
    #  # header row
    #  csv << [
    #      "User id",
    #  ]
    #end
    #  OwnerMailer.send_reward_redeemed(nil, "inoe.bainur@gmail.com", path, "user_id.csv",exception).deliver!
  #end


  def execute_job(job)
    if job.miss_you?
      execute_miss_you(job)
    else
      execute_standard(job)
    end
  end

  ## standard job
  def execute_standard(job)
    sql = sql_for_standard(job)

    offset = 0
    limit = 500
    total_user = 0

    begin
      results = User.find_by_sql("#{sql} OFFSET #{offset} LIMIT #{limit}")
      offset += limit
      puts "ofset #{offset}"
      total_user += results.size
      # Do stuff here
      push_reward_or_point(job, results)

    end while !results.blank?

    job.update_column(:total_users, total_user)
  end

  def sql_for_standard(job)
    owner = Owner.find(job.owner_id)
    chain = owner.chain
    restaurant_ids = owner.chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    sql = "select users.* from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date <= date('#{Time.now - job.percentage.weeks}')
"  if job.job_type == 1

    sql = "select users.* from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date > date('#{Time.now - job.activity_days.weeks}')
" if job.job_type == 2

    sql = "select users.* from users where chain_id = #{chain.id} and users.active = 't'" if job.job_type == 3

    return sql
  end

  #end standard

  ## miss you job
  def execute_miss_you(job)
    sql = sql_for_miss_you(job)

    offset = 0
    limit = 500
    total_user = 0

    begin
      results = User.find_by_sql("#{sql} OFFSET #{offset} LIMIT #{limit}")
      offset += limit
      puts "ofset #{offset}"
      total_user += results.size
      # Do stuff here

      push_reward_or_point(job, results)


    end while !results.blank?

    job.update_column(:total_users, total_user)
  end

  def sql_for_miss_you(job)
    owner = Owner.find(job.owner_id)
    chain = owner.chain
    restaurant_ids = owner.chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    #### sql initialize
    sql = "select distinct(users.*) from (
            select user_id , date(max(max)) from (
            select user_id, max from (
             SELECT user_id, max(last_active) FROM restaurant_users
             inner join users on users.id = restaurant_users.id
             WHERE users.active = 't ' and (users.miss_you_date is null or users.miss_you_date < date('#{Time.now - 200.days}'))
             and
             (restaurant_id in (#{restaurant_id_sql})) group by user_id
             ) as s
            UNION
            select reward_transactions.user_id, max(reward_transactions.created_at) from
            reward_transactions
            left join restaurant_users on reward_transactions.user_id = restaurant_users.user_id
            where
            reward_transactions.restaurant_id in(#{restaurant_id_sql})
            group by reward_transactions.user_id
            ) as x group by user_id) as mm
            inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
            and (users.miss_you_date is null or users.miss_you_date < date('#{Time.now - 200.days}'))
            and date <= date('#{Time.now - job.percentage.weeks}')
            " if job.job_type == 1

    sql = "select distinct(users.*) from (
              select user_id , date(max(max)) from (
              select user_id, max from (
               SELECT user_id, max(last_active) FROM restaurant_users
               inner join users on users.id = restaurant_users.id
               WHERE users.active = 't ' and (users.miss_you_date is null or users.miss_you_date < date('#{Time.now - 200.days}'))
                and
               (restaurant_id in (#{restaurant_id_sql})) group by user_id
               ) as s
              UNION
              select reward_transactions.user_id, max(reward_transactions.created_at) from reward_transactions
              left join restaurant_users on reward_transactions.user_id = restaurant_users.user_id
              where
              reward_transactions.restaurant_id in(#{restaurant_id_sql})
              group by reward_transactions.user_id
              ) as x group by user_id ) as mm
              inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
              and (users.miss_you_date is null or users.miss_you_date < date('#{Time.now - 200.days}'))
              and date > date('#{Time.now - job.activity_days.weeks}')
" if job.job_type == 2

    sql = "select users.* from users where chain_id = #{chain.id} and users.active = 't' " if job.job_type == 3
    return sql
  end

  # end miss you job


  def push_reward_or_point(job, users)
    #puts "push reward or point " * 100

    puts "---------------------"
    puts "job"
    puts "users === #{users}"
    success_user_ids = []
    failed_user_ids = []

    case job.reward_type
      when 1
        ## push reward
        users.each do |user|
          begin
            puts "success job push reward for #{user.id}"
            RewardWallet.create_with_delay({:reward_id => job.reward_id, :user_id => user.id,
                                            :description => "pushed from dashboard #{job.created_by_user}"},
                                           true) ## do not send pn for this type reward
            if job.notification and job.push_email
              job.owner_job_push_email([user])
            end

            if job.notification and job.push_phone
              success_notif = job.push_notification([user], job.content_phone)
            end

            success_user_ids << {:id => user.id, :email => user.email,
                                 :date_sent => Time.zone.now.strftime('%D'),
                                 :success_notif => success_notif
            }
            job.update_miss_you(user) ## update miss you column
          rescue => e
            puts "OWNERJOB::EXECUTEJOB::PUSHREWARD => #{e.inspect}"
           failed_user_ids << user.id
            next
          end
        end
      when 2
        ## points
        unless users.blank?
          users.each do |user|
            begin
              user.earn(job.points, nil, "Owner")
              PointHistory.add_to_history(user.id, "Dashboard Push Point #{job.created_by_user}", job.points)
              if job.notification and job.push_email
                job.owner_job_push_email([user])
              end

              if job.notification and job.push_phone
                success_notif = job.push_notification([user], job.content_phone)
              end
              #puts "earch #{points} point to user #{user.email} from owner dashboard "
              #success_user_ids << user.id
              success_user_ids << {:id => user.id, :email => user.email,
                                   :date_sent => Time.zone.now.strftime('%D'),
                                   :success_notif => success_notif
              }
              job.update_miss_you(user) ## update miss you column
            rescue
              failed_user_ids << user.id
              next
            end
          end
        end
      when 3
        puts "------------------"
    end

    history = OwnerJobHistory.where(:owner_job_id => job.id).first rescue []

    if history.blank?
      OwnerJobHistory.create(:owner_job_id => job.id,
                             :success => success_user_ids,
                             :failed => failed_user_ids
      )
    else
      data_success_before = history.success

      history.update_attributes(:success => data_success_before + success_user_ids,
                                :failed => failed_user_ids)
    end
    #self.update_attribute(:status, 3)
    success_callback(job)
    puts "Done processing #{job.description} Jobs"
    puts "*" * 50
    puts "push reward or point Done" * 100
  end


end