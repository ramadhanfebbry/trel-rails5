class OwnerJob::GuestRelationJob < Struct.new(:guest_relation)
  def perform
    ## locked the job
    if guest_relation.class == Array
      guest_relation.each do |gr|
        GuestRelation.find(gr).execute
      end
    else
      guest_relation.execute
    end

  end

  def success
    if guest_relation.class == Array
      guest_relation.each do |gr|
        GuestRelation.find(gr).update_column(:status, 4)
        #GuestRelationMailer.send_notif(GuestRelation.find(gr)).deliver!
      end
    else
      guest_relation.update_column(:status, 4)
      #GuestRelationMailer.send_notif(guest_relation).deliver!
    end
  end

  def error(job, exception)
    #job.update_column(:status,4)
  end
end