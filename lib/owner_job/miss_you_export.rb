class OwnerJob::MissYouExport < Struct.new(:job, :string_status, email)

  def perform
    job = OwnerJob.find 1172
    email = "mbainurb@gmail.com"
    string_status = "success"
    string_status = "failed"
    total_pages = job.ownerjob_logs.send("#{string_status}").paginate(:page => 1, :per_page => 1000).total_pages
    file_name = "owner_job.csv"

    reward_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      csv << ["id", "Email", "Date Sent", "Notification"]
      1.upto(total_pages) do |i|
        logs = job.ownerjob_logs.success.paginate(:page => i, :per_page => 1000)

        logs.each do |log|
          csv << [
              log.user_id,
              log.email,
              log.date_sent,
              (OwnerjobLog::NOTIFICATION_STATUS.key(log.notification_status) rescue "-")
          ]

        end
      end
    end

    OwnerMailer.send_reward_redeemed(job.chain, email, "tmp/#{file_name}.csv", file_name,"Owner Job Task #{job.id}").deliver!
    File.delete("tmp/#{file_name}.csv")
    puts "Success EXECUTING OwnerJob::MissYouExport"
  end
end