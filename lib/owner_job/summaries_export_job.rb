
class OwnerJob::SummariesExportJob < Struct.new(:all_restaurants,:restaurants,:chain, :owner)
  include ActionView::Helpers::NumberHelper

  def perform
    @is_avg = REDIS.get "avg_survey_setting_#{chain.id}"
    @is_avg = @is_avg.blank?
    list_surveys = Survey.where("chain_id in (?)",[chain.id])
    if owner.chain_owner?
      survey = list_surveys.last
      surveys = list_surveys
    else
      survey = SurveysUser.where("restaurant_id in (?)", owner.restaurant_list).first.survey rescue list_surveys.first
      surveys = SurveysUser.where("restaurant_id in (?)", owner.restaurant_list).select('distinct(survey_id)')
    end

    email = owner.email || 'inoe.bainur@gmail.com' #|| owner.email
    file_name = "summary_export.csv"
    puts "performing" * 100
    reward_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row

      avg_header =  ["Avg general survey score"]
      avg_header = [] if @is_avg == false
      csv << [
          "Location",
          "Total members",
          "New members this month ",
          "New members prior month "] + avg_header + ["Avg visits per month"]
      #csv.delete_at(4) if  @is_avg == false
      #averages

      avg_averages = [DashboardQuery::SummaryChain.sum_summary_average_survey(all_restaurants, surveys)[:all].to_s]
      avg_averages = [] if @is_avg == false
      csv << [
          "AVERAGES",
          float_format(DashboardQuery::SummaryChain.sum_total_members(all_restaurants), 1).to_s,
          float_format(DashboardQuery::SummaryChain.sum_average_month(all_restaurants), 1).to_s,
          float_format(DashboardQuery::SummaryChain.sum_average_last_month(all_restaurants), 1).to_s] + avg_averages +         [float_format(DashboardQuery::SummaryChain.sum_avg_visit(all_restaurants), 1).to_s]
      #csv.delete_at(4) if  @is_avg == false

      puts "asdfasfasdfsdafasdf" * 100

      restaurants.each do |restaurant|
        ## survey
        puts "AYE" * 100
        sur = SurveysUser.where("restaurant_id in (?)", [restaurant]).first.survey rescue nil
        puts "Alalal" * 100
        all_survey_avg = restaurant.survey_averages_restaurant(sur) rescue 0
        if all_survey_avg == 0
          all_survey_avg = restaurant.survey_averages_restaurants(surveys)
        end
        avg_survey = all_survey_avg[:all] rescue 0
        avg_survey = [float_format(avg_survey,2)]
        avg_survey = [] if @is_avg == false
        csv << [
            restaurant.dashboard_display_text,
            restaurant.all_total_member,
            restaurant.member_this_month,
            restaurant.member_last_month] + avg_survey + [restaurant.avg_coming.round(1)]
        #csv.delete_at(4) if  @is_avg == false

      end
      puts "asdfasfasdfsdafasdf ----------------" * 100

    end
    OwnerMailer.send_reward_redeemed(chain, email, "tmp/#{file_name}.csv", file_name,"Summaries Export").deliver!
    File.delete("tmp/#{file_name}.csv")
    puts "Success EXECUTING OwnerJob::SummariesExportJob"
  end

  def float_format(number,precision = 2)
    begin
      if number.nan?
        return 0
      else
        return number_with_precision(number, :precision => precision)
      end
    rescue
      return number_with_precision(number, :precision => precision)
    end
    return number_with_precision(number, :precision => precision) if number.class == Float
  end

end