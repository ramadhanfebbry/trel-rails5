class OwnerJob::RestaurantUserJob
  attr_accessor :user_id, :last_active, :subtotal, :restaurant_id, :issue_date

  def initialize(user_id, restaurant_id, last_active, subtotal,issue_date)
    @user_id = user_id
    @last_active = last_active
    @subtotal = subtotal
    @restaurant_id = restaurant_id
    @issue_date = issue_date
  end

  def update_restaurant_user
    ru = RestaurantUser.where("restaurant_id = ? and user_id = ?", restaurant_id, user_id).first
    puts "OwnerJob::RestaurantUserJob update_restaurant_user"
    puts "updating last_active column ....."
    unless ru.blank?
      ru.update_column(:last_active, last_active.to_date)
      puts "#{ru.user_id}"
      puts "done ........................................"
      puts "updating total transaction column ....."
      ru.update_column(:total_transaction, ru.total_transaction.to_f + subtotal)
      puts "*" * 30
      puts "done ........................................."
    else
      res_user = RestaurantUser.new(:user_id => user_id,
                                    :restaurant_id => restaurant_id,
                                    :joined_date => @issue_date)
      res_user.save!
      #res_user.update_column(:created_at, issue_date)
      res_user.update_column(:last_active, Date.today)
    end
  end

  ##  OwnerJob::RestaurantUserJob.insert_default_value
  def self.insert_default_value
    ## insert the default value for total transaction and last active column
    res = []
    Chain.all.each do |chain|
      sql = "select u.id as u_id, u.email,
                          u.register_type as u_type, timezone('EST', timezone('UTC', u.created_at))
                         as signup_date, count(case when t1.status=3 then c.id end)
                         as r_count, sum(coalesce(case when t1.status=3 then t1.tax else 0 end,0) +
                         coalesce(case when t1.status=3 then t1.subtotal else 0 end,0)) as spent
                         from users u left join receipts c on u.id=c.user_id left join
                         receipt_transactions t1 on t1.receipt_id = c.id left join
                         receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
                         where  u.chain_id=#{chain.id} group by u.id, u.email, u.created_at"
      res = ActiveRecord::Base.connection.execute(sql)

      unless res.blank?
        res.each do |r|
          begin
            ru = RestaurantUser.where("user_id = ?", r['u_id']).first
            unless ru.blank?
              ru.update_attributes({:total_transaction => r['spent']})
              last_activity = User.find(r['u_id']).receipts.where('receipts.status = 3').last
              ru.update_column(:last_active, last_activity.created_at.to_date) unless last_activity.blank?
            end
          rescue
            next
          end
        end
      end
    end
  end
end