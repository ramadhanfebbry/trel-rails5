class OwnerJob::RewardRedeemedNoDupJob < Struct.new(:chain, :owner, :params)
  def perform

#     SELECT  distinct on(date(t.created_at), t.reward_id, t.restaurant_id, t.user_id)
#     t.created_at at time zone 'EST' as date_created_at ,
# t.created_at at time zone 'EST'  as created_at,
#                                     t.user_id,w.name as reward_name,us.email as email_user, rs.name as restaurant_name, rs.fishbowl_restaurant_identifier as fishbowl_restaurant_identifier FROM reward_transactions t LEFT JOIN rewards w ON w.id = t.reward_id
#     INNER JOIN restaurants rs on rs.id = t.restaurant_id
#     INNER JOIN users us on us.id = t.user_id
#     WHERE w.chain_id = $P{chain_id} AND date(
#                                             timezone('EST', timezone('UTC', t.created_at)))
#     >= $P{start_date} AND date(timezone('EST', timezone('UTC', t.created_at))) <= $P{end_date}
#     AND pos_used is TRUE and redeeming is FALSE
    email = owner.email

    if params['restaurant_id'].blank?
      if owner.is_chain_owner?
        params['restaurant_id'] = chain.restaurants.map(&:id)
      else
        params['restaurant_id'] = owner.restaurant_list
        end
    end

    if params['reward_id'].blank?
      reward_conditions  = nil
    else
      reward_conditions = "AND reward_id = #{params['reward_id']}"
    end
    puts "EXECUTING OwnerJob::RewardRedeemedJob"
    puts params
    start_date = params['start_date']  || Date.today
    end_date = params['end_date'] || (Date.today + 31.days).to_date

    puts "============== #{start_date} ============="
    puts "============== #{end_date} ============="
    ## if all the rewards are exported
    if params['reward_id'].blank?
      file_name = "reward_redeemed_#{chain.name}_#{start_date}_#{end_date}.csv"
    else
    ## if all the rewards are only one which selected
      file_name = "#{Reward.find(params['reward_id']).name}_redeemed_#{chain.name}_#{start_date}_#{end_date}.csv"
    end
    date_zone = "date(TIMEZONE('UTC', reward_transactions.created_at) AT TIME ZONE 'EST')"
    time_hour_scale = ''#"AND date_part('HOUR',reward_transactions.created_at - interval '5 hour') between #{params["hour_start_date"]["hours(4i)"]} AND #{params["hour_end_date"]["hours(4i)"]}"
    time_min_scale =  ''#"AND date_part('MINUTE',reward_transactions.created_at - interval '5 hour') between #{params["hour_start_date"]["hours(5i)"]} AND #{params["hour_end_date"]["hours(5i)"]}"

   # rt_count = RewardTransaction.where("#{date_zone} between date(?) and date(?) and restaurant_id in(?) #{reward_conditions} #{time_hour_scale} #{time_min_scale}",
  #                                     start_date, end_date, params['restaurant_id']).count
    puts file_name

   # loop = (rt_count.to_f / Setting.pagination.per_page.to_f).ceil


    reward_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << [
          "Reward id",
          "Code",
          "Name",
          "Date",
          "Time",
          "Restaurant Name",
          "Description"
          #"Latitude",
          #"Longitude"
      ]
      #1.upto(loop) do |page|
       # puts page
      results = RewardTransaction.select("distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
reward_transactions.restaurant_id,
 reward_transactions.user_id)  reward_transactions.*").where("date(timezone('EST', timezone('UTC', reward_transactions.created_at))) >=
'#{start_date}' AND date(timezone('EST', timezone('UTC', reward_transactions.created_at))) <= '#{end_date}'
AND  redeeming is FALSE AND restaurant_id in (?)  #{reward_conditions} #{time_hour_scale} #{time_min_scale}" ,
                                                             params['restaurant_id']).order("date(created_at) desc")





        #puts results
        results.each do |reward_transaction|

          csv << [
              reward_transaction.reward_id,
              reward_transaction.staffcode,
              reward_transaction.reward.name,
              reward_transaction.created_at.in_time_zone('EST').strftime('%D'),
              reward_transaction.created_at.in_time_zone('EST').strftime('%H:%M %z'),
              (reward_transaction.restaurant.dashboard_display_text rescue nil),
              (reward_transaction.reward.description rescue nil)
              #reward_transaction.latitude,
              #reward_transaction.longitude
          ]
        end
      #end

    end
    OwnerMailer.send_reward_redeemed(chain, email, "tmp/#{file_name}.csv", file_name).deliver!
    File.delete("tmp/#{file_name}.csv")
    puts "Success EXECUTING OwnerJob::RewardRedeemedJob"
  end

  def success

  end

  def error(job, exception)
    #job.update_column(:status,4)
  end
end