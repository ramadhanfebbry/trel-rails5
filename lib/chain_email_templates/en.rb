module ChainEmailTemplates

  class En
    def self.welcome_email_body(chain_name)
      "Thanks for signing up for #{chain_name}'s rewards program.

        To thank you for being a loyal customer.The #{chain_name} rewards app is an easy way to earn free food on your smartphone. Simply snap a pic of your receipt, and submit it through the app.
        If you have any feedback or other questions, simply reply to this email.

        Happy snapping and buen provecho!

        #{chain_name}

        -powered by Relevant"
    end

    def self.welcome_email_subject(chain_name)
      "Welcome to #{chain_name}"
    end

    def self.receipt_reject_email_body(chain_name)
      "Dear App User,

Thank you for your submission to our #{chain_name} app. We are sorry for the inconvenience, but unfortunately we are unable to process your receipt submission.

Reason: %{reject_reason}

Steps you can take:
Give us another shot at giving you rewards. If your receipt was not readable or partially cut off, please resubmit another picture of your receipt through the app. To avoid blurry images, wait for the camera to focus before snapping your picture.

Should you have any questions, simply reply to this email. Again, our apologies for the inconvenience and thank you for dining with us!

Regards,

#{chain_name}
App Technical Support

-powered by Relevant
      "
    end

    def self.receipt_reject_email_subject(chain_name)
      "#{chain_name} (Receipt# %{receipt_id}) - Unable to process receipt"
    end

    def self.forgot_password_email_body(chain_name)
      "Hi,

So you want to reset your password.  It's ok, it happens to all of us.
Below is your account information. You can use your temporary password and email address to login.

Password: %{temp_pwd}

Please reset this password after logging in from the app.

Sincerely,
#{chain_name}

-powered by Relevant"
    end

    def self.forgot_password_email_subject(chain_name)
      "Password reset request - #{chain_name}"
    end

    def self.welcome_chain_owner_email_subject(chain_name)
      "Welcome to #{chain_name}"
    end

    def self.welcome_chain_owner_email_body(chain_name)
      "Congratulations your account has been created please see the details below.

       Login email : %{chain_owner_email}
       Password : %{chain_owner_pwd}
       Chain : #{chain_name}

       %{chain_locations}

       You can use this link to access your dashboard: http://dashboard.relevantmobile.com
       Thank you
       If you have questions please respond to this email.

       Thank you
       Relevant"
    end

    def self.welcome_rest_owner_email_subject(chain_name)
      "Welcome to #{chain_name}"
    end

    def self.welcome_rest_owner_email_body(chain_name)
      "Congratulations your account has been created please see the details below.

       Login email : %{rest_owner_email}
       Password : %{rest_owner_pwd}
       Chain : #{chain_name}

       %{rest_owner_locations}

       You can use this link to access your dashboard: http://dashboard.relevantmobile.com
       Thank you
       If you have questions please respond to this email.

       Thank you
       Relevant"
    end

    def self.device_signup_welcome_email_body(chain_name)
      "Thanks for signing up for #{chain_name}'s rewards program.

        To thank you for being a loyal customer.The #{chain_name} rewards app is an easy way to earn free food on your smartphone. Simply snap a pic of your receipt, and submit it through the app.
        If you have any feedback or other questions, simply reply to this email.

        Happy snapping and buen provecho!

        #{chain_name}

        -powered by Relevant"
    end

    def self.device_signup_welcome_email_subject(chain_name)
      "Welcome to #{chain_name}"
    end


    def self.browser_signup_welcome_email_body(chain_name)
      "Thanks for signing up for #{chain_name}'s rewards program.

        To thank you for being a loyal customer.The #{chain_name} rewards app is an easy way to earn free food on your smartphone. Simply snap a pic of your receipt, and submit it through the app.
        If you have any feedback or other questions, simply reply to this email.

        Happy snapping and buen provecho!

        #{chain_name}

        -powered by Relevant"
    end

    def self.browser_signup_welcome_email_subject(chain_name)
      "Welcome to #{chain_name}"
    end

  end

end