module ChainEmailTemplates

  class Es
    def self.welcome_email_body(chain_name)
      "Gracias por inscribirte al programa de premios de #{chain_name}

Para agradecerte que seas un cliente leal, queremos recompensarte con un coctel gratis en cuanto subas tu primer recibo.

La APP de premios #{chain_name} es una manera facil de ganar deliciosos premios en comida y otras sorpresas con tu telefono inteligente. Simplemente toma una foto de tu recibo con la APP y mandanoslo.

Si tienes preguntas y cualquier comentario, simplemente contestanos a este mismo mail.

Atentamente,
#{chain_name}

-producido por Relevant"
    end

    def self.welcome_email_subject(chain_name)
      "Gracias por inscribirte al programa de premios de #{chain_name}"
    end

    def self.receipt_reject_email_body(chain_name)
      "Querido usuario de la APP #{chain_name},


Gracias por su ingreso a la APP de #{chain_name}. Disculpe por el inconveniente, pero desgraciadamente  nos es imposible procesar el ticket que ingreso como %{reject_reason}

Pasos que puede seguir:
Intente nuevamente. Si su recibo esta maltratado o cortado parcialmente, por favor reingrese el recibo usando la APP nuevamente. Para evitar imagenes borrosas, espere hasta que la camara enfoque antes de tomar la imagen.

Si tiene cualquier duda, simplemente mandenos un email. Una vez mas una disculpa por el inconveniente y gracias por comer en Moshi Moshi.


Premios,

#{chain_name}
App Soporte Tecnico


-producido por Relevant"
    end

    def self.receipt_reject_email_subject(chain_name)
      "#{chain_name} (Recibo# %{receipt_id}) - Imposible procesar el recibo"
    end

    def self.forgot_password_email_body(chain_name)
      "Hola,

Entonces quieres cambiar tu contrasena? No se preocupa, le sucede a todo el mundo.
A continuacion se muestra informacion de su cuenta. Se puede utilizar su contrasena temporal y la direccion de correo electronico para iniciar la sesion.

Contrasena: %{temp_pwd}

Favor de restablecer la contrasena despues de iniciar la sesion desde la aplicacion.

Atentamente,
#{chain_name}

-powered by Relevant"
    end

    def self.forgot_password_email_subject(chain_name)
      "Instrucciones para cambiar su contrasena #{chain_name}"
    end

    def self.welcome_chain_owner_email_subject(chain_name)
      "Welcome to #{chain_name}"
    end

    def self.welcome_chain_owner_email_body(chain_name)
      "Congratulations your account has been created please see the details below.

       Login email : %{chain_owner_email}
       Password : %{chain_owner_pwd}
       Chain : #{chain_name}

       %{chain_locations}

       You can use this link to access your dashboard: http://dashboard.relevantmobile.com
       Thank you
       If you have questions please respond to this email.

       Thank you
       Relevant"
    end

    def self.welcome_rest_owner_email_subject(chain_name)
      "Welcome to #{chain_name}"
    end

    def self.welcome_rest_owner_email_body(chain_name)
      "Congratulations your account has been created please see the details below.

       Login email : %{rest_owner_email}
       Password : %{rest_owner_pwd}
       Chain : #{chain_name}

       %{rest_owner_locations}

       You can use this link to access your dashboard: http://dashboard.relevantmobile.com
       Thank you
       If you have questions please respond to this email.

       Thank you
       Relevant"
    end

    def self.device_signup_welcome_email_body(chain_name)
      "Gracias por inscribirte al programa de premios de #{chain_name}

Para agradecerte que seas un cliente leal, queremos recompensarte con un coctel gratis en cuanto subas tu primer recibo.

La APP de premios #{chain_name} es una manera facil de ganar deliciosos premios en comida y otras sorpresas con tu telefono inteligente. Simplemente toma una foto de tu recibo con la APP y mandanoslo.

Si tienes preguntas y cualquier comentario, simplemente contestanos a este mismo mail.

Atentamente,
#{chain_name}

-producido por Relevant"
    end

    def self.device_signup_welcome_email_subject(chain_name)
      "Gracias por inscribirte al programa de premios de #{chain_name}"
    end

    def self.browser_signup_welcome_email_body(chain_name)
      "Gracias por inscribirte al programa de premios de #{chain_name}

Para agradecerte que seas un cliente leal, queremos recompensarte con un coctel gratis en cuanto subas tu primer recibo.

La APP de premios #{chain_name} es una manera facil de ganar deliciosos premios en comida y otras sorpresas con tu telefono inteligente. Simplemente toma una foto de tu recibo con la APP y mandanoslo.

Si tienes preguntas y cualquier comentario, simplemente contestanos a este mismo mail.

Atentamente,
#{chain_name}

-producido por Relevant"
    end

    def self.browser_signup_welcome_email_subject(chain_name)
      "Gracias por inscribirte al programa de premios de #{chain_name}"
    end

  end

end