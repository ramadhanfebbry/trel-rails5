class ExternalParser::ExParser
  attr_accessor :data_request

  # x = ExternalParser::ExParser.new(data_request)
  def initialize(data_request = nil)
    @data_request = data_request
  end

  # :order => {"id"=>"741ef79c-b590-4aa5-852b-30260c07b93a",
  # "restaurant_id"=>"e423b065-a69c-4507-b4f6-a737013d1818",
  # "menu_type"=>"ToGo",
  # "order_items"=>[{"id"=>"a28d399c-7a3f-436b-9da9-4a210379d529", "menu_item_id"=>"82e547e1-dde7-4ba9-8301-d7396c90d436",
  # "title"=>"Cheesesteak", "price"=>7.09, "discounted_price"=>7.09, "discount"=>0.0, "quantity"=>1}],
  # "totals"=>{"discount_amount"=>2.49, "subtotal"=>7.09, "discounted_subtotal"=>7.09, "grand_total"=>7.5,
  # "tip"=>0.0, "delivery_fee"=>0.0,
  # "taxes"=>{"SalesTax"=>0.41}, "discounts"=>nil, "total_taxes"=>0.41, "total_price"=>7.5, "payments"=>nil}},
  #  :order_partner => “nuorder”
  # }


  def order_id
    return @data_request["id"]
  end

  def restaurant_id
    return  @data_request["restaurant_id"]
  end

  def menu_type
    return @data_request["menu_type"]
  end

  def order_items
    @data_request["order_items"]
  end

  def get_subtotal
    @data_request["totals"]["subtotal"]
  end

  def get_tax
    @data_request["totals"]["taxes"]["SalesTax"] rescue 0
  end

  def get_payment
    @data_request["payments"]
  end

  def menu_items
    @data_request["order_items"]
  end
end
