# encoding: UTF-8
class Parser::CbcOloParser
  require 'rest-client'
  require 'nokogiri'

  attr_accessor :html_data, :result, :open_struct_result

  ## example
   # a = Parser::CbcOloParser.new("")
   # a.parse_html
  def initialize(html_data = nil)
    #html_data = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\"><style type=\"text/css\" style=\"display:none;\"><!-- P {margin-top:0;margin-bottom:0;} --></style></head><body dir=\"ltr\"><div id=\"divtagdefaultwrapper\" style=\"font-size:12pt;color:#000000;background-color:#FFFFFF;font-family:Calibri,Arial,Helvetica,sans-serif;\"><p><br></p>Catering Delivery, House Account<br><br><div style=\"color: rgb(0, 0, 0);\"><hr tabindex=\"-1\" style=\"display:inline-block; width:98%\"><div id=\"divRplyFwdMsg\" dir=\"ltr\"><font face=\"Calibri, sans-serif\" color=\"#000000\" style=\"font-size:11pt\"><b>From:</b> noreply@onosys.com &lt;noreply@onosys.com&gt;<br><b>Sent:</b> Wednesday, November 11, 2015 8:15 PM<br><b>To:</b> CB - McPherson Square<br><b>Subject:</b> McPherson Square - K St. Online Order</font><div>&nbsp;</div></div><div><div align=\"center\" style=\"text-align:center\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: normal; text-align: left; background: rgb(238, 238, 238);\"><tbody><tr><td style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"700\" align=\"center\" style=\"text-align: left; background: rgb(255, 255, 255);\"><tbody><tr><td style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"text-align: left;\"><tbody><tr><td style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><img alt=\"\" style=\"-webkit-user-select: none;\" src=\"https://orderonline.cornerbakerycafe.com/Content/images/email/email_header.jpg\"></td></tr></tbody></table><table cellpadding=\"4\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"text-align: left;\"><tbody><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"padding-right: 20px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Restaurant Name</b></td><td valign=\"top\" width=\"100%\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">McPherson Square - K St.</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Address</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">1425 K St NW<br>Washington D.C., DC 20005<br>(202) 898-5704</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Order Number</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">1311856</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Date Placed</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">11/11/2015 8:15:29 PM</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Type</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">Delivery</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Expected Date</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">11/12/2015</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Pick Up Time</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">12:15 PM</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Customer Name</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">Lolonyo Carter</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Customer Phone</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">(202) 696-1000</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Customer Address</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">1101 Vermont Avenue, NW<br>11th FL<br>Washington, DC 20005<br>US<br>(202) 696-1000</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Customer Email</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">lcarter@impaqint.com</td></tr><tr><td valign=\"top\" nowrap=\"nowrap\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><b>Payment Info</b></td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"paymentInfo\" style=\"text-align: left;\"><tbody><tr><td valign=\"top\" style=\"padding-right: 35px; font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0); padding-bottom: 14px; border-bottom-style: none;\">House Account </td><td valign=\"top\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0); padding-bottom: 14px; border-bottom-style: none;\">Amount: $168.81 </td></tr></tbody></table></td></tr></tbody></table><table cellpadding=\"4\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"font-weight: bold; text-align: left;\"><tbody><tr><td colspan=\"3\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><hr style=\"border:none; height:2px; background:#999\"></td></tr><tr><th align=\"left\" style=\"font-family:Arial,Helvetica,Sans-Serif; font-size:18px; color:#333\">Product Name</th><th align=\"center\" style=\"font-family:Arial,Helvetica,Sans-Serif; font-size:18px; color:#333\">Qty</th><th align=\"right\" style=\"font-family:Arial,Helvetica,Sans-Serif; font-size:18px; color:#333\">Price</th></tr><tr><td colspan=\"3\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><hr style=\"border:none; height:2px; background:#999\"></td></tr><tr><td valign=\"top\" align=\"left\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">Sandwich Basket</td><td valign=\"top\" align=\"center\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">1</td><td valign=\"top\" align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">$140.00 </td></tr><tr><td valign=\"top\" colspan=\"3\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><div style=\"margin-left:5px\"><span style=\"font-weight:bold; display:inline\">Size::<br></span><div style=\"margin-left:5px; font-weight:normal\"><div>&nbsp;&nbsp;&nbsp;&nbsp;Medium (24 Half Sandwiches) <span style=\"color:#999\">&nbsp;($120.00)</span><div style=\"margin-left:5px\"><span style=\"font-weight:bold; display:inline\">Choose Sandwiches::<br></span><div style=\"margin-left:5px; font-weight:normal\"><div>&nbsp;&nbsp;&nbsp;&nbsp;2 x Chicken Pesto </div><div>&nbsp;&nbsp;&nbsp;&nbsp;2 x Ham on Pretzel </div><div>&nbsp;&nbsp;&nbsp;&nbsp;2 x Turkey on Pretzel </div><div>&nbsp;&nbsp;&nbsp;&nbsp;3 x Tomato Mozzarella </div><div>&nbsp;&nbsp;&nbsp;&nbsp;2 x Turkey &amp; Swiss </div><div>&nbsp;&nbsp;&nbsp;&nbsp;Mom's Roast Beef on Sesame Roll </div></div></div></div></div></div><div style=\"margin-left:5px\"><span style=\"font-weight:bold; display:inline\">Side::<br></span><div style=\"margin-left:5px; font-weight:normal\"><div>&nbsp;&nbsp;&nbsp;&nbsp;Chips </div></div></div><div style=\"margin-left:5px\"><span style=\"font-weight:bold; display:inline\">Add Soup:<br></span><div style=\"margin-left:5px; font-weight:normal\"><div>&nbsp;&nbsp;&nbsp;&nbsp;NEW! Butternut Squash Soup <span style=\"color:#999\">&nbsp;($20.00)</span> </div></div></div></td></tr><tr><td colspan=\"3\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"><hr style=\"border:none; height:1px; background:#ddd\"></td></tr><tr><td style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"></td><td align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">Subtotal: </td><td align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">$140.00 </td></tr><tr><td style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"></td><td align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">Delivery Fee: </td><td align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">$14.00 </td></tr><tr><td style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"></td><td align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">Approx. Sales Tax: </td><td align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">$14.81 </td></tr><tr><td style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"></td><td align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">Total: </td><td align=\"right\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\">$168.81 </td></tr></tbody></table><table cellpadding=\"4\" cellspacing=\"0\" border=\"0\" width=\"100%\" style=\"color: rgb(153, 153, 153); padding-top: 20px; padding-bottom: 10px; text-align: left;\"><tbody><tr><td style=\"font-family: Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal; color: rgb(0, 0, 0);\"></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></div></div></div></div></body></html>" if html_data.blank?

    html_data = "  <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'> <html xmlns='http://www.w3.org/1999/xhtml'> <head runat='server'> <title>Corner Bakery Cafe Online Ordering</title> <style type='text/css' media='all'> html, body { margin: 0; padding: 0; font-family: Arial, Helvetica, Sans-Serif; color: ; } table { text-align: left; } td { font-family: Arial, Helvetica, Sans-Serif; font-size: 13px; font-weight: normal; color: #000; } a, a:visited { color: #0000FF; } #paymentInfo { } #paymentInfo tr td { padding-bottom: 14px; } #paymentInfo tr + tr td { padding-top: 14px; border-top: 1px dashed #ddd; } #paymentInfo tr:last-child td { border-bottom: none; } </style> </head> <body style='margin: 0; padding: 0;'> <div style='text-align: center;' align='center'>         <table class='ConfirmationEmail' cellpadding='0' cellspacing='0' border='0' width='100%' style='background: #eee; font-family: Arial, Helvetica, Sans-Serif; font-size: 12px; font-weight: normal;'>         <tr>         <td>         <table cellpadding='0' cellspacing='0' border='0' width='700' style='background: #fff;' align='center'>         <tr>         <td>         <table cellpadding='0' cellspacing='0' border='0' width='100%'>         <tr>         <td><img src='https://orderonline.cornerbakerycafe.com/Content/images/email/email_header.jpg' alt='' /></td>         </tr>         </table>         <table cellpadding='4' cellspacing='0' border='0' width='100%' style='text-align: left;'>         <tr>         <td valign='top' nowrap='nowrap' style='padding-right: 20px;'><b>Restaurant Name</b></td>         <td valign='top' width='100%' style='color: #000;'>Xpient CBC Test Lab</td>         </tr>         <tr>         <td valign='top' nowrap='nowrap'><b>Address</b></td>         <td valign='top' style='color: #666;'>1607 South Harrison Street<br />Amarillo, TX 79102<br />(111) 222-3333</td>         </tr>         <tr>         <td valign='top' nowrap='nowrap'><b>Order Number</b></td>         <td valign='top' style='color: #666;'>1331150</td>         </tr>         <tr>         <td valign='top' nowrap='nowrap'><b>Date Placed</b></td>         <td valign='top' style='color: #666;'>11/20/2015 3:29:57 PM</td>         </tr>         <tr>         <td valign='top' nowrap='nowrap'><b>Type</b></td>         <td valign='top' style='color: #666;'>Pickup</td>         </tr>          <tr>         <td valign='top' nowrap='nowrap'><b>Expected Date</b></td>         <td valign='top' style='color: #666;'>Today</td>         </tr>         <tr>         <td valign='top' nowrap='nowrap'><b>Pick Up Time</b></td>         <td valign='top' style='color: #666;'>6:00 PM</td>         </tr>         <tr>         <td valign='top' nowrap='nowrap'><b>Customer Name</b></td>         <td valign='top' style='color: #666;'>Tyler Borenstein</td>         </tr>         <tr>         <td valign='top' nowrap='nowrap'><b>Customer Phone</b></td>         <td valign='top' style='color: #666;'>(347) 885-5717</td>         </tr> <tr> <td valign='top' nowrap='nowrap'><b>Customer Email</b></td> <td valign='top' style='color: #666;'>cb111@slickeats.com</td> </tr>                                                             <tr>             <td valign='top' nowrap='nowrap'> </td>             <td valign='top' style='color: #666;'>FIRST ORDER</td>             </tr>         <tr>         <td valign='top' nowrap='nowrap'><b>Payment Info</b></td>         <td valign='top' style='color: #666;'>         <table cellpadding='0' cellspacing='0' border='0' id='paymentInfo'>         <tr>         <td valign='top' style='color: #666; padding-right: 35px; font-size: .9em;'> Visa<br /> ****-****-****-1111        </td>         <td valign='top' style='color: #666;'> Amount: $9.68                                                                </td>             </tr>         </table>         </td>         </tr>         </table>         <table cellpadding='4' cellspacing='0' border='0' width='100%' style='font-weight:bold;'>         <tr>         <td colspan='3'><hr style='border: none; height: 2px; background: #999;' /></td>         </tr>         <tr>         <th align='left' style='font-family: Arial, Helvetica, Sans-Serif; font-size: 18px; color: #333;'>Product Name</th>         <th align='center' style='font-family: Arial, Helvetica, Sans-Serif; font-size: 18px; color: #333;'>Qty</th>         <th align='right' style='font-family: Arial, Helvetica, Sans-Serif; font-size: 18px; color: #333;'>Price</th>         </tr>         <tr>         <td colspan='3'><hr style='border: none; height: 2px; background: #999;'  /></td>         </tr>         <tr>         <td valign='top' align='left' style='color: #000;'>Signature Salad Combo</td>         <td valign='top' align='center' style='color: #000;'>1</td>         <td valign='top' align='right' style='color: #000;'>         $8.79         </td>         </tr>  <tr style='color: #666; font-size: 11px;'> <td valign='top' align='left'>Harvest Salad</td> </tr>  <tr> <td valign='top' colspan='3' style='color: #666; font-size: 11px;'>      <div style='margin-left:5px;'> <span style='font-weight: bold;display: inline;'>Customize Dressing: <br /></span> <div style='margin-left:5px; font-weight: normal;'>     <div>    Balsamic Vinaigrette       </div>  </div> </div>      <div style='margin-left:5px;'> <span style='font-weight: bold;display: inline;'>Customize Produce: <br /></span> <div style='margin-left:5px; font-weight: normal;'>     <div>    Green Apples       </div>     <div>    Mixed Greens Lettuce       </div>  </div> </div>      <div style='margin-left:5px;'> <span style='font-weight: bold;display: inline;'>Customize Toppings: <br /></span> <div style='margin-left:5px; font-weight: normal;'>     <div>    Currants       </div>     <div>    Toasted Walnuts       </div>  </div> </div>      <div style='margin-left:5px;'> <span style='font-weight: bold;display: inline;'>Customize Cheese: <br /></span> <div style='margin-left:5px; font-weight: normal;'>     <div>    Bleu Cheese       </div>  </div> </div> </td> </tr> <tr style='color: #666; font-size: 11px;'> <td valign='top' align='left'>Cheddar Broccoli Soup</td> </tr>           <tr>         <td colspan='3'><hr style='border: none; height: 1px; background: #ddd;' /></td>         </tr>         <tr>             <td></td>             <td align='right' style='font-weight:normal; font-size: 18px; color:#000'>         Subtotal:             </td>             <td align='right' style='font-weight:normal; font-size: 18px; color:#000'>         $8.79             </td>         </tr>                                 <tr>                     <td></td>             <td align='right' style='font-weight:normal; font-size: 18px; color:#000'>                 Approx. Sales Tax:                     </td>             <td align='right' style='font-weight:normal; font-size: 18px; color:#000'>                 $0.89                     </td>                 </tr>                 <tr>           <td></td>           <td align='right' style='font-weight:normal; font-size: 18px; color:#000'>         Total:           </td>           <td align='right' style='font-weight:normal; font-size: 18px; color:#000'>         $9.68           </td>         </tr>         </table>         <table cellpadding='4' cellspacing='0' border='0' width='100%' style='color: #999; padding-top:20px; padding-bottom: 10px;'>         <tr>         <td></td>         </tr>         </table>         </td>         </tr>         </table>         </td>         </tr>         </table> </div> </body> </html>    " if html_data.blank?
    html_data = html_data.gsub(/\r/," ").gsub(/\n/,'').gsub(/\t/,'').gsub(/\"/,"'")
    @html_data = html_data
  end

  def parse_html
    puts @html_data.class
    doc = Nokogiri::HTML(@html_data)
    #doc = Nokogiri::HTML(html)
    rows =  doc.xpath('//table[@cellpadding="4"]/tr')
    ret = {}
    details = rows.each_with_index do |row, index|
      key =  row.at_xpath('td[1]/b/text()').to_s.strip
      unless key.empty?
        conf = {
            "Restaurant Name"=>'td[2]/text()',
            "Address"=>'td[2]/text()',
            "Order Number"=>'td[2]/text()',
            "Date Placed"=>'td[2]/text()',
            "Type"=>'td[2]/text()',
            "Expected Date"=>'td[2]/text()',
            "Pick Up Time"=>'td[2]/text()',
            "Customer Name"=>'td[2]/text()',
            "Customer Phone"=>'td[2]/text()',
            "Customer Address"=>'td[2]/text()',
            "Customer Email"=>'td[2]/text()',
            "Payment Info"=> '//table[@id="paymentInfo"]/tr/td/text()'
        }
        ret[key] =  if conf[key].class == Array
                      info = ""
                      conf[key].each do |s|
                        info += row.at_xpath(s).to_s.strip + " "
                      end
                      info
                    else
                      row.at_xpath(conf[key]).to_s.strip
                    end
      end

    end
    #pp ret
    #pp "-"*100

    rowss =  doc.xpath('//table[@cellpadding="4"]/tr')
    ret_order = {}
    details = rowss.each_with_index do |rowd, index|
      key =  rowd.at_xpath('td[2]/text()').to_s.strip

      if !key.empty? && "#{key.to_i}" != key
        conf = {
            "Subtotal:"=>'td[3]/text()',
            "Subtotal::" => 'td[3]/text()',
            "Delivery Fee:"=>'td[3]/text()',
            "Approx. Sales Tax:"=>'td[3]/text()',
            "Total:"=>'td[3]/text()'
        }
        ret_order[key] =  if conf[key].class == Array
                            info = ""
                            conf[key].each do |s|
                              info += rowd.at_xpath(s).to_s.strip + " "
                            end
                            info
                          else
                            rowd.at_xpath(conf[key]).to_s.strip
                          end
      end

    end
    @result = ret_order.merge!(ret)
    struct_result
  end

  def struct_result
    # {"Subtotal:"=>"$140.00", "Delivery Fee:"=>"$14.00", "Approx. Sales Tax:"=>"$14.81", "Total:"=>"$168.81",
    # "Restaurant Name"=>"McPherson Square - K St.",
    # "Address"=>"1425 K St NW",
    # "Order Number"=>"1311856", "Date Placed"=>"11/11/2015 8:15:29 PM", "Type"=>"Delivery",
    # "Expected Date"=>"11/12/2015", "Pick Up Time"=>"12:15 PM", "Customer Name"=>"Lolonyo Carter",
    # Customer Phone"=>"(202) 696-1000", "Customer Address"=>"1101 Vermont Avenue, NW",
    # "Customer Email"=>"lcarter@impaqint.com", "Payment Info"=>"House Account"}
    os = OpenStruct.new

    os.subtotal = @result["Subtotal:"] rescue nil
    
    if os.subtotal.blank?
      os.subtotal = @result["Subtotal::"] rescue nil
    end
    os.total = @result["Total"] rescue nil
    os.restaurant = @result["Restaurant Name"].split('-').first.squish rescue nil
    os.total = @result["Total:"] rescue nil
    os.order_no = @result["Order Number"] rescue nil
    os.email = @result["Customer Email"] rescue nil
    os.payment_type = @result["Payment Info"] rescue nil

    os.tax = @result["Approx. Sales Tax:"] rescue nil
    os.is_payed = !@result["Payment Info"].downcase.include?("pay at restaurant") rescue false
    date_tmp = @result["Expected Date"].include?("Today") ?  Date.strptime(@result["Date Placed"],"%M/%d").to_date.strftime("%m/%d/%Y") :  @result["Expected Date"] rescue nil
    os.date_pickup =  date_tmp +" "+ @result["Pick Up Time"] rescue nil
    os.chain_id = parse_chain_id
    os.user_id = parse_user_id
    os.restaurant_id = parse_restaurant_id


    @open_struct_result = os
  end

  def parse_user_id
    email = @result["Customer Email"] rescue nil
    return false if email.blank?
    chain_id = parse_chain_id
    user_id = User.where("email = ? and chain_id = ?", email,chain_id).first.id rescue nil
    return user_id
  end

  def parse_chain_id
    if @html_data.include?("cornerbakerycafe")
      return Chain.where("LOWER(name) like '%corner%'").first.id rescue nil
    else
      return Chain.where("LOWER(name) like '%corner%'").first.id rescue nil
    end
  end

  def parse_restaurant_id
    name = @result["Restaurant Name"].split('-').first.squish rescue nil
    return false if name.blank?
    chain_id = parse_chain_id
    restaurant_id = Restaurant.where("name like  ? and chain_id = ?","%#{name}%", chain_id).first.id rescue nil

    return restaurant_id
  end
end