class Parser::OfferItemsParser
  require 'rest-client'
  require 'nokogiri'

  attr_accessor :xml_data, :result


  def initialize(pos_check_upload = nil)
    @xml_data = xml_data
    @pos_check_upload = pos_check_upload
  end


  def find_item_number_and_quantity

    # POS_USED_TYPE = {
    #     "MICROS" => 1,
    #     "XPIENT" => 2,
    #     "NCR" => 3,
    #     "TREATWARE" => 4
    # }
    @result = []

    if @pos_check_upload.class == OloWebhook
      @result = olo_orders
      @result = remove_menu_item_discount(@result)
      return @result
    end

    if @pos_check_upload.blank? or @pos_check_upload.pos_used_type == Chain::POS_USED_TYPE["MICROS/NORTHKEY"] # micros
      json_check = YAML.load(@pos_check_upload.xml_data) rescue nil

      if json_check.class != ActiveSupport::HashWithIndifferentAccess
        doc = Nokogiri::XML(@xml_data)
        a = []

        if doc.search("menu-item").count > 0
          doc.search("menu-item").each do |x|
            a << {:id => x.attributes["id"].value, :quantity => x.children.children.first.text.to_i,
                  :price => x.search('total-amount').text.to_f}
          end
        end
        @result = a
      else
        a = []
        pos_xml = PosReceiptJson.new(@pos_check_upload.xml_data)
        items = pos_xml.list_item
        unless items.blank?
          items.each do |x|
            a << {:id => x["id"], :quantity => x["qty"], :price => x["amount"]}
          end
        end
        @result = a
      end
    end

    ## end micros

    ## treatware begin
    if !@pos_check_upload.blank? and @pos_check_upload.pos_used_type == Chain::POS_USED_TYPE["TREATWARE"] # TREATWARE
      @result = treatware_matching_items
    end
    ## treatware end

    ## xpient begin
    if !@pos_check_upload.blank? and @pos_check_upload.pos_used_type == Chain::POS_USED_TYPE["XPIENT"] # XPIENT
      @result = xpient_matching_items
    end
    ## xpient end


    ## no pos
    if !@pos_check_upload.blank? and @pos_check_upload.pos_used_type == Chain::POS_USED_TYPE["NO POS"] # XPIENT
      @result = nopos_matching_items
    end

    @result = remove_menu_item_discount(@result)
    return @result
  end

  ## for treatware
  def treatware_matching_items
    result = []
    json_check = YAML.load(@pos_check_upload.xml_data)
    treatware_check = TreatwareCheck.new(json_check)
    items_check_buy = TreatwareCheck.get_line_items(@pos_check_upload.chain, treatware_check.line_items)
    unless items_check_buy.blank?
      items_check_buy.each do |res|
        result << {:id => res[:menu_item_id], :quantity => res[:quantity], :price => res[:price]}
      end
    end
    return result
  end

  def xpient_matching_items
    result = []
    @pos_check_upload = @pos_check_upload.reload
    json_check = YAML.load(@pos_check_upload.xml_data)
    xpient_check = XpientCheck.new(json_check)
    items_check_buy = TreatwareCheck.get_line_items(@pos_check_upload.chain, xpient_check.line_items)

    unless items_check_buy.blank?
      items_check_buy.each do |res|
        result << {:id => res[:menu_item_id], :quantity => res[:quantity], :price => res[:price]}
        begin
          unless res[:child_items].blank?
            res[:child_items].each do |child|
              result << {:id => child[:menu_item_id], :quantity => child[:quantity], :price => child[:price]}
            end
          end
        rescue
          next
        end
      end
    end
    return result
  end

  def nopos_matching_items
    a = []
    doc = Nokogiri::XML(@pos_check_upload.xml_data)
    if doc.search("menu-item").count > 0
      doc.search("menu-item").each do |x|
        a << {:id => x.attributes["id"].value, :quantity => x.children.children.first.text.to_i, :price => x.search('total-amount').text.to_f}
      end
    end
    return a
  end

  def remove_menu_item_discount(result)
    return result.select { |x| !x[:id].blank? }
  end

  def olo_orders
    puts "OFFER ITEM PARSER :: OLO ORDERS -----------"
    result = []
    parse_result = OloParser::ParserOlo.new(@pos_check_upload.content)
    res = parse_result.menu_items
    puts "menu items === #{res}"
    unless res.blank?
      res.each do |r|
        result << {:id => r["productId"], :quantity => r["quantity"], :price => r["sellingPrice"]}
      end
    end
    puts "END OFFER ITEM PARSER :: OLO ORDERS -----------"
    return result
  end

  # sample xml
  # '<check xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" code="3956329" code_type="usercode" obj_num="3001" seq_num="43903" id="3001" check-id="JDGJD" xmlns="ltt:ABC"><meal><open-time>10/27/15 19:00:40</open-time><close-time /><date>10/27/15 19:01:15</date><meal-period>0</meal-period><cover-count>0</cover-count><emp-num>12</emp-num><rvc-num>1</rvc-num><rvc-name>NEKTER</rvc-name><order-type-num>3</order-type-num><order-type-name>To Go</order-type-name><payment-total>9.50</payment-total><tax-total>0.00</tax-total><tip-total>0.00</tip-total><discount-total>0.00</discount-total><sales-total>9.50</sales-total></meal><payment><total>0</total><direct-tips>0</direct-tips><tax>0</tax><item-sales>0</item-sales></payment><menu-items><menu-item id="6019"><quantity>1</quantity><name>Grn Apple Detox</name><total-amount>9.50</total-amount></menu-item><menu-item id="500011"><quantity>1</quantity><name>ADD ICE</name><total-amount>0.00</total-amount></menu-item><menu-item id="500019"><quantity>1</quantity><name>SPECIAL PREP</name><total-amount>0.00</total-amount></menu-item></menu-items><discounts /><tenders><tender id="1209"><value>0.00</value><type>CodeTender</type><last-four-digits>3956329</last-four-digits></tender><tender id="1"><value>20.00</value><type>CASH</type><last-four-digits>3956329</last-four-digits></tender><tender id="1"><value>-10.50</value><type>CASH</type><last-four-digits>3956329</last-four-digits></tender></tenders><service-charges /></check>'
end