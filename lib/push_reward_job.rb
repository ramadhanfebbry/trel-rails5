class PushRewardJob < Struct.new(:push_reward,:user_ids,:admin_email, :push_reward_id)

  include PushNotificationData

  def perform
    puts "PERFORM ==== PUSHREWARDJOB"
    #puts user_selected.class
    #user_selected_test = user_selected[0..10]
    success_logs = []
    success_users = []
    error_logs = []
    p push_reward
    push_reward = PushReward.find push_reward_id
    p push_reward
    chain = push_reward.chain
    p chain
    reward = push_reward.reward
    admin_email = "admin / customer support pushed by #{push_reward.admin.email}" rescue "admin email not found"
    p reward
    status_reward =  check_reward_status(reward)
    user_ids.each do |user_id|
      puts "#{user_id}"
      if status_reward[:send]
        begin
          user = User.where("id = ? and active is true", user_id).first
          p user
          if user && user.chain.eql?(reward.chain) && reward.reward_type.eql?(Reward::TYPES["GIFTABLE"]) &&
            user.reward_wallets.where("reward_id = ? AND status = ? AND expiry_date > ?", reward.id, RewardWallet::STATUS[:ACTIVE], Time.current).blank?
            RewardWallet.create_with_delay({:reward_id => reward.id,
                                            :user_id => user.id,
                                            :gifter => true, :description => admin_email}, true)
            RewardSentHistory.create(:reward_id => reward.id,
                                     :user_id => user.id,
                                     :chain_id => reward.chain.id
            )
            success_logs << { :user_id => user_id, :notification => "", :email => "", :reward => "Success", :log => "Pushed reward #{reward.name}", :created_at => Time.zone.now }
            success_users << user

          elsif user && user.chain.eql?(reward.chain) && !reward.reward_type.eql?(Reward::TYPES["GIFTABLE"])
            RewardWallet.create_with_delay({:reward_id => reward.id,
                                            :user_id => user.id,
                                            :description => admin_email}, true)

            RewardSentHistory.create(:reward_id => reward.id, :user_id => user.id, :chain_id => reward.chain.id)
            success_logs << { :user_id => user_id, :notification => "", :email => "", :reward => "Success", :log => "Pushed reward #{reward.name}", :created_at => Time.zone.now }
            success_users << user

          elsif user && user.chain.eql?(reward.chain) && reward.reward_type.eql?(Reward::TYPES["GIFTABLE"])
            error_logs << {:user_id => user_id, :email => (user.email rescue ""), :log => "User has Giftable Reward #{reward.id} that still active, Can't have 2 same reward that still active, not gifted yet.", :created_at => Time.zone.now}

          else
            error_logs << {:user_id => user_id, :email => (user.email rescue ""), :log => "User not included on chain that selected. User chain is #{user.chain.name}, but selected chain is  #{chain.name}", :created_at => Time.zone.now}

          end
        rescue => e
          puts "PUSHREWARDJOB::ERROR => #{e.inspect}"
          user = User.find(user_id) rescue nil
          error_logs << {:user_id => user_id, :email => (user.email rescue ""), :log => "It can be 2 reason, #{user_id} doesn't exist or user with id #{user_id} is inactive", :created_at => Time.zone.now}
        end
      else
        user = User.find(user_id) rescue nil
        error_logs << {:user_id => user_id, :email => (user.email rescue ""), :log => "Reward #{reward.name} is expired"}
      end

    end unless user_ids.blank?

    if status_reward[:send]
      executed_time = Time.zone.now#.end_of_day
      executed_time = executed_time + reward.hours_delay.hours if status_reward[:set_run_at] && reward.hours_delay.to_f > 0
      p executed_time


      emails_and_device_tokens_group = RewardNotification.group_emails_and_device_token_by_locale(success_users)
      notification_by_locale = RewardNotification.group_message_pn_by_locale(reward)

      success_logs = RewardNotification.set_valid_pushed_for_each_user(success_logs)
      success_logs = RewardNotification.set_status_notif_when_reward_notif_not_set(success_logs, notification_by_locale)
      if chain.admin_push_reward_notif_to_device
        Delayed::Job.enqueue(GcmAndroidJob.new(reward, emails_and_device_tokens_group, notification_by_locale, status_reward[:exp_at].strftime("%m/%d/%Y")), chain_id: chain.id, delayable_type: reward.class.to_s,	delayable_id: reward.id, run_at: executed_time)
        Delayed::Job.enqueue(PushIphoneJob.new(reward, emails_and_device_tokens_group, notification_by_locale,status_reward[:exp_at].strftime("%m/%d/%Y")), chain_id: chain.id, delayable_type: reward.class.to_s,	delayable_id: reward.id, run_at: executed_time)
      else
        success_logs.each do |succ_log|
          succ_log[:notification] = "N/A"
        end
      end

      if chain.admin_push_reward_notif_to_email
        notif_setting = chain.notification_settings.where(email_template: "admin_push_reward_notification").first rescue nil
        p "=========================== notif setting from lib push reward job ==========================="

        unless notif_setting.blank?
          if notif_setting.send_type.eql?(1)
            Delayed::Job.enqueue(PushRewardMailJob.new(reward, emails_and_device_tokens_group, notification_by_locale, status_reward[:exp_at].strftime("%m/%d/%Y")), chain_id: chain.id, delayable_type: reward.class.to_s,	delayable_id: reward.id, run_at: executed_time)
          elsif notif_setting.send_type.eql?(2)
            Delayed::Job.enqueue(PushRewardMandrillJob.new(reward, emails_and_device_tokens_group, notification_by_locale, status_reward[:exp_at].strftime("%m/%d/%Y"), notif_setting), chain_id: chain.id, delayable_type: reward.class.to_s,	delayable_id: reward.id, run_at: executed_time)
          elsif notif_setting.send_type.eql?(3)
            #for fishbowl
          end
        else
          Delayed::Job.enqueue(PushRewardMailJob.new(reward, emails_and_device_tokens_group, notification_by_locale, status_reward[:exp_at].strftime("%m/%d/%Y")), chain_id: chain.id, delayable_type: reward.class.to_s,	delayable_id: reward.id, run_at: executed_time)
        end
      else
        success_logs.each do |succ_log|
          succ_log[:email] = "N/A"
        end
      end
    end

    success_log_entry = PushLogEntry.new(:pushable_id => push_reward.id, :pushable_type => push_reward.class.to_s, :log_type => 1)
    success_log_entry.logs = success_logs
    success_log_entry.save

    error_log_entry = PushLogEntry.new(:pushable_id => push_reward.id, :pushable_type => push_reward.class.to_s, :log_type => 2)
    error_log_entry.logs = error_logs
    error_log_entry.save

    dj_count = DelayedJob.where(:delayable_id => push_reward.id, :delayable_type => push_reward.class.to_s).count

    # send email if job has done
    if dj_count < 2
      unless ENV["DEFAULT_HOST_EMAIL"].to_s.blank?
        RewardMailer.admin_push_notification("Push Reward admin ID:#{push_reward.id}",
                                             "Push Reward admin ID:#{push_reward.id}",
                                             push_reward, "END @").deliver! #unless last_execution.blank?
      end
    end


    puts "FINISHED PERFORM ==== PUSHREWARDJOB"
  end

  #def success(job)
  #  puts "PUSHREWARDJOB:: CHAIN= #{find_chain}"
  #  puts "PUSHREWARDJOB:: user_failed= #{@error_logs}"
  #  path = write_to_csv
  #  RewardMailer.push_reward_success_nofitication(@success_users,@error_logs,find_chain,"Push Reward #{reward.name} to chain #{find_chain.name} is successfull",path,admin_email).deliver
  #  File.delete(path)
  #  puts "mailer success"
  #end

  #def error(job, exception)
  #  job = job.delayable_type.classify.constantize.find(job.delayable_id)
  #  error_text = job.last_error
  #  UserMailer.reward_error(emails, error_text).deliver
  #end

  private

  def check_reward_status(reward)
    reward_obj = reward
    tmp_expired_at = RewardWallet.get_expiry_date(reward_obj)
    if !reward_obj.expired and reward_obj.status == "active"
      if Reward::TYPES["REGULAR"] != reward_obj.reward_type and reward_obj.hours_delay.to_f > 0 and
          reward_obj.status == "active"
         return {:send => true, :set_run_at => true, :exp_at => tmp_expired_at}
      else
        return {:send => true, :set_run_at => false, :exp_at => tmp_expired_at}
      end
    else
      return {:send => false}
    end
  end
  #
  #def user_list
  #  @user = user_selected
  #end
  #
  #def write_to_csv
  #  path = "tmp/push_reward_#{find_chain.name}.csv"
  #  CSV.open("tmp/push_reward_#{find_chain.name}.csv", "w") do |csv|
  #    csv << [
  #        "Success","Failed"
  #    ]
  #    if @success_users.size >= @error_logs.size
  #      @success_users.each_with_index do |user,index|
  #        user_failed = @user_failed[index] rescue ""
  #        csv << [user.id,user_failed]
  #      end
  #    else
  #      @user_failed.each_with_index do |user,index|
  #        user_success = @user_success[index] rescue ""
  #        csv << [user_success,user[:user_id]]
  #      end
  #    end
  #  end
  #  return path
  #end
  #
  #
  #def find_chain
  #  @chain = Chain.find chain_id
  #end

end
