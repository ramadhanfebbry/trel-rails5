class DailyImportRestaurantsJob < Struct.new(:chain, :files_executed)
  require 'net/ftp'
  def perform
    targetnames = []
    file_type = ""
    targetname = ""
    p files_executed
    ftp =Net::FTP.new
    ftp.passive = true
    ftp.connect(chain.ftp_url,chain.ftp_port)
    ftp.login(chain.ftp_username,chain.ftp_password)
    file_executed_with_targetname = []
    files_executed.each do |file_exe|
      rih = RestaurantImportHistory.find file_exe[:history]
      rih.update_column(:status, "processing")
      file_exe[:history] = rih
      
      if file_exe[:file].split(".").last.downcase == "xls"
        file_type = "xls"
      elsif file_exe[:file].split(".").last.downcase == "csv"
        file_type = "csv"
      end
      targetname = "import_resto" + rand(1000).to_s + "." + file_type
      file_exe[:targetname] = targetname
      file_executed_with_targetname << file_exe
      ftp.getbinaryfile("#{file_exe[:file]}","tmp/#{targetname}")
    end  
    ftp.close

    file_executed_with_targetname.each do |file_exe|
      if File.exist?("#{Rails.root}/tmp/#{file_exe[:targetname]}") 
        @res_success = []
        @res_fail = []
        bad_format = false

        if file_type == "xls"
          p "Processing by XLS"
          book = Spreadsheet.open "#{Rails.root}/tmp/#{targetname}"
          sheet1 = book.worksheet 0
          last_row_name = sheet1.first.last.strip rescue ""
          if sheet1.first.size == 30 && last_row_name == "Location qrcode identifier"
            sheet1.each 1 do |row|
              processing_file(chain, row, file_type) unless row[0].blank?
            end
          else
            bad_format = true
          end
        elsif file_type == "csv"
          p "Processing by CSV"
          sheet1 = CSV.read("#{Rails.root}/tmp/#{targetname}", {:headers => true, :encoding => 'iso-8859-1:utf-8'})
          last_row_name = sheet1.first.last.strip rescue ""
          if sheet1.first.size == 30 && last_row_name == "Location qrcode identifier"
            sheet1.each do |row|
              processing_file(chain, row, file_type) unless row[0].blank?
            end
          else
            bad_format = true
          end
        end
      else
        p "file is missing . . . "
      end

      if bad_format
        file_exe[:history].update_column(:status, "Failed - Bad xls/csv format")
      else
        file_exe[:history].update_attributes(:status => "completed", :success_restaurants => @res_success, :failed_restaurants => @res_fail )

        p "=" * 100
        p @res_fail
        p "=" * 100
      end
    end
  end

  def processing_file(chain, row, file_type)
    @res = chain.restaurants.find_or_initialize_by_name(row[0].to_s.strip.chomp(".0")) rescue nil
    if @res
      begin
        p row[0].to_s
        p "=" * 100

        # App Display Text
        @res.app_display_text = row[1].to_s.strip.chomp(".0") if row[1].present?

        # DashboardDisplayName
        @res.dashboard_display_text = row[2].to_s.strip.chomp(".0") if row[2].present?

        # Address
        @res.address = row[3].to_s.strip.chomp(".0") if row[3].present?

        # City
        @res.city_import = row[4].to_s.strip.downcase.chomp(".0") if row[4].present?

        # State
        @res.state_import = row[5].to_s.strip.downcase.chomp(".0") if row[5].present?

        # Country
        @res.country_import = row[6].to_s.strip.downcase.chomp(".0") if row[6].present?

        # ZipCode
        @res.zipcode = (row[7].to_s.chomp(".0").split(".").first rescue nil) if row[7].present?

        # PhoneNumber
        @res.phone_number = row[8].to_s.chomp(".0") if row[8].present?

        # Latitude
        @res.latitude = (row[9].to_s.chomp(".0") rescue nil) if row[9].present?
        puts "Latitude FROM FILE : #{row[9]}"


        # Longitude
        @res.longitude = (row[10].to_s.chomp(".0") rescue nil) if row[10].present?
        puts "Longitude FROM FILE : #{row[10]}"

        # Tags
        @res.tag_list = (row[11].to_s.gsub(";",",").split(',').map{|x| x.strip} rescue nil) if row[11].present?

        # SiteLive
        @res.status = (row[12].to_i == 1 ? true : false) if row[12].present?

        # Beacon serial number
        @res.beacon_serial_number = row[27].to_s.chomp(".0") if row[27].present?

        # Beacon uuid
        @res.beacon_uuid = row[28].to_s.chomp(".0") if row[28].present?

        # Location qrcode identifier
        @res.location_qrcode_identifier = row[29].to_s.chomp(".0") if row[29].present?

        # ExternalPartnerID
        @res.external_partner_id = row[30] if row[30].present?

        # City assignment to city_id
        @res.city_id = check_city_state_country(@res)

        # @res.save!

        save_and_backup_restaurant_related_data(row)

        # Open and close hour (Sunday)
        @res_hours_day_one = @res.restaurant_hours.find_or_initialize_by_day_of_week(0)
        @res_hours_day_one.open_at = DateTime.parse(row[13].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_one.close_at = DateTime.parse(row[20].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_one.save!

        # Open and close hour (Monday)
        @res_hours_day_two = @res.restaurant_hours.find_or_initialize_by_day_of_week(1)
        @res_hours_day_two.open_at = DateTime.parse(row[14].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_two.close_at = DateTime.parse(row[21].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_two.save!

        # Open and close hour (Tuesday)
        @res_hours_day_three = @res.restaurant_hours.find_or_initialize_by_day_of_week(2)
        @res_hours_day_three.open_at = DateTime.parse(row[15].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_three.close_at = DateTime.parse(row[22].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_three.save!

        # Open and close hour (Wednesday)
        @res_hours_day_four = @res.restaurant_hours.find_or_initialize_by_day_of_week(3)
        @res_hours_day_four.open_at = DateTime.parse(row[16].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_four.close_at = DateTime.parse(row[23].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_four.save!

        # Open and close hour (Thursday)
        @res_hours_day_five = @res.restaurant_hours.find_or_initialize_by_day_of_week(4)
        @res_hours_day_five.open_at = DateTime.parse(row[17].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_five.close_at = DateTime.parse(row[24].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_five.save!

        # Open and close hour (Friday)
        @res_hours_day_six = @res.restaurant_hours.find_or_initialize_by_day_of_week(5)
        @res_hours_day_six.open_at = DateTime.parse(row[18].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_six.close_at = DateTime.parse(row[25].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_six.save!

        # Open and close hour (Saturday)
        @res_hours_day_seven = @res.restaurant_hours.find_or_initialize_by_day_of_week(6)
        @res_hours_day_seven.open_at = DateTime.parse(row[19].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_seven.close_at = DateTime.parse(row[26].to_s).strftime('%I:%M %p') rescue nil
        @res_hours_day_seven.save!

        @res_success << row[0].to_s.strip

      rescue => e
        p e
        fail_row = []
        fail_row << row[0].to_s.strip
        fail_row << e.to_s
        @res_fail << fail_row
      end
    end
  end

  def check_city_state_country(res)
    puts "okeeeeey"
    country_exist = Country.where(:ISO => "#{res.country_import.upcase}").first rescue nil
    if country_exist.blank?
      country_exist = Country.where("lower(name) like '#{res.country_import.gsub("'", "''").downcase}'").first rescue nil
    end 
    if country_exist.blank?
      country_exist = Country.create(
        :name => res.country_import.titleize,
        :abbreviation => (CS.countries.key(res.country_import.titleize) rescue nil)
      )
    end

    state_exist = nil
    if res.state_import.size <= 2
      state_exist = country_exist.regions.where("lower(abbreviation) = '#{res.state_import.downcase}'").first rescue nil

      if state_exist.blank?
        state_exist = Region.create(
            :name => ( CS.states(country_exist.abbreviation)[res.state_import.upcase.to_sym] rescue res.state_import.titleize ),
            :abbreviation => res.state_import.upcase,
            :country_id => country_exist.id
        )
      end
    else
      state_exist = country_exist.regions.where("lower(name) like '#{res.state_import.gsub("'", "''").downcase}' ").first rescue nil

      if state_exist.blank?
        state_exist = Region.create(
            :name => res.state_import.titleize,
            :abbreviation => (CS.states(country_exist.abbreviation).key(res.state_import.titleize) rescue nil),
            :country_id => country_exist.id
        )
      end
    end

    city_exist = state_exist.cities.where("lower(name) like '#{res.city_import.gsub("'", "''").downcase}'").first rescue nil
    if city_exist.blank?
      city_exist = City.create(
          :name => res.city_import.titleize,
          :region_id => state_exist.id
      )
    end

    puts "CITY HEREE"
    p city_exist

    return city_exist.id
  end

  def save_and_backup_restaurant_related_data(row)

    # Save restaurant
    if @res.save!

      # Initalize restaurant detail
      res_detail = RestaurantDetail.find_or_initialize_by_restaurant_id(@res.id)

      # Set value for restaurant detail here
      ######################################

      # Save restaurant detail
      if res_detail.save

        if @res.status == false

          # Preparing backup data for
          # restaurant, restaurant detail, and offers related data
          data = @res.prepare_data_in_backup_format

          # Set backups column with current data
          @res.update_column('backups', data.to_json)

          # Clear data and change status to false
          @res.clear_restaurant_data_and_offers
        elsif @res.status == true
          @res.perform_change_status(true)
        end
        @res.clear_non_mandatory if @res.status
      end

    end

  end

end