class PushPromotionNotificationDeviceJob < Struct.new(:protmotion_id,:emails,:body_content, :chain_id)
  def perform
    puts "perform Push Plain Notification device job #{protmotion_id}"
    puts "#{self.emails.size} will push the notification"
    locales = Chain.find(chain_id).locales
    plain = PromotionNotification.find(protmotion_id)
    locales.each do |lc|
      ## android Push notif
      users = User.where("users.id IN (?) and users.active is true and users.push_device is true and locales.key = ? AND chain_id in (?) AND users.sign_in_device_type = 'android'",
                         emails.map(&:to_i), lc.key, chain_id).joins(:locale)

      android_tokens = users.map(&:device_token).compact
      plain.push_plain_to_android(android_tokens.compact, body_content["device_text_#{lc.key}"]) unless android_tokens.blank?
      ## split it max 1000 request to gcm
      #android_tokens.in_groups_of(999).each do |android_split|
      #plain.push_plain_to_android(android_split.compact, body_content["device_text_#{lc.key}"]) unless android_tokens.blank?
      #Delayed::Job.enqueue(PushPlainNotificationDevicesJob.new(
      #                         plain, 'android', android_split.compact,
      #                         body_content["device_text_#{lc.key}"],
      #                         chain_id
      #                     ))
      #end
      ## iphone push notif
      users = User.where("users.id IN (?) and users.active is true and users.push_device is true and locales.key = ? AND chain_id in (?) AND lower(users.sign_in_device_type) = 'iphone'", emails.map(&:to_i), lc.key, chain_id).
          joins(:locale)
      iphone_tokens = users.map(&:device_token).compact
      plain.push_plain_to_iphone(iphone_tokens.compact, body_content["device_text_#{lc.key}"]) unless iphone_tokens.blank?
      ## split it max 1000 request to gcm
      #iphone_tokens.in_groups_of(999).each do |iphone_split|
      #plain.push_plain_to_iphone(iphone_split.compact, body_content["device_text_#{lc.key}"]) unless iphone_tokens.blank?
      #Delayed::Job.enqueue(PushPlainNotificationDevicesJob.new(
      #                         plain, 'iphone',
      #                         iphone_split.compact,
      #                         body_content["device_text_#{lc.key}"],
      #                         chain_id
      #                    ))
      #end
    end

    puts "push notification success"
  end
end
