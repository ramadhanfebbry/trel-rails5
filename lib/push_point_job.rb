class PushPointJob < Struct.new(:push_point, :user_ids, :push_point_id)

  include PushNotificationData

  def perform
    puts "perform"
    p push_point
    push_point = PushPoint.find push_point_id
    #users = User.where("id in (?) and chain_id = ?", push_point.user_ids)
    success_logs = []
    success_users = []
    admin_email = " ("+push_point.admin.email+")" rescue "-"
    error_logs = []
    user_ids.each do |user_id|
      begin
        user = User.where("id = ? and active is true", user_id).first
        if user.chain.eql?(push_point.chain)
          user.earn(push_point.points, nil, "push_point")
          PointHistory.add_to_history(user.id, "Admin Push Point #{admin_email}", push_point.points)
          success_logs << {:user_id => user_id, :notification => "", :email => "", :point => "Success", :log => "Pushed Point #{push_point.points}", :created_at => Time.zone.now}
          success_users << user_id
        else
          error_logs << {:user_id => user_id, :email => (user.email rescue ""), :log => "Failed, #{user_id} not included in #{push_point.chain.name}"}
        end
      rescue => e
        error_logs << {:user_id => user_id, :email => (user.email rescue ""), :log => "It can be 2 reason, #{user_id} doesn't exist or user with id #{user_id} is inactive"}
      end
    end unless push_point.user_ids.blank?

    emails_and_device_tokens_group = PointNotification.group_emails_and_device_token_by_locale(success_users)
    notification_by_locale = PointNotification.group_message_pn_by_locale(push_point.chain)

    unless success_logs.blank?
      success_logs = PointNotification.set_valid_pushed_for_each_user(success_logs)
      success_logs = PointNotification.set_status_notif_when_reward_notif_not_set(success_logs, notification_by_locale)
    end

    if push_point.chain.admin_push_point_notif_to_device and !success_logs.blank?
      PointNotification.gcm_push(push_point.chain, emails_and_device_tokens_group, notification_by_locale, push_point.points, nil)
      PointNotification.push_to_iphone2(push_point.chain, emails_and_device_tokens_group, notification_by_locale, push_point.points, nil)
    else
      success_logs.each do |succ_log|
        succ_log[:notification] = "N/A"
      end
    end

    if push_point.chain.admin_push_point_notif_to_email and !success_logs.blank?
      chain = push_point.chain
      notif_setting = chain.notification_settings.where(email_template: "admin_push_points_notification").first rescue nil

      if notif_setting.blank? || ((notif_setting && notif_setting.send_type.eql?(1)) || (notif_setting && notif_setting.send_type.eql?(3)))
        if chain.fishbowl_setting
          chain.send_points_notif_for_multi_user(push_point.chain, emails_and_device_tokens_group, notification_by_locale, push_point.points, nil)
        else
          PointNotification.push_email_group(push_point.chain, emails_and_device_tokens_group, notification_by_locale, push_point.points, nil)
        end
      elsif notif_setting.send_type.eql?(2)
        PointNotification.push_email_group_using_mandrill(push_point.chain, emails_and_device_tokens_group, notification_by_locale, push_point.points, nil, notif_setting)
      end

    else
      success_logs.each do |succ_log|
        succ_log[:email] = "N/A"
      end
    end

    success_log_entry = PushLogEntry.new(:pushable_id => push_point.id, :pushable_type => push_point.class.to_s, :log_type => 1)
    success_log_entry.logs = success_logs
    success_log_entry.save

    error_log_entry = PushLogEntry.new(:pushable_id => push_point.id, :pushable_type => push_point.class.to_s, :log_type => 2)
    error_log_entry.logs = error_logs
    error_log_entry.save

    dj_count = DelayedJob.where(:delayable_id => push_point.id, :delayable_type => push_point.class.to_s).count

    # send email if job has done
    if dj_count < 2
      unless ENV["DEFAULT_HOST_EMAIL"].to_s.blank?
        RewardMailer.admin_push_notification("Push POINT admin ID:#{push_point.id}",
                                             "Push POINT admin ID:#{push_point.id}",
                                             push_point, "END @").deliver! #unless last_execution.blank?
      end

    end
  end
end