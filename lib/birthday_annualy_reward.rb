class BirthdayAnnualyReward
  attr_accessor :chain_id, :push_type

  def initialize(chain_id,push_type = nil)
    @chain_id = chain_id
    @push_type = push_type.blank? ? 1 : push_type.to_i
  end

  def get_users
    now = Time.zone.now
    day = now.day
    month = now.month
    chain = Chain.find(@chain_id)
    users = chain.users.where("dob_month = ? and dob_day = ?", month.to_s,day.to_s)

    return users
  end

  def get_rewards
    ch = Chain.find(@chain_id)
    return ch.birthday_rewards.where('status = ?', 'active').first
  end

  def switch_for_testing
    case @push_type
      when 1 # yearly ( default )
         return 1.years
      when 2 # hourly ( every 1 hour )
         return 1.hours
      when 3 # 10 minutes ( every 10 minutes)
         return 10.minutes
    end
  end

  def execute_annualy_reward
    users = get_users
    reward = get_rewards

    # if reward on chain are exists ( the chain has birthday reward active )
    unless reward.blank?
      users.each do |u|
        if u.register_device_type.try(:downcase).eql?("browser") || u.register_device_type.try(:downcase).eql?("browser_compatible")
          push_reward_to_user(reward, u)
        elsif u.signup_device_status.eql?(User::SIGNUP_STATUS["GREEN"])
          push_reward_to_user(reward, u)
        end
      end
    end
  end

  def push_reward_to_user(reward, user)
    time_diff = switch_for_testing
    exist = RewardWallet.where(:user_id => user.id, :reward_id => reward.id).last

    # if its not pushed yet
    if exist.blank?
      RewardWallet.create_with_delay({:reward_id => reward.id,
                                      :user_id => user.id,
                                      :description => "Birthday reward on #{Time.now.strftime('%D')}"
                                     })
    else
      # check if this user has get annualy, and push it once every one year
      created_at =  exist.created_at
      if (created_at + time_diff) <= Time.zone.now
        RewardWallet.create_with_delay({:reward_id => reward.id,
                                        :user_id => user.id,
                                        :description => "Birthday reward on #{Time.now.strftime('%D')}"
                                       })
      end
    end
  end

  def push_imadiately_to_user_since_signup_and_dob_same(user)
    reward = get_rewards
    unless reward.blank?
      user_created_date = user.created_at.blank? ? Time.zone.now : user.created_at.in_time_zone
      push_reward_to_user(reward, user) if user.dob_day == user_created_date.day && user.dob_month == user_created_date.month
    end
  end
end