class OloParser::ParserOlo
  attr_accessor :data_request

  def initialize(data_request = nil)
    @data_request = data_request
  end

  def get_date
    date =  @data_request["timePlaced"]
    return date.to_date
  end

  def get_time
    date =  @data_request["timePlaced"]
    return date.to_date
  end

  def get_restaurant
    restaurant =  @data_request["storeNumber"]
    return restaurant
  end

  def get_email
    @data_request["customer"]["email"]
  end

  def get_subtotal
    @data_request["totals"]["subTotal"]
  end

  def get_tax
    @data_request["totals"]["salesTax"]
  end

  def get_payment
    @data_request["totals"]["paymets"].first["type"] rescue nil
  end

  def get_first_name
    @data_request["customer"]["firstName"]
  end

  def get_last_name
    @data_request["customer"]["lastName"]
  end

  #new-parse============
  def tax
    @data_request["totals"]["salesTax"]
  end

  def email
    @data_request["customer"]["email"]
  end

  def chain_id
    @data_request["chain_id"]
  end

  def restaurant_id
    @data_request["storeNumber"]
  end

  def order_id
    @data_request["orderId"]
  end

  def time_placed
    date =  @data_request["timePlaced"]
    return date.to_date
  end

  def time_wanted
    date =  @data_request["timeWanted"]
    return (date.to_date rescue nil)
  end

  def issue_time
    date =  @data_request["timeWanted"]
    date = @data_request["timePlaced"] if date.blank?
    return (date.to_time rescue nil)
  end

  def issue_date
    dt = self.time_wanted.blank? ? self.time_placed : self.time_wanted
    if dt.blank?
      dt = Date.today
    end
    return dt
  end

  def receipt_date
    dt = self.time_wanted.blank? ? self.time_placed : self.time_wanted
    if dt.blank?
      dt = Date.today
    end
    return dt
  end

  def subtotal
    temp_discount = @data_request["totals"]["discount"] rescue 0
    temp_subtotal = @data_request["totals"]["subTotal"] rescue 0
    temp_subtotal = temp_subtotal.to_f - temp_discount.to_f
    temp_subtotal = 0 if temp_subtotal < 0
    temp_subtotal
  end

  def store_name
    @data_request["location"]["name"] rescue ""
  end

  def menu_items
    @data_request["items"]
  end
  # new-parse=============
end
