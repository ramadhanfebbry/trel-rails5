class MailParseLpq
  #sattribute_accessor :email_body
  # m = MailParseLpq.new(Comment.last.body)
  def initialize(email_body)
    @email_body = email_body
  end

  def parse_email
    #comment = Comment.find(comment_id)
    nok_obj = Nokogiri.parse @email_body
    result 	= OpenStruct.new

    result.chain 		=    parse_chain
    result.location 	= nok_obj.xpath('//span[@id="UnitNameLabel"]').text.squish rescue nil + nok_obj.xpath('//table//tbody//tr//td//table//tbody//tr//td//p/span')[2].text.gsub("\n","") rescue nil
    result.restaurant = nok_obj.xpath('//span[@id="UnitNameLabel"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='Order/Confirmation #']")
    result.order_no 	= nok_obj.xpath('//span[@id="lblOrderNumber"]').text.squish rescue nil


    temp = nok_obj.search("[text()*='Check #']")
    result.check_id 	= nok_obj.xpath('//span[@id="lblPosCheckID"]').text.squish rescue nil


    temp = nok_obj.search("[text()*='Pick-Up Time']")
    result.date_pickup 	= nok_obj.xpath('//span[@id="PickupTimeDisplayLabel"]').text.squish rescue nil


    temp = nok_obj.search("[text()*='Name']")
    result.name = nok_obj.xpath('//span[@id="lblCustomerName"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='Email*']")
    result.email 		= nok_obj.xpath('//span[@id="lblEmail"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='Phone Number*']")
    result.phone 		=  nok_obj.xpath('//span[@id="lblPhoneNumber"]').text.squish rescue nil

    result.orders 		= nok_obj.xpath('//table[@id="OrderDetail_OrderDetailsDataList"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='Payment Type']")
    result.payment_type = nok_obj.xpath('//span[@id="RcptChargeSummary_lblPaymentType"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='SubTotal']")
    result.subtotal 	= nok_obj.xpath('//span[@id="RcptChargeSummary_lblSubTotal"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='Tax']")
    result.tax 			= nok_obj.xpath('//span[@id="RcptChargeSummary_lblTax"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='Total']")
    result.total = nok_obj.xpath('//span[@id="RcptChargeSummary_lblTotal"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='Amount Paid']")
    result.amount_paid = nok_obj.xpath('//span[@id="RcptChargeSummary_lblAmountPaid"]').text.squish rescue nil

    temp = nok_obj.search("[text()*='Balance']")
    result.balance 	= nok_obj.xpath('//span[@id="RcptChargeSummary_lblBalance"]').text.squish rescue nil

    result.is_payed = !nok_obj.xpath('//span[@id="RcptChargeSummary_lblPaymentType"]').text.squish.downcase.eql?('pay at restaurant') rescue false

    result.chain_id =  parse_chain_id
    result.restaurant_id = parse_restaurant_id
    result.user_id = parse_user_id

    # result.date_placed 	= parse_date_placed rescue nil
    # result.order_id 	= parse_order_id rescue nil
    # result.user_id 		= parse_user_id
    # result.customer_no 	= parse_customer_no rescue nil
    # result.restaurant 	= parse_restaurant
    # result.restaurant_id 	= parse_restaurant_id rescue nil
    # result.chain_id 	= parse_chain_id rescue nil

    result

    return result
  end

  def parse_chain
    nok_obj = Nokogiri.parse @email_body
    res = nok_obj.xpath('//span[@id="UnitNameLabel"]').text.squish.include?("Le Pain Quotidien") rescue false
    if res == true
      return "Le Pain Quotidien"
    else
      return "LPQ"
    end
  end

  def parse_restaurant
    nok_obj = Nokogiri.parse @email_body
    nok_obj.xpath('//span[@id="UnitNameLabel"]').text.squish rescue nil
  end

  def parse_restaurant_id
    r_id = Restaurant.active.where("lower(name) like '%#{parse_restaurant.downcase}%' and chain_id = #{parse_chain_id}").first.id rescue nil
    return r_id
  end

  def parse_chain_id
    x = parse_chain
    ch = Chain.where("lower(name) like '%#{x.downcase}%'").first.id rescue nil
    if ch.blank?
      ch = Chain.where("lower(name) like '%lpq%'").first.id rescue nil
    end
    return ch
  end

  def parse_user_id
    nok_obj = Nokogiri.parse @email_body
    temp = nok_obj.search("[text()*='Email*']")
    email = nok_obj.xpath('//span[@id="lblEmail"]').text.squish rescue nil
    ch = parse_chain_id
    User.where("chain_id = ? AND lower(email) = ?", ch, email.downcase  ).first.id rescue nil
  end
end