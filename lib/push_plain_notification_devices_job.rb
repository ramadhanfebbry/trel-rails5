class PushPlainNotificationDevicesJob < Struct.new(:plain, :dev_type, :reg_ids, :body_content, :chain_ids)

  def perform
    puts "performing notification device jobs"
    if dev_type == "android"
      plain.push_plain_to_android(reg_ids.compact, body_content) unless reg_ids.blank?
    else
      plain.push_plain_to_iphone(reg_ids.compact, body_content) unless reg_ids.blank?
    end
  end
end