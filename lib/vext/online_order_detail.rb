module Vext
  class OnlineOrderDetail < ActiveRecord::Base

# attr_accessible :title, :body
    self.table_name = "vext_online_order_details"

    belongs_to :online_order, :class_name => "Vext::OnlineOrder"

  end

end
