module Onosys
  class OnlineOrderProcessJob < Struct.new(:receipt_id, :ooo_id, :restaurant_id, :offer_id)

    def perform
      p " **************** PERFORM VEXTOnlineOrderProcessJob ******************"

      receipt = Receipt.find(receipt_id)
      ooo = Vext::OnlineOrder.find(ooo_id)
      restaurant = Restaurant.find(restaurant_id)
      offer = Offer.find(offer_id)

      ooo_detail = Vext::OnlineOrderDetail.where(:vext_online_order_id => ooo.id).first

      sub_total = ooo_detail.sub_total
      tax = ooo_detail.tax
      chain = receipt.chain
      error_messages = {}

      receipt_transaction = ReceiptTransaction.new
      receipt_transaction.receipt_id = receipt.id
      receipt_transaction.subtotal = sub_total
      receipt_transaction.tax = tax
      receipt_transaction.restaurant_id = restaurant.id

      restaurant_offer = RestaurantOffer.where(:restaurant_id => restaurant.id, :offer_id => offer.id).first rescue nil
      receipt_transaction.restaurant_offer_id = (restaurant_offer.id rescue nil)

      is_unique, comp_receipt_transaction = unique_receipt?(chain, ooo)
      if is_unique
        if chain and chain.today_max_user_limit?(receipt, receipt.created_at)
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
        elsif !offer.blank? && offer.min_subtotal_criteria_for_receipt_approval > 0 && offer.min_subtotal_criteria_for_receipt_approval.to_f > receipt_transaction.subtotal.to_f
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          error_messages[:min_subtotal] = "MIN SUBTOTAL LIMIT NOT CROSSED"
        elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, sub_total, receipt.created_at, offer)
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
        else
          receipt_transaction.status = Receipt::STATUS[:APPROVED]
          receipt.status = Receipt::STATUS[:APPROVED]
        end
      else
        comp_receipt = comp_receipt_transaction.try(:receipt)
        if comp_receipt.try(:user_id).eql?(receipt.user_id) && !comp_receipt_transaction.subtotal.to_f.eql?(sub_total.to_f)
          if !offer.blank? && offer.min_subtotal_criteria_for_receipt_approval > 0 && offer.min_subtotal_criteria_for_receipt_approval.to_f > receipt_transaction.subtotal.to_f
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            error_messages[:min_subtotal] = "MIN SUBTOTAL LIMIT NOT CROSSED"
          elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, receipt.created_at, offer)
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
          else
            receipt_transaction.status = Receipt::STATUS[:APPROVED]
            receipt.status = Receipt::STATUS[:APPROVED]

            #update old receipt to be rejected
            comp_receipt.status = Receipt::STATUS[:REJECTED]
            old_comp_receipt_transaction = comp_receipt.last_transaction

            new_comp_receipt_transaction = ReceiptTransaction.new(old_comp_receipt_transaction.attributes)
            new_comp_receipt_transaction.status = Receipt::STATUS[:REJECTED]
            new_comp_receipt_transaction.receipt = comp_receipt
            new_comp_receipt_transaction.restaurant_offer = old_comp_receipt_transaction.restaurant_offer
            new_comp_receipt_transaction.reject_reason_id = 0 if comp_receipt.status!=4

            new_comp_receipt_transaction.instructions = {:rejected => "Reject Receipt"} if comp_receipt.status.to_i.eql?(4)

            new_comp_receipt_transaction.save
            comp_receipt.save
          end
        else
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          error_messages[:receipt] = "Receipt should be uniq, this receipt have submitted before"
          #ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, ncr_receipt_obj.error_messages, receipt).deliver
        end
      end

      receipt_transaction.instructions = error_messages
      receipt_transaction.receipt_number = "vext_#{ooo.online_order_identifier}"
      receipt_transaction.issue_date = receipt.created_at
      receipt_transaction.time_stamp = receipt.created_at.strftime("%I:%M%p")
      receipt_transaction.receipt_date = receipt.created_at
      receipt_transaction.receipt_time = receipt.created_at.strftime("%I:%M%p")
      receipt_transaction.restaurant_id = restaurant ? restaurant.id : nil
      p "this is receipt transaction object"
      p receipt_transaction
      ## if the rules not met

      begin
        if receipt_transaction.offer and receipt_transaction.offer.offer_rules.size > 0 # doe not met the rules
          puts "OFFER RULE:: NCR ONLINE ORDER PROCESS JOB start ---" * 19

          off_rule = OfferRuleCalculation::CalculateOfferRule.new(receipt_transaction.offer, receipt_transaction)
          off_rule.result
          tmp_total_points_earned = off_rule.total_points_get
          if tmp_total_points_earned.to_f == 0.0 || tmp_total_points_earned == -1 || tmp_total_points_earned == -2
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt_transaction.instructions = {:offer_rules_not_met => "This receipt does not met any rules"}
            receipt_transaction.instructions = {:ignore_item_list => "This receipt get all the ignore items listed"} if tmp_total_points_earned == -1
            receipt_transaction.instructions = {:ignore_item_reject => "Rejected due the item"} if tmp_total_points_earned == -2
            receipt.status = Receipt::STATUS[:REJECTED]
          end
          puts "OFFER RULE:: NCR ONLINE ORDER PROCESS JOB end ---" * 19
        end
      rescue => e
        puts "CALCULATE::OFFERRULES ----> #{e.inspect}"
      end
      ##
      receipt.receipt_transactions << receipt_transaction
      receipt.save
      ReceiptProcess::ReceiptSetIssuedate.set_date(receipt.id,{:issue_date => ooo.created_at, :issue_time => ooo.created_at, :timezone => ooo.created_at})
      if receipt.status == Receipt::STATUS[:APPROVED]
        ooo.update_column(:loyalty_status, 2)
      end
      puts "------ end ------"
      p " ******************** END Vext OnlineOrderProcessJob ********************"
    end

    def unique_receipt?(chain, ooo)
      receipt_transaction = ReceiptTransaction.includes(:receipt).where("receipts.chain_id = ? AND receipt_transactions.receipt_number = ? AND receipt_transactions.status = ?", chain.id, "onosys_#{ooo.online_order_identifier}", Receipt::STATUS[:APPROVED]).first
      return [true, nil] if receipt_transaction.blank?
      return [false, receipt_transaction]
    end

  end

end

