module Vext
  class ReceiptOnlineOrder< ActiveRecord::Base

    self.table_name = "vext_receipt_online_orders"
    attr_accessible :vext_online_order_id, :receipt_id

    belongs_to :receipt
    belongs_to :online_order, :class_name => "Vext::OnlineOrder"

  end

end

