class PointHistoryGroupDescriptionJob < Struct.new(:status)

  def perform
    count = PointHistory.where(:group_no => 0).count
    loop = (count.to_f / 1000.to_f).ceil
    1.upto(loop) do |page|
      PointHistory.where(:group_no => 0).paginate(page: page, per_page: 1000).each do |ph|
        keys = PointHistory::GROUP.keys
        key = keys.select{|a| ph.description.downcase.include?(a.downcase)}.first
        unless key.blank?
          group_no = PointHistory::GROUP[key]
          ph.update_column(:group_no, group_no)
        end
      end
    end
  end

end


