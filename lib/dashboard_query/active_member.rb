class DashboardQuery::ActiveMember

  def self.active_members_generator(res_obj, filter, show_interval, chain_obj)
    time_now = Time.zone.now
    time_zone = time_now.strftime('%Z')
    tmp_result = []
    case_issue_date = "(
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end)"
    select_stamp_column = nil;
    select_column = "date(TIMEZONE('UTC',
    #{case_issue_date})
    AT TIME ZONE '#{time_zone}'))
    date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}')"
    column_for_day_nested = nil
    if show_interval == "day"
      select_column = "TIMEZONE('UTC',
      #{case_issue_date})
      AT TIME ZONE '#{time_zone}')
      TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}' as date"
      select_stamp_column = ",t.time_stamp as stamp "
      group_stamp_column = ", stamp "
      stamp_column = ",receipt_transactions.time_stamp"
      nested_column = ", s.stamp "
      1.upto(24) do |hour|
        tmp_result << [Time.zone.now.utc.change(:hour => hour).to_i * 1000, nil]
      end
    end
    interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i
    val = if filter.blank?
            ## dashboard setting
            week = "'#{(time_now - 6.days).to_date}' and '#{(time_now + 1.days).to_date}'"
            if show_interval == "month"
              week = "'#{(time_now - 30.days).to_date}' and '#{(time_now + 1.days).to_date}'"
            end
            Receipt.find_by_sql("
          SELECT current_date + sd.a AS dates,s.date,s.count   FROM generate_series(#{interval},0,1) AS sd(a) left join  (
          select count(t.user_id),t.date #{select_stamp_column} from(
          SELECT distinct on(receipts.user_id, date(TIMEZONE('UTC',
          #{case_issue_date})
          AT TIME ZONE '#{time_zone}'))
          date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}'),
          receipts.user_id FROM receipts
          INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
          WHERE receipts.deleted_at IS NULL AND
          (date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}')
          between #{week} and
          receipt_transactions.restaurant_id = #{res_obj.id})) t
          group by t.date #{select_stamp_column} order by date asc) s on s.date = current_date + sd.a where
          current_date + sd.a between #{week}")
          elsif show_interval == "day"
            Receipt.find_by_sql("SELECT current_date + sd.a AS dates,s.date,s.count , s.stamp FROM generate_series(-203,0,1) AS sd(a) left join (
 select count(t.user_id),t.created_at as date ,t.time_stamp as stamp from(
 SELECT distinct on(receipts.user_id, date(TIMEZONE('UTC',
 (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end))
 AT TIME ZONE '#{time_zone}'))
 TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end) AT TIME ZONE '#{time_zone}') as date,
 receipts.user_id ,receipts.created_at,receipt_transactions.time_stamp FROM receipts
 INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
  where
 receipt_transactions.restaurant_id = #{res_obj.id}) t
 group by t.created_at , stamp order by date asc) s on date(s.date) = date(current_date + sd.a)
where date(current_date + sd.a) = '#{Date.today}'")
          else
            tmp_filter = filter
            tmp_filter = "current_date + sd.a is not null" if filter == "all"
            if show_interval == "week"
              date_filter = "between '#{(time_now - 6.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -7;
            elsif show_interval == "month"
              date_filter = "between '#{(time_now - 30.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -30;
            elsif show_interval == "3month"
              date_filter = "between '#{(time_now - 90.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -90;
            elsif show_interval == "all"
              date_filter = "between '#{(time_now + interval.days).to_date}' and '#{(time_now + 1.days).to_date}'"
            end

            Receipt.find_by_sql (" SELECT current_date + sd.a AS dates,s.date,s.count #{nested_column} FROM generate_series(#{interval},0,1) AS sd(a) left join  (
          select count(t.user_id),t.date #{select_stamp_column} from(
          SELECT distinct on(receipts.user_id, date(TIMEZONE('UTC',
          #{case_issue_date})
          AT TIME ZONE '#{time_zone}'))
          date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}'),
          receipts.user_id #{stamp_column} FROM receipts
          INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
          WHERE receipts.deleted_at IS NULL AND
          (date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}')
             #{date_filter}  and
          receipt_transactions.restaurant_id = #{res_obj.id})) t
          group by t.date #{group_stamp_column} order by date asc) s on s.date = current_date + sd.a where
          #{tmp_filter}")
          end

    tmp_value = []


    unless val.blank?
      count = 0; count_before = 0
      val.each do |v|
        if show_interval == "day"
          tmp_date = v.date.nil? ? v.dates : v.date
          count += v.count.to_i
          count = nil if count == 0
          dt = tmp_date.to_datetime.in_time_zone((Time.zone_offset(time_zone)/3600))
          hour = dt.hour
          min = dt.min
          zn = dt.strftime("%p")

          if  !v.stamp.blank?
            time_stamp = v.stamp
            split_time = time_stamp.split(':')
            hour = split_time[0]
            # zn = split_time[1][2..3]
            zn = if time_stamp.downcase.include?('am')
                   "am"
                 else
                   "pm"
                 end
            min = split_time[1][0..1]
            hour = hour.to_i + 12 if zn.downcase == "pm" and hour.to_i < 12
            hour = 0 if hour.to_i == 12 and zn.downcase == "am"
          end
          #dt = dt.utc.change(:hour => hour.to_i, :min => min.to_i)
          dt = Time.parse(dt.strftime('%Y-%m-%d %I:%M:%S UTC')).to_datetime.change(:hour => hour.to_i, :min => min.to_i)
          dt = dt.to_datetime.change(:hour => hour.to_i, :min => min.to_i)

          tmp_value = [dt.to_i * 1000, count]


          tmp_result << tmp_value
          #end
        else
          count = v.count.to_i
          count = nil if count == 0
          tmp_result << [v.dates.to_datetime.to_i * 1000, count]
        end
      end
    end

    return tmp_result.sort! { |a, b| a[0] <=> b[0] }

  end

  #def self.new_active_members_generator(restaurants, filter, show_interval, chain_obj)
  #  time_now = Time.zone.now
  #  time_zone = time_now.strftime('%Z')
  #  tmp_result = []
  #
  #  case_issue_date = "(CASE WHEN receipt_transactions.issue_date is NULL
  #                      THEN receipts.created_at
  #                      ELSE receipt_transactions.issue_date
  #                      END)"
  #
  #  select_column = "DATE(TIMEZONE('UTC',#{case_issue_date})
  #                   AT TIME ZONE '#{time_zone}'))
  #                   DATE(TIMEZONE('UTC', #{case_issue_date})
  #                   AT TIME ZONE '#{time_zone}')"
  #
  #  interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i
  #
  #  ## dashboard setting default 1 week / 1 month
  #  puts "filter == #{filter}"
  #  puts "show interfal == #{show_interval}"
  #  if filter.blank?
  #    ## dashboard setting
  #    if show_interval != "month"
  #      show_interval = "week"
  #      filter = "current_date + sd.a BETWEEN '#{(time_now - 6.days).to_date}'
  #                           AND '#{(time_now + 1.days).to_date}'"
  #    else
  #      filter = "current_date + sd.a BETWEEN '#{(time_now - 30.days).to_date}'
  #                           AND '#{(time_now + 1.days).to_date}'"
  #    end
  # end
  #
  #  tmp_filter = filter
  #  #puts tmp_filter
  #  tmp_filter = "current_date + sd.a is NOT NULL " if filter == "all"
  #  if show_interval == "week"
  #    date_filter = "BETWEEN '#{(time_now - 6.days).to_date}'
  #                           AND '#{(time_now + 1.days).to_date}'";
  #    interval = -7;
  #  elsif show_interval == "month"
  #    date_filter = "BETWEEN '#{(time_now - 30.days).to_date}'
  #                           AND '#{(time_now + 1.days).to_date}'";
  #    interval = -30;
  #  elsif show_interval == "3month"
  #    date_filter = "BETWEEN '#{(time_now - 90.days).to_date}'
  #                           AND '#{(time_now + 1.days).to_date}'";
  #    interval = -90;
  #  elsif show_interval == "all"
  #    date_filter = "BETWEEN '#{(time_now + interval.days).to_date}'
  #                           AND '#{(time_now + 1.days).to_date}'"
  #  elsif show_interval == "day"
  #    date_filter = "BETWEEN '#{(time_now - 1.days).to_date}'
  #                           AND '#{(time_now + 1.days).to_date}'"
  #  end
  #  sql_query = " SELECT current_date + sd.a AS dates,s.date,s.count," +
  #      " s.restaurant_id as id FROM generate_series(#{interval},0,1) AS sd(a) LEFT JOIN  (" +
  #      " SELECT count(t.user_id),t.date ,t.restaurant_id FROM ( " +
  #      " SELECT distinct on(receipts.user_id,restaurant_id , " +
  #      " DATE(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}'))" +
  #      " DATE(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}')," +
  #      " receipts.user_id ,restaurant_id FROM receipts" +
  #      " INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id" +
  #      " WHERE receipts.deleted_at IS NULL AND" +
  #      " (DATE(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}')" +
  #      " #{date_filter}  AND" +
  #      " receipt_transactions.restaurant_id in (#{restaurants.map(&:id).join(',')}))) t " +
  #      " GROUP BY t.date ,t.restaurant_id ORDER BY date asc) s" +
  #      " ON s.date = current_date + sd.a" +
  #      " WHERE #{tmp_filter} "
  #
  #  puts "#{sql_query}"
  #
  #  val = Receipt.find_by_sql (sql_query)
  #
  #  unless val.blank?
  #    restaurants.each do |res|
  #      data = val.group_by(&:id)[res.id].map { |x| [x.dates.to_datetime.to_i * 1000,
  #                                                   x.count.to_i] }.
  #                                                   sort! { |a, b| a[0] <=> b[0]
  #      } rescue []
  #
  #      tmp_result << {:name => res.dashboard_display_text,
  #                     :data => data}
  #    end
  #  end
  #  return tmp_result
  #end

  def self.new_active_members_generator(restaurants, filter, show_interval, chain_obj)
      time_now = Time.zone.now
      time_zone = time_now.strftime('%Z')
      tmp_result = []

      #case_issue_date = "(CASE WHEN receipt_transactions.issue_date is NULL
      #                    THEN receipts.created_at
      #                    ELSE receipt_transactions.issue_date
      #                    END)"
      date_coalesce = "date(coalesce(t1.issue_date,c.created_at))"
      restaurant_sql = restaurants.map(&:id).join(',')#restaurants.map(&:id)

      interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i

      ## dashboard setting default 1 week / 1 month
      puts "filter == #{filter}"
      puts "show interfal == #{show_interval}"
      if filter.blank?
        ## dashboard setting
        if show_interval != "month"
          show_interval = "week"
          filter = "current_date + sd.a BETWEEN '#{(time_now - 6.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
        else
          filter = "current_date + sd.a BETWEEN '#{(time_now - 30.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
        end
    end

      tmp_filter = filter
      #puts tmp_filter
      tmp_filter = "current_date + sd.a is NOT NULL " if filter == "all"
      if show_interval == "week"
        date_filter = "BETWEEN '#{(time_now - 6.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
        interval = -7;
      elsif show_interval == "month"
        date_filter = "BETWEEN '#{(time_now - 30.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
        interval = -30;
      elsif show_interval == "3month"
        date_filter = "BETWEEN '#{(time_now - 90.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
        interval = -90;
      elsif show_interval == "all"
        date_filter = "BETWEEN '#{(time_now + interval.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
      elsif show_interval == "day"
        date_filter = "BETWEEN '#{(time_now - 1.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
      end
      sql_query = "
                      SELECT current_date + sd.a AS dates,s.date,s.count,s.restaurant_id as id   FROM generate_series(#{interval},0,1) AS sd(a) left join  (
                      select count(t.user_id),t.date ,t.restaurant_id from(
                      select distinct(c.user_id,
                                      t1.restaurant_id,
                                      #{date_coalesce}),
                                      c.user_id,t1.restaurant_id,
                                      #{date_coalesce}

                      from receipts c left join
                      receipt_transactions t1 on t1.receipt_id = c.id left join
                      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id > t2.id
                      where
                      c.deleted_at is NULL AND #{date_coalesce}  #{date_filter}
                      and t1.restaurant_id in(#{restaurant_sql})
                      ) t group by t.date, t.restaurant_id order by date, restaurant_id asc

                      ) s
                       on s.date = current_date + sd.a where
                      current_date + sd.a is not null  order by
                      current_date + sd.a asc "

      puts "#{sql_query}"

      val = Receipt.find_by_sql (sql_query)

      unless val.blank?
        restaurants.each do |res|
          data = val.group_by(&:id)[res.id].map { |x| [x.dates.to_datetime.to_i * 1000,
                                                       x.count.to_i] }.
                                                       sort! { |a, b| a[0] <=> b[0]
          } rescue []

          tmp_result << {:name => res.dashboard_display_text,
                         :data => data}
        end
      end
      return tmp_result
    end


  def self.active_members_generator_day(restaurants, filter, show_interval, chain_obj)
#    a = Time.now
    time_now = Time.zone.now
    time_zone = time_now.strftime('%Z')
    tmp_result = []

    val = ReceiptTransaction.find_by_sql("select distinct on(users.id) user_id,
receipt_transactions.issue_date as date, receipt_transactions.time_stamp as stamp,1 as count, receipt_transactions.restaurant_id
 from receipt_transactions inner join receipts on
receipts.id = receipt_transactions.receipt_id inner join users on users.id = receipts.user_id
 where restaurant_id in (#{restaurants.map(&:id).join(',')}) and date(issue_date) = date(now())")


    tmp_value = []
    1.upto(24) do |hour|
      tmp_value << [(Time.zone.now).utc.change(:hour => hour).to_i * 1000, nil]
    end
#puts "QUERY::ACTIVEMEMBERS::DAY #{Time.now - a}"
    if !val.blank? and val.size >= 1
      tmp_restaurant_id = 0
      count = 0; count_before = 0

      val.each_with_index do |v, index|
        tmp_date = v.date.nil? ? v.dates : v.date
        count = v.count.to_i

        count = nil if count == 0
        dt = tmp_date.to_datetime.in_time_zone((Time.zone_offset(time_zone)/3600))
        hour = dt.hour
        min = dt.min
        zn = dt.strftime("%p")

        if  !v.stamp.blank?
          time_stamp = v.stamp
          hour = time_stamp[0..1]
          min = time_stamp[3..4]
          zn = time_stamp[5..6]
          hour = hour.to_i + 12 if zn.downcase == "pm" and hour.to_i < 12
          hour = 0 if hour.to_i == 12 and zn.downcase == "am"
        end
        #dt = dt.utc.change(:hour => hour.to_i, :min => min.to_i)
        puts dt
        dt = Time.parse(dt.strftime('%Y-%m-%d %I:%M:%S UTC')).to_datetime.change(:hour => hour.to_i, :min => min.to_i)
        dt = dt.to_datetime.change(:hour => hour.to_i, :min => min.to_i)
        tmp_value << [dt.to_i * 1000, count]


        if (tmp_restaurant_id != v.restaurant_id and tmp_restaurant_id != 0) or (index == (val.size - 1))
          tmp_result << {:name => Restaurant.find(v.restaurant_id).dashboard_display_text, :data => tmp_value.sort! { |a, b| a[0] <=> b[0] }} rescue {}
          tmp_value = []
          1.upto(24) do |hour|
            tmp_value << [(Time.zone.now).utc.change(:hour => hour).to_i * 1000, nil]
          end
        end

        tmp_restaurant_id = v.restaurant_id
      end
    else
      restaurants.each do |res|
        tmp_result << {:name => res.dashboard_display_text, :data => []}
      end
    end

    return tmp_result #.sort#! { |a, b| a[0] <=> b[0] }

  end

  def self.average_active_members_generator_day(restaurants, filter, show_interval, chain_obj)
    a = Time.now
    time_now = Time.zone.now
    time_zone = time_now.strftime('%Z')
    tmp_result = []
    case_issue_date = "(
    case when receipt_transactions.issue_date is null
      then receipts.created_at
    else receipt_transactions.issue_date
    end)"
    select_stamp_column = nil;
    select_column = "date(TIMEZONE('UTC',
    #{case_issue_date})
    AT TIME ZONE '#{time_zone}'))
    date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}')"
    column_for_day_nested = nil

    select_column = "TIMEZONE('UTC',
      #{case_issue_date})
      AT TIME ZONE '#{time_zone}')
      TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}' as date"
    select_stamp_column = ",t.time_stamp as stamp "
    group_stamp_column = ", stamp "
    stamp_column = ",receipt_transactions.time_stamp"
    nested_column = ", s.stamp "


    interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i
    val =

        ReceiptTransaction.find_by_sql("SELECT current_date + sd.a AS dates,date_trunc('hour',s.date) as date,avg(s.count) as count  FROM generate_series(-203,0,1) AS sd(a) left join (
 select count(t.user_id),t.created_at as date ,t.time_stamp as stamp, t.restaurant_id from(
 SELECT distinct on(receipts.user_id, date(TIMEZONE('UTC',
 (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end))
 AT TIME ZONE '#{time_zone}'))
 TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end) AT TIME ZONE '#{time_zone}') as date,
 receipts.user_id ,receipts.created_at,receipt_transactions.time_stamp, receipt_transactions.restaurant_id FROM receipts
 INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
  where
 receipt_transactions.restaurant_id in(#{restaurants.map(&:id).join(',')})) t
 group by t.created_at ,t.restaurant_id, stamp order by t.restaurant_id,date asc) s on date(s.date) = date(current_date + sd.a)
where date(current_date + sd.a) = '#{Date.today}' group by date_trunc('hour',s.date),date(current_date + sd.a) order by date_trunc('hour', s.date) asc")


    tmp_value = []
    1.upto(24) do |hour|
      tmp_value << [(Time.zone.now).utc.change(:hour => hour).to_i * 1000, nil]
    end
    #puts "QUERY::ACTIVEMEMBERS::DAY #{Time.now - a}"
    if !val.blank? and val.size > 1
      tmp_restaurant_id = 0
      count = 0; count_before = 0

      val.each_with_index do |v, index|
        tmp_date = v.date.nil? ? v.dates : v.date
        count += v.count.to_i
        count = nil if count == 0
        dt = tmp_date.to_datetime.in_time_zone((Time.zone_offset(time_zone)/3600))
        hour = dt.hour
        min = dt.min
        zn = dt.strftime("%p")


        #dt = dt.utc.change(:hour => hour.to_i, :min => min.to_i)
        dt = Time.parse(dt.strftime('%Y-%m-%d %I:%M:%S UTC')).to_datetime.change(:hour => hour.to_i, :min => min.to_i)
        dt = dt.to_datetime.change(:hour => hour.to_i, :min => min.to_i)

        tmp_value << [dt.to_i * 1000, count]

      end
    else
      restaurants.each do |res|
        tmp_result << {:name => res.dashboard_display_text, :data => []}
      end
    end
    tmp_result = [{:name => "AVG", :data => tmp_value}]
    return tmp_result #.sort#! { |a, b| a[0] <=> b[0] }

  end


  def self.average_active_members_generator(restaurants, filter, show_interval, chain_obj)
    time_now = Time.zone.now
    time_zone = time_now.strftime('%Z')
    tmp_result = []
    restaurant_sql = restaurants.map(&:id).join(',')#restaurants.map(&:id)
#    case_issue_date = "(
#case when receipt_transactions.issue_date is null
#then receipts.created_at
# else receipt_transactions.issue_date
# end)"
    case_issue_date = "(
  case when exists(select receipt_transactions.issue_date)
  then receipt_transactions.issue_date
    else receipts.created_at
  end
  )"
    select_stamp_column = nil;
    select_column = "date(TIMEZONE('UTC',
    #{case_issue_date})
    AT TIME ZONE '#{time_zone}'))
    date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}')"
    column_for_day_nested = nil

    interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i
    val = if filter.blank?
            sequel = "
          SELECT current_date + sd.a AS dates,avg(s.count) as count   FROM generate_series(#{interval},0,1) AS sd(a) left join  (
          select count(t.user_id),t.date ,t.restaurant_id from(
          SELECT distinct on(receipts.user_id,restaurant_id, date(TIMEZONE('UTC',
          #{case_issue_date})
          AT TIME ZONE '#{time_zone}'))
          date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}'),
          receipts.user_id,restaurant_id FROM receipts
          INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
          WHERE receipts.deleted_at IS NULL AND
          (date(TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{time_zone}')
          between '#{(time_now - 6.days).to_date}' and '#{(time_now + 1.days).to_date}'and
          receipt_transactions.restaurant_id in (#{restaurants.map(&:id).join(',')}))) t
          group by t.date ,t.restaurant_id order by date asc) s on s.date = current_date + sd.a where
          current_date + sd.a between '#{(time_now - 6.days).to_date}' and '#{(time_now + 1.days).to_date}' group by current_date + sd.a"
            #puts sequel
            Receipt.find_by_sql(sequel)

          elsif show_interval == "day"
            Receipt.find_by_sql("SELECT current_date + sd.a AS dates,s.date,s.count , s.stamp FROM generate_series(-203,0,1) AS sd(a) left join (
 select count(t.user_id),t.created_at as date ,t.time_stamp as stamp from(
 SELECT distinct on(receipts.user_id, date(TIMEZONE('UTC',
 (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end))
 AT TIME ZONE '#{time_zone}'))
 TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end) AT TIME ZONE '#{time_zone}') as date,
 receipts.user_id ,receipts.created_at,receipt_transactions.time_stamp FROM receipts
 INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
  where
 receipt_transactions.restaurant_id = #{res_obj.id}) t
 group by t.created_at , stamp order by date asc) s on date(s.date) = date(current_date + sd.a)
where date(current_date + sd.a) = '#{Date.today}'")
          else
            tmp_filter = filter
            tmp_filter = "current_date + sd.a is not null" if filter == "all"
            if show_interval == "week"
              date_filter = "between '#{(time_now - 6.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -7;
            elsif show_interval == "month"
              date_filter = "between '#{(time_now - 30.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -30;
            elsif show_interval == "3month"
              date_filter = "between '#{(time_now - 90.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -90;
            elsif show_interval == "all"
              date_filter = "between '#{(time_now + interval.days).to_date}' and '#{(time_now + 1.days).to_date}'"
            end

#            sequel = " SELECT current_date + sd.a AS dates,avg(s.count) as count  FROM generate_series(#{interval},0,1) AS sd(a) left join  (
#select count(t.user_id),t.date ,t.restaurant_id from(
#SELECT distinct on(receipts.user_id,restaurant_id, date(TIMEZONE('UTC',
#          (
#receipts.created_at
#))
#          AT TIME ZONE 'EDT'))
#          date(TIMEZONE('UTC', (
# receipts.created_at
#)) AT TIME ZONE 'EDT'),
#          receipts.user_id ,restaurant_id FROM receipts
#          INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
#          WHERE receipts.deleted_at IS NULL AND
#          (date(TIMEZONE('UTC', (
# receipts.created_at
#)) AT TIME ZONE 'EDT') #{date_filter}
#               and
#          receipt_transactions.restaurant_id in (select restaurants.id from restaurants where chain_id=#{chain_obj.id}
#          AND restaurants.deleted_at is null))) t where t.date #{date_filter} group by t.date ,
#t.restaurant_id order by date,restaurant_id asc) s on s.date = current_date + sd.a where
#          current_date + sd.a is not null group by current_date + sd.a order by current_date + sd.a asc"
            date_coalesce = "date(coalesce(t1.issue_date,c.created_at))"
            sequel = "
                      SELECT current_date + sd.a AS dates,avg(s.count) as count  FROM generate_series(#{interval},0,1) AS sd(a) left join  (
                      select count(t.user_id),t.date ,t.restaurant_id from(
                      select distinct(c.user_id,
                                      t1.restaurant_id,
                                      #{date_coalesce}),
                                      c.user_id,t1.restaurant_id,
                                      #{date_coalesce}

                      from receipts c left join
                      receipt_transactions t1 on t1.receipt_id = c.id left join
                      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id > t2.id
                      where
                      c.deleted_at is NULL AND #{date_coalesce}  #{date_filter}
                      and t1.restaurant_id in(#{restaurant_sql})
                      ) t group by t.date, t.restaurant_id order by date, restaurant_id asc

                      ) s
                       on s.date = current_date + sd.a where
                      current_date + sd.a is not null group by current_date + sd.a order by
                      current_date + sd.a asc "

            Receipt.find_by_sql(sequel)

          end

    tmp_value = []

    unless val.blank?
      count = 0; count_before = 0
      val.each do |v|
        count = v.count.to_f.round.to_i
        count = nil if count.to_i == 0
       # if ENV['S3_BUCKET'] == "trelevant" and v.dates == "2013-11-26" and chain_obj.id == 19
       #   tmp_result << [v.dates.to_datetime.to_i * 1000, count - 2]
       # else
          tmp_result << [v.dates.to_datetime.to_i * 1000, count]
       # end
      end
    end
    puts "--------------------------------------------------------------"
    a = {:name => "AVG", :data => tmp_result}
    # puts a
    return {:name => "AVG", :data => tmp_result}

  end

  #DashboardQuery::ActiveMember.populate_active_members(Chain.first)
  def self.populate_active_members(chain)
    time_now = Time.zone.now
    #query_delete = "delete  from daily_users"
    connection = ActiveRecord::Base.connection
    #ActiveRecord::Base.connection.execute(query_delete)
    #puts "Executing ActiveMembers Migration -------------- #{a}"
    interval = -(DateTime.now.to_date - chain.created_at.to_date).to_i
    date_filter = "between '#{(time_now + interval.days).to_date}' and '#{(time_now + 1.days).to_date}'"

    query = " insert into daily_users(restaurant_id,total,active_date,chain_id)
 select t.restaurant_id,count(t.user_id),t.date,#{chain.id} as chain_id from(
 SELECT distinct on(receipts.user_id, date(TIMEZONE('UTC',
 (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end))
 AT TIME ZONE 'EDT'))
 date(TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end)) AT TIME ZONE 'EDT'),
 receipts.user_id,receipt_transactions.restaurant_id FROM receipts
 INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
 WHERE receipts.deleted_at IS NULL AND
 (date(TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end)) AT TIME ZONE 'EDT')
 #{date_filter} and
 receipt_transactions.restaurant_id in (select id from restaurants where chain_id = #{chain.id}))) t
 group by t.date,t.restaurant_id order by date asc
"
        puts "#{query}"


    a = Time.now

    ActiveRecord::Base.connection.execute(query)
    puts "Executing ActiveMembers Migration completed -------------- #{Time.now - a}"
  end


  #DashboardQuery::ActiveMember.populate_daily_users(restaurants, filter, show_interval, chain_obj)
  def self.populate_daily_users(restaurants, filter, show_interval, chain_obj, owner = nil)
    time_now = Time.zone.now
    time_zone = time_now.strftime('%Z')
    tmp_result = []

    #case_issue_date = "(CASE WHEN receipt_transactions.issue_date is NULL
    #                    THEN receipts.created_at
    #                    ELSE receipt_transactions.issue_date
    #                    END)"
    date_coalesce =  " active_date "
    restaurant_sql = restaurants.map(&:id).join(',')#restaurants.map(&:id)

    interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i

    if !owner.blank? and owner.chain_owner?
      restaurant_condition = "chain_id = #{owner.chain.id}"
    else
      restaurant_condition = "restaurant_id in (#{restaurant_sql})"
    end
    ## dashboard setting default 1 week / 1 month
    puts "filter == #{filter}"
    puts "show interfal == #{show_interval}"
    if filter.blank?
      ## dashboard setting
      if show_interval != "month"
        show_interval = "week"
        filter = "current_date + sd.a BETWEEN '#{(time_now - 6.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
      else
        filter = "current_date + sd.a BETWEEN '#{(time_now - 30.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
      end
    end

    tmp_filter = filter
    #puts tmp_filter
    tmp_filter = "current_date + sd.a is NOT NULL " if filter == "all"
    if show_interval == "week"
      date_filter = "BETWEEN '#{(time_now - 6.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
      interval = -7;
    elsif show_interval == "month"
      date_filter = "BETWEEN '#{(time_now - 30.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
      interval = -30;
    elsif show_interval == "3month"
      date_filter = "BETWEEN '#{(time_now - 90.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
      interval = -90;
    elsif show_interval == "all"
      date_filter = "BETWEEN '#{(time_now + interval.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
    elsif show_interval == "day"
      date_filter = "BETWEEN '#{(time_now - 1.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
    end


    #puts "#{sql_query}"



    #unless val.blank?
      restaurants.each do |res|
        sql_query = "
                      SELECT current_date + sd.a AS dates ,
 s.restaurant_id, s.total  FROM generate_series(#{interval},0,1) AS sd(a)
left join (
select active_date, restaurant_id, total from  daily_users where restaurant_id = #{res.id} ) s
on s.active_date =  current_date + sd.a where s.active_date #{date_filter}
        "
        val = Receipt.find_by_sql (sql_query)



        data = val.map { |x| [x.dates.to_datetime.to_i * 1000, x.total.to_i] }  rescue []
        tmp_result << {:name => res.dashboard_display_text,
                       :data => data}
      end
    #end
    return tmp_result
  end
end