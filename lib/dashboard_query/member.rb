class DashboardQuery::Member
  def self.average_member_chart_restaurant_user(filter, chains, restaurants, show_interval, chain_obj, user_ids = nil)
    restaurant_size = restaurants.size #+ 1
    tz_now = Time.zone.now

    time_zone = Time.zone.now.strftime("%Z")
    date_zone = "date(TIMEZONE('UTC',restaurant_users.created_at AT TIME ZONE '#{time_zone}'))"


    if filter.blank?
      if show_interval == "month"
        filter = "#{date_zone} between date('#{tz_now - 30.days}') and date('#{tz_now}')"
      elsif show_interval == "all"
        filter = "date(restaurant_users.created_at) is not null"
      elsif show_interval == "3month"
        filter = "#{date_zone} between date('#{tz_now - 90.days}') and date('#{tz_now}')"
      else
        filter = "#{date_zone} between date('#{tz_now - 7.days}') and date('#{tz_now}')"
      end
    elsif show_interval == "all"
      filter = "date(restaurant_users.created_at) is not null"
    end

    results = RestaurantUser.where(filter).group("date(restaurant_users.created_at)").
        where("restaurant_id in (?) and users.active = ?", restaurants,true).joins(:user).order("date(restaurant_users.created_at) asc").count
    count_before = self.avg_count_before_member(show_interval, restaurants) rescue 0
    #puts "RESTAURANT::MEMBER_CHART count_before_member #{count_before}"
    count_before = 0 if count_before.blank?
    count = 0
    tmp_result = []

    #puts "RESTAURANT::MEMBER_CHART result hash #{results}"
    results.each do |rs|
      puts "RESTAURANT::MEMBER_CHART count_before =  #{count_before}"
      count = (count_before) + (rs[1])
      puts "RESTAURANT::MEMBER_CHART count =  #{count}"
      puts "RESTAURANT::MEMBER_CHART result =  #{rs[1]}"
      unless rs[0].blank?
        final_result = (count.to_f).floor / restaurant_size
        puts "RESTAURANT::MEMBER_CHART final result =  #{final_result}"
        tmp_result <<  [rs[0].to_datetime.to_i  * 1000, final_result]
      end
      count_before = count
    end
    #puts "RESTAURANT::MEMBER_CHART TMP_RESULT #{tmp_result}"
    return [{:name => "Avg", :data => tmp_result}]
  end



  ################## AVERAGE ?
  def self.average_day_member_chart_restaurant_user(filter, chains, restaurants, show_interval, chain_obj, user_ids = nil)
    tz_now = Time.zone.now
    time_zone = Time.zone.now.strftime("%Z")
    date_zone = "date(TIMEZONE('UTC',created_at AT TIME ZONE '#{time_zone}'))"

    restaurant_size = restaurants.size
    results = RestaurantUser.where("restaurant_id in (?) and date(created_at) = date(now())", restaurants).
        select("count(user_id) as sum_user,date_trunc('hour',created_at) as updated_at").group("date_trunc('hour',created_at)")
    count_before = self.avg_count_before_member("day", restaurants) / restaurant_size  rescue 0
    #puts "RESTAURANT::MEMBER_CHART count_before_member #{count_before}"
    count_before = 0 if count_before.blank?
    count = 0
    tmp_result = []

    #puts "RESTAURANT::MEMBER_CHART result hash #{results}"
    results.each do |rs|
      count = (count_before + rs.sum_user.to_i) /  restaurant_size
      #puts "RESTAURANT::MEMBER_CHART count =  #{count}"
      unless rs[0].blank?
        tmp_result <<  [rs.updated_at.to_datetime.to_i  * 1000, count]
      end
      count_before = count
    end
    #puts "RESTAURANT::MEMBER_CHART TMP_RESULT #{tmp_result}"
    return [{:name => "Avg", :data => tmp_result}]
  end

  def self.avg_count_before_member(interval, restaurant_ids)
    time_zone = Time.zone.now.strftime("%Z")
    date_zone = "date(TIMEZONE('UTC',created_at AT TIME ZONE '#{time_zone}'))"
    if interval == "day"
      RestaurantUser.where("restaurant_id in (?) and #{date_zone} < date('#{Time.zone.now.to_date}')", restaurant_ids).count
    elsif interval == "week" || interval.blank?
      RestaurantUser.where("restaurant_id in (?) and #{date_zone} < date('#{Time.zone.now - 7.days}')",restaurant_ids).count
    elsif interval == "month"
      RestaurantUser.where("restaurant_id in (?) and #{date_zone} <= date('#{Time.zone.now - 30.days}')",restaurant_ids).count
    elsif interval == "3month"
      RestaurantUser.where("restaurant_id in (?) and #{date_zone} < date('#{Time.zone.now - 90.days}')",restaurant_ids).count
    elsif interval == "all"
      0
    end
  end
end


## code for member chart before using restaurant users tables

#def self.member_chart(filter, chains, restaurants, show_interval, chain_obj, user_ids = nil)
#  execution_time = Time.now
#  colors = {}; condition_day = nil;column_for_day = nil;
#  tmp_result = []
#  condition_not_day = "s.date = current_date + sd.a"
#  restaurants.each_with_index do |res, index|
#    colors.merge!({"#{res.dashboard_display_text}" => CHART_COLOR[index]})
#  end
#  time_zone = Time.zone.now.strftime("%Z")
#  case_issue_date = "(
#case when receipt_transactions.issue_date is null
#then receipts.created_at
#else receipt_transactions.issue_date
#end)"
#
#  date_zone = "date(TIMEZONE('UTC',
##{case_issue_date}
#AT TIME ZONE '#{time_zone}'))"
#  column_date = date_zone
#
#  if filter.blank? || show_interval.blank?
#    tmp_filter = "date(date) between '#{(Time.zone.now - 6.days).to_date}' and '#{(Time.zone.now + 1.days).to_date}'"
#  else
#    tmp_filter = "#{filter}"
#    tmp_filter = "date(date) is not null" if show_interval == "all"
#    if show_interval == "day"
#      condition_day = ", time_stamp"
#      column_for_day = ",t.time_stamp as stamp"
#      column_date = " #{case_issue_date}"
#      column_for_day_nested = ",s.stamp"
#      condition_not_day =       "date(s.date) = date(current_date + sd.a)"
#      1.upto(24) do |hour|
#        tmp_result << [Time.zone.now.utc.change(:hour => hour).to_i * 1000, nil]
#      end
#    end
#
#  end
#
#  user_ids = user_ids.map(&:id).join(',')
#  restaurant_ids = restaurants.map(&:id).join(',')
#  inverval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i #+ (-90)
#  results =
#      Receipt.find_by_sql("
#        SELECT current_date + sd.a AS dates,s.date,s.count #{column_for_day_nested} FROM generate_series(#{inverval},0,1) AS sd(a) left join  (
#select count(t.count), t.date #{column_for_day}, t.dashboard_display_text from
#(SELECT count(distinct receipts.user_id),MIN(#{column_date}) as date,
#restaurants.dashboard_display_text #{condition_day} FROM receipts inner join receipt_transactions on
#receipts.id = receipt_transactions.receipt_id
#inner join restaurants on restaurants.id = receipt_transactions.restaurant_id
#WHERE receipts.deleted_at IS NULL AND (receipt_transactions.restaurant_id in (#{restaurant_ids})
#and
#receipts.user_id in(#{user_ids}))
#GROUP BY receipts.user_id ,dashboard_display_text #{condition_day}) t
#where(date is not null)
#group by date,dashboard_display_text #{condition_day} ORDER BY date asc) s
#on #{condition_not_day}
#                          ")
#
#  data = []
#  color = nil, count = 0
#  res = nil
#  summary_count = RestaurantUser.where(:restaurant_id => restaurants.first.id).count
#  puts "Dashboard::MemberChart ---- Query took = #{Time.now - execution_time} "
#
#  last_count = 0
#  results.each_with_index do |result, idx|
#    tmp_date = result.date.nil? ? result.dates : result.date
#    dt = tmp_date.to_datetime.in_time_zone((Time.zone_offset(time_zone)/3600))#.utc
#    dt_date = dt.to_date
#
#    hour = dt.hour
#    min = dt.min
#    color = colors[restaurants.first.dashboard_display_text]
#    count = result.count.to_i + count  unless result.count.blank?
#
#    if count == last_count
#      res = nil
#    else
#      res = count
#    end
#
#
#    if show_interval == "day"
#      if dt_date == Date.today || dt_date == Time.zone.now.to_date
#        dt = dt.change(:hour => hour)
#        if  !result.stamp.blank?
#          time_stamp = result.stamp
#
#          hour = time_stamp[0..1]
#          min = time_stamp[3..4]
#          zn =  time_stamp[5..6]
#          hour = hour.to_i + 12 if zn.downcase == "pm" and hour.to_i < 12
#          dt = dt.change(:hour => hour.to_i, :min => min.to_i)
#        end
#
#        index = nil
#        dt = dt.change(:hour => hour.to_i, :min => min.to_i)
#
#        ## force to utc time zone
#        dt = Time.parse(dt.strftime('%Y-%m-%d %I:%M:%S UTC')).to_datetime.change(:hour => hour.to_i, :min => min.to_i)
#        data = [dt.to_i * 1000, count]
#
#
#        tmp_result << data
#      end
#    elsif show_interval == "week" || show_interval.blank?
#      tmp_result << [dt.to_i * 1000, res] if dt_date > (Time.zone.now - 7.days).to_date and dt_date <= Date.today and res != nil
#    elsif show_interval == "month"
#      tmp_result << [dt.to_i * 1000, res]   if dt_date > (Time.zone.now - 1.months).to_date and dt_date <= Date.today and res != nil
#    elsif show_interval == "3month"
#      tmp_result << [dt.to_i * 1000, res]  if dt_date > (Time.zone.now - 3.months).to_date and dt_date <= Date.today and res != nil
#    else
#      tmp_result << [dt.to_i * 1000, res] if res != nil
#    end
#    last_count = count
#  end
#  tmp_result = tmp_result.sort! {|a,b| a[0] <=> b[0] }
#  if tmp_result.last and tmp_result.last[1]  != summary_count
#    tmp_result[tmp_result.size - 1][1] = summary_count
#  end
#  puts "Dashboard::MembersChart  ---- Processing Took = #{Time.now - execution_time}"
#
#  return {:name => restaurants.first.dashboard_display_text, :colors => color, :data => tmp_result}
#end

#def self.member_chart(filter, chains, restaurants, show_interval, chain_obj, user_ids = nil)
#  ###17 apr 2013
#  execution_time = Time.now; time_zone = Time.zone.now.strftime("%Z")
#  colors = {}; tmp_result = []
#
#  restaurants.each_with_index do |res, index|
#    colors.merge!({"#{res.dashboard_display_text}" => CHART_COLOR[index]})
#  end
#
#  user_ids = user_ids.map(&:id).join(',')
#  restaurant_ids = restaurants.map(&:id).join(',')
#
#  interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i #+ (-90)
#
#  query = DashboardQuery::Member.get_query_member(restaurant_ids, chain_obj.id, show_interval, interval)
#  results = ReceiptTransaction.find_by_sql(query)
#
#  data = []; final_result = []
#  color = nil, count = 0
#  res = nil
#  summary_count = RestaurantUser.where(:restaurant_id => restaurants.first.id).count
#  puts "Dashboard::MemberChart ---- Query took = #{Time.now - execution_time} "
#  changed_dashboard_display = nil
#  last_count = 0
#  group_restaurant = results.group_by(&:restaurant_id)
#
#  group_restaurant.keys.each_with_index do |k,index|
#    dp = Restaurant.find(k).dashboard_display_text
#            data << {
#              :name => dp,
#              :color => colors["#{dp}"],
#              :data => self.regenerate_data(group_restaurant[k], show_interval, filter, chain_obj)
#            }
#          end
#
#  #results.each_with_index do |result, idx|
#  #  puts result.attributes
#  #  puts result.class
#  #  puts result.restaurant_id
#  #  #puts "restaurant_id = #{result.restaurant_id}"
#  #  #puts "sum = #{result.sum}"
#  #
#  #  if (result.restaurant_id.to_i == changed_dashboard_display.to_i) or changed_dashboard_display == nil
#  #    puts "get in next restaurant_id"
#  #    tmp_result = []
#  #    count = 0
#  #    last_count = 0
#  #  end
#  #    tmp_date = result.dates.nil? ? result.date : result.dates
#  #    dt = tmp_date.to_datetime.in_time_zone((Time.zone_offset(time_zone)/3600)) #.utc
#  #    dt_date = dt.to_date
#  #
#  #
#  #    color = colors[result.dashboard_display_text]
#  #    count = result.sum.to_i + count unless result.sum.blank?
#  #
#  #    if count == last_count
#  #      res = nil
#  #    else
#  #      res = count
#  #    end
#  #
#  #
#  #    if show_interval == "day"
#  #    elsif show_interval == "week" || show_interval.blank?
#  #      tmp_result << [dt.to_i * 1000, res] if dt_date > (Time.zone.now - 7.days).to_date and dt_date <= Date.today and res != nil
#  #    elsif show_interval == "month"
#  #      tmp_result << [dt.to_i * 1000, res] if dt_date > (Time.zone.now - 1.months).to_date and dt_date <= Date.today and res != nil
#  #    elsif show_interval == "3month"
#  #      tmp_result << [dt.to_i * 1000, res] if dt_date > (Time.zone.now - 3.months).to_date and dt_date <= Date.today and res != nil
#  #    else
#  #      tmp_result << [dt.to_i * 1000, res] if res != nil
#  #    end
#  #    last_count = count
#  #
#  #
#  #  if changed_dashboard_display != result.restaurant_id
#  #    tmp_result = tmp_result.sort! { |a, b| a[0] <=> b[0] }
#  #    summary_count = RestaurantUser.where(:restaurant_id => result.restaurant_id).count
#  #    if tmp_result.last and tmp_result.last[1] != summary_count
#  #      tmp_result[tmp_result.size - 1][1] = summary_count
#  #    end
#  #    final_result << {:name => result.dashboard_display_text, :colors => color, :data => tmp_result}
#  #  end
#  #  changed_dashboard_display = result.restaurant_id
#  #  puts "changed_dashboard_display = #{changed_dashboard_display}"
#  #
#  #
#  # # end ## end restaurant if
#  #end ## end loop
#  #
#  ##puts "Dashboard::MembersChart  ---- Processing Took = #{Time.now - execution_time}"
#  ##puts "final result #{final_result}"
#  #return final_result
#
#  return data
#end


#def self.regenerate_data(val, show_interval, filter,chain_obj)
#  res = []
#  total = 0
#  unless val.blank?
#    m = val.group_by(&:dates)
#    m = m.sort
#    next_date = m[0][0].to_date
#    m.each do |x|
#      total += x[1].size
#      if next_date.to_s != x[0].to_s
#        diff = (x[0].to_s.to_date - next_date.to_date).to_i
#        1.upto(diff -1) do |ll|
#          res << [(next_date + ll).to_date.to_s, total]
#        end
#      end
#
#      res << [x[0].to_date.to_s, total]
#
#      next_date = x[0].to_date + 1.days
#    end
#  end
#
#  days = 7
#  case show_interval
#    when "day";days = 0;
#    when "week";days = 7;
#    when "month"; days = 30;
#    when "3month";days = 90;
#  end
#
#  if filter == "all"
#    days = (DateTime.now.to_date - chain_obj.created_at.to_date).to_i
#    days = 90 + days if days <= 90
#  end
#
#  tmp_value = []
#  total = 0
#  tmp_total = 0
#  tmp_date = nil
#  days.downto(0) do |i|
#    res.each do |r|
#      if r[0] == (Time.zone.now - i.days).to_date.to_s
#        total =  r[1]  if r[1].to_i > 0
#      end
#    end
#
#    dt  = (Time.zone.now - i.days).to_date.to_datetime
#    if tmp_total != total or tmp_date != dt
#      tmp_value <<  [dt.to_i * 1000, total]
#    else
#      tmp_value <<  [dt.to_i * 1000, nil]
#    end
#    tmp_total = total
#
#    tmp_date = dt
#
#  end
#  return tmp_value
#end