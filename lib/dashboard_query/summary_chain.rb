class DashboardQuery::SummaryChain

  #DashboardQuery::SummaryChain.sum_total_members(restaurants)
  def self.sum_total_members(restaurants)
   total_restaurants = RestaurantUser.joins(:restaurant).select("distinct(restaurant_id)").
       where("restaurant_id in (?) and users.active = ? and restaurants.status = 't'", restaurants.map(&:id),true).joins(:user)
   total_restaurants = total_restaurants.size

   return RestaurantUser.select("count(distinct(user_id, restaurant_id))").
         where("restaurant_id in (?) and users.active = ?", restaurants.map(&:id),true).joins(:user).first.count.to_f / total_restaurants.to_f
  end

  #DashboardQuery::SummaryChain.sum_average_month(restaurants)
  def self.sum_average_month(restaurants)
    #member this month / restaurant new member in this month
    dt_zone_created_at = "date(TIMEZONE('UTC', restaurant_users.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
    dt_zone_joined_date = "date(TIMEZONE('UTC', restaurant_users.joined_date) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"

    ## restaurant who get new member in this month
    restaurant_month_member = RestaurantUser.joins(:restaurant).select('distinct(restaurant_id)').
        where("(#{dt_zone_joined_date} between  date(?) and date(?) and restaurant_id in (?) and restaurants.status='t')
                                 or
                                 (#{dt_zone_created_at} between  date(?) and date(?) and restaurant_id  in (?))",
              Time.zone.now.beginning_of_month, Time.zone.now,restaurants.map(&:id),
              Time.zone.now.beginning_of_month, Time.zone.now,
                         restaurants.map(&:id))
    restaurant_month_member =   restaurant_month_member.size
    puts "-------------------------------------------"
    result = 0
    if restaurant_month_member > 0
    result = RestaurantUser.where("(#{dt_zone_joined_date} between  date(?) and date(?) and restaurant_id in (?))
                                 or
                                 (#{dt_zone_created_at} between  date(?) and date(?) and restaurant_id  in (?))",
                                  Time.zone.now.beginning_of_month, Time.zone.now,restaurants.map(&:id),
                                  Time.zone.now.beginning_of_month, Time.zone.now,
                                  restaurants.map(&:id)).count.to_f / restaurant_month_member.to_f
    end
    return result
  end

  def self.sum_average_last_month(restaurants)
    #member this month / restaurant new member in this month
    dt_now = Time.zone.now
    dt_zone_created_at = "date(TIMEZONE('UTC', restaurant_users.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
    dt_zone_joined_date = "date(TIMEZONE('UTC', restaurant_users.joined_date) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"

    ## restaurant who get new member in this month
    restaurant_month_member = RestaurantUser.joins(:restaurant).select('distinct(restaurant_id)').
        where("(#{dt_zone_joined_date} between  date(?) and date(?) and restaurant_id in (?) and restaurants.status='t')
                                 or
                                 (#{dt_zone_created_at} between  date(?) and date(?) and restaurant_id  in (?))",
              (dt_now -1.month).beginning_of_month, (dt_now -1.month).end_of_month,restaurants.map(&:id),
              (dt_now -1.month).beginning_of_month, (dt_now -1.month).end_of_month,
              restaurants.map(&:id))
    restaurant_month_member =   restaurant_month_member.size
    puts "-------------------------------------------"
    result = 0
    if restaurant_month_member > 0
      result = RestaurantUser.where("(#{dt_zone_joined_date} between  date(?) and date(?) and restaurant_id in (?))
                                 or
                                 (#{dt_zone_created_at} between  date(?) and date(?) and restaurant_id  in (?))",
                                    (dt_now -1.month).beginning_of_month,  (dt_now -1.month).end_of_month,restaurants.map(&:id),
                                    (dt_now -1.month).beginning_of_month,  (dt_now -1.month).end_of_month,
                                    restaurants.map(&:id)).count.to_f / restaurant_month_member.to_f
    end
    return result
  end

  #DashboardQuery::SummaryChain.sum_summary_average_survey(restaurants,surveys)
  def self.sum_summary_average_survey(restaurants,surveys)
    survey_ids = surveys.map(&:id)
    survey_ids =  surveys.map(&:survey_id) if survey_ids.compact.blank?
    active_restaurants = restaurants.delete_if {|x| x.status == false}
    active_restaurants = active_restaurants.size
    restaurant_with_averages = []
    restaurants.each do |rs|
      ag =  RestaurantSurveyAvg.where("survey_id in (?) and restaurant_id = #{rs.id}", survey_ids).first
      unless ag.blank?
        restaurant_with_averages << rs if ag.averages.to_i > 0
      end
    end
    restaurant_with_averages = restaurant_with_averages.flatten.uniq

    avg = {:all => 0, :q1 => 0, :q2 => 0, :q3 => 0, :q4 => 0, :q5 => 0}
#    average = RestaurantSurveyAvg.where("restaurant_id in(?) and survey_id in (?)", restaurant_with_averages.map(&:id),survey_ids ).select("sum(averages)/#{restaurant_with_averages.size} as avg")
    average = RestaurantSurveyAvg.where("restaurant_id in(?)
                                         and survey_id in (?)",
                                        restaurant_with_averages.map(&:id),
                                        survey_ids ).select("distinct on (restaurant_id)   *")
    begin
      #avg =  {:all => average.first.avg.to_f.round(2), :q1 => 0, :q2 => 0, :q3 => 0, :q4 => 0, :q5 => 0}
      avg =  {:all => (average.map(&:averages).sum / average.size).round(2), :q1 => 0, :q2 => 0, :q3 => 0, :q4 => 0, :q5 => 0}
    rescue
    end
    return  avg
  end

  ## DashboardQuery::SummaryChain.sum_avg_visit(restaurants)
  def self.sum_avg_visit(restaurants)
    #total_restaurant = AvgVisit.where("restaurant_id in (?) and average > 0 and average < 100", restaurants.map(&:id)).
    #    select("distinct(restaurant_id)")
    #
    #total_restaurant = total_restaurant.map(&:restaurant_id)
    #return (AvgVisit.where("restaurant_id in (?) and average > 0 and average < 100", total_restaurant).
    #    select("(30/ avg(average)) as avg").first.avg.to_f / total_restaurant.size).round(2)
    total_coming = 0
    restaurant_avg_coming = 0

    restaurants.each do |restaurant|
      coming = restaurant.avg_coming.round(1)
      total_coming += coming
      restaurant_avg_coming += 1 if coming > 0 and restaurant.status == true
    end

    return (total_coming.round(2) / restaurant_avg_coming).to_f.round(2)
  end

  ### version 2

  def self.sum_total_members_v2(restaurants)
    #total = Rails.cache.fetch("sum_total_members_v2__#{restaurants.map(&:id).join('_')}",
    #                          :expires_in => 1.hours) do
     total=   DbSummary.where("restaurant_id in (?) and total_member > 0", restaurants).
        average('total_member')
    #end
    return total.to_f
  end


  def self.sum_average_month_v2(restaurants)
    #total = Rails.cache.fetch("sum_average_month_v2__#{restaurants.map(&:id).join('_')}",
    #                          :expires_in => 1.hours) do
    total =    DbSummary.where("restaurant_id in (?) and member_this_month > 0", restaurants).
            average('member_this_month').to_f
    #end
    return total.ceil
  end

  def self.sum_average_last_month_v2(restaurants)
    #total = Rails.cache.fetch("sum_average_last_month_v2__#{restaurants.map(&:id).join('_')}",
    #                          :expires_in => 1.hours) do
    total =    DbSummary.where("restaurant_id in (?) and member_last_month > 0", restaurants).
            average('member_last_month').to_f
    #end
    return total.ceil
  end


  def self.sum_summary_average_survey_v2(restaurants)
    #total = Rails.cache.fetch("sum_summary_average_survey_v2_#{restaurants.map(&:id).join('_')}",
    #                          :expires_in => 1.hours) do
     total =  DbSummary.where("restaurant_id in (?) and avg_general > 0", restaurants).
          average('avg_general').to_f
    #end
    return total.to_f
  end

  def self.sum_avg_visit_v2(restaurants)
    #total = Rails.cache.fetch("sum_avg_visit_v2_#{restaurants.map(&:id).join('_')}",
    #                          :expires_in => 1.hours) do
     total = DbSummary.where("restaurant_id in (?) and avg_visit_month > 0", restaurants).
          average('avg_visit_month').to_f
    #end
    return total.to_f
  end


end