class DashboardQuery::ActiveMemberV2
  #DashboardQuery::ActiveMember.populate_daily_users(restaurants, filter, show_interval, chain_obj)
  def self.populate_daily_users(restaurants, filter, show_interval, chain_obj, owner = nil)
    time_now = Time.zone.now
    time_zone = time_now.strftime('%Z')
    tmp_result = []


    date_coalesce = " active_date "
    restaurant_sql = restaurants.map(&:id).join(',') #restaurants.map(&:id)

    interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i

    if !owner.blank? and owner.chain_owner?
      restaurant_condition = "chain_id = #{owner.chain.id}"
    else
      restaurant_condition = "restaurant_id in (#{restaurant_sql})"
    end
    ## dashboard setting default 1 week / 1 month
    puts "filter == #{filter}"
    puts "show interfal == #{show_interval}"
    if filter.blank?
      ## dashboard setting
      if show_interval != "month"
        show_interval = "week"
        date_filter = "current_date + sd.a BETWEEN '#{(time_now - 6.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
      else
        date_filter = "current_date + sd.a BETWEEN '#{(time_now - 30.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
      end
    end

    tmp_filter = filter
    #puts tmp_filter
    tmp_filter = "current_date + sd.a is NOT NULL " if filter == "all"
    if show_interval == "week"
      date_filter = "BETWEEN '#{(time_now - 6.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
      interval = -7;
    elsif show_interval == "month"
      date_filter = "BETWEEN '#{(time_now - 30.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
      interval = -30;
    elsif show_interval == "3month"
      date_filter = "BETWEEN '#{(time_now - 90.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'";
      interval = -90;
    elsif show_interval == "all"
      date_filter = "BETWEEN '#{(time_now + interval.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
    elsif show_interval == "day"
      date_filter = "BETWEEN '#{(time_now - 1.days).to_date}'
                               AND '#{(time_now + 1.days).to_date}'"
    end

    sql_query = "
                  select distinct on(dates,restaurant_id) * from (
                    SELECT current_date + sd.a AS dates ,
                    s.restaurant_id, s.total  FROM generate_series(#{interval},0,1) AS sd(a)
                    left OUTER join (
                    select active_date, restaurant_id, total from  daily_users
                    where #{restaurant_condition} AND active_date #{date_filter}) s
                    on s.active_date =  current_date + sd.a ) m
"
    val = ReceiptTransaction.find_by_sql (sql_query)
    #restaurants.each do |res|
    #  #sql_query = "
    #  #                SELECT current_date + sd.a AS dates ,
    #  #                s.restaurant_id, s.total  FROM generate_series(#{interval},0,1) AS sd(a)
    #  #                left join (
    #  #                select active_date, restaurant_id, total from  daily_users
    #  #                where restaurant_id = #{res.id} ) s
    #  #                on s.active_date =  current_date + sd.a where s.active_date #{date_filter}
    #  #"
    #
    #
    #
    #  data = val.map { |x| [x.dates.to_datetime.to_i * 1000, x.total.to_i] } rescue []
    #  tmp_result << {:name => res.dashboard_display_text,
    #                 :data => data}
    #end
    unless val.blank?
      restaurants.each do |res|
        data = val.group_by(&:restaurant_id)[res.id].map { |x| [x.dates.to_datetime.to_i * 1000,
                                                     x.total.to_i] }.
            sort! { |a, b| a[0] <=> b[0]
        } rescue []

        tmp_result << {:name => res.dashboard_display_text,
                       :data => data}
      end
    else
      restaurants.each do |res|
        data = val.map{|x| [x.dates.to_datetime.to_i * 1000,
                            x.total.to_i]}
        tmp_result << {:name => res.dashboard_display_text,
                       :data => data}
      end
    end
    return tmp_result
  end

  def self.average_populate(restaurants, filter, show_interval, chain_obj, owner = nil)
    time_now = Time.zone.now
    time_zone = time_now.strftime('%Z')
    tmp_result = []
    restaurant_sql = restaurants.map(&:id).join(',')#restaurants.map(&:id)
    interval = -(DateTime.now.to_date - chain_obj.created_at.to_date).to_i

    if !owner.blank? and owner.chain_owner?
      restaurant_condition = "chain_id = #{owner.chain.id}"
    else
      restaurant_condition = "restaurant_id in (#{restaurant_sql})"
    end

    tmp_filter = filter
    tmp_filter = "current_date + sd.a is not null" if filter == "all"
    if filter.blank?
      date_filter = "between '#{(time_now - 6.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -7;
    end
    if show_interval == "week"
      date_filter = "between '#{(time_now - 6.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -7;
    elsif show_interval == "month"
      date_filter = "between '#{(time_now - 30.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -30;
    elsif show_interval == "3month"
      date_filter = "between '#{(time_now - 90.days).to_date}' and '#{(time_now + 1.days).to_date}'"; interval = -90;
    elsif show_interval == "all"
      date_filter = "between '#{(time_now + interval.days).to_date}' and '#{(time_now + 1.days).to_date}'"
    end

    sql_query = "
        SELECT current_date + sd.a AS dates ,
        avg(s.total) as subtotal FROM generate_series(-686,0,1) AS sd(a)
        left join (
        select active_date, total from daily_users
        where #{restaurant_condition} ) s
        on s.active_date = current_date + sd.a where s.active_date #{date_filter}
        group by  current_date + sd.a order by dates
    "
    val = ReceiptTransaction.find_by_sql(sql_query)

    puts val
    puts "*****" * 100
    tmp_value = []

    unless val.blank?
      count = 0; count_before = 0
      val.each do |v|
        count = v.subtotal.to_f.round.to_i
        count = nil if count.to_i == 0
        # if ENV['S3_BUCKET'] == "trelevant" and v.dates == "2013-11-26" and chain_obj.id == 19
        #   tmp_result << [v.dates.to_datetime.to_i * 1000, count - 2]
        # else
        tmp_result << [v.dates.to_datetime.to_i * 1000, count]
        # end
      end
    end
    puts "--------------------------------------------------------------"
    a = {:name => "AVG", :data => tmp_result}
                                                    # puts a
    return {:name => "AVG", :data => tmp_result}
  end

  #ActiveMemberV2.populate_daily_data(chain)
  def self.populate_daily_data(chain)
    time_now = Time.zone.now
    interval = -3 # 3 days before until now its running
    date_filter = "between '#{(time_now + interval.days).to_date}' and '#{(time_now + 1.days).to_date}'"

    query_to_delete = "
      DELETE FROM daily_users where date(active_date) #{date_filter} and chain_id = #{chain.id}
    "

    ActiveRecord::Base.connection.execute(query_to_delete)
    ## delete it first if there are any existing data forit

    puts "Deleting ActiveMembers Data  #{date_filter}"


    query = " insert into daily_users(restaurant_id,total,active_date,chain_id)
 select t.restaurant_id,count(t.user_id),t.date,#{chain.id} as chain_id from(
 SELECT distinct on(receipts.user_id, date(TIMEZONE('UTC',
 (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end))
 AT TIME ZONE 'EDT'))
 date(TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end)) AT TIME ZONE 'EDT'),
 receipts.user_id,receipt_transactions.restaurant_id FROM receipts
 INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
 WHERE receipts.deleted_at IS NULL AND receipts.user_id is not null AND
 (date(TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end)) AT TIME ZONE 'EDT')
 #{date_filter} and
 receipt_transactions.restaurant_id in (select id from restaurants where chain_id = #{chain.id}))) t
 group by t.date,t.restaurant_id order by date asc
"
    puts "#{query}"


    a = Time.now

    ActiveRecord::Base.connection.execute(query)
    puts "Executing ActiveMembers Migration completed -------------- #{Time.now - a}"
  end

  def self.populate_daily_data_v2(chain, restaurant_id)
    # update 4 august 2015
    time_now = Time.zone.now
    interval = -3 # 3 days before until now its running
    date_filter = "between '#{(time_now + interval.days).to_date}' and '#{(time_now + 1.days).to_date}'"

    query_to_delete = "
      DELETE FROM daily_users where date(active_date) #{date_filter} and chain_id = #{chain.id}
      and restaurant_id = #{restaurant_id}
    "

    ActiveRecord::Base.connection.execute(query_to_delete)
    ## delete it first if there are any existing data forit

    puts "Deleting ActiveMembers Data  #{date_filter}"


    query = " insert into daily_users(restaurant_id,total,active_date,chain_id)
 select t.restaurant_id,count(t.user_id),t.date,#{chain.id} as chain_id from(
 SELECT distinct on(receipts.user_id, date(TIMEZONE('UTC',
 (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end))
 AT TIME ZONE 'EDT'))
 date(TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end)) AT TIME ZONE 'EDT'),
 receipts.user_id,receipt_transactions.restaurant_id FROM receipts
 INNER JOIN receipt_transactions ON receipt_transactions.receipt_id = receipts.id
 WHERE receipts.deleted_at IS NULL AND receipts.user_id is not null AND
 (date(TIMEZONE('UTC', (
case when receipt_transactions.issue_date is null
then receipts.created_at
 else receipt_transactions.issue_date
 end)) AT TIME ZONE 'EDT')
 #{date_filter} and
 receipt_transactions.restaurant_id = #{restaurant_id})) t
 group by t.date,t.restaurant_id order by date asc
"
    puts "#{query}"


    a = Time.now

    ActiveRecord::Base.connection.execute(query)
    puts "Executing ActiveMembers Migration completed -------------- #{Time.now - a}"
  end

end