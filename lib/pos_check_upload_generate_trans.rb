class PosCheckUploadGenerateTrans < struct(:chain_id)

def perform
  total_pages = Receipt.where("chain_id = ? and is_receipt_barcode is TRUE and date(created_at) < date(?)", chain_id,'2016-04-01').order("created_at desc").total_pages

   1.upto(total_pages).each do |page|
     receipts = Receipt.where("chain_id = ? and is_receipt_barcode is TRUE and date(created_at) < date(?)", chain_id,'2016-04-01').order("created_at desc")

     receipts.each do |receipt|


        pos_check_upload = receipt.pos_check_upload
     end
   end
end


  def parser_pos_used_type(receipt, offer)
    pos_check_upload = receipt.pos_check_upload
    case pos_check_upload.pos_used_type
      when 1 #MICROS/NORTHKEY
        restaurant_id = pos_check_upload.pos_location.restaurant_id
        json_check = YAML.load(pos_check_upload.xml_data) rescue nil
        if json_check.class == ActiveSupport::HashWithIndifferentAccess
          pos_xml = PosReceiptJson.new(pos_check_upload.xml_data)
        else
          pos_xml = PosReceiptXml.new(pos_check_upload.xml_data)
        end
        restaurant_offer = RestaurantOffer.where(:restaurant_id => restaurant_id, :offer_id => offer.id).first rescue nil

        "**************--------- get reward redeem discount ---------- ***********"
        check_id = pos_xml.check_id
        seq_num = pos_xml.seq_num
        p "-- check id is #{check_id}"
        p "--- sequence number is #{seq_num}"
        total_reward_discount = RewardDiscount.sum(:discount, :conditions => {:check_id => check_id, :seq_num => seq_num, :restaurant_id => restaurant_id})
        p total_reward_discount
        p "----- total discount selected is #{total_reward_discount}"

        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.restaurant_offer_id = (restaurant_offer.id rescue nil)
        receipt_transaction.subtotal = pos_xml.sub_total
        receipt_transaction.tax = pos_xml.tax
        receipt_transaction.restaurant_id = restaurant_id
        receipt_transaction.total_discount = total_reward_discount
        receipt_transaction.receipt_id = receipt.id
        receipt_transaction.status = receipt.status
        receipt_transaction.receipt_number = pos_xml.receipt_number

        valid_date = pos_xml.valid_date?

        receipt_transaction.issue_date = pos_xml.converted_date
        receipt_transaction.time_stamp = (pos_xml.converted_time.strip rescue nil)
        receipt_transaction.receipt_date = pos_xml.converted_date
        receipt_transaction.receipt_time = (pos_xml.converted_time.strip rescue nil)
        sql = receipt_transaction.class.arel_table.create_insert.tap { |im| im.insert(record.send(:arel_attributes_values, false)) }.to_sql
        ActiveRecord::Base.connection.execute(sql)
      when 2 #XPIENT
        json_check =  YAML.load(pos_check_upload.xml_data)
        xpient_check = XpientCheck.new(json_check)
        chain = receipt.chain
        restaurant_id = pos_check_upload.pos_location.restaurant_id
        location = Restaurant.find(restaurant_id)
        restaurant_offer = RestaurantOffer.where(:restaurant_id => restaurant_id, :offer_id => offer.id).first rescue nil
        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.receipt_id = receipt.id
        receipt_transaction.status = receipt.status
        receipt_transaction.restaurant_offer_id = (restaurant_offer.id rescue nil)
        receipt_transaction.subtotal = xpient_check.sub_total
        receipt_transaction.tax = xpient_check.tax
        receipt_transaction.restaurant_id = restaurant_id
        receipt_transaction.receipt_number = xpient_check.receipt_number

        valid_date = xpient_check.valid_date?

        receipt_transaction.receipt_number = xpient_check.receipt_number
        receipt_transaction.restaurant_id = location ? location.id : nil
        receipt_transaction.issue_date = xpient_check.converted_date
        receipt_transaction.time_stamp = (xpient_check.converted_time.strip rescue nil)
        receipt_transaction.receipt_date = xpient_check.converted_date
        receipt_transaction.receipt_time = (xpient_check.converted_time.strip rescue nil)
        sql = receipt_transaction.class.arel_table.create_insert.tap { |im| im.insert(record.send(:arel_attributes_values, false)) }.to_sql
        ActiveRecord::Base.connection.execute(sql)
      when 3 #NCR
         #runs on NPREL
      when 4 #TREATWARE
        json_check =  YAML.load(pos_check_upload.xml_data)
        treatware_check = TreatwareCheck.new(json_check)
        restaurant_id = pos_check_upload.pos_location.restaurant_id
        chain = receipt.chain
        location = Restaurant.find(restaurant_id)
        restaurant_offer = RestaurantOffer.where(:restaurant_id => restaurant_id, :offer_id => offer.id).first rescue nil

        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.receipt_id = receipt.id
        receipt_transaction.status = receipt.status
        receipt_transaction.restaurant_offer_id = (restaurant_offer.id rescue nil)
        receipt_transaction.subtotal = treatware_check.sub_total
        receipt_transaction.tax = treatware_check.tax
        receipt_transaction.restaurant_id = restaurant_id
        valid_date = treatware_check.valid_date?
        receipt_transaction.issue_date = treatware_check.converted_date
        receipt_transaction.time_stamp = (treatware_check.converted_time.strip rescue nil)
        receipt_transaction.receipt_date = treatware_check.converted_date
        receipt_transaction.receipt_time = (treatware_check.converted_time.strip rescue nil)
        receipt_transaction.receipt_number = treatware_check.receipt_number
        sql = receipt_transaction.class.arel_table.create_insert.tap { |im| im.insert(record.send(:arel_attributes_values, false)) }.to_sql
        ActiveRecord::Base.connection.execute(sql)
    end

    return restaurant_id
  end
end