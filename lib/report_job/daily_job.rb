class ReportJob::DailyJob < Struct.new(:chain_id, :kind, :start_date, :end_date, :owner,:file_type)
  
  def perform
    report_server = Setting.jasper.server
    report_user = Setting.jasper.username
    report_password = Setting.jasper.password

    report_type,last_date =
      case kind
      when "daily"
        ["Day", start_date - 1.days]
      when "weekly"
        ["Week", start_date  - 7.days]
      when "monthly"
        ["Month", start_date  - 30.days]
      end

    #login to JasperServer and acquire cookies for its session
    response_login =
      RestClient.post "#{report_server}/login.html",
      :j_username => report_user,
      :j_password => report_password

    #get resourceDescriptor before generate the report
    response_resource =
      RestClient.get "#{report_server}/rest/resource/reports/samples/daily_report/daily",
      {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}

    doc = Nokogiri::XML.parse(response_resource)
    chain_param = Nokogiri::XML::Node.new "parameter", doc
    chain_param['name'] = 'chain_id'
    chain_param.content = chain_id

    start_param = Nokogiri::XML::Node.new "parameter", doc
    start_param['name'] = 'start_date'
    start_param.content = start_date.to_datetime.to_i * 1000

    ## params date daily / weekly / monthly before
    kind_param = Nokogiri::XML::Node.new "parameter", doc
    kind_param['name'] = 'kind'
    kind_param.content = last_date.to_datetime.to_i * 1000

    type_param = Nokogiri::XML::Node.new "parameter", doc
    type_param['name'] = 'report_type'
    type_param.content = report_type


    end_param = Nokogiri::XML::Node.new "parameter", doc
    end_param['name'] = 'end_date'
    end_param.content = end_date.to_datetime.to_i * 1000


    doc.children.first.add_child(chain_param)
    doc.children.first.add_child(start_param)
    doc.children.first.add_child(end_param)
    doc.children.first.add_child(kind_param)
    doc.children.first.add_child(type_param)

    response_put_report =
      RestClient.put "#{report_server}/rest/report/reports/samples/daily_report/daily?RUN_OUTPUT_FORMAT=HTML",
      doc.to_s,
      {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}} if file_type.blank? or file_type.downcase == "html"

    response_put_report_pdf =
      RestClient.put "#{report_server}/rest/report/reports/samples/daily_report/daily?RUN_OUTPUT_FORMAT=PDF",
      doc.to_s,
      {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}} if  file_type.downcase == "pdf"

    #get the UUID that generated from previous action
    uuid = Nokogiri::XML.parse(response_put_report).xpath('//report/uuid').text  if file_type.blank? or file_type.downcase == "html"
    uuid_pdf = Nokogiri::XML.parse(response_put_report_pdf).xpath('//report/uuid').text if  file_type.downcase == "pdf"

    #download the report
    response_get_report =
      RestClient.get "#{report_server}/rest/report/#{uuid}?file=report",
      {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}  if file_type.blank? or file_type.downcase == "html"

    response_get_report_pdf =
      RestClient.get "#{report_server}/rest/report/#{uuid_pdf}?file=report",
      {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}} if  file_type.downcase == "pdf"

    if Rails.env != "production"
      secret_key = Setting.storage.s3_access_key_id
      access_key = Setting.storage.s3_secret_access_key
      bucket = Setting.storage.s3_bucket
    else
      secret_key = ENV["S3_KEY"]
      access_key = ENV["S3_SECRET"]
      bucket = ENV["S3_BUCKET"]
    end


    aw = AWS::S3.new(
      :access_key_id     =>secret_key ,
      :secret_access_key =>access_key
    )

    File.open("#{Rails.root}/tmp/test.html","w+"){|f| f << response_get_report.force_encoding("utf-8")} if  file_type.downcase == "html"
    File.open("#{Rails.root}/tmp/test.pdf","w+"){|f| f << response_get_report_pdf.force_encoding("utf-8")} if  file_type.downcase == "pdf"

    
    if  file_type.downcase == "html"
      key = "/report/#{chain_id}/#{start_date.to_date.to_s}_#{end_date.to_date.to_s}.html"
      aw.buckets[bucket].objects[key].write(:file =>  "tmp/test.html")
      File.delete("tmp/test.html")
    end

    if file_type.downcase == "pdf"
      key_pdf = "/report/#{chain_id}/#{start_date.to_date.to_s}_#{end_date.to_date.to_s}.pdf" if  file_type.downcase == "pdf"
      aw.buckets[bucket].objects[key_pdf].write(:file =>  "tmp/test.pdf")
      File.delete("tmp/test.pdf")
    end
    
    url = aw.buckets[bucket].objects[key].url_for(:read).to_s
    key = key_pdf if  file_type.downcase == "pdf"
    
    report = Report.find_or_create_by_start_date_and_end_date_and_chain_id(
      start_date.to_date,
      end_date.to_date,
      chain_id,
      key,
      url,
      kind
    )

    ChainMailer.report_owner(owner, report).deliver
  end
end