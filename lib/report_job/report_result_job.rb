class ReportJob::ReportResultJob < Struct.new(:report_result)
  def perform
    puts "---------------------- job report result executed ----------------------"
    report_result.sending_method_use
    puts "---------------------- job report result finished ----------------------"
  end


  def error(job, exception)
  end
end