class ReportJob::UserEmailLocationVisit < Struct.new(:chain, :email, :params)
  def perform
    #email = "inoe.bainur@gmail.com"
    # start_date = params[:start_date]
    # end_date = params[:end_date]

    #chain = Chain.find 22
    start_date = params[:start_date].blank? ? '2000-01-01'.to_datetime : params[:start_date].to_datetime
    end_date = params[:start_date].blank? ?  Date.today.to_datetime : params[:end_date].to_datetime

    total_count = User.where(:chain_id => chain).order('created_at desc').count
    total_pages = (total_count.to_f / 500.to_f).ceil
    total_pages = 1 if total_pages == 0
    path = "#{Rails.root}/tmp/user_email_location_#{chain.id}_#{Time.now}.csv"


    puts total_pages
    puts "aaaaaaaaaaaaaaaaa"
    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      csv << ["Users With Locations"]
      csv << ["User id", "Email", "Restaurant Name", "# Of Visits this location",
              "Most Recent Visit Date", "Sign up", "DOB"
      ]

      1.upto(total_pages) do |page|
        users = User.where(:chain_id => chain).
            order('id desc').paginate(:page => page, :per_page => 500)

        users.each do |user|
          #r = user.receipts.where("status = 3").order("created_at desc").first
          restaurant_users = RestaurantUser.where(:user_id => user.id).select('distinct on (restaurant_id) *') rescue nil
          dob = "#{user.dob_year}-#{user.dob_month}-#{user.dob_day}"
          count_visit = 0

          restaurant_users.each do |ru|
            sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id and t2.restaurant_id = #{ru.restaurant_id}
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where  c.status = 3
      and users.id = #{user.id}
      and c.created_at >= '#{start_date}'
      and c.created_at <= '#{end_date}'
      and t2.subtotal is not null
      order by r_created desc) a ) cc" rescue ""

            count_visit = ActivitySummary.find_by_sql(sql).first.count.to_i rescue 0

            if count_visit > 0
              csv << [
                  user.id,
                  user.email,
                  (Restaurant.find(ru.restaurant_id).name rescue nil),
                  count_visit,
                  (ru.last_active.utc.strftime('%D') rescue nil ),
                  user.created_at.in_time_zone('EST').strftime('%D'),
                  dob
              ]
            end
          end
        end
      end
    end

    OwnerMailer.send_reward_redeemed(nil, email, path, "Users With Locations.csv", "Users With Locations_#{chain.name}").deliver!
    File.delete(path)
    puts "Success EXECUTING report"
  end
end