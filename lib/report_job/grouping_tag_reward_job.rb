class ReportJob::GroupingTagRewardJob < Struct.new(:period1, :period2, :group1, :group2, :group3, :chain, :email)
  def perform
    # testing purpose
    # all_location = ["13", "31", "7"]
    # chain = Chain.find 19
    # email = "inoe.bainur@gmail.com"
    #
    # period1 = {:start_date => Time.zone.now - 300.days, :end_date => Time.now}
    # period2 = {}
    #all_location = [53]


    ## live testing purpose

    #     chain = Chain.find 22
    # # #   all_location = chain.restaurants.where("status IS TRUE")
    #     email = "inoe.bainur@gmail.com"
    #     period1 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    #     period2 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    #     group2 = [132,19]
    #     group1 = [132]
    #
    # # group2 = [] if group2.blank?
    #   group3 = [] if group3.blank?
    #
    all_location = chain

    file_name = "Loyalty Transactions #{chain.name}.csv"
    # if period1[:start_date].blank?
    #   start_date1 = Time.zone.now - 31.days
    #   end_date1 = Time.zone.now
    # else
    #   start_date1 = period1[:start_date]
    #   end_date1 = period1[:end_date]
    # end

    # unless period2[:start_date].blank?
    #   start_date2 = period2[:start_date]
    #   end_date2 = period2[:end_date]
    # end

    header = array_header(period1, period2)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << ["receipt transactions by group"]
      csv << ["Period 1 : #{period1[:start_date].to_date.strftime('%D')} TO #{period1[:end_date].to_date.strftime('%D')} "]
      csv << ["Period 2 : #{period2[:start_date].to_date.strftime('%D')} TO #{period2[:end_date].to_date.strftime('%D')} "] if !period2.blank? and !period2[:start_date].blank?
      csv << ["*"] + header
      total_all_receipt = generate_all_location_tag(group1, all_location, period1, period2)
      csv << total_all_receipt[0]#generate_all_location(chain, period1, period2)

      puts total_all_receipt[1].size
 #     group1_total = generate_all_location_tag(group1, nil, period1, period2)
      p total_all_receipt[1]
      total_all_receipt[0]
#      csv << group1_total[1]

      csv << [nil,nil,nil,nil,nil,nil,nil,nil]
      total_all_receipt[1].each do |x|
        csv << x
      end
      csv << [nil,nil,nil,nil,nil,nil,nil,nil]
      group2.each do |group2_single|
        x = ReportJob::GroupingTagRewardHelper.new(group2_single, period1, period2,chain)
        header_array, array_content =  x.generate_array
        csv << header_array
        array_content.each do |arr|
          csv << arr
        end
        csv << [nil,nil,nil,nil,nil,nil,nil,nil]
      end

      begin
      group3.each do |group3_single|
        x = ReportJob::GroupingTagRewardHelper.new(group3_single, period1, period2)
        header_array, array_content =  x.generate_array
        csv << header_array
        array_content.each do |arr|
          csv << arr
        end
        csv << [nil,nil,nil,nil,nil,nil,nil,nil]
      end
      rescue
        end
      # csv << generate_group2(group2, header, period1, period2)
      # csv << generate_group3(group3, header, period1, period2)
    end

    puts "EAAAAAAAAAAAAAAA"
    begin
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name, "Loyalty Transactions #{chain.name}").deliver!
    rescue
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name).deliver!
    end
    File.delete("tmp/#{file_name}.csv")
  end
  ### all grouping tag
  def generate_all_location_tag(group1, all_location, period1, period2)
    tags = find_tags(group1)
    all_total_receipt_period1 = 0
    all_total_receipt_period2 = 0
    all_total_mobile_pay1 = 0
    all_total_mobile_pay2 = 0
    all_total_spent1 = 0
    all_total_spent2 = 0
    all_total_online_order1 = 0
    all_total_online_order2 = 0

    csv_array = []
    tags.each do |t|
      receipt_period1, receipt_period2 = all_location_receipt_total_tag(t, all_location, period1, period2)
      total_mobile_period1, total_mobile_period2 = mobile_pay_total_tag(t, all_location, period1, period2)
      total_spent1, total_spent2 = total_spent_tag(t, all_location, period1, period2)
      total_online_order1, total_online_order2 = total_online_tag(t, all_location, period1, period2)

      csv_array << ([t.name, receipt_period1, receipt_period2] + [total_mobile_period1, total_mobile_period2] +
          [total_online_order1, total_online_order2] + [total_spent1, total_spent2]).compact

      all_total_receipt_period1 += receipt_period1.to_i rescue nil
      all_total_receipt_period2 += receipt_period2.to_i if receipt_period2.present? rescue nil
      all_total_receipt_period2 = nil if period2.blank?

      all_total_mobile_pay1 += total_mobile_period1.to_i rescue nil
      all_total_mobile_pay2 += total_mobile_period2.to_i if total_mobile_period2.present? rescue nil
      all_total_mobile_pay2 = nil if period2.blank?

      all_total_spent1 += total_spent1.to_f rescue nil
      all_total_spent2 += total_spent2.to_f if total_spent2.present? rescue nil
      all_total_spent2 = nil if period2.blank?

      all_total_online_order1 += total_online_order1.to_i rescue nil
      all_total_online_order2 += total_online_order2.to_i if total_spent2.present? rescue nil
      all_total_online_order2 = nil if period2.blank?

      puts "TOTAL SPENT = #{all_total_spent1}"
    end
    puts "TOTAL SPENT ! =#{all_total_spent1}"
    total_array = ["All Location", all_total_receipt_period1, all_total_receipt_period2,
                   all_total_mobile_pay1, all_total_mobile_pay2, all_total_online_order1, all_total_online_order2,
                   all_total_spent1,
                   all_total_spent2].compact
    return total_array.compact.flatten, csv_array.compact
  end

  def total_online_tag(tag, all_location, period1, period2)
    restaurants = find_restaurants(tag,all_location) #if all_location.blank?
    #restaurants = all_location unless all_location.blank?
    total_online_order1 = 0
    total_online_order2 = 0

    ## first period
    restaurants.each do |res|
      receipts = Receipt.select('distinct(receipts.id)').joins(:receipt_transactions).
          where("date(receipts.created_at - interval '5 hour') between date(?) and date(?) and receipts.status = 3  and restaurant_id = ? and is_online_order IS TRUE",
                period1[:start_date].to_date, period1[:end_date].to_date, res.id)

      receipts.each do |r|
        total_online_order1 = total_online_order1 + 1
      end
    end

    ## first period
    if !period2.blank? and !period2[:start_date].blank?
      restaurants.each do |res|
        receipts = Receipt.select('distinct(receipts.id)').joins(:receipt_transactions).
            where("date(receipts.created_at) between date(?) and date(?) and receipts.status = 3  and restaurant_id = ? and is_online_order IS TRUE",
                  period2[:start_date].to_date, period2[:end_date].to_date, res.id)

        receipts.each do |r|
          total_online_order2 = total_online_order2 + 1
        end
      end
    end

    ## second period


    total_online_order2 = nil if period2.blank?# and period2[:start_date].blank?

    return total_online_order1, total_online_order2
  end

  def all_location_receipt_total_tag(tag, all_location, period1, period2)
    # all location means all restaurant on this tags
    restaurants = find_restaurants(tag,all_location) #if all_location.blank?
    #restaurants = all_location unless all_location.blank?

    total1 = 0
    total2 = 0
    # period1
    restaurants.each do |res|
      #total1 += ActivitySummary.where("transaction_date between date(?) and date(?) and restaurant_id = ?", period1[:start_date].to_date, period1[:end_date].to_date, res.id).count
      sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id and t2.restaurant_id = #{res.id}
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where  date(c.created_at - interval '5 hour') >= date('#{period1[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period1[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null
      order by r_created desc) a ) cc"
      total1 += ActivitySummary.find_by_sql(sql).first.count.to_i
    end

    #period2
    if !period2.blank? and !period2[:start_date].blank?
      restaurants.each do |res|
        sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where t2.restaurant_id=#{res.id} and date(c.created_at - interval '5 hour') >= date('#{period2[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period2[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null
      order by r_created desc) a ) cc"
        total2 += ActivitySummary.find_by_sql(sql).first.count.to_i
        #total2 += ActivitySummary.where("transaction_date between date(?) and date(?) and restaurant_id = ?", period2[:start_date].to_date, period2[:end_date].to_date, res.id).count
      end
    end

    total2 = nil if period2.blank? #and period2[:start_date].blank?

    return total1, total2
  end

  def mobile_pay_total_tag(tag, all_location, period1, period2)
    count = 0
    restaurants = find_restaurants(tag,all_location) #if all_location.blank?
    #restaurants = all_location unless all_location.blank?
    total_mobile_pay1 = 0
    total_mobile_pay2 = 0

    #period1
    restaurants.each do |res|
     #  count = PaymentHistory.where("restaurant_id = ? and date(created_at - interval '5 hour') >= date(?)
     # and date(created_at - interval '5 hour') <= (?) ",
     #                               res.id, period1[:start_date].to_date, period1[:end_date].to_date).count
     #  total_mobile_pay1 += count
      sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where restaurants.id=#{res.id} and date(c.created_at - interval '5 hour') >= date('#{period1[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period1[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null and pc.payment_included IS TRUE
      order by r_created desc) a ) cc"
      total_mobile_pay1 += ActivitySummary.find_by_sql(sql).first.count.to_i
    end
    count = 0

    #period2
    if !period2.blank? and !period2[:start_date].blank?
      restaurants.each do |res|
     #    count = PaymentHistory.where("restaurant_id = ? and date(created_at - interval '5 hour') >= date(?)
     # and date(created_at - interval '5 hour') <= (?) ",
     #                                 res.id, period2[:start_date].to_date, period2[:end_date].to_date).count
     #    total_mobile_pay2 += count
        sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where restaurants.id=#{res.id} and date(c.created_at - interval '5 hour') >= date('#{period2[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period2[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null and pc.payment_included IS TRUE
      order by r_created desc) a ) cc"
        total_mobile_pay2 += ActivitySummary.find_by_sql(sql).first.count.to_i
      end
    end


    total_mobile_pay2 = nil if period2.blank? #and period2[:start_date].blank?

    return total_mobile_pay1, total_mobile_pay2
  end

  def total_spent_tag(tag, all_location, period1, period2)
    restaurants = find_restaurants(tag,all_location) #if all_location.blank?
    #restaurants = all_location unless all_location.blank?
    total_restaurant1 = 0
    total_restaurant2 = 0

    # period1
    restaurants.each do |res|
      receipts = Receipt.select('distinct(receipts.*)').joins(:receipt_transactions).
          where("date(receipts.created_at - interval '5 hour') between date(?) and date(?) and receipts.status = 3  and restaurant_id = ?",
                period1[:start_date].to_date, period1[:end_date].to_date, res.id)

      receipts.each do |r|
        total_restaurant1 = total_restaurant1.to_f.round(2) + r.last_transaction.subtotal.to_f.round(2)
      end
    end

    # period2
    if !period2.blank? and !period2[:start_date].blank?
      restaurants.each do |res|

        receipts = Receipt.select('distinct(receipts.*)').joins(:receipt_transactions).
            where("date(receipts.created_at - interval '5 hour') between date(?) and date(?) and receipts.status = 3  and restaurant_id = ?",
                  period2[:start_date].to_date, period2[:end_date].to_date, res.id)

        receipts.each do |r|
          total_restaurant2 = total_restaurant2.to_f.round(2) + r.last_transaction.subtotal.to_f.round(2)
        end
      end
    end

    total_restaurant2 = nil if period2.blank? #and period2[:start_date].blank?

    return total_restaurant1, total_restaurant2
  end

  def generate_group3(group3, header, period1, period2)

  end

  def find_tags(tag_ids)
    tags = Tag.where("id in (?)", tag_ids)
    return tags
  end

  def find_restaurants(tag_ids,chain_id = nil)
    restaurants = []
    tags = find_tags(tag_ids)
    tags.each { |t| restaurants << t.restaurants.where("restaurants.status  IS TRUE and chain_id = ?",chain_id) }
    return restaurants.uniq.flatten
  end

  def array_header(period1, period2)
    if period1[:start_date].blank?
      start_date1 = Time.zone.now - 31.days
      end_date1 = Time.zone.now
    else
      start_date1 = period1[:start_date]
      end_date1 = period1[:end_date]
    end

    if !period2.blank? and !period2[:start_date].blank?
      start_date2 = period2[:start_date]
      end_date2 = period2[:end_date]
    end

    unless period2.blank?
      period2_show =  !period2[:start_date].blank?
    else
      period2_show = false
    end

    title_period = "(#{start_date1.to_date.strftime('%D')}) - (#{end_date1.to_date.strftime('%D')})"
    title_period2 = "(#{start_date2.to_date.strftime('%D')}) - (#{end_date2.to_date.strftime('%D')})" if period2_show == true
    total_receipt2 = []
    mobile_pay2 = []
    online_order2 = []
    total_spent2 = []

    total_receipt2 = ["Total Loyalty Scans #{title_period2}"] if period2_show == true
    mobile_pay2 = ["Mobile Pay Transactions #{title_period2}"] if period2_show == true
    online_order2 = ["Online Orders #{title_period2}"] if period2_show == true
    total_spent2 = ["Total Spent #{title_period2}"] if period2_show == true

    arr = ["Total Loyalty Scans #{title_period}"] + total_receipt2

    arr = arr + ["Mobile Pay Transactions #{title_period}"] + mobile_pay2

    arr = arr + ["Online Orders #{title_period}"] + online_order2

    arr = arr + ["Total Spent #{title_period}"] + total_spent2
    return arr
  end

  # all location tag 2
  def generate_all_location(chain, period1, period2)
    receipt_period1, receipt_period2 = chain_receipt_total_tag(chain, period1, period2)
    total_mobile_period1, total_mobile_period2 = chain_mobile_pay_total(chain, period1, period2)
    total_spent1, total_spent2 = chain_total_spent(chain, period1, period2)
    total_online_order1, total_online_order2 = chain_total_online(chain, period1, period2)

    if period2.blank?
      total_array = ["All Location 1 group", receipt_period1,
                     total_mobile_period1, total_online_order1,
                     total_spent1].compact
    else
      total_array = ["All Location", receipt_period1, receipt_period2,
                     total_mobile_period1, total_mobile_period2, total_online_order1, total_online_order2,
                     total_spent1,
                     total_spent2].compact
    end


    return total_array
  end

  def chain_total_online(chain, period1, period2)
    total_online_order1 = 0
    total_online_order2 = 0

    ## first period
    total_online_order1 = Receipt.
          where("date(receipts.created_at) between date(?) and date(?) and receipts.status = 3  and chain_id = ? and is_online_order IS TRUE",
                period1[:start_date].to_date, period1[:end_date].to_date, chain.id).count


    ## second period
    if !period2.blank? and !period2[:start_date].blank?
        total_online_order2 = Receipt.
            where("date(receipts.created_at) between date(?) and date(?) and receipts.status = 3  and chain_id = ? and is_online_order IS TRUE",
                  period2[:start_date].to_date, period2[:end_date].to_date, chain.id).count

    end

    total_online_order2 = nil if period2.blank? #and period2[:start_date].blank?

    return total_online_order1, total_online_order2
  end

  def chain_total_spent(chain,period1, period2)
    total_restaurant1 = 0
    total_restaurant2 = 0
    # period1
      receipts = Receipt.
          where("date(receipts.created_at) between date(?) and date(?) and receipts.status = 3  and chain_id = ?",
                period1[:start_date].to_date, period1[:end_date].to_date, chain.id)

      receipts.each do |r|
        total_restaurant1 = total_restaurant1.to_f.round(2) + r.last_transaction.subtotal.to_f.round(2)
      end


    # period2
    if !period2.blank? and !period2[:start_date].blank?
      receipts = Receipt.
          where("date(receipts.created_at) between date(?) and date(?) and receipts.status = 3  and chain_id = ?",
                period2[:start_date].to_date, period2[:end_date].to_date, chain.id)

        receipts.each do |r|
          total_restaurant2 = total_restaurant2.to_f.round(2) + r.last_transaction.subtotal.to_f.round(2)
        end
      end

    total_restaurant2 = nil if period2.blank? #and period2[:start_date].blank?

    return total_restaurant1, total_restaurant2
  end

  def chain_receipt_total_tag(chain,period1, period2)
    # all location means all restaurant on this tags
    #restaurants = find_restaurants(tag) if all_location.blank?
    #restaurants = all_location unless all_location.blank?
    restaurants = chain.restaurants.where("restaurants.status IS TRUE")
    total1 = 0
    total2 = 0
    # period1
    #  total1 = ActivitySummary.where("transaction_date between date(?) and date(?) and chain_id = ? and restaurant_id in(?)", period1[:start_date].to_date, period1[:end_date].to_date, chain.id,restaurants).count
    sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where restaurants.chain_id=#{chain.id} and date(c.created_at - interval '5 hour') >= date('#{period1[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period1[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null
      order by r_created desc) a ) cc"
    total1 = ActivitySummary.find_by_sql(sql).first.count.to_i

    #period2
    if !period2.blank? and !period2[:start_date].blank?
     # total2 = ActivitySummary.where("transaction_date between date(?) and date(?) and chain_id = ? and restaurant_id in(?)",
     #                                period2[:start_date].to_date, period2[:end_date].to_date, chain.id,restaurants).count
    end

    total2 = nil if period2.blank? #and period2[:start_date].blank?

    return total1, total2
  end

  def chain_mobile_pay_total(chain, period1, period2)
    total_mobile_pay1 = 0
    total_mobile_pay2 = 0
    count = 0

    #period1
    total_mobile_pay1 = PaymentHistory.where("chain_id = ? and date(created_at) between date(?) and (?) ",
                                   chain.id, period1[:start_date].to_date, period1[:end_date].to_date).count


    #period2
    if !period2.blank? and !period2[:start_date].blank?
        count = PaymentHistory.where("chain_id = ? and date(created_at) between date(?) and (?) ",
                                     chain.id, period2[:start_date].to_date, period2[:end_date].to_date).count
        total_mobile_pay2 = count
    end


    total_mobile_pay2 = nil if period2.blank?# and period2[:start_date].blank?

    return total_mobile_pay1, total_mobile_pay2
  end
end