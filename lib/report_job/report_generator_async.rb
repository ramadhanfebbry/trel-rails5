module ReportJob
  module ReportGeneratorAsync

    def self.generate_report(chain_id,report_definition,report_subscription)
      ### variable definition
      custom_params = report_subscription.custom_param_hash
      report_kind = report_subscription.report_kind
      report_settings = report_subscription.report_setting_hash
      owners = report_subscription.owners
      file_extension = ReportSubscription::FILE_TYPES.key(report_subscription.file_type.to_i).downcase

      start_date = Time.zone.now
      #### end variable ###

      report_type,last_date =
          case report_subscription.report_kind
            when 1
              ["Day", start_date - 1.days]
            when 2
              ["Week", start_date  - 7.days]
            when 3
              ["Month", start_date  - 30.days]
          end

      ##login to JasperServer and acquire cookies for its session
      puts "--------------------------authenticating-----------------"
      cookies = report_subscription.authenticate_jasper
      puts cookies
      puts "--------------------------done authenticating-----------------"
      ##get resourceDescriptor before generate the report

      puts "\n\n----------------response resource-----------------"
      response_resource  = report_subscription.get_response_resource(cookies)
      puts "\n\n----------------Done ..response resource-----------------"
      ### parsing the xml and then add the parameters
      doc = Nokogiri::XML.parse(response_resource)

      custom_params.each do |key,value|
        instance_variable_set("@#{key}",  Nokogiri::XML::Node.new("parameter", doc))
        puts "key ------------------------#{key}"
        puts "\n\n\n---------- variables ---------------"

        puts instance_variable_get("@#{key}")

        puts "\n\n\n---------- END variables ---------------"

        instance_variable_get("@#{key}")['name'] = key
        values = set_default_value(key, value, report_type)

        puts "values ==================== #{values}"

        instance_variable_get("@#{key}").content = values

        puts "End values ==================== #{values}"
        doc.children.first.add_child(instance_variable_get("@#{key}"))
      end

      ## get the UUID for the report
      puts " \n\n------------------------get put response---------------"
      uuid = report_subscription.get_put_response_resource_uuid(doc,cookies)

      puts " \n\n------------------------done get put response---------------"


      puts "-----------------------------------------------------------------"
      puts "I GET THE UUID"
      puts "#{uuid}"

      puts "-----------------------------------------------------------------"

      puts "\n\n\n get the strings ------------------------------------"

      response_get_report = report_subscription.get_report_sync(uuid, cookies)
      report_file_name = "report_#{chain_id}_#{Date.today.to_s}.#{file_extension}"

      File.open("#{Rails.root}/tmp/#{report_file_name}","w+"){|f| f << response_get_report.force_encoding("utf-8")}

      #set the key
      key = "/report/#{chain_id}/#{Date.today.to_s}/#{report_file_name}"

      ### stored to aws storage
      report_subscription.stored_in_aws("tmp/#{report_file_name}",key)

      ### stored on report model
      puts "\n\n stored to report ---------------"
      report_subscription.stored_to_report(chain_id,key,ReportSubscription::TYPES.key(report_subscription.report_kind))
      puts "------------------- end stored report ------------------------"
    end

    def self.set_default_value(key, value, report_kind)
      if value.blank?
        start_date = Time.zone.now
        if key.to_s.downcase == "start_date"
          return Time.zone.now.to_datetime.to_i * 1000
        elsif key.to_s.downcase == "end_date"
          case report_kind
            when "Day"
              return start_date.to_datetime.to_i * 1000
            when "Week"
              return (start_date - 7.days).to_datetime.to_i * 1000
            when "Month"
              return (start_date - 30.days).to_datetime.to_i * 1000
          end
        elsif key.to_s.downcase == "kind"
          return start_date.to_datetime.to_i * 1000
        end
      else
        return value
      end
    end
  end
end