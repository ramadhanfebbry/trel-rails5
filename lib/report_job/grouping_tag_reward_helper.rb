class ReportJob::GroupingTagRewardHelper < Struct.new(:tag_ids, :period1, :period2, :chain_id)

  def generate_array
    ## live testing purpose
    # tag_ids = [132]
    # chain = Chain.find 13
    # email = "inoe.bainur@gmail.com"
    # period1 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    # period2 = {:start_date => Time.zone.now - 2.days, :end_date => Time.now}

    tags = find_tags([tag_ids])
    header_tag = []
    a = []
    b = []
    tags.each do |tag|
      restaurants = find_restaurants(tag, chain_id)
      all_total_receipt1 = 0
      all_total_receipt2 = 0

      all_mobile_pay1=0
      all_mobile_pay2=0

      all_online_total1=0
      all_online_total2=0

      all_total_spent1=0
      all_total_spent2=0

      restaurants.each do |res|


        receipt_total1, receipt_total2 = restaurant_location_receipt_total(res, period1, period2)
        mobile_pay1, mobile_pay2 = restaurant_mobile_pay_total(res, period1, period2)
        online_total1, online_total2 = restaurant_online_order_count(res, period1, period2)

        spent1, spent2 = restaurant_total_spent(res, period1, period2)

        # row
        a << ([res.name, receipt_total1, receipt_total2] +
            [mobile_pay1, mobile_pay2] +
            [online_total1, online_total2] +
            [spent1, spent2]).compact


        ## totalling on the header
        all_total_receipt1 += receipt_total1.to_i rescue nil
        begin
          all_total_receipt2 += receipt_total2.to_i if receipt_total2.present? rescue nil
          all_total_receipt2 = nil if period2.blank?
        rescue
          all_total_receipt2 = nil
        end


        all_mobile_pay1 += mobile_pay1.to_i rescue nil
        begin
          all_mobile_pay2 += mobile_pay2.to_i if mobile_pay2.present?
          all_mobile_pay2 = nil if period2.blank?
        rescue
          all_mobile_pay2 = nil
        end

        all_online_total1 += online_total1.to_i rescue nil
        begin
          all_online_total2 += online_total2.to_i #rescue nil
          all_online_total2 = nil if period2.blank?
        rescue
          all_online_total2 = nil if period2.blank?
        end

        all_total_spent1 += spent1.to_f rescue nil
        begin
          all_total_spent2 += spent2.to_f rescue nil
          all_total_spent2 = nil if period2.blank?
        rescue
          all_total_spent2 = nil
        end

        b = ([tag.name, all_total_receipt1, all_total_receipt2] +
            [all_mobile_pay1, all_mobile_pay2] +
            [all_online_total1, all_online_total2] +
            [all_total_spent1, all_total_spent2]).compact
      end
    end
    puts "eaaaaaaaa"
    p a
    return [b, a.compact]
  end

  def find_restaurants(tag_ids, chain_id = nil)
    restaurants = []
    tags = find_tags(tag_ids)
    tags.each { |t| restaurants << t.restaurants.where("restaurants.status IS TRUE AND chain_id = ?", chain_id) }
    return restaurants.uniq.flatten
  end

  def find_tags(tag_ids)
    tags = Tag.where("id in (?)", tag_ids)
    return tags
  end

  def restaurant_location_receipt_total(res, period1, period2)
    total1 = 0
    total2 = 0
    # period1
    #total1 = ActivitySummary.where("transaction_date between date(?) and date(?) and restaurant_id = ?", period1[:start_date].to_date, period1[:end_date].to_date, res.id).count
    sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where restaurants.id=#{res.id} and date(c.created_at - interval '5 hour') >= date('#{period1[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period1[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null
      order by r_created desc) a ) cc"
    total1 = ActivitySummary.find_by_sql(sql).first.count.to_i

    #period2
    if !period2.blank? and !period2[:start_date].blank?
      #total2 = ActivitySummary.where("transaction_date between date(?) and date(?) and restaurant_id = ?", period2[:start_date].to_date, period2[:end_date].to_date, res.id).count
      sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where restaurants.id=#{res.id} and date(c.created_at - interval '5 hour') >= date('#{period2[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period2[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null
      order by r_created desc) a ) cc"
      total2 = ActivitySummary.find_by_sql(sql).first.count.to_i
    end

    total2 = nil if period2.blank? #and period2[:start_date].blank?

    return total1, total2
  end

  def restaurant_online_order_count(single_res, period1, period2)
    total_online_order1 = 0
    total_online_order2 = 0
    restaurants = [single_res]
    ## first period
    restaurants.each do |res|
      receipts = Receipt.select('distinct(receipts.*)').joins(:receipt_transactions).
          where("date(receipts.created_at) between date(?) and date(?) and receipts.status = 3  and restaurant_id = ? and is_online_order IS TRUE",
                period1[:start_date].to_date, period1[:end_date].to_date, res.id)

      receipts.each do |r|
        total_online_order1 = total_online_order1 + 1
      end
    end

    ## first period
    if !period2.blank? and !period2[:start_date].blank?
      restaurants.each do |res|
        receipts = Receipt.select('distinct(receipts.*)').joins(:receipt_transactions).
            where("date(receipts.created_at) between date(?) and date(?) and receipts.status = 3  and restaurant_id = ? and is_online_order IS TRUE",
                  period2[:start_date].to_date, period2[:end_date].to_date, res.id)

        receipts.each do |r|
          total_online_order2 = total_online_order2 + 1
        end
      end
    end

    ## second period


    total_online_order2 = nil if period2.blank? #and period2[:start_date].blank?

    return total_online_order1, total_online_order2
  end

  def restaurant_mobile_pay_total(single_res, period1, period2)
    total_mobile_pay1 = 0
    total_mobile_pay2 = 0
    restaurants = [single_res]
    count = 0
    #period1
    restaurants.each do |res|
      # count = PaymentHistory.where("restaurant_id = ? and date(created_at) between date(?) and (?) ",
      #                              res.id, period1[:start_date].to_date, period1[:end_date].to_date).count
      # total_mobile_pay1 += count
      sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where restaurants.id=#{res.id} and date(c.created_at - interval '5 hour') >= date('#{period1[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period1[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null and pc.payment_included IS TRUE
      order by r_created desc) a ) cc"
      total_mobile_pay1 += ActivitySummary.find_by_sql(sql).first.count.to_i
    end

    count = 0
    #period2
    if !period2.blank? and !period2[:start_date].blank?
      restaurants.each do |res|
        # count = PaymentHistory.where("restaurant_id = ? and date(created_at) between date(?) and (?) ",
        #                              res.id, period2[:start_date].to_date, period2[:end_date].to_date).count
        # total_mobile_pay2 += count
        sql = "select count(*) from (
                                 select distinct on(user_id,receipt_number) a.* from (
                                                                                         select coalesce(t2.time_stamp, to_char(c.created_at, 'HH12:MI AM')) as time_a ,trim(both E'\t' from restaurants.name)  as name,users.id as user_id, coalesce(t2.issue_date - interval '5 hour',c.created_at - interval '5 hour')  as r_created, users.email, t2.subtotal, t2.receipt_number, c.is_online_order as olo, pc.payment_included from receipts c
      left join receipt_transactions t1 on
      t1.receipt_id = c.id left join
      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
      inner join users on c.user_id = users.id
      inner join restaurants on t2.restaurant_id = restaurants.id
      left join pos_check_uploads pc on c.pos_check_upload_id = pc.id
      where restaurants.id=#{res.id}  and date(c.created_at - interval '5 hour') >= date('#{period2[:start_date].to_date}')
      and date(t2.created_at - interval '5 hour') <= date('#{period2[:end_date].to_date}') and c.status = 3
      and t2.subtotal is not null and pc.payment_included IS TRUE
      order by r_created desc) a ) cc"
        total_mobile_pay2 += ActivitySummary.find_by_sql(sql).first.count.to_i
      end
    end


    total_mobile_pay2 = nil if period2.blank? #and period2[:start_date].blank?

    return total_mobile_pay1, total_mobile_pay2
  end

  def restaurant_total_spent(single_res, period1, period2)
    restaurants = [single_res]
    total_restaurant1 = 0
    total_restaurant2 = 0

    # period1
    restaurants.each do |res|
      receipts = Receipt.select('distinct(receipts.*)').joins(:receipt_transactions).
          where("date(receipts.created_at - interval '5 hour') between date(?) and date(?) and receipts.status = 3  and restaurant_id = ?",
                period1[:start_date].to_date, period1[:end_date].to_date, res.id)

      receipts.each do |r|
        total_restaurant1 = total_restaurant1.to_f.round(2) + r.last_transaction.subtotal.to_f.round(2)
      end
    end

    # period2
    if !period2.blank? and !period2[:start_date].blank?
      restaurants.each do |res|

        receipts = Receipt.select('distinct(receipts.*)').joins(:receipt_transactions).
            where("date(receipts.created_at - interval '5 hour') between date(?) and date(?) and receipts.status = 3  and restaurant_id = ?",
                  period2[:start_date].to_date, period2[:end_date].to_date, res.id)

        receipts.each do |r|
          total_restaurant2 = total_restaurant2.to_f.round(2) + r.last_transaction.subtotal.to_f.round(2)
        end
      end
    end

    total_restaurant2 = nil if period2.blank? #and period2[:start_date].blank?

    return total_restaurant1, total_restaurant2
  end
end