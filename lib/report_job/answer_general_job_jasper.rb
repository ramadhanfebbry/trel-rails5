class ReportJob::AnswerGeneralJobJasper < Struct.new(:survey_id, :owner_emails, :params, :filename, :subscription_id)
  def perform
    require 'net/ftp'
    puts "beginning AnswerGeneralJobJasper"
    puts owner_emails
    dt_zone_created_at = "date(TIMEZONE('UTC', created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
    file_name = "answer_general_exports"
    if params[:start_date].blank?
      start_date = Time.zone.now - 31.days
      end_date = Time.zone.now
    else
      start_date = params[:start_date]
      end_date = params[:end_date]
    end

    survey = Survey.find(survey_id)
    if start_date.blank? || end_date.blank?
      puts "all survey id"
      #surveys = SurveysUser.where("survey_id = ?", survey_id)
      total_pages = SurveysUser.where("survey_id = ?", survey_id).paginate(:page => 1, :per_page => 500).total_pages
      file_name = "survey_#{survey.title.gsub(' ', '_')}_answer_exports.csv"
    else
      puts "else all survey id"
      #surveys = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?", survey_id, start_date, end_date)
      total_pages = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?", survey_id, start_date, end_date).paginate(:page => 1, :per_page => 500).total_pages
      file_name = "survey_#{survey.title.gsub(' ', '_')}_#{start_date.to_date.strftime('%y%m%d')}_#{end_date.to_date.strftime('%y%m%d')}.csv"
    end

    survey_questions = survey.all_questions_collection(nil,nil)
    questions = survey_questions.map(&:text)
    question_ids = survey_questions.map(&:id)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << [
          "Receipt Id",
          "Date",
          "Time",
          "Location",
          "Member", "Avg"] + questions

      1.upto(total_pages).each do |i|
        if start_date.blank? || end_date.blank?
          surveys = SurveysUser.where("survey_id = ?", survey_id).paginate(:page => i, :per_page => 500)
        else
          surveys = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?", survey_id, start_date, end_date).paginate(:page => i, :per_page => 500)
        end

        surveys.each do |answer_survey|
          answers_tmp = answer_survey.answers.where("question_id in (?)", question_ids)

          csv << [
              answer_survey.receipt_id,
              answer_survey.created_at.strftime('%Y-%m-%d'),
              answer_survey.created_at.strftime('%H:%M %Z'),
              (answer_survey.restaurant.dashboard_display_text rescue nil),
              (answer_survey.user.email rescue nil),
              (answer_survey.scores.round(2) rescue nil)
          ] + slice_answers(answers_tmp, question_ids)
        end
      end
    end
    owner_emails.each do |owner_email|
      puts "sending to === #{owner_email}"
      OwnerMailer.send_survey_dashboard(survey.chain, owner_email, "tmp/#{file_name}.csv", filename).deliver!
    end

    rs = ReportSubscription.find subscription_id

    if !rs.blank? and rs.sending_method == 4
      return if rs.blank?
      return if rs.ftp_host.blank?
      return if rs.ftp_username.blank?
      return if rs.ftp_password.blank?
      return if rs.ftp_upload_path.blank?
      #s3_file = write_to_file
      @retry = 1 # retry 3 time if there was time out error
      #begin
      ftp = Net::FTP.new(rs.ftp_host)
      ftp.passive = true
      ftp.login(rs.ftp_username, rs.ftp_password)
      ftp.chdir(rs.ftp_upload_path)
      ftp.storbinary("STOR #{file_name}.csv", open("tmp/#{file_name}.csv", 'rb'), 1024)
      puts "put file"
      puts "deleting file "
      File.delete("tmp/#{file_name}.csv")
    else
      File.delete("tmp/#{file_name}.csv")
    end
  end

  #def slice_answers(answers, question_ids)
  #  result = []
  #  answers_question_ids = answers.map(&:question_id)
  #  question_ids.each do |q_id|
  #    if answers_question_ids.include?(q_id)
  #      index = answers_question_ids.index(q_id)
  #      val = answers[index].value rescue nil
  #      if is_number?(val)
  #        puts "#{val.to_i}"
  #        puts "#{Question.find(q_id).question_type}"
  #        puts "------------------"
  #        if val.to_i == 0 and Question.find(q_id).question_type == 3 # if dropdown
  #          puts "null"
  #          result << "null"
  #        else
  #          puts "above #{q_id}"
  #          result << val
  #        end
  #      else
  #        puts "#{q_id}"
  #        result << val
  #      end
  #    else
  #      result << "null"
  #    end
  #  end
  #  return result
  #end

  #def slice_answers(answers_tmp, question_ids)
  #  result = []
  #  answers_question_ids = answers_tmp.map(&:question_id)
  #
  #  ## if there no duplicate
  #  if answers_question_ids.detect { |e| answers_question_ids.count(e) > 1 }.blank? # select if there are duplicate questoin_id
  #    question_ids.each do |q_id|
  #      if answers_question_ids.include?(q_id)
  #        index = answers_question_ids.index(q_id)
  #        val = answers_tmp[index].value #rescue nil
  #        result << val
  #      else
  #        result << ""
  #      end
  #    end
  #    return result
  #  else
  #    ## if there are duplicate / multiple answers for 1 question
  #    question_ids.each do |q_id|
  #      multiple =  answers_tmp.select{|x| x if x.question_id.to_i == q_id}
  #      if multiple.size > 1
  #        tmp = []
  #        multiple.each_with_index do |answ_collection,i|
  #
  #          value =  answ_collection.value
  #
  #          if value.to_s.include?("null")
  #            tmp << ""
  #          else
  #            tmp << "Answer #{i+1}: "+value.to_s
  #          end
  #        end
  #        result << tmp.join(' - ')
  #      else
  #        result << multiple.first.value rescue nil
  #      end
  #    end
  #    return result
  #
  #  end
  #end

  def slice_answers(answers_tmp, question_ids)
    result = []
    answers_question_ids = answers_tmp.map(&:question_id)

    ## if there no duplicate
    if answers_question_ids.detect { |e| answers_question_ids.count(e) > 1 }.blank? # select if there are duplicate questoin_id
      question_ids.each do |q_id|
        if answers_question_ids.include?(q_id)
          index = answers_question_ids.index(q_id)
          val = answers_tmp[index].value rescue " " #rescue nil
          val = " " if val == ""
          result << val
        else
          result << " "
        end
      end
      return result
    else
      ## if there are duplicate / multiple answers for 1 question
      question_ids.each do |q_id|
        multiple =  answers_tmp.select{|x| x if x.question_id.to_i == q_id}
        if multiple.size > 1
          tmp = []
          multiple.each_with_index do |answ_collection,i|

            value =  answ_collection.value

            if value.to_s.include?("null")
              tmp << ""
            else
              #tmp << "Answer #{i+1}: "+value.to_s
              tmp << value.to_s
            end
          end
          result << tmp.join(' and ')
        else
          #result << multiple.first.value rescue nil
          if answers_question_ids.include?(q_id)
            index = answers_question_ids.index(q_id)
            val = answers_tmp[index].value rescue " " #rescue nil
            val = " " if val == ""
            result << val
          else
            result << " "
          end
        end
      end
      return result
    end
  end


  def answer_text_for(answer)
    case answer.answer_type
      when ::Answer::TYPES['Comments']
        a = ""
        begin
          a = AnswerText.find(answer.value_id).try(:text) #rescue ""
          a = "" if a.downcase.include?('null')
        rescue
          a
        end
        a
      when ::Answer::TYPES['Multiple Choice'], ::Answer::TYPES['Slider']
        value_type = Question.find(answer.question_id).label_or_value rescue "value"
        QuestionChoice.find(answer.value_id).try(value_type.to_sym) rescue ''
      when ::Answer::TYPES['Dropdown']
        value_type = Question.find(answer.question_id).label_or_value rescue "value"
        QuestionChoice.find(answer.value_id).try(value_type.to_sym) rescue ''
      when ::Answer::TYPES["Multiple Dropdown"]
        value_type = Question.find(self.question_id).label_or_value rescue "value"
        QuestionChoice.find(self.value_id).try(value_type.to_sym) rescue ""
      else
        raise 'unknown question/answer type'
    end
  end

  def is_number?(var)
    true if Float(var) rescue false
  end
end
