class ReportJob::GroupingTagRedemptionJob < Struct.new(:period1, :period2, :group1, :group2, :group3, :chain, :email, :reward_ids)
  def perform
    ## live testing purpose

    # chain = Chain.find 22
    # reward_ids = chain.rewards.map(&:id)#[469,366,364,362,359,352]
    # # # #   all_location = chain.restaurants.where("status IS TRUE")
    # email = "inoe.bainur@gmail.com"
    # period1 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    # period2 = nil
    # period2 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    # group2 = [132]
    # group1 = [132, 19]

    all_location = chain
    #reward_ids = [365, 357]
    file_name = "Reward Transaction Tag #{chain.name}.csv"

    header = array_header(chain, period1, period2, reward_ids)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << ["Reward Redemption"]
      csv << ["Period 1 : #{period1[:start_date].to_date.strftime('%D')} TO #{period1[:end_date].to_date.strftime('%D')} "]
      csv << ["Period 2 : #{period2[:start_date].to_date.strftime('%D')} TO #{period2[:end_date].to_date.strftime('%D')} "] if !period2.blank? and !period2[:start_date].blank?
      csv << ["*"]
      csv << ['Reward Name :'] + header
      csv << ["*"]
      total_all_receipt = generate_all_location_tag(header, group1, all_location, period1, period2, reward_ids)
      csv << total_all_receipt[0]
      csv << ["*"]
      p total_all_receipt[1]
      total_all_receipt[1].each do |m|
        csv << m
      end
      csv << [nil, nil, nil, nil, nil, nil, nil, nil,nil, nil, nil, nil, nil, nil, nil, nil,nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
      group2.each do |group2_single|
        restaurants = find_restaurants(group2_single, all_location)
        arr = generate_csv_reward_tag(group2_single,restaurants,reward_ids, period1, period2)

        
        csv << arr[0]
        arr[1].each do |x|
          csv << x
        end
        csv << [nil, nil, nil, nil, nil, nil, nil, nil,nil, nil, nil, nil, nil, nil, nil, nil,nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
      end

    end

    puts "EAAAAAAAAAAAAAAA"
    begin
      OwnerMailer.send_survey_dashboard_encoding(chain, email, "tmp/#{file_name}.csv", file_name, "Reward Transaction Tag #{chain.name}").deliver!
    rescue
      OwnerMailer.send_survey_dashboard_encoding(chain, email, "tmp/#{file_name}.csv", file_name).deliver!
    end
    File.delete("tmp/#{file_name}.csv")
  end

  def generate_csv_reward_tag(group2_single,restaurants,reward_ids, period1,period2)
    # group2 = [132,19]
    # chain = Chain.find 22
    # restaurants = find_restaurants(group2[0], chain)
    # period1 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    # period2 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    #
    # reward_ids = [365, 357]
    tags = find_tags([group2_single])
    csv_array = []
    total1 = 0
    total2 = 0
    restaurants.each do |r|
      name = "#{r.name}"
      arr_tmp = []
      arr_tmp << name
      reward_ids.each do |reward_id|
        #reward_id = head.scan(/\[(.*)\]/)[0][0].to_i
        rw = Reward.find(reward_id)
        count_period1, count_period2 = calculate_reward_restaurant(r, rw, period1, period2)
        arr_tmp << count_period1
        arr_tmp << count_period2 unless count_period2.blank?
      end
      csv_array << arr_tmp
    end
    p csv_array
    p csv_array.size
    total_array =  csv_array.compact.transpose.map { |x| x.reduce(:+) rescue 0}
    total_array[0] = tags.first.name  rescue "-"
    return total_array, csv_array
  end

  def calculate_reward_restaurant(res, reward, period1, period2)
    # period1
    pos_used = RewardTransaction.where(:reward_id => reward.id).last.pos_used rescue false
    if pos_used == true
      pos_query = " AND pos_used is true"
    end

    sql = "select count(*) from (SELECT
             distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
             reward_transactions.restaurant_id,
             reward_transactions.user_id) reward_transactions.*
             FROM reward_transactions WHERE reward_transactions.deleted_at IS NULL
             AND redeeming IS FALSE AND restaurant_id = #{res.id} and date(created_at) >= '#{period1[:start_date].to_date}'
             AND date(created_at) <= '#{period1[:end_date].to_date}' and reward_id = #{reward.id} #{pos_query}
) a
      "
    total1 = ActivitySummary.find_by_sql(sql).first.count.to_i

  if !period2.blank? and !period2[:start_date].blank?
      sql = "select count(*) from (SELECT
             distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
             reward_transactions.restaurant_id,
             reward_transactions.user_id) reward_transactions.*
             FROM reward_transactions WHERE reward_transactions.deleted_at IS NULL
             AND redeeming IS FALSE AND restaurant_id = #{res.id} and date(created_at) >= '#{period2[:start_date].to_date}'
             AND date(created_at) <= '#{period2[:end_date].to_date}'
  and reward_id = #{reward.id} #{pos_query}) a
      "
      total2 = ActivitySummary.find_by_sql(sql).first.count.to_i
  end
  return total1, total2
end

  def chain_rewards(chain)
    chain.rewards.order('rewards.id desc')
  end

  def calculate_reward_period(tag, reward, period1, period2)
    all_location = reward.chain
    # all location means all restaurant on this tags
    restaurants = find_restaurants(tag, all_location) #if all_location.blank?
    #restaurants = all_location unless all_location.blank?
    pos_used = RewardTransaction.where(:reward_id => reward.id).last.pos_used rescue false
    if pos_used == true
      pos_query = " AND pos_used is true"
    end

    total1 = 0
    total2 = 0
    # period1
    restaurants.each do |res|
      sql = "select count(*) from (SELECT
             distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
             reward_transactions.restaurant_id,
             reward_transactions.user_id) reward_transactions.*
             FROM reward_transactions WHERE reward_transactions.deleted_at IS NULL
             AND redeeming IS FALSE AND restaurant_id = #{res.id} and date(created_at) >= '#{period1[:start_date].to_date}'
             AND date(created_at) <= '#{period1[:end_date].to_date}' and reward_id = #{reward.id} #{pos_query}
) a
      "
      total1 += ActivitySummary.find_by_sql(sql).first.count.to_i
    end

    #period2
    if !period2.blank? and !period2[:start_date].blank?
      restaurants.each do |res|
        sql = "select count(*) from (SELECT
             distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
             reward_transactions.restaurant_id,
             reward_transactions.user_id) reward_transactions.*
             FROM reward_transactions WHERE reward_transactions.deleted_at IS NULL
             AND redeeming IS FALSE AND restaurant_id = #{res.id} and date(created_at) >= '#{period2[:start_date].to_date}'
             AND date(created_at) <= '#{period2[:end_date].to_date}'
  and reward_id = #{reward.id} #{pos_query}) a
      "
        total2 += ActivitySummary.find_by_sql(sql).first.count.to_i
      end
    end

    total2 = nil if period2.blank? #and period2[:start_date].blank?

    return total1, total2
  end

  ### all grouping tag
  def generate_all_location_tag(headers, group1, all_location, period1, period2, reward_ids)
    tags = find_tags(group1)

    csv_array = []
    total_array = []

    tags.each do |t|
      name = "#{t.name}"
      arr_tmp = []
      arr_tmp << name
      reward_ids.each do |reward_id|
        #reward_id = head.scan(/\[(.*)\]/)[0][0].to_i
        rw = Reward.find(reward_id)
        count_period1, count_period2 = calculate_reward_period(t, rw, period1, period2)
        arr_tmp << count_period1
        arr_tmp << count_period2 unless count_period2.blank?
      end
      csv_array << arr_tmp
    end
    #calculate total
    total_array = csv_array.compact.transpose.map { |x| x.reduce(:+) rescue 0}
    total_array[0] = "ALL Location"

    return total_array, csv_array.compact
  end

  def find_tags(tag_ids)
    tags = Tag.where("id in (?)", tag_ids)
    return tags
  end

  def find_restaurants(tag_ids, chain_id = nil)
    restaurants = []
    tags = find_tags(tag_ids)
    tags.each { |t| restaurants << t.restaurants.where("restaurants.status  IS TRUE and chain_id = ?", chain_id) }
    return restaurants.uniq.flatten
  end

  def array_header(chain, period1, period2, reward_ids)
    # chain = Chain.find(22)
    # period1 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    # period2 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}

    if period1[:start_date].blank?
      start_date1 = Time.zone.now - 31.days
      end_date1 = Time.zone.now
    else
      start_date1 = period1[:start_date]
      end_date1 = period1[:end_date]
    end

    if !period2.blank? and !period2[:start_date].blank?
      start_date2 = period2[:start_date]
      end_date2 = period2[:end_date]
    end
    period2_show = !period2.blank?
    title_period = "(#{start_date1.to_date.strftime('%D')}) - (#{end_date1.to_date.strftime('%D')})"
    title_period2 = "(#{start_date2.to_date.strftime('%D')}) - (#{end_date2.to_date.strftime('%D')})" if period2_show == true

    arr = []
    #chain.rewards.where("id in(?)", reward_ids).order("rewards.id desc").each do |reward|
    reward_ids.each do |r|
      reward = Reward.find(r)
      arr << "#{reward.name} [#{reward.id}] #{title_period}"
      arr << "#{reward.name} [#{reward.id}] #{title_period2}" if period2_show == true
    end
    p arr
    return arr
  end
end