class ReportJob::GroupingLapsedUser < Struct.new(:period1, :period2, :group1, :group2, :group3, :chain, :email)
  def perform
    ## live testing purpose

    #  chain = Chain.find 22
    # # reward_ids = chain.rewards.map(&:id)#[469,366,364,362,359,352]
    # # # # #   all_location = chain.restaurants.where("status IS TRUE")
    #  email = "inoe.bainur@gmail.com"
    # # period1 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    # # period2 = nil
    # # #period2 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
      period1 = nil
      period2=  nil
    #  group2 = [132]
    #  group1 = [132, 19]

    all_location = chain
    #reward_ids = [365, 357]
    file_name = "Lapsed User #{chain.name}.csv"

    #header = array_header(chain, period1, period2, reward_ids)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << ["Lapsed User"]
      csv << ['*','Have not Visited in 60 days', 'Have not visited in more than 60 days', '%Lapsed', 'Total associated users']
      lapsed = generate_all_location_tag(group1, all_location, period1, period2)
      csv << lapsed[0]

      p lapsed[1]
      lapsed[1].each do |m|
        csv << m
      end
        csv << [nil, nil, nil, nil, nil, nil, nil, nil]
       group2.each do |group2_single|
        arr = generate_single_tag_lapsed(group2_single, all_location)

        csv << arr[0]
        arr[1].each do |x|
          csv << x
        end
        csv << [nil, nil, nil, nil, nil, nil, nil, nil]
      end

    end

    puts "EAAAAAAAAAAAAAAA"
    begin
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name, "Lapsed User #{chain.name}").deliver!
    rescue
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name).deliver!
    end
    File.delete("tmp/#{file_name}.csv")
  end

  def generate_single_tag_lapsed(group2_single, all_location)
    restaurants = find_restaurants(group2_single, all_location)

    today_time = Time.zone.now.in_time_zone('EST')
    csv_array = []
    total_array = []

    #### sql initialize
    condition_30day = "date(last_active) <= date('#{today_time - 30.days}') AND date(last_active) >= date('#{today_time - 60.days}') AND
                        (date(last_redeem) <= date('#{today_time - 30.days}') OR last_redeem IS NULL)"

    condition_60day = "date(last_active) <= date('#{today_time - 60.days}')  AND
                       ( date(last_redeem) <= date('#{today_time - 60.days}') OR last_redeem IS NULL)"

    condition_120day = "date(last_active) <= date('#{today_time - 120.days}') AND
                        (date(last_redeem) <= date('#{today_time - 120.days}') OR last_redeem IS NULL)"

    restaurants.each do |res|
      restaurant_id_sql = res.id


      sql30 = build_query(condition_30day, all_location, restaurant_id_sql)
      total30 = ActivitySummary.find_by_sql(sql30).first.count.to_i

      sql60 = build_query(condition_60day, all_location, restaurant_id_sql)
      total60 = ActivitySummary.find_by_sql(sql60).first.count.to_i

      # sql120 = build_query(condition_120day, all_location, restaurant_id_sql)
      # total120 = ActivitySummary.find_by_sql(sql120).first.count.to_i


      total_user = ActivitySummary.find_by_sql(total_user_query(restaurant_id_sql,all_location)).first.count.to_i

      if (total30 + total60) > 0
        percent_lapsed = (total30 + total60)
        percent_lapsed = (percent_lapsed.to_f / total_user.to_f) * 100
        percent_lapsed = percent_lapsed.to_f.round(2)
      else
        percent_lapsed =0
      end
      csv_array << [res.name, total30,total60, percent_lapsed,total_user]
    end

    total_array =  csv_array.compact.transpose.map { |x| x.reduce(:+) rescue 0}
    total_array[0] = Tag.find(group2_single).name  rescue "-"
    percent_lapsed_tag = (total_array[1].to_i + total_array[2].to_i)
    total_array[4] = total_user([group2_single],all_location)
    total_array[3] = ((percent_lapsed_tag.to_f / total_array[4].to_f) * 100).to_f.round(2)

    return total_array, csv_array.compact
  end

  ### all grouping tag
  def generate_all_location_tag(group1, all_location, period1, period2)
    tags = find_tags(group1)


    today_time = Time.zone.now.in_time_zone('EST')
    csv_array = []
    total_array = []

    #### sql initialize
    condition_30day = "date(last_active) <= date('#{today_time - 30.days}') AND date(last_active) >= date('#{today_time - 60.days}') AND
                        (date(last_redeem) <= date('#{today_time - 30.days}') OR last_redeem IS NULL)"

    condition_60day = "date(last_active) <= date('#{today_time - 60.days}')  AND
                       ( date(last_redeem) <= date('#{today_time - 60.days}') OR last_redeem IS NULL)"

    condition_120day = "date(last_active) <= date('#{today_time - 120.days}') AND
                        (date(last_redeem) <= date('#{today_time - 120.days}') OR last_redeem IS NULL)"

    tags.each do |tag|
      restaurants = find_restaurants(tag,all_location)
      restaurant_id_sql = restaurants.map(&:id).join(',')


      sql30 = build_query(condition_30day, all_location, restaurant_id_sql)
      total30 = ActivitySummary.find_by_sql(sql30).first.count.to_i

      sql60 = build_query(condition_60day, all_location, restaurant_id_sql)
      total60 = ActivitySummary.find_by_sql(sql60).first.count.to_i

      # sql120 = build_query(condition_120day, all_location, restaurant_id_sql)
      # total120 = ActivitySummary.find_by_sql(sql120).first.count.to_i


      total_user = ActivitySummary.find_by_sql(total_user_query(restaurant_id_sql,all_location)).first.count.to_i

      if (total30 + total60) > 0
        percent_lapsed = (total30 + total60)
        percent_lapsed = (percent_lapsed.to_f / total_user.to_f) * 100
        percent_lapsed = percent_lapsed.to_f.round(2)
      else
        percent_lapsed =0
      end
      csv_array << [tag.name, total30,total60, percent_lapsed,total_user]
    end

    total_array =  csv_array.compact.transpose.map { |x| x.reduce(:+) rescue 0}
    total_array[0] = "ALL LOCATION"  rescue "-"
    percent_lapsed_tag = (total_array[1].to_i + total_array[2].to_i)
    total_array[4] = total_user(tags,all_location)
    total_array[3] = ((percent_lapsed_tag.to_f / total_array[4].to_f) * 100).to_f.round(2)
    total_array[1] = total_all_location60(tags,all_location)
    total_array[2] = total_all_location120(tags,all_location)


    return total_array, csv_array.compact
  end

  def total_user(tags,all_location)
    restaurants = find_restaurants(tags, all_location)
    restaurant_ids_sql = restaurants.map(&:id).join(',')
    RestaurantUser.where("restaurant_id in(#{restaurant_ids_sql})").count
  end

  def total_all_location60(tags, all_location)
    today_time = Time.zone.now.in_time_zone('EST')
    restaurants = find_restaurants(tags, all_location)
    restaurant_id_sql = restaurants.map(&:id).join(',')
    condition_30day = "date(last_active) <= date('#{today_time - 30.days}') AND date(last_active) >= date('#{today_time - 60.days}') AND
                        (date(last_redeem) <= date('#{today_time - 30.days}') OR last_redeem IS NULL)"

    sql30 = build_query(condition_30day, all_location, restaurant_id_sql)
    total30 = ActivitySummary.find_by_sql(sql30).first.count.to_i
    return total30
  end

  def total_all_location120(tags, all_location)
    today_time = Time.zone.now.in_time_zone('EST')
    restaurants = find_restaurants(tags, all_location)
    restaurant_id_sql = restaurants.map(&:id).join(',')
    condition_30day = "date(last_active) <= date('#{today_time - 60.days}') AND
                        (date(last_redeem) <= date('#{today_time - 60.days}') OR last_redeem IS NULL)"

    sql30 = build_query(condition_30day, all_location, restaurant_id_sql)
    total30 = ActivitySummary.find_by_sql(sql30).first.count.to_i
    return total30
  end

  def build_query(condition, chain, restaurant_id_sql)
    select_condition = "SELECT count(distinct(user_id))  FROM restaurant_users"
    sql = "
    #{select_condition}

    INNER JOIN  users on users.id = restaurant_users.user_id
    WHERE users.active = 't ' AND users.chain_id = #{chain.id} AND
    #{condition}  AND
    restaurant_users.restaurant_id IN (#{restaurant_id_sql})
    "

    return sql
  end

  def total_user_query(restaurant_id_sql,chain)
    select_condition = "SELECT count(distinct(user_id))  FROM restaurant_users"
    sql = "
    #{select_condition}

    INNER JOIN  users on users.id = restaurant_users.user_id
    WHERE users.active = 't ' AND users.chain_id = #{chain.id}
    AND
    restaurant_users.restaurant_id IN (#{restaurant_id_sql})
    "

    return sql
  end

  def find_tags(tag_ids)
    tags = Tag.where("id in (?)", tag_ids)
    return tags
  end

  def find_restaurants(tag_ids, chain_id = nil)
    restaurants = []
    tags = find_tags(tag_ids)
    tags.each { |t| restaurants << t.restaurants.where("restaurants.status  IS TRUE and chain_id = ?", chain_id) }
    return restaurants.uniq.flatten
  end

end