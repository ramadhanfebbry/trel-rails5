class ReportJob::ReportSchedulerJob < Struct.new(:report_subscriptions)
  def perform
    @success_ids = []
    @failed_ids = []
    @error_text = []

    report_subscriptions.each do |rs|
      begin
        rs.time_to_run_at
        @success_ids << rs.id
      rescue => e
        @failed_ids << rs.id
        @error_text << e.inspect
        next
      end
    end
  end

  def success(job)
    puts "ReportSchedulerJob::#{@success_ids} Success" rescue "-"
    puts "-----------------------------SUCCESS-------------------------------"
  end

  def error(job, exception)
    puts "ReportSchedulerJob::#{rs.report_definition.name} Error" rescue "-"
    puts "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    puts "#{@error_text}"
    puts "#{@failed_ids}"
    OwnerMailer.report_error_notification(@failed_ids, job.id,@error_text).deliver!
  end


end


