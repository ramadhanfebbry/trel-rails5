class ReportJob::ZoesGeodata < Struct.new(:chain, :email, :start_date, :end_date)
  def perform
    ## live testing purpose
    # email = "inoe.bainur@gmail.com"
    # chain = Chain.find 5
    # start_date = Time.now - 10.days
    # end_date = Time.now
    # start_date = start_date.to_date
    #    end_date = end_date.to_date

    all_location = chain
    #reward_ids = [365, 357]
    file_name = "Geodata #{chain.name}.csv"

    #header = array_header(chain, period1, period2, reward_ids)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << ['User Id', 'Lat', 'Long', 'Created at', 'Collection Time', 'Last receipt Submission', 'Spent Trailing 60 days']

      sql = " select distinct on (user_id, date(created_at)) * from analytics_geodata where date(created_at) between '#{start_date.to_date}' and '#{end_date.to_date}'"
      anl = AnalyticsGeodata.find_by_sql(sql)

      anl.each do |dt|
        begin
          u = User.find(dt.user_id)
          csv<< [
              dt.user_id,
              dt.lat,
              dt.long,
              dt.created_at.in_time_zone('EST'),
              (dt.device_time.blank?? dt.created_at.in_time_zone('EST') : dt.device_time),
              (u.receipts.where(:status => 3).last.last_transaction.restaurant.name rescue nil),
              (subtotal_from_days(u) rescue 0)
          ]
        rescue
          next
        end
      end
    end

    puts "EAAAAAAAAAAAAAAA"
    begin
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name, "Geodata #{chain.name}").deliver!
    rescue
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name).deliver!
    end
    File.delete("tmp/#{file_name}.csv")
  end

  def subtotal_from_days(user)
    sql = "
      select sum(t1.subtotal) from receipts c
left join receipt_transactions t1 on
t1.receipt_id = c.id left join
receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
where t2.id is null and c.user_id= #{user.id} and
date(c.created_at - interval '5 hour') >= '#{(Time.zone.now - 60.days).to_date}'
and date(c.created_at - interval '5 hour') <= '#{Time.zone.now.to_date}'
      "

    total = ActivitySummary.find_by_sql(sql).first.sum.to_f
    return total
  end

end