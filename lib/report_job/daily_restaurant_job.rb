class ReportJob::DailyRestaurantJob < Struct.new(:chain_id,:restaurant_id, :kind, :start_date, :end_date, :owner)
  #puts "params = #{params}"
  def perform
    report_server = Setting.jasper.server
    report_user = Setting.jasper.username
    report_password = Setting.jasper.password
    
    report_type,last_date =
      case kind
      when "daily"
        ["Day", (Time.zone.now - 1.days).to_date]
      when "weekly"
        ["Week", (Time.zone.now - 7.days).to_date]
      when "monthly"
        ["Month", (Time.zone.now - 30.days).to_date]
      end

    #login to JasperServer and acquire cookies for its session
    response_login =
      RestClient.post "#{report_server}/login.html",
      :j_username => report_user,
      :j_password => report_password

    #get resourceDescriptor before generate the report
    response_resource =
      RestClient.get "#{report_server}/rest/resource/reports/samples/daily",
      {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}

    doc = Nokogiri::XML.parse(response_resource)
    chain_param = Nokogiri::XML::Node.new "parameter", doc
    chain_param['name'] = 'chain_id'
    chain_param.content = chain_id

    res_param = Nokogiri::XML::Node.new "parameter", doc
    res_param['name'] = 'restaurant_id'
    res_param.content = restaurant_id

    ## params date daily / weekly / monthly before
    kind_param = Nokogiri::XML::Node.new "parameter", doc
    kind_param['name'] = 'kind'
    kind_param.content = last_date.to_datetime.to_i * 1000

    start_param = Nokogiri::XML::Node.new "parameter", doc
    start_param['name'] = 'start_date'
    start_param.content = start_date.to_datetime.to_i * 1000


    end_param = Nokogiri::XML::Node.new "parameter", doc
    end_param['name'] = 'end_date'
    end_param.content = end_date.to_datetime.to_i * 1000


    doc.children.first.add_child(chain_param)
    doc.children.first.add_child(start_param)
    doc.children.first.add_child(end_param)
    doc.children.first.add_child(kind_param)

    response_put_report_pdf =
          RestClient.put "#{report_server}/rest/report/reports/samples/daily_report/daily?RUN_OUTPUT_FORMAT=PDF",
          doc.to_s,
          {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}

    #get the UUID that generated from previous action    
    uuid_pdf = Nokogiri::XML.parse(response_put_report_pdf).xpath('//report/uuid').text

    #download the report
    response_get_report =
      RestClient.get "#{report_server}/rest/report/#{uuid_pdf}?file=report",
      {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}    

    if Rails.env != "production"
      secret_key = Setting.storage.s3_access_key_id
      access_key = Setting.storage.s3_secret_access_key
      bucket = Setting.storage.s3_bucket
    else
      secret_key = ENV["S3_KEY"]
      access_key = ENV["S3_SECRET"]
      bucket = ENV["S3_BUCKET"]
    end


    aw = AWS::S3.new(
      :access_key_id     =>secret_key ,
      :secret_access_key =>access_key
    )

    File.open("#{Rails.root}/tmp/test.pdf","w+"){|f| f << response_get_report.force_encoding("utf-8")}
    #File.open("#{Rails.root}/tmp/test.pdf","w+"){|f| f << response_get_report_pdf.force_encoding("utf-8")}

    key = "/report/chain_#{chain_id}/restaurant_#{restaurant_id}/#{start_date.to_date.to_s}_#{end_date.to_date.to_s}.pdf"
    
    aw.buckets[bucket].objects[key].write(:file =>  "#{Rails.root}/tmp/test.pdf")
    File.delete("#{Rails.root}/tmp/test.pdf")

    url = aw.buckets[bucket].objects[key].url_for(:read).to_s

    report = Report.find_or_create_by_start_date_and_end_date_and_chain_id(
      start_date.to_date,
      end_date.to_date,
      chain_id,
      key,
      url,
      kind,
       restaurant_id
    )

    ChainMailer.report_owner(owner, report).deliver
  end
end