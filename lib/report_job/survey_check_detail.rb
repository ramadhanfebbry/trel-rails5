class ReportJob::SurveyCheckDetail < Struct.new(:survey_id, :owner_email, :params)
  def perform
    dt_zone_created_at = "date(TIMEZONE('UTC', created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
    file_name = "answer_general_exports"
    if params[:start_date].blank?
      start_date = Time.zone.now - 31.days
      end_date = Time.zone.now
    else
      start_date = params[:start_date]
      end_date = params[:end_date]
    end

    survey = Survey.find(survey_id)
    if start_date.blank? || end_date.blank?
      #surveys = SurveysUser.where("survey_id = ?", survey_id)
      total_pages = SurveysUser.where("survey_id = ?", survey_id).paginate(:page => 1, :per_page => 500).total_pages
      file_name = "survey_#{survey.title.gsub(' ', '_')}_answer_exports.csv"
    else
      #surveys = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?", survey_id, start_date, end_date)
      total_pages = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?", survey_id, start_date, end_date).paginate(:page => 1, :per_page => 500).total_pages
      file_name = "survey_#{survey.title.gsub(' ', '_')}_#{start_date.to_date.strftime('%y%m%d')}_#{end_date.to_date.strftime('%y%m%d')}.csv"
    end

    survey_questions = survey.all_questions_collection(nil,nil)
    questions = survey_questions.map(&:text)
    question_ids = survey_questions.map(&:id)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << [
          "Restaurant id",
          "Restaurant name",
          "Check Detail",
          "Issue Date",
          "Date",
          ] + questions


      1.upto(total_pages).each do |i|
        if start_date.blank? || end_date.blank?
          surveys = SurveysUser.where("survey_id = ? and show_dashboard = ?", survey_id,true).paginate(:page => i, :per_page => 500)
        else
          surveys = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ? and show_dashboard = ? ", survey_id, start_date, end_date,true).paginate(:page => i, :per_page => 500)
        end

        surveys.each do |answer_survey|
          answers_tmp = answer_survey.answers.where("question_id in (?)", question_ids)
          lt = answer_survey.receipt.last_transaction rescue nil
          begin
            res_name = (lt.restaurant.name)
          rescue
            res_name = Restaurant.find(lt.restaurant_id).name rescue "-"
          end

          average = ""
          average = (answer_survey.scores.round(2) rescue nil) if survey.having_averages?

          csv << [
              answer_survey.receipt_id,
              #answer_survey.created_at.strftime('%Y-%m-%d'),
              issue_date_switch(lt,answer_survey),
              #answer_survey.created_at.in_time_zone('EST').strftime('%I:%M%p %Z'),
              issue_hour_switch(lt,answer_survey),
              res_name,
              (answer_survey.user.email rescue nil),
              average,
          ] + slice_answers(answers_tmp, question_ids)
        end
      end
    end

    OwnerMailer.send_survey_dashboard(survey.chain, owner_email, "tmp/#{file_name}.csv", file_name).deliver!
    File.delete("tmp/#{file_name}.csv")
  end


  def issue_date_switch(lt,lts)
    unless lt.blank?
      lt.issue_date.blank?? lts.created_at.to_datetime.strftime("%Y-%m-%d") : lt.issue_date.strftime("%Y-%m-%d")
    else
      lts.created_at.to_datetime.strftime("%Y-%m-%d")
    end
  end

  def issue_hour_switch(lt,lts)
    unless lt.blank?
      lt.time_stamp.blank?? lts.created_at.in_time_zone('EST').strftime('%I:%M%p %Z') : lt.time_stamp
    else
      lts.created_at.in_time_zone('EST').strftime('%I:%M%p %Z')
    end
  end

  def slice_answers(answers_tmp, question_ids)
    result = []
    answers_question_ids = answers_tmp.map(&:question_id)

    ## if there no duplicate
    if answers_question_ids.detect { |e| answers_question_ids.count(e) > 1 }.blank? # select if there are duplicate questoin_id
      question_ids.each do |q_id|
        if answers_question_ids.include?(q_id)
          index = answers_question_ids.index(q_id)
          val = answers_tmp[index].value #rescue nil
          result << val
        else
          result << ""
        end
      end
      return result
    else
      ## if there are duplicate / multiple answers for 1 question
      question_ids.each do |q_id|
        multiple =  answers_tmp.select{|x| x if x.question_id.to_i == q_id}
        if multiple.size > 1
          tmp = []
          multiple.each_with_index do |answ_collection,i|

            value =  answ_collection.value

            if value.to_s.include?("null")
              tmp << ""
            else
              tmp << value.to_s
            end
          end
          result << tmp.join(' and ')
        else
          #result << multiple.first.value rescue nil
          if answers_question_ids.include?(q_id)
            index = answers_question_ids.index(q_id)
            val = answers_tmp[index].value rescue " " #rescue nil
            val = " " if val == ""
            result << val
          else
            result << " "
          end
        end
      end
      return result

    end
  end

  def answer_text_for(answer)
    case answer.answer_type
      when ::Answer::TYPES['Comments']
        a = ""
        begin
          a = AnswerText.find(answer.value_id).try(:text) #rescue ""
          a = "" if a.downcase.include?('null')
        rescue
          a
        end
        a
      when ::Answer::TYPES['Multiple Choice'], ::Answer::TYPES['Slider']
        value_type = Question.find(answer.question_id).label_or_value rescue "value"
        QuestionChoice.find(answer.value_id).try(value_type.to_sym) rescue ''
      when ::Answer::TYPES['Dropdown']
        value_type = Question.find(answer.question_id).label_or_value rescue "value"
        #QuestionChoice.find(answer.value_id).try(:label) rescue ''
        QuestionChoice.find(answer.value_id).try(value_type.to_sym) rescue ''
      when ::Answer::TYPES["Multiple Dropdown"]
        value_type = Question.find(self.question_id).label_or_value rescue "value"
        QuestionChoice.find(self.value_id).try(value_type.to_sym) rescue ""
      else
        raise 'unknown question/answer type'
    end
  end

  def is_number?(var)
    true if Float(var) rescue false
  end
end
