class ReportJob::AnswerJob < Struct.new(:params)
  def perform

    file_name = params.to_a.join('_')
    file_name = "answer_exports.csv" if file_name.blank?
    admin = Admin.find(params['admin_id'])
    
    surveys = SurveysUser.includes(:user,:answers, :survey => :chain ).search(params).order("surveys_users.updated_at desc")
    survey = Survey.find(surveys.first.survey_id)
    survey_questions = survey.all_questions_collection(nil,nil)
    questions = survey_questions.map(&:text)
    question_ids = survey_questions.map(&:id)

    question_row = []
    survey_questions.each_with_index do |x,i|
      question_row << "q#{i+1}"
    end

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << [
        "Survey Submit Id",
        "Email",
        "Chain",
        "Survey Name",
        "Offer / Reward / Deal Names"] + questions +

        ["Restaurant Location (for offers)",
        "Receipt review status","Date time"
      ]

      # data rows
      surveys.each do |answer_survey|
        answers_tmp = answer_survey.answers.where("question_id in (?)", question_ids)

        csv << [
          answer_survey.receipt_id,
          (answer_survey.user.email rescue nil),
          (answer_survey.survey.chain.name rescue nil),
          (answer_survey.survey.title rescue nil),
          (answer_survey.receipt.receipt_transactions.first.offer.name rescue nil)] +
             slice_answers(answers_tmp, question_ids) +
          [(answer_survey.receipt.last_transaction.restaurant.dashboard_display_text rescue nil),
          (Receipt::STATUS.index(answer_survey.receipt.status) rescue nil),
          (answer_survey.updated_at.strftime('%m-%d-%Y'))
        ]
      end
    end

    puts "aws process"

    if RAILS_ENV != "production"
      secret_key = Setting.storage.s3_access_key_id
      access_key = Setting.storage.s3_secret_access_key
      bucket = Setting.storage.s3_bucket
    else
      secret_key = ENV["S3_KEY"]
      access_key = ENV["S3_SECRET"]
      bucket = ENV["S3_BUCKET"]
    end
    
    aw = AWS::S3.new(
      :access_key_id     =>secret_key ,
      :secret_access_key =>access_key
    )
    puts "/report/#{file_name}.csv"
    puts "============================================================"
    #aw.buckets[bucket].objects["/report/#{file_name}.csv"].write(:file =>  "tmp/#{file_name}.csv")
    OwnerMailer.send_reward_redeemed(nil, admin.email, "tmp/#{file_name}.csv", file_name+".csv","Admin Survey Answer Export").deliver!
    File.delete("tmp/#{file_name}.csv")

  end

  #def slice_answers(answers, question_ids)
  #  result = []
  #  answers_question_ids = answers.map(&:question_id)
  #  question_ids.each do |q_id|
  #    if answers_question_ids.include?(q_id)
  #      index = answers_question_ids.index(q_id)
  #      val = answers[index].value rescue nil
  #      if is_number?(val)
  #        puts "#{val.to_i}"
  #        puts "#{Question.find(q_id).question_type}"
  #        puts "------------------"
  #        if val.to_i == 0 and Question.find(q_id).question_type == 3 # if dropdown
  #          puts "null"
  #          result << "null"
  #        else
  #          puts "above #{q_id}"
  #          result << val
  #        end
  #      else
  #        puts "#{q_id}"
  #        result << val
  #      end
  #    else
  #      result << "null"
  #    end
  #  end
  #  return result
  #end
  def slice_answers(answers, question_ids)
    result = []
    answers_question_ids = answers.map(&:question_id)
    question_ids.each do |q_id|
      if answers_question_ids.include?(q_id)
        index = answers_question_ids.index(q_id)
        val = answer_text_for(answers[index]) #rescue nil
        result << val
      else
        result << ""
      end
    end
    return result
  end

  def answer_text_for(answer)
    case answer.answer_type
      when ::Answer::TYPES['Comments']
        a = ""
        begin
          a = AnswerText.find(answer.value_id).try(:text) #rescue ""
          a = "" if a.downcase.include?('null')
        rescue
          a
        end
        a
      when ::Answer::TYPES['Multiple Choice'], ::Answer::TYPES['Slider']
        value_type = Question.find(answer.question_id).label_or_value rescue "value"
        QuestionChoice.find(answer.value_id).try(value_type.to_sym) rescue ''
      when ::Answer::TYPES['Dropdown']
        value_type = Question.find(answer.question_id).label_or_value rescue "value"
        QuestionChoice.find(answer.value_id).try(value_type.to_sym) rescue ''
      else
        raise 'unknown question/answer type'
    end
  end

  def is_number?(var)
    true if Float(var) rescue false
  end
end