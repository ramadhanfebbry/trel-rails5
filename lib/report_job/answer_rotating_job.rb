class ReportJob::AnswerRotatingJob < Struct.new(:rotating_id, :owner_email,:params)
  def perform
    file_name = "answer_exports"
    dt_zone_created_at = "date(TIMEZONE('UTC', created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
    rotate = RotatingGroup.find(rotating_id)
    survey = rotate.survey
    if params[:start_date].blank?
      start_date = Time.zone.now - 31.days
      end_date = Time.zone.now
    else
      start_date = params[:start_date]
      end_date = params[:end_date]
    end

    survey_id = survey.id
    if start_date.blank? || end_date.blank?
      surveys = SurveysUser.where("survey_id = ?", survey_id)
      file_name = "rotating_#{rotate.name}_survey_#{survey.title}_export.csv"
    else
      surveys = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?", survey_id, start_date, end_date)
      file_name = "rotating_#{rotate.name}_#{start_date.to_date.strftime('%y%m%d')}_#{end_date.to_date.strftime('%y%m%d')}.csv"
    end

    rotating_questions = survey.rotating_questions
    questions = rotating_questions.map(&:text)
    question_ids = rotating_questions.map(&:id)
    question_size = questions.size
    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << [
          "Receipt Id",
          "Date",
          "Time",
          "Location",
          "Member", "Avg"] + questions


      surveys.each do |answer_survey|
        answers_tmp = answer_survey.answers.where("question_id in (?)", question_ids)
        begin
          res_name = (answer_survey.receipt.last_transaction.restaurant.name)
        rescue
          res_name = Restaurant.find(answer_survey.receipt.last_transaction.restaurant_id).name rescue "-"
        end
        ans = slice_answers(answers_tmp, question_ids)
        puts "ANSWER TMP = #{ans}"
        ans_tmp = ans
        un_blank = ans_tmp.delete_if{|x| x.blank? || x.to_s == "-"}
        puts "UNBLANK = #{un_blank}"

        if un_blank.size > 0
        csv << [
            answer_survey.receipt_id,
            answer_survey.created_at.strftime('%Y-%m-%d'),
            answer_survey.created_at.in_time_zone('EST').strftime('%I:%M%p %Z'),
            res_name,
            (answer_survey.user.email rescue nil),
            " "
        ] + slice_answers(answers_tmp, question_ids)
          #puts "CSV IS #{csv}"
          puts slice_answers(answers_tmp, question_ids)
          end
      end
    end

    OwnerMailer.send_survey_dashboard(survey.chain, owner_email, "tmp/#{file_name}.csv", file_name).deliver!
    File.delete("tmp/#{file_name}.csv")
  end

  #def slice_answers(answers, question_ids)
  #    result = []
  #    answers_question_ids = answers.map(&:question_id)
  #    question_ids.each do |q_id|
  #       if answers_question_ids.include?(q_id)
  #          index = answers_question_ids.index(q_id)
  #          val = answers[index].value rescue " "
  #          val = " " if val == ""
  #         result << val
  #       else
  #         result << " "
  #       end
  #    end
  #  return result
  #end

  def slice_answers(answers_tmp, question_ids)
    result = []
    answers_question_ids = answers_tmp.map(&:question_id)

    ## if there no duplicate
    if answers_question_ids.detect { |e| answers_question_ids.count(e) > 1 }.blank? # select if there are duplicate questoin_id
      question_ids.each do |q_id|
        if answers_question_ids.include?(q_id)
          index = answers_question_ids.index(q_id)
          val = answers_tmp[index].value rescue " " #rescue nil
          val = " " if val == ""
          result << val
        else
          result << " "
        end
      end
      return result
    else
      ## if there are duplicate / multiple answers for 1 question
      question_ids.each do |q_id|
        multiple =  answers_tmp.select{|x| x if x.question_id.to_i == q_id}
        if multiple.size > 1
          tmp = []
          multiple.each_with_index do |answ_collection,i|

            value =  answ_collection.value

            if value.to_s.include?("null")
              tmp << ""
            else
              #tmp << "Answer #{i+1}: "+value.to_s
              tmp << value.to_s
            end
          end
          result << tmp.join(' | ')
        else
          #result << multiple.first.value rescue nil
            if answers_question_ids.include?(q_id)
              index = answers_question_ids.index(q_id)
              val = answers_tmp[index].value rescue " " #rescue nil
              val = " " if val == ""
              result << val
            else
              result << " "
          end
        end
      end
      return result
    end
  end
end
