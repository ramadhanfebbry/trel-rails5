module ReportJob
  module ReportGenerator

    # frel test 994122838
    # ReportJob::ReportGenerator.generate_report(5, ReportDefinition.last, ReportSubscription.last)
    def self.generate_report(chain_id,report_definition,report_subscription, test_emails = [])
      ### variable definition
      custom_params = report_subscription.custom_param_hash
      file_extension = ReportSubscription::FILE_TYPES.key(report_subscription.file_type.to_i).downcase

      start_date = Time.zone.now
      #### end variable ###

      report_type,last_date =
          case report_subscription.report_kind
            when 1
              ["Day", start_date - 1.days]
            when 2
              ["Week", start_date  - 7.days]
            when 3
              ["Month", start_date  - 30.days]
          end

      ##login to JasperServer and acquire cookies for its session
      puts "--------------------------authenticating-----------------"
      cookies = report_subscription.authenticate_jasper
      puts cookies
      puts "--------------------------done authenticating-----------------"
      ##get resourceDescriptor before generate the report

      puts "\n\n----------------response resource-----------------"
      response_resource  = report_subscription.get_response_resource(cookies)
      puts "\n\n----------------Done ..response resource-----------------"
      ### parsing the xml and then add the parameters

      if report_definition.asynchronous == false
        return v2_api_get(chain_id,report_subscription,cookies, file_extension,test_emails)
      end

      doc = Nokogiri::XML.parse(response_resource)

      custom_params.each do |key,value|
        instance_variable_set("@#{key}",  Nokogiri::XML::Node.new("parameter", doc))
        puts "key ------------------------#{key}"
        puts "\n\n\n---------- variables ---------------"

        puts instance_variable_get("@#{key}")

        puts "\n\n\n---------- END variables ---------------"

        instance_variable_get("@#{key}")['name'] = key
        values = set_default_value(key, value, report_type)

        puts "values ==================== #{values}"

        instance_variable_get("@#{key}").content = values

        puts "End values ==================== #{values}"
        doc.children.first.add_child(instance_variable_get("@#{key}"))
      end

      ## get the UUID for the report
      puts " \n\n------------------------get put response---------------"
      uuid = report_subscription.get_put_response_resource_uuid(doc,cookies)

      puts " \n\n------------------------done get put response---------------"


      puts "-----------------------------------------------------------------"
      puts "I GET THE UUID"
      puts "#{uuid}"

      puts "-----------------------------------------------------------------"

      puts "\n\n\n get the strings ------------------------------------"

      report_file_name = "report_#{chain_id}_#{Date.today.to_s}.#{file_extension}"

      ## stored into report result tables , uuid and cookies, session for this user. it will takes 30 - 1 hour session
      report_subscription.stored_uuid_and_session(uuid, cookies, report_file_name)
=begin
      response_get_report = report_subscription.get_report_sync(uuid, cookies)


      File.open("#{Rails.root}/tmp/#{report_file_name}","w+"){|f| f << response_get_report.force_encoding("utf-8")}

      #set the key
      key = "/report/#{chain_id}/#{Date.today.to_s}/#{report_file_name}"

      ### stored to aws storage
      report_subscription.stored_in_aws("tmp/#{report_file_name}",key)

      ### stored on report model
      puts "\n\n stored to report ---------------"
      report_subscription.stored_to_report(chain_id,key,ReportSubscription::TYPES.key(report_subscription.report_kind))
      puts "------------------- end stored report ------------------------"
=end
    end

    def self.set_default_value(key, value, report_kind)
      if value.blank?
        start_date = Time.zone.now
        if key.to_s.downcase == "end_date"
          return Time.zone.now.to_datetime.to_i * 1000
        elsif key.to_s.downcase == "start_date" or key.to_s.downcase == "kind"
          case report_kind
            when "Day"
              return start_date.to_datetime.to_i * 1000
            when "Week"
              return (start_date - 7.days).to_datetime.to_i * 1000
            when "Month"
              return (start_date - 30.days).to_datetime.to_i * 1000
          end
        #elsif key.to_s.downcase == "kind"
        #  return start_date.to_datetime.to_i * 1000
        end
      else
        return value
      end
    end

    def self.v2_api_get(chain_id,report_subscription, cookies, file_extension,test_emails = [])
      report_file_name = "#{report_subscription.report_definition.name}_#{Date.today.to_s}_report.#{file_extension}"
      #if report_subscription && report_subscription.report_definition &&
      #    report_subscription.report_definition.email_subject && report_subscription.report_definition.name.include?("CUSTOM SURVEY COMMENT")
      #  chain = Chain.find(chain_id)
      #  report_file_name = (report_subscription.report_definition.email_subject % {
      #      :chain => (chain.name rescue ""),
      #      :start_date => Date.today.to_s,
      #      :end_date => nil
      #  }).gsub(/\r\n?/, "").gsub(/\n/, "")
      #  report_file_name = "#{report_file_name.gsub(' ', '_')}.#{file_extension}"
      #end

      uuid = "DIRECT"
      ## stored into report result tables , uuid and cookies, session for this user. it will takes 30 - 1 hour session
      report_subscription.stored_uuid_and_session(uuid, cookies, report_file_name,test_emails)
    end
  end
end