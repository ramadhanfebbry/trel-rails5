class ReportJob::GroupingLapsedUserNew < Struct.new(:period1, :period2, :group1, :group2, :group3, :chain, :email)
  def perform
    ## live testing purpose

    #  chain = Chain.find 22
    # # reward_ids = chain.rewards.map(&:id)#[469,366,364,362,359,352]
    # # # # #   all_location = chain.restaurants.where("status IS TRUE")
    #  email = "inoe.bainur@gmail.com"
    # # period1 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    # # period2 = nil
    # # #period2 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    period1 = nil
    period2=  nil
    #  group2 = [132]
    #  group1 = [132, 19]

    all_location = chain
    #reward_ids = [365, 357]
    file_name = "Lapsed User #{chain.name}.csv"

    #header = array_header(chain, period1, period2, reward_ids)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << ["Lapsed User"]
      csv << ['*','Have not made a visit in the last 30- 60 days', 'Have not made a visit in more than 60 days', '%Lapsed', 'Total associated users']
      #lapsed = generate_all_location_tag(group1, all_location, period1, period2)
      a = generate_all_user_total(chain)
      total_user = RestaurantUser.where("restaurant_id in(?)", chain.restaurants).count
      lapsed = generate_all_user_by_tag_1(group1,all_location, a[2], a[3])

      if (a[0].to_i + a[1].to_i) > 0
        percent_lapsed = (a[0].to_i + a[1].to_i)
        percent_lapsed = (percent_lapsed.to_f / total_user.to_f) * 100
        percent_lapsed = percent_lapsed.to_f.round(2)
      else
        percent_lapsed =0
      end
      csv << lapsed[0]




      p lapsed[1]
      lapsed[1].each do |m|
        csv << m
      end
      csv << [nil, nil, nil, nil, nil, nil, nil, nil]
      group2.each do |group2_single|
        arr = generate_single_tag_lapsed(group2_single, all_location, a[2], a[3])

        csv << arr[0]
        arr[1].each do |x|
          csv << x
        end
        csv << [nil, nil, nil, nil, nil, nil, nil, nil]
      end

    end

    puts "EAAAAAAAAAAAAAAA"
    begin
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name, "Lapsed User #{chain.name}").deliver!
    rescue
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name).deliver!
    end
    File.delete("tmp/#{file_name}.csv")
  end

  def generate_single_tag_lapsed(group2_single,all_location, users30,users60)
    restaurants = find_restaurants(group2_single, all_location)
    csv_array = []
    restaurants.each do |res|
      restaurant_id_sql = res.id

      total30 = 0
      total60 = 0
      users30.each do |u30|
        res_id = u30.restaurant_id
        total30 = total30 + 1 if res.id == res_id
      end

      users60.each do |u60|
        res_id = u60.restaurant_id
        total60 = total60 + 1 if res.id == res_id
      end

      #total_user = ActivitySummary.find_by_sql(total_user_query(restaurant_id_sql,all_location)).first.count.to_i
      total_user = RestaurantUser.where(:restaurant_id => res.id).count

      if (total30 + total60) > 0
        percent_lapsed = (total30 + total60)
        percent_lapsed = (percent_lapsed.to_f / total_user.to_f) * 100
        percent_lapsed = percent_lapsed.to_f.round(2)
      else
        percent_lapsed =0
      end
      csv_array << [res.name, total30,total60, percent_lapsed,total_user]
    end

    total_array =  csv_array.compact.transpose.map { |x| x.reduce(:+) rescue 0}
    total_array[0] = Tag.find(group2_single).name  rescue "-"
    percent_lapsed_tag = (total_array[1].to_i + total_array[2].to_i)
    total_array[4] = total_user_tag([group2_single],all_location)
    total_array[3] = ((percent_lapsed_tag.to_f / total_array[4].to_f) * 100).to_f.round(2)

    return total_array, csv_array.compact
  end


  def generate_all_user_by_tag_1(group1, all_location, users30, users60)
    #tags = find_tags(group1)

    tags = find_tags(group1)

    chain_id = all_location.id

    today_time = Time.zone.now.in_time_zone('EST')
    csv_array = []


    tags.each do |tag|
      restaurants = find_restaurants(tag, all_location)
      restaurant_ids = restaurants.map(&:id)
      sql_res = restaurant_ids.join(',')

      total30 = 0
      users30.each do |u30|
        res_id = u30.restaurant_id#res_id = RestaurantUser.where(:user_id => u30).order('last_active desc').first.restaurant_id
        total30 = total30 + 1 if restaurant_ids.include?(res_id)
      end

      total60 = 0
      users60.each do |u60|
        res_id = u60.restaurant_id
        total60 = total60 + 1 if restaurant_ids.include?(res_id)
      end

      puts sql_res
      #total_user = ActivitySummary.find_by_sql(total_user_query(sql_res, all_location)).first.count.to_i
      total_user = total_user_tag(tag, all_location)

      if (total30 + total60) > 0
        percent_lapsed = (total30 + total60)
        percent_lapsed = (percent_lapsed.to_f / total_user.to_f) * 100
        percent_lapsed = percent_lapsed.to_f.round(2)
      else
        percent_lapsed =0
      end
      csv_array << [tag.name, total30,total60, percent_lapsed,total_user]
    end

    total_array =  csv_array.compact.transpose.map { |x| x.reduce(:+) rescue 0}
    total_array[0] = "ALL LOCATION"  rescue "-"
    percent_lapsed_tag = (total_array[1].to_i + total_array[2].to_i)
    #total_array[4] = total_user_tag(tags,all_location)
    total_array[3] = ((percent_lapsed_tag.to_f / total_array[4].to_f) * 100).to_f.round(2)
    #total_array[1] = total_all_location60(tags,all_location)
    #total_array[2] = total_all_location120(tags,all_location)

    return total_array, csv_array.compact
  end

  def total_user_query(restaurant_id_sql,chain)
    select_condition = "SELECT count(distinct(user_id))  FROM restaurant_users"
    sql = "
    #{select_condition}

    INNER JOIN  users on users.id = restaurant_users.user_id
    WHERE users.active = 't ' AND users.chain_id = #{chain.id}
    AND
    restaurant_users.restaurant_id IN (#{restaurant_id_sql})
    "

    return sql
  end


  def total_user_tag(tags,all_location)
    restaurants = find_restaurants(tags, all_location)
    restaurant_ids_sql = restaurants.map(&:id).join(',')
    RestaurantUser.where("restaurant_id in(#{restaurant_ids_sql})").count
  end

  def find_tags(tag_ids)
    tags = Tag.where("id in (?)", tag_ids)
    return tags
  end

  def find_restaurants(tag_ids, chain_id = nil)
    restaurants = []
    tags = find_tags(tag_ids)
    tags.each { |t| restaurants << t.restaurants.where("restaurants.status  IS TRUE and chain_id = ?", chain_id) }
    return restaurants.uniq.flatten
  end


  ### all grouping tag
  def generate_all_user_total(all_location)
    chain_id = all_location.id

    today_time = Time.zone.now.in_time_zone('EST')

    #### sql initialize
    condition_60day = "date(last_active) <= date('#{today_time - 30.days}') AND date(last_active) >= date('#{today_time - 60.days}') AND
                        (date(last_redeem) <= date('#{today_time - 30.days}') OR last_redeem IS NULL)"

    condition_more60day = "date(last_active) <= date('#{today_time - 60.days}') AND
                       ( date(last_redeem) <= date('#{today_time - 60.days}') OR last_redeem IS NULL)"


    sql_60_day = "
      select ru.user_id as id,users.email, MAX(last_active) as date from restaurant_users ru
      inner join users on users.id = ru.user_id and users.chain_id = #{chain_id}
      where #{condition_60day}  group by ru.user_id ,users.email

    "

    sql_120_day = " select ru.user_id as id,users.email, MAX(last_active) as date from restaurant_users ru
      inner join users on users.id = ru.user_id and users.chain_id = #{chain_id}
      where #{condition_more60day}  group by ru.user_id ,users.email
"

    users60 = User.find_by_sql(sql_60_day)

    users120 = User.find_by_sql(sql_120_day)

    users60.delete_if { |x| users120.include?(x) }
    users30return = []
    users60return = []
    total30 = 0
    total60 = 0

    users60.each do |user|
      if user.receipts.where(:status => 3).count >= 1 and check_ru_60(user)
        total30 = total30 + 1
        struct = OpenStruct.new
        struct.user_id = user.id
        struct.restaurant_id = RestaurantUser.where(:user_id => user.id).order('last_active desc').first.restaurant_id
        users30return << struct
      end
    end

    users120.each do |user|
      if user.receipts.where(:status => 3).count >=1 and check_ru_120(user)
        total60 = total60 + 1
        struct = OpenStruct.new
        struct.user_id = user.id
        struct.restaurant_id = RestaurantUser.where(:user_id => user.id).order('last_active desc').first.restaurant_id
        users60return << struct
      end
    end

    return total30, total60, users30return, users60return
  end

  def check_ru_60(user)
    today_time = Time.zone.now.in_time_zone('EST')
    date = RestaurantUser.where(:user_id => user.id).order('last_active desc').first.last_active rescue nil

    if date > (today_time - 30.days).to_date
      return false
    elsif date <= (today_time - 30.days).to_date and date >= (today_time - 60.days).to_date
      return true
    end

    return false if date.blank?
  end

  def check_ru_120(user)
    today_time = Time.zone.now.in_time_zone('EST')
    date = RestaurantUser.where(:user_id => user.id).order('last_active desc').first.last_active rescue nil

    if date > (today_time - 30.days).to_date
      return false
    elsif date <= (today_time - 60.days).to_date
      return true
    end

    return false if date.blank?
  end



end