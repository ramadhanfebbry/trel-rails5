class ReportJob::ReportDefinitionJob < Struct.new(:report_definition,:chain, :owners)
  #owners = list of owners
  # report decs = Report Desc from database

  ## variable definition
  report_server = Setting.jasper.server
  report_user = Setting.jasper.username
  report_password = Setting.jasper.password
  rd = report_definition
  ch = chain
  query = rd.query

  report_type,last_date =
      case report_definition.report_kind
        when 1
          ["Day", start_date - 1.days]
        when 2
          ["Week", start_date  - 7.days]
        when 3
          ["Month", start_date  - 30.days]
      end

  #login to JasperServer and acquire cookies for its session
  cookies = rd.authenticate_jasper

  #get resourceDescriptor before generate the report
  response_resource = RestClient.get "#{rd.get_report_url}",cookies
  ## parsing the xml and then add the parameters
  doc = Nokogiri::XML.parse(response_resource)


  doc.children.first.add_child(chain_param)
  doc.children.first.add_child(start_param)
  doc.children.first.add_child(end_param)
  doc.children.first.add_child(kind_param)
  doc.children.first.add_child(type_param)

  response_put_report =
      RestClient.put "#{report_server}/rest/report/reports/samples/daily_report/daily?RUN_OUTPUT_FORMAT=HTML",
                     doc.to_s,
                     {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}} if file_type.blank? or file_type.downcase == "html"

  response_put_report_pdf =
      RestClient.put "#{report_server}/rest/report/reports/samples/daily_report/daily?RUN_OUTPUT_FORMAT=PDF",
                     doc.to_s,
                     {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}} if  file_type.downcase == "pdf"

  #get the UUID that generated from previous action
  uuid = Nokogiri::XML.parse(response_put_report).xpath('//report/uuid').text  if file_type.blank? or file_type.downcase == "html"
  uuid_pdf = Nokogiri::XML.parse(response_put_report_pdf).xpath('//report/uuid').text if  file_type.downcase == "pdf"

  #download the report
  response_get_report =
      RestClient.get "#{report_server}/rest/report/#{uuid}?file=report",
                     {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}  if file_type.blank? or file_type.downcase == "html"

  response_get_report_pdf =
      RestClient.get "#{report_server}/rest/report/#{uuid_pdf}?file=report",
                     {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}} if  file_type.downcase == "pdf"


  secret_key, access_key, bucket = rd.get_s3_information

  aw = AWS::S3.new(
      :access_key_id     =>secret_key ,
      :secret_access_key =>access_key
  )

  File.open("#{Rails.root}/tmp/test.html","w+"){|f| f << response_get_report.force_encoding("utf-8")} if  file_type.downcase == "html"
  File.open("#{Rails.root}/tmp/test.pdf","w+"){|f| f << response_get_report_pdf.force_encoding("utf-8")} if  file_type.downcase == "pdf"


  if  file_type.downcase == "html"
    key = "/report/#{chain_id}/#{start_date.to_date.to_s}_#{end_date.to_date.to_s}.html"
    aw.buckets[bucket].objects[key].write(:file =>  "tmp/test.html")
    File.delete("tmp/test.html")
  end

  if file_type.downcase == "pdf"
    key_pdf = "/report/#{chain_id}/#{start_date.to_date.to_s}_#{end_date.to_date.to_s}.pdf" if  file_type.downcase == "pdf"
    aw.buckets[bucket].objects[key_pdf].write(:file =>  "tmp/test.pdf")
    File.delete("tmp/test.pdf")
  end

  url = aw.buckets[bucket].objects[key].url_for(:read).to_s
  key = key_pdf if  file_type.downcase == "pdf"

  report = Report.find_or_create_by_start_date_and_end_date_and_chain_id(
       start_date.to_date,
       end_date.to_date,
       chain_id,
       key,
       url,
       kind
  )

  ChainMailer.report_owner(owner, report).deliver
end