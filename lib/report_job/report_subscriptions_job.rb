class ReportJob::ReportSubscriptions < Struct.new(:chains)
  include ReportJob::Subscription

  def perform
    chain_collection = chains

    chain_collection.each do |ch|
      ## collect the information on chain report subscription
      unless ch.report_subscription.blank?
        process_subscription(ch.subscription,ch)
      end
    end
  end



  def process_subscription(subscription, chain)
    ## initialize variable to hold the subscription settings

    report_definition = subscription.report_definition

    if report_definition.asynchronous == false
      ReportJob::ReportGenerator.generate_report(chain,report_definition,subscription)
    else
       # asynh process
      ReportJob::ReportGeneratorAsync.generate_report(chain,report_definition,subscription)
    end
  end
end