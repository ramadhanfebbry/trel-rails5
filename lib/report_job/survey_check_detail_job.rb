class ReportJob::SurveyCheckDetailJob < Struct.new(:survey_id, :owner_email, :start_date, :end_date)
  def perform
    a = Time.now
    puts "SurveyCheckDetailJob running ----- #{a}"
    dt_zone_created_at = "date(timezone('EST', timezone('UTC', surveys_users.created_at)))"
    file_name = "survey_check_detail"
    puts "survey_id = #{survey_id}"
    puts "start_date = #{start_date}"

puts "end_date = #{end_date}"

    ## testing purpose
    # owner_email = 'mbainurb@gmail.com'
    # survey_id = 24
    # start_date = '2016-04-01'
    # end_date = '2016-04-03'

    #user_id = 471629
    # if params[:start_date].blank?
    #   start_date = Time.zone.now - 31.days
    #   end_date = Time.zone.now
    # else
    #   start_date = params[:start_date]
    #   end_date = params[:end_date]
    # end

    ## testing
    # survey_id = 24
    # owner_email = "mbainurb@gmail.com"
    # start_date = '2016-04-01'
    # end_date = '2016-04-01'

    # if start_date.blank?
    #   start_date = Date.today
    #   end_date = Date.today
    # end

    survey = Survey.find(survey_id)
      #surveys = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?", survey_id, start_date, end_date)
      total_pages = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?", survey_id, start_date, end_date).paginate(:page => 1, :per_page => 500).total_pages
      file_name = "survey_check_detail#{survey.title.gsub(' ', '_')}_#{start_date.to_date.strftime('%y%m%d')}_#{end_date.to_date.strftime('%y%m%d')}.csv"

    survey_questions = survey.all_questions_collection(nil,nil)
    questions = survey_questions.map(&:text)
    question_ids = survey_questions.map(&:id)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << [
          "User ID",
          "Restaurant Id",
          "Check Detail",
          "Issue Date",
          "Restaurant Name"
          ] + questions


      1.upto(total_pages).each do |i|
          surveys = SurveysUser.where("survey_id = ? and #{dt_zone_created_at} between ? and ?  ", survey_id, start_date, end_date).paginate(:page => i, :per_page => 500)

        surveys.each do |answer_survey|
          answers_tmp = answer_survey.answers.where("question_id in (?)", question_ids)
          lt = answer_survey.receipt.last_transaction rescue nil
          begin
            res_name = (lt.restaurant.name)
          rescue
            res_name = Restaurant.find(lt.restaurant_id).name rescue "-"
          end
          next if res_name == "-" || res_name.blank?

          @pos_check_upload = answer_survey.receipt.pos_check_upload rescue nil
          @pos_xml = @pos_check_upload#PosReceiptXml.new(@pos_check_upload.xml_data) rescue nil

          csv << [
              answer_survey.user_id,
              (lt.restaurant_id rescue nil),
              (YAML.load(@pos_xml.xml_data) rescue nil),
              (lt.restaurant.dashboard_display_text rescue nil),
              (answer_survey.created_at.in_time_zone('EST').strftime("%m/%d/%Y") rescue nil)
          ] + slice_answers(answers_tmp, question_ids)
        end
      end
    end

    OwnerMailer.send_survey_dashboard_encoding(survey.chain, owner_email, "tmp/#{file_name}.csv", file_name).deliver!
    b = Time.now - a
    puts "SurveyCheckDetailJob Completed in-----#{b}"
    File.delete("tmp/#{file_name}.csv")
  end


  def issue_date_switch(lt,lts)
    unless lt.blank?
      lt.issue_date.blank?? lts.created_at.to_datetime.strftime("%Y-%m-%d") : lt.issue_date.strftime("%Y-%m-%d")
    else
      lts.created_at.to_datetime.strftime("%Y-%m-%d")
    end
  end

  def issue_hour_switch(lt,lts)
    unless lt.blank?
      lt.time_stamp.blank?? lts.created_at.in_time_zone('EST').strftime('%I:%M%p %Z') : lt.time_stamp
    else
      lts.created_at.in_time_zone('EST').strftime('%I:%M%p %Z')
    end
  end

  def slice_answers(answers_tmp, question_ids)
    result = []
    answers_question_ids = answers_tmp.map(&:question_id)

    ## if there no duplicate
    if answers_question_ids.detect { |e| answers_question_ids.count(e) > 1 }.blank? # select if there are duplicate questoin_id
      question_ids.each do |q_id|
        if answers_question_ids.include?(q_id)
          index = answers_question_ids.index(q_id)
          val = answers_tmp[index].value #rescue nil
          result << val
        else
          result << ""
        end
      end
      return result
    else
      ## if there are duplicate / multiple answers for 1 question
      question_ids.each do |q_id|
        multiple =  answers_tmp.select{|x| x if x.question_id.to_i == q_id}
        if multiple.size > 1
          tmp = []
          multiple.each_with_index do |answ_collection,i|

            value =  answ_collection.value

            if value.to_s.include?("null")
              tmp << ""
            else
              tmp << value.to_s
            end
          end
          result << tmp.join(' and ')
        else
          #result << multiple.first.value rescue nil
          if answers_question_ids.include?(q_id)
            index = answers_question_ids.index(q_id)
            val = answers_tmp[index].value rescue " " #rescue nil
            val = " " if val == ""
            result << val
          else
            result << " "
          end
        end
      end
      return result

    end
  end

  def answer_text_for(answer)
    case answer.answer_type
      when ::Answer::TYPES['Comments']
        a = ""
        begin
          a = AnswerText.find(answer.value_id).try(:text) #rescue ""
          a = "" if a.downcase.include?('null')
        rescue
          a
        end
        a
      when ::Answer::TYPES['Multiple Choice'], ::Answer::TYPES['Slider']
        value_type = Question.find(answer.question_id).label_or_value rescue "value"
        QuestionChoice.find(answer.value_id).try(value_type.to_sym) rescue ''
      when ::Answer::TYPES['Dropdown']
        value_type = Question.find(answer.question_id).label_or_value rescue "value"
        #QuestionChoice.find(answer.value_id).try(:label) rescue ''
        QuestionChoice.find(answer.value_id).try(value_type.to_sym) rescue ''
      when ::Answer::TYPES["Multiple Dropdown"]
        value_type = Question.find(self.question_id).label_or_value rescue "value"
        QuestionChoice.find(self.value_id).try(value_type.to_sym) rescue ""
      else
        raise 'unknown question/answer type'
    end
  end

  def is_number?(var)
    true if Float(var) rescue false
  end
end
