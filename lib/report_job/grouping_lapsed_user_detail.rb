class ReportJob::GroupingLapsedUserDetail < Struct.new(:period1, :period2, :group1, :group2, :group3, :chain, :email)
  def perform
    ## live testing purpose

    #chain = Chain.find 22
    # # reward_ids = chain.rewards.map(&:id)#[469,366,364,362,359,352]
    # # # # #   all_location = chain.restaurants.where("status IS TRUE")
    #email = "inoe.bainur@gmail.com"
    # # period1 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    # # period2 = nil
    # # #period2 = {:start_date => Time.zone.now - 10.days, :end_date => Time.now}
    period1 = nil
    period2= nil
    group2 = [132]
    group1 = [132, 19]

    all_location = chain
    #reward_ids = [365, 357]
    file_name = "Lapsed User Detail #{chain.name}.csv"

    #header = array_header(chain, period1, period2, reward_ids)

    surveys_csv = CSV.open("tmp/#{file_name}.csv", "w+") do |csv|
      # header row
      csv << ["Lapsed User"]
      csv << ['User id', 'Email', ' Have not made a visit in the last 30- 60 days',
              'Have not made a visit in more than 60 days']
      arr = generate_all_user(group1, all_location, period1, period2)
      arr.each do |x|
        csv << x
      end
    end

    puts "EAAAAAAAAAAAAAAA"
    begin
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name, "Lapsed User Detail #{chain.name}").deliver!
    rescue
      OwnerMailer.send_survey_dashboard(chain, email, "tmp/#{file_name}.csv", file_name).deliver!
    end
    File.delete("tmp/#{file_name}.csv")
  end


  ### all grouping tag
  def generate_all_user(group1, all_location, period1, period2)
    #tags = find_tags(group1)
    chain_id = all_location.id

    today_time = Time.zone.now.in_time_zone('EST')
    #csv_array = []
    #total_array = []

    #### sql initialize
    condition_60day = "date(last_active) <= date('#{today_time - 30.days}') AND date(last_active) >= date('#{today_time - 60.days}') AND
                        (date(last_redeem) <= date('#{today_time - 30.days}') OR last_redeem IS NULL)"

    condition_more60day = "date(last_active) <= date('#{today_time - 60.days}') AND
                       ( date(last_redeem) <= date('#{today_time - 60.days}') OR last_redeem IS NULL)"

    # condition_120day = "date(last_active) <= date('#{today_time - 120.days}') AND
    #                     (date(last_redeem) <= date('#{today_time - 120.days}') OR last_redeem IS NULL)"


    # sql_60_day = " select users.* from users
    #         inner join restaurant_users ru on ru.user_id  = users.id
    #         where #{condition_60day} and  chain_id  = #{chain_id}"

    sql_60_day = "
      select ru.user_id as id,users.email, MAX(last_active) as date from restaurant_users ru
      inner join users on users.id = ru.user_id and users.chain_id = #{chain_id}
      where #{condition_60day}  group by ru.user_id ,users.email

    "

    sql_120_day = " select ru.user_id as id,users.email, MAX(last_active) as date from restaurant_users ru
      inner join users on users.id = ru.user_id and users.chain_id = #{chain_id}
      where #{condition_more60day}  group by ru.user_id ,users.email
"

    users60 = User.find_by_sql(sql_60_day)

    csv_user = []
    # users.each do |user|
    #   csv_user << [user.id,
    #                user.email,
    #                (Restaurant.find(user.restaurant_id).name rescue nil),
    #                nil,
    #                nil
    #
    #   ]
    # end

    users120 = User.find_by_sql(sql_120_day)

    users60.delete_if { |x| users120.include?(x) }

    users60.each do |user|
      if user.receipts.where(:status => 3).count >= 1 and check_ru_60(user)
        csv_user << [user.id,
                     user.email,
                     ((RestaurantUser.where(:user_id => user.id).order('last_active desc').first.restaurant.name) rescue nil),
                     nil,
                     nil

        ]
      end
    end
    #
    # users = User.find_by_sql(sql_120_day)

    users120.each do |user|
      if user.receipts.where(:status => 3).count >=1 and check_ru_120(user)
        csv_user << [user.id,
                     user.email,
                     nil,
                     ((RestaurantUser.where(:user_id => user.id).order('last_active desc').first.restaurant.name) rescue nil)
        ]
      end
    end


    return csv_user
  end

  def check_ru_60(user)
    today_time = Time.zone.now.in_time_zone('EST')
    date = RestaurantUser.where(:user_id => user.id).order('last_active desc').first.last_active rescue nil

    if date > (today_time - 30.days).to_date
      return false
    elsif date <= (today_time - 30.days).to_date and date >= (today_time - 60.days).to_date
      return true
    end

    return false if date.blank?
  end

  def check_ru_120(user)
    today_time = Time.zone.now.in_time_zone('EST')
    date = RestaurantUser.where(:user_id => user.id).order('last_active desc').first.last_active rescue nil

    if date > (today_time - 30.days).to_date
      return false
    elsif date <= (today_time - 60.days).to_date
      return true
    end

    return false if date.blank?
  end
end