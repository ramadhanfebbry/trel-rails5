class JasperRetry::RetryJasperJob < Struct.new(:params)
  def perform
    a = Time.now
    puts "Time start @ {a}"
    error_jobs = DelayedJob.where("last_error is not null and delayable_type = 'ReportSubscription' AND date(created_at) between ? and ?", (Time.zone.now - 1.days).to_date, (Time.zone.now + 2.days).to_date)
    error_jobs.each do |er|
      report_id = er.handler.split('id: ')[1].split('   ')[0].squish.to_i rescue nil
      unless report_id.blank?
        begin
          rep = ReportResult.find(report_id)
          rep.retry_date = rep.created_at.in_time_zone('EST') - 6.hours
          rep.sending_method_use
          "deleting id #{er.id}"
          er.delete
        rescue => e
          puts "Error #{e.inspect}"
          if e.inspect.include?("path")
            #er.delete
          end
          next
        end
      end
    end
  end
end