module RelevantRitas

  include RelevantAuthentication

  def migrate_old_ritas_user_to_relevant(params)
    rita_user = RitasUser.where(:email => params[:email]).first
    if rita_user
      user = User.new(params[:user])
      user.first_name =rita_user.first_name
      user.last_name = rita_user.last_name
      user.created_at = rita_user.member_sign_up_date
      user.name = [user.first_name, user.last_name].join(" ")
      user.password = user.random_password if user.password.blank?
      if user.locale_id.blank?
        user.locale_id = user.try(:chain).try(:locales).try(:first).try(:id) || Locale.find_by_key('en').try(:id)
      end
      #should join to table favorites to get the text only, not the ids
      p "user from rita---"
      p user
      user.favorite_menu_item = RitasUserFavItem.where(:email => user.email).pluck(:fid).join(", ")
      if user.save
        rita_user.update_column(:migrated, true)
        set_device_token_as_unique_for_user(user, (params[:sign_in_device_type] || params[:register_device_type]), params[:device_token], params[:device_id])
        user.update_phone_info(params[:phone_model], params[:os], params[:appkey])
        #user device stuff
        UserDeviceLog.add_log(user, params[:android_id], params[:keychain], (params[:sign_in_device_type] || params[:register_device_type]), "punchh migration", Time.current)
        user.earn(rita_user.number_of_punches)
        PointHistory.add_to_history(user.id, "Migration point", rita_user.number_of_punches)
        rita_reward = RitasUserReward.where(:email => params[:email], :pushed => false).first
        unless rita_reward.blank?
          1.upto(rita_reward.current_redeemable_cards) do |i|
            Reward.push_reward(ChainRewardEvent::EVENTS["PUNCHH_MIGRATION"], user.chain, user)
          end
          rita_reward.update_column(:pushed, true)
        end
        if params[:register_type].eql?("1")
          result = HTTParty.post(Rails.application.routes.url_helpers.forgot_password_api_v1_user_index_url(:host => ENV["HOST_URL"]),
                                 :body => { :appkey => user.chain_app_key,
                                            :email => user.email
                                 })
          if result.parsed_response && result.parsed_response.class == Hash && result.parsed_response["status"]
            return {:status => false, :notice => "A new password has been emailed to you as a security measure. Please login to access your account. We hope you enjoy the new app!", :nav_main_page => true}
          else
            p "error updating password process. RITAS"
            p user.errors
            return {:status => false, :notice => "We are sorry. We could not complete your request. Please try again. If error persists, reach out to our support team from the app."}
          end
        else
          return "CONT"
        end
      else
        p "error on creating user on RITA"
        p user.errors
        return {:status => false, :notice => "We are sorry. We could not complete your request. Please try again. If error persists, reach out to our support team from the app."}
      end
    else
      return  "CONT"
    end
  end

  def forgot_password_migrate_w_ritas(params)
    if params[:migrate].to_i == 1
      ritas_user = RitasUser.where(:email => params[:email]).first
      if ritas_user && !ritas_user.migrated
        p "Implies user was not migrated and we do not want to create a password as user is not on Relevant."
        p "Urging user to Sign up will handle new password creation."
        return {:status => false, notice: "Please sign up to continue using the app."}
      elsif ritas_user && ritas_user.migrated
        p "Implies there is an error associated with  user migration.. ie the user's account as he is not on Relevant but status implies he was migrated"
        return {:status => false, notice: "Please sign up to continue using the app. In case of any issues, please contact support from the app."}
      else
        return "CONT"
      end
    end
  end

  def process_loyalty_with_beacon_information(barcode, beacons, latitude, longitude)
    return {:status => false, :notice => "We are sorry! We could not associate your scan to a participating location. Please scan the code located at the store. If error persists, contact app support"} if barcode.blank?
    restaurant = nil
    chain = self.chain
    unless beacons.blank?
      beacon_uuid = beacons[/UMM(.*?)SN/,1].strip rescue nil
      beacon_serial_number = beacons[/SN(.*?)\z/,1].strip rescue nil
      if beacon_uuid && beacon_serial_number
        restaurant = chain.restaurants.active.where("UPPER(location_qrcode_identifier) = ? AND beacon_uuid = ? AND beacon_serial_number = ?", barcode, beacon_uuid, beacon_serial_number).first
      end
      p "----- restaurant from beacon data is ==="
      p restaurant
    end

    if restaurant.blank?
      return {:status => false, :notice => "We are sorry! We could not associate your scan to a participating location. Please scan the code located at the store. If error persists, contact app support"} if latitude.to_f == 0 && longitude.to_f == 0
      p "restaurant from beacon data is blank,, so gets it from lat lng...."
      geo_scope_conditions = {:origin => [latitude, longitude]}
      geo_scope_conditions[:within] = chain.code_scan_range if chain.code_scan_range > 0
      p "----- geo_scope_conditions = #{geo_scope_conditions}"
      restaurant = Restaurant.active.geo_scope(geo_scope_conditions)
                       .where("chain_id = ? AND UPPER(location_qrcode_identifier) = ?", self.chain_id, barcode).group(Restaurant.col_list).order("distance asc, restaurants.name asc").first
      p "restaurant from lat lng"
      p restaurant
    end

    return {:status => false, :notice => "We are sorry! We could not associate your scan to a participating location. Please scan the code located at the store. If error persists, contact app support"} if restaurant.blank?
    offer = restaurant.try(:first_active_offer)
    return {:status => false, :notice => "Invalid Offer"} if offer.blank?

    receipt_date = Time.current
    identifier =  [self.id, chain.id, rand(9)].join
    curr_seq = PosCheckUpload.where(:user_id => self.id, :chain_id => chain.id).count
    seq_num = curr_seq + 1
    dummy_receipt_xml = "<check obj_num=\"#{identifier}\" seq_num=\"#{seq_num}\" id=\"#{identifier}\" check-id=\"\"><meal><date>#{receipt_date.strftime("%m/%d/%y %H:%M:%S")}</date><payment-total>0.00</payment-total><tax-total>0.00</tax-total><tip-total>0.00</tip-total><discount-total>0.00</discount-total><sales-total>1.0</sales-total><due-total>1.00</due-total></meal><payment><total>0</total><direct-tips>0</direct-tips><tax>0</tax><item-sales>0</item-sales></payment><menu-items><menu-item id=\"1\"><quantity>1</quantity><name>Restaurant VISIT</name><total-amount>1.00</total-amount></menu-item></menu-items><discounts /><tenders /><service-charges /></check>"
    pos_check_upload = PosCheckUpload.create(
        :chain_id => chain.id,
        :xml_data => dummy_receipt_xml,
        :check_id => identifier,
        :status => PosCheckUpload::STATUS[:NEW],
        :sequence_number =>  seq_num.to_s,
        :restaurant_id => restaurant.id,
        :user_id => self.id,
        :barcode => barcode,
        :pos_used_type => chain.pos_used_type
    )

    survey_id = offer.try(:survey).try(:id)
    receipt = Receipt.new(:chain_id => self.chain_id, :user_id => self.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true, :pos_check_upload_id => pos_check_upload.try(:id))
    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.status = Receipt::STATUS[:RECEIVED]
    receipt.receipt_transactions << receipt_transaction
    receipt.save
    ReceiptBeacon.create(:receipt_id => receipt.id, :beacon_info => beacons, :beacon_uuid => beacon_uuid, :beacon_serial_number => beacon_serial_number ) rescue nil
    Delayed::Job.enqueue(ReceiptCodeJob.new(pos_check_upload, self, restaurant.id, offer.id, receipt.id))
    return {
        status: true,
        message: (ApiResponseText.get_message_chain_by_locale(chain.id, self.locale_id, 7)),
        survey_id: survey_id,
        receipt_id: receipt.id
    }

  end

end