require 'active_record'

module Delayed
  module Backend
    module ActiveRecord
      # A job object that is persisted to the database.
      # Contains the work object as a YAML field.
      class Job < ::ActiveRecord::Base
        unloadable
        include Delayed::Backend::Base
        self.table_name = :delayed_jobs

        before_save :set_default_run_at, :set_polymorphic

        scope :ready_to_run, lambda {|worker_name, max_run_time|
          where(['(run_at <= ? AND (locked_at IS NULL OR locked_at < ?) OR locked_by = ?) AND failed_at IS NULL', db_time_now, db_time_now - max_run_time, worker_name])
        }
        scope :by_priority, order('priority ASC, run_at ASC')

        def self.before_fork
          ::ActiveRecord::Base.clear_all_connections!
        end

        def self.after_fork
          ::ActiveRecord::Base.establish_connection
        end

        # When a worker is exiting, make sure we don't have any locked jobs.
        def self.clear_locks!(worker_name)
          update_all("locked_by = null, locked_at = null", ["locked_by = ?", worker_name])
        end

        # Find a few candidate jobs to run (in case some immediately get locked by others).
        def self.find_available(worker_name, limit = 5, max_run_time = Worker.max_run_time)
          scope = self.ready_to_run(worker_name, max_run_time)
          scope = scope.scoped(:conditions => ['priority >= ?', Worker.min_priority]) if Worker.min_priority
          scope = scope.scoped(:conditions => ['priority <= ?', Worker.max_priority]) if Worker.max_priority

          ::ActiveRecord::Base.silence do
            scope.by_priority.all(:limit => limit)
          end
        end

        # Lock this job for this worker.
        # Returns true if we have the lock, false otherwise.
        def lock_exclusively!(max_run_time, worker)
          now = self.class.db_time_now
          affected_rows = if locked_by != worker
            # We don't own this job so we will update the locked_by name and the locked_at
            self.class.update_all(["locked_at = ?, locked_by = ?", now, worker], ["id = ? and (locked_at is null or locked_at < ?) and (run_at <= ?)", id, (now - max_run_time.to_i), now])
          else
            # We already own this job, this may happen if the job queue crashes.
            # Simply resume and update the locked_at
            self.class.update_all(["locked_at = ?", now], ["id = ? and locked_by = ?", id, worker])
          end
          if affected_rows == 1
            self.locked_at = now
            self.locked_by = worker
            self.locked_at_will_change!
            self.locked_by_will_change!
            return true
          else
            return false
          end
        end

        # Get the current time (GMT or local depending on DB)
        # Note: This does not ping the DB to get the time, so all your clients
        # must have syncronized clocks.
        def self.db_time_now
          if Time.zone
            Time.zone.now
          elsif ::ActiveRecord::Base.default_timezone == :utc
            Time.now.utc
          else
            Time.now
          end
        end

        def set_polymorphic
          begin
            ans = find_delayable
            self.delayable_type = ans.class.to_s
            self.delayable_id = ans.id
            set_chain
          rescue => e
            puts "No relation #{e.inspect}"
          end
        end

        def set_chain
          ans = find_chain
          if ans.class == Chain
            self.chain_id = ans.id
          end
        end

        def find_chain
          ans = find_delayable
          if ans.class == Reward
            return ans.chain
          elsif ans.class == Promotion
            return ans.chain
          end          
        end

        def find_delayable
          # parses the self.handler and creates a has_many style association that returns the related record.          
          if self.handler.include?("!ruby/struct:Delayed::PerformableMethod")
            the_type = self.handler.scan(/ActiveRecord\:(.+)\n/).flatten.first.to_s.downcase.gsub(' ','') rescue nil
            the_value = self.handler.scan(/id\:(.+)\n/).flatten.first.to_s.gsub(/[^0-9]/, "").to_i rescue nil
          else           
            the_type= get_type(self.handler)
            the_value = get_value(self.handler)
          end

          if the_type != nil && the_value != nil            
            return the_type.classify.constantize.find(the_value)
          else
            return nil
          end
        end

        def get_type(handler)
          arr = handler.split(' ')
          arr[2].gsub('_id:','')
        end

        def get_value(handler)
          arr = handler.split(' ')
          arr[3].to_i
        end
      end
    end
  end
end