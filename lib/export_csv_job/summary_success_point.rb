class ExportCsvJob::SummarySuccessPoint < Struct.new(:params, :admin)
  def perform
    puts params
    puts admin
    puts "EXECUTING .... SummarySuccessPoint --- START"
    @push_point = PushPoint.find params['id']
    @logs = @push_point.success_log_entries.select { |a| a[(params[:search_type] || "user_id").to_sym].to_s =~ /#{params[:key]}/ }.
        paginate(:page => params[:page], :per_page => 100)
    total_pages = @logs.total_pages
    path = "#{Rails.root}/tmp/push_point_summary_#{@push_point.id}.csv"
    File.new(path, 'w')

    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      puts "asup coooy"
      csv << ["User ID", "Device Notification", "Email", "Point", "Created at"]

      # data rows
      1.upto(total_pages) do |x|
        logs = @push_point.success_log_entries.select { |a|
          a[(params['search_type'] || "user_id").
                to_sym].to_s =~ /#{params['key']}/ }.paginate(:page => x, :per_page => 100)

        logs.each do |log|
          crt_at = log[:created_at].in_time_zone(Time.now.zone) rescue "-"
          user = User.find(log[:user_id]).email rescue ""
          csv << [
              log[:user_id],
              user,
              log[:notification],
              log[:email],
              log[:point],
              crt_at
          ]
        end
      end
    end
    OwnerMailer.send_reward_redeemed(nil, admin.email, path, "Push Point Summary #{@push_point.id}.csv", "Push Point Summary #{@push_point.id}").deliver!
    File.delete(path)
    puts "EXECUTING .... SummarySuccessPoint --- END"

  end
end