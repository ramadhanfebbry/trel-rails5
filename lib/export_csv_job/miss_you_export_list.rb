class ExportCsvJob::MissYouExportList < Struct.new(:job_id, :error,:email)
  def perform
    @job = OwnerJob.find job_id
    path = "#{Rails.root}/tmp/miss_you_export.#{job_id}.csv"
    File.new(path, 'w')

    if error.blank?
      total_pages = @job.ownerjob_logs.success.paginate(:page => 1, :per_page => 1000).total_pages
    else
      total_pages = @job.ownerjob_logs.failed.paginate(:page => 1, :per_page => 1000).total_pages
    end

    transaction_csv = CSV.open(path, "wb") do |csv|
      csv << ["id", "Email", "Date Sent", "Notification"]

      1.upto(total_pages) do |i|
        if error.blank?
          logs = @job.ownerjob_logs.success.paginate(:page => i, :per_page => 1000)
        else
          logs = @job.ownerjob_logs.failed.paginate(:page => i, :per_page => 1000)
        end

        logs.each do |log|
          csv << [
              log.user_id,
              log.email,
              log.date_sent,
              (OwnerjobLog::NOTIFICATION_STATUS.key(log.notification_status) rescue "-")
          ]
        end
      end
    end
    OwnerMailer.send_reward_redeemed(nil, email, path, "list_miss_you_job_#{@job.chain.name}_#{@job.id}.csv","Miss You job EXPORT").deliver!
    File.delete(path)
    puts "MissYouExportList"
  end
end