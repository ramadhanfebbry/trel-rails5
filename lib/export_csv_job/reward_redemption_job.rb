class ExportCsvJob::RewardRedemptionJob < Struct.new(:reward_id,:user_email)

  def perform
    #user_email = "inoe.bainur@gmail.com"
    #reward_id = 684
    reward = Reward.find reward_id
    path = "#{Rails.root}/tmp/#{reward.name}_claim_history.csv"
    #transactions = RewardTransaction.where("reward_id = ?", reward_id).order("id DESC")
    transactions = RewardTransaction.select("
          distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
reward_transactions.restaurant_id,
 reward_transactions.user_id)  reward_transactions.*").where("reward_id  = ? and redeeming IS FALSE", reward_id).
        order("date(reward_transactions.created_at) DESC")
    File.new(path,'w')
    transaction_csv = CSV.open(path,"wb") do |csv|
      # header row
      csv << [
          "Claimed at",
          "User ID",
          "Email",
          "Lat",
          "Long",
          "Restaurant_id"
      ]

      # data rows
      transactions.each do |transaction|
        user = User.find(transaction.user_id) rescue ""
        csv << [
            transaction.created_at.in_time_zone('EST').strftime("%Y-%m-%d %H:%M"),
            (user.id rescue ''),
            (user.email rescue ''),
            transaction.latitude ,
            transaction.longitude ,
            transaction.restaurant_id
        ]
      end
    end


    RewardMailer.reward_transaction_files(reward,user_email, path, reward.chain,"#{reward.name} CLAIM HISTORY",
                                          'Here are the request').deliver!
    File.delete(path)
  end
end