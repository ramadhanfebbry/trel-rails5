class ExportCsvJob::ReceiptsOnlineJob < Struct.new(:params, :admin, :from_index)
  def perform
    if from_index != true
      q = params[:search][:q]
      q = q.gsub("'", "''")
      params[:search][:q] = q

      ar_field = [
          "receipts.id",
          "offers.name",
          "users.email",
          "chains.name",
          "restaurants.name",
          "receipt_transactions.receipt_number",
          "receipts.created_at",
          "receipts.status"
      ]

      order = params[:qry_order]

      if order.to_i < 1000
        qry_order = "#{ar_field[order.to_i]}"
        @receipts = Receipt.receipt_order_online.custom_search_online_orders(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order).reverse_order

        qry_order = qry_order + " desc"
      elsif order.to_i > 999
        order = order.to_i - 1000
        qry_order = "#{ar_field[order]}"

        @receipts = Receipt.receipt_order_online.custom_search_online_orders(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order)

      else
        qry_order = 'receipt_transactions.receipt_id desc'

        @receipts = Receipt.receipt_order_online.custom_search_online_orders(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order)

      end
      page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
      count = Receipt.receipt_order_online.custom_search_online_orders(params[:search]).where("chains.status = 'active'").joins(:chain).count(:distinct => true)


      @receipts = @receipts.paginate(:page => page, :per_page => Setting.pagination.per_page, :total_entries => count)
      @action = '-search'
    end
    if from_index == true
        count = Receipt.active_chain.receipt_order_online.desc_list.count(:distinct => true)

    end
    path = "#{Rails.root}/tmp/receipts_export.csv"
    loop = (count.to_f / 500.to_f).ceil
    File.new(path, 'w')

    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      csv << ["Receipt Id", "User Name",
              "User Email", "Chain",
              "Restaurant Location",
              "Points Multiplier", "Receipt Number/Order Id",
              "Submit Date", "Review status"]

      1.upto(loop) do |page|
        if from_index != true

            @receipts = Receipt.receipt_order_online.custom_search_online_orders(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order).paginate(:page => page, :per_page => 500)

        else
            @receipts = Receipt.active_chain.receipt_order_online.desc_list.paginate(:page => page, :per_page => 500)
        end
        @receipts.each do |receipt|
          # data rows
          trans=receipt.last_transaction
          csv << [
              receipt.id,
              receipt.try(:user).try(:full_name).try(:strip),
              receipt.try(:user).try(:email).try(:strip),
              receipt.try(:chain).try(:name).try(:strip),
              trans.try(:restaurant_offer).try(:restaurant).try(:name).try(:strip),
              trans.try(:restaurant_offer).try(:offer).try(:multiplier),
              trans.try(:receipt_number),
              receipt.try(:created_at).try(:strftime, '%m-%d-%Y'),
              receipt_status_name(receipt.status, admin)
          ]
        end
      end
    end
    OwnerMailer.send_reward_redeemed(nil, admin.email, path, "receipts_online_orders_export.csv", "admin receipts Online export").deliver!
    File.delete(path)
    puts "Success EXECUTING OwnerJob::ReceiptsJob"
  end


  def receipt_status_name(status, current_admin)
    case status
      when Receipt::STATUS[:APPROVED]
        'Approved'
      when Receipt::STATUS[:REJECTED]
        'Rejected'
      when Receipt::STATUS[:RECEIVED]
        'Received'
      when Receipt::STATUS[:OCRED]
        if current_admin
          'OCRed'
        elsif current_user
          'Received'
        end
      when Receipt::STATUS[:PENDING_OCR]
        "Pending OCR"
      else
        'Unknown'
    end
  end
end
