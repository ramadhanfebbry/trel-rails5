class ExportCsvJob::SocialParticipant < Struct.new(:params, :admin,:social)
   def perform
     path = "#{Rails.root}/tmp/social_share_user.csv"
     search  = params['search'].blank?? nil : params['search']
     @participants = social.social_share_participants.search(search)#.paginate(page: params[:page], per_page: 50)

     transaction_csv = CSV.open(path,"wb") do |csv|
       # header row
       csv << [
           "User id",
           "Email",
           "Share Date/Time",
           "Medium",
           "Got Incentive",
           "Message"
       ]

       # data rows
       @participants.each do |part|
         receive = part.received == true ? "YES" : "NO"
         csv << [
             part.user_id,
             part.email,
             part.created_at.strftime("%Y-%m-%d %H:%M"),
             part.medium,
             receive,
             part.log
         ]
       end
     end

     OwnerMailer.send_reward_redeemed(nil, admin.email, "tmp/social_share_user.csv", "social_share_user.csv","Social Share Participants").deliver!
     File.delete("tmp/social_share_user.csv")
     puts "Success EXECUTING EXPORTCSVJOB::SOCIALPARTICIPANT"

   end
end