class ExportCsvJob::GyuKakuUserIdCheckJob < Struct.new(:chain_id, :start_date, :end_date, :emails)

  def perform
    #  chain_id = 19
    # # chain = Chain.find 19
    #  emails = ["mbainurb@gmail.com"]
    #  start_date = '2015-07-27'
    #  end_date = '2015-07-28'
    chain = Chain.find(chain_id)
    path = "#{Rails.root}/tmp/user_gyu_kaku_est.csv"
    total_pages = User.where("chain_id = ? AND active = ?", chain_id, true).order("created_at asc").
        paginate(:page => 1, :per_page => 500).total_pages


    transaction_csv = CSV.open(path, "wb") do |csv|
      csv << [
          "User id",
          "Email",
          "Check ID",
          "Store Name",
          "Points Awarded",
          "Subtotal",
          "Transaction Date",
          "Transaction Time",
          "Receipt Status"
      ]

      1.upto(total_pages).each do |i|
        users = User.where("chain_id = ? AND active = ?", chain_id, true).order("created_at asc").
            paginate(:page => i, :per_page => 500)

        users.each do |user|
          receipts = user.receipts.where("date(receipts.created_at) >= ? AND date(receipts.created_at) <= ? AND receipts.status = ?",
                                         start_date, end_date, Receipt::STATUS[:APPROVED])
          #puts receipts.size
          receipts.each do |receipt|
            json_data = Receipt.find_by_sql("select json_data from ncr_data_receipts where id = #{receipt.ncr_data_receipt_id}").first.json_data rescue nil
            json_encode = json_data.gsub("=>",":").gsub("nil", "0") rescue nil
            receipt_data_in_json = ActiveSupport::JSON.decode(json_encode) rescue nil

            last_trans = receipt.last_transaction
            iss_date =  (last_trans.issue_date.blank? ? last_trans.created_at.in_time_zone('EST').strftime("%Y-%m-%d") : last_trans.issue_date.utc.strftime("%Y-%m-%d") )
            iss_time = (last_trans.created_at.in_time_zone('EST').strftime('%I:%M %p') rescue last_trans.created_at.strftime('%H:%M %p'))
            csv << [
                user.id,
                user.email,
                (receipt_data_in_json["CheckId"] rescue nil),
                (last_trans.restaurant.name rescue '-'),
                last_trans.total_points_earned.to_i.to_s,
                last_trans.subtotal,
                # change it, with the data from json data
                #(last_trans.issue_date.blank? ? last_trans.created_at.in_time_zone('EST').strftime("%Y-%m-%d") : last_trans.issue_date.utc.strftime("%Y-%m-%d") ),
                #(last_trans.created_at.in_time_zone('EST').strftime('%I:%M %p') rescue last_trans.created_at.strftime('%H:%M %p')),
                (receipt_data_in_json["OpenTime"].to_datetime.strftime("%Y-%m-%d") rescue iss_date),
                (receipt_data_in_json["OpenTime"].to_datetime.strftime("%I:%M %p") rescue iss_time),
                Receipt::STATUS.key(last_trans.status).to_s
            ]
          end
        end
      end
    end

    file_name_est = " #{chain.name}_users_collection_#{start_date}-#{end_date}_EST.csv".gsub('UTC_','')
    emails.each do |email|
      OwnerMailer.send_reward_redeemed(nil,email , path,file_name_est,
                                       "User id #{chain.name} ON EST").deliver!
    end
  end
end