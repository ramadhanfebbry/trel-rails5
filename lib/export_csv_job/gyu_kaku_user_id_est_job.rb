class ExportCsvJob::GyuKakuUserIdEstJob < Struct.new(:chain_id, :start_date, :end_date, :emails)

  def perform
    #  chain_id = 19
    # # chain = Chain.find 19
    #  emails = ["mbainurb@gmail.com"]
    #  start_date = '2015-07-27'
    #  end_date = '2015-07-28'
    chain = Chain.find(chain_id)
    path = "#{Rails.root}/tmp/user_gyu_kaku_est.csv"
    total_pages = User.where("chain_id = ? AND active = ?", chain_id, true).order("created_at asc").
        paginate(:page => 1, :per_page => 500).total_pages


    transaction_csv = CSV.open(path, "wb") do |csv|
      csv << [
          "User id",
          "Email",
          "Store Name",
          "Points Awarded",
          "Subtotal",
          "Transaction Date",
          "Transaction Time",
          "Receipt Status"
      ]

      1.upto(total_pages).each do |i|
        users = User.where("chain_id = ? AND active = ?", chain_id, true).order("created_at asc").
            paginate(:page => i, :per_page => 500)

        users.each do |user|
          receipts = user.receipts.where("date(receipts.created_at) >= ? AND date(receipts.created_at) <= ? AND receipts.status = ?",
                                         start_date, end_date, Receipt::STATUS[:APPROVED])
          #puts receipts.size
          receipts.each do |receipt|

            last_trans = receipt.last_transaction
            csv << [
                user.id,
                user.email,
                (last_trans.restaurant.name rescue '-'),
                last_trans.total_points_earned.to_i.to_s,
                last_trans.subtotal,
                (last_trans.issue_date.blank? ? last_trans.created_at.in_time_zone('EST').strftime("%Y-%m-%d") : last_trans.issue_date.utc.strftime("%Y-%m-%d") ),
                (last_trans.created_at.in_time_zone('EST').strftime('%I:%M %p') rescue last_trans.created_at.strftime('%H:%M %p')),
                Receipt::STATUS.key(last_trans.status).to_s
            ]
          end
        end
      end
    end

    file_name_est = " #{chain.name}_users_collection_#{start_date}-#{end_date}_EST.csv".gsub('UTC_','')
    emails.each do |email|
      OwnerMailer.send_reward_redeemed(nil,email , path,file_name_est,
                                       "User id #{chain.name} ON EST").deliver!
    end
  end
end