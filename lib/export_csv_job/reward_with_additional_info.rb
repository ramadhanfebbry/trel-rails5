class ExportCsvJob::RewardWithAdditionalInfo < Struct.new(:reward_id, :start_date, :end_date,:emails)

  def perform
    #emails = ["mbainurb@gmail.com"]
    reward = Reward.find reward_id
    path = "#{Rails.root}/tmp/#{reward.name}_claim_history.csv"
    transactions = RewardTransaction.where("reward_id = ?", reward_id).order("id DESC")
    File.new(path,'w')
    transaction_csv = CSV.open(path,"wb") do |csv|
      # header row
      #additional_header = transactions.first.details.first.
      #additional_header = RewardInformation.where(:reward_id => reward_id).map(&:app_text)
      additional_header = RewardInformation.where(:reward_id => reward_id).order('id asc').map(&:app_text)
      csv << [
          "Claimed at",
          "Email",
          "Name"
      ] + additional_header

      # data rows
      transactions.each do |transaction|
        begin
        puts additional_information(transaction)
        user = User.find(transaction.user_id) rescue ""
        a_row = additional_information(transaction) rescue []
        puts a_row
        csv << [
            transaction.created_at.to_date,
            (user.email rescue ''),
            (user.first_name + user.last_name rescue '')
        ] +  additional_information(transaction)
          #puts
        rescue => e
          puts "#{e.inspect}"
          next
          end
      end
    end


    emails.each do |email|
      RewardMailer.reward_transaction_files(reward,email, path, reward.chain,"#{reward.name} CLAIM HISTORY",
                                            'Here are the request').deliver!
    end

    File.delete(path)
  end

  def additional_information(transaction)
    result = []
    unless transaction.details.blank?
      transaction.details.joins(:reward_information).order('reward_informations.id asc').each do |det|
        if det.answer.blank?
        result <<   det.description
        else
          result << det.answer
      end
      end
    end
    return result.flatten
  end
end