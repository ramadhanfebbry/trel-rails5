class ExportCsvJob::ReceiptsStatusJob < Struct.new(:params, :admin, :from_index, :receipt_type)
  def perform
    ar_field = [
        "receipts.id",
        "receipts.status",
        "receipts.created_at"
    ]

    qry_order = 'receipt_transactions.receipt_id desc'
      if receipt_type.eql?("barcode")
        @receipts = Receipt.receipt_barcode.custom_search_with_barcode(params[:export_status]).where("chains.status = 'active'").joins(:chain).order(qry_order)
      else
        @receipts = Receipt.receipt_image.custom_search(params[:export_status]).where("chains.status = 'active'").joins(:chain).order(qry_order)
      end

     @action = '-search'

    path = "#{Rails.root}/tmp/receipts_status_export_#{receipt_type}.csv"
    File.new(path, 'w')

    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      csv << ["Receipt Id", "Review status", "Submit Date"]

      @receipts.each do |receipt|
        # data rows
        if receipt.status == 3 || receipt.status == 4
          csv << [
              receipt.id,
              receipt_status_name(receipt.status,admin),
              receipt.try(:created_at).try(:strftime, '%m-%d-%Y')
          ]
        end
      end

    end
    OwnerMailer.send_reward_redeemed(nil, admin.email, path, "receipts_status_export_#{receipt_type}.csv","Admin Receipt Status Export").deliver!
    File.delete(path)
    puts "Success EXECUTING OwnerJob::ReceiptsStatusJob"
  end


  def receipt_status_name(status,current_admin)
    case status
      when Receipt::STATUS[:APPROVED]
        'Approved'
      when Receipt::STATUS[:REJECTED]
        'Rejected'
      when Receipt::STATUS[:RECEIVED]
        'Received'
      when Receipt::STATUS[:OCRED]
        if current_admin
          'OCRed'
        elsif current_user
          'Received'
        end
      when Receipt::STATUS[:PENDING_OCR]
        "Pending OCR"
      else
        'Unknown'
    end
  end
end
