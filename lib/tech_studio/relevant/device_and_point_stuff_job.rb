module TechStudio
  module Relevant
    class DeviceAndPointStuffJob < Struct.new(:user_id, :params, :tech_user_id)
      include RelevantAuthentication

      def perform
        user = User.find(user_id)
        tech_studio_user = TechStudio::Models::User.find(tech_user_id)
        chain = user.chain
        #execute real time sync fishbowl
        real_type_sync = chain.fishbowl_setting.real_time_sync rescue false
        user_fb_direct_sync = chain.add_to_fishbowl_user(user, real_type_sync) if chain.fishbowl_setting

        set_device_token_as_unique_for_user(user, (params[:sign_in_device_type] || params[:register_device_type]), params[:device_token], params[:device_id])

        #user device stuff
        #check first what device type used
        device_type_selected = (params[:sign_in_device_type] || params[:register_device_type])
        if device_type_selected.downcase.eql?("android")
          key_checked = params[:android_id]
        elsif device_type_selected.downcase.eql?("iphone")
          key_checked = params[:keychain]
        end

        p "device detected ---"
        p device_type_selected
        p key_checked
        unique_signed_up_user = UserDeviceLog.check_unique_signup_user(user.chain_id, key_checked, device_type_selected)
        p "per uniq uniq kan"
        p unique_signed_up_user
        p user.chain.required_unique_device_info
        UserDeviceLog.add_log(user, params[:android_id], params[:keychain], (params[:sign_in_device_type] || params[:register_device_type]), "Technology STUDIO migration", Time.current)

        #track the device info
        if key_checked.blank?
          user.update_columns(:signup_device_status => 3, :selected_device_info => key_checked)
        elsif unique_signed_up_user
          user.update_columns(:signup_device_status => 1, :selected_device_info => key_checked)
        else
          user.update_columns(:signup_device_status => 2, :selected_device_info => key_checked)
        end

        user_points = tech_studio_user.points.to_i
        user.earn(user_points)
        PointHistory.add_to_history(user.id, "Migration point", tech_studio_user.points)
      end
    end
  end
end
