module TechStudio
  module Relevant
     module Connect

       def migrate_tech_user_as_relevant(params)
         tech_studio_user = TechStudio::Models::User.where(:email => params[:email].downcase, :user_id => nil).first
         if tech_studio_user
           user = User.new(params[:user])
           user.first_name = tech_studio_user.first_name
           user.last_name = tech_studio_user.last_name
           user.name = [user.first_name, user.last_name].join(" ")
           user.set_birthday(tech_studio_user.birthday)
           user.password = user.random_password if user.password.blank?
           user.zipcode = tech_studio_user.zip unless tech_studio_user.zip.blank?
           user.marketing_optin = tech_studio_user.optin || 0
           # user.gender = tech_studio_user.gender
           user.created_at = tech_studio_user.signup_date || Time.zone.now
           user.phone_number = params[:phone_number] unless params[:phone_number].blank?
           user.special_occassion = params[:special_occassion] unless params[:special_occassion].blank?
           if user.locale_id.blank?
             user.locale_id = user.try(:chain).try(:locales).try(:first).try(:id) || Locale.find_by_key('en').try(:id)
           end
           user.user_profile = UserProfile.new(:tech_studio_guest_identifier => tech_studio_user.account_key)
           #should join to table favorites to get the text only, not the ids
           p "user from guest dna---"
           p user
           if user.save
             tech_studio_user.update_attributes(:user_id => user.id, :migration_date => Time.zone.now, :migrated => true)
             Delayed::Job.enqueue(TechStudio::Relevant::DeviceAndPointStuffJob.new(user.id, params, tech_studio_user.id))
             user.update_phone_info(params[:phone_model], params[:os], params[:appkey])
             if params[:register_type].eql?("1")
               pass = user.random_password
               chk = user.update_attributes(:password => pass, :forgot_password => true)
               if (chk)
                 return {:status => true, :notice => "A new password has been emailed to you as a security measure. Please login to access your account. We hope you enjoy the new app!", :nav_main_page => true}
               else
                 p "error updating password process. Tech Studio User"
                 p user.errors
                 return {:status => false, :notice => "We are sorry. We could not complete your request. Please try again. If error persists, reach out to our support team from the app."}
               end
             else
               return "CONT"
             end
           else
             p "error on creating user on Guest DNA"
             p user.errors
             if user.errors.messages[:phone_number].blank?
               return {:status => false, :notice => "We are sorry. We could not complete your request. Please try again. If error persists, reach out to our support team from the app."}
             else
               return {:status => false, :notice => "Please enter a valid 10 digit phone number. If your phone number was used before, please try with your secondary phone number."}
             end

           end
         else
           return  "CONT"
         end
       end

     end
  end
end

