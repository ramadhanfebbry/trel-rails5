module Micros

  class Check
    def self.update_check_with_discount(xml_receipt, discount_amount)
      if xml_receipt.class.to_s != "String"
        json_check = xml_receipt
        total = json_check["Total"].to_s
        json_check["Total"] = (BigDecimal.new(total) - discount_amount).to_f

        subtotal = json_check["Subtotal"].to_s
        json_check["Subtotal"] = (BigDecimal.new(subtotal) - discount_amount).to_f

        discount = json_check["Discount"].to_s
        json_check["Discount"] = (BigDecimal.new(discount) + discount_amount).to_f
        json_check
      else
        @pos_xml = PosReceiptXml.new(xml_receipt)
        sales_total = @pos_xml.sales_total.to_s
        tax_total = @pos_xml.tax.to_s
        updated_payment_total = ((BigDecimal.new(sales_total) + BigDecimal.new(tax_total)) - discount_amount).to_s
        xml_receipt.gsub!("<payment-total>0.00</payment-total>", "<payment-total>#{updated_payment_total}</payment-total>")
        xml_receipt.gsub!("<discount-total>0.00</discount-total>", "<discount-total>#{discount_amount.to_s}</discount-total>")
        xml_receipt.gsub!("<discounts />", "<discounts><discount id=\"208\"><name>Relevant Dsc</name><dollar-value>-#{discount_amount}</dollar-value><reference>6492.17859</reference></discount></discounts>")
        xml_receipt
      end
    end
  end
end
