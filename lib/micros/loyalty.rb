module Micros

  class Loyalty
    def self.execute(chain, pos_location, user, user_session, user_code_user, location, xml_receipt, check_id, sequence_number, revenue_center, payment_included, code_type, barcode, employee_num, tbl_num)
      p "check_id is #{check_id}"
      p "sequence_number is #{sequence_number}"
      p "revenue center is #{revenue_center}"
      #p "micros barcode = #{micros_barcode}"

      return {:confirm => 0, :response_code => 2} if check_id.blank?
      return {:confirm => 0, :response_code => 3} if sequence_number.blank?

      time_current = Time.current
      pos_check_upload = PosCheckUpload.where(:pos_location_id => pos_location.id, :sequence_number => sequence_number, :check_id => check_id, :created_at => time_current.beginning_of_day..time_current.end_of_day).first

      if pos_check_upload.blank?
        pos_check_upload = PosCheckUpload.create(
            :pos_location_id => pos_location.id,
            :chain_id => pos_location.chain_id,
            :xml_data => xml_receipt,
            :check_id => check_id,
            #:barcode =>  micros_barcode,
            :user_code => barcode,
            :status => PosCheckUpload::STATUS[:NEW],
            :sequence_number =>  sequence_number,
            :user_id => (user.try(:id)),
            :service => PosCheckUpload::SERVICE[:COUNTER],
            :payment_included => payment_included,
            :code_type => code_type,
            :rvc_num => revenue_center,
            :emp_num => employee_num,
            :tbl_num => tbl_num
        )
      elsif pos_check_upload.status.eql?(3)
        if code_type.eql?("paycode")
          pos_check_upload.update_attributes(:pos_location_id => pos_location.id,
                                             :chain_id => pos_location.chain_id,
                                             :xml_data => xml_receipt,
                                             :check_id => check_id,
                                             :user_code => barcode,
                                             :sequence_number =>  sequence_number,
                                             :user_id => (user.try(:id)),
                                             :service => PosCheckUpload::SERVICE[:COUNTER],
                                             :payment_included => payment_included,
                                             :code_type => code_type,
                                             :rvc_num => revenue_center,
                                             :emp_num => employee_num,
                                             :tbl_num => tbl_num
          )
          if code_type.eql?("paycode") && user_session && user_session.active == false
            pos_check_upload.update_column(:error_code, 10)
            return {:confirm => 0, :response_code => 10}
          end
          return {confirm: 1, response_code: 0}
        else
          return {confirm: 0, response_code: 4}
        end
      else
        pos_check_upload.update_attributes(:pos_location_id => pos_location.id,
                                           :chain_id => pos_location.chain_id,
                                           :xml_data => xml_receipt,
                                           :check_id => check_id,
                                           :user_code => barcode,
                                           :sequence_number =>  sequence_number,
                                           :status => PosCheckUpload::STATUS[:UPDATED],
                                           :user_id => (user.try(:id)),
                                           :service => PosCheckUpload::SERVICE[:COUNTER],
                                           :payment_included => payment_included,
                                           :code_type => code_type,
                                           :rvc_num => revenue_center,
                                           :emp_num => employee_num,
                                           :tbl_num => tbl_num
        )
      end
      #------UPLOAD BARCODE USER PROCESS -----------------

      if user_session && user_session.expired_at && (user_session.expired_at < Time.zone.now)
        pos_check_upload.update_column(:error_code, 10)
        return {:confirm => 0, :response_code => 10}
      end

      if user_code_user && user_code_user.expired_at && (user_code_user.expired_at < Time.zone.now)
        pos_check_upload.update_column(:error_code, 10)
        return {:confirm => 0, :response_code => 10}
      end

      if code_type.eql?("paycode") && user_session && user_session.active == false
        pos_check_upload.update_column(:error_code, 10)
        return {:confirm => 0, :response_code => 10}
      end

      if user.blank?
        pos_check_upload.update_column(:error_code, 11)
        return {:confirm => 0, :response_code => 11}
      end

      if chain.id != user.chain_id
        pos_check_upload.update_attributes(:error_code => 12, :user_id => nil)
        return {confirm: 0, response_code: 5}
      end

      offer = location.first_active_offer
      if (offer.blank?)
        return {confirm: 0, response_code: 6}
      else
        return {confirm: 0, response_code: 7} unless chain.offers.include?(offer)
        valid_pos_check = true
        pos_location = pos_location
        if valid_pos_check
          receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true, :pos_check_upload_id => pos_check_upload.id)
          receipt_transaction = ReceiptTransaction.new
          receipt_transaction.status = Receipt::STATUS[:RECEIVED]
          receipt.receipt_transactions << receipt_transaction
          if receipt.save
            receipt_id = receipt.id
            Delayed::Job.enqueue(Micros::PosReceiptParseJob.new(pos_check_upload.id, user.id, pos_location.restaurant_id, offer.id, receipt_id), :run_at => Time.zone.now + chain.pos_receipt_processing_delay.minutes)
            updated_attributes = {:executed => true, :receipt_id => receipt.id, :active => false}
            updated_attributes.merge!(:mp_processed => true) if code_type.eql?("paycode")
            user_session.update_attributes(updated_attributes) if user_session
            user_code_user.destroy if user_code_user
            return {:confirm => 1, :response_code => 0}
          end
        else
          return {confirm: 0, response_code: 9}
        end
      end
    end
  end

end