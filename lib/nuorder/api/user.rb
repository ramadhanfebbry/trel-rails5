require "net/http"
require "uri"

module Nuorder
  module Api
    class User
      attr_accessor :api_root, :api_key, :client_id, :secret_id

      def initialize(nuorder_connect_setting)
        @api_root = nuorder_connect_setting["api_root"]
        @requester_id = nuorder_connect_setting["requester_id"]
        @license_code = nuorder_connect_setting["license_code"]
        @passcode = nuorder_connect_setting["passcode"]
        @subscriber_id = nuorder_connect_setting["subscriber_id"]
        @location_id = nuorder_connect_setting["location_id"]
      end

      def create(params)
        p "NuOrder API REGISTRATION #{params[:email]}"
        begin
          data = {"patron" => {
              "login" => params[:email],
              "password" => params[:password],
              "first_name" => params[:first_name],
              "last_name" => params[:last_name],
              "phone1" => params[:phone_number],
              "hint" => params[:password_hint]
          }}
          p uri = URI.parse("#{@api_root}/service/patrons.json")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = "#{@api_root}".include?("https")
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request["Content-Type"] = "application/json; charset=utf-8"
          request["REQUESTERID"] = @requester_id
          request["LICENSECODE"] = @license_code
          request["PASSCODE"] = @passcode
          request["SUBSCRIBERID"] = @subscriber_id
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            p "RESPONSE NuOrder API REGISTRATION FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body)
            unless response_json["patron"].blank?
              if params[:referral_code].present?
                message = "Your account has been created and referral code has been submitted."
              else
                message = "Your account has been created."
              end
              return [true, message, response_json]
            else
              return [false, "Bad Response", nil]
            end
          else
            p "RESPONSE NuOrder API REGISTRATION FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body)
            return [false, "#{response_json["service_response"]["message"]}", nil]
          end
        rescue => e
          return [false, e.message, nil]
        end
      end

      def authenticate(params)
        p "NuOrder API LOGIN #{params[:email]}"
        begin
          data = {
              "email" => params[:email],
              "password" => params[:password]
          }
          p uri = URI.parse("#{@api_root}/service/authenticate.json")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = "#{@api_root}".include?("https")
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request["Content-Type"] = "application/json; charset=utf-8"
          request["REQUESTERID"] = @requester_id
          request["LICENSECODE"] = @license_code
          request["PASSCODE"] = @passcode
          request["SUBSCRIBERID"] = @subscriber_id
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            p "RESPONSE NuOrder API LOGIN FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body)
            unless response_json["service_response"]["patron_id"].blank?
              return [true, "Welcome back!", response_json]
            else
              return [false, "Bad Response", response_json]
            end
          else
            p "RESPONSE NuOrder API LOGIN FOR #{params[:email]} - #{response.body}"
            response_json = JSON.parse(response.body)
            return [false, "#{response_json["service_response"]["message"]}", response_json]
          end
        rescue => e
          return [false, e.message, nil]
        end
      end

      def get_profile(patron_id)
        p "NuOrder API Get PROFILE"
        begin
          p uri = URI.parse("#{@api_root}/service/patrons/#{patron_id}.json")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = "#{@api_root}".include?("https")
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Get.new(uri.request_uri)
          request["Content-Type"] = "application/json; charset=utf-8"
          request["REQUESTERID"] = @requester_id
          request["LICENSECODE"] = @license_code
          request["PASSCODE"] = @passcode
          request["SUBSCRIBERID"] = @subscriber_id
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            p "RESPONSE NuOrder API GET PROFILE FOR #{patron_id} - #{response.body}"
            response_json = JSON.parse(response.body)
            unless response_json["patron"].blank?
              return [true, response_json]
            else
              return [false, "Bad Response"]
            end
          else
            p "RESPONSE NuOrder API GET PROFILE FOR #{patron_id} - #{response.body}"
            response_json = JSON.parse(response.body)
            return [false, response_json]
          end
        rescue => e
          return [false, e.message]
        end
      end

      def search_profile(params)
        p "NuOrder API SEARCH PROFILE #{params[:patron_id]}"
        begin
          data = {}
          data["patron_id"] = params[:patron_id] if params[:patron_id].present?
          data["patron_phone"] = params[:phone_number] if params[:phone_number].present?
          data["patron_email"] = params[:email] if params[:email].present?
          data["patron_loyalty_number"] = params[:loyalty_number_id] if params[:loyalty_number_id].present?

          p uri = URI.parse("#{@api_root}/service/patrons/search.json")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = "#{@api_root}".include?("https")
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Get.new(uri.request_uri)
          request["Content-Type"] = "application/json; charset=utf-8"
          request["REQUESTERID"] = @requester_id
          request["LICENSECODE"] = @license_code
          request["PASSCODE"] = @passcode
          request["SUBSCRIBERID"] = @subscriber_id
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            p "RESPONSE NuOrder API SEARCH PROFILE FOR #{params}"
            response_json = JSON.parse(response.body)
            unless response_json["patron"].blank?
              return [true, response_json]
            else
              return [false, "Bad Response"]
            end
          else
            p "RESPONSE NuOrder API SEARCH PROFILE FOR #{params}"
            response_json = JSON.parse(response.body)
            return [false, response_json]
          end
        rescue => e
          return [false, e.message]
        end
      end

      def forgot_password(params)
        p "NuOrder API FORGOT PASSWORD #{params[:patron_id]}"
        begin
          data = {
              "email" => params[:email]
          }
          p uri = URI.parse("#{@api_root}/service/reset.json")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = "#{@api_root}".include?("https")
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request["Content-Type"] = "application/json; charset=utf-8"
          request["REQUESTERID"] = @requester_id
          request["LICENSECODE"] = @license_code
          request["PASSCODE"] = @passcode
          request["SUBSCRIBERID"] = @subscriber_id
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            p "RESPONSE NuOrder API FORGOT PASSWORD FOR #{params}"
            return [true, "An email with password reset link has been sent. Please check your inbox in sometime and follow the instructions to reset your password. If you have any concerns, please contact support."]
          else
            p "RESPONSE NuOrder API FORGOT PASSWORD FOR #{params}"
            return [false, "We are sorry! We could not process your request. Please check your input and try again. If you have any concerns, please contact support."]
          end
        rescue => e
          return [false, e.message]
        end
      end

      def update_password(patron_id, params)
        p "NuOrder API UPDATE PASSWORD #{params[:email]}"
        begin
          data = { :patron => { "password" => params[:new_password] }}
          p uri = URI.parse("#{@api_root}/service/patrons/#{patron_id}.json")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = "#{@api_root}".include?("https")
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Put.new(uri.request_uri)
          request["Content-Type"] = "application/json; charset=utf-8"
          request["REQUESTERID"] = @requester_id
          request["LICENSECODE"] = @license_code
          request["PASSCODE"] = @passcode
          request["SUBSCRIBERID"] = @subscriber_id
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            p "RESPONSE NuOrder API UPDATE PASSWORD FOR #{params}"
            return [true, "Your password has been updated."]
          else
            p "RESPONSE NuOrder API UPDATE PASSWORD FOR #{params}"
            return [false, "We are sorry! We could not process your request. Please check your current password and ensure the new password matches the minimum requirement."]
          end
        rescue => e
          return [false, e.message]
        end
      end

      def update_profile(patron_id, params)
        p "NuOrder API UPDATE PROFILE #{params}"
        begin
          data = { "patron" => {}}
          data["patron"]["login"] = params[:email] if params[:email].present?
          data["patron"]["first_name"] = params[:first_name] if params[:first_name].present?
          data["patron"]["last_name"] = params[:last_name] if params[:last_name].present?
          data["patron"]["phone"] = params[:phone_number]
          data["patron"]["hint"] = params[:password_hint]
          p uri = URI.parse("#{@api_root}/service/patrons/#{patron_id}.json")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = "#{@api_root}".include?("https")
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Put.new(uri.request_uri)
          request["Content-Type"] = "application/json; charset=utf-8"
          request["REQUESTERID"] = @requester_id
          request["LICENSECODE"] = @license_code
          request["PASSCODE"] = @passcode
          request["SUBSCRIBERID"] = @subscriber_id
          request.body = data.to_json
          response = http.request(request)
          p "RESPONSE: #{response}"
          if response.is_a?(Net::HTTPOK)
            p "RESPONSE NuOrder API UPDATE PROFILE FOR #{params}"
            return [true, "Your profile information has been updated."]
          else
            p "RESPONSE NuOrder API UPDATE PROFILE FOR #{params}"
            return [false, "We are sorry! We could not process your request. Please check the data and try again."]
          end
        rescue => e
          return [false, e.message]
        end
      end

    end
  end
end

params = {"appkey"=>"bILhhyjUH4m12a8j", "auth_token"=>"ADHWYbbTueLq8kwBxVr8", "email"=>"", "first_name"=>"sssss", "last_Name"=>"", "phone_number"=>"", "password_hint"=>"", "dob_year"=>"1900", "dob_month"=>"1", "dob_day"=>"1"}
