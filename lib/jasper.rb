require "rest-client"

class Jasper
  ## this class to retrieve jasper report based on direct parameters from controller
  attr_accessor :jasper_path, :file_type, :params , :s3_url , :owner_email

  def initialize(jasper_path,file_type, params, owner_email)
    @jasper_path = jasper_path
    @file_type = file_type
    @params = params
    @owner_email = owner_email
  end

  def authenticate_jasper
    #login to get cookies from jasper server and return the cookies generated
    report_server = Setting.jasper.server
    report_user = Setting.jasper.username
    report_password = Setting.jasper.password

    response_login =
        RestClient.post "#{report_server}/login.html",
                        :j_username => report_user,
                        :j_password => report_password

    cookies = {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}
    return cookies
  end

  def request_survey_report
    ## this is the main logic for generating the reports, its v2 api webservice, but doing synchronous
    ## example url = ## http://<host>:<port>/jasperserver[-pro]/rest_v2/reports/path/to/report.<format>?<arguments>
    puts "file type = #{@file_type}"
    url = "#{Setting.jasper.server}/rest_v2/reports#{@jasper_path}.#{@file_type.downcase}?#{set_survey_params}"
    puts "file_path #{url}"
    cookies = authenticate_jasper
    response_get_report = RestClient.get url, cookies
    path = "#{Rails.root}/tmp/jasper_file.#{@file_type}"
    File.open(path, "w+") { |f| f << response_get_report.force_encoding("utf-8") }
    return path
  end

  def process_file_report
    path = request_survey_report
    #@s3_url = stored_in_aws(path,"/reports/dashboard/survey_#{@params[:survey_id]}_#{@params[:start_data]}_#{@params[:end_data]}")
    file_name = "survey_#{@params[:survey_id]}_#{@params[:start_date]}_#{@params[:end_date]}.csv"
    puts "Jasper::sending_email #{@owner_email}"
    OwnerMailer.send_survey_dashboard(Survey.find(@params[:survey_id]).chain,owner_email,path, file_name).deliver!
    puts "finish::sending_email #{@owner_email}"
    return true
  end

  def set_survey_params
    ## set params for v2 web service
    ## it will return &name=jhon&age=15, etc
    str="&survey_id=#{@params[:survey_id]}&start_date=#{@params[:start_date]}&end_date=#{@params[:end_date]}"
    return str
  end

  def stored_in_aws(file, key)
    ## store file in aws and give back the url
    ## returning url for aws
    aw, bucket = initialize_aws
    aw.buckets[bucket].objects[key].write(:file => "#{file}")
    File.delete("#{file}")
    url_s3 = aw.buckets[bucket].objects[key].url_for(:read).to_s
    return url_s3
  end

  def initialize_aws
    ## establish connection to AWS, return the Aws obj, and bucket
    if Rails.env != "production"
      secret_key = Setting.storage.s3_access_key_id
      access_key = Setting.storage.s3_secret_access_key
      bucket = Setting.storage.s3_bucket
    else
      secret_key = ENV["S3_KEY"]
      access_key = ENV["S3_SECRET"]
      bucket = ENV["S3_BUCKET"]
    end


    aw = AWS::S3.new(
        :access_key_id => secret_key,
        :secret_access_key => access_key
    )
    return aw, bucket
  end
end