# encoding: utf-8
require 'nokogiri'
require 'date'
require 'time'

class NetPosReceipt
  attr_accessor :json, :error_messages, :converted_date, :converted_time, :time_zone

  def initialize(json, time_zone = "HST")
    @json = YAML.load(json)
    @error_messages = {}
    @converted_date = nil
    @converted_time = nil
    @time_zone = time_zone.blank? ? "HST" : time_zone
  end

  def check_id
    @json["order_id"].to_s rescue nil
  end

  def seq_num
    @json["order_checks"][0]["check_num"].to_s rescue nil
  end

  def sub_total
    subtotal_text = (@json["order_checks"][0]["subttl_txb"].to_f + @json["order_checks"][0]["subttl_ste"].to_f).to_s rescue 0
    BigDecimal.new(subtotal_text)
  end

  def total
    #@xml_in_nokogiri_format.xpath("//total").text.to_decimal
    total_amount_text = @json["order_checks"][0]["total"].to_f.to_s
    BigDecimal.new(total_amount_text)
    #if total_amount == 0
    #  total_amount = self.sub_total + self.tax
    #end
  end

  def tax
    #@xml_in_nokogiri_format.xpath("//tax").text.to_decimal
    tax_text = @json["order_checks"][0]["tax_ttl"].to_f.to_s
    BigDecimal.new(tax_text)
  end

  def discount_total
    discount_total_text = @json["order_checks"][0]["disc_ttl"].to_f.to_s
    BigDecimal.new(discount_total_text)
  end

  def receipt_date
    #@xml_in_nokogiri_format.xpath("//date").text
    selected_date = @json["start"]
    if selected_date.blank?
      selected_date = Time.zone.now.strftime("%Y-%m-%d %H:%M:%S")
    else
      selected_date = ActiveSupport::TimeZone[@time_zone].parse(selected_date).in_time_zone.strftime("%Y-%m-%d %H:%M:%S")
    end
    selected_date
  end

  def receipt_number
    [self.check_id, self.seq_num].join("-")
  end

  def employee_num
    @json["tslip_id"].to_s
  end

  def revenue_center
    @json["class_id"].to_s
  end

  def check_open_time
    @json["start"].to_s
  end

  def tip_total
    tip_total_text = @json["order_checks"][0]["tip_ttl"].to_f.to_s
    BigDecimal.new(tip_total_text)
  end


  def valid_total?(offer_id=nil,check_upload = nil)
    valid = true

    if self.sub_total.to_f == 0.0 # offer rule purpose forgot loyalty items
      offer = Offer.find(offer_id) rescue nil
      # check the rule first
      if !offer.blank? and offer.is_zero_subtotal?
        g_items = []
        unless offer.offer_rules_subtotal_zeros.blank?
          offer.offer_rules_subtotal_zeros.each do |zero_rules|
            g_items << zero_rules.offer_rule_menu_items
          end
        end
        g_items = g_items.flatten
        #item_numbers = g_items.pos_menu_items.map(&:item_number)
        item_numbers = []
        g_items.each do |g_item|
          item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number)
        end
        offer_item_parser = Parser::OfferItemsParser.new(check_upload)
        offer_item_parser.xml_data = @xml
        result_with_qty = offer_item_parser.find_item_number_and_quantity
        #purchased_item_match = []

        unless result_with_qty.blank?
          result_with_qty.each do |res|
            # if the purchase are correct
            if item_numbers.include?(res[:id].to_i)
              return valid # there are item match with the zero subtotal ( forgot loyalty item)
            end
          end
        end

      end
    end
    #unless self.total > 0
    #  valid = false
    #  @error_messages[:total] = "Total should greater than 0"
    #  return valid
    #end

    unless self.sub_total > 0
      valid = false
      @error_messages[:total] = "Sub Total less than 0"
      return valid
    end

    #unless (self.total.round(3)).eql?((self.tax + self.sub_total).round(3))
    #  valid = false
    #  @error_messages[:total] = "SubTotal + Tax not equal Total"
    #  return valid
    #end
    #unless self.total >= self.sub_total
    #  valid = false
    #  @error_messages[:total] = "Total less than subtotal"
    #  return valid
    #end
    return valid
  end

  def due_total
    due_total_text = @json["DueTotal"].to_s
    BigDecimal.new(due_total_text)
  end

  def valid_date?
    begin
      @converted_date = DateTime.strptime(self.receipt_date, "%Y-%m-%d %H:%M:%S")
      @converted_time = @converted_date.strftime("%I:%M%p")
      return true
    rescue
      @error_messages[:date] = "Invalid date"
      return false
    end
  end

end