class NetPos::ReceiptUnimportedCheckJob < Struct.new(:chain, :receipt, :pos_location, :barcode, :order_id, :seq_num, :trans_num)

  def perform
    base_conditions = ["chain_id = #{chain.id}", "pos_location_id = #{pos_location.id}", "barcode = '#{barcode}'", "check_id = '#{order_id}'", "sequence_number = '#{seq_num}'"]
    base_conditions << "tran_num = '#{trans_num}'" unless trans_num.blank?
    pos_check_upload = PosCheckUpload.where(base_conditions.join(" AND ")).first
    if pos_check_upload && pos_check_upload.status != PosCheckUpload::STATUS[:PROCESSED]
      net_pos_check = YAML.load(pos_check_upload.xml_data)
      receipt_date = DateTime.strptime(net_pos_check["start"], "%Y-%m-%d %H:%M:%S")
      if chain.is_expired_micros_receipt?(receipt_date)
        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.receipt_transactions << receipt_transaction
        receipt_transaction.instructions = {:expired_receipt => "This is receipt is expired"}
        receipt.pos_check_upload_id = pos_check_upload.id
        receipt.status = Receipt::STATUS[:REJECTED]
        receipt.save
        return
      end

      restaurant = pos_location.restaurant
      offer = restaurant.first_active_offer
      if offer.blank?
        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.receipt_transactions << receipt_transaction
        receipt_transaction.instructions = {:offer_blank => "This is receipt could not connect to any offer"}
        receipt.pos_check_upload_id = pos_check_upload.id
        receipt.status = Receipt::STATUS[:REJECTED]
        receipt.save
        return
      end
      user = receipt.user
      pos_check_upload.update_column(:service,PosCheckUpload::SERVICE[:TABLE])
      receipt.update_column(:pos_check_upload_id, pos_check_upload.id)
      Delayed::Job.enqueue(ReceiptCodeJob.new(pos_check_upload, user, restaurant.id, offer.id, receipt.id))
    elsif pos_check_upload && pos_check_upload.status == PosCheckUpload::STATUS[:PROCESSED]
      receipt_transaction = ReceiptTransaction.new
      receipt_transaction.status = Receipt::STATUS[:REJECTED]
      receipt.receipt_transactions << receipt_transaction
      receipt_transaction.instructions = {:loyalty_processed => "Loyalty has been processed before"}
      receipt.pos_check_upload_id = pos_check_upload.id
      receipt.status = Receipt::STATUS[:REJECTED]
      receipt.save
    elsif pos_check_upload.blank?
      receipt_transaction = ReceiptTransaction.new
      receipt_transaction.status = Receipt::STATUS[:REJECTED]
      receipt.receipt_transactions << receipt_transaction
      receipt_transaction.instructions = {:invalid_barcode => "Barcode is invalid"}
      receipt.status = Receipt::STATUS[:REJECTED]
      receipt.save
    end

  end

end