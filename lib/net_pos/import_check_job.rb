class NetPos::ImportCheckJob < Struct.new(:chain, :pos_location)

  def perform
    chain.net_pos_import_check(pos_location)
  end

end