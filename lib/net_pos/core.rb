  module NetPos
  module Core
    extend ActiveSupport::Concern

    module ClassMethods
      def net_pos_check_type
        {
            "TRANSACTION RECEIPT" => 7,
            "GUEST CHECK" => 5,
            "CHECK" => 6
        }
      end

      def net_pos_code(barcode)
        if [net_pos_check_type["CHECK"], net_pos_check_type["GUEST CHECK"]].include?(barcode[0].to_i)
          codes =  barcode.scan(/(^\d{1})?(\w{3,4})(\w*)(\w{2}$)/)
        else
          codes = barcode.scan(/(^\d{1})?(\w{3,4})(\w*)(\w{2,2})(\w{2}$)/)
        end
        codes.flatten
      end

      def net_pos_import_all_check
        PosLocation.where("\"POS_StoreID\" IS NOT NULL AND \"POS_StoreID\" != '' AND \"POS_ChainID\" IS NOT NULL AND \"POS_ChainID\" != ''").each do |pos_location|
          chain = pos_location.chain
          Delayed::Job.enqueue(NetPos::ImportCheckJob.new(chain, pos_location)) if chain.pos_used_type == Chain::POS_USED_TYPE["NETPOS"]
        end
      end

      def generate_barcode_32(pos_check_upload_ids)
        barcodes = []
        pos_check_upload_ids.each do |pos_check_upload_id|
          pos_check_upload = PosCheckUpload.find pos_check_upload_id
          pos_location = pos_check_upload.pos_location
          barcode = "6"
          barcode += pos_location.POS_StoreID.to_i.to_s(32).upcase.rjust(4, '0')
          barcode += pos_check_upload.check_id.to_i.to_s(32).upcase
          barcode += pos_check_upload.sequence_number.to_i.to_s(32).upcase.rjust(2, '0')
          barcodes << barcode
        end
      end

      def barcode_32(pos_check_upload)
        barcode = ""
        if pos_check_upload.xml_data.class == String
          json_check = YAML.load(pos_check_upload.xml_data) rescue nil
        elsif pos_check_upload.xml_data.class == Hash
          json_check = pos_check_upload.xml_data
        end
        return barcode if json_check.blank?
        trans = json_check["trans"]
        trans = trans.to_a.flatten if trans.class != Array
        if trans.map{|a| a["tran_num"]}.uniq.count < 2
          barcode = "6"
          barcode += pos_check_upload.pos_location.POS_StoreID.to_i.to_s(32).upcase.rjust(4, '0')
          barcode += pos_check_upload.check_id.to_i.to_s(32).upcase
          barcode += pos_check_upload.sequence_number.to_i.to_s(32).upcase.rjust(2, '0')
        else
          barcode = "7"
          barcode += pos_check_upload.pos_location.POS_StoreID.to_i.to_s(32).upcase.rjust(4, '0')
          barcode += pos_check_upload.check_id.to_i.to_s(32).upcase
          barcode += pos_check_upload.sequence_number.to_i.to_s(32).upcase.rjust(2, '0')
          barcode += pos_check_upload.tran_num.to_i.to_s(32).upcase.rjust(2, '0')
        end
        barcode
      end
    end


    module InstanceMethods
      def net_pos_import_check(pos_location)
        pos_time_zone = pos_location.netpos_timezone.blank? ? "HST" : pos_location.netpos_timezone
        time_current = Time.current.in_time_zone(pos_time_zone)
        chain = self
        netpos_setting = chain.netpos_setting
        return if netpos_setting.blank?
        return if !netpos_setting.blank? && netpos_setting.root_url.blank?
        soap = <<SM
<?xml version="1.0"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <soap:Header>
	  <chainId>#{pos_location.POS_ChainID}</chainId>
	  <store_id>#{pos_location.POS_StoreID}</store_id>
	  <login_name>#{netpos_setting.login}</login_name>
    <login_passwd>#{netpos_setting.password}</login_passwd>
  </soap:Header>
  <soap:Body>
	<listOrderDetails xmlns="http://www.netpos.com/Soap/Central">
  	<date_range start_time="#{(time_current - 70.minutes).strftime("%Y-%m-%d %H:%M:%S")}" end_time="#{(time_current + 10.minutes).strftime("%Y-%m-%d %H:%M:%S")}" tz="#{pos_location.netpos_timezone.blank? ? "HST" : ActiveSupport::TimeZone[pos_location.netpos_timezone].now.strftime("%Z")}"/>
	</listOrderDetails>
  </soap:Body>
</soap:Envelope>
SM
        result =  HTTParty.post(netpos_setting.root_url, body: soap, headers: {'Content-type' => 'text/xml'})
        if result.response.class == Net::HTTPOK
          xml_check = result.parsed_response
          checks =  chain.net_pos_build_checks(xml_check)
          checks.keys.each do |check_identifier|
            ids = check_identifier.split("-")
            pos_check_upload = PosCheckUpload.where(:chain_id => chain.id, :pos_location_id => pos_location.id, :check_id => ids[1], :sequence_number => ids[2], :tran_num => ids[3]).first
            if pos_check_upload.blank?
              pos_check_upload = PosCheckUpload.new(
                  :pos_location_id => pos_location.id,
                  :chain_id => chain.id,
                  :xml_data => checks[check_identifier],
                  :check_id => ids[1],
                  :status => PosCheckUpload::STATUS[:NEW],
                  :sequence_number =>  ids[2],
                  :tran_num => ids[3],
                  :pos_used_type => Chain::POS_USED_TYPE["NETPOS"]
              )
              pos_check_upload.save
              pos_check_upload.set_barcode_32
            elsif pos_check_upload.status != PosCheckUpload::STATUS[:PROCESSED]
              pos_check_upload.pos_location_id = pos_location.id
              pos_check_upload.chain_id = chain.id
              pos_check_upload.xml_data = checks[check_identifier]
              pos_check_upload.check_id = ids[1]
              pos_check_upload.status = PosCheckUpload::STATUS[:UPDATED]
              pos_check_upload.sequence_number =  ids[2]
              pos_check_upload.tran_num = ids[3]
              pos_check_upload.pos_used_type = Chain::POS_USED_TYPE["NETPOS"]
              pos_check_upload.save
              pos_check_upload.set_barcode_32
            end
          end unless checks.blank?
        end
      end

      def net_pos_sample_check
        chain = self
        start_order_id = 261986
        order_id = PosCheckUpload.where(:chain_id => chain.id).count + start_order_id
        curr_time = Time.zone.now
        start_time = curr_time.strftime("%Y-%m-%d %H:%M%S")
        end_time = (curr_time + 10.minutes).strftime("%Y-%m-%d %H:%M%S")
        xml_check = <<SM
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<soap:Body>
<listOrderDetailsResponse xmlns="http://www.netpos.com/Soap/Central">
<order_details>
<orders>
<order tslip_id="3644" table_num="25" rep_g_count="1" mod_time="2009-11-17 14:29:07" g_count="1" order_id="#{order_id}" end="#{end_time}" class_id="1" start="#{start_time}"/>
<order tslip_id="3644" table_num="31" rep_g_count="1" mod_time="2009-11-17 15:21:18" g_count="1" order_id="#{order_id+1}" end="#{end_time}" class_id="1" start="#{start_time}"/>
<order tslip_id="3644" table_num="33" rep_g_count="2" mod_time="2009-11-17 15:21:48" g_count="2" order_id="#{order_id+2}" end="#{end_time}" class_id="1" start="#{start_time}"/>
<order tslip_id="3644" table_num="24" rep_g_count="1" mod_time="2009-11-17 15:25:24" g_count="1" order_id="#{order_id+3}" end="#{end_time}" class_id="1" start="#{start_time}"/>
</orders>
    	<order_items>
      	<order_item pnet="679" mitem_id="38" item_num="1" itax="0" mod_time="2009-11-17 14:29:46" status="1" order_id="#{order_id}" has_itax="N" qty="1" texempt="N" pcomp="0" etax="41" padj="0" price="679" disc="0"/>
<order_item pnet="599" mitem_id="804" item_num="1" itax="0" mod_time="2009-11-17 15:23:21" status="1" order_id="#{order_id+1}" has_itax="N" qty="1" texempt="Y" pcomp="0" etax="0" padj="0" price="599" disc="0"/>
<order_item pnet="995" mitem_id="815" item_num="2" itax="0" mod_time="2009-11-17 15:23:21" status="1" order_id="#{order_id+1}" has_itax="N" qty="1" texempt="N" pcomp="0" etax="60" padj="0" price="995" disc="0"/>
<order_item pnet="555" mitem_id="41" item_num="1" itax="0" mod_time="2009-11-17 15:24:00" status="1" order_id="#{order_id+2}" has_itax="N" qty="1" texempt="N" pcomp="0" etax="33" padj="0" price="555" disc="0"/>
<order_item pnet="555" mitem_id="41" item_num="2" itax="0" mod_time="2009-11-17 15:24:00" status="1" order_id="#{order_id+2}" has_itax="N" qty="1" texempt="N" pcomp="0" etax="34" padj="0" price="555" disc="0"/>
<order_item pnet="250" mitem_id="336" item_num="1" itax="56" mod_time="2009-11-17 15:28:39" status="1" order_id="#{order_id+3}" has_itax="N" qty="1" texempt="N" pcomp="0" etax="0" padj="0" price="250" disc="0"/>
<order_item pnet="250" mitem_id="330" item_num="2" itax="56" mod_time="2009-11-17 15:28:39" status="1" order_id="#{order_id+3}" has_itax="N" qty="1" texempt="N" pcomp="0" etax="0" padj="0" price="250" disc="0"/>
</order_items>
    	<order_mods>
      	<order_mod pnet="50" mitem_id="384" mod_num="1" itax="12" item_num="2" order_id="#{order_id+1}" pcomp="0" etax="0" padj="0" price="50" disc="0"/>
<order_mod pnet="50" mitem_id="384" mod_num="2" itax="12" item_num="2" order_id="#{order_id+1}" pcomp="0" etax="0" padj="0" price="50" disc="0"/>
<order_mod pnet="50" mitem_id="384" mod_num="3" itax="12" item_num="2" order_id="#{order_id+1}" pcomp="0" etax="0" padj="0" price="50" disc="0"/>
<order_mod pnet="50" mitem_id="384" mod_num="4" itax="12" item_num="2" order_id="#{order_id+1}" pcomp="0" etax="0" padj="0" price="50" disc="0"/>
<order_mod pnet="50" mitem_id="384" mod_num="5" itax="12" item_num="2" order_id="#{order_id+1}" pcomp="0" etax="0" padj="0" price="50" disc="0"/>
<order_mod pnet="50" mitem_id="384" mod_num="6" itax="12" item_num="2" order_id="#{order_id+1}" pcomp="0" etax="0" padj="0" price="50" disc="0"/>
<order_mod pnet="50" mitem_id="384" mod_num="7" itax="12" item_num="2" order_id="#{order_id+1}" pcomp="0" etax="0" padj="0" price="50" disc="0"/>
</order_mods>
    	<order_checks>
     	<order_check tax_ttl="41" check_num="1" tslip_id="3644" autograt="0" mod_time="2009-11-17 14:29:46" order_id="#{order_id}" subttl_txb="679" total="720" texempt="N" subttl_ste="0" disc_ttl="0" itax_ttl="0"/>
<order_check tax_ttl="60" check_num="1" tslip_id="3644" autograt="0" mod_time="2009-11-17 15:23:21" order_id="#{order_id+1}" subttl_txb="995" total="1654" texempt="N" subttl_ste="599" disc_ttl="0" itax_ttl="0"/>
<order_check tax_ttl="67" check_num="1" tslip_id="3644" autograt="0" mod_time="2009-11-17 15:24:00" order_id="#{order_id+2}" subttl_txb="1110" total="1177" texempt="N" subttl_ste="0" disc_ttl="0" itax_ttl="0"/>
<order_check tax_ttl="0" check_num="1" tslip_id="3644" autograt="0" mod_time="2009-11-17 15:28:39" order_id="#{order_id+3}" subttl_txb="850" total="850" texempt="N" subttl_ste="0" disc_ttl="0" itax_ttl="196"/>
</order_checks>
    	<trans>
      	<tran check_num="1" tran_type_id="1" mod_time="2009-11-17 14:29:46" tran_num="1" order_id="#{order_id}" amount="720" applied="720" change_amt="0"/>
<tran check_num="1" tran_type_id="1" mod_time="2009-11-17 15:23:21" tran_num="1" order_id="#{order_id+1}" amount="2000" applied="1654" change_amt="346"/>
<tran check_num="1" tran_type_id="1" mod_time="2009-11-17 15:24:00" tran_num="1" order_id="#{order_id+2}" amount="2000" applied="1177" change_amt="823"/>
<tran check_num="1" tran_type_id="1" mod_time="2009-11-17 15:28:39" tran_num="1" order_id="#{order_id+3}" amount="1000" applied="850" change_amt="150"/>
</trans>
    	<loyalty_requests/>
</order_details>
	</listOrderDetailsResponse>
</soap:Body>
</soap:Envelope>
SM
        pos_location = chain.pos_locations.where("\"POS_StoreID\" IS NOT NULL AND \"POS_StoreID\" != '' AND \"POS_ChainID\" IS NOT NULL AND \"POS_ChainID\" != ''").last
        xml_check = Hash.from_xml(xml_check)
        checks =  chain.net_pos_build_checks(xml_check)
        checks.keys.each do |check_identifier|
          ids = check_identifier.split("-")
          pos_check_upload = PosCheckUpload.where(:chain_id => chain.id, :pos_location_id => pos_location.id, :check_id => ids[1], :sequence_number => ids[2], :tran_num => ids[3]).first
          if pos_check_upload.blank?
            PosCheckUpload.create(
                :pos_location_id => pos_location.id,
                :chain_id => chain.id,
                :xml_data => checks[check_identifier],
                :check_id => ids[1],
                :status => PosCheckUpload::STATUS[:NEW],
                :sequence_number =>  ids[2],
                :tran_num => ids[3],
                :pos_used_type => Chain::POS_USED_TYPE["NETPOS"]
            )
          elsif pos_check_upload.status != PosCheckUpload::STATUS[:PROCESSED]
            pos_check_upload.update_attributes(
                :pos_location_id => pos_location.id,
                :chain_id => chain.id,
                :xml_data => checks[check_identifier],
                :check_id => ids[1],
                :status => PosCheckUpload::STATUS[:UPDATED],
                :sequence_number =>  ids[2],
                :tran_num => ids[3],
                :pos_used_type => Chain::POS_USED_TYPE["NETPOS"]
            )
          end
        end
      end

      def net_pos_build_checks(hash)
        orders = hash["Envelope"]["Body"]["listOrderDetailsResponse"]["order_details"]["orders"]["order"] rescue []
        return if orders.blank?
        orders = [orders].flatten rescue []
        items = hash["Envelope"]["Body"]["listOrderDetailsResponse"]["order_details"]["order_items"]["order_item"] rescue []
        items = [items].flatten rescue []
        mods = hash["Envelope"]["Body"]["listOrderDetailsResponse"]["order_details"]["order_mods"]["order_mod"] rescue []
        mods = [mods].flatten rescue []
        checks = hash["Envelope"]["Body"]["listOrderDetailsResponse"]["order_details"]["order_checks"]["order_check"] rescue []
        checks = [checks].flatten rescue[]
        trans = hash["Envelope"]["Body"]["listOrderDetailsResponse"]["order_details"]["trans"]["tran"] rescue []
        trans = [trans].flatten rescue []
        separated_checks = {}
        orders.each do |order|
          order["order_items"] = items.select{|item| item["order_id"] == order["order_id"] }
          order["order_mods"] = mods.select{|mod| mod["order_id"] == order["order_id"] }
          order_checks =  checks.select{|check| check["order_id"] == order["order_id"] }
          check_nums = order_checks.map{|a| a["check_num"]}.uniq rescue []
          check_nums.each do |check_num|
            order["order_checks"] = order_checks.select{|check| check["check_num"] == check_num}
            trans_checks = trans.select{|tran| tran["order_id"] == order["order_id"] && tran["check_num"] == check_num}
            tran_nums = trans_checks.map{|a| a["tran_num"]}.uniq rescue []
            tran_nums.each do |tran_num|
              the_check = order.dup
              the_check["trans"] = trans_checks.select{|tran| tran["tran_num"] == tran_num}
              separated_checks["#{self.id}-#{order["order_id"]}-#{check_num}-#{tran_num}"] = the_check
            end
            if tran_nums.blank?
              the_check = order.dup
              the_check["trans"] = []
              separated_checks["#{self.id}-#{order["order_id"]}-#{check_num}"] = the_check
            end
          end
          if check_nums.blank?
            the_check = order.dup
            the_check["order_checks"] = []
            separated_checks["#{self.id}-#{order["order_id"]}"] = the_check
          end
        end
        separated_checks
      end

      def net_pos_process_loyalty(user, barcode)
        chain = self
        codes = Chain.net_pos_code(barcode)
        check_type, store_id, order_id, check_num, tran_num = codes
        if store_id.blank? || order_id.blank? || check_num.blank?
          return {:status => false, :notice => "We are sorry. The code scanned is either invalid or expired. Please contact customer support from the info section of the app for further assistance."}
        end
        store_id_base_32 = store_id.to_i(32)
        order_id_base_32 = order_id.to_i(32)
        check_num_base_32 = check_num.to_i(32)
        tran_num_base_32 = tran_num.to_i(32) unless tran_num.blank?
        pos_location = PosLocation.where("chain_id = ? AND \"POS_StoreID\" = ?", chain.id, store_id_base_32.to_s).first
        return {:status => false, :notice => "Barcode is not accosiated with any location."} if pos_location.blank?
        base_conditions = ["chain_id = #{chain.id}", "pos_location_id = #{pos_location.id}", "barcode = '#{barcode}'", "check_id = '#{order_id_base_32}'", "sequence_number = '#{check_num_base_32}'"]
        base_conditions << "tran_num = '#{tran_num_base_32}'" unless tran_num_base_32.blank?
        conditions = ["status <> #{PosCheckUpload::STATUS[:PROCESSED]}"]
        pos_check_upload = PosCheckUpload.where(base_conditions.join(" AND ")).first
        if pos_check_upload && pos_check_upload.status != PosCheckUpload::STATUS[:PROCESSED]
          net_pos_check = YAML.load(pos_check_upload.xml_data)
          receipt_date = DateTime.strptime(net_pos_check["start"], "%Y-%m-%d %H:%M:%S")
          if chain.is_expired_micros_receipt?(receipt_date)
            return  {:status => false, :notice => "Receipt code expired. Please contact customer support from the info section of the app for further assistance."}
          end
          pos_location = pos_check_upload.pos_location
          restaurant = pos_location.restaurant
          offer = restaurant.first_active_offer
          render :json => {:status => false, :notice => "Invalid Offer"} and return if offer.blank?
          survey_id = offer.try(:survey).try(:id)
          receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true, :pos_check_upload_id => pos_check_upload.id)
          receipt_transaction = ReceiptTransaction.new
          receipt_transaction.status = Receipt::STATUS[:RECEIVED]
          receipt.receipt_transactions << receipt_transaction
          receipt.save
          pos_check_upload.update_column(:service,PosCheckUpload::SERVICE[:TABLE])
          Delayed::Job.enqueue(ReceiptCodeJob.new(pos_check_upload, user, restaurant.id, offer.id, receipt.id), :run_at => Time.zone.now + chain.pos_receipt_processing_delay.minutes)
          return {
              status: true,
              message: (ApiResponseText.get_message_chain_by_locale(chain.id, user.locale_id, 7)),
              survey_id: survey_id,
              receipt_id: receipt.id
          }
        elsif pos_check_upload && pos_check_upload.status == PosCheckUpload::STATUS[:PROCESSED]
          return {status: false, notice: "We are sorry. The code scanned is either invalid or expired. Please contact customer support from the info section of the app for further assistance."}
        elsif pos_check_upload.blank?
          restaurant = pos_location.restaurant
          offer = restaurant.first_active_offer
          render :json => {:status => false, :notice => "Invalid Offer"} and return if offer.blank?
          survey_id = offer.try(:survey).try(:id)
          receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true)
          receipt_transaction = ReceiptTransaction.new
          receipt_transaction.status = Receipt::STATUS[:RECEIVED]
          receipt.receipt_transactions << receipt_transaction
          receipt.save
          Delayed::Job.enqueue(NetPos::ReceiptUnimportedCheckJob.new(chain, receipt, pos_location, barcode,  order_id_base_32, check_num_base_32, tran_num_base_32), :run_at => Time.zone.now + chain.pos_receipt_processing_delay.minutes)
          return {
              status: true,
              message: (ApiResponseText.get_message_chain_by_locale(chain.id, user.locale_id, 7)),
              survey_id: survey_id,
              receipt_id: receipt.id
          }
        end

      end

      def get_menu_items(pos_chain_id, pos_store_id)
        chain = self
        netpos_setting = chain.netpos_setting
        return if netpos_setting.blank?
        soap = <<SM
<?xml version="1.0"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <soap:Header>
	  <chainId>#{pos_chain_id}</chainId>
	  <store_id>#{pos_store_id}</store_id>
	  <login_name>#{netpos_setting.login}</login_name>
    <login_passwd>#{netpos_setting.password}</login_passwd>
  </soap:Header>
  <soap:Body>
	<listMenuItems xmlns="http://www.netpos.com/Soap/Central"/>
  </soap:Body>
</soap:Envelope>
SM
        result =  HTTParty.post(netpos_setting.root_url, body: soap, headers: {'Content-type' => 'text/xml'})
        if result.response.class == Net::HTTPOK
          xml_check = result.parsed_response
          p xml_check
        else
          p "FAILED"
          p result
        end
      end

      def get_menu_item_categories(pos_chain_id, pos_store_id)
        chain = self
        netpos_setting = chain.netpos_setting
        return if netpos_setting.blank?
        soap = <<SM
<?xml version="1.0"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
  <soap:Header>
	  <chainId>#{pos_chain_id}</chainId>
	  <store_id>#{pos_store_id}</store_id>
	  <login_name>#{netpos_setting.login}</login_name>
    <login_passwd>#{netpos_setting.password}</login_passwd>
  </soap:Header>
  <soap:Body>
	<listMenuItemCategories xmlns="http://www.netpos.com/Soap/Central"/>
  </soap:Body>
</soap:Envelope>
SM
        result =  HTTParty.post(netpos_setting.root_url, body: soap, headers: {'Content-type' => 'text/xml'})
        if result.response.class == Net::HTTPOK
          xml_check = result.parsed_response
          p xml_check
        else
          p "FAILED"
          p result
        end
      end
    end
  end
end