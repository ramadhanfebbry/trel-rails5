class NetPos::ApiHelper
  attr_accessor :result, :reward_code, :chain_id, :store_id, :params, :chain

  def initialize(params)
    #"chainId"=>"hijklmn", "storeId"=>"abcdefg"
    @params = params
    @reward_code = reward_code
    @chain_id = params["params"]["chainId"]
    @store_id = params["params"]["storeId"]
  end


  def result
    res = nil

    chain = get_chain
    user_tmp = get_user rescue nil
    #return {:status => false, :notice => "Pos Store ID, Pos Chain ID NOT FOUND"} if chain.blank?
    if chain.blank?
      pos_reward_history("no_store_related")
      return invalid_store_message
    end

    ## if void redeeem skip this part
    if @params["method"].downcase != "loyalty.voidredeem"
      reward = get_reward
      if reward.blank?
        pos_reward_history("no_barcode_related")
        return invalid_barcode_message
      else
        if reward.is_net_pos_reward?
          return result_discount unless reward.pos_discount_type.blank?
        end
      end
    end

    case @params["method"].downcase
      when "loyalty.inquire"
        pos_reward_history("success",nil)
        res = {
            "jsonrpc" => "2.0",
            "id" => @params["id"], #same as in request
            "result" => {
                "pointsAccumulated" => "0", #hardcode to Null
                "pointsBalance" => "0", #hardcode to Null
                "visits" => "0", #//hardcode to Null
                "firstName" => "", #//hardcode to Null
                "lastName" => "", #//hardcode to Null
                "funFacts" => "",
                "issuable" => false,#//hardcode to Null
                "compRewards" => [
                    {
                        "rewardType" => "comp", # //hardcode, will use for loyalty program
                        "rewardId" => "#{@params['rewardId'].blank? ? @params["params"]["customerId"] : @params['rewardId']}", #//use customerId/reward code in request
                        "rewardName" => reward.name, #//use “Reward Title” on Relevant
                        "balance" => "1", #//hardcode to 1..means how many reward of type comp rewardcode has
                        "targetType" => reward.targetType, #//defines qualifying item in POS, need to setup beforehand when creating Reward, see: Defining Rewards
                        "targetId" => reward.targetId, #//defines qualifying item in POS.. like bonus plan id ncr, need to setup before hand when creating Reward see: Defining Rewards
                        "adjType" => reward.adj_type, #//hardcode to “pct”
                        "adjValue" => reward.adj_value #//hardcode to 100
                    },
                ],
            }
        }

      when "loyalty.redeem"
        rt = redeem_reward
        pos_reward_history("success",rt.user_id)
        user_tmp.burn(reward.points) rescue nil
        RewardWallet.where(:user_id => user_tmp.id , :reward_id => reward.id).first.update_attribute(:status,RewardWallet::STATUS[:CLAIMED]) rescue nil
        res = {
            "jsonrpc" => "2.0",
            "id" => @params["id"],
            "result" => {
                "tid" => "#{rt.id}", #reward redeem transaction id
                "quantity" => "1", # the qty that was redeemed for user for a request. will also be in pennies for discRewards, and compRewards will be always 1 because we allow only 1 unit discount of an item (will do discRewards later, only do compRewards now)
                "balance" => "0" #//remaining balance. For discRewards type in pennies, should be 0 always for compRewards (will do discRewards later, only do compRewards now)
            }

        }

      when "loyalty.voidredeem"
        rt = find_reward_transaction
        unless rt.blank?
          pos_reward_history("success",rt.user_id)
          res = {
              "jsonrpc" => "#{@params["jsonrpc"]}",
              "id" => @params["id"],
              "result" => {
                  "balance" => "1" #//should always be 1
              }
          }

          user_tmp.earn(reward.points) rescue nil
          PointHistory.add_to_history(user_tmp.id, "Void Redeem for ##{reward.name}", (reward.points * 1)) rescue nil
          RewardWallet.where(:user_id => user_tmp.id , :reward_id => reward.id).first.update_attribute(:status,RewardWallet::STATUS[:ACTIVE]) rescue nil
          rt.update_column(:pos_used , false) rescue nil
        else
          ## reward transaction no ID
          res = invalid_transaction_message
          pos_reward_history("no_transaction",rt.user_id)
        end

    end

    @result = res.to_json
  end

  def find_reward_transaction
    res = RewardTransaction.find(@params["params"]["tid"]) rescue nil
    res
  end

  def redeem_reward
    reward_transaction = get_reward_transaction
    reward_transaction.update_column(:pos_used, true)
    reward_code_user = RewardCodeUser.where(:chain_id => @chain.id, :staffcode => reward_transaction.staffcode, :active => true).first
    reward_code_user.destroy if reward_code_user
    return reward_transaction
  end


  def get_reward
    #reward = RewardTransaction.where("").find(@params["id"]) rescue nil
    return (get_reward_transaction.reward rescue nil)
  end

  def get_reward_transaction_for_discount
    return (get_reward_transaction rescue nil)
  end

  def get_user
    puts "API::HELPER::NETPOS::GETUSER--------------"
    u = get_reward_transaction.user
    puts "USER = #{u}"
    return u
  end

  # def get_reward_transaction
  #   puts "Get reward transaction"
  #   puts @params
  #   r_code = @params["params"]["customerId"].blank? ?  @params["params"]["rewardId"] : @params["params"]["customerId"]
  #   puts @chain.rewards.map(&:id)
  #
  #   if @params["method"].downcase != "loyalty.voidredeem"
  #     reward_transaction = RewardTransaction.where("UPPER(staffcode) = ? AND reward_id IN (?) AND pos_used IS FALSE", r_code,
  #                                                  @chain.rewards.map(&:id)).first
  #   elsif @params["method"].downcase == "loyalty.voidredeem"
  #     reward_transaction = RewardTransaction.where("UPPER(staffcode) = ? AND reward_id IN (?)", r_code,
  #                                                  @chain.rewards.map(&:id)).first
  #   end
  #   return reward_transaction
  # end

  def get_reward_transaction
    chain = @chain
    r_code = @params["params"]["customerId"].blank? ? @params["params"]["rewardId"] : @params["params"]["customerId"]
    reward_ids = @chain.rewards.map(&:id)
    reward_code = r_code.to_s.upcase
    @pos_location = get_pos_location

    reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
    reward_code_setting = JSON.parse(reward_code_setting) rescue nil
    reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
    if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
      if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
        reward_code = reward_code.to_ean_13_format
      end
    end
    reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
    reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => reward_code, :active => true).first
    return nil if reward_code_user.blank?

    if @params["method"].downcase != "loyalty.voidredeem" ## if its not cancel the transaction ( void redeem )
      reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", reward_code, reward_ids)

      reward_code_expire = reward_code_setting["timer"].to_i
      if reward_code_expire != 0
        reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
      else
        reward_transaction = reward_transactions.order("created_at DESC").first
      end
      return reward_transaction
    elsif  @params["method"].downcase == "loyalty.voidredeem"
      reward_transaction = RewardTransaction.where("staffcode = ? AND reward_id IN (?)", reward_code,
                                                        @chain.rewards.map(&:id)).first
      return reward_transaction
    end
  end

  def get_chain
    puts @params["params"]
    puts "get chain #{@params["params"]["chainId"]} ----  #{@params["params"]["storeId"]}"
    puts "get chain ==== #{@store_id} ===== #{@chain_id}"
    #ch = PosLocation.where("'POS_StoreID' = ? and 'POS_ChainID' = ?", @store_id, @chain_id).first.chain rescue nil
    ch = PosLocation.where("\"POS_StoreID\" = ? and \"POS_ChainID\" = ?", @params["params"]["storeId"], @params["params"]["chainId"]).first.chain rescue nil
    puts "CHAIN IS #{ch}"
    @chain = ch
    return ch
  end

  def get_pos_location
    puts @params["params"]
    puts "get POS LOCATION #{@params["params"]["chainId"]} ----  #{@params["params"]["storeId"]}"
    puts "get POS LOCATION ==== #{@store_id} ===== #{@chain_id}"
    #ch = PosLocation.where("'POS_StoreID' = ? and 'POS_ChainID' = ?", @store_id, @chain_id).first.chain rescue nil
    pl = PosLocation.where("\"POS_StoreID\" = ? and \"POS_ChainID\" = ?", @params["params"]["storeId"], @params["params"]["chainId"]).first rescue nil
    return pl
  end

  def pos_reward_history(mode = nil, user_id = nil)
    begin
      r_code = @params["params"]["customerId"].blank? ? @params["params"]["rewardId"] : @params["params"]["customerId"]
      pos_location = get_pos_location
      pos_reward_check = PosRewardCheck.new(:pos_location_id => (pos_location.id rescue nil),
                                            :chain_id => (pos_location.chain_id rescue nil),
                                            :xml_data => (@params.except("action","controller","net_po").to_json),
                                            :reward_code => r_code,
                                            :pos_used_type => Chain::POS_USED_TYPE["NETPOS"])

      case mode
        when "no_store_related"
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = invalid_store_message
          pos_reward_check.save
        when "no_barcode_related"
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = invalid_barcode_message
          pos_reward_check.save
        when "no_transaction"
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = invalid_transaction_message
          pos_reward_check.save
        else
          pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
          pos_reward_check.user_id = user_id
          pos_reward_check.save
      end
    rescue => e
      puts "NETPOS::APIHELPER::POS_REWARD_HISTORY:: => #{e.inspect}"
    end
  end

  def invalid_store_message
    json_msg = {
        "jsonrpc" => "2.0",
        "id" => @params["id"],
        "error" => {
            "code" => "101",
            "message" => "Invalid Store ID."
        }
    }

    return json_msg
  end

  def invalid_transaction_message
    json_msg = {
        "jsonrpc" => "2.0",
        "id" => @params["id"],
        "error" => {
            "code" => "100",
            "message" => "Invalid Transaction ID."
        }
    }

    return json_msg
  end

  def invalid_barcode_message
    json_msg = {
        "jsonrpc" => "2.0",
        "id" => @params["id"],
        "error" => {
            "code" => "100",
            "message" => "Invalid short code."
        }
    }

    return json_msg
  end



  ################################################################################################ discount Reward ####################


  def result_discount
    res = nil
    chain = get_chain
    user_tmp = get_user rescue nil
    #return {:status => false, :notice => "Pos Store ID, Pos Chain ID NOT FOUND"} if chain.blank?
    if chain.blank?
      pos_reward_history("no_store_related")
      return invalid_store_message
    end

    reward_transaction = get_reward_transaction_for_discount
    reward = get_reward

    if reward.blank?
      pos_reward_history("no_barcode_related")
      return invalid_barcode_message
    end

    case @params["method"].downcase
      when "loyalty.inquire"
        pos_reward_history("success",nil)
        rw = get_wallet_balance(reward_transaction)
        bal = rw.rest_balance rescue 0
        bal = 1 if reward_transaction.reward.pos_discount_type.discount_type ==  PosDiscountType::NETPOS_TYPES["fixed"]
        puts "BALANCE" * 100
        puts "#{reward_transaction.reward.pos_discount_type.discount_type}"
        puts "balance --- #{bal}"
        res = {
            "jsonrpc" => "2.0",
            "id" => @params["id"], #same as in request
            "result" => {
                "pointsAccumulated" => "0", #hardcode to Null
                "pointsBalance" => "0", #hardcode to Null
                "visits" => "0", #//hardcode to Null
                "firstName" => "", #//hardcode to Null
                "lastName" => "", #//hardcode to Null
                "funFacts" => "",
                "issuable" => false,#//hardcode to Null
                "discRewards" => [
                    {
                        "rewardType" => 'disc', # //hardcode, will use for loyalty program
                        "rewardId" => "#{@params['rewardId'].blank? ? @params["params"]["customerId"] : @params['rewardId']}", #//use customerId/reward code in request
                        "rewardName" => reward.name, #//use “Reward Title” on Relevant
                        "balance" => "#{(bal.to_i)}", #//hardcode to 1..means how many reward of type comp rewardcode has

                    }.merge!(pos_discount_type_helper(reward_transaction,rw)),
                ],
            }
        }

      when "loyalty.redeem"
        rt = redeem_reward
        pos_reward_history("success",rt.user_id)
        user_tmp.burn(reward.points) rescue nil
        rw = get_wallet_balance(reward_transaction)
        balance = rw.rest_balance.to_f
        rest_calculation = (balance -  @params["params"]["quantity"].to_f)

        if rest_calculation  <= 0
          rw.update_attribute(:status,RewardWallet::STATUS[:CLAIMED]) rescue nil
        else
          rw.update_attribute(:rest_balance, balance -  @params["params"]["quantity"].to_f)
        end
        rest_calculation = 0 if reward_transaction.reward.pos_discount_type.discount_type ==  PosDiscountType::NETPOS_TYPES["fixed"]

        res = {
            "jsonrpc" => "2.0",
            "id" => @params["id"],
            "result" => {
                "tid" => "#{rt.id}", #reward redeem transaction id
                "quantity" => "#{@params["params"]["quantity"]}", # the qty that was redeemed for user for a request. will also be in pennies for discRewards, and compRewards will be always 1 because we allow only 1 unit discount of an item (will do discRewards later, only do compRewards now)
                "balance" => "#{rest_calculation.to_i}" #//remaining balance. For discRewards type in pennies, should be 0 always for compRewards (will do discRewards later, only do compRewards now)
            }

        }

      when "loyalty.voidredeem"
        rt = find_reward_transaction
        unless rt.blank?
          pos_reward_history("success",rt.user_id)
          res = {
              "jsonrpc" => "#{@params["jsonrpc"]}",
              "id" => @params["id"],
              "result" => {
                  "balance" => "1" #//should always be 1
              }
          }

          user_tmp.earn(reward.points) rescue nil
          PointHistory.add_to_history(user_tmp.id, "Void Redeem for ##{reward.name}", (reward.points * 1)) rescue nil
          RewardWallet.where(:user_id => user_tmp.id , :reward_id => reward.id).first.update_attribute(:status,RewardWallet::STATUS[:ACTIVE]) rescue nil
          rt.update_column(:pos_used , false) rescue nil
        else
          ## reward transaction no ID
          res = invalid_transaction_message
          pos_reward_history("no_transaction",rt.user_id)
        end

    end

    @result = res.to_json
  end

  def pos_discount_type_helper(reward_transaction,rw)

    #"amountType" => "#{pos_discount_type_helper(reward_transaction)[0]}",
    #"adjValue" => pos_discount_type_helper(reward_transaction)[1] #//hardcode to 100
    discount_type =  reward_transaction.reward.pos_discount_type#.discount_amount
    return false if discount_type.blank?
    dc_type = ""
    case discount_type.discount_type
      when PosDiscountType::NETPOS_TYPES["open"]
        dc_type = "open"
      when PosDiscountType::NETPOS_TYPES["fixed"]
        dc_type = "fixed"
    end

    hash_result = {"amountType" => dc_type}
    if dc_type == "open"
      amount = discount_type.discount_amount if rw.blank?
      amount = rw.rest_balance if !rw.blank?
      hash_result = hash_result.merge!({"balance" => amount})
    end
    if dc_type == "fixed"
      hash_result = hash_result.merge!({"adjValue" => discount_type.discount_amount})
    end

    return hash_result
  end

  def get_wallet_balance(reward_transaction)
    reward_id = reward_transaction.reward_id
    puts "GET WALLET BALANCE----" * 100
    puts "Reward id ==== #{reward_id}"
    user_id = reward_transaction.user_id
    puts "USER ID ==== #{user_id}"

    puts "*" * 100

    rw = RewardWallet.where("user_id = #{user_id} and reward_id = #{reward_id} and status != #{RewardWallet::STATUS[:CLAIMED]}").first

     if rw.blank?
       rw = RewardWallet.create({:user_id => user_id , :reward_id => reward_id, :status => RewardWallet::STATUS[:ACTIVE]})
     end
    return rw
  end
end