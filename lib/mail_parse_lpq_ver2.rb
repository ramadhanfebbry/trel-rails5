class MailParseLpqVer2
  attr_accessor :email_body, :nok_obj
  # m = MailParseLpq.new(Comment.last.body)
  def initialize(email_body)
#     a = '<html>
#
# <script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"e6c3537574","applicationID":"393059","transactionName":"IlcIQkQKXVpVREtTXQxRCBlVClxbVVgQQRYSUAlB","queueTime":13,"applicationTime":44,"ttGuid":"","agentToken":null,"agent":"js-agent.newrelic.com/nr-632.min.js"}</script>
# <script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o?o:n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({QJf3ax:[function(e,n){function t(e){function n(n,t,a){e&&e(n,t,a),a||(a={});for(var u=c(n),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,t);return s}function a(e,n){f[e]=c(e).concat(n)}function c(e){return f[e]||[]}function u(){return t(n)}var f={};return{on:a,emit:n,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=e("gos");n.exports=t()},{gos:"7eSDFh"}],ee:[function(e,n){n.exports=e("QJf3ax")},{}],3:[function(e,n){function t(e){return function(){r(e,[(new Date).getTime()].concat(i(arguments)))}}var r=e("handle"),o=e(1),i=e(2);"undefined"==typeof window.newrelic&&(newrelic=window.NREUM);var a=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit","noticeError"];o(a,function(e,n){window.NREUM[n]=t("api-"+n)}),n.exports=window.NREUM},{1:12,2:13,handle:"D5DuLP"}],"7eSDFh":[function(e,n){function t(e,n,t){if(r.call(e,n))return e[n];var o=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return e[n]=o,o}var r=Object.prototype.hasOwnProperty;n.exports=t},{}],gos:[function(e,n){n.exports=e("7eSDFh")},{}],handle:[function(e,n){n.exports=e("D5DuLP")},{}],D5DuLP:[function(e,n){function t(e,n,t){return r.listeners(e).length?r.emit(e,n,t):(o[e]||(o[e]=[]),void o[e].push(n))}var r=e("ee").create(),o={};n.exports=t,t.ee=r,r.q=o},{ee:"QJf3ax"}],id:[function(e,n){n.exports=e("XL7HBI")},{}],XL7HBI:[function(e,n){function t(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:i(e,o,function(){return r++})}var r=1,o="nr@id",i=e("gos");n.exports=t},{gos:"7eSDFh"}],G9z0Bl:[function(e,n){function t(){var e=d.info=NREUM.info,n=f.getElementsByTagName("script")[0];if(e&&e.licenseKey&&e.applicationID&&n){c(p,function(n,t){n in e||(e[n]=t)});var t="https"===s.split(":")[0]||e.sslForHttp;d.proto=t?"https://":"http://",a("mark",["onload",i()]);var r=f.createElement("script");r.src=d.proto+e.agent,n.parentNode.insertBefore(r,n)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=e("handle"),c=e(1),u=(e(2),window),f=u.document,s=(""+location).split("?")[0],p={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-632.min.js"},d=n.exports={offset:i(),origin:s,features:{}};f.addEventListener?(f.addEventListener("DOMContentLoaded",o,!1),u.addEventListener("load",t,!1)):(f.attachEvent("onreadystatechange",r),u.attachEvent("onload",t)),a("mark",["firstbyte",i()])},{1:12,2:3,handle:"D5DuLP"}],loader:[function(e,n){n.exports=e("G9z0Bl")},{}],12:[function(e,n){function t(e,n){var t=[],o="",i=0;for(o in e)r.call(e,o)&&(t[i]=n(o,e[o]),i+=1);return t}var r=Object.prototype.hasOwnProperty;n.exports=t},{}],13:[function(e,n){function t(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(0>o?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=t},{}]},{},["G9z0Bl"]);</script><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" bgcolor="#FFFFFF">
#     <table width="100%" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
#     <tr>
#     <td valign="top" align="center" style="font-family:helvetica;">
#     <table width="600" cellpadding="30" cellspacing="0" bgcolor="#FFFFFF" border="#eeeeee" style="border:3px solid #eeeeee;">
#     <tr>
#     <td>
#     <table>
#     <tr>
#     <td colspan="2">
#     <table>
#     <tr>
#     <td valign="top" width="450"><span style="font-family:helvetica;font-size:14px;line-height:130%">Le Pain Quotidien Soho<br>100 Grand Street <br>New York, NY 10013&nbsp;<a href="http://maps.google.com/maps?client=gme-snapfinger&amp;channel=SnapfingerEmailReceipt&amp;oi=map&amp;q=100+Grand+Street%2c+New+York%2c+NY+10013">                                    View map
#     </a><br>212-625-9009</span><br><br></td>
#                           <td width="150" valign="top" align="right"><img alt="Le Pain Quotidien Soho" width="200" height="200" src="https://www.snapfinger.com/View_Image.axd?imageId=15299"></td>
#                         </tr>
#                       </table>
#                     </td>
#                   </tr>
#                   <tr>
#                     <td colspan="3"><span style="font-family:helvetica;font-size:18px"><strong>
#                               Thanks for your order, Leigh Cashman!
#                             </strong><br><br></span><span style="font-family:helvetica;font-size:14px;line-height:130%">
#                             Your contact information:<br><strong>617-372-0057<br>leighcashman@gmail.com<br><br></strong>
#                             Your pick up date and time is
#                             <strong>5/6/2015 2:15 PM</strong>.
#                             <br>
#                             Confirmation #:  <strong>45311403</strong><br><br><br>
#                               Check #:  <strong>1876</strong><br><br></span><br><br></td>
#                   </tr>
#                   <tr>
#                     <td colspan="3">
#                       <hr>
#                     </td>
#                   </tr>
#                   <tr>
#                     <td valign="top" width="450" colspan="2"><span style="font-family:helvetica;font-size:14px;line-height:130%">Avocado Toast</span></td>
#                     <td valign="top" align="right" width="150"><span style="font-family:helvetica;font-size:14px;line-height:130%">$10.50</span></td>
#                   </tr>
#                   <tr>
#                     <td colspan="3">
#                       <hr>
#                     </td>
#                   </tr>
#                   <tr>
#                     <td width="200" valign="top"><span style="font-family:helvetica;font-size:14px;line-height:130%">If you need to change your order, please contact the restaurant.</span></td>
#                     <td width="250" valign="top" align="right" nowrap=""><span style="font-family:helvetica;font-size:14px;line-height:130%">
#                             ** Payment Type<br>SubTotal<br>Tax<br>Total<br>Paid<br>Amount Due<br></span></td>
#                     <td width="150" valign="top" align="right" nowrap=""><span style="font-family:helvetica;font-size:14px;line-height:130%">Amex<br>$10.50<br>$.94<br>$11.44<br>$11.44<br>$.00<br></span></td>
#                   </tr>
#                 </table>
#               </td>
#             </tr>
#             <tr>
#               <td style="background-color:#eeeeee;" valign="top" colspan="3"><span style="font-family:helvetica;font-size:12px">
#         ** If you paid with a credit card you must present it at pick up.
#                                                                           </span></td>
#             </tr>
#     </table>
#         </td>
#     </tr>
#     </table>
#     </body>
# </html>
#         '
    @email_body = email_body
#    @email_body = a
    @nok_obj = Nokogiri.parse(@email_body)
  end

  def parse_email
    #comment = Comment.find(comment_id)
    result 	= OpenStruct.new

    result.chain 		=    parse_chain rescue nil
    result.location 	= #nok_obj.xpath('//span[@id="UnitNameLabel"]').text.squish rescue nil + nok_obj.xpath('//table//tbody//tr//td//table//tbody//tr//td//p/span')[2].text.gsub("\n","") rescue nil
        result.restaurant = parse_restaurant rescue nil
    result.order_no 	= parse_order_no rescue nil
    result.check_id 	= parse_check_id
    result.date_pickup 	= parse_date_pickup
    result.name = parse_user_name
    result.email 		= parse_user_email
    result.phone 		=  parse_phone_number
    result.orders 		= nok_obj.xpath('//table[@id="OrderDetail_OrderDetailsDataList"]').text.squish rescue nil
    result.payment_type = parse_payment rescue true
    result.subtotal 	= parse_subtotal rescue 0
    result.tax 			= parse_tax rescue 0
    result.total = parse_total rescue 0
    result.amount_paid = parse_amount_paid rescue 0
    result.balance 	= parse_balance rescue 0

    result.is_payed = !parse_payment.squish.downcase.eql?('restaurant') rescue false

    result.chain_id =  parse_chain_id
    result.restaurant_id = parse_restaurant_id
    result.user_id = parse_user_id

    # result.date_placed 	= parse_date_placed rescue nil
    # result.order_id 	= parse_order_id rescue nil
    # result.user_id 		= parse_user_id
    # result.customer_no 	= parse_customer_no rescue nil
    # result.restaurant 	= parse_restaurant
    # result.restaurant_id 	= parse_restaurant_id rescue nil
    # result.chain_id 	= parse_chain_id rescue nil

    result

    return result
  end

  def parse_user_email
    @nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[2]//td//span//strong").children[3].text.squish rescue nil
  end

  def parse_user_name
    @nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[2]/td/span").children.first.text.squish.gsub('Thanks for your order, ','').gsub('!','') rescue nil
  end

  def parse_subtotal
    page = @nok_obj.to_s
    key =   page.match(/(Type<br>SubTotal<br>Tax<br>Total<br>Paid<br>)/)[0]
    value =   page.match(/(\w*<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3})/)[0]
    key_array = key.split("<br>")
    value_array = value.split("<br>")
    if key_array.size == value_array.size
      hasil = {}
      key_array.each_with_index do |k, index|
        hasil[k] = value_array[index]
      end
      return hasil["SubTotal"]
    end
    #@nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[6]/td[3]/span").children[2].text.squish rescue nil
  end

  def parse_total
    #@nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[6]/td[3]/span").children[6].text.squish rescue nil
    page = @nok_obj.to_s
    key =   page.match(/(Type<br>SubTotal<br>Tax<br>Total<br>Paid<br>)/)[0]
    value =   page.match(/(\w*<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3})/)[0]
    key_array = key.split("<br>")
    value_array = value.split("<br>")
    if key_array.size == value_array.size
      hasil = {}
      key_array.each_with_index do |k, index|
        hasil[k] = value_array[index]
      end
      return hasil["Total"]
    end
  end

  def parse_amount_paid
    #@nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[6]/td[3]/span").children[8].text.squish rescue nil
    page = @nok_obj.to_s
    key =   page.match(/(Type<br>SubTotal<br>Tax<br>Total<br>Paid<br>)/)[0]
    value =   page.match(/(\w*<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3})/)[0]
    key_array = key.split("<br>")
    value_array = value.split("<br>")
    if key_array.size == value_array.size
      hasil = {}
      key_array.each_with_index do |k, index|
        hasil[k] = value_array[index]
      end
      return hasil["Paid"]
    end
  end

  def parse_balance
    @nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[6]/td[3]/span").children[10].text.squish rescue nil
  end

  def parse_tax
    #@nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[6]/td[3]/span").children[4].text.squish rescue nil
    page = @nok_obj.to_s
    key =   page.match(/(Type<br>SubTotal<br>Tax<br>Total<br>Paid<br>)/)[0]
    value =   page.match(/(\w*<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3})/)[0]
    key_array = key.split("<br>")
    value_array = value.split("<br>")
    if key_array.size == value_array.size
      hasil = {}
      key_array.each_with_index do |k, index|
        hasil[k] = value_array[index]
      end
      return hasil["Tax"]
    end
  end

  def parse_chain
    res = parse_restaurant.include?("Le Pain Quotidien") rescue false
    if res == true
      return "Le Pain Quotidien"
    else
      return "LPQ"
    end
  end

  def parse_payment
    #@nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[6]/td[3]/span").children.first.text rescue nil
    page = @nok_obj.to_s
    key =   page.match(/(Type<br>SubTotal<br>Tax<br>Total<br>Paid<br>)/)[0]
    value =   page.match(/(\w*<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3}<br>\$\d{0,2}.\d{0,3})/)[0]
    key_array = key.split("<br>")
    value_array = value.split("<br>")
    if key_array.size == value_array.size
      hasil = {}
      key_array.each_with_index do |k, index|
        hasil[k] = value_array[index]
      end
      return hasil["Type"]
    end
  end

  def parse_phone_number
    @nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[2]/td/span/strong[1]").children[1].text rescue nil
  end

  def parse_check_id
    @nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[2]/td/span/strong[4]").text.squish rescue nil
  end

  def parse_date_pickup
    @nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[2]/td/span/strong[2]").text.squish rescue nil
  end

  def parse_order_no
    @nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[2]/td/span/strong[3]").text.squish rescue nil
  end


  def parse_restaurant
    #nok_obj.xpath('//span[@id="UnitNameLabel"]').text.squish rescue nil
    @nok_obj.xpath("//table[@bgcolor='#FFFFFF'][@border='#eeeeee']//table//tr[1]/td/span").children.first.text rescue nil
  end

  def parse_restaurant_id
    r_id = Restaurant.active.where("lower(name) like '%#{parse_restaurant.downcase}%' and chain_id = #{parse_chain_id}").first.id rescue nil
    return r_id
  end

  def parse_chain_id
    x = parse_chain
    ch = Chain.where("lower(name) like '%#{x.downcase}%'").first.id rescue nil
    if ch.blank?
      ch = Chain.where("lower(name) like '%lpq%'").first.id rescue nil
    end
    return ch
  end

  def parse_user_id
    email = parse_user_email
    ch = parse_chain_id
    User.where("chain_id = ? AND lower(email) = ?", ch, email.downcase  ).first.id rescue nil
  end
end