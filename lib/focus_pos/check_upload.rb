module FocusPos
  class CheckUpload < ActiveRecord::Base
    self.table_name = "focus_check_uploads"

    belongs_to :pos_location
    belongs_to :chain
    belongs_to :user
    has_one :check_upload_detail, :class_name => "FocusPos::CheckUploadDetail", :foreign_key => "focus_check_upload_id"

    STATUS = {
        :NEW => 1,
        :UPDATED => 2,
        :CONVERTED => 3,
        :PROCESSED => 3
    }

    def create_detail
      focus_check = FocusPos::Check.new(self.check_data)
      detail = FocusPos::CheckUploadDetail.find_or_initialize_by_focus_check_upload_id(self.id)
      detail.ticket_id = focus_check.ticket_id
      detail.total = focus_check.total
      detail.check_date = focus_check.receipt_date
      detail.employee_num = focus_check.employee_num
      detail.save
    end

    def status_name
      PosCheckUpload::STATUS.invert[self.status]
    end

  end

end
