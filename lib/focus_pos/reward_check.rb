module FocusPos
  class RewardCheck < ActiveRecord::Base
    self.table_name = "focus_reward_checks"

    belongs_to :pos_location
    belongs_to :chain
    belongs_to :user

    STATUS = {"SUCCESS" => 1, "ERROR" => 2}

  end
end
