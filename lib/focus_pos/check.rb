# encoding: utf-8
require 'nokogiri'
require 'date'
require 'time'
module FocusPos
  class Check
    attr_accessor :json, :error_messages, :converted_date, :converted_time

    def initialize(json)
      @json = JSON.load(json)
      @error_messages = {}
      @converted_date = nil
      @converted_time = nil
    end

    def check_id
      begin
        pos_location = PosLocation.where(:apikey => @json["apikey"]).first
        ticket_id = @json["ticket_id"].split("-")
        t_num = ticket_id[0]
        t_year = ticket_id[1]
        t_month = ticket_id[2]
        t_date = ticket_id[3]
        check_id = "#{pos_location.restaurant_id}-#{t_num}-#{t_year}-#{t_month}-#{t_date}"
        return check_id
      rescue
        return nil
      end

    end

    def ticket_id
      @json["ticket_id"] rescue nil
    end

    def seq_num
      @json["SeqNum"].to_s rescue nil
    end

    def discount_total
      nil
    end

    def total
      begin
        #@xml_in_nokogiri_format.xpath("//total").text.to_decimal
        total_amount_text = @json["revenue"].to_s
        BigDecimal.new(total_amount_text)
          #if total_amount == 0
          #  total_amount = self.sub_total + self.tax
          #end
      rescue
        return nil
      end
    end

    def receipt_date
      #@xml_in_nokogiri_format.xpath("//date").text
      date = @json["ticket_id"].split("-") rescue nil
      selected_date = "#{date[2]}/#{date[3]}/#{date[1].last(2)} #{date[4]}:#{date[5]}:00" rescue nil
      if selected_date.blank?
        selected_date = Time.zone.now.strftime("%m/%d/%y %H:%M:%S")
      end
      selected_date
    end

    def receipt_number
      self.check_id
    end

    def employee_num
      @json["employee_id"].to_s rescue nil
    end

    def valid_total?(offer_id=nil,check_upload = nil)
      valid = true
      return false if self.total.blank?

      if self.total.to_f == 0.0 # offer rule purpose forgot loyalty items
        offer = Offer.find(offer_id) rescue nil
        # check the rule first
        if !offer.blank? and offer.is_zero_subtotal?
          g_items = []
          unless offer.offer_rules_subtotal_zeros.blank?
            offer.offer_rules_subtotal_zeros.each do |zero_rules|
              g_items << zero_rules.offer_rule_menu_items
            end
          end
          g_items = g_items.flatten
          #item_numbers = g_items.pos_menu_items.map(&:item_number)
          item_numbers = []
          g_items.each do |g_item|
            item_numbers += g_item.general_menu_item.pos_menu_items.map(&:item_number)
          end
          offer_item_parser = Parser::OfferItemsParser.new(check_upload)
          offer_item_parser.xml_data = @xml
          result_with_qty = offer_item_parser.find_item_number_and_quantity
          #purchased_item_match = []

          unless result_with_qty.blank?
            result_with_qty.each do |res|
              # if the purchase are correct
              if item_numbers.include?(res[:id].to_i)
                return valid # there are item match with the zero subtotal ( forgot loyalty item)
              end
            end
          end

        end
      end
      #unless self.total > 0
      #  valid = false
      #  @error_messages[:total] = "Total should greater than 0"
      #  return valid
      #end

      unless self.total > 0
        valid = false
        @error_messages[:total] = "Sub Total less than 0"
        return valid
      end

      #unless (self.total.round(3)).eql?((self.tax + self.sub_total).round(3))
      #  valid = false
      #  @error_messages[:total] = "SubTotal + Tax not equal Total"
      #  return valid
      #end
      #unless self.total >= self.sub_total
      #  valid = false
      #  @error_messages[:total] = "Total less than subtotal"
      #  return valid
      #end
      return valid
    end

    def valid_date?
      begin
        if /^(\d{1,2})\/(\d{1,2})\/(\d{2,4})/i =~ self.receipt_date
          @converted_date = DateTime.strptime(self.receipt_date, "%m/%d/%y %H:%M:%S")
          @converted_time = @converted_date.strftime("%I:%M%p")
          #if @converted_date < Date.current - Setting.ocr.expired_days.days
          #  @error_messages[:date] = "Receipt is expired"
          #  return false
          #end
          return true
        else
          @error_messages[:date] = "Invalid date"
          return false
        end
      rescue
        @error_messages[:date] = "Invalid date"
        return false
      end
    end

    def list_item
      sku_items = @json["ArrayOfSKUItem"]["SKUItem"]
      items = []
      if sku_items.class.eql?(Array)
        @json["ArrayOfSKUItem"]["SKUItem"].each do |menu_item|
          item = {}
          item["id"] = menu_item["ID"]
          item["name"] = URI.unescape(menu_item["Description"])
          item["amount"] = menu_item["Price"].to_f
          item["qty"] = menu_item["Qty"]
          items << item
        end
      elsif
      item = {}
        item["id"] = sku_items["ID"]
        item["name"] = URI.unescape(sku_items["Description"])
        item["amount"] = sku_items["Price"].to_f
        item["qty"] = sku_items["Qty"]
        items << item
      end
      return items
    end
  end
end
