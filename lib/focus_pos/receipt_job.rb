module FocusPos
  class ReceiptJob < Struct.new(:pos_check_upload_id, :user_id, :restaurant_id, :offer_id, :receipt_id)

    def perform
      focus_parser = FocusPos::CheckParser.new(pos_check_upload_id, user_id, restaurant_id, offer_id, receipt_id)
      focus_parser.do_parse
    end

  end
end
