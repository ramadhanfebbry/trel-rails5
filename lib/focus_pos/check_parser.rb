module FocusPos
  class CheckParser
    attr_accessor :pos_check_upload_id, :user_id, :restaurant_id, :offer_id, :receipt_id, :parsed_receipt, :json_check

    def initialize(pos_check_upload_id, user_id, restaurant_id, offer_id, receipt_id)
      @pos_check_upload_id = pos_check_upload_id
      @user_id = user_id
      @restaurant_id = restaurant_id
      @offer_id = offer_id
      @receipt_id = receipt_id
      @parsed_receipt = nil
      @json_check = nil
    end

    def do_parse
      puts "start parse json checkk focus data from pos check upload"
      puts "create receipt received record"

      p @receipt_id
      receipt = Receipt.find(@receipt_id)
      @user = User.find(@user_id)
      @pos_check_upload = FocusPos::CheckUpload.find(@pos_check_upload_id)
      json_check = @pos_check_upload.check_data
      focus_check = FocusPos::Check.new(json_check)
      chain = @user.chain
      location = Restaurant.find(@restaurant_id)
      restaurant_offer = RestaurantOffer.where(:restaurant_id => @restaurant_id, :offer_id => @offer_id).first rescue nil
      offer = Offer.find(@offer_id)

      #LET RECEIPT IN RECEIVED STATUS WHEN FOCUS DID NOT HIT CLOSE BY SKU API CALL
      return if json_check.blank?

      receipt_transaction = ReceiptTransaction.new
      receipt_transaction.restaurant_offer_id = (restaurant_offer.id rescue nil)
      receipt_transaction.restaurant_id = @restaurant_id

      valid_total = focus_check.valid_total?(@offer_id,@pos_check_upload)
      valid_date = focus_check.valid_date?

      p "validation total and date"
      p "valid_total #{valid_total}"
      p "valid_date #{valid_date}"
      p "--------------"

      receipt_transaction.subtotal = focus_check.total
      receipt_transaction.issue_date = focus_check.converted_date
      receipt_transaction.time_stamp = (focus_check.converted_time.strip rescue nil)
      receipt_transaction.receipt_date = focus_check.converted_date
      receipt_transaction.receipt_time = (focus_check.converted_time.strip rescue nil)

      if valid_total and valid_date
        if unique_receipt?(chain, focus_check)
          if !chain.blank? and chain.today_max_user_limit?(receipt, focus_check.converted_date)
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            focus_check.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
          elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, focus_check.converted_date, offer)
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            focus_check.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
          else
            receipt_transaction.status = Receipt::STATUS[:APPROVED]
            receipt.status = Receipt::STATUS[:APPROVED]
          end
        else
          if location
            if unique_receipt?(chain, focus_check, location)
              if !chain.blank? and chain.today_max_user_limit?(receipt, focus_check.converted_date)
                receipt_transaction.status = Receipt::STATUS[:REJECTED]
                receipt.status = Receipt::STATUS[:REJECTED]
                focus_check.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
              elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, focus_check.converted_date, offer)
                receipt_transaction.status = Receipt::STATUS[:REJECTED]
                receipt.status = Receipt::STATUS[:REJECTED]
                focus_check.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
              else
                receipt_transaction.status = Receipt::STATUS[:APPROVED]
                receipt.status = Receipt::STATUS[:APPROVED]
              end
            elsif unique_receipt?(chain, focus_check, location, focus_check.receipt_number)
              if !chain.blank? and chain.today_max_user_limit?(receipt, focus_check.converted_date)
                receipt_transaction.status = Receipt::STATUS[:REJECTED]
                receipt.status = Receipt::STATUS[:REJECTED]
                focus_check.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
              elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, focus_check.converted_date, offer)
                receipt_transaction.status = Receipt::STATUS[:REJECTED]
                receipt.status = Receipt::STATUS[:REJECTED]
                focus_check.error_messages[:maximum_offer_points_earned_per_day] = "PER DAY OFFER POINTS LIMIT CROSSED"
              else
                receipt_transaction.status = Receipt::STATUS[:APPROVED]
                receipt.status = Receipt::STATUS[:APPROVED]
              end
            else
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              focus_check.error_messages[:receipt] = "Receipt should be uniq, this receipt have submitted before"
              ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, focus_check.error_messages, receipt).deliver
            end
          else
            focus_check.error_messages[:restaurant] = "Restaurant/Location is invalid."
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, focus_check.error_messages, receipt).deliver
          end
        end
      else
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.status = Receipt::STATUS[:REJECTED]
        ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, focus_check.error_messages, receipt).deliver
      end
      receipt_transaction.instructions = focus_check.error_messages
      receipt_transaction.receipt_number = focus_check.receipt_number
      receipt_transaction.restaurant_id = location ? location.id : nil
      receipt_transaction.subtotal = focus_check.total
      p "this is receipt transaction object"
      p receipt_transaction

      ## end offer rule

      receipt.receipt_transactions << receipt_transaction
      ## offer rules validation not met criteria
      ## if the rules not met

      begin
        if receipt_transaction.offer and receipt_transaction.offer.offer_rules.size > 0 # doe not met the rules
          puts "OFFER RULE:: NCR ONLINE ORDER PROCESS JOB start ---" * 19

          off_rule = OfferRuleCalculation::CalculateOfferRule.new(receipt_transaction.offer, receipt_transaction)
          off_rule.result
          tmp_total_points_earned = off_rule.total_points_get
          if tmp_total_points_earned.to_f == 0.0 || tmp_total_points_earned == -1 || tmp_total_points_earned == -2
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt_transaction.instructions = {:offer_rules_not_met => "This receipt does not met any rules"}
            receipt_transaction.instructions = {:ignore_item_list => "This receipt get all the ignore items listed"} if tmp_total_points_earned == -1
            receipt_transaction.instructions = {:ignore_item_reject => "Rejected due the item"} if tmp_total_points_earned == -2
            receipt.status = Receipt::STATUS[:REJECTED]
          end
          puts "OFFER RULE:: Focus check PROCESS JOB end ---" * 19
        end
      rescue => e
        puts "CALCULATE::OFFERRULES::focus check ----> #{e.inspect}"
      end

      receipt.save
      ticket_id = focus_check.ticket_id.split("-") rescue nil
      t_year = ticket_id[1] rescue nil
      t_month = ticket_id[2] rescue nil
      t_date = ticket_id[3] rescue nil
      t_hour = ticket_id[4] rescue nil
      t_mins = ticket_id[5] rescue nil
      tdate = "#{t_year}/#{t_month}/#{t_date}" if !t_year.nil? && !t_month.nil? && !t_date.nil?
      ttime = "#{t_hour}:#{t_mins}:00" if !t_hour.nil? && !t_mins.nil?
      if !t_date.nil? && !ttime.nil?
        ReceiptProcess::ReceiptSetIssuedate.set_date(receipt.id, {:issue_date => tdate, :issue_time => ttime})
      else
        ReceiptProcess::ReceiptSetIssuedate.setdatetime(receipt.id, focus_check.converted_date)
      end

      p "this is receipt final status #{receipt.status}"
      if receipt.status == Receipt::STATUS[:APPROVED]
        "because receipt final status is approved, update pos check upload to approved"
        @pos_check_upload.update_attributes(
            :user_id => @user.id,
            :status => PosCheckUpload::STATUS[:PROCESSED]
        )
      end
      p "this is final pos check upload object"
      p @pos_check_upload
      puts "------ end ------"
    end

    def unique_receipt?(chain, ocr_xml, location = nil, receipt_number = nil)
      p "UNIQ RECEIPT?"
      p ocr_xml.total.to_f
      p ocr_xml.converted_date.in_time_zone(Time.zone).strftime('%Y-%m-%d')
      p ocr_xml.converted_time.strip
      p location
      p receipt_number
      p "=========aaaa"
      if location.blank? and receipt_number.blank?
        receipt_transaction_count = ReceiptApprovedDetail.select("id").where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ?", chain.id, ocr_xml.total, ocr_xml.converted_date, ocr_xml.converted_time.strip).count
        return true if receipt_transaction_count == 0
        return false
      elsif location and receipt_number.blank?
        receipt_transaction_count = ReceiptApprovedDetail.select("id").where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ?", chain.id, ocr_xml.total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id).count
        return true if receipt_transaction_count == 0
        return false
      elsif receipt_number
        receipt_transaction_count = ReceiptApprovedDetail.select("id").where("chain_id = ? AND subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ? AND receipt_number = ?", chain.id, ocr_xml.total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id, receipt_number).count
        return true if receipt_transaction_count == 0
        return false
      else
        return false
      end
    end

  end

end

