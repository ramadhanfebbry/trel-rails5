module ActiveRecordExtensions
  class PosCheckUpload < ActiveRecord::Base

    STATUS = {
        :NEW => 1,
        :UPDATED => 2,
        :CONVERTED => 3,
        :PROCESSED => 3
    }

    SERVICE = {
        :TABLE => 1,
        :COUNTER => 2
    }

    CHECK_STATE_STATUS = {
        :CHECK_RECEIVED_UNPAID => 1,
        :CHECK_RECEIVED_PAID => 2,
        :CHECK_SCANNED_UNPAID => 3,
        :CHECK_SCANNED_PAID => 4
    }
    #need to switch to the shard database connection from heroku config
    follower_database_url =  ENV["HEROKU_POSTGRESQL_WHITE_URL"]


    if(!follower_database_url.nil?)
      parsed_connection_string = follower_database_url.split("://")
      adapter = parsed_connection_string[0]
      parsed_connection_string = parsed_connection_string[1].split(":")
      username = parsed_connection_string[0]
      parsed_connection_string = parsed_connection_string[1].split("@")
      password = parsed_connection_string[0]
      parsed_connection_string = parsed_connection_string[1].split("/")
      host = parsed_connection_string[0]
      parsed_connection_string = follower_database_url.split("://")
      parsed_connection_string = parsed_connection_string[1].split(":")
      database = parsed_connection_string[2].split('/')[1]
      port =parsed_connection_string[2].split('/')[0]

      establish_connection(
          :adapter  => 'postgresql',
          :host     => host,
          :username => username,
          :password => password,
          :database => database,
          :port     => port
      )
    else
      self.establish_connection "shard_#{Rails.env}"
    end

    def user
      ActiveRecordExtensions::PosCheckUpload.find_by_sql("select * from users where id = #{self.user_id}").first rescue nil
    end

    def pos_location
      ActiveRecordExtensions::PosCheckUpload.find_by_sql("select * from pos_locations where pos_locations.id = #{self.pos_location_id}").first rescue nil
    end

    def restaurant
      ActiveRecordExtensions::PosCheckUpload.find_by_sql("select * from restaurants where restaurants.id = #{self.restaurant_id}").first rescue nil
    end

    def status_name
      PosCheckUpload::STATUS.invert[self.status]
    end

    def service_name
      PosCheckUpload::SERVICE.invert[self.service]
    end
  end


  class User < ActiveRecord::Base

    STATUS = {
        :NEW => 1,
        :UPDATED => 2,
        :CONVERTED => 3,
        :PROCESSED => 3
    }

    SERVICE = {
        :TABLE => 1,
        :COUNTER => 2
    }

    CHECK_STATE_STATUS = {
        :CHECK_RECEIVED_UNPAID => 1,
        :CHECK_RECEIVED_PAID => 2,
        :CHECK_SCANNED_UNPAID => 3,
        :CHECK_SCANNED_PAID => 4
    }
    puts "SINI BOOOI"
    #need to switch to the shard database connection from heroku config
    follower_database_url = ENV["HEROKU_POSTGRESQL_WHITE_URL"]


    if(!follower_database_url.nil?)
      parsed_connection_string = follower_database_url.split("://")
      adapter = parsed_connection_string[0]
      parsed_connection_string = parsed_connection_string[1].split(":")
      username = parsed_connection_string[0]
      parsed_connection_string = parsed_connection_string[1].split("@")
      password = parsed_connection_string[0]
      parsed_connection_string = parsed_connection_string[1].split("/")
      host = parsed_connection_string[0]
      parsed_connection_string = follower_database_url.split("://")
      parsed_connection_string = parsed_connection_string[1].split(":")
      database = parsed_connection_string[2].split('/')[1]
      port =parsed_connection_string[2].split('/')[0]

      establish_connection(
          :adapter  => 'postgresql',
          :host     => host,
          :username => username,
          :password => password,
          :database => database,
          :port     => port
      )
    else
      self.establish_connection "shard_#{Rails.env}"
    end

    def user
      ActiveRecordExtensions::PosCheckUpload.find_by_sql("select * from users where id = #{self.user_id}").first rescue nil
    end

    def pos_location
      ActiveRecordExtensions::PosCheckUpload.find_by_sql("select * from pos_locations where pos_locations.id = #{self.pos_location_id}").first rescue nil
    end

    def restaurant
      ActiveRecordExtensions::PosCheckUpload.find_by_sql("select * from restaurants where restaurants.id = #{self.restaurant_id}").first rescue nil
    end

    def status_name
      PosCheckUpload::STATUS.invert[self.status]
    end

    def service_name
      PosCheckUpload::SERVICE.invert[self.service]
    end
  end

  class ShardMigration < ActiveRecord::Migration[5.0]
    def connection
      ActiveRecord::Shard.connection
    end
  end
end