module CoordinateFromAddress
  extend ActiveSupport::Concern

  included do
    before_validation :get_coordinate
  end

  module ClassMethods
  end

  # This method perform getting coordinate (latitude and longitude)
  # from whether, address, city, state, and zipcode inputed,
  # if there was no value inputed for latitude and longitude field inside an instance
  def get_coordinate

    # If Latitude or Longitude is blank,
    # begin Geocoder to perform lookup for it's coordinates
    # by Address, City, State + Zip
    if self.latitude.blank? || self.longitude.blank?

      # Ignore blank address, city_import, state_import, or zipcode,
      # if one of them is blank than avoid join with comas, to avoid multiple comas without string between them
      arr_str_address = [self.address, self.city.try(:name), self.region.try(:name), self.zipcode]
      arr_str_address.reject!(&:blank?)

      puts '======================== BLANK COORDINATES ==================='
      puts "#{arr_str_address.join(', ')}"

      # If one of them (address, city_import, state_import, or zipcode) filled then perform geocoding
      unless arr_str_address.blank?
        geo_lookup = Geokit::Geocoders::Google3Geocoder.geocode(arr_str_address.join(', '))
        puts "geo_lookup status : #{geo_lookup.success}"

        # Assign coordinates from geocoder lookup above,
        # put 0 (zero) instead if no result but success is true
        if geo_lookup.success
          puts "Coordinate found at : #{geo_lookup.ll}"
        else
          puts "Couldn't search address string : #{arr_str_address.join(', ')}"
        end

        self.latitude = geo_lookup.lat.blank? ? 0 : geo_lookup.lat
        self.longitude = geo_lookup.lng.blank? ? 0 : geo_lookup.lng
      end
    else
      puts '======================== FILLED COORDINATES ==================='
      # Assign by value inputed by user
      self.latitude = self.latitude rescue 0 #.try(:round,6)
      self.longitude = self.longitude rescue 0 #.try(:round,6)
    end
  end
end