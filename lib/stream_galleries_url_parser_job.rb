class StreamGalleriesUrlParserJob < Struct.new(:stream_galleries_image_id)

  def perform
    begin
      stream_gallery_image = StreamGalleryImage.find(stream_galleries_image_id)
      # doc = Nokogiri::HTML(open(stream_gallery_image.feed_link))
      uri = URI.parse(stream_gallery_image.feed_link)
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      image_feed_lists = []
      if response.code == "200"
        doc = Nokogiri::XML(response.body)
        doc.search("feed-item").each do |image_feed_list|
          image_feed_lists << {:hyperlink => image_feed_list.search("link").text, 
            :image_url => image_feed_list.search("image").text
          } unless image_feed_list.search("link").text.blank? && image_feed_list.search("image").text.blank?
        end
      end
        # stream_gallery_image.update_attribute(:image_feed_lists, image_feed_lists) unless image_feed_list.blank?
      stream_gallery_image.update_attribute(:image_feed_lists, image_feed_lists)
    rescue
      image_feed_lists = []
      stream_gallery_image.update_attribute(:image_feed_lists, image_feed_lists)
    end
   
  end
end