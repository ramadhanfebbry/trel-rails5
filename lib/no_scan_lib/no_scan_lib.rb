class NoScanLib::NoScanLib < Struct.new(:job)
  def perform
    ## locked the job
    job.update_column(:status, 2)
    # last method before editted
    #job.execute_job
    execute_job(job)
  end

  def success_callback(job)
    if job.schedule_type == 1
      job.update_column(:status, 3)
    else
      complete = job.is_job_complete?
      if complete
        job.update_column(:status, 3)
      else
        job.update_column(:status, 0)
      end
    end
  end

  def error(job, exception)
    #job.update_column(:status,4)
    #OwnerJob.where("id = ?", job.delayable_id).first.update_column(:status, 4)

    #OwnerJob.where("id = ?", job.delayable_id).first.update_column(:status, 4)
    OwnerMailer.db_job_error_notification("JobFromOwner", job.id,exception).deliver! #rescue nil
  end


  def execute_job(job)
   execute_no_scan(job)
  end

  def execute_no_scan(job)
    sql = sql_for_no_scan(job)

    offset = 0
    limit = 500
    total_user = 0

    begin
      results = ActiveRecordExtensions::User.find_by_sql("#{sql} OFFSET #{offset} LIMIT #{limit}")
      offset += limit
      puts "ofset #{offset}"
      total_user += results.size
      # Do stuff here
     push_reward_or_point(job, results)

    end while !results.blank?

    job.update_column(:total_users, total_user)
  end

  ## standard job
  def execute_standard(job)
    sql = sql_for_standard_new(job)

    offset = 0
    limit = 500
    total_user = 0

    begin
      results = User.find_by_sql("#{sql} OFFSET #{offset} LIMIT #{limit}")
      offset += limit
      puts "ofset #{offset}"
      total_user += results.size
      # Do stuff here
      push_reward_or_point(job, results)

    end while !results.blank?

    job.update_column(:total_users, total_user)
  end
  #end standard

  ## miss you job
  def execute_miss_you(job)
    #sql = sql_for_miss_you(job)
    sql = sql_for_miss_you_new(job)

    offset = 0
    limit = 500
    total_user = 0

    begin
      results = User.find_by_sql("#{sql} OFFSET #{offset} LIMIT #{limit}")
      offset += limit
      puts "ofset #{offset}"
      total_user += results.size
      # Do stuff here

      push_reward_or_point(job, results)


    end while !results.blank?

    job.update_column(:total_users, total_user)
  end

  def sql_for_no_scan(job, count = nil)
    owner = Owner.find(job.owner_id) rescue nil
    chain = owner.chain rescue job.chain
    restaurant_ids = owner.chain.restaurants.map(&:id) rescue chain.restaurants.map(&:id)

    sql = "SELECT users.*, (SELECT count(receipts.id) FROM receipts where receipts.user_id = users.id) as
        count_receipt FROM users WHERE no_scan_date is NULL AND DATE(users.created_at AT TIME ZONE 'EST') <= DATE(now() - interval '#{job.percentage} days')
        AND users.chain_id = #{job.chain.id} and (SELECT count(receipts.id) FROM receipts where receipts.user_id = users.id) = 0  and users.deleted_at is null order by created_at desc"
    return sql
  end

  def sql_for_standard_new(job, count = nil)
    owner = Owner.find(job.owner_id) rescue nil
    chain = owner.chain rescue job.chain
    restaurant_ids = owner.chain.restaurants.map(&:id) rescue chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    #### sql initialize
    condition_weeks = "(date(last_active) <= date('#{Time.now - job.percentage.weeks}') AND
                        date(last_redeem) <= date('#{Time.now - job.percentage.weeks}'))" if job.job_type == 1

    condition_weeks = "(date(last_active) > date('#{Time.now - job.activity_days.weeks}') AND
                        date(last_redeem) > date('#{Time.now - job.activity_days.weeks}'))" if job.job_type == 2

    select_condition = "SELECT distinct on (user_id) user_id as id, users.* FROM restaurant_users"
    select_condition = "SELECT count(distinct(user_id))  FROM restaurant_users" unless count.blank?

    sql = "
    #{select_condition}
    INNER JOIN  users on users.id = restaurant_users.user_id
    WHERE users.active = 't ' AND users.chain_id = #{chain.id} AND
    #{condition_weeks} AND
    restaurant_users.restaurant_id IN (#{restaurant_id_sql})
    "

    sql = "select users.* from users where chain_id = #{chain.id} and users.active = 't' " if job.job_type == 3
    return sql

  end


  def sql_for_miss_you_new(job, count = nil)
    owner = Owner.find(job.owner_id) rescue nil
    chain = owner.chain rescue job.chain
    restaurant_ids = owner.chain.restaurants.map(&:id) rescue chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    #### sql initialize
    condition_weeks = "(date(last_active) <= date('#{Time.now - job.percentage.weeks}') AND
                        date(last_redeem) <= date('#{Time.now - job.percentage.weeks}'))" if job.job_type == 1

    condition_weeks = "(date(last_active) > date('#{Time.now - job.activity_days.weeks}') AND
                        date(last_redeem) > date('#{Time.now - job.activity_days.weeks}'))" if job.job_type == 2

    select_condition = "SELECT distinct on (user_id) user_id as id, users.* FROM restaurant_users"
    select_condition = "SELECT count(distinct(user_id))  FROM restaurant_users" unless count.blank?

    sql = "
    #{select_condition}
    INNER JOIN  users on users.id = restaurant_users.user_id
    WHERE users.active = 't ' AND users.chain_id = #{chain.id} AND
    #{condition_weeks} AND
    (users.miss_you_date is null) AND
    restaurant_users.restaurant_id IN (#{restaurant_id_sql})
    "

    sql = "select users.* from users where chain_id = #{chain.id} and users.active = 't' " if job.job_type == 3
    return sql
  end

  # end miss you job


  def push_reward_or_point(job, users)
    #puts "push reward or point " * 100

    puts "---------------------"
    puts "job"
    puts "users === #{users.size}"
    success_user_ids = []
    failed_user_ids = []
    sending_emails = []

    case job.reward_type
      when 1
        ## push reward
        selected_reward = Reward.find(job.reward_id)
        tmp_expired_at = RewardWallet.get_expiry_date(selected_reward)
        tmp_expired_at = tmp_expired_at.strftime("%m/%d/%Y")

        users.each do |user|
          user = User.find user.id
          is_pushed = user.no_scan_date#RestaurantUser.where(:user_id => user.id).map(&:miss_you_date) rescue []
          next if is_pushed.present?
          begin
            puts "success job push reward for #{user.id}"
            RewardWallet.create_with_delay({:reward_id => job.reward_id, :user_id => user.id,
                                            :description => "No Scan push"},
                                           true) ## do not send pn for this type reward

            success_notif = NoscanjobLog::NOTIFICATION_STATUS["SUCCESS"]
            log = NoscanjobLog.new(:user_id => user.id,
                                  :email => user.email,
                                  :date_sent => Time.zone.now,
                                  :notification_status => success_notif,
                                  :no_scan_job_id => job.id,
                                  :status => NoscanjobLog::STATUS["SUCCESS"]
            )
            log.save
            sending_emails << user
            job.update_miss_you(user) ## update miss you column
          rescue => e
            puts "OWNERJOB::JOBFROMOWNER::PUSHREWARD => #{e.inspect}"
            success_notif = NoscanjobLog::NOTIFICATION_STATUS["FAILED"]
            log = NoscanjobLog.new(:user_id => user.id,
                                  :email => user.email,
                                  :date_sent => Time.zone.now,
                                  :notification_status => success_notif,
                                  :no_scan_job_id => job.id,
                                  :status => NoscanjobLog::STATUS["FAILED"]
            ) rescue nil
            log.save  rescue nil
            next
          end
        end

        ## notification

        if (job.notification and job.push_email)  or job.is_fishbowl == true
          sending_emails.in_groups_of(500).each do |split_users|
            job.owner_job_push_email(split_users.compact,tmp_expired_at)
          end
        end
        failed_id = []
        if (job.notification and job.push_phone)  or job.is_fishbowl == true
          sending_emails.in_groups_of(500).each do |g_users|
            failed_id = push_notification(g_users.compact, job.content_phone, job.chain_id)
          end

          NoscanjobLog.update_all("notification_status = 2",["user_id in (?)",failed_id.flatten.uniq.compact])
        end

      ################# END CASE 1
      when 2
        ## points
        unless users.blank?
          users.each do |user|
            user = User.find user.id
            is_pushed = user.no_scan_date#RestaurantUser.where(:user_id => user.id).map(&:miss_you_date) rescue []
            next if is_pushed.present? #and job.miss_you? # only once for this miss you task
            begin
              user.earn(job.points, nil, "Owner")
              PointHistory.add_to_history(user.id, "No Scan push", job.points)
              #puts "earch #{points} point to user #{user.email} from owner dashboard "
              sending_emails << user
              success_notif = NoscanjobLog::NOTIFICATION_STATUS["SUCCESS"]

              log = NoscanjobLog.new(:user_id => user.id,
                                    :email => user.email,
                                    :date_sent => Time.zone.now,
                                    :notification_status => success_notif,
                                    :no_scan_job_id => job.id,
                                    :status => NoscanjobLog::STATUS["SUCCESS"]
              )
              log.save
              job.update_miss_you(user) ## update miss you column
            rescue => e
              puts "OWNERJOB::JOBFROMOWNER::PUSHPOINT => #{e.inspect}"

              success_notif = OwnerjobLog::NOTIFICATION_STATUS["FAILED"]
              log = NoscanjobLog.new(:user_id => user.id,
                                    :email => user.email,
                                    :date_sent => Time.zone.now,
                                    :notification_status => success_notif,
                                    :no_scan_job_id => job.id,
                                    :status => OwnerjobLog::STATUS["FAILED"]
              ) rescue nil
              log.save rescue nil
              #failed_user_ids << user.id
              next
            end
          end
        end

        ## notification


        if (job.notification and job.push_email) or job.is_fishbowl == true
          sending_emails.in_groups_of(500).each do |split_users|
            job.owner_job_push_email(split_users.compact)
          end
        end

        failed_id = []
        if job.notification and job.push_phone
          sending_emails.in_groups_of(500).each do |g_users|
            failed_id = push_notification(g_users.compact, job.content_phone,job.chain_id)
            NoscanjobLog.update_all("notification_status = 2",["user_id in (?)",failed_id.flatten.uniq.compact])
          end
        end

      #failed_id.compact.flatten.each do |u_id|
      #  tmp = success_user_ids.select{|a| a[:id] == u_id } rescue {}
      #  puts "#{failed_id}"
      #  puts "tmp =========== #{tmp}"
      #  success_user_ids.delete_if{|a| a[:id] == u_id }
      #  tmp[:success_notif] = "FALSE"  if !tmp.blank? and tmp.class == Hash and !tmp[:success_notif].blank?
      #  success_user_ids << tmp unless tmp.blank?
      #end
      when 3
        puts "------------------"
    end

    #history = OwnerJobHistory.where(:owner_job_id => job.id).first rescue []

    #if history.blank?
    #  OwnerJobHistory.create(:owner_job_id => job.id,
    #                         :success => success_user_ids,
    #                         :failed => failed_user_ids
    #  )
    #else
    #  data_success_before = history.success

    #  history.update_attributes(:success => data_success_before + success_user_ids,
    #                            :failed => failed_user_ids)
    #end
    #self.update_attribute(:status, 3)
    success_callback(job)
    #puts "Done processing #{job.description} Jobs"
    puts "*" * 50
    puts "push reward or point Done" * 100
  end


  def push_notification(user_tmp, content,chain_id)
    puts "perform owner dashboard device job"
    locales = Chain.find(chain_id).locales
    success = nil
    locales.each do |lc|
      ## android Push notif
      users = User.where("users.id IN (?) and users.active is true and locales.key = ? AND chain_id in (?) AND users.sign_in_device_type = 'android'",
                         user_tmp, lc.key, chain_id).joins(:locale)

      android_tokens = users.map(&:device_token).compact
      puts "------ here ------- #{android_tokens}"
      failed_users = push_plain_to_android(android_tokens, content,chain_id) unless android_tokens.blank?


      ## iphone push notif
      users = User.where("users.id IN (?) and users.active is true  and locales.key = ?
                        AND chain_id in (?) AND lower(users.sign_in_device_type) =
                        'iphone'", user_tmp, lc.key, chain_id).
          joins(:locale)

      iphone_tokens = users.map(&:device_token).compact
      failed_users_iphone = push_plain_to_iphone(iphone_tokens, content,chain_id) unless iphone_tokens.blank?

      puts "push notification success"
      return (failed_users.to_a + failed_users_iphone.to_a)
    end

  end


  def push_plain_to_iphone(device_tokens, message,chain_id)
    chain = Chain.find(chain_id)
    failed_users = []
    if application = chain.applications.by_device_type(2).first
      message = message.blank? ? "" : message
      device_tokens.each do |device_token|
        ## logging
        begin
          unless device_token.blank?
            user = User.find_by_device_token(device_token) rescue nil
            user_id = user.id rescue nil
            if user && user.chain_id == chain.id
              rapns_app = application.rapns_app
              return if rapns_app.blank?
              n = Rpush::Apns::Notification.new
              n.app = rapns_app
              n.device_token = device_token
              n.alert = message
              n.sound = "1.aiff"
              n.save!
              p user.id
              p user.email
              #HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 1)
              p "iphone success user -------"
            elsif user
              p user.id
              p user.email
              #HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "User not included on chain selected.")
              p "iphone failed user not in chain-----"
              failed_users << user.id
            else
              #HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "User not found device token #{device_token}")
              p "ihpne failed user not found"
              failed_users << user.id
            end
          else
            #HistoryNotification.create(:user_id => 0, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "blank device token")
          end
        rescue => e
          puts "IPHONE PLAINPUSHNOTIFICATION::History Plain logging #{e.inspect}"
          user_id = User.find_by_device_token(device_token)
          if user_id.blank?
            user_id = 0
            user_text = "User not found device token #{device_token}"
          else
            user_id = user_id.id
            user_text = e.inspect
          end

          p "iphone failed user from begin rescue"
          failed_users << user.id
          next
        end
      end
    end
    return failed_users.compact.flatten
  end


  def push_plain_to_android(reg_ids, notification,chain_id)
    chain = Chain.find(chain_id)
    puts "PUSH PLAIN ANDROID"
    failed_users = []
    if application = chain.applications.by_device_type(1).first
      return if application.key.blank?
      message = GcmHelper::Message.new
      message.delay_while_idle = true
      message.add_data('alert', notification)
      message.add_data('timestamp', "#{Time.now}")
      #      key="AIzaSyCHAQW9i1vGKNUD83jSsSn8Jfgb2TPFGKc"
      sender = GcmHelper::Sender.new(application.key)
      response = sender.multicast_with_retry(message, reg_ids, 3)

      ## logging

      success_reg_ids = reg_ids#response.success
      begin
        success_reg_ids.each do |sc|
          begin
            unless sc.blank?
              user = User.find_by_device_token(sc) rescue nil
              if user && user.chain.id == chain.id
                user_id = user.id rescue nil
                p user.id
                p user.email
                #HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 1)
                puts "ANDROID PUSHPLAINNOTIFICATION SUCCESS LOG CREATED"
                p "-----success------"
              elsif user
                p user.id
                p user.email
                p "android failed user----"
                failed_users << user.id
                # HistoryNotification.create(:user_id => user.id, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "User not included on chain selected.")
              else
                p "---- android dt not found---"
                failed_users << user.id
                #  HistoryNotification.create(:user_id => 0, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "User not found device token #{sc}")
              end
            else
              p "---- android dt is blank"
              failed_users << user.id
              # HistoryNotification.create(:user_id => 0, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "blank device token")
            end
          rescue => e
            user_id = User.find_by_device_token(sc).id rescue nil
            if user_id.blank?
              user_id = 0
              error_text = "User not found device token #{sc}"
            else
              error_text = e.inspect
            end
            failed_users << user.id
            puts "PLAINPUSHNOTIFICATION::PUSH_PLAIN_TO_ANDROID LOGGING #{e.inspect}"
            #HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2,
            #                           :status => 2, :error_text => error_text)
            next
          end
        end
      rescue => e
        puts "ANDROID PUSHPLAINNOTIFICATION Success Reg ids  #{e.inspect}"
      end
    end
    return failed_users.compact.flatten
  end

  ## old sql
  def sql_for_miss_you(job)
    owner = Owner.find(job.owner_id) rescue nil
    chain = owner.chain rescue job.chain
    restaurant_ids = owner.chain.restaurants.map(&:id) rescue job.chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    #### sql initialize
    sql = "select distinct(users.*) from (
            select user_id , date(max(max)) from (
            select user_id, max from (
             SELECT user_id, max(last_active) FROM restaurant_users
             inner join users on users.id = restaurant_users.user_id
             WHERE users.active = 't ' and (users.miss_you_date is null )
             and
             (restaurant_id in (#{restaurant_id_sql})) group by user_id
             ) as s
            UNION
            select reward_transactions.user_id, max(reward_transactions.created_at) from
            reward_transactions
            left join restaurant_users on reward_transactions.user_id = restaurant_users.user_id
            where
            reward_transactions.restaurant_id in(#{restaurant_id_sql})
            group by reward_transactions.user_id
            ) as x group by user_id) as mm
            inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
            and (users.miss_you_date is null)
            and date <= date('#{Time.now - job.percentage.weeks}')
            " if job.job_type == 1

    sql = "select distinct(users.*) from (
              select user_id , date(max(max)) from (
              select user_id, max from (
               SELECT user_id, max(last_active) FROM restaurant_users
               inner join users on users.id = restaurant_users.user_id
               WHERE users.active = 't ' and (users.miss_you_date is null)
                and
               (restaurant_id in (#{restaurant_id_sql})) group by user_id
               ) as s
              UNION
              select reward_transactions.user_id, max(reward_transactions.created_at) from reward_transactions
              left join restaurant_users on reward_transactions.user_id = restaurant_users.user_id
              where
              reward_transactions.restaurant_id in(#{restaurant_id_sql})
              group by reward_transactions.user_id
              ) as x group by user_id ) as mm
              inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
              and (users.miss_you_date is null)
              and date > date('#{Time.now - job.activity_days.weeks}')
" if job.job_type == 2

    sql = "select users.* from users where chain_id = #{chain.id} and users.active = 't' " if job.job_type == 3
    return sql
  end


  def sql_for_standard(job)
    owner = Owner.find(job.owner_id) rescue nil
    chain = owner.chain rescue job.chain
    restaurant_ids = owner.chain.restaurants.map(&:id) rescue chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    sql = "select users.* from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.user_id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date <= date('#{Time.now - job.percentage.weeks}')
"  if job.job_type == 1

    sql = "select users.* from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.user_id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date > date('#{Time.now - job.activity_days.weeks}')
" if job.job_type == 2

    sql = "select users.* from users where chain_id = #{chain.id} and users.active = 't'" if job.job_type == 3

    return sql
  end


  ######### old query

  def count_for_show(job)
      sql = sql_for_no_scan(job, "true")
      return User.find_by_sql(sql).first.count.to_i
  end

end