class MailScharacter
  ## handle special character mail

  def initialize(email, from, subject, body, content_type = nil)
    @email = email
    @from = from
    @subject = subject
    @body = body
    @content_type = content_type
  end

  def send_mail
    mail = setup_mail
    mail.deliver!
  end

  def setup_mail
    mail = Mail.new do
      to @email
      from @from
      subject @subject
    end


    if @content_type.eql?("multipart/alternative") or @content_type.eql?("text/plain")
      text_part = Mail::Part.new do
        body @body
      end
      mail.text_part = text_part
    end

    if @content_type.eql?("multipart/alternative") or @content_type.eql?("text/html")
      html_part = Mail::Part.new do
        content_type 'text/html'
        body @body
      end
      mail.html_part = html_part
    end
    mail
  end
end