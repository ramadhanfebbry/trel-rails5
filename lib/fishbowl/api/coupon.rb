require "net/http"
require "uri"

module Fishbowl
  module Api
    class Coupon
      attr_accessor :username, :password, :authorization_key, :host, :promotion_id, :brandid, :count, :client_id, :secret_id

      def initialize params = {}
        params.each { |key, value| send "#{key}=", value }
      end

      def login
        begin
          p data = {"grant_type" => "password", "username" => username, "password" => password}
          p uri = URI.parse(host+"/oauth/access_token")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request.form_data = data
          x = Base64.encode64("#{client_id}:#{secret_id}")
          request["Content-Type"] = 'application/x-www-form-urlencoded'
          request["Authorization"] =  "Basic #{x.gsub("\n", "")}"
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            response_json =JSON.parse(response.body)
            @authorization_key = response_json["access_token"]
          end
          response.code
        rescue
          nil
        end
      end

      def batch(reward)
        begin
          data = {"promotion"=>{"id"=>promotion_id},"brandid"=>brandid,"count"=>count,"expiration_date" => reward.expiryDate.strftime("%Y-%m-%d"), "utm_medium" => "Relevant"}
          uri = URI.parse(host+"/coupon/code/batch")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request.body = data.to_json
          request["Content-Type"] = "application/json"
          request["Authorization"] =  "Bearer #{authorization_key}"
          response = http.request(request)
          JSON.parse(response.body)
        rescue
          nil
        end
      end

    end
  end
end
