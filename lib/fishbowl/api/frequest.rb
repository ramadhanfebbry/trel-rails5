require "net/http"
require "uri"

module Fishbowl
  module Api
    module Frequest
      extend ActiveSupport::Concern

      module ClassMethods
        def update_user_fishbowl_location(user, restaurant_id)
          return if user.blank?
          return if !user.marketing_optin
          if user.chain.fishbowl_setting
            return if !user.chain.fishbowl_setting.ignore_marketing_optin
            p "---- update email marketing location ---"
            p restaurant_id
            uf = UserFishbowl.where(:chain_id =>  user.chain_id, :user_id => user.id).first
            uf.update_attributes(:status => UserFishbowl::STATUS["CHANGED"], :email => user.email, :restaurant_id => restaurant_id ) unless uf.blank?
          end
        end

      end

      module InstanceMethods

        def add_users_to_fishbowl_user
          count = self.users.count
          loop = (count.to_f / 500.to_f).ceil
          1.upto(loop) do |page|
            self.users.paginate(page: page, per_page: 500).each do |user|
              p "adding  user #{user.id} - #{user.email} to fishbowl user"
              add_to_fishbowl_user(user)
            end
          end
          p "end added user to fishbowl user"
        end

        def add_to_fishbowl_user(user, execute_direct_sync = false)
          direct_synced_uf = nil
          rs = user.restaurant_users.order("created_at DESC").first
          user_fishbowl = UserFishbowl.where(:user_id => user.id).first
          if user_fishbowl
            #if !user.marketing_optin
            #  return if user_fishbowl.status.eql?(UserFishbowl::STATUS["SYNCED"])
            #  user_fishbowl.update_column(:status, UserFishbowl::STATUS["MARK OPT OUT"])
            #  return if !self.fishbowl_setting.ignore_marketing_optin
            #end
            if (user_fishbowl.email != user.email && !user_fishbowl.member_id.blank?) || user_fishbowl.status == UserFishbowl::STATUS["REPEATED"] || ( rs && user_fishbowl.restaurant_id != rs.restaurant_id)
              user_fishbowl.update_attributes(:status => UserFishbowl::STATUS["CHANGED"], :email => user.email, :restaurant_id => (rs.restaurant_id rescue nil))
            else
              #status = user.marketing_optin ? UserFishbowl::STATUS["NEW"] : UserFishbowl::STATUS["MARK OPT OUT"]
              if user.marketing_optin_changed?
                user_fishbowl.update_attributes(:email => user.email, :status => UserFishbowl::STATUS["NEW"])
              else
                user_fishbowl.update_attributes(:email => user.email)
              end

            end
          else
            status = user.marketing_optin ? UserFishbowl::STATUS["NEW"] : UserFishbowl::STATUS["MARK OPT OUT"]
            uf = UserFishbowl.new(:chain_id => user.chain_id, :user_id => user.id, :email => user.email, :status => status, :restaurant_id => (rs.restaurant_id rescue nil), :signup_date => (rs.created_at rescue nil))
            uf.save
          end

          if execute_direct_sync
            p "----EXECUTE DIRECT OR REAL TIME SYNC FISHBOWL-------"
            p user
            direct_synced_uf = direct_sync(user)
          end
          return direct_synced_uf
        end

        def add_to_fishbowl_user_without_direct_sync(user)
          rs = user.restaurant_users.order("created_at DESC").first
          user_fishbowl = UserFishbowl.where(:user_id => user.id).first
          if user_fishbowl
            #if !user.marketing_optin
            #  return if user_fishbowl.status.eql?(UserFishbowl::STATUS["SYNCED"])
            #  user_fishbowl.update_column(:status, UserFishbowl::STATUS["MARK OPT OUT"])
            #  return if !self.fishbowl_setting.ignore_marketing_optin
            #end
            if (user_fishbowl.email != user.email && !user_fishbowl.member_id.blank?) || user_fishbowl.status == UserFishbowl::STATUS["REPEATED"] || ( rs && user_fishbowl.restaurant_id != rs.restaurant_id)
              user_fishbowl.update_attributes(:status => UserFishbowl::STATUS["CHANGED"], :email => user.email, :restaurant_id => (rs.restaurant_id rescue nil))
            else
              #status = user.marketing_optin ? UserFishbowl::STATUS["NEW"] : UserFishbowl::STATUS["MARK OPT OUT"]
              if user.marketing_optin_changed?
                user_fishbowl.update_attributes(:email => user.email, :status => UserFishbowl::STATUS["NEW"])
              else
                user_fishbowl.update_attributes(:email => user.email)
              end

            end
          else
            status = user.marketing_optin ? UserFishbowl::STATUS["NEW"] : UserFishbowl::STATUS["MARK OPT OUT"]
            uf = UserFishbowl.new(:chain_id => user.chain_id, :user_id => user.id, :email => user.email, :status => status, :restaurant_id => (rs.restaurant_id rescue nil), :signup_date => (rs.created_at rescue nil))
            uf.save
          end
        end

        def send_welcome_email(user)
          uf = UserFishbowl.where(:user_id => user.id).first
          return false if uf.blank?
          if uf.member_id.blank?
            member_id = self.get_member_id_by_address(user.email)
            uf.update_attributes(:member_id => member_id)
            uf.reload
          end
          return false if uf.member_id.blank?

          begin
            welcome_email_id = ["browser", "browser_compatible"].include?(user.register_device_type) ? self.fishbowl_setting.browser_welcome_email_mailing_id : self.fishbowl_setting.welcome_email_mailing_id
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/SendTriggeredMailingBySingleID?siteID=#{self.fishbowl_setting.site_id}&mailingID=#{welcome_email_id}&memberID=#{uf.member_id}")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Get.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              return true
              p "fishbowl welcome email sent to #{user.email}"
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            p p "fishbowl welcome email did not sent to #{user.email}, there is error---"
            return false
          end
        end

        def send_reward_notif(user, reward, expired_at)
          p "--------send reward notif fishbowl---------"
          reward_notif_sent = false
          return false if self.fishbowl_setting.blank?
          return false if self.fishbowl_setting.reward_redeem_mailing_id.blank?
          return false if user.blank?
          uf = UserFishbowl.where(:user_id => user.id).first
          return false if uf.blank?
          if uf.member_id.blank?
            member_id = self.get_member_id_by_address(user.email)
            uf.update_attributes(:member_id => member_id)
            uf.reload
          end
          return false if uf.member_id.blank?
          data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SendTriggeredMailingBySingleIDWithVariables xmlns="Quotient.QTools.API">
      <siteID>#{self.fishbowl_setting.site_id}</siteID>
      <mailingID>#{self.fishbowl_setting.reward_redeem_mailing_id}</mailingID>
      <memberID>#{uf.member_id}</memberID>
      <localVariables>
        <LocalVariable>
          <Name>Reward</Name>
          <Value>#{reward.name}</Value>
        </LocalVariable>
        <LocalVariable>
          <Name>Exp</Name>
          <Value>#{expired_at}</Value>
        </LocalVariable>
      </localVariables>
    </SendTriggeredMailingBySingleIDWithVariables>
  </soap:Body>
</soap:Envelope>
          EOF
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.body = data
            request["Content-Type"] = "text/xml; charset=utf-8"
            request["Content-Length"] =  "length"
            request["SOAPAction"] = "Quotient.QTools.API/SendTriggeredMailingBySingleIDWithVariables"
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              reward_notif_sent = true
              p "fishbowl reward email sent to #{user.email}"
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            p p "fishbowl welcome email did not sent to #{user.email}, there is error---"
          end
          p "---- end send reward notif fishbowl -----"
          return reward_notif_sent
        end

        def send_point_notif_for_single_user(user, point)
          p "--------send points notif fishbowl---------"
          points_notif_sent = false
          return false if self.fishbowl_setting.blank?
          return false if self.fishbowl_setting.points_notification_mailing_id.blank?
          return false if user.blank?
          uf = UserFishbowl.where(:user_id => user.id).first
          return false if uf.blank?
          if uf.member_id.blank?
            member_id = self.get_member_id_by_address(user.email)
            uf.update_attributes(:member_id => member_id)
            uf.reload
          end
          return false if uf.member_id.blank?
          data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SendTriggeredMailingBySingleIDWithVariables xmlns="Quotient.QTools.API">
      <siteID>#{self.fishbowl_setting.site_id}</siteID>
      <mailingID>#{self.fishbowl_setting.points_notification_mailing_id}</mailingID>
      <memberID>#{uf.member_id}</memberID>
      <localVariables>
        <LocalVariable>
        </LocalVariable>
      </localVariables>
    </SendTriggeredMailingBySingleIDWithVariables>
  </soap:Body>
</soap:Envelope>
          EOF
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.body = data
            request["Content-Type"] = "text/xml; charset=utf-8"
            request["Content-Length"] =  "length"
            request["SOAPAction"] = "Quotient.QTools.API/SendTriggeredMailingBySingleIDWithVariables"
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              points_notif_sent = true
              p "fishbowl points notif email sent to #{user.email}"
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            p p "fishbowl points notif email did not sent to #{user.email}, there is error---"
          end
          p "---- end send points notif fishbowl -----"
          return points_notif_sent
        end

        # This documentation is part of FISHBOWL-RELEVANTINTEGRATION document
        # PARAMETERS REQUIRED:
        # Points value (string)
        # PARAMETER email_body contain, hash keys :
        def send_points_notif_for_multi_user(chain, reg_ids_by_locale, hash_notification, point, image_url = nil)
          p 'send point notification email on fishbowl here for multi user----'
          reg_ids_by_locale.each do |key, value|
            value[:emails].each_with_index do |email, i|
              user = User.where(chain_id: self.id, email: email).first
              unless self.send_point_notif_for_single_user(user, point)
                PointNotification.push_email_group(chain, reg_ids_by_locale, hash_notification, point, image_url = nil)
              end
            end
          end
        end

        # This documentation is part of FISHBOWL-RELEVANTINTEGRATION document
        # PARAMETERS REQUIRED:
        # - Date of transaction (string)(mm-dd-yyyy) // will send time part as part of this string when we sort of time zone issue Transaction Amount( string)
        # - Transaction Amount( string)
        # - Card type(string)
        # - Four digit card number(string)
        # - Restaurant Location (string)// send Restaurant App Display name here
        # PARAMETER email_body contain, hash keys :
        # :date,:amount, :card_type, :four_digit_card_number,:restaurant_address, :restaurant_zipcode, :restaurant_phone_number
        def send_payment_notif(user, email_body)
          p "--------send payment notif fishbowl---------"
          payment_notif_sent = false
          return false if self.fishbowl_setting.blank?
          return false if self.fishbowl_setting.payment_notification_mailing_id.blank?
          return false if user.blank?
          uf = UserFishbowl.where(:user_id => user.id).first
          return false if uf.blank?
          if uf.member_id.blank?
            member_id = self.get_member_id_by_address(user.email)
            uf.update_attributes(:member_id => member_id)
            uf.reload
          end
          return false if uf.member_id.blank?
          data = <<-EOF
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:quot="Quotient.QTools.API">
   <soapenv:Header/>
   <soapenv:Body>
      <quot:SendTriggeredMailingBySingleIDWithVariables>
         <quot:siteID>#{self.fishbowl_setting.site_id}</quot:siteID>
         <quot:mailingID>#{self.fishbowl_setting.payment_notification_mailing_id}</quot:mailingID>
         <quot:memberID>#{uf.member_id}</quot:memberID>
         <quot:localVariables>
            <quot:LocalVariable>
               <quot:Name>DateofTransaction</quot:Name>
               <quot:Value>#{Time.zone.now}</quot:Value>
            </quot:LocalVariable>
	    <quot:LocalVariable>
               <quot:Name>TransactionAmount</quot:Name>
               <quot:Value>#{email_body[:amount]}</quot:Value>
            </quot:LocalVariable>
	    <quot:LocalVariable>
               <quot:Name>CardType</quot:Name>
               <quot:Value>#{email_body[:card_type]}</quot:Value>
            </quot:LocalVariable>
	    <quot:LocalVariable>
               <quot:Name>FourDigitCardNumber</quot:Name>
               <quot:Value>#{email_body[:four_digit_card_number].to_i}</quot:Value>
            </quot:LocalVariable>
	    <quot:LocalVariable>
               <quot:Name>RestaurantLocation</quot:Name>
               <quot:Value>#{email_body[:restaurant_address]}</quot:Value>
            </quot:LocalVariable>
         </quot:localVariables>
      </quot:SendTriggeredMailingBySingleIDWithVariables>
   </soapenv:Body>
</soapenv:Envelope>
          EOF

          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.body = data
            request["Content-Type"] = "text/xml; charset=utf-8"
            request["Content-Length"] =  "length"
            request["SOAPAction"] = "Quotient.QTools.API/SendTriggeredMailingBySingleIDWithVariables"
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              payment_notif_sent = true
              p "fishbowl payment notif email sent to #{user.email}"
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            p "fishbowl payment notif email did not sent to #{user.email}, there is error---"
          end
          p "---- end send payment notif fishbowl -----"
          return payment_notif_sent
        end

        def remove_users_from_fishbowl_user
          count = self.user_fishbowls.count
          loop = (count.to_f / 500.to_f).ceil
          1.upto(loop) do |page|
            self.user_fishbowls.paginate(page: page, per_page: 500).each do |uf|
              uf.destroy
            end
          end
        end

        def get_members_fishbowl_for_initial_sync
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/GetMembersByCreatedDate")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            today_date = Date.today
            request.set_form_data({"siteID" => self.fishbowl_setting.site_id, "start" => today_date - 2.day, "finish" => today_date + 2.day})
            response = http.request(request)
            result = Hash.from_xml(Hash.from_xml(response.body)["string"])
            return result["members"]["member"]
          rescue => e
            p "------------- error request get_members_fishbowl_for_initial_sync ------"
            p e.message
            return []
          end
        end

        def get_fishbowl_member_by_address(email)
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/GetMemberByAddress")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            today_date = Date.today
            request.set_form_data({"siteID" => self.fishbowl_setting.site_id, "emailAddress" => email })
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              true
            else
              false
            end
          rescue => e
            p "------------- error request get_fishbowl_member_by_address ------"
            p e.message
            return false
          end
        end

        def get_member_id_by_address(email)
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/GetMemberByAddress")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            today_date = Date.today
            request.set_form_data({"siteID" => self.fishbowl_setting.site_id, "emailAddress" => email })
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              result = Hash.from_xml(Hash.from_xml(response.body)["string"])
              return result["member"]["memberid"]
            else
              return nil
            end
          rescue => e
            p "------------- error request get_member_id_by_address ------"
            p e.message
            return nil
          end
        end

        def initial_sync_user_with_members_fishbowl(log)
          success_users = []
          failed_users = []
          self.add_users_to_fishbowl_user
          user_added_ids = []
          members = self.get_members_fishbowl_for_initial_sync
          if members.is_a?(Hash)
            member = members
            custom_fields = member["customfields"]
            if custom_fields["relevantid"]
              user = self.users.where(:id => custom_fields["relevantid"], :email => member["emailaddress"]).first
            else
              user = self.users.where(:email => member["emailaddress"]).first
            end
            user_added_ids << user.id if user
          elsif members.is_a?(Array)
            members.each do |member|
              custom_fields = member["customfields"]
              if custom_fields["relevantid"]
                user = self.users.where(:id => custom_fields["relevantid"], :email => member["emailaddress"]).first
              else
                user = self.users.where(:email => member["emailaddress"]).first
              end
              user_added_ids << user.id if user
            end
          end
          if user_added_ids.blank?
            count = UserFishbowl.where("chain_id = #{self.id}").count
            loop = (count.to_f / 500.to_f).ceil
            1.upto(loop) do |page|
              UserFishbowl.where("chain_id = #{self.id}").paginate(page: page, per_page: 500).each do |uf|
                failed_users << uf.user_id
                uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
              end
            end
          else
            count = UserFishbowl.where("chain_id = #{self.id} AND user_id IN (#{user_added_ids.join})").count
            loop = (count.to_f / 500.to_f).ceil
            1.upto(loop) do |page|
              UserFishbowl.where("chain_id = #{self.id} AND user_id IN (#{user_added_ids.join})").paginate(page: page, per_page: 500).each do |uf|
                success_users << uf.user_id
                uf.update_column(:status, UserFishbowl::STATUS["SYNCED"])
              end
            end

            count = UserFishbowl.where("chain_id = #{self.id} AND user_id NOT IN (#{user_added_ids.join})").count
            loop = (count.to_f / 500.to_f).ceil
            1.upto(loop) do |page|
              UserFishbowl.where("chain_id = #{self.id} AND user_id NOT IN (#{user_added_ids.join})").paginate(page: page, per_page: 500).each do |uf|
                failed_users << uf.user_id
                uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
              end
            end
          end
          log.update_attributes(
              :success_users => success_users,
              :failed_users => failed_users,
              :status => "completed",
              :completed_at => Time.now
          )
        end

        def direct_sync(user)
          default_user_fishbowl_status = [UserFishbowl::STATUS["NEW"],UserFishbowl::STATUS["CHANGED"], UserFishbowl::STATUS["FAILED_SYNC"]]
          if self.fishbowl_setting.ignore_marketing_optin || self.fishbowl_setting.sync_flow == "v2"
            default_user_fishbowl_status << UserFishbowl::STATUS["MARK OPT OUT"]
            uf = UserFishbowl.includes(:user).where("user_fishbowls.status in (#{default_user_fishbowl_status.join(",")})  AND user_fishbowls.chain_id = #{self.id} AND users.id = #{user.id}").order("user_id DESC").first
          else
            uf = UserFishbowl.includes(:user).where("users.marketing_optin IS TRUE AND user_fishbowls.status in (#{default_user_fishbowl_status.join(",")}) AND user_fishbowls.chain_id = #{self.id} AND users.id = #{user.id}").order("user_id DESC").first
          end

          return if uf.blank?
          user = uf.user
          restaurant = uf.restaurant
          if self.fishbowl_setting.sync_flow == "v1"
            member_xml = member_xml_for(user, restaurant)
          else
            member_xml = v2_member_xml_for(user, restaurant)
          end
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/UpdateMember")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.set_form_data({"siteID" => self.fishbowl_setting.site_id, "memberXml" => member_xml, "sendConfirmation" => "false"})
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              result = Hash.from_xml(response.body)
              if result && result["long"]
                UserFishbowl.where(:member_id => result["long"]).each do |user_repeated|
                  user_repeated.update_attributes(:status => UserFishbowl::STATUS["REPEATED"], :member_id => nil)
                end
                uf.update_attributes(:member_id => result["long"], :status => UserFishbowl::STATUS["SYNCED"])
              else
                uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
              end
            else
              uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
          end
          return uf
        end

        def sync(log)
          success_users = []
          failed_users = []
          default_user_fishbowl_status = [UserFishbowl::STATUS["NEW"],UserFishbowl::STATUS["CHANGED"], UserFishbowl::STATUS["FAILED_SYNC"]]
          if self.fishbowl_setting.ignore_marketing_optin || self.fishbowl_setting.sync_flow == "v2"
            default_user_fishbowl_status << UserFishbowl::STATUS["MARK OPT OUT"]
            count = UserFishbowl.includes(:user).where("user_fishbowls.status in (#{default_user_fishbowl_status.join(",")})  AND user_fishbowls.chain_id = #{self.id}").order("user_id DESC").count
            query = "user_fishbowls.status in (#{default_user_fishbowl_status.join(",")})  AND user_fishbowls.chain_id = #{self.id}"
          else
            count = UserFishbowl.includes(:user).where("users.marketing_optin IS TRUE AND user_fishbowls.status in (#{default_user_fishbowl_status.join(",")}) AND user_fishbowls.chain_id = #{self.id}").order("user_id DESC").count
            query = "users.marketing_optin IS TRUE AND user_fishbowls.status in (#{default_user_fishbowl_status.join(",")})  AND user_fishbowls.chain_id = #{self.id}"
          end
          loop = (count.to_f / 500.to_f).ceil
          1.upto(loop) do |page|
            UserFishbowl.includes(:user).where(query).order("user_id DESC").paginate(page: page, per_page: 500, :total_entries => -1).each do |uf|
              user = uf.user
              restaurant = uf.restaurant
              if self.fishbowl_setting.sync_flow == "v1"
                member_xml = member_xml_for(user, restaurant)
              else
                member_xml = v2_member_xml_for(user, restaurant)
              end
              begin
                uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/UpdateMember")
                http = Net::HTTP.new(uri.host, uri.port)
                http.use_ssl = true
                http.verify_mode = OpenSSL::SSL::VERIFY_NONE
                request = Net::HTTP::Post.new(uri.request_uri)
                request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
                request.set_form_data({"siteID" => self.fishbowl_setting.site_id, "memberXml" => member_xml, "sendConfirmation" => "false"})
                response = http.request(request)
                if response.is_a?(Net::HTTPOK)
                  result = Hash.from_xml(response.body)
                  if result && result["long"]
                    success_users << user.id
                    UserFishbowl.where(:member_id => result["long"]).each do |user_repeated|
                      user_repeated.update_attributes(:status => UserFishbowl::STATUS["REPEATED"], :member_id => nil)
                    end
                    uf.update_attributes(:member_id => result["long"], :status => UserFishbowl::STATUS["SYNCED"])
                  else
                    failed_users << {user.id => "Member Id is not found"}
                    uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
                  end
                else
                  failed_users << {user.id => response.body}
                  uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
                end
              rescue => e
                p "------------- error request add members ------"
                p e.message
                failed_users << {user.id => e.message}
                uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
              end
            end
          end

          log.update_attributes(
              :success_users => success_users,
              :failed_users => failed_users,
              :status => "completed",
              :completed_at => Time.now
          )
        end

        def add_sync_job(run_type = "Sync Now", execute_time = nil)
          if self.fishbowl_setting
            fl = FishbowlLog.create(:chain_id => self.id, :log_type => run_type, :status => "in queue")
            if execute_time
              dl = Delayed::Job.enqueue(FishbowlJob.new(self, fl), :run_at => execute_time)
            else
              dl = Delayed::Job.enqueue(FishbowlJob.new(self, fl))
            end
          end
        end

        def delete_members
          count =  self.user_fishbowls.count
          loop = (count.to_f / 500.to_f).ceil
          1.upto(loop) do |page|
            self.user_fishbowls.paginate(page: page, per_page: 500).each do |uf|
              p "deleting #{uf.user_id}"
              p "email = #{uf.email}"
              uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/DeleteMemberByID")
              http = Net::HTTP.new(uri.host, uri.port)
              http.use_ssl = true
              http.verify_mode = OpenSSL::SSL::VERIFY_NONE
              request = Net::HTTP::Post.new(uri.request_uri)
              request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
              request.set_form_data({"siteID" => self.fishbowl_setting.site_id, "memberID" => uf.member_id})
              response = http.request(request)
              uf.update_column(:status, UserFishbowl::STATUS["NEW"])
              p "deleted"
              p "---------"
            end
          end
        end

        def member_xml_for(user, restaurant)
          subscribed = get_fishbowl_member_by_address(user.email)
          member_xml = ""
          if subscribed
            member_xml = "<member>
<emailaddress>#{user.email}</emailaddress>
<password />
<softbounces>0</softbounces>
<hardbounces>0</hardbounces>
<customfields>
  <relevantid>#{user.id}</relevantid>"
            member_xml += "<relevantloyaltylocation>#{restaurant.try(:fishbowl_restaurant_identifier).try(:sanitize_restaurant_identifier_for_fishbowl)}</relevantloyaltylocation>"
            member_xml += "<firstname>#{user.first_name}</firstname>"
            member_xml += "<lastname>#{user.last_name}</lastname>"
            member_xml += "<relevantjoindate>#{user.created_at.strftime("%Y-%m-%dT%H:%M:%S")}</relevantjoindate>"
            if user.dob_day.to_i != 0 && user.dob_month.to_i != 0 && user.dob_year.to_i != 0
              dob = "#{user.dob_year}-#{user.dob_month}-#{user.dob_day}".to_datetime rescue nil
              member_xml += "<birthdate>#{dob.strftime("%Y-%m-%dT%H:%M:%S")}</birthdate>" unless dob.blank?
            end
            member_xml += "<zip>#{user.zipcode}</zip>"
            member_xml += "<mobilephone>#{user.phone_number}</mobilephone>" unless user.phone_number.blank?
            member_xml += "<relevantuserstatus>#{user.active ? "Active" : "Disabled"}</relevantuserstatus>
  <relevantsubsstatusemail>#{user.push_email ? "Active" : "Disabled"}</relevantsubsstatusemail>
  <relevantsubsstatusdevice>#{user.push_device ? "Active" : "Disabled"}</relevantsubsstatusdevice>"
            member_xml += "<sms_optin>#{user.marketing_optin_texting ? true : false}</sms_optin>" unless user.marketing_optin_texting.blank?
            member_xml += "<purpose>#{user.app_usage_purpose.to_s}</purpose>" unless user.app_usage_purpose.blank?
            member_xml += "</customfields>"
            self.fishbowl_setting.list_ids.each do |list_id|
              member_xml += "<subscription>"
              member_xml += "<listid>#{list_id}</listid>"
              member_xml += "<issubscribed>true</issubscribed>"
              member_xml += "</subscription>"
            end
            member_xml += "</member>"
          else
            member_xml = "<member>
<emailaddress>#{user.email}</emailaddress>
<password />
<softbounces>0</softbounces>
<hardbounces>0</hardbounces>
<customfields>
  <relevantid>#{user.id}</relevantid>"
            member_xml += "<relevantloyaltylocation>#{restaurant.try(:fishbowl_restaurant_identifier)}</relevantloyaltylocation>"
            member_xml += "<storecode>#{user.try(:favorite_restaurant).try(:fishbowl_restaurant_identifier)}</storecode>" unless user.try(:favorite_restaurant).try(:fishbowl_restaurant_identifier).blank?
            member_xml += "<firstname>#{user.first_name}</firstname>"
            member_xml += "<lastname>#{user.last_name}</lastname>"
            member_xml += "<relevantjoindate>#{user.created_at.strftime("%Y-%m-%dT%H:%M:%S")}</relevantjoindate>"
            if user.dob_day.to_i != 0 && user.dob_month.to_i != 0 && user.dob_year.to_i != 0
              dob = "#{user.dob_year}-#{user.dob_month}-#{user.dob_day}".to_datetime rescue nil
              member_xml += "<birthdate>#{dob.strftime("%Y-%m-%dT%H:%M:%S")}</birthdate>" unless dob.blank?
            end
            member_xml += "<zip>#{user.zipcode}</zip>"
            member_xml += "<mobilephone>#{user.phone_number}</mobilephone>" unless user.phone_number.blank?
            member_xml += "<relevantuserstatus>#{user.active ? "Active" : "Disabled"}</relevantuserstatus>
  <relevantsubsstatusemail>#{user.push_email ? "Active" : "Disabled"}</relevantsubsstatusemail>
  <relevantsubsstatusdevice>#{user.push_device ? "Active" : "Disabled"}</relevantsubsstatusdevice>"
            member_xml += "<inputsource>#{["browser_compatible", "browser"].include?(user.register_device_type) ? "Relevant Web" : "Relevant Mobile"}</inputsource>"
            member_xml += "<sms_optin>#{user.marketing_optin_texting ? true : false}</sms_optin>" unless user.marketing_optin_texting.blank?
            member_xml += "<purpose>#{user.app_usage_purpose.to_s}</purpose>" unless user.app_usage_purpose.blank?
            member_xml += "</customfields>"
            self.fishbowl_setting.list_ids.each do |list_id|
              member_xml += "<subscription>"
              member_xml += "<listid>#{list_id}</listid>"
              member_xml += "<issubscribed>true</issubscribed>"
              member_xml += "</subscription>"
            end
            member_xml += "</member>"
          end
          p "--this is member xml fishbowl member------"
          p member_xml
          return member_xml
        end

        def v2_member_xml_for(user, restaurant)
          subscribed = get_fishbowl_member_by_address(user.email)
          activity_notification_optin = user.user_profile && user.user_profile.activity_notification_optin.present? ? user.user_profile.activity_notification_optin : true
          member_xml = ""
          if subscribed
            member_xml = "<member>
<emailaddress>#{user.email}</emailaddress>
<password />
<softbounces>0</softbounces>
<hardbounces>0</hardbounces>
<customfields>
  <relevantid>#{user.id}</relevantid>"
            member_xml += "<relevantloyaltylocation>#{restaurant.try(:fishbowl_restaurant_identifier).try(:sanitize_restaurant_identifier_for_fishbowl)}</relevantloyaltylocation>"
            member_xml += "<firstname>#{user.first_name}</firstname>"
            member_xml += "<lastname>#{user.last_name}</lastname>"
            member_xml += "<relevantjoindate>#{user.created_at.strftime("%Y-%m-%dT%H:%M:%S")}</relevantjoindate>"
            if user.dob_day.to_i != 0 && user.dob_month.to_i != 0 && user.dob_year.to_i != 0
              dob = "#{user.dob_year}-#{user.dob_month}-#{user.dob_day}".to_datetime rescue nil
              member_xml += "<birthdate>#{dob.strftime("%Y-%m-%dT%H:%M:%S")}</birthdate>" unless dob.blank?
            end
            member_xml += "<zip>#{user.zipcode}</zip>"
            member_xml += "<mobilephone>#{user.phone_number}</mobilephone>" unless user.phone_number.blank?
            member_xml += "<relevantuserstatus>#{user.active ? "Active" : "Disabled"}</relevantuserstatus>
  <relevantsubsstatusemail>#{user.push_email ? "Active" : "Disabled"}</relevantsubsstatusemail>
  <relevantsubsstatusdevice>#{user.push_device ? "Active" : "Disabled"}</relevantsubsstatusdevice>
  <sms_optin>#{user.marketing_optin_texting ? true : false}</sms_optin>
  <relevant_activitynotification>#{activity_notification_optin}</relevant_activitynotification>
  <relevantmarketingoptin>#{user.marketing_optin ? true : false}</relevantmarketingoptin>
</customfields>"

            self.fishbowl_setting.list_ids.each do |list_id|
              member_xml += "<subscription>"
                member_xml += "<listid>#{list_id}</listid>"
                member_xml += "<issubscribed>true</issubscribed>"
              member_xml += "</subscription>"
            end
            member_xml += "</member>"
          else
            member_xml = "<member>
<emailaddress>#{user.email}</emailaddress>
<password />
<softbounces>0</softbounces>
<hardbounces>0</hardbounces>
<customfields>
  <relevantid>#{user.id}</relevantid>"
            member_xml += "<relevantloyaltylocation>#{restaurant.try(:fishbowl_restaurant_identifier).try(:sanitize_restaurant_identifier_for_fishbowl)}</relevantloyaltylocation>"
            member_xml += "<storecode>#{user.try(:favorite_restaurant).try(:fishbowl_restaurant_identifier).try(:sanitize_restaurant_identifier_for_fishbowl)}</storecode>" unless user.try(:favorite_restaurant).try(:fishbowl_restaurant_identifier).blank?
            member_xml += "<firstname>#{user.first_name}</firstname>"
            member_xml += "<lastname>#{user.last_name}</lastname>"
            member_xml += "<relevantjoindate>#{user.created_at.strftime("%Y-%m-%dT%H:%M:%S")}</relevantjoindate>"
            if user.dob_day.to_i != 0 && user.dob_month.to_i != 0 && user.dob_year.to_i != 0
              dob = "#{user.dob_year}-#{user.dob_month}-#{user.dob_day}".to_datetime rescue nil
              member_xml += "<birthdate>#{dob.strftime("%Y-%m-%dT%H:%M:%S")}</birthdate>" unless dob.blank?
            end
            member_xml += "<zip>#{user.zipcode}</zip>"
            member_xml += "<mobilephone>#{user.phone_number}</mobilephone>" unless user.phone_number.blank?
            member_xml += "<relevantuserstatus>#{user.active ? "Active" : "Disabled"}</relevantuserstatus>
  <relevantsubsstatusemail>#{user.push_email ? "Active" : "Disabled"}</relevantsubsstatusemail>
  <relevantsubsstatusdevice>#{user.push_device ? "Active" : "Disabled"}</relevantsubsstatusdevice>
  <inputsource>Relevant Mobile</inputsource>
  <sms_optin>#{user.marketing_optin_texting ? true : false}</sms_optin>
  <relevant_activitynotification>#{activity_notification_optin}</relevant_activitynotification>
  <relevantmarketingoptin>#{user.marketing_optin ? true : false}</relevantmarketingoptin>"
            member_xml += "<purpose>#{user.app_usage_purpose.to_s}</purpose>" unless user.app_usage_purpose.blank?
            member_xml += "</customfields>"
            self.fishbowl_setting.list_ids.each do |list_id|
              member_xml += "<subscription>"
                member_xml += "<listid>#{list_id}</listid>"
                member_xml += "<issubscribed>true</issubscribed>"
              member_xml += "</subscription>"
            end
            member_xml += "</member>"
          end
          p "--this is member xml fishbowl member------"
          p member_xml
          return member_xml
        end

        def send_we_miss_u_reward_notif(user, reward, expired_at, reward_fishbowl_id = nil)
          p "--------send we miss u reward notif fishbowl---------"
          reward_notif_sent = false
          return false if self.fishbowl_setting.blank?
          return false if self.fishbowl_setting.we_miss_you_reward_notification.blank?
          return false if user.blank?
          uf = UserFishbowl.where(:user_id => user.id).first
          return false if uf.blank?
          return false if uf.member_id.blank?
          data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SendTriggeredMailingBySingleIDWithVariables xmlns="Quotient.QTools.API">
      <siteID>#{self.fishbowl_setting.site_id}</siteID>
      <mailingID>#{self.fishbowl_setting.we_miss_you_reward_notification}</mailingID>
      <memberID>#{uf.member_id}</memberID>
      <localVariables>
        <LocalVariable>
          <Name>reward_title</Name>
          <Value>#{reward.name}</Value>
        </LocalVariable>
        <LocalVariable>
          <Name>reward_expiration_date</Name>
          <Value>#{expired_at}</Value>
        </LocalVariable>
      </localVariables>
    </SendTriggeredMailingBySingleIDWithVariables>
  </soap:Body>
</soap:Envelope>
          EOF
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.body = data
            request["Content-Type"] = "text/xml; charset=utf-8"
            request["Content-Length"] =  "length"
            request["SOAPAction"] = "Quotient.QTools.API/SendTriggeredMailingBySingleIDWithVariables"
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              reward_notif_sent = true
              p "fishbowl reward email sent to #{user.email}"
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            p p "fishbowl welcome email did not sent to #{user.email}, there is error---"
          end
          p "---- end send reward notif fishbowl -----"
          return reward_notif_sent
        end

        def send_we_miss_u_point_notif(user, point, point_fishbowl_id)
          p "--------send we miss u point notif fishbowl---------"
          reward_notif_sent = false
          return false if self.fishbowl_setting.blank?
          return false if self.fishbowl_setting.we_miss_you_point_notification.blank?
          return false if user.blank?
          uf = UserFishbowl.where(:user_id => user.id).first
          return false if uf.blank?
          return false if uf.member_id.blank?
          data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SendTriggeredMailingBySingleIDWithVariables xmlns="Quotient.QTools.API">
      <siteID>#{self.fishbowl_setting.site_id}</siteID>
      <mailingID>#{self.fishbowl_setting.we_miss_you_point_notification}</mailingID>
      <memberID>#{uf.member_id}</memberID>
      <localVariables>
        <LocalVariable>
          <Name>points_value</Name>
          <Value>#{point}</Value>
        </LocalVariable>
      </localVariables>
    </SendTriggeredMailingBySingleIDWithVariables>
  </soap:Body>
</soap:Envelope>
          EOF
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.body = data
            request["Content-Type"] = "text/xml; charset=utf-8"
            request["Content-Length"] =  "length"
            request["SOAPAction"] = "Quotient.QTools.API/SendTriggeredMailingBySingleIDWithVariables"
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              reward_notif_sent = true
              p "fishbowl reward email sent to #{user.email}"
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            p p "fishbowl welcome email did not sent to #{user.email}, there is error---"
          end
          p "---- end send reward notif fishbowl -----"
          return reward_notif_sent
        end

        def send_no_scan_after_7_days_notif(user)
          p "--------send send_no_scan_after_7_days_notif fishbowl---------"
          send_no_scan_after_7_days_notif = false
          return false if self.fishbowl_setting.blank?
          return false if self.fishbowl_setting.no_scans_7_days_mailing_id.blank?
          return false if user.blank?
          uf = UserFishbowl.where(:user_id => user.id).first
          return false if uf.blank?
          return false if uf.member_id.blank?
          data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SendTriggeredMailingBySingleID xmlns="Quotient.QTools.API">
      <siteID>#{self.fishbowl_setting.site_id}</siteID>
      <mailingID>#{self.fishbowl_setting.no_scans_7_days_mailing_id}</mailingID>
      <memberID>#{uf.member_id}</memberID>
    </SendTriggeredMailingBySingleID>
  </soap:Body>
</soap:Envelope>
          EOF
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.body = data
            request["Content-Type"] = "text/xml; charset=utf-8"
            request["Content-Length"] =  "length"
            request["SOAPAction"] = "Quotient.QTools.API/SendTriggeredMailingBySingleID"
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              send_no_scan_after_7_days_notif = true
              p "fishbowl send_no_scan_after_7_days_notif sent to #{user.email}"
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            p p "fishbowl send_no_scan_after_7_days_notif did not sent to #{user.email}, there is error---"
          end
          p "---- end send send_no_scan_after_7_days_notif fishbowl -----"
          return send_no_scan_after_7_days_notif
        end

        def unsubscribe_member(user)
          return false if self.fishbowl_setting.blank?
          data = <<-EOF
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <DeleteMemberByAddress xmlns="Quotient.QTools.API">
      <siteID>#{self.fishbowl_setting.site_id}</siteID>
      <emailAddress>#{user.email}</emailAddress>
    </DeleteMemberByAddress>
  </soap:Body>
</soap:Envelope>
          EOF

          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.body = data
            request["Content-Type"] = "text/xml; charset=utf-8"
            request["Content-Length"] =  "length"
            request["SOAPAction"] = "Quotient.QTools.API/DeleteMemberByAddress"
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              return true
            end
          rescue => e
            p "------------- error request delete members ------"
            p e.message
            p p "fishbowl unsubscribe_member #{user.email}, there is error---"
            return false
          end
        end

        def update_fishbowl_user_profile(user)
          uf = UserFishbowl.where(:user_id => user.id).first
          return if uf.blank?
          restaurant = uf.restaurant
          member_xml = member_xml_for(user, restaurant)
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/UpdateMember")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            request.set_form_data({"siteID" => self.fishbowl_setting.site_id, "memberXml" => member_xml, "sendConfirmation" => "false"})
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              result = Hash.from_xml(response.body)
              if result && result["long"]
                UserFishbowl.where("member_id = ? AND id != ?",result["long"], uf.id).each do |user_repeated|
                  user_repeated.update_attributes(:status => UserFishbowl::STATUS["REPEATED"], :member_id => nil)
                end
                uf.update_attributes(:member_id => result["long"], :status => UserFishbowl::STATUS["SYNCED"])
              else
                uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
              end
            else
              uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
            end
          rescue => e
            p "------------- error request add members ------"
            p e.message
            uf.update_column(:status, UserFishbowl::STATUS["FAILED_SYNC"])
          end
        end

        def get_fishbowl_member_by_member_id(member_id)
          begin
            uri = URI.parse("https://services.fishbowl.com/API/Common.asmx/GetMemberByID")
            http = Net::HTTP.new(uri.host, uri.port)
            http.use_ssl = true
            http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            request = Net::HTTP::Post.new(uri.request_uri)
            request.basic_auth(self.fishbowl_setting.api_username, self.fishbowl_setting.api_password)
            today_date = Date.today
            request.set_form_data({"siteID" => self.fishbowl_setting.site_id, "memberid" => member_id })
            response = http.request(request)
            if response.is_a?(Net::HTTPOK)
              true
            else
              false
            end
          rescue => e
            p "------------- error request get_fishbowl_member_by_address ------"
            p e.message
            return false
          end
        end
      end
    end
  end
end