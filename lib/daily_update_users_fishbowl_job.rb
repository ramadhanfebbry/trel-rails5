class DailyUpdateUsersFishbowlJob < Struct.new(:chain, :files_executed, :log)

  require 'net/ftp'
  def perform
    log.update_attributes(:status => "processing", :initiate_at => Time.now)
    success_users = []
    failed_users = []

    file_type = ""
    p file = files_executed
    ftp =Net::FTP.new
    ftp.passive = true
    ftp.connect(chain.fishbowl_setting.ftp_url, chain.fishbowl_setting.ftp_port)
    ftp.login(chain.fishbowl_setting.ftp_username, chain.fishbowl_setting.ftp_password)

    if file.split(".").last.downcase == "xls"
      file_type = "xls"
    elsif file.split(".").last.downcase == "csv"
      file_type = "csv"
    end
    targetname = "update_users_fishbowl" + rand(1000).to_s + "." + file_type
    begin
      ftp.chdir(chain.fishbowl_setting.ftp_path_directory)
    rescue => e
      puts "Update user fishbowl #{e}"
    end
    ftp.getbinaryfile("#{file}","tmp/#{targetname}")
    ftp.close

    if File.exist?("#{Rails.root}/tmp/#{targetname}")
    
      if file_type == "xls"
        p "Processing by XLS"
        book = Spreadsheet.open "#{Rails.root}/tmp/#{targetname}"
        sheet1 = book.worksheet 0
        sheet1.each 1 do |row|
          user = chain.users.find_by_email(row[0].strip) rescue nil
          if user
            user.dob_year = row[1].split("/")[2].strip.first(4) if row[1] rescue nil
            user.dob_month = row[1].split("/")[0].strip if row[1]
            user.dob_day = row[1].split("/")[1].strip if row[1]
            user.favorite_location = row[2].to_s.strip if row[2]
            user.marketing_optin = row[3] if row[3]
            user.first_name = row[4].to_s.strip if row[4]
            user.last_name = row[5].to_s.strip if row[5]
            user.zipcode = row[6].to_s.strip if row[6]
            user.app_usage_purpose = row[7].to_s.strip if row[7]
            if user.save
              success_users << user.id
            else
              failed_users << user.id
            end
          end
        end
      elsif file_type == "csv"
        p "Processing by CSV"
        sheet1 = CSV.read("#{Rails.root}/tmp/#{targetname}", {:headers => true, :encoding => 'iso-8859-1:utf-8'})
        sheet1.each do |row|
          user = chain.users.find_by_email(row[0].strip) rescue nil
          if user
            user.dob_year = row[1].split("/")[2].strip.first(4) if row[1] rescue nil
            user.dob_month = row[1].split("/")[0].strip if row[1]
            user.dob_day = row[1].split("/")[1].strip if row[1]
            user.favorite_location = row[2].to_s.strip if row[2]
            user.marketing_optin = row[3] if row[3]
            user.first_name = row[4].to_s.strip if row[4]
            user.last_name = row[5].to_s.strip if row[5]
            user.zipcode = row[6].to_s.strip if row[6]
            user.app_usage_purpose = row[7].to_s.strip if row[7]
            if user.save
              success_users << user.id
            else
              failed_users << user.id
            end
          end

        end
      end

      log.update_attributes(
          :success_users => success_users,
          :failed_users => failed_users,
          :status => "completed",
          :completed_at => Time.now
      )
    else
      p "file is missing . . ."
    end
  end

  def error(job, exception)
    p "---------FB Synch erroor handling-------"
    struct = YAML::load(job.handler)
    chain = struct.chain
    log = struct.log
    log.update_attributes(:status =>  "failed", :completed_at => Time.now, :error_report => exception.to_s)  unless log.blank?
    p log
    ChainMailer.mailchimp_sync_log_error_report(chain, exception.to_s, "Fishbowl").deliver
    p "-----------------finishhhhh------"
  end


end