module WarningClaimTemplates

  class Es
    def self.title_warning(chain_name)
      "Esta seguro ?"
    end

    def self.body_warning(chain_name, confirm_text = 'confirmar', validity_period = '2')
      "Por favor, no haga clic en  '#{confirm_text}' until you are in front of our staff. Voucher valid for #{validity_period} minutes only."
    end
  end

end