module WarningClaimTemplates

  class En
    def self.title_warning(chain_name)
      "Are You Sure ?"
    end

    def self.body_warning(chain_name, confirm_text = 'confirm', validity_period = '2')
      "Please do NOT click '#{confirm_text}' until you are in front of our staff. Voucher valid for #{validity_period} minutes only."
    end
  end
  
end