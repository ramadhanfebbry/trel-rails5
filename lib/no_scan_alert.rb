class NoScanAlert < Struct.new(:chain_id)
  def perform
    #chain_id = 63
    chain = Chain.find(chain_id)
    chain_setting_alert = chain.chain_setting_alert
    return false if chain_setting_alert.is_active == false
    #zone = Time.now.in_time_zone(chain_setting_alert.send_zone).strftime('%Z')
    zone = Time.now.in_time_zone(chain_setting_alert.send_zone).strftime('%z').to_i / 100
    # restaurants = Receipt.where("chain_id = ? and receipts.created_at at time zone ? > now() at time zone ? - INTERVAL '24 HOUR'", chain.id, zone, zone).
    # joins("LEFT JOIN receipt_transactions on receipt_transactions.receipt_id = receipts.id").
    # select("receipt_transactions.restaurant_id , count(receipts.id)").group('receipt_transactions.restaurant_id')
    sql = "
    select restaurants.id, a.count from restaurants left join (SELECT receipt_transactions.restaurant_id ,
      count(receipts.id) FROM receipts LEFT JOIN receipt_transactions on receipt_transactions.receipt_id = receipts.id
      WHERE receipts.deleted_at IS NULL AND (chain_id = #{chain_id}
      and receipts.created_at - interval '5 hours' > now() - INTERVAL '24 HOUR')
      GROUP BY receipt_transactions.restaurant_id) a on a.restaurant_id = restaurants.id where restaurants.chain_id = #{chain_id}
      and restaurants.status IS TRUE;
    "
    restaurants = Receipt.find_by_sql(sql)

    restaurant_ids = []
    restaurants.each do |restaurant|
      if restaurant.count.to_i == 0
        restaurant_ids << restaurant.id
      end
    end
    rest = Restaurant.where(id: restaurant_ids)
    #from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email rescue Setting.email.default_from
    from = Setting.email.default_from

    if 1 == 0 and chain_setting_alert && chain_setting_alert.is_active && !restaurant_ids.empty?
      rest = Restaurant.where(id: restaurant_ids)
      #send email
      body = chain_setting_alert.email_content.gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")
      @email_content = body
      @html_body = (@email_content %{
          :restaurant_list => generate_html(rest).html_safe,
      }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")


     # ChainMailer.no_scan_alert_mailer(chain_setting_alert.email, from, chain_setting_alert.email_subject, @html_body.html_safe).deliver
    end
    rr = []
    approv_csv = generate_html_approved_csv(rest)
    r_csv = receipt_status_process_csv(chain_id)
    onl_csv = online_order_generator_csv(chain_id)
    mobile_csv = mobile_payment_restaurant_csv(chain_id)
    replicate_csv = replicate_receipt_restaurant_csv(chain_id)

    rr += r_csv unless r_csv.blank?
    rr += approv_csv unless approv_csv.blank?
    #rr += onl_csv unless onl_csv.blank?
    rr += mobile_csv unless mobile_csv.blank?
    rr += replicate_csv unless replicate_csv.blank?


    pth = []
    pth += onl_csv unless onl_csv.blank?
    pth << creating_csv_file(approv_csv,"approved") unless approv_csv.blank?
    #pth << creating_csv_file(onl_csv,"online_order") unless onl_csv.blank?

    pth << creating_csv_file(r_csv,"alt_receipt")  unless r_csv.blank?
    pth << creating_csv_file(mobile_csv,"mobile_payment") unless mobile_csv.blank?
    pth << creating_csv_file(replicate_csv,"replication") unless replicate_csv.blank?
    # onl_csv.each do |ol_csv|
    #   pth << ol_csv
    # end
    puts pth
    puts "PATH is #{pth}"
    sending_multiple_attachment(pth.reverse,"bb@dailygobble.com", from, chain_setting_alert.email_subject) unless pth.blank?
    #creating_csv(rr,chain_setting_alert.email, from, chain_setting_alert.email_subject)
    #mobile_payment_restaurants(chain_id)
    #replicate_receipt_restaurants(chain_id)
  end

  def generate_html(restaurants)
    str = ""
    str += "Restaurants :<br />"
    str += "<ul>"
    restaurants.each do |restaurant|
      ncr_cloud_id = nil
      ncr_aloha_id = nil
      ao_id = nil

      ncr_cloud_id = "NCR cloud connect id : #{restaurant.ncr_store_id}<br/>" unless restaurant.ncr_store_id.blank?
      ncr_aloha_id = "NCR aloha loyalty store id : #{restaurant.ncr_aloha_loyalty_store_id}<br/>" unless restaurant.ncr_aloha_loyalty_store_id.blank?
      ao_id = "AO id = #{restaurant.aloha_online_id} <br/> " unless restaurant.aloha_online_id.blank?

      last_approved = ReceiptTransaction.where("restaurant_id = #{restaurant.id} and status = 3").last
      last_approved = last_approved.issue_date.strftime('%D %H:%M') unless last_approved.blank?


      str += "<li>Name : #{restaurant.dashboard_display_text} <br/>
                   Last Approved Date: #{last_approved} <br/>
                   Restaurant ID: #{restaurant.id} <br/>
                   #{ncr_cloud_id}
      #{ncr_aloha_id}
      #{ao_id}
              </li>"
    end
    str += "</ul>"

    return str
  end

  def generate_html_alt(restaurants)
    str = ""
    str += "Restaurants :<br />"
    str += "<ul>"
    restaurants.each do |restaurant|
      ncr_cloud_id = nil
      ncr_aloha_id = nil
      ao_id = nil

      ncr_cloud_id = "NCR cloud connect id : #{restaurant.ncr_store_id}<br/>" unless restaurant.ncr_store_id.blank?
      ncr_aloha_id = "NCR aloha loyalty store id : #{restaurant.ncr_aloha_loyalty_store_id}<br/>" unless restaurant.ncr_aloha_loyalty_store_id.blank?
      ao_id = "AO id = #{restaurant.aloha_online_id} <br/> " unless restaurant.aloha_online_id.blank?

      last_approved = ReceiptTransaction.where("restaurant_id = #{restaurant.id} and status = 8").last
      last_approved = last_approved.created_at.strftime('%D %H:%M') unless last_approved.blank?


      str += "<li>Name : #{restaurant.dashboard_display_text} <br/>
                   Last ALT RECEIPT Date: #{last_approved} <br/>
                   Restaurant ID: #{restaurant.id} <br/>
                   #{ncr_cloud_id}
      #{ncr_aloha_id}
      #{ao_id}
              </li>"
    end
    str += "</ul>"

    return str
  end

  def generate_html_alt_csv(restaurants)
    csv_header = []
    csv_header <<["Receipt ALT Notification"]
    csv_header << ["Name", "Last ALT RECEIPT DATE","Restaurant id", "NCR cloud connect id","NCR aloha loyalty store id","AO id", "External Id", "Olo reward process ( olo order id)"]
    restaurants.each do |restaurant|
      ncr_cloud_id = nil
      ncr_aloha_id = nil
      ao_id = nil

      ncr_cloud_id = restaurant.ncr_store_id unless restaurant.ncr_store_id.blank?
      ncr_aloha_id = restaurant.ncr_aloha_loyalty_store_id unless restaurant.ncr_aloha_loyalty_store_id.blank?
      ao_id = restaurant.aloha_online_id unless restaurant.aloha_online_id.blank?

      last_approved = ReceiptTransaction.where("restaurant_id = #{restaurant.id} and status = 8").last
      rd = ReceiptDetail.where(:receipt_id => last_approved.receipt_id).first rescue nil
      unless rd.blank?
        last_approved = rd.issue_date.strftime('%D').to_s + " " + rd.issue_time.strftime("%H:%M") + " " + rd.timezone
      else
        last_approved = last_approved.created_at.strftime('%D %H:%M %Z') unless last_approved.blank?
      end


      csv_header << [
          restaurant.dashboard_display_text,
          last_approved,
          restaurant.id,
          ncr_cloud_id, ncr_aloha_id, ao_id,
          restaurant.external_id,
          (restaurant.restaurant_detail.olo_order_id rescue nil)
      ]


    end
    csv_header <<["***************************************"]

    return csv_header
  end

  def generate_html_approved_csv(restaurants)
    csv_header = []
    csv_header <<["Receipt APPROVED notification"]
    csv_header << ["Name", "Last Approved receipt date","Restaurant id", "NCR cloud connect id","NCR aloha loyalty store id","AO id", "external id", "Olo reward process ( olo order id)"]
    restaurants.each do |restaurant|
      ncr_cloud_id = nil
      ncr_aloha_id = nil
      ao_id = nil

      ncr_cloud_id = restaurant.ncr_store_id unless restaurant.ncr_store_id.blank?
      ncr_aloha_id = restaurant.ncr_aloha_loyalty_store_id unless restaurant.ncr_aloha_loyalty_store_id.blank?
      ao_id = restaurant.aloha_online_id unless restaurant.aloha_online_id.blank?

      last_approved = ReceiptTransaction.where("restaurant_id = #{restaurant.id} and status = 3").last
      rd = ReceiptDetail.where(:receipt_id => last_approved.receipt_id).first rescue nil
      unless rd.blank?
        last_approved = rd.issue_date.strftime('%D').to_s + " " + rd.issue_time.strftime("%H:%M") + " " + rd.timezone
      else
        last_approved = last_approved.created_at.strftime('%D %H:%M %Z') unless last_approved.blank?
      end


      csv_header << [
          restaurant.dashboard_display_text,
          last_approved,
          restaurant.id,
          ncr_cloud_id, ncr_aloha_id, ao_id,
          restaurant.external_id,
          (restaurant.restaurant_detail.olo_order_id rescue nil)
      ]


    end
    csv_header <<["***************************************"]

    return csv_header
  end

  def additional_information(chain_setting_alert)
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    receipt_status =JSON.parse REDIS.get(flag_string + "receipt_status") rescue nil
    online_order_type = JSON.parse REDIS.get(flag_string + "online_order_type") rescue nil
    mp = REDIS.get(flag_string + "mp").to_s == "1" ? true : false
    is_replicate = REDIS.get(flag_string + "is_replicate").to_s == "1" ? true : false
  end

  def receipt_status_process_sql(chain_id)
    ch = Chain.find chain_id
    chain_setting_alert = ch.chain_setting_alert
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email rescue Setting.email.default_from
    receipt_status =JSON.parse REDIS.get(flag_string + "receipt_status") rescue nil
    # RECEIPT_TYPES =  {APPROVED: 1, ALT: 2}
    return if receipt_status.blank?
    condition = []
    #condition << "receipts.status = 3" if receipt_status.include?("1")
    condition << "receipts.status = 8"  if receipt_status.include?("2")
    return if receipt_status.include?("1")

    sql = "
    select restaurants.id, a.count from restaurants left join (SELECT receipt_transactions.restaurant_id ,
      count(receipts.id) FROM receipts LEFT JOIN receipt_transactions on receipt_transactions.receipt_id = receipts.id
      WHERE receipts.deleted_at IS NULL AND (chain_id = #{chain_id} AND #{condition.join(" AND ")}
      and receipts.created_at - interval '5 hours' > now() - INTERVAL '24 HOUR')
      GROUP BY receipt_transactions.restaurant_id) a on a.restaurant_id = restaurants.id where restaurants.chain_id = #{chain_id}
      and restaurants.status IS TRUE ;
    "

    restaurants = Receipt.find_by_sql(sql)

    restaurant_ids = []
    restaurants.each do |restaurant|
      if restaurant.count.to_i == 0
        restaurant_ids << restaurant.id
      end
    end
    if chain_setting_alert && chain_setting_alert.is_active && !restaurant_ids.empty?
      rest = Restaurant.where(id: restaurant_ids)
      #send email
      body = chain_setting_alert.email_content.gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")
      @email_content = body
      @html_body = (@email_content %{
          :restaurant_list => generate_html_alt(rest).html_safe,
      }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")


      ChainMailer.no_scan_alert_mailer(chain_setting_alert.email, from, chain_setting_alert.email_subject, @html_body.html_safe).deliver
    end
  end

  def receipt_status_process_csv(chain_id)
    ch = Chain.find chain_id
    chain_setting_alert = ch.chain_setting_alert
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email rescue Setting.email.default_from
    receipt_status =JSON.parse REDIS.get(flag_string + "receipt_status") rescue nil
    # RECEIPT_TYPES =  {APPROVED: 1, ALT: 2}
    return if receipt_status.blank?
    condition = []
    #condition << "receipts.status = 3" if receipt_status.include?("1")
    condition << "receipts.status = 8"  if receipt_status.include?("2")
    #return if receipt_status.include?("1")
    return if condition.blank?
    sql = "
    select restaurants.id, a.count from restaurants left join (SELECT receipt_transactions.restaurant_id ,
      count(receipts.id) FROM receipts LEFT JOIN receipt_transactions on receipt_transactions.receipt_id = receipts.id
      WHERE receipts.deleted_at IS NULL AND (chain_id = #{chain_id} AND #{condition.join(" AND ")}
      and receipts.created_at - interval '5 hours' > now() - INTERVAL '24 HOUR')
      GROUP BY receipt_transactions.restaurant_id) a on a.restaurant_id = restaurants.id where restaurants.chain_id = #{chain_id}
      and restaurants.status IS TRUE ;
    "

    restaurants = Receipt.find_by_sql(sql)

    restaurant_ids = []
    restaurants.each do |restaurant|
      if restaurant.count.to_i == 0
        restaurant_ids << restaurant.id
      end
    end
    if 1 == 0 &&chain_setting_alert && chain_setting_alert.is_active && !restaurant_ids.empty?

      #send email
      body = chain_setting_alert.email_content.gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")
      @email_content = body
      @html_body = (@email_content %{
          :restaurant_list => generate_html_alt_csv(rest).html_safe,
      }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")


      ChainMailer.no_scan_alert_mailer(chain_setting_alert.email, from, chain_setting_alert.email_subject, @html_body.html_safe).deliver
    end
    rest = Restaurant.where(id: restaurant_ids)
    return generate_html_alt_csv(rest)
  end

  def online_order_sql(chain_id)
    ch = Chain.find chain_id
    chain_setting_alert = ch.chain_setting_alert
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    online_order_type =JSON.parse REDIS.get(flag_string + "online_order_type") rescue nil
    return [] if online_order_type.blank?
    ch = Chain.find chain_id
    res = ""
    condition = []
    condition << "receipts.is_online_order is TRUE" if online_order_type.include?("3")
    condition << "receipts.is_ncr_online_order_receipt is TRUE" if online_order_type.include?("1")
    condition << "receipts.is_olo is TRUE" if online_order_type.include?("2")
    condition.each do |cc|
      sql = "
    select restaurants.id, a.count from restaurants left join (SELECT receipt_transactions.restaurant_id ,
      count(receipts.id) FROM receipts LEFT JOIN receipt_transactions on receipt_transactions.receipt_id = receipts.id
      WHERE receipts.deleted_at IS NULL AND (chain_id = #{ch.id} AND #{cc}
      and receipts.created_at - interval '5 hours' > now() - INTERVAL '24 HOUR')
      GROUP BY receipt_transactions.restaurant_id) a on a.restaurant_id = restaurants.id where restaurants.chain_id = #{ch.id}
      and restaurants.status IS TRUE;
    "

      restaurants = Receipt.find_by_sql(sql)

      restaurant_ids = []
      restaurants.each do |restaurant|
        #p restaurant.count
        #p "xx"
        if restaurant.count.to_i == 0
          restaurant_ids << restaurant.id
        end
      end
      p restaurant_ids
      res += generate_html_online_order(restaurant_ids, cc)
      #p res
      #p "auoooo"
    end
    sending_email(ch,res)
  end

  def online_order_generator_csv(chain_id)
    ch = Chain.find chain_id
    pth =[]
    chain_setting_alert = ch.chain_setting_alert
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    online_order_type =JSON.parse REDIS.get(flag_string + "online_order_type") rescue nil
    return [] if online_order_type.blank?

    ch = Chain.find chain_id
    res = []
    condition = []
    condition << "receipts.is_online_order is TRUE" if online_order_type.include?("3")
    condition << "receipts.is_ncr_online_order_receipt is TRUE" if online_order_type.include?("1")
    condition << "receipts.is_olo is TRUE" if online_order_type.include?("2")
    condition.each do |cc|
      types = "NCR" if cc.include?("is_ncr_online_order_receipt")
      types = "OLO" if cc.include?("is_olo")
      types = "ONOSYS" if cc.include?("is_online_order")
      sql = "
    select restaurants.id, a.count from restaurants left join (SELECT receipt_transactions.restaurant_id ,
      count(receipts.id) FROM receipts LEFT JOIN receipt_transactions on receipt_transactions.receipt_id = receipts.id
      WHERE receipts.deleted_at IS NULL AND (chain_id = #{ch.id} AND #{cc}
      and receipts.created_at - interval '5 hours' > now() - INTERVAL '24 HOUR')
      GROUP BY receipt_transactions.restaurant_id) a on a.restaurant_id = restaurants.id where restaurants.chain_id = #{ch.id}
      and restaurants.status IS TRUE;
    "

      restaurants = Receipt.find_by_sql(sql)

      restaurant_ids = []
      restaurants.each do |restaurant|
        #p restaurant.count
        #p "xx"
        if restaurant.count.to_i == 0
          restaurant_ids << restaurant.id
        end
      end
      p restaurant_ids
      res = generate_html_online_order_csv(restaurant_ids, cc)
      pth << creating_csv_file(res,"online_order_#{types}")# unless res.blank?

    end
    return pth
  end


  def mobile_payment_restaurant_csv(chain_id)
    p "MOBILE PAYMENT RESTAURANT START"
    ch = Chain.find chain_id
    chain_setting_alert = ch.chain_setting_alert
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    mp_status = REDIS.get(flag_string + "mp").to_s == "1" ? true : false

    if mp_status == true
      restaurant_ids = []
      uph = UserPaymentHistory.joins(:user).where("users.chain_id = #{chain_id} and user_payment_histories.created_at - interval '24 hours' > now() - INTERVAL '8 HOUR'").select('user_payment_histories.id,receipt_id')
      uph.each do |u|
        restaurant_ids << u.receipt.last_transaction.restaurant_id rescue nil
      end

      restaurant_ids = restaurant_ids.uniq
      #p restaurant_ids.size

      restaurants = Restaurant.where("chain_id = #{chain_id} and id not in(?)", restaurant_ids.compact).select('*')
      if restaurants.blank? # therea re no replicated anyway
        restaurants =Restaurant.where("chain_id = #{chain_id}").select('*')
      end

      res = generate_html_mobil_payment_csv(restaurants)
      return res
      #sending_email(ch,res)
    else
      return ""
    end
  end

  def mobile_payment_restaurants(chain_id)
    p "MOBILE PAYMENT RESTAURANT START"
    ch = Chain.find chain_id
    chain_setting_alert = ch.chain_setting_alert
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    mp_status = REDIS.get(flag_string + "mp").to_s == "1" ? true : false

    if mp_status == true
      restaurant_ids = []
      uph = UserPaymentHistory.joins(:user).where("users.chain_id = #{chain_id} and user_payment_histories.created_at - interval '5 hours' > now() - INTERVAL '8 HOUR'").select('user_payment_histories.id,receipt_id')
      uph.each do |u|
        restaurant_ids << u.receipt.last_transaction.restaurant_id rescue nil
      end

      restaurant_ids = restaurant_ids.uniq
      p restaurant_ids.size

      restaurants = Restaurant.where("chain_id = #{chain_id} and id not in(?)", restaurant_ids.compact).select('id,name, external_id, status,dashboard_display_text')
      if restaurants.blank? # therea re no replicated anyway
        restaurants =Restaurant.where("chain_id = #{chain_id}").select('id,name, external_id, status, dashboard_display_text')
      end

      res = generate_html_mobil_payment(restaurants)
      #return res
      sending_email(ch,res)
    else
      return ""
    end
  end

  def replicate_receipt_restaurants( chain_id)
    ch = Chain.find chain_id
    chain_setting_alert = ch.chain_setting_alert
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    is_replicate = REDIS.get(flag_string + "is_replicate").to_s == "1" ? true : false

    if is_replicate == true
      sql = "
select distinct(restaurant_id) from rep_receipt_transaction_ncrs rt
inner join restaurants r on r.id = rt.restaurant_id
 where rt.created_at - interval '5 hours' > now() - INTERVAL '48 HOUR'
and chain_id = #{chain_id}
"
      rts = ReceiptTransaction.find_by_sql(sql)
      restaurant_ids = rts.map(&:restaurant_id)
      restaurants =Restaurant.where("chain_id = #{chain_id} and id not in(?)", restaurant_ids.compact).select('*') unless restaurant_ids.compact.blank?
      if restaurants.blank? # therea re no replicated anyway
        restaurants =Restaurant.where("chain_id = #{chain_id}").select('*')
      end

      res = generate_html_replicated(restaurants)
      #return res
      sending_email(ch,res)
    else
      return ""
    end
  end

  def replicate_receipt_restaurant_csv( chain_id)
    ch = Chain.find chain_id
    chain_setting_alert = ch.chain_setting_alert
    flag_string = "chain_setting_alert_#{chain_setting_alert.id}"
    is_replicate = REDIS.get(flag_string + "is_replicate").to_s == "1" ? true : false

    if is_replicate == true
      sql = "
select distinct(restaurant_id) from rep_receipt_transaction_ncrs rt
inner join restaurants r on r.id = rt.restaurant_id
 where rt.created_at - interval '5 hours' > now() - INTERVAL '48 HOUR'
and chain_id = #{chain_id}
      "
      rts = ReceiptTransaction.find_by_sql(sql)
      restaurant_ids = rts.map(&:restaurant_id)
      restaurants =Restaurant.where("chain_id = #{chain_id} and id not in(?)", restaurant_ids.compact).select('*') unless restaurant_ids.compact.blank?
      if restaurants.blank? # therea re no replicated anyway
        restaurants =Restaurant.where("chain_id = #{chain_id}").select('*')
      end

      res = generate_html_replicated_csv(restaurants)
      return res
      #sending_email(ch,res)
    else
      return ""
    end
  end

  # def latest_replicate_date(restaurants)
  #   return if restaurants.blank?
  #
  #   sql = " select max(created_at),restaurant_id from rep_receipt_transaction_ncrs where restaurant_id in (#{restaurants.map(&:id).join(',')}) group by restaurant_id"
  #   res = ReceiptTransaction.find_by_sql(sql)
  #   return res
  # end

  def generate_html_replicated_csv(result)
    csv = []
    csv << ["REPLICATION NOTIFICATION"]
    csv << ["Name","Last replication Date", "Restaurant id", "NCR cloud connect id","NCR aloha loyalty store id","AO id", "External id","Olo reward process ( olo order id)"]
    result.each do |restaurant|
      next if restaurant.status != true
      sql = "select created_at from  rep_receipt_transaction_ncrs where restaurant_id = #{restaurant.id} order by id desc limit 1"
      res = Receipt.find_by_sql(sql)
      ncr_cloud_id = restaurant.ncr_store_id unless restaurant.ncr_store_id.blank?
      ncr_aloha_id = restaurant.ncr_aloha_loyalty_store_id unless restaurant.ncr_aloha_loyalty_store_id.blank?
      ao_id = restaurant.aloha_online_id unless restaurant.aloha_online_id.blank?

      csv << [
          restaurant.dashboard_display_text,
          (res.first.created_at.strftime('%D %H:%M %Z') rescue "-"),
          restaurant.id, ncr_cloud_id, ncr_aloha_id, ao_id,
          restaurant.external_id,
          (restaurant.restaurant_detail.olo_order_id rescue nil)
      ]

    end
    csv <<["***************************************"]
    return csv
  end

  def generate_html_replicated(result)
    str = "------------------------------ REPLICATION NOTIFICATION ----------------------------------------------------"
    str += "Restaurants :<br />"
    str += "<ul>"
    p result
    result.each do |restaurant|
      sql = "select created_at from  rep_receipt_transaction_ncrs where restaurant_id = #{restaurant.id} order by id desc limit 1"
      res = Receipt.find_by_sql(sql)
      next if restaurant.status != true
      str += "<li>Name : #{restaurant.dashboard_display_text} <br/>
                   Last Replication Date: #{(res.first.created_at rescue "-")} <br/>
                   Restaurant ID: #{restaurant.id} <br/>
                   External ID : #{restaurant.external_id}
              </li><hr/>"
    end
    str += "</ul>"

    return str
  end

  def generate_html_mobil_payment_csv(restaurants)
    return if restaurants.blank?
    csv_header  = []
    csv_header << ["MOBILE PAYMENT NOTIFICATION"]
    csv_header << ["Name", "Last mobile Payment", "Restaurant ID", "NCR cloud connect id","NCR aloha loyalty store id","AO id", "External Id", "Olo reward process ( olo order id)"]

    restaurants.each do |restaurant|
      next if restaurant.status != true
      ncr_cloud_id = restaurant.ncr_store_id unless restaurant.ncr_store_id.blank?
      ncr_aloha_id = restaurant.ncr_aloha_loyalty_store_id unless restaurant.ncr_aloha_loyalty_store_id.blank?
      ao_id = restaurant.aloha_online_id unless restaurant.aloha_online_id.blank?

      csv_header <<[restaurant.dashboard_display_text,
                   find_last_mobile_payment(restaurant),
                   restaurant.id,ncr_cloud_id, ncr_aloha_id, ao_id, restaurant.external_id, (restaurant.restaurant_detail.olo_order_id rescue nil)
]


    end
    csv_header <<["***************************************"]
    return csv_header
  end


  def find_last_mobile_payment(restaurant)
    rt=ReceiptTransaction.where(:restaurant_id => restaurant.id).select("receipt_id").order('id desc').limit(500)

    uph=    UserPaymentHistory.where("receipt_id in(?)", rt.map(&:receipt_id)).select("max(created_at)")
    return uph.first.max rescue nil
  end

  def generate_html_online_order(restaurant_ids, types)
    return if restaurant_ids.blank?
    types = "NCR" if types.include?("is_ncr_online_order_receipt")
    types = "OLO" if types.include?("is_olo")
    types = "ONOSYS" if types.include?("is_online_order")

    str = "------------------------------ Online ORDER TYPE #{types} NOTIFICATION ----------------------------------------------------"
    str += "Restaurants :<br />"
    str += "<ul>"
    restaurant_ids.each do |restaurant|
      restaurant = Restaurant.find restaurant
      next if restaurant.status != true
      str += "<li>Name : #{restaurant.dashboard_display_text} <br/>
                  Restaurant ID: #{restaurant.id} <br/>
                  Last #{types} transaction : #{last_online_order_time(restaurant,types)}<br/>
                  <hr/>
              </li>"
    end
    str += "</ul>"

    return str
  end

  def generate_html_online_order_csv(restaurant_ids, types)
    return if restaurant_ids.blank?
    types = "NCR" if types.include?("is_ncr_online_order_receipt")
    types = "OLO" if types.include?("is_olo")
    types = "ONOSYS" if types.include?("is_online_order")
    csv_header = []

    csv_header <<["Online Order Type #{types} Notification"]
    csv_header << ["Name", "Restaurant ID", "Last #{types} transaction", "NCR cloud connect id","NCR aloha loyalty store id","AO id", "External ID","Olo reward process ( olo order id)"]


    restaurant_ids.each do |restaurant|
      restaurant = Restaurant.find restaurant
      next if restaurant.status != true
      ncr_cloud_id = restaurant.ncr_store_id unless restaurant.ncr_store_id.blank?
      ncr_aloha_id = restaurant.ncr_aloha_loyalty_store_id unless restaurant.ncr_aloha_loyalty_store_id.blank?
      ao_id = restaurant.aloha_online_id unless restaurant.aloha_online_id.blank?


      csv_header << [restaurant.dashboard_display_text,
                   restaurant.id,
      last_online_order_time(restaurant,types),ncr_cloud_id, ncr_aloha_id, ao_id,restaurant.external_id, (restaurant.restaurant_detail.olo_order_id rescue nil)

      ]

    end
    csv_header <<["***************************************"]

    return csv_header
  end

  def last_online_order_time(restaurant,types)
    con = "is_ncr_online_order_receipt" if types =="NCR"
    con = "is_olo" if types == "OLO"
    con = "is_online_order" if types == "ONOSYS"
    #puts "TYPES ONLINE ORDER TIME ==== #{types}"
    if types == "ONOSYS"
      a = Onosys::OnlineOrder.where(:restaurant_id => restaurant.id).order("id desc").first.try(:created_at).strftime('%D %H:%M %Z') rescue nil
      puts "#{a} TIME"
      return a
    elsif types == "OLO"
      a = OloWebhook.where("restaurant like '%#{restaurant.name}%' and is_match_restauran is TRUE").order("id desc").first.try(:created_at).strftime('%D %H:%M %Z') rescue nil
      return a
    elsif types =="NCR"
      sql = "select created_at from ncr_online_orders where restaurant_id = #{restaurant.id} order by id desc limit 1"
      res = Receipt.find_by_sql(sql)
      a = res.first.created_at.strftime('%D %H:%M %Z') rescue nil
      #puts "#{a} TIME"
      return a
    end

  end

  def sending_email(chain, rest)
    chain_setting_alert = chain.chain_setting_alert
    if chain_setting_alert && chain_setting_alert.is_active
      #send email
      from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email rescue Setting.email.default_from
      body = chain_setting_alert.email_content.gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")
      @email_content = body
      @html_body = (@email_content %{
          :restaurant_list => rest.html_safe,
      }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")


      ChainMailer.no_scan_alert_mailer(chain_setting_alert.email, from, chain_setting_alert.email_subject, @html_body.html_safe).deliver
    end
  end

  def creating_csv(result,email,from, email_subject)
    path = "#{Rails.root}/tmp/no_scan_alert_tmp.csv"
    #p result
    #p "EAAAAAAAAAAAAAAAAAAAAAAAAA"
    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      result.each do |x|
          csv << x
      end
    end
    #OwnerMailer.send_reward_redeemed(nil, email, path, "no_scan_alert.csv", email_subject).deliver!
    ChainMailer.no_scan_alert_mailer(email, from, email_subject, "Pls check the files", path, "no_scan_alert.csv").deliver!
  end

  def creating_csv_file(result, type)
     path = "#{Rails.root}/tmp/#{type}.csv"

     transaction_csv = CSV.open(path, "wb") do |csv|
       # header row
       result.each do |x|
         csv << x
       end
     end

     return path
  end

  def sending_multiple_attachment(paths,email,from, email_subject)
    ChainMailer.no_scan_alert_multi_mailer(email, from, email_subject, "Pls check the files", paths, "no_scan_alert.csv").deliver!
  end
end
