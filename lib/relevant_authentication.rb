module RelevantAuthentication

  def set_device_token_as_unique_for_user(user, device_type, device_token, device_id)
    p "*******************DEVICE ID**********************************"
    p "device id is #{device_id}"
    p "device type is #{device_type}"
    p "device token is #{device_token}"

    p "*************************************************************"
    User.transaction do
      if device_type.downcase.eql?("android")
        if !device_id.blank? && !device_token.blank?
          users = User.where(:chain_id => user.chain_id, :device_id => device_id, :device_token => device_token, :sign_in_device_type => device_type)
        else
          users = []
        end
      elsif device_type.downcase.eql?("iphone")
        unless device_token.blank?
          users = User.where(:chain_id => user.chain_id, :device_token => device_token)
        else
          users = []
        end
      end
      users.each do |u|
        u.update_columns(device_token: nil, sign_in_device_type: nil) unless u.eql?(user)
      end  unless users.blank?

      user.update_columns(
          sign_in_device_type: device_type,
          device_token: device_token,
          device_id: device_type.downcase.eql?("android") ? device_id : nil
      )

    end
  end

end