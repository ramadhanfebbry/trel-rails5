module RelevantGuestDna

  include RelevantAuthentication

  def migrate_old_guest_dna_user_to_relevant(params)
    guest_dna_user = GuestDnaUser.where(:email => params[:email].downcase, :user_id => nil).first
    if guest_dna_user
      # return {:status => false, :notice => "We are sorry. We could not complete your request. Please try again. If error persists, reach out to our support team from the app."} unless guest_dna_user.user_id.blank?
      user = User.new(params[:user])
      user.first_name = guest_dna_user.first_name
      user.last_name = guest_dna_user.last_name
      user.name = [user.first_name, user.last_name].join(" ")
      user.set_birthday(guest_dna_user.birthday)
      user.password = user.random_password if user.password.blank?
      user.zipcode = guest_dna_user.zip unless guest_dna_user.zip.blank?
      user.marketing_optin = guest_dna_user.optin || 0
      user.gender = guest_dna_user.gender
      user.created_at = guest_dna_user.signup_date || Time.zone.now
      user.phone_number = params[:phone_number] unless params[:phone_number].blank?
      user.special_occassion = params[:special_occassion] unless params[:special_occassion].blank?
      if user.locale_id.blank?
        user.locale_id = user.try(:chain).try(:locales).try(:first).try(:id) || Locale.find_by_key('en').try(:id)
      end
      user.user_profile = UserProfile.new(:guest_dna_user_acc_key => guest_dna_user.account_key)
      #should join to table favorites to get the text only, not the ids
      p "user from guest dna---"
      p user
      if user.save
        guest_dna_user.update_attributes(:user_id => user.id, :migration_date => Time.zone.now, :migrated => true)
        Delayed::Job.enqueue(RelevantGuestDnaDeviceAndPointStuffJob.new(user, params, guest_dna_user))
        user.update_phone_info(params[:phone_model], params[:os], params[:appkey])
        if params[:register_type].eql?("1")
          pass = user.random_password
          chk = user.update_attributes(:password => pass, :forgot_password => true)
          if (chk)
            return {:status => true, :notice => "A new password has been emailed to you as a security measure. Please login to access your account. We hope you enjoy the new app!", :nav_main_page => true}
          else
            p "error updating password process. Guest DNA"
            p user.errors
            return {:status => false, :notice => "We are sorry. We could not complete your request. Please try again. If error persists, reach out to our support team from the app."}
          end
        else
          return "CONT"
        end
      else
        p "error on creating user on Guest DNA"
        p user.errors
        if user.errors.messages[:phone_number].blank?
          return {:status => false, :notice => "We are sorry. We could not complete your request. Please try again. If error persists, reach out to our support team from the app."}
        else
          return {:status => false, :notice => "Please enter a valid 10 digit phone number. If your phone number was used before, please try with your secondary phone number."}
        end

      end
    else
      return  "CONT"
    end
  end

  def forgot_password_migrate_w_guest_dna(params)
    if params[:migrate].to_i == 2
      guest_dna_user = GuestDnaUser.where(:email => params[:email].downcase).first
      if guest_dna_user && !guest_dna_user.migrated
        p "Implies user was not migrated and we do not want to create a password as user is not on Relevant."
        p "Urging user to Sign up will handle new password creation."
        return {:status => false, notice: "Please sign up to continue using the app."}
      elsif guest_dna_user && guest_dna_user.migrated
        p "Implies there is an error associated with  user migration.. ie the user's account as he is not on Relevant but status implies he was migrated"
        return {:status => false, notice: "Please sign up to continue using the app. In case of any issues, please contact support from the app."}
      else
        return "CONT"
      end
    end
  end

end