class DailySyncRelevantToRevelJob < Struct.new(:chain)

  def perform
    begin
      p "perfom DailySyncRelevantToRevelJob"
      require 'net/ftp'
      success_users = []
      failed_users = []

      run_time_est = Time.zone.now.in_time_zone("EST")
      selected_run_date = run_time_est - 1.day
      run_time_est_begin = selected_run_date.beginning_of_day
      run_time_est_end = selected_run_date.end_of_day
      users = chain.users.select("chain_id, phone_number, id").where(:updated_at => run_time_est_begin..run_time_est_end )

      if users.present?
        ftp =Net::FTP.new
        ftp.passive = true
        ftp.connect(chain.relevant_to_revel_ftp_url, chain.relevant_to_revel_ftp_port)
        ftp.login(chain.relevant_to_revel_ftp_username, chain.relevant_to_revel_ftp_password)
        begin
          ftp.chdir(chain.relevant_to_revel_ftp_root_dir)
        rescue => e
          puts "Relevant to Revel #{e}"
        end

        date = Date.today
        log = RevelLog.create(:chain_id => chain.id, :log_type => "relevant to revel", :status => "in queue")
        log.update_attributes(:status => "processing", :initiate_at => Time.now)

        file_name = "#{date.strftime('%m')}_#{date.strftime('%d')}_#{date.strftime('%Y')}_Relevant_to_Revel_SKTrans.csv"
        file = "#{Rails.root}/tmp/#{file_name}"
        File.open(file, "w") do |csv|
          users.each do |user|
            unless user.try(:phone_number).blank?
              csv << user.phone_number
              csv << "\n"
              success_users << user.id
            else
              failed_users << user.id
            end
          end
        end

        log.update_attributes(
            :success_users => success_users,
            :failed_users => failed_users,
            :status => "completed",
            :completed_at => Time.now,
            :attachment => File.open(file)
        )
      end

      ftp.passive = true
      ftp.putbinaryfile(file) if users
      ftp.close

      p "END DailySyncRelevantToRevelJob"

    rescue
      if users.present?
        log.update_attributes(:status =>  "failed", :completed_at => Time.now, :error_report => exception.to_s)  unless log.blank?
      end
    end
  end

  def error(job, exception)
    p "---------FB Synch erroor handling-------"
    struct = YAML::load(job.handler)
    chain = struct.chain
    ChainMailer.mailchimp_sync_log_error_report(chain, exception.to_s, "Fishbowl").deliver
    p "-----------------finishhhhh------"
  end


end