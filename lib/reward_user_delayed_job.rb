class RewardUserDelayedJob < Struct.new(:params)

  def perform
     reward_wallet_user =  RewardWallet.create(params)
     reward_wallet_user.set_expired_based_on_setting_expire_and_reward
  end

  def error
    
  end

end