class SiteJasperHealth
  require "net/http"
  require "uri"

  attr_accessor :url

  # how to run
  # health=SiteJasperHealth.new(nil)
  # health.check_health


  def initialize(url)
    @url = url || "http://jr.relevantmobile.com:8080/jasperserver/"
  end

  def check_health
    uri = URI.parse(@url)
    result = false
    puts "*" * 10
    puts "Check health jasper :: #{@url}"
    response = Net::HTTP.get_response(uri) rescue nil
    puts "Response : #{response}"

    result= true if response and response.code == "200"
    return result
  end

  def insert_ids_to_redis
    ReportSubscription.all.each do |rs|
      begin
        if rs.report_definition.active == true
          time_to_run_at(rs)
        end
      rescue
        next
      end
    end
    # when site was down. hold the ids so when server is up, we could knew what should re-run
  end

  def time_to_run_at(subscription)
    ## this is where we check the daily /weekly/ monthly setting, and then generate their report
    rs = subscription.report_settings
    ra = subscription.run_at
    ids= REDIS.get("rety_subscription_ids")
    ids = ids.to_s
    time_zone_now = Time.zone.now.in_time_zone(rs["time_zone"]) rescue Time.zone.now.in_time_zone('EST')

    if subscription.report_kind == 2 and
        time_zone_now.wday == rs["week_day"].to_i and
        time_zone_now.hour == subscription.run_at.hour ## weekly
      puts "ReportSubscriptionJob::weekly executed retry inserting to redis id #{subscription.id.to_s}"
      ids += '_' + subscription.id.to_s
    elsif subscription.report_kind == 1 and time_zone_now.hour == subscription.run_at.hour
      puts "ReportSubscriptionJob::daily Executed retry inserting to redis id #{subscription.id.to_s}"
      ids += '_' + subscription.id.to_s
    elsif subscription.report_kind == 3
      exec_date = time_zone_now.end_of_month
      if subscription.report_settings["month_day"] == -1
        exec_date = exec_date
      elsif subscription.report_settings["month_day"] == 0
        exec_date = exec_date + 1.days
      else
        exec_date = time_zone_now.end_of_month
      end
      if subscription.run_at.hour == time_zone_now.hour and time_zone_now.to_date == exec_date.to_date
        puts "ReportSubscriptionJob::monthly retry inserting to redis id #{subscription.id.to_s}"
        ids += '_' + subscription.id.to_s
      end
    end

    REDIS.set("rety_subscription_ids", ids)
  end

  def retry_subscription_from_redis
    ids = REDIS.get("rety_subscription_ids")

    unless ids.blank?
      ids = ids.split('_')

      ids.uniq.each do |subscription_id|
        subscription = ReportSubscription.find subscription_id rescue next
        ReportJob::ReportGenerator.generate_report(get_chain_id(subscription), subscription.report_definition, subscription)
      end

    end
    REDIS.set("rety_subscription_ids",nil)
  end

  def get_chain_id(subscription)
    ## get chain id for this report
    res = subscription.custom_params["chain_id"].to_i rescue subscription.chain_id
    if res == 0
      return subscription.chain_id
    else
      return res
    end
  end

  def get_retry_ids_from_redis
    ids= REDIS.get("rety_subscription_ids")
    return ids
  end
end