class DailySyncRevelToRelevantJob < Struct.new(:chain, :files_executed, :log, :type)

  def perform
    p "perform DailySyncRevelToRelevantJob"
    log.update_attributes(:status => "processing", :initiate_at => Time.now)
    success_users = []
    failed_users = []
    file_type = ""
    file = files_executed
    if file.split(".").last.downcase == "xls"
      file_type = "xls"
    elsif file.split(".").last.downcase == "csv"
      file_type = "csv"
    end

    if type == "manual_sync"
      p "manual_sync"
      aw,bucket = Reward.initialize_aws
      s3_path = aw.buckets[bucket].objects["#{chain.id}_#{file.split('.').first}_temporary"].url_for(:read).to_s
      revel_file = Rails.root.join('tmp', file)
      File.open(revel_file, 'wb') do |file|
        file.write(open(s3_path).read)
      end
    elsif type == "ftp_sync"
      require 'net/ftp'
      ftp =Net::FTP.new
      ftp.passive = true
      ftp.connect(chain.revel_to_relevant_ftp_url, chain.revel_to_relevant_ftp_port)
      ftp.login(chain.revel_to_relevant_ftp_username, chain.revel_to_relevant_ftp_password)
      targetname = "revel_sync" + rand(1000).to_s + "." + file_type
      begin
        ftp.chdir(chain.revel_to_relevant_ftp_root_dir)
      rescue => e
        puts "Revel to Relevant #{e}"
      end
      ftp.getbinaryfile("#{file}","tmp/#{targetname}")
      ftp.close
      revel_file = "#{Rails.root}/tmp/#{targetname}"
      p "ftp_sync"
      p "revel_file FTP: #{revel_file}"
    end

    p "revel_file #{revel_file}"
    if File.exist?(revel_file)
      if file_type == "xls"
        p "Processing by XLS"
        book = Spreadsheet.open(revel_file)
        sheet1 = book.worksheet 0
        sheet1.each 1 do |row|
          user_code_setting = chain.user_code_setting
          user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
          user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE

          unless row[2].blank?
            usercode = row[2]
            usercode = usercode.remove_prepostfix(user_code_setting["prefix"].to_s)
            usercode = usercode.remove_prepostfix(user_code_setting["postfix"].to_s)
            usercode = usercode.remove_zero_padding(user_code_setting["zero_padding"].to_i)
            user = User.where(chain_id: chain.id, phone_number: usercode).first rescue nil
            if user
              if row[4].to_i > 0
                user.earn(row[4].to_i)
                #PointHistory.add_to_history(user.id, "Revel Incentive", row[4].to_i)
                date = Time.zone.parse(row[0]).utc rescue Time.now.utc
                p "revel date: #{row[0]} ------ #{date}"
                PointHistory.create(:created_at => date, :user_id => user.id, :description => "Revel Incentive", :point => row[4].to_i)
                success_users << user.id
              else
                failed_users << user.id
              end
            end
          end
        end
        log.update_attribute("status", "completed")
      elsif file_type == "csv"
        p "Processing by CSV"
        sheet1 = CSV.read(revel_file, {:headers => false, :encoding => 'iso-8859-1:utf-8'})
        sheet1.each do |row|
          user_code_setting = chain.user_code_setting
          user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
          user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE

          unless row[2].blank?
            usercode = row[2]
            usercode = usercode.remove_prepostfix(user_code_setting["prefix"].to_s)
            usercode = usercode.remove_prepostfix(user_code_setting["postfix"].to_s)
            usercode = usercode.remove_zero_padding(user_code_setting["zero_padding"].to_i)
            user = User.where(chain_id: chain.id, phone_number: usercode).first rescue nil
            if user.present?
              if row[4].to_i > 0
                user.earn(row[4].to_i)
                #PointHistory.add_to_history(user.id, "Revel Incentive", row[4].to_i)
                date = Time.zone.parse(row[0]).utc rescue Time.now.utc
                p "revel date: #{row[0]} ------ #{date}"
                PointHistory.create(:created_at => date, :user_id => user.id, :description => "Revel Incentive", :point => row[4].to_i)
                success_users << user.id
              else
                failed_users << user.id
              end
            end
          end
        end
        log.update_attribute("status", "completed")
      else
        log.update_attribute("status", "failed")
        log.update_attribute("error_report", "Error processing the file: wrong format file.")
        ChainMailer.mailchimp_sync_log_error_report(chain, "Error processing the file: wrong format file.", "REVEL TEST ENV").deliver
        ChainMailer.error_report_for_tester(chain, "Error processing the file: wrong format file.", "REVEL TEST ENV").deliver
      end

      log.update_attributes(
          :success_users => success_users,
          :failed_users => failed_users,
          :completed_at => Time.now,
          :attachment_file_name => file
      )
      log.update_attribute("attachment", File.open(revel_file)) if type == "ftp_sync"
    else
      p "file is missing . . ."
      log.update_attributes(
          :success_users => [],
          :failed_users => [],
          :status => "failed",
          :completed_at => Time.now,
          :attachment_file_name => file,
          :error_report => "file is missing or wrong format file."
      )
      log.update_attribute("attachment", File.open(revel_file)) if type == "ftp_sync"
      ChainMailer.mailchimp_sync_log_error_report(chain, "Error processing the file.", "REVEL TEST ENV").deliver
      ChainMailer.error_report_for_tester(chain, "Error processing the file.", "REVEL TEST ENV").deliver
    end

    p "END DailySyncRevelToRelevantJob"
  end

  def error(job, exception)
    p "---------revel Synch erroor handling-------"
    struct = YAML::load(job.handler)
    chain = struct.chain
    log = struct.log
    log.update_attributes(:status =>  "failed", :completed_at => Time.now, :error_report => exception.to_s)  unless log.blank?
    p log
    ChainMailer.mailchimp_sync_log_error_report(chain, "Error processing the file:  #{exception.to_s}", "REVEL TEST ENV").deliver
    ChainMailer.error_report_for_tester(chain, "Error processing the file:  #{exception.to_s}", "REVEL TEST ENV").deliver
    p "-----------------finishhhhh------"
  end


end