class Olo::Order
  require 'rest-client'
  require 'nokogiri'

  attr_accessor :olo_url, :key, :batch_ids, :export_url
  #DbYublCut7IqBbVlccheN0OAnF9Wh500yLEI3wDZnFz2-YunsdzdYmD8GjF7sqcf
  #x = Olo::Order.new
  #start_date = Time.now - 12.days
  #end_date = Time.now + 5.minutes
  #x.list_order_summaries(start_date, end_date)
  #x.orders

  def initialize
    olo_key = ENV["OLO_KEY"] || "DbYublCut7IqBbVlccheN0OAnF9Wh500yLEI3wDZnFz2-YunsdzdYmD8GjF7sqcf"
    @olo_url = Setting.olo["url_staging"] if ENV['DEFAULT_HOST_EMAIL'].to_s.include?('trelevant')
    @olo_url = Setting.olo["url_live"] if ENV['DEFAULT_HOST_EMAIL'].to_s.include?('prelevant')
    @olo_url = ENV["OLO_URL"]
    if ENV['DEFAULT_HOST_EMAIL'].to_s.blank?
      @olo_url = Setting.olo["url_staging"]
    end
    @key = "?key="+olo_key
    @batch_ids = []
    @export_url = nil
  end

  def list_order_summaries(start_date, end_date)
    url = list_url(start_date, end_date)
    #url = "https://api.olostaging.com:443/v1.1/reports/orders?start=2014-01-01&end=2014-01-01&key=DbYublCut7IqBbVlccheN0OAnF9Wh500yLEI3wDZnFz2-YunsdzdYmD8GjF7sqcf"
    puts "*****"
    puts url
    RestClient.proxy = ENV["QUOTAGUARDSTATIC_URL"] || "http://quotaguard2027:330181881e83@us-east-1-static-brooks.quotaguard.com:9293"
    response = RestClient.get url, :content_type => :json
    puts response

    if response.code == 200
      return response
    else
      render :text => "Response Error list_order_summaries"
    end
  end


  def list_url(start_date, end_date)
    start_date = start_date.strftime("%Y-%m-%d")
    end_date = end_date.strftime("%Y-%m-%d")

    @olo_url+"reports/orders#{@key}&start=#{start_date}&end=#{end_date}"
  end

  def orders
    # 1. List all order exports awaiting download
    # GET /orderexports
    url = @olo_url+orders_url+@key
    puts url
    RestClient.proxy = ENV["QUOTAGUARDSTATIC_URL"] || "http://quotaguard2027:330181881e83@us-east-1-static-brooks.quotaguard.com:9293"
    response = RestClient.get(url)
    puts response
    puts response

    noko= Nokogiri.parse(response)
    @batch_ids = noko.xpath('//batches//batchId').map(&:text)
    @batch_ids = @batch_ids.flatten


    if response.code == 200
      return response
    else
      render :text => "Response Error orders"
    end
  end

  def orders_url
    "orderexports"
  end

  def get_batch(batch_id, chain_id = nil)
    #url = "https://api.olostaging.com:443/v1.1/orderexports/5?key=DbYublCut7IqBbVlccheN0OAnF9Wh500yLEI3wDZnFz2-YunsdzdYmD8GjF7sqcf"
    url = @olo_url+orders_url+"/"+batch_id.to_s+@key

    RestClient.proxy = ENV["QUOTAGUARDSTATIC_URL"] || "http://quotaguard2027:330181881e83@us-east-1-static-brooks.quotaguard.com:9293"
    puts url
    #response = RestClient.post(url,{})
    #response.follow_redirection(request, result, &block)
    #puts response
    #puts response.code

    RestClient.post(url, {}) do |response, request, result, &block|
      if [301, 302, 307].include? response.code
        redirected_url = response.headers[:location]
        puts redirected_url

        @export_url = redirected_url

        Delayed::Job.enqueue(Olo::OrderBatchJob.new(redirected_url, batch_id, chain_id))
        #puts "getting file"
        #File.open("#{Rails.root}/tmp/batch_#{batch_id}.zip", "w+") { |f| f << response.force_encoding("utf-8") }
        #puts "writing file"
        #Zip::File.open("#{Rails.root}/tmp/batch_#{batch_id}.zip") do |zip_file|
        #  # Handle entries one by one
        #  zip_file.each do |entry|
        #    # Extract to file/directory/symlink
        #    puts "Extracting #{entry.name}"
        #
        #    xml_filename = entry.name.gsub('batch','batch_'+batch_id.to_s)
        #    if File.exist?("#{Rails.root}/tmp/#{xml_filename}")
        #      File.delete("#{Rails.root}/tmp/#{xml_filename}")
        #    end
        #    entry.extract("#{Rails.root}/tmp/#{xml_filename}")
        #
        #    ol = OloOrder.where(:batch_id => batch_id).first
        #
        #    if ol.blank?
        #      ol = OloOrder.new(:batch_id => batch_id, :xml_data => File.open("#{Rails.root}/tmp/#{xml_filename}", "r").read)
        #      ol.save
        #    else
        #      ol = ol.update_attributes(
        #          :xml_data => File.open("#{Rails.root}/tmp/#{xml_filename}", "r").read
        #      )
        #    end
        #
        #    # parse the result here and stored into olo detail
        #    parse_xml(batch_id)
        #  end
        #end
      else
        response.return!(request, result, &block)
      end
    end
  end

  def extract_zip_and_process(redirected_url, batch_id, chain_id = nil)
    response = RestClient.get redirected_url
    puts "getting file"
    File.open("#{Rails.root}/tmp/batch_#{batch_id}.zip", "w+") { |f| f << response.force_encoding("utf-8") }
    puts "writing file"
    Zip::File.open("#{Rails.root}/tmp/batch_#{batch_id}.zip") do |zip_file|
      # Handle entries one by one
      zip_file.each do |entry|
        # Extract to file/directory/symlink
        puts "Extracting #{entry.name}"

        xml_filename = entry.name.gsub('batch', 'batch_'+batch_id.to_s)
        if File.exist?("#{Rails.root}/tmp/#{xml_filename}")
          File.delete("#{Rails.root}/tmp/#{xml_filename}")
        end
        entry.extract("#{Rails.root}/tmp/#{xml_filename}")

        ol = OloOrder.where(:batch_id => batch_id).first

        if ol.blank?
          ol = OloOrder.new(:batch_id => batch_id, :xml_data => File.open("#{Rails.root}/tmp/#{xml_filename}", "r").read,
          :chain_id => chain_id)
          ol.save
        else
          ol = ol.update_attributes(
              :xml_data => File.open("#{Rails.root}/tmp/#{xml_filename}", "r").read
          )
        end

        # parse the result here and stored into olo detail
        parse_xml(batch_id)
      end
    end
  end

  def parse_xml(batch_id)
    ol= OloOrder.where(:batch_id => batch_id).first
    unless ol.blank?
      noko = Nokogiri.parse(ol.xml_data)

      orders = noko.xpath('//orders//order')


      if orders.size > 0
        # delete it first
        #if OloOrderDetail.where("olo_order_id = #{ol.id}").count
        #  OloOrderDetail.delete_all("olo_order_id = #{ol.id}")
        #end
        if OloOrderDetail.where("olo_order_id = #{ol.id}").count == 0
          orders.each do |order|
            res = parse_order(order)

            detail = OloOrderDetail.new(
                :olo_order_id => ol.id,
                :olo_ref => res.olo_ref,
                :store_name => res.store_name,
                :store_ref => res.store_ref,
                :email => res.email,
                :is_guest => res.is_guest,
                :subtotal => res.subtotal,
                :discount => res.discount,
                :tax => res.tax,
                :total => res.total,
                :time_placed => res.time_placed,
                :time_closed => res.time_closed,
                :time_wanted => res.time_wanted,
                :issue_time => res.issue_time
            )

            detail.save
          end
        end
      end
    end
  end

  def parse_order(order)
    result = OpenStruct.new
    result.olo_ref = order.at("oloRef").text rescue nil
    result.store_name = order.at("storeName").text rescue nil
    result.store_ref = order.at("storeRef").text rescue nil
    result.email = order.at("customer").at_xpath('emailAddress').text rescue nil
    result.is_guest = order.at("customer").at_xpath('isGuest').text == "false" ? false : true rescue nil
    result.subtotal = order.at("subtotal").text rescue nil
    result.discount = order.at("discount").text rescue nil
    result.tax = order.at("tax").text rescue nil
    result.total = order.at("total").text rescue nil
    result.time_placed = order.at("timePlaced").text rescue nil
    result.time_wanted = order.at("timeWanted").text rescue nil

    time_wanted_close = order.at("timeWanted").text.to_datetime.strftime('%I:%M %p') rescue nil
    time_wanted_close = order.at("timePlaced").text.to_datetime.strftime('%I:%M %p') rescue nil if time_wanted_close.blank?

    result.issue_time = time_wanted_close
    result.time_closed = order.at("timeClosed").text rescue nil
    return result
  end

  def post_orders(batch_id)
    # 2. Get Download redirect
    # POST /orderexports
    # parameters batch id

    url = @olo_url+post_orders_url
    RestClient.proxy = ENV["QUOTAGUARDSTATIC_URL"]
    response = Restclient.post(url, {'batchId' => batch_id})

    if response.code == 200
      render :ok
    else
      render :text => "Response Error post_orders"
    end
  end

  def post_orders_url
    "/orderexports"
  end

  def delete_order(batch_id)
    url = @olo_url+delete_order_url(batch_id)
    RestClient.proxy = ENV["QUOTAGUARDSTATIC_URL"]
    response = ResClient.delete(url)

    if response.code == 200
      render :ok
    else
      render :text => "Response Error delete_order"
    end
  end

  def delete_order_url(batch_id)
    #4. Mark the batch as processed
    #DELETE /orderexports/{batchId}

    "/orderexports/#{batch_id}"
  end
end