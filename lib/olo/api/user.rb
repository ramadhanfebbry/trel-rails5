require "net/http"
require "uri"

module Olo
  module Api
    class User
      attr_accessor :api_root, :api_key, :client_id, :secret_id, :iphone_api_key, :android_api_key, :provider

      def initialize(olo_connect_setting)
        @api_root = olo_connect_setting["api_root"]
        @api_key = olo_connect_setting["api_key"]
        @client_id = olo_connect_setting["client_id"]
        @client_secret = olo_connect_setting["client_secret"]
        @iphone_api_key = olo_connect_setting["iphone_api_key"]
        @android_api_key = olo_connect_setting["android_api_key"]
        @provider = olo_connect_setting["provider"]
      end

      def update_profile(user, params)
        p "OLO API UPDATE PROFILE #{params}"
        begin
          data = {}
          data["cardsuffix"] = ""
          data["authtoken"] = user.olo_profile.account_token
          data["emailaddress"] = params[:email].present? ? params[:email] : user.email
          data["firstname"] =  params[:first_name].present? ? params[:first_name] : user.first_name
          data["lastname"] =  params[:last_name].present? ? params[:last_name] : user.last_name

          apikey = if user.register_device_type.to_s.downcase == "android"
            p "ANDROID APIKEY"
            @android_api_key
          elsif user.register_device_type.to_s.downcase == "iphone"
            p "IPHONE APIKEY"
           @iphone_api_key
          else
            p "GLOBAL APIKEY"
            @api_key
          end
          uri = URI.parse("#{@api_root}/users/#{user.olo_profile.account_token}?key=#{apikey}")
          p uri
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Put.new(uri.request_uri)
          request["content-type"] = "application/json; charset=utf-8"
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            p "SUCCESS"
            p response.body
            return [true, "Your profile information has been updated."]
          else
            p "FAILED"
            p response.body
            response_json = JSON.parse(response.body) rescue ""
            return [false, (response_json)]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

      def update_profile_detail(user, params)
        p "OLO API UPDATE PROFILE #{params}"
        begin
          data = {}
          data["contactdetails"] = params[:phone_number]
          apikey = if user.register_device_type.to_s.downcase == "android"
                     p "ANDROID APIKEY"
                     @android_api_key
                   elsif user.register_device_type.to_s.downcase == "iphone"
                     p "IPHONE APIKEY"
                     @iphone_api_key
                   else
                     p "GLOBAL APIKEY"
                     @api_key
                   end
          uri = URI.parse("#{@api_root}/users/#{user.olo_profile.account_token}/contactdetails?key=#{apikey}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Put.new(uri.request_uri)
          request["content-type"] = "application/json; charset=utf-8"
          request.body = data.to_json
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            return [true, "Your profile information has been updated."]
          else
            response_json = JSON.parse(response.body) rescue ""
            return [false, (response_json)]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

      def get_olo_profile(user)
        p "OLO API GET PROFILE #{user.email}"
        begin
          apikey = if user.register_device_type.to_s.downcase == "android"
                     p "ANDROID APIKEY"
                     @android_api_key
                   elsif user.register_device_type.to_s.downcase == "iphone"
                     p "IPHONE APIKEY"
                     @iphone_api_key
                   else
                     p "GLOBAL APIKEY"
                     @api_key
                   end
          uri = URI.parse("#{@api_root}/users/#{user.olo_profile.account_token}?key=#{apikey}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Get.new(uri.request_uri)
          request["content-type"] = "application/json; charset=utf-8"
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            return [true, (JSON.parse(response.body) rescue "")]
          else
            response_json = JSON.parse(response.body) rescue ""
            return [false, (response_json)]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

      def get_olo_phone_number(user)
        p "OLO API GET PHONE NUMBER #{user.email}"
        begin
          apikey = if user.register_device_type.to_s.downcase == "android"
                     p "ANDROID APIKEY"
                     @android_api_key
                   elsif user.register_device_type.to_s.downcase == "iphone"
                     p "IPHONE APIKEY"
                     @iphone_api_key
                   else
                     p "GLOBAL APIKEY"
                     @api_key
                   end
          uri = URI.parse("#{@api_root}/users/#{user.olo_profile.account_token}/contactdetails?key=#{apikey}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = true
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Get.new(uri.request_uri)
          request["content-type"] = "application/json; charset=utf-8"
          response = http.request(request)
          if response.is_a?(Net::HTTPOK)
            return [true, (JSON.parse(response.body) rescue "")]
          else
            response_json = JSON.parse(response.body) rescue ""
            return [false, (response_json)]
          end
        rescue => e
          p "ERROR INI--- #{e.message}"
          return [false, e.message]
        end
      end

    end
  end
end