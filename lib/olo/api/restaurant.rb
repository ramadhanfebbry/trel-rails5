require "net/http"
require "uri"

module Olo
  module Api
    class Restaurant
      attr_accessor :api_root, :api_key

      def initialize(api_root, api_key)
        @api_root = api_root
        @api_key = api_key
      end

      def get_open_hours(external_partner_id, from, to)
        p "OLO API GET OPEN HOURS RESTAURANT"
        begin
          p uri = URI.parse("#{api_root}/restaurants/#{external_partner_id}/calendars?from=#{from}&to=#{to}&key=#{api_key}")
          http = Net::HTTP.new(uri.host, uri.port)
          http.use_ssl = api_root.include?("https")
          http.read_timeout = 20
          http.open_timeout = 20
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Get.new(uri.request_uri)

          request["Content-Type"] = "application/json; charset=utf-8"
          request["Authorization"] =  api_key
          response = http.request(request)
          p "RESPONSE OLO API GET OPEN HOURS RESTAURANT #{response.body}"
          response_json = JSON.parse(response.body)
          return response_json
        rescue => e
          p "FAILED: RESPONSE OLO API GET OPEN HOURS RESTAURANT #{e.message}"
          return nil
        end
      end

    end
  end
end