class Olo::OrderBatchJob < Struct.new(:redirect_url, :batch_id, :chain_id)
  def perform
      puts "OLO::ORDER BATCH JOB PROCESSING"
      x= Olo::Order.new
      x.extract_zip_and_process(redirect_url, batch_id, chain_id)

      result = OloOrder.where("batch_id = ?", batch_id)
      unless result.blank?
        result.each do |res|
          unless res.olo_order_details.blank?
          res.olo_order_details.not_processed.each do |x|
            begin
             # puts res.olo_order_details.count
              ## only on prelevant
             if ENV['DEFAULT_HOST_EMAIL'].include?('prelevant')
              #x.receipt_process
             else
               puts "On trelevant , do not process"
             end
            rescue
              next
              end
          end
            end
        end
      end
      puts "OLO::ORDER BATCH JOB Done"
  end
end