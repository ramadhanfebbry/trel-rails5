module Rpush
  module Apns
    class CertificateExpiredError < StandardError
      attr_reader :app, :time, :application, :chain

      def initialize(app, time)
        @app = app
        @application = Application.find(@app.name)
        @chain = @application.chain
        @time = time
      end

      def to_s
        message
      end

      def message
        "[#{@chain.id}]#{@chain.name}::#{@application.name} certificate expired at #{time}."
      end
    end
  end
end
