# encoding: utf-8
require 'mail'

class PushPlainNotificationJob < Struct.new(:emails, :body_content, :chain_ids,:plain_obj,:last_execution)

  Mail.defaults do
    delivery_method :smtp, {
        :address => Setting.smtp.host,
        :port => Setting.smtp.port,
        :user_name => Setting.smtp.username,
        :password => Setting.smtp.password,
        :authentication => 'plain',
        :enable_starttls_auto => true
    }
  end

  ## emails content is changed to user ids
  def perform
    puts "perform"
    puts "send email to #{emails}"
    locales = Chain.find(chain_ids).locales
    locales.each do |lc|
      user_emails = User.
          where("users.id in (?) and locales.key = ? and chain_id in (?) and users.active is true and users.push_email is true", emails.map(&:to_i), lc.key, chain_ids).
          joins(:locale)
      puts "sending"

      mail_list = user_emails.map(&:email)
      user_ids = user_emails.map(&:id)
      subject = body_content["subject_#{lc.key}"]
      content = HTMLEntities.new.encode(body_content["#{lc.key}"].gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />"), :named) rescue body_content["#{lc.key}"].gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")
      content = CGI.unescapeHTML(content)
      unless mail_list.blank?
        mail_list.each_with_index do |mail,index|
          begin
            unless user_emails.blank?
              from = Setting.email.default_from

              mail =  Mail.new do
                to      mail
                from    from
                subject subject
              end

              text_part = Mail::Part.new do
                body content
              end
              mail.text_part = text_part

              html_part = Mail::Part.new do
                content_type 'text/html'
                body content
              end
              mail.html_part = html_part
              mail.deliver!
              #UserMailer.push_chain_plain_notification(body_content["subject_#{lc.key}"], mail, body_content["#{lc.key}"]).deliver unless user_emails.blank?
              HistoryNotification.create(:user_id => user_ids[index], :plain_id => plain_obj.id, :plain_kind => 1, :status => 1)
            end
          rescue => e
            puts "PUSHPLAINNOTIFICATION JOB::ERROR => #{e.inspect}"
            HistoryNotification.create(:user_id => user_ids[index], :plain_id => plain_obj.id, :plain_kind => 1, :status => 2, :error_text => e.inspect)
            next
          end
        end
      end
      puts "#{lc.key} sending emails success"
    end
    puts "send email"
  end

#  def error(job, exception)
#    job = job.delayable_type.classify.constantize.find(job.delayable_id)
#    error_text = exception
#    UserMailer.reward_error(emails, error_text).deliver
#  end

end
