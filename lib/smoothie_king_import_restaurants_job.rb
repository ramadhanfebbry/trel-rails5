class SmoothieKingImportRestaurantsJob < Struct.new(:chain)
  require 'geokit'
  require 'geokit-rails'
  include Geokit::Geocoders
  require 'net/ftp'
  def perform
    p "***** RUN SmoothieKingImportRestaurantsJob *****"
    @rih = RestaurantImportHistory.create(chain_id: chain.id, status: "processing", log_type: "#{chain.try(:name)} JSON", file_name: REDIS.get("json_url#{chain.id}"))
    begin
      @res_success = []
      @res_fail = []
      @pos_location_created = []
      @pos = 0
      uri = URI.parse(REDIS.get("json_url#{chain.id}"))
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      if response.kind_of? Net::HTTPSuccess
        restaurants = JSON.parse(response.body)
        restaurants["data"].each_with_index do |rest,index|
          p "#{chain.try(:name)}: ImportRestaurantsJob processing #{rest["storeid"]} ---- #{index+1}"
          p "= " * 100
          processing_file(rest, chain)
          #if index == 10
          #  break
          #end
        end
        @rih.update_attributes(:status => "completed", :success_restaurants => @res_success, :failed_restaurants => @res_fail )
        address = REDIS.get("json_report_address#{chain.id}") rescue nil
        address.split(",").each do |receiver|
          p "Sending email report to location JSON #{receiver}"
          p "@pos_location_created #{@pos_location_created}"
          p ChainMailer.location_json_report(chain, @pos_location_created, receiver).deliver if receiver.present? && @pos_location_created.present?
        end
      else
        p "= " * 100
        p "#{chain.try(:name)}: ImportRestaurantsJob failed"
        @rih.update_attributes(status: "failed")
      end
    rescue
      p "= " * 100
      p "#{chain.try(:name)}: ImportRestaurantsJob failed"
      @rih.update_attributes(status: "failed")
    end
  end

  def processing_file(rest, chain)
    #@res = chain.restaurants.find_or_initialize_by_name("#{rest["storeid"]}") rescue nil
    new_restaurant = false
    @res = chain.restaurants.where(name: rest["storeid"]).first rescue nil
    if @res.blank?
      new_restaurant = true
      @res = Restaurant.new(chain_id: chain.id, name: rest["storeid"] )
    end

    if @res
      begin
        # App Display Text
        @res.app_display_text = "#{rest["name"].present? ? rest["name"] : '-' }"

        # DashboardDisplayName
        @res.dashboard_display_text = "#{rest["storeid"]}"

        # Address
        @res.address = "#{rest["address"]}"

        # City
        @res.city_import = "#{rest["city"]}".strip.downcase if rest["city"].present?

        # State
        @res.state_import = "#{rest["state"]}".strip.downcase if rest["state"].present?

        # Country
        @res.country_import = "#{rest["country"]}".strip.downcase if rest["country"].present?

        # ZipCode
        @res.ignored_zipcode = true
        @res.zipcode = "#{rest["zip"]}"

        # PhoneNumber
        @res.ignored_phone = true
        @res.phone_number = "#{rest["phone"]}"

        if rest["lat"].present? && rest["lng"].present?
          @res.latitude = "#{rest["lat"]}"
          @res.longitude = "#{rest["lng"]}"
        else
          loc = MultiGeocoder.geocode("#{rest["address"]}, #{rest["city"]}, #{rest["zip"]}")
          if loc.success
            @res.latitude = loc.lat
            @res.longitude = loc.lng
          else
            @res.latitude = rest["lat"].to_i
            @res.longitude = rest["lng"].to_i
          end
        end

        # Tags
        tags = []
        tags << rest["city"]
        tags << rest["custom_dma"]
        tags << rest["manager_name"]
        @res.tag_list = tags

        # SiteLive
        # open, closed, coming soon, temp closed
        @res.status = rest["storestatus"].downcase.eql?("open") || rest["storestatus"].downcase.eql?("coming soon")
        @res.skip_participate_to_rewards_and_offers = ["closed", "coming soon", "temp closed"].include?(rest["storestatus"].downcase)
        @res.skip_set_hybrid_reward_redemption = ["closed", "coming soon", "temp closed"].include?(rest["storestatus"].downcase)

        # Fishbowl ID
        @res.fishbowl_restaurant_identifier = "#{rest["storeid"]}"

        # Beacon serial number
        # @res.beacon_serial_number = row[27].to_s.chomp(".0") if row[27].present?

        # Beacon uuid
        # @res.beacon_uuid = row[28].to_s.chomp(".0") if row[28].present?

        # Location qrcode identifier
        # @res.location_qrcode_identifier = row[29].to_i_s.chomp(".0") if row[29].present?

        # City assignment to city_id
        @res.city_id = check_city_state_country(@res)
        p "@res.city_id #{@res.city_id}"
        @res.save!

        # restaurant status detail
        # open, closed, coming soon, temp closed
        @res_detail = RestaurantDetail.find_or_initialize_by_restaurant_id(@res.id)
        @res_detail.status = ["open", "closed", "coming soon", "temp closed", "testing"].each_with_index.select{|x,i| x.eql?("#{rest["storestatus"].downcase}")}.first.last rescue 1

        if [Restaurant::TYPES["OPEN"], Restaurant::TYPES["CLOSED"], Restaurant::TYPES["TESTING"]].include?(@res_detail.status)
          @res.update_column(:app_display_text, "#{@res.app_display_text.chomp("- Coming Soon")}") if @res.app_display_text.include?("- Coming Soon")
          @res.update_column(:app_display_text, "#{@res.app_display_text.chomp("- Temporary Closed")}") if @res.app_display_text.include?("- Temporary Closed")
        elsif @res_detail.status.eql?(Restaurant::TYPES["COMING_SOON"])
          @res.update_column(:app_display_text, "#{@res.app_display_text.chomp("- Temporary Closed")}") if @res.app_display_text.include?("- Temporary Closed")
          @res.update_column(:app_display_text, "#{@res.app_display_text}- Coming Soon") unless @res.app_display_text.include?("- Coming Soon")
        elsif @res_detail.status.eql?(Restaurant::TYPES["TEMP_CLOSED"])
          @res.update_column(:app_display_text, "#{@res.app_display_text.chomp("- Coming Soon")}") if @res.app_display_text.include?("- Coming Soon")
          @res.update_column(:app_display_text, "#{@res.app_display_text}- Temporary Closed") unless @res.app_display_text.include?("- Temporary Closed")
        end

        @res_detail.save!

        if rest["operatingHours"].present?
          Date::DAYNAMES.each_with_index do |dayname,i|
            op = rest["operatingHours"].select{|x| x["day"].downcase.eql?(dayname.downcase)}.first
            if op.present?
              @res_hours = @res.restaurant_hours.find_or_initialize_by_day_of_week(i)
              @res_hours.open_at = "#{op["from"]}"
              @res_hours.close_at = "#{op["until"]}"
              @res_hours.save!
            end
          end
        end

        #create pos location
        if new_restaurant
          p "------------- create pos location -------------"
          pos_location = PosLocation.new(:chain => chain, restaurant_id: @res.try(:id))
          pos_location.set_apikey
          if pos_location.save
            @pos_location_created << [pos_location.restaurant.try(:name), pos_location.apikey]
          end
        end

        @res_success << "#{rest["storeid"]}"
        p "= " * 100
        p "#{chain.try(:name)}: ImportRestaurantsJob EXECUTE #{rest["storeid"]} success"

      rescue => e
        p "= " * 100
        p "#{chain.try(:name)}: ImportRestaurantsJob EXECUTE #{rest["storeid"]} fail"
        p e
        fail_row = []
        fail_row << "#{rest["storeid"]}"
        fail_row << e.to_s
        @res_fail << fail_row
      end
    end
  end

  def check_city_state_country(res)
    p res.name
    p "res.state_import #{res.state_import} --- res.city_import #{res.city_import} --- res.country_import #{res.country_import}"
    country_exist = Country.where(:ISO => "#{res.country_import.upcase}").first rescue nil
    country_exist = Country.where("lower(abbreviation) = '#{res.country_import.downcase}'").first rescue nil if country_exist.blank?
    if country_exist.blank?
      country_exist = Country.where("lower(name) like '#{res.country_import.gsub("'", "''").downcase}'").first rescue nil
    end
    if country_exist.blank?
      country_exist = Country.create(
          :name => res.country_import.titleize,
          :abbreviation => (CS.countries.key(res.country_import.titleize) rescue nil)
      )

    end

    state_exist = nil
    if res.state_import.size <= 2
      state_exist = country_exist.regions.where("lower(abbreviation) = '#{res.state_import.downcase}'").first rescue nil

      if state_exist.blank?
        state_exist = Region.create(
            :name => ( CS.states(country_exist.abbreviation)[res.state_import.upcase.to_sym] rescue res.state_import.titleize ),
            :abbreviation => res.state_import.upcase,
            :country_id => country_exist.id
        )
      end
    else
      state_exist = country_exist.regions.where("lower(name) like '#{res.state_import.gsub("'", "''").downcase}' ").first rescue nil

      if state_exist.blank?
        state_exist = Region.create(
            :name => res.state_import.titleize,
            :abbreviation => (CS.states(country_exist.abbreviation).key(res.state_import.titleize) rescue nil),
            :country_id => country_exist.id
        )
      end
    end

    city_exist = state_exist.cities.where("lower(name) like '#{res.city_import.gsub("'", "''").downcase}'").first rescue nil
    if city_exist.blank?
      city_exist = City.create(
          :name => res.city_import.titleize,
          :region_id => state_exist.id
      )
    end

    p "city_exist:  #{city_exist} -- #{city_exist.id}"
    return city_exist.id
  end
end