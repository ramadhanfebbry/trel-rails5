class OwnerJobNotificationDeviceJob < Struct.new(:device, :tokens, :content, :owner_job)
  def perform
    puts "perform OwnerJobNotificationDeviceJob \n" * 100
    if device.to_s == "android"
      owner_job.push_for_android(tokens, content)
      #xxxxx
    end

    if device.to_s == "iphone"
      owner_job.push_for_iphone(tokens, content)
      #xxxxxx
    end
    puts "push OwnerJobNotificationDeviceJob success \n" * 10
  end
end