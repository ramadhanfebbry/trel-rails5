module Exceptions
  class UserPointsError < StandardError; end
  class InsufficientUserPointsError < UserPointsError; end


end