class QueueEmailNotification < Struct.new(:env_process, :limit_queue, :emails)

  def perform

    ## LIMIT FOR EACH WORKER ON EACH ENV
    # nprel :
    # adjust_credit_worker : 500
    # ncr_aloha_production : 500


    # prelevant :
    # receipt_updated_job : 350
    # prel_job : 750
    if env_process == "prelevant"
      limit_queue_prel_job = (REDIS.get('limit_queue_prel_job') || 750).to_i

      count = DelayedJob.where("queue = 'prel_job' and locked_by is null and failed_at is null").select('count(id)').first.count.to_i

      if count >= limit_queue_prel_job
        ChainMailer.queue_notification(env_process,"prel_job",count,emails).deliver!
      end


      limit_queue_receipt_updated_job = (REDIS.get('limit_queue_receipt_updated_job') || 350).to_i
      count = DelayedJob.where("queue = 'receipt_updated_job' and locked_by is null and failed_at is null").select('count(id)').first.count.to_i

      if count >= limit_queue_receipt_updated_job
        ChainMailer.queue_notification(env_process,"receipt_updated_job",count,emails).deliver!
      end
    end

    ## nprel
    if env_process == "nprel"
      count = DelayedJob.where("queue = 'ncr_aloha_production' and locked_by is null and failed_at is null").select('count(id)').first.count.to_i

      limit_queue_ncr_aloha_production = (REDIS.get('limit_queue_ncr_aloha_production') || 500).to_i

      if count >= limit_queue_ncr_aloha_production
        ChainMailer.queue_notification(env_process,"nprel_job",count,emails).deliver!
      end

      limit_queue_aloha_adjust_credit_production_job =  (REDIS.get('limit_queue_aloha_adjust_credit_production_job') || 500).to_i
      count = DelayedJob.where("queue = 'aloha_adjust_credit_production_job' and locked_by is null and failed_at is null").select('count(id)').first.count.to_i
      if count >= limit_queue_aloha_adjust_credit_production_job
        ChainMailer.queue_notification(env_process,"nprel_job",count,emails).deliver!
      end
    end
  end
end