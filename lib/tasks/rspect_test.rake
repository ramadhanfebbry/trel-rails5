namespace :rspec do
  desc 'running the rspec'
  task :run => :environment do    
    system("rake db:migrate RAILS_ENV=test")
    system("rake:db:fixtures:load RAILS_ENV=test")
    system("rspec spec/")
  end
end