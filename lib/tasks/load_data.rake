namespace :db do
  desc 'load pre-existing authorization for admin'
  task :load_admin_authorization => :environment do
    if Role.all.blank?
      admin = Role.create(:title => "admin")
      reviewer = Role.create(:title => "reviewer")
      Admin.all.each do |adm|
        adm.role = admin
        adm.save
      end
    end
  end

  desc 'create default user locale'
  task :user_locale => :environment do
    locale = Locale.where(:key => "en").first
    User.all.each do |user|
      user.update_attribute(:locale_id, locale.id)
    end
  end

  desc 'update email template with the latest updated'
  task :update_email_template => :environment do
    Chain.all.each do |chain|
      chain.chain_locales.each do |chain_locale|
        chain_locale.save_email_template_to_redis
      end
    end
  end

  desc "default permission for guest relations setting"
  task :guest_relation_setting => :environment do
    GuestRelation.setup_guest_relation_features
  end


  desc 'create receipt reject reason'
  task :create_reject_reasons => :environment do
    locales = Locale.all
    reject_reasons = YAML.load_file("#{Rails.root}/lib/tasks/reject_reasons.yml")
    reject_reasons.each do |key, value|
      #push to redis based on locales
      locales.each do |locale|
        REDIS.set "#{locale.key}.receipt_reject_reason.#{value['reason_key']}", value[locale.name.downcase]
        RejectReason.create(:description => value['description'], :reason_key => value['reason_key']) if RejectReason.where(:reason_key => value['reason_key']).first.blank?
      end
    end
  end

  desc 'clear Rails cache'
  task :clear_cache => :environment do
    Rails.cache.clear
  end

  desc 'create tester admin user'
  task :admin_tester => :environment do
    admin = Admin.new
    admin.email = 'tester@dg.com'
    admin.password = 'tester'
    admin.password_confirmation = 'tester' 
    admin.role = ADMIN_ROLES[:TESTER]
    admin.save
  end

  desc 'fix data role owner'
  task :fix_owner => :environment do
    Owner.all.each do |owner|
      puts "fix data owner #{owner.email}"
      if owner.parent_id.nil?
        owner.update_attribute(:role_id, 1)
      else
        owner.chains_owners.destroy_all
        owner.update_attribute(:role_id, 2)
      end
    end
  end

  desc 'fix reward transction data'
  task :fix_reward_transaction_data => :environment do
    RewardTransaction.all.each do |rt|
      rt.fix_restaurant_and_reward_by_lat_lng(rt.latitude, rt.longitude)
    end
  end


  #task params 102-123-34-55 => 102-123-34-55 is list of reward_ids separated by '-'
  desc 'fix reward transaction data with params'
  task :fix_reward_transaction_data_with_params, [:reward_ids] => :environment do |t, args|
    array_reward_ids = args[:reward_ids].split("-")
    array_reward_ids.each do |reward_id|
      RewardTransaction.where(:reward_id => reward_id).each do |rt|
        reward = Reward.find(reward_id)
        restaurant_id = Restaurant.get_id_by_lat_lng(reward, rt.latitude, rt.longitude)
        rt.update_column(:restaurant_id, restaurant_id)
      end
    end
    puts "executed---"
  end

  desc "move restaurant reward to each reward_id and restaurant_id"
  task :move_restaurant_reward => :environment do
    RewardTransaction.all.each do |rt|
      p " rt id #{rt.id}"
      restaurant_reward = rt.restaurant_reward
      if restaurant_reward
        rt.restaurant_id = restaurant_reward.restaurant_id
        rt.reward_id = restaurant_reward.reward_id
        rt.save
        p rt.errors
      end
    end
  end

  desc "move user_id and resturant_id to restaurant_user model"
  task :move_to_restaurant_user => :environment do
    RestaurantUser.delete_all
    ReceiptTransaction.joins(:receipt).select("receipts.user_id, receipt_transactions.restaurant_id,receipt_transactions.created_at").each do |receipt_transaction|
      unless receipt_transaction.restaurant_id.blank?
      res = RestaurantUser.create(:user_id => receipt_transaction.user_id,
                            :restaurant_id => receipt_transaction.restaurant_id
                           )
      res.update_column(:created_at, receipt_transaction.created_at) if res.errors.blank?
      end
    end
  end

  desc "populate joined date on restaurant users table"
  task :adding_joined_date_to_restaurant_user => :environment do
      RestaurantUser.populate_joined_data_column
  end

  desc "populate activity summary and reward summaries tables"
  task :activity_and_reward_summary => :environment do
    ActivitySummary.execute_first
    RewardSummary.execute_first
  end


  desc "day between visit preloaded data tasks"
  task :day_between_visit  => :environment do
    DayVisit.generate_all_data
    AvgVisit.calculate_avg_from_day_visit
  end

  desc "move point to point history"
  task :move_point_to_history => :environment do
    User.all.each do |user|
      PointHistory.create(:user_id => user.id, :description => "Total Point Until #{Time.now.in_time_zone.strftime("%d %B %Y")}", :point => user.points)
    end
  end

  desc 'fix data steppingstone step with polymorph relation'
  task :fix_steps => :environment do
    SteppingstoneStep.all.each do |step|
      step.update_attributes(:stepable_id => step.steppingstone_id, :stepable_type => "Steppingstone")
    end
  end

  desc 'load pre-existing data tag stepping stone'
  task :load_tags => :environment do
    Tag.create(:name => "Light User", :category_type => "Frequency", :min => 0, :max => 1)
    Tag.create(:name => "Medium User", :category_type => "Frequency", :min => 1, :max => 5)
    Tag.create(:name => "Heavy User", :category_type => "Frequency", :min => 5, :max => 1000)
  end

  desc 'insert default value for last visit in restaurant users table'
  task :load_last_active => :environment do
    OwnerJob::RestaurantUserJob.insert_default_value
  end

  desc 'only calculating dropdown questions for surveys average'
  task :load_only_dropdown_question_avg => :environment do
    RestaurantSurveyAvg.all.each do |rs_avg|
      begin
      su = SurveysUser.first
      total = su.calculate_avg(Survey.find(rs_avg.survey_id),rs_avg.restaurant_id)
      rs_avg.update_attributes(
          :averages => total[:all], :q1 => total[:q1], :q2 => total[:q2], :q3 => total[:q3],
          :q4 => total[:q4],
          :q5 => total[:q5]
      )
      rescue => e
        puts "Load Data.rake :: load only dropdown question avg #{e.inspect}"
        next
      end
    end
  end

  desc "set device id status"
  task :set_device_id_status_user => :environment do
    results = {}
    chains = Chain.all
    chains.each do |chain|
      results[chain.id.to_s] = {}
      chain.users.unscoped.select("id, email, chain_id, created_at").order("created_at ASC").each do |user|
        p "user id #{user.id} -- email = #{user.email}"
        user_device_log = UserDeviceLog.where(:user_id => user.id, :access_type => "Signup").first
        if user_device_log && (!user_device_log.keychain_value.blank? || !user_device_log.device_id.blank?)
          if user_device_log.device_type.downcase.eql?("iphone")
            signed_up_users_count_based_on_keychain = UserDeviceLog.where(:chain_id => user.chain_id, :keychain_value => user_device_log.keychain_value, :access_type => "Signup").order("created_at ASC")
          elsif user_device_log.device_type.downcase.eql?("android")
            signed_up_users_count_based_on_keychain = UserDeviceLog.where(:chain_id => user.chain_id, :device_id => user_device_log.device_id, :access_type => "Signup").order("created_at ASC")
          end
          if signed_up_users_count_based_on_keychain.first.user_id.eql?(user.id)
            user.update_column(:signup_device_status, 1)
          else
            user.update_column(:signup_device_status, 2)
          end
        else
          user.update_column(:signup_device_status, 3)
        end
      end
    end
  end

  desc "set selected device info"
  task :set_selected_device_info => :environment do
    chains = Chain.all
    chains.each do |chain|
      chain.users.unscoped.select("id, email, chain_id, created_at").order("created_at ASC").each do |user|
        user_device_logs = user.top_5_logs
        unless user_device_logs.blank?
          udl = user_device_logs.first
          if udl.device_type.downcase.eql?("iphone")
            keychain_selected = udl.keychain_value
          elsif user_device_log.device_type.downcase.eql?("android")
            keychain_selected = udl.device_id
          end
          user.update_column(:selected_device_info, keychain_selected)
        end
      end
    end
  end

  desc "copy email template content from redis to db"
  task :add_default_html_template_email => :environment do
    Chain.all.each do |chain|
      chain.chain_locales.each do |chain_locale|
        chain_id = chain_locale.chain.id
        chain_name = chain_locale.chain.name
        locale_key = chain_locale.locale.key

        (ChainLocale::EMAILS + ChainLocale::NCR_EMAIL).each do |e|
          mail_subject = REDIS.hget "chain_#{chain_id}", "#{locale_key}_#{e}_email_subject" || ""
          mail_body = REDIS.hget "chain_#{chain_id}", "#{locale_key}_#{e}_email_body" || ""
          email_template = EmailTemplate.new
          email_template.template_name = key
          email_template.chain_id = chain.id
          email_template.locale_id = self.locale.id
          email_template.mail_subject = mail_subject
          email_template.mail_content_text = mail_body
          html_body = mail_body
          html_body.gsub!(/\r\n?/, "<br />")
          html_body.gsub!(/\n/, "<br />")
          email_template.mail_content_html = html_body
          email_template.send_as = "multipart/alternative"
          email_template.save
        end
      end
    end
  end

  desc "set default email template to database for each chain"
  task :set_default_email_template_db => :environment do
    Chain.all.each do |chain|
      chain.chain_locales.each do |chain_locale|
        chain_locale.save_email_template_to_db
      end
    end
  end

end