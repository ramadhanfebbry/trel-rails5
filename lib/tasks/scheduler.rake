desc "This task is called by the Heroku scheduler add-on"
task :clear_feedback_rapns => :environment do
  puts "clearing feedback and removing bad device token from user"
  Rpush::Apns::Feedback.clear_feedback
  puts "done."
end

task :activity_reward_scheduler  => :environment do # once a day on 5 am EST
  puts "ACTIVITY REWARD SCHEDULER scheduler running ..."
  ActivitySummary.scheduler_daily
  RewardSummary.scheduler_daily
  puts "ACTIVITY REWARD SCHEDULER ..."
end

task :queue_email_notification => :environment do
  puts "queue_email_notification scheduler running ..."
  timer =  (REDIS.get('queue_notif_time') || 10).to_i
  limit_prel = 0
  emails =  (REDIS.get('sending_email_queue') || ["mbainurb@gmail.com"])
  if timer == 10
    process = QueueEmailNotification.new("prelevant",limit_prel,emails)
    process.perform

    process = QueueEmailNotification.new("nprel",limit_prel,emails)
    process.perform
    puts "queue_email_notification scheduler Finished ..."
  else
    puts "queue_email_notification using dj timer = #{timer}"
    # update it if any other if exist
    if DelayedJob.where("delayable_type = 'QueueEmailNotification'").count > 0
      DelayedJob.where("delayable_type = 'QueueEmailNotification'").update_all(:run_at => Time.zone.now + timer.minutes)
    else
      Delayed::Job.enqueue(QueueEmailNotification.new("prelevant",limit_prel,emails), :run_at => Time.zone.now + timer.minutes, :delayable_type => "QueueEmailNotification" )
      Delayed::Job.enqueue(QueueEmailNotification.new("nprel",limit_prel,emails), :run_at => Time.zone.now + timer.minutes, :delayable_type => "QueueEmailNotification")
    end

  end
end

task :update_restaurant_survey_member => :environment do
  puts "Restaurant count Survey, And Member running "
  #if Time.zone.now.in_time_zone('EST').hour > 23 and Time.zone.now.in_time_zone('EST').hour < 9
      a = Time.now
      puts "REDIS FOR DASHBOARD starting @#{a}"
      RestaurantUser.update_survey_and_member_for_each
      RestaurantUser.update_member_chart_area
      puts "REDIS FOR DASHBOARD END @#{Time.now - a}"
  #end
  #puts "Restaurant count Survey, And Member Finished "
end

task :birthday_reward_annualy => :environment do
  job_type = ENV['BIRTHDAY_REWAR_VAR'] || 1
  Chain.active.select('id').where(:connect_to_aloha_program => false).each do |ch_id|
    a = BirthdayAnnualyReward.new(ch_id,job_type)
    a.execute_annualy_reward
  end
end

task :anniversary_reward_annualy => :environment do
  job_type = ENV['ANNIVERSARY_REWARD_VAR'] || 1
  Chain.active.select('id').where(:connect_to_aloha_program => false).each do |ch_id|
    a = AnniversaryAnnualyReward.new(ch_id,job_type)
    a.execute_anniversary_annualy_reward
  end
end

task :olo_order_task => :environment do
  puts "OLO Scheduler Started"
  OloOrderDetail.olo_order_gathering

  #OloOrderDeail.daily_olo_order
  puts "OLO Scheduler Finished"
end


task :owner_jobs_scheduler => :environment do
  puts "Owner scheduler running ..."
  OwnerScheduler.running_owner_scheduler
  NoScanJobScheduler.running_owner_scheduler
  puts "Owner scheduler completed ..."
end

task :daily_users_chart_populate => :environment do
  a = Time.now
  puts "starting @#{a}"
  puts "Scheduler::daily_users_chart_populate => START"
  Chain.all.each do |chain|
    #DashboardQuery::ActiveMemberV2.populate_daily_data(chain)
    chain.restaurants.each do |res|
      DashboardQuery::ActiveMemberV2.populate_daily_data_v2(chain, res.id)
    end
  end
  puts "Scheduler::daily_users_chart_populate => END"
  puts "END #{Time.now - a}"
end

task :owner_jobs_expired => :environment do
  puts "Owner scheduler set expired running ..."
  OwnerScheduler.set_expired
  puts "Owner scheduler set expired completed ..."
end

task :clear_unreg_regids => :environment do
  puts "clearing unreg registration id"
  GcmUnsentHistory.clear_unsent_histories
  puts 'done'
end

task :create_ocr_transaction_from_sqs => :environment do
  puts " ----- get message from sqs -----"
  Receipt.create_ocr_transaction_from_sqs
  puts ' ------- done -------'
end

task :fixing_stuck_pending_ocr => :environment do
  puts "----- fixing stuck pending ocr -----"
  Receipt.process_pending_ocr_receipt_from_xml_data_archived_db
  puts "------ done fixing stuck pending ocr -----"
end

task :remove_no_pending_ocr_archived_xml_receipt => :environment do
  puts "remove no pending ocr archived xml receipt"
  acrchived_xml_count =  ArchivedXmlReceipt.count
  loop = (acrchived_xml_count.to_f/100.to_f).ceil
  1.upto(loop) do |i|
    ArchivedXmlReceipt.all.paginate(page: i, :per_page => 100).each do |archived_xml_receipt|
      receipt = Receipt.find(archived_xml_receipt.receipt_id)
      date_range = Time.current - 7.days
      if !receipt.status.eql?( Receipt::STATUS[:PENDING_OCR]) && receipt.created_at <= date_range
        archived_xml_receipt.destroy
      end
    end
  end
  puts " endd----- removed -----"
end

task :create_available_week_and_user_generate_goals => :environment do
  puts " ----- create_available_week_and_user_generate_goals -----"
  User.create_available_week_and_user_generate_goals
  puts '-------------- done =================================='
end

task :check_receipt_no_transaction => :environment do
  puts "------------checking receipt with no transaction ---------------"
  Receipt.check_no_transaction_receipt
  puts "------------------checked -----------------------"
end

task :owner_reports => :environment do
  puts "------------------- owner report generator ---------------- "
   ReportSubscription.subscription_job
  puts "------------------- End owner report generator ---------------- "
end

task :chain_no_scan_alert => :environment do
  puts "Chain No Scan ALERT Starting ---------- on #{Time.zone.now}"
  ChainSettingAlert.run_on_execution_time
  puts "Chain No Scan ALERT End ---------- on #{Time.zone.now}"
end

task :set_user_tag_by_frequency => :environment do
  puts "starting set tags-----------------"
  date_now = Date.current
  eight_week_prior = date_now - 8.weeks
  set_tag_time = Time.current
  update_chain_user_tags = []
  Chain.where(:activate_milestone_receipt => false).each do |chain|
    if chain.last_set_tag_at.blank? || (((set_tag_time - chain.last_set_tag_at)/1.hour)/24) >  56
      all_users_count = chain.users.count
      loop = (all_users_count.to_f/100.to_f).ceil
      1.upto(loop) do |i|
        chain.users.paginate(page: i, :per_page => 100).each do |user|
          puts "----- set tag users chain #{chain.name} ---------"
          q = "select count(distinct(c.id)) from receipts c left join
receipt_transactions t1 on t1.receipt_id = c.id left join
                         receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
                         where  c.user_id = #{user.id} and c.status = 3 and date(
  case when exists(select t1.issue_date)
  then t1.issue_date
    else c.created_at
  end
  ) between date('#{eight_week_prior.strftime('%Y-%m-%d')}') and date('#{date_now.strftime('%Y-%m-%d')}')"
          res = Receipt.find_by_sql(q)
          user_receipt_approved = res.first.count.to_i

          puts "set tags #{user.email} -- receipt approved count = #{user_receipt_approved}"
          selected_tag = Tag.where("min <= ? AND max > ?", user_receipt_approved, user_receipt_approved).first
          p puts "user selected tag #{selected_tag.name}"
          if selected_tag
            user_tag = user.user_tag
            if user_tag
              user_tag.update_column(:tag_id, selected_tag.id)
            else
              UserTag.create(:user_id => user.id, :tag_id => selected_tag.id)
            end
          end
          puts "---****_--------------"
          puts "----- set tag users chain #{chain.name} ---------"
        end
      end
      chain.update_column(:last_set_tag_at, set_tag_time)
      p chain.last_set_tag_at
      update_chain_user_tags << chain
    end
  end
  unless update_chain_user_tags.blank?
    RewardPosCodeMailer.send_notification_when_tag_updated(update_chain_user_tags).deliver
  end
  puts "------------ end set tags ---------------"
end

task :set_notify_status_pos_codes => :environment do
  Chain.where("use_generated_code is FALSE").each do |chain|
    chain.rewards.each do |reward|
      all_code = reward.reward_pos_codes.count
      unused_pos_code_count = reward.reward_pos_codes.where("user_id IS NULL").count
      if all_code > 0 && unused_pos_code_count < 2000
        puts "reward #{reward.name} should send notify"
        reward.update_column(:notify_sys_admin_for_pos_codes, true)
      else
        reward.update_column(:notify_sys_admin_for_pos_codes, false)
      end
    end
  end
end

task :send_pos_codes_notification => :environment do
  puts "---- starting check pos codes"
  Chain.where("use_generated_code is FALSE").each do |chain|
    chain.rewards.each do |reward|
      if reward.notify_sys_admin_for_pos_codes
        if Time.current.to_i - reward.last_notification_sent_at.to_i > 2600000
          puts "send notification for reward #{reward.name}"
          RewardPosCodeMailer.send_notification_lt_two_thousand(reward).deliver
          reward.update_attributes(:notify_sys_admin_for_pos_codes =>  false, :last_notification_sent_at => Time.current)
        end
      end
    end
  end
end

task :check_ocr_down => :environment do
  puts " ---checking ocr -----"
  Receipt.check_sqs_output_queue_messages
end

task :check_ocr_service_down => :environment do
  puts "---- checking ocr_service down"
  Receipt.check_sqs_input_queue_messages
  puts "end"
end

task :repeated_promotions_push_incentive => :environment do
  puts "------------ start repeated promotion push incentive ---------"
  UserRepeatedPromotion.where(:expired => false).each do |urp|
    promotion = urp.promotion
    unless promotion.is_repeated
      urp.update_column(:expired, true)
      next
    end
    reward = promotion.promotable
    user = urp.user
    today = Time.current
    date_comparator = urp.last_pushed_at.blank? ? urp.created_at : urp.last_pushed_at
    unless today.to_date.eql?(date_comparator.to_date)
      if promotion.effective_date <= today && promotion.expiry_date >= today
        unless reward.expired
          applied_at = urp.applied_at
          duration = (today.to_date - applied_at).to_i
          if (duration%promotion.duration) == 0
            RewardWallet.create_with_delay(:reward_id => reward.id, :user_id => user.id)
            urp.update_column(:last_pushed_at, today)
          end
        else
          urp.update_column(:expired, true)
        end
      else
        urp.update_column(:expired, true)
      end
    end
  end
end

task :import_users_to_email_marketing => :environment do
  puts "importing ------ email marketing"
  EmailMarketing.import_from_users
end

task :prepopulate_chart => :environment do
  Chain.all.each do |c|
    DbSummary.prepulate_data(c.id)
  end
end

task :added_email_as_subscribers => :environment do
  puts 'adding as subscriber mailchimp'
  EmailMarketing.add_as_subscribers
end

task :move_delayed_job_archive => :environment do
  puts "Delayed Job Moving To archives"
  DelayedJobArchive.move_archived
end

task :fishbowl_daily_sync => :environment do
  puts "execute fishbowl daily sync"
  t_zone = Time.current
  if REDIS.get("fishbowl_daily_sync_run_at").blank? || (!REDIS.get("fishbowl_daily_sync_run_at").blank? && Time.parse(REDIS.get("fishbowl_daily_sync_run_at")) < t_zone.midnight)
    REDIS.set("fishbowl_daily_sync_run_at", t_zone)
    Chain.all.each do |chain|
      chain.add_sync_job("Daily Sync", t_zone.midnight + 195.minutes)
    end
  end
end

# task :emma_daily_sync => :environment do
#   puts "execute emma daily sync"
#   t_zone = Time.current
#   if REDIS.get("emma_daily_sync_run_at").blank? || (!REDIS.get("emma_daily_sync_run_at").blank? && Time.parse(REDIS.get("emma_daily_sync_run_at")) < t_zone.midnight)
#     REDIS.set("emma_daily_sync_run_at", t_zone)
#     Chain.all.each do |chain|
#       chain.emma_add_sync_job("Daily Sync", t_zone.midnight + 195.minutes)
#     end
#   end
# end

namespace :heroku do
  require "heroku_backup_task"
  require "heroku_cloud_backup"
  desc "PostgreSQL database backups from Heroku to Amazon S3"
  task :backup => :environment do
    HerokuBackupTask.execute
    HerokuCloudBackup.execute
  end

end

task :send_reward_exp_notification => :environment do
  t_zone = Time.current
  if (REDIS.get("send_reward_exp_notification_run_at").nil? || Time.zone.parse(REDIS.get("send_reward_exp_notification_run_at")) < t_zone.midnight)
    REDIS.set("send_reward_exp_notification_run_at", t_zone)
    Chain.all.each do |chain|
      unless chain.reward_exp_notifications.blank?
        date_condition = (t_zone + chain.ren_number_of_days.days)
        all_rw_count = RewardWallet.includes(:user, :reward).where("reward_wallets.status = ? AND date(TIMEZONE('UTC', reward_wallets.expiry_date) AT TIME ZONE '#{t_zone.strftime('%Z')}') = ? AND rewards.points <= users.points AND rewards.reward_type != 3 AND users.chain_id = ?", 1, date_condition.strftime("%Y-%m-%d"), chain.id).count
        loop = (all_rw_count.to_f/100.to_f).ceil
        1.upto(loop) do |i|
          Delayed::Job.enqueue(RewardExpNotificationJob.new(chain, date_condition, i), :run_at => t_zone.midnight + 11.hours)
        end
      end
    end
  end
end

task :time_gap => :environment do
#  q = "select distinct(c.id) as receipt_id, u.id as user_id, u.email as user_email, ch.id as chain_id, ch.name as chain_name, t2.restaurant_id as restaurant_id, t2.issue_date as issue_date, t2.time_stamp as time_stamp, c.created_at as submission_date, t2.status as status from receipts c
#RIGHT JOIN users u on u.id = c.user_id
#RIGHT JOIN chains ch ON ch.id = c.chain_id
#LEFT JOIN receipt_transactions t1 on t1.receipt_id = c.id
#left join receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id where  c.status = 3 ORDER BY c.created_at DESC LIMIT(10000)"

  loop = (10000.to_f / Setting.pagination.per_page.to_f).ceil
  receipt_csv = CSV.open("time_gap_receipt_3.csv", "w+") do |csv|
    # header row
    csv << [
        "Receipt id",
        "User Id",
        "User Email",
        "Chain Name",
        "Time GAP in Minutes"
    ]
    317.upto(loop) do |page|
      puts page
      res = Receipt.select("id, user_id, status, chain_id, created_at").where("status = ? AND id < ?", 3, 649313).order("created_at DESC").paginate(page: page, per_page: Setting.pagination.per_page)
      res.each do |receipt|
        p "processing receipt #{receipt.id}"
        last_transaction = receipt.last_transaction
        if last_transaction
          issue_date = last_transaction.issue_date.utc.strftime("%Y-%m-%d")
          time_stamp = last_transaction.time_stamp
          issue_date_time = "#{issue_date} #{time_stamp}"
          restaurant = last_transaction.restaurant
          json_response = Net::HTTP.get(URI.parse("http://api.geonames.org/timezoneJSON?lat=#{restaurant.latitude}&lng=#{restaurant.longitude}&username=syafik"))
          time_zone = ActiveSupport::JSON.decode(json_response)
          Time.zone = time_zone["timezoneId"]
          restaurant_receipt_time = Time.zone.parse issue_date_time
          submission_date = receipt.created_at.in_time_zone
          seconds_diff = (submission_date - restaurant_receipt_time).to_i.abs

          hours = seconds_diff / 3600
          seconds_diff -= hours * 3600

          minutes = seconds_diff / 60
          seconds_diff -= minutes * 60

          seconds = seconds_diff

          diff_time_in_min = (hours * 60) + minutes + (seconds / 60)

          csv << [
              receipt.id,
              receipt.user_id,
              (receipt.user.email rescue ""),
              (receipt.chain.name rescue ""),
              diff_time_in_min
          ]
          p "end proccessing receipt #{receipt.id} time gap is #{diff_time_in_min}"
        end
      end
    end
  end
end

task :remove_unused_reward_codes => :environment do
  puts " -----start remove_unused_reward_codes -----"
  Barcode.remove_unused_reward_codes
  puts ' ------- end remove_unused_reward_codes -------'
end


task :remove_unused_user_codes => :environment do
  puts " -----start remove_unused_user_codes -----"
  Barcode.remove_unused_user_codes
  puts ' ------- end remove_unused_user_codes -------'
end

task :remove_unused_gift_codes => :environment do
  puts " -----start remove_unused_gift_codes -----"
  Barcode.remove_unused_gift_codes
  puts ' ------- end remove_unused_gift_codes -------'
end

task :import_netpos_checks => :environment do
  puts " -----start import_netpos_checks -----"
  Chain.net_pos_import_all_check
  puts ' ------- end import_netpos_checks -------'
end

task :count_user_codes => :environment do
  Chain.all.each do |chain|
    #USERCODE COUNT
    user_code_setting = chain.user_code_setting
    user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
    user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
    if user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      regex = '^\d{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
      code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["USERCODE"]).count
    elsif user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
      code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["USERCODE"]).count
    end
    chain.update_column(:user_code_count, code_count)

    #GIFTCODE COUNT
    gift_code_setting = chain.gift_code_setting
    gift_code_setting = JSON.parse(gift_code_setting.to_json) rescue nil
    gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
    if gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      regex = '^\d{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
      code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).count
    elsif gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
      code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).count
    end
    chain.update_column(:gift_code_count, code_count)
  end
end

task :update_users_fishbowl_from_ftp_files => :environment do
  puts "update_users_fishbowl_from_ftp_files"
  Chain.all.each do |chain|
    User.delay.update_users_fishbowl_from_ftp(chain) if chain.fishbowl_setting
  end
  puts "end update_users_fishbowl_from_ftp_files"
end

task :revel_sync_from_ftp_files => :environment do
  puts "revel_sync_from_ftp_files"
  Chain.all.each do |chain|
    Chain.delay.revel_to_relevant_sync_from_ftp_files(chain) if chain.revel_to_relevant_ftp_url
    Chain.delay.relevant_to_revel_sync_from_ftp_files(chain) if chain.relevant_to_revel_ftp_url
  end
  puts "end revel_sync_from_ftp_files"
end

task :send_email_less_active_vantiv_card_to_admin => :environment do
  puts "send_email_less_active_vantiv_card_to_admin"
  available_cards = VantivGiftCard.group("chain_id").count
  available_cards.each do |key,value|
    chain = Chain.find(key) rescue nil
    if chain
      cards = VantivGiftCard.where(chain_id: chain.id, :used => false).count
      ChainMailer.less_available_card_report(chain, "Please upload new cards soon! Because we have available cards below #{Setting.vantiv.min_available_card} that can be used by user. ").deliver if cards < Setting.vantiv.min_available_card
    end
  end
  puts "end send_email_less_active_vantiv_card_to_admin"
end

task :generate_fishbowl_promotion_code_below_limit => :environment do
  puts "generate fishbowl promotion code below limit"
  FishbowlPromotionSetting.select("id, chain_id").each do |fps|
    chain = fps.chain
    chain.rewards.select("id, 'expiryDate'").each do |r|
      unless r.fishbowl_reward_promotion_setting.blank?
        unused_count = FishbowlPromotionCode.select("id").where("reward_id = #{r.id} AND user_id IS NULL").count
        if unused_count < 500
          Delayed::Job.enqueue(FishbowlPromotionCodeGenerateJob.new(r))
        end
      end
    end
  end
end

task :import_ritas_restaurants_from_ftp_files => :environment do
  puts "import_restaurants_from_ftp_files for ritas only"
  ritas_chain = Chain.find(73)
  Restaurant.delay.import_from_ftp_folder(ritas_chain)
  puts "end import_restaurants_from_ftp_files for ritas only"
end

task :remove_receipt_approved_detail_old_data => :environment do
  puts "remove_receipt_approved_detail_old_data"
  today = Time.zone.now.in_time_zone("EST")
  last_day = today -  1.days
  ReceiptApprovedDetail.where(:issue_date => last_day.beginning_of_day..last_day.end_of_day).delete_all
end

task :import_restaurants_from_json_url=> :environment do
  puts "import_restaurants_from_json_url for SmoothieKing only"
  sk_chain_test = Chain.find(132)
  json_url = REDIS.get("json_url#{sk_chain_test.id}")
  json_frequency = REDIS.get("json_frequency#{sk_chain_test.id}") || 0
  frequency = 24 / json_frequency.to_i
  (1..frequency).each do |time|
    p "creating import_restaurants_from_ftp_files frequency #{time}"
    run_at = Time.zone.now.in_time_zone("EST").midnight + (time * json_frequency.to_i).hours
    Delayed::Job.enqueue(SmoothieKingImportRestaurantsJob.new(sk_chain_test), :run_at => run_at)
  end if json_url

  puts "end import_restaurants_from_json_url for SmoothieKing only"
end

task :import_restaurants_from_momentfeed=> :environment do
  MomentfeedSetting.all.each do |momentfeed_setting|
    puts "import_restaurants_momentfeed"
    chain = momentfeed_setting.chain
    # momentfeed = chain.momentfeed_setting unless chain.momentfeed_setting.blank?

    json_url = "#{momentfeed_setting.base_url}?api_token=#{momentfeed_setting.api_key}"
    if momentfeed_setting.update_type == "1"
      json_frequency = momentfeed_setting.update_interval
      frequency = 24 / json_frequency.to_i
      (1..frequency).each do |time|
        p "creating import_restaurants_from_ftp_files frequency #{time}"
        run_at = Time.zone.now.in_time_zone("EST").midnight + (time * json_frequency.to_i).hours
        Delayed::Job.enqueue(MomentfeedImportRestaurantsJob.new(chain), :run_at => run_at)
      end
    else
      run_at = DateTime.parse(momentfeed_setting.update_interval_option).in_time_zone("EST")
      Delayed::Job.enqueue(MomentfeedImportRestaurantsJob.new(chain), :run_at => run_at)
    end if json_url

  end
  puts "end import_restaurants_from_momentfeed"
end

task :onosys_to_relevant_store_info_synchronization => :environment do
  puts "onosys_to_relevant_store_info_synchronization"
  onosys_settings = OnosysConnectSetting.where("api_root is not null and api_key is not null")
  onosys_settings.each do |setting|
    Delayed::Job.enqueue(OnosysToRelevantStoreInfoSynchronizationJob.new(setting))
  end
  puts "end onosys_to_relevant_store_info_synchronization"
end

task :update_restauants_hour_connect_to_olo => :environment do
  puts "update_restauants_hour_connect_to_olo"
  # OloConnectSetting.all.each do |olo_connect|
  #   chain = olo_connect.chain
  #   chain.update_restaurant_hours_olo_connected(olo_connect) if chain
  # end
  chain = Chain.find(36)
  chain.update_restaurant_hours_olo_connected
end
