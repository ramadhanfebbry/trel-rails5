source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.3.4'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', "5.2"
gem 'pg'
gem 'test-unit'
#gem 'ruby-debug19'
gem 'devise'
gem "omniauth-facebook"
gem "spreadsheet"
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'time_difference'
#gem "omniauth-twitter"
gem 'settingslogic'
gem 'aws-sdk'
gem 'rubyzip'
gem 'zip-zip' # will load compatibility for old rubyzip API.
#gem 'aws-s3'
#gem 'aws'
#gem 'right_aws'
gem 'yajl-ruby'
gem 'execjs'
gem 'sass'
gem 'redis'
gem 'cancan'
gem 'fastercsv'
gem 'redcarpet'
gem 'unicorn'
gem 'sanitize'
gem 'griddler', github: 'thoughtbot/griddler'
gem 'griddler-sendgrid', github: 'thoughtbot/griddler-sendgrid'
# gem 'json_pure'
gem "coffee-script"
#gem 'mailcatcher', :platforms => :ruby
#gem "rmagick", "2.12.0", :require => 'RMagick', :platforms => :ruby
gem "rmagick", :require => 'rmagick'
#platforms => :ruby  added b/c not working in windows with libv8 (3.3.10.4)  gem
#gem 'therubyracer', :platforms => :ruby #, :require => nil
#gem 'cancan'
gem 'aasm'
#gem 'ar-octopus'
gem 'premailer-rails'
gem 'nokogiri'
gem "escape_utils"
gem "pusher"
gem "ZenTest"
gem 'fuzzy-string-match'
#gem 'fuzzy_match'
gem 'mail'
gem 'fog-aws'
gem 'fog-rackspace'
gem 'fog-google'
gem "cocoon"
#gem "heroku-forward"
gem 'gibbon'
gem "mongoid"
gem "ckeditor"
gem 'mail_view', :git => 'https://github.com/basecamp/mail_view.git'
#gem "rack-timeout"
gem 'sidekiq'
gem 'sinatra'
gem 'slim'

# group :assets do
gem 'sass-rails'
#gem 'therubyracer'
gem 'coffee-rails'
gem 'uglifier'
# end

gem 'jquery-rails'
gem 'acts-as-taggable-on'
gem 'geokit'
gem 'city-state'
gem 'geocoder'
gem 'geokit-rails'
gem 'will_paginate'
gem 'oauth2'
gem 'fcm'
gem 'fcm_pusher'
#gem 'gridhook' # webhook from sendgrid
# gem "aws-sdk"
#gem "delayed_job", "~> 2.1.4"
gem 'delayed_job_active_record'
gem "paperclip"
#gem "paperclip", :git => "http://github.com/thoughtbot/paperclip.git"
gem 'simple_form'
#gem 'rapns', '2.0.0'
gem 'rpush'
gem 'permanent_records'
gem 'gcm_helper', :git => 'git://github.com/xinge/gcm_helper.git'
gem 'net-ssh'
gem 'net-sftp'
#gem 'formtastic'
#gem "geocoder"
#gem 'simple_captcha', :git => 'git://github.com/galetahub/simple-captcha.git'
gem "recaptcha", :require => "recaptcha/rails"
#gem 'ruby-prof', :git => 'git://github.com/wycats/ruby-prof.git'
#gem 'test-unit'
gem 'activerecord-import'
gem 'braintree'
gem "validate_url"
gem 'net-ldap'
gem 'httparty'
gem 'mandrill-api'
gem 'json', '1.8.6'
gem 'roo'
gem 'area'
gem 'barby'
gem "chunky_png"

#gem 'rollbar'
gem 'actionpack-xml_parser'
gem 'rails-observers'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15', '< 4.0'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'config'
# gem 'heroku'
gem 'rest-client'
