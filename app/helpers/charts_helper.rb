module ChartsHelper
  def render_chart_widget(name, locals = {})
    options = {
      title: name.humanize.camelize,      
      chart_name: name
    }
    render partial: 'shared/ajax_chart_widget', locals: options.merge(locals)
  end

  def render_chart(name, locals = {})
    options = {
      title: name.humanize.camelize,      
      chart_name: name
    }
    render partial: 'shared/ajax_chart', locals: options.merge(locals)
  end


  def render_table(name, locals = {})
    options = {
      title: name.humanize.camelize,      
      chart_name: name
    }
    render partial: 'shared/ajax_table', locals: options.merge(locals)
  end


  # useful in case we want to pass more locals from views
  def chart_url_params(params, locals)
    keys = [ :method, :utf8, :authenticity_token ]
    params.except(:controller, :action).merge(locals.slice(keys))
  end

  def with_url_params(query_string, params, locals)    
    url_params = chart_url_params(params, locals)
    if url_params && url_params.keys.length > 0
      "#{query_string}?#{url_params.to_query}"
    else
      query_string
    end
  end

end
