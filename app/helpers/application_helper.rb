module ApplicationHelper

  def receipt_status_name(status)
    case status
    when Receipt::STATUS[:APPROVED]
      'Approved'
    when Receipt::STATUS[:REJECTED]
      'Rejected'
    when Receipt::STATUS[:RECEIVED]
      'Received'
    when Receipt::STATUS[:OCRED]
      if current_admin
        'OCRed'
      elsif current_user
        'Received'
      end
    when Receipt::STATUS[:PENDING_OCR]
      "Pending OCR"
    else
      'Unknown'
    end
  end

  def receipt_status_color(status)
    case status
      when Receipt::STATUS[:APPROVED]
        content_tag(:div, "", :style => "background:green;", :class => "receipt_status_box")
      when Receipt::STATUS[:REJECTED]
        content_tag(:div, "", :style => "background:red;", :class => "receipt_status_box")
      when Receipt::STATUS[:RECEIVED]
        content_tag(:div, "", :style => "background:black;", :class => "receipt_status_box")
      when Receipt::STATUS[:OCRED]
        content_tag(:div, "", :style => "background:grey;", :class => "receipt_status_box")
      when Receipt::STATUS[:RECEIVED]
        content_tag(:div, "", :style => "background:black;", :class => "receipt_status_box")
      when Receipt::STATUS[:PENDING_OCR]
        content_tag(:div, "", :style => "background:blue;", :class => "receipt_status_box")
    end
  end

  def reward_check_status_color(status)
    case status
      when PosRewardCheck::STATUS["SUCCESS"]
        content_tag(:div, "", :style => "background:green;", :class => "receipt_status_box")
      when PosRewardCheck::STATUS["ERROR"]
        content_tag(:div, "", :style => "background:red;", :class => "receipt_status_box")
    end
  end

  def usercode_status(pos_check_upload)
    return if (pos_check_upload.service.blank? || pos_check_upload.service.eql?(PosCheckUpload::SERVICE[:TABLE])) && !pos_check_upload.pos_used_type.eql?(Chain::POS_USED_TYPE["NCR"])
    if pos_check_upload.user_code.blank?
      content_tag(:div, "", :style => "background:grey;margin: auto; left: 0px; right: 0px; position: relative;", :class => "receipt_status_box") +
      "NO DATA"
    elsif pos_check_upload.error_code.blank?
      content_tag(:div, "", :style => "background:green;margin: auto; left: 0px; right: 0px; position: relative;", :class => "receipt_status_box") +
      "VALID"
    elsif [10,11,12, 13].include?(pos_check_upload.error_code)
      content_tag(:div, "", :style => "background:red;margin: auto; left: 0px; right: 0px; position: relative;", :class => "receipt_status_box") +
          "INVALID/EXPIRED"
    end
  end

  def focus_usercode_status(pos_check_upload)
    if pos_check_upload.user_code.blank?
      content_tag(:div, "", :style => "background:grey;margin: auto; left: 0px; right: 0px; position: relative;", :class => "receipt_status_box") +
          "NO DATA"
    else
      content_tag(:div, "", :style => "background:green;margin: auto; left: 0px; right: 0px; position: relative;", :class => "receipt_status_box") +
          "VALID"
    end
  end


  def user_signup_status_color(status)
    selected_status = User::SIGNUP_STATUS.invert[status]
    case selected_status
      when "GREEN"
        content_tag(:div, "", :style => "background:green;", :class => "receipt_status_box")
      when "ORANGE"
        content_tag(:div, "", :style => "background:orange;", :class => "receipt_status_box")
      when "GREY"
        content_tag(:div, "", :style => "background:grey;", :class => "receipt_status_box")
    end
  end

  def user_signup_status_name(status)
    selected_status = User::SIGNUP_STATUS.invert[status]
    case selected_status
      when "GREEN"
        "UNIQUE"
      when "ORANGE"
        "NOT UNIQUE"
      when "GREY"
        "NOT AVAILABLE"
    end
  end



  def link_to_granted_receipt(receipt, name, url, options = {})
    #if receipt.status.eql?(Receipt::STATUS[:PENDING_OCR])
    #  "<a href='#' onclick='$.modal(\"#{escape_javascript(render("receipt_confirmation", {:url => url}))}\")'>#{name}</a>".html_safe
    #else
      link_to name, url, options
    #end
  end


  def payment_method_name(method)
    unless method.blank?
      case method
      when Receipt::PAYMENT_METHOD[:CASH]
        'Cash'
      when Receipt::PAYMENT_METHOD[:CARD]
        'Credit Card'
        #when Receipt::PAYMENT_METHOD[:DEBIT]
        #	'Debit Card'
        #when Receipt::PAYMENT_METHOD[:CHEQUE]
        #	'Cheque'
      when Receipt::PAYMENT_METHOD[:OTHER]
        ''
      else
        ''
      end
    end
  end

  def survey_status_name(status)
    case status
    when Survey::STATUS["Active"]
      'Active'
    when Survey::STATUS["InActive"]
      'InActive'
    else
      'InActive'
    end
  end

  def reward_type(type)
    case type
    when Reward::TYPES["ONE_TIME"]
      'One Time'
    when Reward::TYPES["REGULAR"]
      'Reguar'
    when Reward::TYPES["MILESTONE"]
      'Milestone'
    end
  end

  def incentive_reward_type_detail(incentive)
    if incentive.class.to_s == "DealOffer"
      if incentive.kind == DealOffer::TYPES["POINTS"]
        return "POINTS"
      else
        return "OFFLINE"
      end
    else
      return "Reward"
    end
  end

  def incentive_type_detail(incentive)
    if incentive.user_activity.class.to_s == "UserCheckin"
      return "Checkin"
    else
      return "Submit Survey"
    end
  end

  def incentive_detail(incentive)
    unless incentive.blank?
      case incentive.class.to_s
      when "Reward"
        incentive.name+": points -- #{incentive.points}"
      when "DealOffer"
        if incentive.kind == DealOffer::TYPES["POINTS"]
          incentive.points.to_s+" points"
        else
          incentive.title
        end
      end
    else
      return "-"
    end
  end

  def incentive_status_earn(status)
    if status.downcase == "guaranteed"
      "Earned"
    else
      "Participated"
    end
  end

  def user_type(type)
    case type
    when User::USERS_TYPES["Email PWD"]
      'Email PWD'
    when User::USERS_TYPES["Facebook"]
      'Facebook'
    when User::USERS_TYPES["Twitter"]
      'Twitter'
    else
      'Unknown'
    end
  end

  def format_date(date)
    date = date.split("-")
    date = date[2]+"-"+date[0]+'-'+date[1]
  end

  def device_name(device_id)
    case device_id
    when 1
      "Android"
    when 2
      "Iphone"
    else
      "-"
    end
  end

  def reward_event(event)
    case event
    when ChainRewardEvent::EVENTS["SIGN_UP"]
      "Sign Up Reward"
    when ChainRewardEvent::EVENTS["FIRST_RECEIPT"]
      "First Receipt Reward"
    when ChainRewardEvent::EVENTS["HOHOHO"]
      "HOHOHO Reward"
    when ChainRewardEvent::EVENTS["PUNCHH_MIGRATION"]
        "Punchh Migration Reward"
    end
  end

  def date_helper(date)
    zone = Setting.default_time_zone["zone"] || Time.zone.now.zone
    # with current zone first.
    #zone = Time.zone.now.zone
    if date and date.class == DateTime
      date = date.in_time_zone(zone).strftime("%Y-%m-%d %H:%M:%S %Z")
    end
    date
  end

  def date_helper_timezone(date, timezone = nil)
    timezone ||= Setting.default_time_zone["zone"] || Time.zone.now.zone

    if date and date.class == DateTime
     date =  date.in_time_zone(timezone).strftime("%Y-%m-%d %H:%M:%S %Z")
    end
    date
  end

  def date_only(date)
    zone = Setting.default_time_zone["zone"] || Time.zone.now.zone
    zone = Time.zone.now.zone
    #date.to_datetime.in_time_zone(zone).strftime("%Y-%m-%d") if date
    date.to_datetime.strftime("%Y-%m-%d %Z") if date
  end

  def string_format(str,opts = {})

    if is_number?(str)
      #slice_size = opts[:slice_limit] || 10
      #ln = str.length
      #res = []
      #
      #if ln > slice_size
      #  slice = ln / slice_size
      #
      #  to = slice_size
      #  from = 0
      #  0.upto(slice) do |x|
      #    res << str.slice(from..to)
      #    from += slice_size + 1
      #    to += slice_size + 1
      #  end
      #else
     #   return str
     # end
      return "" if str.to_i == 0
      return str
    else
      return str
    end
  end

  def is_number?(var)
    true if Float(var) rescue false
  end

  def total_members(restaurant)
    Receipt.restaurants(restaurant.id).uniq.count
  end

  def set_perk_data_value(perk)
    val = if perk.perk_type.nil?
      nil
    else
      case perk.perk_type
      when Perk::TYPE[:MULTIPLY]
        perk.multiplier
      when Perk::TYPE[:THRESHOLD]
        perk.bonus_threshold
      when Perk::TYPE[:BONUS_POINT]
        perk.bonus_points
      end
    end
    val
  end

  def float_format(number,precision = 2)
    begin
      if number.nan?
        return 0
      else
        return number_with_precision(number, :precision => precision)
      end
    rescue
      return number_with_precision(number, :precision => precision)
    end
    return number_with_precision(number, :precision => precision) if number.class == Float
  end

  def markdown(text)
    @md=Redcarpet::Markdown.new(Redcarpet::Render::HTML, :no_intra_emphasis => true, :autolink => true, :space_after_headers => true) if @md.nil?
    @md.render(text).html_safe
  end

  def hour_only(date)
    date.strftime("%I:%M %p")
  end

  def hour_only_datetime(date, timezone = nil)
    timezone ||= Setting.default_time_zone["zone"] || Time.zone.now.zone
    date.in_time_zone(timezone).strftime("%I:%M %p") if date
  end

  def table_generate_receipts(weeks,restaurants)
    str = ""
    str << "<thead>"

    str<< "<th class='tblheader '>Location<div class='icontbl'></div></th>"
    weeks.downto(0).each do |week|
      str << "<th class='tblheader'>#{(Time.zone.now - (week).days).strftime("%d %b")}<div class='icontbl'></div></th>"
    end

    str << "<thead>"
    #table_generate_body_receipts(weeks, restaurants)
    return str.html_safe
  end

  def table_generate_receipts_search(start_date,end_date,restaurants)
    end_date = end_date.to_date
    start_date = start_date.to_date
    str = ""
    str << "<thead>"
    loop = (end_date - start_date).to_i

    str<< "<th class='tblheader '>Location<div class='icontbl'></div></th>"
    loop.downto(0).each do |week|
      str << "<th class='tblheader'>#{(end_date - (week).days).strftime("%d %b")}<div class='icontbl'></div></th>"
    end

    str << "<thead>"
    #table_generate_body_receipts(weeks, restaurants)
    return str.html_safe
  end


  def table_generate_rewards_search(start_date,end_date,restaurants)
    end_date = end_date.to_date
    start_date = start_date.to_date
    loop = (end_date - start_date).to_i
    str = ""
    str << "<thead>"

    str<< "<th class='tblheader '>Location<div class='icontbl'></div></th>"
    loop.downto(0).each do |week|
      str << "<th class='tblheader'>#{date_handler(loop - week)}<div class='icontbl'></div></th>"
    end

    str << "<thead>"
    return str.html_safe
  end

  def table_generate_body_receipts(weeks, restaurants)
    str = ""
    restaurants.each do |res|
      str << "<tr class='first'>"
      str << "<td>#{res.dashboard_display_text}</td>"

      weeks.downto(0).each do |week|
        approved = approved_receipt(week,res.id)
        if approved > 0
          if current_owner.is_manager?
            str << "<td>#{approved}</td>"
          else
            str << "<td>#{link_to approved,show_detail_receipt_owner_activity_path(week, :restaurant_id => res.id)}</td>"
          end
        else
          str << "<td>#{approved}</td>"
        end
      end

      str<< "</tr>"
    end

    return str.html_safe
  end

  def table_generate_body_rewards_ajax(weeks, res)
    #result = Rails.cache.fetch("activities_rewards_#{res.id}_#{current_owner.id}",
    #                           :expires_in => 3.hours) do
      str = ""

      # str << "<tr class='first'>"
      str << "<td>#{res.dashboard_display_text}</td>"
      reward_redeemed = reward_redeemed_prepopulate(weeks, res.id)
      weeks.downto(0).each do |week|
        approved = reward_redeemed.select { |x| x.transaction_date.to_s == (Time.zone.now - week.days).to_date.to_s }
        approved = approved.first
        if !approved.blank? and approved.total.to_i > 0
          if current_owner.is_manager?
            str << "<td>#{approved.total.to_i}</td>"
          else
            str << "<td>#{link_to approved.total.to_i, show_detail_reward_owner_activity_path(week,
                                                                                              :restaurant_id => res.id), :target => '_blank'}</td>"
          end
        else
          unless approved.blank?
            str << "<td>#{approved.total.to_i}</td>"
          else
            str << "<td>0</td>"
          end
        end
      end

      # str<< "</tr>"


      result = str.html_safe
      #str.html_safe
    #end
    return result
  end

  def table_generate_body_receipts_ajax(weeks, res)
   # result = Rails.cache.fetch("activities_restaurant_#{res.id}_#{current_owner.id}",
   #                            :expires_in =>  3.hours) do
    str = ""

     # str << "<tr class='first'>"
      str << "<td>#{res.dashboard_display_text}</td>"
      approved_receipt = approved_receipt_prepopulate(weeks,res.id)
      weeks.downto(0).each do |week|
        approved = approved_receipt.select{|x| x.transaction_date.to_s == (Time.zone.now - week.days).to_date.to_s}
        approved = approved.first
        if weeks == 0
          cnt = Receipt.receipt_date_dashboard_receipt_activity(rs.id, (Time.zone.now - day.days)).count(distinct: true)
        end
        if !approved.blank? and approved.total.to_i > 0
          if current_owner.is_manager?
            str << "<td>#{approved.total.to_i}</td>" if weeks != 0
            str << "<td>#{cnt.to_i}</td>" if weeks == 0
          else
            str << "<td>#{link_to approved.total.to_i,show_detail_receipt_owner_activity_path(week,
                                                                                   :restaurant_id => res.id), :target => '_blank'}</td>" if weeks != 0
            str << "<td>#{link_to cnt.to_i,show_detail_receipt_owner_activity_path(week,
                                                                                              :restaurant_id => res.id), :target => '_blank'}</td>" if weeks == 0
          end
        else
          unless approved.blank?
          str << "<td>#{approved.total.to_i}</td>"
          else
            str << "<td>0</td>"
            end
        end
      end

     # str<< "</tr>"


     result = str.html_safe
    #str.html_safe
    #end
    return result
  end


  def table_generate_body_receipts_ajax_search(start_date,end_date, res)
    # result = Rails.cache.fetch("activities_restaurant_#{res.id}_#{current_owner.id}",
    #                            :expires_in =>  3.hours) do
    start_date = start_date.to_date
    end_date = end_date.to_date
    str = ""
    loop = (end_date - start_date).to_i
    # str << "<tr class='first'>"
    str << "<td>#{res.dashboard_display_text}</td>"
    approved_receipt = approved_receipt_prepopulate_search(start_date,end_date,res.id)
    loop.downto(0).each do |week|
      approved = approved_receipt.select{|x| x.transaction_date.to_s == (end_date - week.days).to_date.to_s}
      approved = approved.first
      if !approved.blank? and approved.total.to_i > 0
        if current_owner.is_manager?
          str << "<td>#{approved.total.to_i}</td>"
        else
          str << "<td>#{link_to approved.total.to_i,show_detail_receipt_owner_activity_path(week,
                                                                                            :restaurant_id => res.id), :target => '_blank'}</td>"
        end
      else
        unless approved.blank?
          str << "<td>#{approved.total.to_i}</td>"
        else
          str << "<td>0</td>"
        end
      end
    end

    # str<< "</tr>"


    result = str.html_safe
    #str.html_safe
    #end
    return result
  end

  def table_generate_body_rewards_ajax_search(start_date,end_date, res)
    str = ""
    start_date = start_date.to_date
    end_date = end_date.to_date
    loop = (end_date - start_date).to_i

    str << "<td>#{res.dashboard_display_text}</td>"
    reward_redeemed = reward_redeemed_prepopulate_search(start_date,end_date,res.id)
    loop.downto(0).each do |week|
      approved = reward_redeemed.select { |x| x.transaction_date.to_s == (end_date - week.days).to_date.to_s }
      approved = approved.first
      if !approved.blank? and approved.total.to_i > 0
        if current_owner.is_manager?
          str << "<td>#{approved.total.to_i}</td>"
        else
          str << "<td>#{link_to approved.total.to_i, show_detail_reward_owner_activity_path(week,
                                                                                            :restaurant_id => res.id), :target => '_blank'}</td>"
        end
      else
        unless approved.blank?
          str << "<td>#{approved.total.to_i}</td>"
        else
          str << "<td>0</td>"
        end
      end
    end

    result = str.html_safe
    return result
  end

  def csv_table_generate_receipts(weeks,restaurants)
    a = ["Locations"]
    b = []
    weeks.downto(0).each do |week|
      b << date_handler(week)
    end
    b = a + b
    #table_generate_body_receipts(weeks, restaurants)
    return b
  end

  def csv_table_generate_body_receipts(weeks, restaurants)
    result = []
    restaurants.each do |res|

      result << res.dashboard_display_text

      weeks.downto(0).each do |week|
        approved = approved_receipt(week,res.id)
        result << approved
      end
    end
    return result
  end

  def date_handler(week)
    #week = week - 2
    (Time.zone.now - week.days).strftime("%d %b")
  end

  def table_generate_rewards(weeks,restaurants)
    str = ""
    str << "<thead>"

    str<< "<th class='tblheader '>Location<div class='icontbl'></div></th>"
    weeks.downto(0).each do |week|
      str << "<th class='tblheader'>#{date_handler(week)}<div class='icontbl'></div></th>"
    end

    str << "<thead>"
    return str.html_safe
  end

  def table_generate_body_rewards(weeks, restaurants)
    str = ""
    restaurants.each do |res|
      str << "<tr class='first'>"
      str << "<td>#{res.dashboard_display_text}</td>"

      weeks.downto(0).each do |week|
        redeemed = reward_redeemed(week,res.id)
        if redeemed > 0
          str << "<td>#{link_to redeemed,show_detail_reward_owner_activity_path(week, :restaurant_id => res.id)}</td>"
        else
          str << "<td>#{redeemed}</td>"
        end
      end

      str<< "</tr>"
    end

    return str.html_safe
  end

  def csv_table_generate_body_rewards(weeks, restaurants)
    str = []
    restaurants.each do |res|

      str << res.dashboard_display_text
      weeks.downto(0).each do |week|
        redeemed = reward_redeemed(week,res.id)
        str << redeemed
      end
    end

    return str
  end

  def generate_detail_header_reward(rewards)
    str = ""
    duplicate = []
    rewards.each do |reward|
      name = reward.restaurant_reward.reward.name
      if !duplicate.include?(name.downcase)
        str << "<th>#{name}</th>"
      end
      duplicate << name.downcase
    end
    str.html_safe
  end

  def generate_detail_body_reward(rewards,w,restaurant_id)
    str = ""
    reward_ids = rewards.map(&:restaurant_reward).map(&:reward_id).uniq
    1.upto(24) do |x|
      str << "<tr>"
      str << "<td>#{x} : 00</td>"


      str << "<td>#{count_total_reward(rewards,x)}</td>"

      reward_ids.each do |r|
        c = 0
        rewards.each do |reward|
          if reward.created_at.utc.hour.to_i == x and reward.restaurant_reward.reward_id == r
            c = c + 1
          end
        end
        #<%= link_to "Add Incentive","#test_modal", :onclick => "$('#test_modal').html('');$.get('#{pick_incentive_admin_deal_path(deal)}')","data-toggle" => "modal", :class => "btn"  %>
        if c > 0
          str << "<td>#{link_to c,"#modal_popup", :onclick =>
          "$('#modal_popup').html('');$('#modal_popup').show();$.get('#{show_popup_reward_activities_owner_activity_path(
          :id => w,:restaurant_id => restaurant_id,
          :hour => x, :format => :js
          )}')",
          "data-toggle" => "modal"}</td>"
        else
          str << "<td>#{c}</td>"
        end
      end
      str << "</tr>"
    end
    str.html_safe
  end

  def csv_generate_detail_header_reward(rewards)
    str = []
    str << "Hour"
    str << "Total"
    rewards.each do |reward|
      str << reward.restaurant_reward.reward.name
    end
    str
  end

  def csv_generate_detail_body_reward(x,rewards)
    str = []
    str << "#{x}00"
    c = 0
    total = 0
    str << count_total_reward(rewards,x)
    rewards.each do |reward|
      if reward.created_at.hour.to_i == x
        c = c + 1
        str << c
        total = total + c
      else
        str << ""
      end
    end
    str
  end

  def count_total_reward(rewards,x)
    res = 0
    rewards.each do |reward|
      if reward.created_at.utc.hour.to_i == x
        res = res + 1
      end
    end
    res == 0 ? "" : res
  end

  def approved_receipt(week,res_id)
    week = week# - 1
    x = Receipt.new_dashboard_receipt_activity(res_id,(Time.zone.now - week.days)).count(distinct: true)
    x
  end

  def approved_receipt_prepopulate(weeks,res_id)
    #total = 0
    #res = ActivitySummary.where("restaurant_id = ? and transaction_date = date(?)", res_id,(Time.zone.now - week.days)).first
    #total = res.total.to_i unless res.blank?
    #return total
    result = ActivitySummary.where('transaction_date between date(?) and date(?) and restaurant_id = ?',
                          (Time.zone.now - (weeks+2).days),Time.zone.now,res_id)
    return result
  end

  def approved_receipt_prepopulate_search(start_date,end_date,res_id)
    #total = 0
    #res = ActivitySummary.where("restaurant_id = ? and transaction_date = date(?)", res_id,(Time.zone.now - week.days)).first
    #total = res.total.to_i unless res.blank?
    #return total
    result = ActivitySummary.where('transaction_date between date(?) and date(?) and restaurant_id = ?',
                                   (start_date - 2.days),end_date,res_id)
    return result
  end

  def reward_redeemed_prepopulate(weeks,res_id)
    result = RewardSummary.where('transaction_date between date(?) and date(?) and restaurant_id = ?',
                                   (Time.zone.now - (weeks+2).days),Time.zone.now,res_id)
    return result
  end

  def reward_redeemed_prepopulate_search(start_date,end_date,res_id)
    result = RewardSummary.where('transaction_date between date(?) and date(?) and restaurant_id = ?',
                                 (start_date - (2).days),end_date,res_id)
    return result
  end

  def reward_redeemed(week,res_id)
    week = week
    RewardTransaction.by_restaurant_and_date(res_id, ((Time.zone.now - week.to_i.days))).count
  end

  def initialize_modal(id_modal, path)
    "$('##{id_modal}').html('#{escape_javascript(render "shared/ajax_loader")}');
      $.get('#{path}')"
  end

  def show_ref_incentive(incentive)
    puts incentive.class
    case incentive.class.to_s
    when "Reward"
      return link_to "#{incentive.name}", admin_reward_path(incentive), :target => '_blank'
    when "ReferralOffer"
      return incentive.points
    else
      "There are no incentive"
    end
  end

  def comparison_value(value)
    content_tag :div, :style => "font-size:12px;font-weight:normal" do
      if value > 0
        image_tag('thumb-up-icon.png') + "   + " + number_to_currency(value, :precision => 0, :unit => "")
      elsif value < 0
        image_tag('thumb-icon.png') + "      " + number_to_currency(value, :precision => 0, :unit => "")
      else
        number_to_currency(value, :precision => 0, :unit => "")
      end
    end
  end

  def render_query_to_json(query)
    str = ""
    begin
      if query.class == String
        JSON.parse(query).each do |k, v|
          in_value = Integer(v) rescue v
          if in_value.class ==  Fixnum
            v = Date::DAYNAMES[v.to_i]
          end
          puts v

          str += "<span>#{k} ---- #{v}</span><br />"
        end
      else
        query.each do |k, v|
          in_value = Integer(v) rescue v
          if in_value.class ==  Fixnum
            v = Date::DAYNAMES[v.to_i]
          end
          puts v
          str += "<span>#{k} ---- #{v}</span><br />"
        end
      end
    rescue
      str = "&nbsp;"
    end
    return str.gsub('week_day','Executed Every').html_safe
  end

  def user_answers_show_list(answers)
    list_answer = ""
    answers.group_by(&:question_id).each do |question_id, ans|
      question = Question.find(question_id) rescue nil
      if question
        list_answer += question.text rescue ""
        list_answer += " = "
        list_answer += ans.map(&:value_id).join(",")
        list_answer += "<br />"
      end
    end
    list_answer
  end

  def issue_hour(lt,lts)
    timezone ||= Setting.default_time_zone["zone"] || Time.zone.now.zone

    ## for xpient bugs
    begin
    if lts.pos_check_upload.is_xpient_check
      return lt.created_at.in_time_zone(timezone).strftime("%I : %M %p")
    end
    rescue
      end
    #date.in_time_zone(timezone).strftime("%Y-%m-%d %H:%M:%S") if date
    unless lt.blank?
     return lt.time_stamp.blank?? lts.created_at.in_time_zone(timezone).strftime("%I : %M %p") : lt.time_stamp
    else
      return lts.created_at.in_time_zone(timezone).strftime("%I : %M %p")
    end

  end

  def issue_date_switch(lt,lts)
    timezone ||= Setting.default_time_zone["zone"] || Time.zone.now.zone

    #date.in_time_zone(timezone).strftime("%Y-%m-%d %H:%M:%S") if date
    unless lt.blank?
      lt.issue_date.blank?? lts.created_at.in_time_zone(timezone).to_datetime.strftime("%Y-%m-%d") : lt.issue_date.in_time_zone(timezone).strftime("%Y-%m-%d")
    else
      lts.created_at.in_time_zone(timezone).to_datetime.strftime("%Y-%m-%d")
      end
  end

  def show_xpient_line_items(line_items, space = 0)
    table_views = ""
    line_items.each do |menu_item|
      item_space = space
      table_views += "<tr>"
      table_views += "<td>#{'&nbsp;'*space}#{menu_item["id"]}</td>"
      table_views += "<td>#{'&nbsp;'*space}#{menu_item["description"] rescue nil}</td>"
      table_views += "<td>#{menu_item["quantity"] rescue nil}</td>"
      table_views += "<td>#{menu_item["type"].eql?("LOYALTY_DISCOUNT") ? "-#{((menu_item["price"] rescue 0))}" : "#{(menu_item["quantity"].to_i * (BigDecimal.new(menu_item["price"].to_s) rescue 0))}"}</td>"
      table_views += "</tr>"
      item_space += 4
      table_views += show_xpient_line_items(menu_item["childItems"], item_space)
    end unless line_items.blank?
    table_views
  end

  def show_xpient_line_items_dashboard(line_items, space = 0)
    views = ""
    line_items.each do |menu_item|
      item_space = space
      views += "<div><span class=\"menu-left\">"
      views += "#{'&nbsp;'*space}#{(menu_item["quantity"] rescue 1)}  x  "
      views += "#{(menu_item["description"] rescue "-")}"
      views += "</span><span class=\"menu-right\">#{menu_item["type"].eql?("LOYALTY_DISCOUNT") ? "-#{((menu_item["price"] rescue 0))}" : "#{(menu_item["quantity"].to_i * (BigDecimal.new(menu_item["price"].to_s) rescue 0))}"}</span>"
      views += "</div><br style=\"clear:both;\" />"
      item_space += 4
      views += show_xpient_line_items_dashboard(menu_item["childItems"], item_space)
    end unless line_items.blank?
    views
  end

  def us_time_zones
    zones = []
    ActiveSupport::TimeZone.us_zones.map do |zone|
      zones << [zone, zone.name]
    end
    zones
  end

  def restaurant_status_title(rest)
    rest = rest
    detail = rest.restaurant_detail

    unless detail.blank? || detail.status.blank?
      if detail.status.eql?(Restaurant::TYPES["OPEN"])
        "open"
      elsif detail.status.eql?(Restaurant::TYPES["CLOSED"])
        "closed"
      elsif detail.status.eql?(Restaurant::TYPES["COMING_SOON"])
        "coming soon"
      elsif detail.status.eql?(Restaurant::TYPES["TEMP_CLOSED"])
        "temp closed"
      elsif detail.status.eql?(Restaurant::TYPES["TESTING"])
        "testing"
      end
    else
      rest.status ? "open" : "closed"
    end
  end

  def restaurant_status_color(rest)
    rest = rest
    detail = rest.restaurant_detail

    unless detail.blank? || detail.status.blank?
      if detail.status.eql?(Restaurant::TYPES["OPEN"])
        content_tag(:div, "", :style => "background:green;", :class => "receipt_status_box rest-circle")
      elsif detail.status.eql?(Restaurant::TYPES["CLOSED"])
        content_tag(:div, "", :style => "background:red;", :class => "receipt_status_box rest-circle")
      elsif detail.status.eql?(Restaurant::TYPES["COMING_SOON"])
        content_tag(:div, "", :style => "background:DarkOrange;", :class => "receipt_status_box rest-circle")
      elsif detail.status.eql?(Restaurant::TYPES["TEMP_CLOSED"])
        content_tag(:div, "", :style => "background:gray;", :class => "receipt_status_box rest-circle")
      elsif detail.status.eql?(Restaurant::TYPES["TESTING"])
        content_tag(:div, "", :style => "background:DarkTurquoise;", :class => "receipt_status_box rest-circle")
      end
    else
      if rest.status
        content_tag(:div, "", :style => "background:green;", :class => "receipt_status_box rest-circle")
      else
        content_tag(:div, "", :style => "background:red;", :class => "receipt_status_box rest-circle")
      end
    end
  end

  def date_string_from_time_zone_to_utc(date)
    timezone ||= Setting.default_time_zone["zone"] || Time.zone.now.zone
    temp_date = date.to_time.in_time_zone(timezone)
    temp_date -= temp_date.utc_offset
    temp_date.utc
  end

  def at_hours
    select_options = [ ["6:00AM", "6:00AM"], ["7:00AM", "7:00AM"], ["8:00AM", "8:00AM"], ["9:00AM", "9:00AM"], ["10:00AM", "10:00AM"], ["11:00AM", "11:00AM"], ["12:00PM", "12:00PM"], ["1:00PM", "1:00PM"], ["2:00PM", "2:00PM"], ["3:00PM", "3:00PM"], ["4:00PM", "4:00PM"], ["5:00PM", "5:00PM"], ["6:00PM", "6:00PM"], ["7:00PM", "7:00PM"], ["8:00PM", "8:00PM"], ["9:00PM", "9:00PM"], ["10:00PM", "10:00PM"], ["11:00PM", "11:00PM"], ["12:00AM", "12:00AM"], ["1:00AM", "1:00AM"], ["2:00AM", "2:00AM"], ["3:00AM", "3:00AM"], ["4:00AM","4:00AM" ], ["5:00AM", "5:00AM"]]
  end

  def daily_hours
      [["1", "1"], ["2", "2"], ["3", "3"], ["4", "4"], ["5", "5"], ["6", "6"], ["7", "7"], ["8", "8"], ["9", "9"], ["10", "10"], ["11", "11"], ["12", "12"], ["13", "13"], ["14", "14"], ["15", "15"], ["16", "16"], ["17", "17"], ["18", "18"], ["19", "19"], ["20", "20"], ["21", "21"], ["22", "22"], ["23", "23"], ["24", "24"]]
  end

  def update_types
    [["Hourly update every", 1], ["Daily update at", 2]]
  end

end
