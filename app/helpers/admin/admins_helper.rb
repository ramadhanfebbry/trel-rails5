module Admin::AdminsHelper
	def custom_form_for(path, options = {}, &block)
	  options = options.deep_merge(html: { class: 'form-horizontal col-sm-9' })
	  simple_form_for(path, options, &block)
	end
end
