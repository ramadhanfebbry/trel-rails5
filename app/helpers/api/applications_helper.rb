module Api::ApplicationsHelper
  include RelevantAuthentication
  attr_accessor :app, :chain

  def authenticateAppKey(appkey)
    @chain = @app.send(:chain) unless (appkey.blank? || (@app = Application.getApplication(appkey)) == nil)
  end

  def authorize_api_app!
    p "----------------authorize_api_app!---------------------"
    if !authenticateAppKey(params[:appkey])
      render :status => 404,
        :json => {status: false, notice: t(:something_went_wrong)} and return
    else
      valid, app_status, notice_text, known_key =  Application.get_available_app_key(params[:appkey])
      unless valid
        render :status => 404,
          :json => {status: false, app_status: app_status, notice: t(notice_text)} and return
      end
    end
  end

  def authorize_api_app_from_header!
    if request.method == "GET"
      p "----------------authorize_api_app!---------------------"
      p "APPKEY FROM HEADER : #{request.headers["appkey"]}"
      if !authenticateAppKey(request.headers["appkey"])
        render :status => 404,
          :json => {status: false, notice: t(:something_went_wrong)} and return
      else
        valid, app_status, notice_text, known_key =  Application.get_available_app_key(request.headers["appkey"])
        unless valid
          render :status => 404,
            :json => {status: false, app_status: app_status, notice: t(notice_text)} and return
        end
      end
    else
      authorize_api_app!
    end
  end

  def onosys_authorize_api_app_from_header!
    p "----------------authorize_api_app!---------------------"
    p "APPKEY FROM HEADER : #{request.headers["appkey"]}"
    if !authenticateAppKey(request.headers["appkey"])
      render :status => 404,
             :json => {status: false, notice: t(:something_went_wrong)} and return
    else
      valid, app_status, notice_text, known_key =  Application.get_available_app_key(request.headers["appkey"])
      unless valid
        render :status => 404,
               :json => {status: false, app_status: app_status, notice: t(notice_text)} and return
      end
    end
  end

  def authorize_api_app_from_request_header!
    if request.method == "GET"
      p "----------------authorize_api_app_from_request_header!--- #{params}---#{request.headers["appkey"]}------"
      params["appkey"] = request.headers["appkey"]
      render :status => 404, :json => {status: false, notice: "Our apologies, we've encountered a temporary issue. Please try again later."} and return if params["appkey"].blank?
    end
  end

  def set_locale
    I18n.locale = params[:locale] || "en"
  end

  def check_api_and_authtoken_from_header
    if request.method == "GET"
      p "----------------check_api_and_authtoken_from_header!---#{request.headers["appkey"]}------"
      params["appkey"] = request.headers["appkey"]
      params["auth_token"] = request.headers["auth_token"]
      render :status => 404, :json => {status: false, notice: "Our apologies, we've encountered a temporary issue. Please try again later."} and return if params["appkey"].blank?
    end if request.fullpath.include?("/api/v2/") && request.get?
  end

  def check_query_string
    if request.query_string.present?
      p "----------------check_query_string!----- #{request.query_string} "
      return render :status => 404, :json => {status: false, notice: "Invalid API request."}
    end if request.fullpath.include?("/api/v2/") && request.post?
  end

  def dump_headers
    request.env.select { |k, v| k.match("^HTTP.*") }.each { |header|
      p header[0].split('_', 2)[1] + ' = ' + header[1] rescue nil
    }
  end

  def migrate_by_user_id(user_id)
    p "migration for user #{user_id}"
    #ActiveRecord::Base.establish_connection(PHP_DB[Rails.env.to_sym])
    #query_get_user = "SELECT * from tbl_users_chain WHERE userid = #{user_id} AND chainName = #{@php_chain_id}"
    #php_user = ActiveRecord::Base.connection.select_all(query_get_user).first
    #p php_user
    php_user = PhpUser.where(:userid => user_id, :chainName => @php_chain_id).first
    new_user = build_user_hash_from_php_object(php_user)

    unless new_user.blank?
      @user = User.new(new_user.merge(:category_part => false, :locale_id => Locale.first.id, :chain_id => @chain.id, :php_user => true, :php_user_id => user_id))
      if @user.register_type.to_i.eql?(2)      #fb user type
        if @user.save
          sign_in(@user)
          @user.reset_authentication_token!
          auth_token = @user.authentication_token
          set_device_token_as_unique_for_user(@user, params[:sign_in_device_type], params[:device_token], params[:device_id])
          @user.update_phone_info(params[:phone_model], params[:os], params[:appkey])
          warden.logout
          render :json => {:success => true, :auth_token => auth_token}
          Delayed::Job.enqueue(MigrationJob.new(@user, user_id, nil, @chain.php_chain_id, nil))
        else
          render :json => {:success => true, :redirect_to => "login"}
        end
      else
        if params[:reset_pwd].eql?("true") && params[:new_pwd]
          @user.password = params[:new_pwd]
          @user.password_confirmation = params[:new_pwd]
          if @user.save
            sign_in(@user)
            @user.reset_authentication_token!
            auth_token = @user.authentication_token
            set_device_token_as_unique_for_user(@user, params[:sign_in_device_type], params[:device_token], params[:device_id])
            @user.update_phone_info(params[:phone_model], params[:os], params[:appkey])
            warden.logout
            render :json => {:success => true, :auth_token => auth_token}
            Delayed::Job.enqueue(MigrationJob.new(@user, user_id, nil, @chain.php_chain_id, params[:new_pwd]))
          else
            render :json => {:success => true, :redirect_to => "login"}
          end
        else
          render :json => {:success => true, :notice => "Please enter your password to continue.", :redirect_to => "submit_password"}
        end
      end
    else
      render :json => {:success => false, :redirect_to => "enter_email"}
    end
  end

  def migrate_by_email(email, register_type)
    begin
      p "migration for user email #{email}"
      conditions = []
      conditions <<  "email = '#{email}' AND chainName = #{@chain.php_chain_id} AND status = 'active'"
      conditions <<  register_type.eql?("1") ? "fb_user_id = 0" : "fb_user_id != 0"
      php_user = PhpUser.where(conditions.join(" AND ")).first

      #ActiveRecord::Base.establish_connection(PHP_DB[Rails.env.to_sym])
      #custom_condition = register_type.eql?("1") ? " AND fb_user_id = 0" : " AND fb_user_id != 0"
      #query_get_user = "SELECT * from tbl_users_chain WHERE email = '#{email}' AND chainName = #{@chain.php_chain_id} AND status = 'active' #{custom_condition}"
      #php_user = ActiveRecord::Base.connection.select_all(query_get_user).first

      new_user = build_user_hash_from_php_object(php_user)
      unless new_user.blank?
        @user = User.new(new_user.merge(:category_part => false, :locale_id => Locale.first.id, :chain_id => @chain.id, :php_user => true, :php_email => email))
        @user.generate_password
        password = @user.password
        if @user.save
          if @user.register_type.to_i.eql?(2)
            sign_in(@user)
            @user.reset_authentication_token!
            auth_token = @user.authentication_token
            render :json => {:success => true, :auth_token => auth_token,  notice: t(:welcome_back)}
          else
            render :json => {
              :status => false,
               :notice => "We have updated our systems. Please check your '#{email}' email account and log in with the password provided in that email. \n\n Once logged in, passwords can be changed in Info> Settings (upper right corner) > Reset Password. \n\nThank you!"
            }
          end
          set_device_token_as_unique_for_user(@user,  params[:sign_in_device_type], params[:device_token], params[:device_id])
          @user.update_phone_info(params[:phone_model], params[:os], params[:appkey])
          Delayed::Job.enqueue(MigrationJob.new(@user, nil, email, @chain.php_chain_id, password))
          return false
        else
          return true
        end
      else
        return true
      end
    rescue
      return true
    end

  end

  protected

  def build_user_hash_from_php_object(php_user)
    new_user = {}
    return new_user if php_user.blank?
    new_user["email"] = php_user.email
    new_user["username"] = php_user.username
    new_user["first_name"] = php_user.first_name
    new_user["last_name"] = php_user.last_name
    new_user["points"] = php_user.credit_points
    new_user["created_at"] = php_user.signup_date
    new_user["updated_at"] = php_user.signup_date
    new_user["register_type"] = php_user.fb_user_id.to_i.eql?(0) ? 1 : 2
    new_user
  end
  
end
