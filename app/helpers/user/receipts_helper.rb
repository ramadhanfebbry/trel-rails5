module User::ReceiptsHelper
	def status_name(status)
		case status
			when ReceiptStatus::APPROVED
				'Approved'
			when ReceiptStatus::REJECTED
				'Rejected'
			else
				'Received'
		end
	end
end
