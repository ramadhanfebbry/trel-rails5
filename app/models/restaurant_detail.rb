class RestaurantDetail < ActiveRecord::Base

  # attr_accessible :ts_pay_support, :restaurant_id, :backups, :online_order_partner, :status, :olo_order_id, :external_location_store_id

  belongs_to :restaurant

  ONLINE_ORDER_PARTNER_TYPES = {
    "DEFAULT" => 0,
    "AOO" => 1,
    "OLO" => 2,
    "NUORDER" => 3,
    "ONOSYS" => 4
  }

  def apikey
    self.olo_order_id
  end

  def chain_id
    Restaurant.find(self.restaurant_id).chain_id
  end

  def chain
    Chain.find(self.chain_id)
  end
end
