class UserIncentive < ActiveRecord::Base
  belongs_to :incentive, :polymorphic => true
  belongs_to :user_activity, :polymorphic => true
  belongs_to :user
  belongs_to :deal

  def self.add_incentive_to_user(incentive, user, user_act, deal)
    user_incentive = UserIncentive.new
    user_incentive.incentive = incentive
    user_incentive.user_id = user.id
    user_incentive.deal_id = deal.id
    user_incentive.user_activity = user_act
    user_incentive.save
  end

  scope :incentive_points_for_user, -> {|user|
    joins("LEFT OUTER JOIN deal_offers ON user_incentives.incentive_id = deal_offers.id AND user_incentives.incentive_type = 'DealOffer'").
      where("user_incentives.incentive_type = 'DealOffer' AND deal_offers.kind = 1 AND user_incentives.user_id = #{user.id}")
  }

  scope :incentive_offline_reward_for_user, -> {|user|
    joins("LEFT OUTER JOIN deal_offers ON user_incentives.incentive_id = deal_offers.id AND user_incentives.incentive_type = 'DealOffer'").
      where("user_incentives.incentive_type = 'DealOffer' AND deal_offers.kind = 2 AND user_incentives.user_id = #{user.id}")
  }

  ## get deal even with the deleted one

  def get_deal    
      dl = self.deal    
      dl = Deal.deleted.find(self.deal_id) if dl.blank?
      return dl    
  end
end
