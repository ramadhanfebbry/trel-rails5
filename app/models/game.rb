class Game < ActiveRecord::Base

  has_many :levels
  belongs_to :chain

  validates :chain_id, :name, :presence => true
  validates :daily_cap, :weekly_cap, :monthly_cap, :numericality => {:greater_than => -1, :message => "Must be a number greater than or equal 0"}
end