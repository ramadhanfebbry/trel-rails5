class BridgWebhook < ActiveRecord::Base
  serialize :content
  belongs_to :chain

  scope :search_chain, -> (chain_id) {  where(chain_id: chain_id) if chain_id.present? }

end
