class SurveysUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :survey
  has_many :answers, :dependent => :destroy
  belongs_to :receipt
  belongs_to :offer
  belongs_to :reward
  belongs_to :restaurant

  has_one :user_deal_survey
  has_many :guest_relations, :dependent => :destroy

  after_create :update_count_restaurant_survey,:update_avg_restaurant_survey  , :set_restaurant_id, :show_survey_or_no

  default_scope { where(:deleted_at => nil) }

#  after_create :update_scores

  scope :date_range, -> (date1, date2) {
    unless date1.blank? and date2.blank?
      where("date(surveys_users.created_at) between date(?) and date(?)", date1, date2)
    end
  }

  scope :only_show, -> { where("show_dashboard = ?", true) }

  scope :by_chain, -> (chain_id) { 
    unless chain_id.blank?
      where("surveys.chain_id in(?)", chain_id).joins(:survey)
    end
  }

  scope :by_restaurant, -> (restaurant_id) { 
    unless restaurant_id.blank?
      where("restaurants.id in(?)", restaurant_id).joins(:survey => {:chain => :restaurants})
    end
  }

  scope :by_receipt_status, -> (rec_status) { 
    unless rec_status.blank?
      where("receipts.status = ?",rec_status.to_i).joins(:receipt)
    end
  }

  scope :by_query_string, -> (query_string) { 
    if !query_string.blank? and query_string.to_i > 0 ## if search integer value
      where("users.email like ? or surveys_users.survey_id = ? or surveys_users.reward_id = ?","%"+query_string+"%",query_string.to_i, query_string.to_i).joins(:user)
    elsif !query_string.blank? and query_string.to_i == 0 ## if search string value
      where("users.email like ?","%"+query_string+"%").joins(:user)
    end
  }

  transaction_offer_join = "INNER JOIN restaurant_offers ON receipt_transactions.restaurant_offer_id=restaurant_offers.id"
  scope :by_survey_and_restaurant, -> (survey, restaurant){
    SurveysUser.where("surveys_users.survey_id = ? and restaurant_offers.restaurant_id = ?", survey.id,restaurant.id).joins([:receipt => :receipt_transactions] ).joins(transaction_offer_join)
  }

  scope :by_offer, -> (offer_ids){
    unless offer_ids.blank?
      SurveysUser.where("restaurant_offers.offer_id in (?)",offer_ids).joins([:receipt => :receipt_transactions] ).joins(transaction_offer_join)
    end
  }

  scope :by_rewards, -> (rew_id){
    unless rew_id.blank?
      where("reward_id in (?)", rew_id)
    end
  }

  def self.search(params)
    date_range(params[:from],params[:to]).
      by_chain(params[:chains]).
      by_restaurant(params[:restaurants]).
      by_receipt_status(params[:payment_method]).
      by_query_string(params[:q]).
      by_rewards(params["rewards"]).by_offer(params[:offers])
  end

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def show_survey_or_no
    ## if there are a survey with only 1 type and its comment type. and the answers is blank, show = false
    ## else true
    question_type_array = self.survey.questions.where("rotating_group_id is null")
    cnt = 0
    question_type_array.each do |x|
      if x.question_type == Question::TYPES["Comments"]
        cnt = cnt + 1
      end
    end

    if cnt == question_type_array.size
      vals = self.answers.where("question_id in(?)",question_type_array).map(&:value)
      vals.delete_if{|x| x == ""}
      if vals.blank?
        self.update_column(:show_dashboard, false)
      else
        self.update_column(:show_dashboard, true)
      end
    end
    #end
  end

  def update_scores
    begin
      value_ids = self.answers.multiple_choices.map(&:value_id)
      selected_value_ids = []
      #filter questioh choice with value integer only for sum
      value_ids.each do |value_id|
        qc_value = QuestionChoice.find(value_id).value rescue ""
        selected_value_ids << value_id if qc_value.is_numeric?
      end
      sum = QuestionChoice.by_ids(selected_value_ids).sum('cast(value as integer)')
      avg = sum.to_f / value_ids.size.to_f
      p "-----"
      p avg
      self.update_attribute(:scores, avg)
    rescue

    end
  end

  def deal_obj
    sur = SubmitSurvey.find_by_survey_id(self.survey_id)
    deal = sur.deal
    return deal
  end

  def show_answers(ans)
    data = {:all => 0, :q1 => 0,:q2 => 0 , :q3 => 0, :q4 => 0, :q5 => 0}

    data[:all] = self.scores#calculate_avg_answer(ans.map{|x| x.scores })
    data[:q1] = handle_null_string(ans[0].value) rescue ""
    data[:q2] = handle_null_string(ans[1].value) rescue ""
    data[:q3] = handle_null_string(ans[2].value) rescue ""
    data[:q4] = handle_null_string(ans[3].value) rescue ""
    data[:q5] = handle_null_string(ans[4].value) rescue ""

    return data
  end

  def show_answers_new(question_ids,ans)
      data = {:all => "", :q1 => "",:q2 => "" , :q3 => "", :q4 => "", :q5 => ""}

      data[:all] = self.scores#calculate_avg_answer(ans.map{|x| x.scores })
      data[:all] = "" if Survey.find(self.survey_id).having_averages? == false

      q1 = ans.select{|x| x if x.question_id.to_i == question_ids[0].to_i} rescue nil
    #puts q1
      data[:q1] = handle_null_string(q1)  rescue ""

      q2 = ans.select{|x| x if x.question_id.to_i == question_ids[1].to_i} rescue nil
    #puts q2
      data[:q2] = handle_null_string(q2)  rescue ""

      q3 = ans.select{|x| x if x.question_id.to_i == question_ids[2].to_i} rescue nil
    #puts q3
      data[:q3] = handle_null_string(q3)  rescue ""

      q4 = ans.select{|x| x if x.question_id.to_i == question_ids[3].to_i} rescue nil
    #puts q4
      data[:q4] = handle_null_string(q4)  rescue ""

      q5 = ans.select{|x| x if x.question_id.to_i == question_ids[4].to_i} rescue nil
    #puts q5
      data[:q5] = handle_null_string(q5)  rescue ""

      return data
  end

  #def show_answers_new(question_ids,ans)
  #  data = {:all => "", :q1 => "",:q2 => "" , :q3 => "", :q4 => "", :q5 => ""}
  #
  #  data[:all] = self.scores#calculate_avg_answer(ans.map{|x| x.scores })
  #  data[:q1] = handle_null_string(ans[0].value) if question_ids[0] == ans[0].question_id #rescue ""
  #  puts "data q1 = #{data[:q1]}"
  #  data[:q2] = handle_null_string(ans[1].value) if question_ids[1] == ans[1].question_id #rescue ""
  #  puts "data q2 = #{data[:q2]}"
  #  data[:q3] = handle_null_string(ans[2].value) if question_ids[2] == ans[2].question_id rescue ""
  #  data[:q4] = handle_null_string(ans[3].value) if question_ids[3] == ans[3].question_id rescue ""
  #  data[:q5] = handle_null_string(ans[4].value) if question_ids[4] == ans[4].question_id rescue ""
  #
  #  return data
  #end

  def handle_null_string(ans)
    tmp= ans.first

    ## if not double answer
    if tmp.answer_type != Answer::TYPES["Multiple Dropdown"]
      ans = ans.first
      value = ans.value
      if value.to_s.include?("null")
        return ""
      else
        return value.to_s
      end

    else
      puts ans
      ret_value = []
      ans.each_with_index do |answ_collection,i|
        value =  answ_collection.value

        if value.to_s.include?("null")
          ret_value << ""
        else
          ret_value << "Answer #{i+1}: "+value.to_s
        end
      end
      return ret_value.join('<br/>')
    end
  end

  def collect_values(ans)

  end

  def calculate_avg_answer(values)
    average = 0.0
    begin
    unless values.blank?
      values = values.flatten.compact.delete_if{|x| x.class == String}
      values = values.flatten.compact.delete_if{|x| x.to_i == 0}
      average = values.sum / values.size.to_f
      average = 0.0 if average.nan? || average.infinite?
    end
    rescue
    end

    return average
  end

  def self.first_time_update_count_restaurant_survey
    RestaurantSurveyCount.delete_all
    SurveysUser.all.each do |r|
      begin
      restaurant_id = r.receipt.restaurant
      x = RestaurantSurveyCount.find_or_create_by_survey_id_and_restaurant_id(r.survey.id, restaurant_id)
      x.total = 1 if x.total.nil?
      x.update_attribute(:total, x.total + 1)
      rescue => e
        puts "#{e.inspect}"
      end
    end
  end

  def first_time_update_avg_restaurant_survey
    RestaurantSurveyAvg.delete_all
    SurveysUser.all.each do |r|
      begin
      restaurant_id = r.receipt.restaurant
      total = calculate_avg(r.survey,restaurant_id)
      average = RestaurantSurveyAvg.find_or_create_by_survey_id_and_restaurant_id(r.survey_id, restaurant_id)
      average.update_attributes(
          :averages => total[:all], :q1 => total[:q1], :q2 => total[:q2], :q3 => total[:q3],
          :q4 => total[:q4],
          :q5 => total[:q5]
      )
      rescue => e
        puts "error #{e.inspect}"
      end
    end
  end

  def self.first_time_update_count_by_survey(survey_id)
    RestaurantSurveyCount.where(:survey_id => survey_id).delete_all
    SurveysUser.where(:survey_id => survey_id).each do |r|
      begin
      restaurant_id = r.receipt.restaurant
      x = RestaurantSurveyCount.find_or_create_by_survey_id_and_restaurant_id(r.survey.id, restaurant_id)
      x.total = 1 if x.total.nil?
      x.update_attribute(:total, x.total + 1)
      rescue => e
        puts "#{e.inspect}"
      end
    end
  end

  def first_time_update_avg_by_survey(survey_id)
    RestaurantSurveyAvg.where(:survey_id => survey_id).delete_all
    SurveysUser.where(:survey_id => survey_id).each do |r|
      begin
      restaurant_id = r.receipt.restaurant
      total = calculate_avg(r.survey,restaurant_id)
      average = RestaurantSurveyAvg.find_or_create_by_survey_id_and_restaurant_id(r.survey_id, restaurant_id)
      average.update_attributes(
          :averages => total[:all], :q1 => total[:q1], :q2 => total[:q2], :q3 => total[:q3],
          :q4 => total[:q4],
          :q5 => total[:q5]
      )
      rescue => e
        puts "error #{e.inspect}"
      end
    end
  end

 def update_count_restaurant_survey
    #if SubmitSurvey.find_by_survey_id(self.survey_id).blank? ## if its not deal survey
    begin
      restaurant_id = self.receipt.restaurant
      count = RestaurantSurveyCount.where("restaurant_id = ? and survey_id = ?", restaurant_id, self.survey_id).first
      count = RestaurantSurveyCount.find_or_create_by_survey_id_and_restaurant_id(self.survey_id,restaurant_id) if count.blank?
      total = count.total.nil?? 0 : count.total
      count.update_attribute(:total, total + 1)
    rescue => e
      puts "Error:SurveyUsers:update_count_restaurant_survey => #{e.inspect}"
    end
  end

  ## update the avg of restaurant survey
  def update_avg_restaurant_survey
    begin
      restaurant_id = self.receipt.restaurant
      cnt = RestaurantSurveyCount.where("restaurant_id = ? and survey_id = ?", restaurant_id, survey_id).first
      cnt = RestaurantSurveyCount.find_or_create_by_survey_id_and_restaurant_id(survey_id,restaurant_id) if cnt.blank?
      min_amount = 500

      if cnt.total < min_amount || cnt.total % min_amount == 0 || RestaurantSurveyAvg.where(:restaurant_id =>restaurant_id, :survey_id => survey_id).blank?
        ## update the survey average per restaurant if the survey submit is below 100 or multiple 100
        total = calculate_avg(Survey.find(survey_id),restaurant_id)
        average = RestaurantSurveyAvg.find_or_create_by_survey_id_and_restaurant_id(survey_id, restaurant_id)
        ## update avg with the new total average
        cnt_total = cnt.total

        if cnt_total > 500
        avg_all = (((cnt_total - min_amount) * average[:all].to_f) + (total[:all] * min_amount)) / cnt_total
        avg1 = (((cnt_total - min_amount) * average[:q1].to_f) + (total[:q1] * min_amount)) / cnt_total
        avg2 = (((cnt_total - min_amount) * average[:q2].to_f) + (total[:q2] * min_amount)) / cnt_total
        avg3 = (((cnt_total - min_amount) * average[:q3].to_f) + (total[:q3] * min_amount)) / cnt_total
        avg4 = (((cnt_total - min_amount) * average[:q4].to_f) + (total[:q4] * min_amount)) / cnt_total
        avg5 = (((cnt_total - min_amount) * average[:q5].to_f) + (total[:q5] * min_amount)) / cnt_total

        avg_all = calculate_total([avg1,avg2,avg3,avg4,avg5])


        average.update_attributes(
          :averages => avg_all, :q1 => avg1,
          :q2 => avg2, :q3 => avg3,
          :q4 => avg4,
          :q5 => avg5
        )
        else
          average.update_attributes(
              :averages => total[:all], :q1 => total[:q1], :q2 => total[:q2], :q3 => total[:q3],
              :q4 => total[:q4],
              :q5 => total[:q5]
          )
        end
        #average.update_attributes(
        #  :averages => total[:all], :q1 => total[:q1], :q2 => total[:q2], :q3 => total[:q3],
        #  :q4 => total[:q4],
        #  :q5 => total[:q5]
        #)
      end
    rescue => e
      puts "Error:SurveyUsers:update_avg_restaurant_survey => #{e.inspect}"
    end
  end

 def calculate_avg(survey, restaurant_id)
    res = Restaurant.find restaurant_id
    #survey_users = SurveysUser.by_survey_and_restaurant(survey, res)#.includes(:answers)
    survey_users = SurveysUser.where(:survey_id => survey.id, :restaurant_id => res.id).limit(500).order("created_at desc")

    value_all = []; value_q1 = [];value_q2 = [];value_q3 = [] ;value_q4 =[]; value_q5 = []
    averages = {:all => 0, :q1 => 0,:q2 => 0 , :q3 => 0, :q4 => 0, :q5 => 0 }
    surveys = [survey_users] if surveys.kind_of?(SurveysUser)
    survey_users.each do |survey_user|
      ans = survey_user.answers.multiple_choices.order("id asc").includes([:question => :question_choices])
      values = ans.map{|x| x.value}
      value_all << values
      value_q1 << values[0]
      value_q2 << values[1]
      value_q3 << values[2]
      value_q4 << values[3]
      value_q4 << values[4]
    end
    averages[:all] = calculate_total(value_all)
    averages[:q1] = calculate_total(value_q1)
    averages[:q2] = calculate_total(value_q2)
    averages[:q3] = calculate_total(value_q3)
    averages[:q4] = calculate_total(value_q4)
    averages[:q5] = calculate_total(value_q5)
    return averages
  end

 def calculate_total(values)
    average = 0.0
    unless values.blank?
      values = values.flatten.compact.delete_if{|x| (x.class == String and x.is_numeric? == false )}
      values = values.flatten.compact.delete_if{|x| x.to_i == 0}
      values = values.map{|x| x.to_f}
      average = values.sum / values.size.to_f
      average = 0.0 if average.nan? || average.infinite?
    end

    return average.round(2)
 end

  def survey_detail_in_table_html_view
    html_view = ""
    answer_lists = Answer.where(:surveys_user_id => self.id)
    html_view += "<table width='50%' cellspacing='0' cellpadding='8' border='1'>"
    html_view += "<tr>"
    html_view += "<th style='background-color:grey;width:40%'>Question</th>"
    html_view += "<th style='background-color:grey;width:60%'>User Response</th>"
    html_view += "</tr>"
    answer_lists.each do |answer|
      html_view += "<tr>"
      html_view += "<td>#{answer.question.text}</td>"
      html_view += "<td>#{answer.value}</td>"
      html_view += "</tr>"
    end
    html_view += "</table>"
    html_view
  end

  # SurveysUser.update_column_restaurant_id
  def self.update_column_restaurant_id
    SurveysUser.all.each do |su|
      begin
      res_id = su.receipt.last_transaction.restaurant_id
      su.update_attributes(:restaurant_id => res_id)
      rescue => e
        puts "SurveyUser:update_column_restaurant_id --- #{e.inspect}"
        next
        end
    end
  end

  def set_restaurant_id
    begin
      res_id = self.receipt.last_transaction.restaurant_id
      self.update_attribute(:restaurant_id, res_id)
    rescue => e
      puts "SURVEYUSERS::set_restaurant_id #{e.inspect}"
    end
  end

  ## dashboard 2.3
  def request_by_owner(owner)
    a = GuestRelation.where(:owner_id => owner.id, :surveys_user_id => self.id).first
    if a.blank?
      a = GuestRelation.where(:chain_id => owner.chain.id, :surveys_user_id => self.id).first
    end
    return a
  end

  def chain_owner_request
    return GuestRelation.where(:surveys_user_id => self.id).first
  end


  def self.survey_count_chain(survey_id,restaurants)
    total = 0
    restaurants.each do |m|
      total = total +   RestaurantSurveyCount.where(:restaurant_id => m,:survey_id => survey_id).first.total rescue 0
    end

    return total
  end
end
