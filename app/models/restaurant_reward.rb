class RestaurantReward < ActiveRecord::Base
  belongs_to :restaurant
  belongs_to :reward
  has_many :reward_transactions
  
  default_scope { where("deleted_at" => nil) }

  scope :get_by_lat_lng , lambda {|lat, lng|
    where("id in (?)", restaurant_ids) 
    
  }
  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

   
end
