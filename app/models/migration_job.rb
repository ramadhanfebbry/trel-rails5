class MigrationJob < Struct.new(:user, :php_user_id, :php_email, :php_chain_id, :password)

  def perform
    UserMailer.reset_password_email(user, password).deliver if user.register_type.to_i.eql?(1)
    if php_user_id
      php_user = PhpUser.where(:userid => php_user_id, :chainName => php_chain_id, :status => 'active').first
      php_user.update_column(:status, "inactive") unless php_user.blank?
    elsif php_email
      p php_email
      p php_chain_id
      conditions = []
      conditions << "email = '#{php_email}' AND chainName = #{php_chain_id} AND status = 'active'"
      conditions << "fb_user_id != 0" if user.register_type.to_i.eql?(2)
      conditions << "fb_user_id = 0" if user.register_type.to_i.eql?(1)
      p conditions.join(" AND ")
      php_user = PhpUser.where(conditions.join(" AND ")).first
      p php_user
      php_user.update_column(:status, "inactive")  unless php_user.blank?
    end
  end

end