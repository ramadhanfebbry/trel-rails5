class Tagging < ActiveRecord::Base
  belongs_to :tag
  belongs_to :restaurant, :foreign_key => 'taggable_id'
end