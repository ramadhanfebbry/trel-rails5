class GiftcardNotificationEmailJob < Struct.new(:chain, :giftcard_notification_email, :card_number, :giftcard_amount, :giftee_email, :giftee_name, :custom_message, :gifter_name, :image_header_url)

  def perform
    p "perform----------GiftcardNotificationEmailJob"
    p "image_header_url #{image_header_url}"
    gc_template = card_number
    if giftcard_notification_email.send_barcode
      require 'barby'
      require 'barby/barcode/code_128'
      require 'barby/outputter/png_outputter'
      card_number_code = Barby::Code128B.new(gc_template)
      barcode = Barby::PngOutputter.new(card_number_code).to_png #Raw PNG data
      File.open('public/barcode.png', 'wb'){|f| f.write Base64.encode64(barcode).gsub("\n", "") }
      File.open('public/barcode.png', 'wb'){|f| f.write barcode }
      file = File.open('public/barcode.png')
      p "file: #{file}"
      giftcard_transaction = GiftcardTransaction.create(
          giftcard_amount: giftcard_amount,
          giftee_email: giftee_email,
          giftee_name: giftee_name,
          custom_message: custom_message,
          gifter_name: gifter_name,
          barcode: file
      )
    end
    barcode_image = giftcard_transaction.barcode rescue nil
    UserMailer.giftcard_notification_email(chain, giftcard_notification_email, giftee_email, giftee_name, custom_message, gifter_name, gc_template, giftcard_amount, image_header_url, barcode_image).deliver!
  end

  def error(job, exception)
    p "---------GiftcardNotificationEmailJob erroor handling-------"
    ChainMailer.giftcard_error_report(chain, exception.to_s).deliver
    p "-----------------finishhhhh GiftcardNotificationEmailJob------"
  end

end