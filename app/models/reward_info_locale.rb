class RewardInfoLocale < ActiveRecord::Base

  belongs_to :reward
  belongs_to :locale

  validates :name, :presence => true

  def self.import_data
    Reward.all.each do |reward|
      chain = reward.chain
      chain.locales.each do |locale|
        reward_info_locale = RewardInfoLocale.where(:reward_id => reward.id, :locale_id => locale.id).first
        if reward_info_locale.blank?
          reward_info_locale = RewardInfoLocale.create(:reward_id => reward.id, :locale_id => locale.id, :name => reward.name, :fineprint => reward.fineprint, :description => reward.description)
        end
      end unless chain.blank?
    end
  end

end
