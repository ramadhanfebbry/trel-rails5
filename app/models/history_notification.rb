class HistoryNotification < ActiveRecord::Base

  belongs_to :plain_push_notification, :foreign_key => 'plain_id'
  belongs_to :user

  STATUS = {
      1 => "Success",
      2 => "Error"
  }

  scope :success, -> { where("status = 1") }
  scope :error, -> { where("status = 2") }

  scope :device, -> { where("plain_kind = 2") }
  scope :email, -> { where("plain_kind = 1") }

  def self.pending(plain_id)
    plain_push_notification = PlainPushNotification.find(plain_id)
    user_ids = plain_push_notification.email[:emails] rescue []
    exist_user_ids = self.where("user_id in (?)", user_ids).map(&:user_id)
    user_ids - exist_user_ids
  end
end
