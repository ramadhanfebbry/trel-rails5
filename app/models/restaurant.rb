require 'geokit'
require 'geokit-rails'
include Geokit::Geocoders
#include DashboardQuery::Member

class Restaurant < ActiveRecord::Base
  include CoordinateFromAddress

  # attr_accessible :tag_list,:zipcode,:name, :app_display_text, :dashboard_display_text,
  #                 :address,:city, :chain_id, :phone_number, :rest_tax, :latitude, :longitude, :city_id,
  #                 :online_order_link, :fishbowl_restaurant_identifier, :ldap_identity, :beacon_uuid,
  #                 :beacon_serial_number, :location_qrcode_identifier, :social_link, :ignored_zipcode, :backups,
  #                 :contact, :status, :external_partner_id, :skip_generate_status, :online_order_support_status,
  #                 :restaurant_hours_attributes
  acts_as_taggable
  #validates , :format => { :with => /A[a-zA-Z]+\z/,
  #  :message => "(Only letters allowed)" }

  attr_accessor :user_coordinate, :min_preset_distance, :from_import , :tag_import, :state_import, :country_import, :city_import, :ignored_zipcode, :ignored_phone, :skip_participate_to_rewards_and_offers, :skip_set_hybrid_reward_redemption, :skip_generate_status

  validates :name, :app_display_text, :dashboard_display_text, :address, :presence => true#, :format => {:with => /\A[-a-zA-Z0-9 ()-]+\z/, :message => "(Only alphanumeric and special(-) characters allowed)"}
  validates_length_of :name, :app_display_text, :address, :presence => true, :maximum => 100, :message => "length should be less then 100 characters"

  validates :zipcode, :presence => true, :format => {:with => /(^\d{3,10}$)|(^\d{5} \d{4}$)/,
                                                     :message => "should be like (12345 in 3-10 digits or 12345 3233)"}, :if => :not_ignored_zipcode?

  validates :city, :presence => { :message => "(City Cannot Be Blank | Import Restaurant, City Must valid with state and country)"}

  validates :chain_id, :latitude, :longitude, :presence => true
  validates :phone_number, :presence => true, :length => {:maximum => 20}, :format => {:with => /\A[-0-9 ()-+]+\z/,
                                                                                       :message => "(Only numbers and special(-+) characters allowed)"}, :if => :not_ignored_phone?
  validates :rest_tax, :numericality => {:greater_than => -0.00001, :message => "must greater than or eq 0"}, :format => {:with => /\A\d+\.*\d{0,5}\z/, :message => "accept decimal value upto 5 decimal value, eg : 1.55000 ,0.01500 ,11.35500"}, :presence => true


  belongs_to :chain
  belongs_to :owner
  belongs_to :city

  has_many :restaurant_offers, :dependent => :destroy
  has_many :restaurant_online_offers, :dependent => :destroy
  has_many :restaurant_rewards
  has_many :partner_menu_items_restaurants
  has_many :restaurant_categories
  has_many :partner_sub_categories, :through => :restaurant_categories
  has_many :restaurant_users, :dependent => :destroy
  has_many :perk_restaurants, :dependent => :destroy
  has_many :perks, :through => :perk_restaurants
  has_many :offers, :through => :restaurant_offers
  has_many :rewards, :through => :restaurant_rewards
  has_many :rewards, :through => :restaurant_categories
  has_many :restaurant_hours, :dependent => :destroy
  has_many :restaurants_owners, :dependent => :destroy
  has_many :owners, :through => :restaurants_owners
  has_many :day_visits, :dependent => :destroy
  has_many :surveys_users
  has_one :db_summary
  has_one :restaurant_detail
  has_many :open_close_restaurant_days , :dependent => :destroy
  has_one :hybrid_reward_redemption_flow

  accepts_nested_attributes_for :restaurant_hours

  TYPES = {
      "OPEN" => 0,
      "CLOSED" => 1,
      "COMING_SOON" => 2,
      "TEMP_CLOSED" => 3,
      "TESTING" => 4
  }

  #before_validation :locate
  after_create :set_default_restaurant_hours, :add_to_chain_owner, :participate_to_rewards_and_offers, :set_hybrid_reward_redemption
  after_save :create_or_update_ldap, :generate_status

  ## remove add to chain owner, every restaurants that are created 1st
  #after_create :set_default_restaurant_hours, :participate_to_rewards_and_offers

  acts_as_mappable :default_units => :miles, # :kms (kilometers), or :nms (nautical miles),
                   :default_formula => :sphere, # :flat
                   :distance_field_name => :distance,
                   :lat_column_name => :latitude,
                   :lng_column_name => :longitude

  default_scope { where("deleted_at" => nil) }
  # scope :active, where("status = ?",true)
  scope :active, -> { where("restaurants.status = ? and restaurant_details.status in(?)",true,[0,2]).joins("INNER JOIN restaurant_details on restaurant_details.restaurant_id = restaurants.id") }
  scope :offer_active, -> { where("restaurants.status = ? and restaurant_details.status in(?)",true,[0,2,4,nil]).joins("INNER JOIN restaurant_details on restaurant_details.restaurant_id = restaurants.id") }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  scope :active_chain, -> { joins(:chain).where("chains.status = 'active'") }
  # scope :active , where("status = ?",true)
  scope :by_ids, lambda {|restaurant_ids| where("id in (?)", restaurant_ids) }


  before_create :set_ldap_identity

  def set_ldap_identity
    self.ldap_identity = self.try(:name).try(:parameterize, "_").try(:classify) if Restaurant.column_names.include?(:ldap_identity)
  end

  def nil_to_blank_string
    self.external_partner_id = self.external_partner_id.blank? ? '' : self.external_partner_id
  end

  def self.total_members(restaurant_ids)
    Receipt.by_restaurant_ids(restaurant_ids).map(&:user_id).uniq.count
  end

  def clear_restaurant_data_and_offers
    puts '---------------WOULDBECLEARED============='
    self.update_attributes(contact: '999999', own_coordinates: false, status: false, region: '999999',
                           ncr_store_id: '999999', online_order_link: '999999', fishbowl_restaurant_identifier: '999999',
                           ncr_aloha_loyalty_store_id: '999999', online_order_support_status: false, aloha_online_id: '999999',
                           ncr_aoo_menu_level: '999999', ncr_aoo_price_level: '999999', delivery_radius_miles: '0', ldap_identity: nil,
                           beacon_serial_number: '999999', beacon_uuid: '999999', location_qrcode_identifier: '999999', external_id: '999999',
                           social_link: '999999', agm_support: false, olo_store_id: '999999', external_partner_id: '999999', ncr_provisioned_rewards: false,
                           ncr_provisioned_loyalty: false)

    # Delete the relations in restaurant_offers
    self.offers.delete_all
  end

  def get_diff_detail_columns
    # Get column names for both restaurant_details and restaurant_detail_backups
    restaurant_detail_columns = RestaurantDetail.column_names
    restaurant_detail_backup_columns = RestaurantDetailBackup.column_names
    restaurant_columns = Restaurant.column_names
    restaurant_backup_columns = RestaurantBackup.column_names

    diff = {
            'restaurant_details' => [],
            'restaurant_detail_backups' => [],
            'restaurants' => [],
            'restaurant_backups' => []
    }

    # RestaurantDetailBackup doesn't have columns
    diff['restaurant_detail_backups'] = restaurant_detail_columns - restaurant_detail_backup_columns
    p "========= restaurant_detail_backups does not have columns : #{diff['restaurant_detail_backups']} =========" unless diff['restaurant_detail_backups'].empty?

    # RestaurantDetail doesn't have columns
    diff['restaurant_details'] = restaurant_detail_backup_columns - restaurant_detail_columns
    p "========= restaurant_details does not have columns : #{diff['restaurant_details']} =========" unless diff['restaurant_details'].empty?

    # RestaurantBackup doesn't have columns
    diff['restaurant_backups'] = restaurant_columns - restaurant_backup_columns
    p "========= restaurant_backups does not have columns : #{diff['restaurant_backups']} =========" unless diff['restaurant_backups'].empty?

    # Restaurant doesn't have columns
    diff['restaurants'] = restaurant_backup_columns - restaurant_columns
    p "========= restaurants does not have columns : #{diff['restaurants']} =========" unless diff['restaurants'].empty?

    diff
  end

  def strict_columns(data)
    diff = get_diff_detail_columns

    p "============== BACKUPDETAILHASH (BEFORE DIFF) : #{data['backup_detail_hash']} =============="


    data['backup_detail_hash'].reject!{|d|diff['restaurant_detail_backups'].include?(d)} unless data['backup_detail_hash'].blank?
    data['backup_hash'].reject!{|d|diff['restaurant_backups'].include?(d)} unless data['backup_hash'].blank?

    p "============== BACKUPDETAILHASH (AFTER DIFF) : #{data['backup_detail_hash']} =============="

    data
  end

  def prepare_data_in_backup_format

    # Prepare a backup hash value for RestaurantDetail
    # since as_json would output string 'null' instead of nil object
    backup_hash = self.as_json rescue nil
    if backup_hash == 'null'
      backup_hash = nil
    else
      backup_hash['status'] = true
    end

    # Remove backup column for being backed up in restaurant
    backup_hash.reject!{|b| b.include?('backups')} unless backup_hash.blank?

    # Prepare a backup hash value for RestaurantDetailBackup
    backup_detail_hash = self.restaurant_detail.as_json rescue nil
    backup_detail_hash = nil if backup_detail_hash == 'null'

    # Remove backup column for being backed up in restaurant detail
    backup_detail_hash.reject!{|b| b.include?('backups')} unless backup_detail_hash.blank?

    # Prepare a backup hash value for offers related with restaurant
    restaurant_offers = self.offers.pluck('offers.id')

    # Returning backup data, backup detail data, and offers related data as hash
    {
        'restaurant_backup_hash' => backup_hash,
        'restaurant_backup_detail_hash' => backup_detail_hash,
        'offers_backup_hash' => restaurant_offers
    }

    # Strict if there was different columns between
    #strict_columns(data_params)
  end

  def perform_change_status(state, detail_state = nil)
    # Preparing backup data for
    # restaurant, restaurant detail, and offers related data
    data = self.prepare_data_in_backup_format

    # Get the backed up data and rollback JSON string to rails
    backups = JSON.parse(self.restaurant_detail.backups) rescue nil

    # If backup data could be find in the database,
    ### then update the data but only if status is true (activated)
    # if backup data couldn't be found in the database,
    ### then create new one
    if !backups.blank?
      if self.status != false
        if self.restaurant_detail
          self.restaurant_detail.update_column('backups', data.to_json)
        else
          RestaurantDetail.create(restaurant_id: self.id, backups: data.to_json) rescue nil
        end
      end
    else
      if self.restaurant_detail
        self.restaurant_detail.update_column('backups', data.to_json)
      else
        RestaurantDetail.create(restaurant_id: self.id, backups: data.to_json) rescue nil
      end
    end

    # Somehow zipcode and phone validation must be ignored
    # this can be done by set this variable
    self.ignored_zipcode = true
    self.ignored_phone = true

    # 'False' value within state variable would cause clearing data
    # and saving the old data to the database
    # 'True' value within state variable would cause
    # restoring / rollback the restaurant value from backup data
    if state == false
      # Clear data and change status to false
      self.clear_restaurant_data_and_offers
    else
      # Rollback from backup data
      unless backups.blank?
        self.update_attributes(backups['restaurant_backup_hash']) unless backups['restaurant_backup_hash'].blank?
        self.restaurant_detail.update_attributes(backups['restaurant_backup_detail_hash']) unless backups['restaurant_backup_detail_hash'].blank?
        status = self.restaurant_detail.try(:status)
        status = self.status ? 0 : 1 if status.blank?
        if [Restaurant::TYPES["COMING_SOON"]].include?(status)
          self.offers.delete_all
        else
          self.offers << Offer.where('id IN(?)', backups['offers_backup_hash']) unless backups['offers_backup_hash'].blank?
        end
      end unless self.status

      # Set restaurant status as activated
      self.update_column(:status, true)
    end

    unless detail_state.blank?
      p " APP DISPLAY TEXT: #{self.app_display_text}"

      if [Restaurant::TYPES["OPEN"], Restaurant::TYPES["CLOSED"], Restaurant::TYPES["TESTING"]].include?(detail_state)
        self.update_column(:app_display_text, "#{self.app_display_text.chomp("- Temporary Closed")}") if self.app_display_text.include?("- Temporary Closed")
        self.update_column(:app_display_text, "#{self.app_display_text.chomp("- Coming Soon")}") if self.app_display_text.include?("- Coming Soon")
      elsif detail_state.eql?(Restaurant::TYPES["COMING_SOON"])
        self.update_column(:app_display_text, "#{self.app_display_text.chomp("- Temporary Closed")}") if self.app_display_text.include?("- Temporary Closed")
        self.update_column(:app_display_text, "#{self.app_display_text}- Coming Soon") unless self.app_display_text.include?("- Coming Soon")
      elsif detail_state.eql?(Restaurant::TYPES["TEMP_CLOSED"])
        self.update_column(:app_display_text, "#{self.app_display_text.chomp("- Coming Soon")}") if self.app_display_text.include?("- Coming Soon")
        self.update_column(:app_display_text, "#{self.app_display_text}- Temporary Closed") unless self.app_display_text.include?("- Temporary Closed")
      end
      restaurant_detail = RestaurantDetail.find_by_restaurant_id(self.id) rescue false
      if restaurant_detail
        restaurant_detail.update_column(:status, detail_state) rescue nil
      else
        RestaurantDetail.create(restaurant_id: self.id, status: detail_state) rescue nil
      end
    end
  end

  def update_change_status_log(changed_from, changed_to)
    restaurant_detail = self.restaurant_detail
    if restaurant_detail
      restaurant_detail.status_changed_from = changed_from
      restaurant_detail.status_changed_to = changed_to
      restaurant_detail.status_changed_date = DateTime.now
      restaurant_detail.save
    end
  end

  def region
    self.city.try(:region)
  end

  def locate
    unless self.own_coordinates
      loc = MultiGeocoder.geocode("#{self.address}, #{self.city.try(:name)}, #{self.region.try(:name)}, #{self.zipcode}")
      if loc.success
        self.latitude = loc.lat
        self.longitude = loc.lng
      end
    end
  end

  def not_ignored_zipcode?
    self.ignored_zipcode.eql?(false) || self.ignored_zipcode.blank?
  end

  def not_ignored_phone?
    self.ignored_phone.eql?(false) || self.ignored_phone.blank?
  end

  def owner_list
    chain_owners = self.chain.owners.where("is_active = ? and role_id = 1",true)
    res_owners = self.owners.where("is_active = ? and role_id = 2",true)
    return chain_owners + res_owners
  end

  def today_open_hour
    today = Date.today
    wday = today.wday
    self.restaurant_hours.select("day_of_week, open_at, close_at").where(:day_of_week => wday).first
  end

  def available_offers
    today = Date.today
    self.offers.where('"effectiveDate" <= ? AND "expiryDate" >= ? ', today, today)
  end

  def restaurant_distance
    return "Unspecified" if self.user_coordinate.blank?
    user_lat_lng = GeoKit::LatLng.new(self.user_coordinate.first, self.user_coordinate.last)
    restaurant_lat_lng = GeoKit::LatLng.new(self.latitude, self.longitude)
    restaurant_lat_lng.distance_from(user_lat_lng, :units=>:miles).round(1)
  end

  def preset_distance_status
    return "Inactive" if self.user_coordinate.blank?
    return "Active" if self.min_preset_distance.to_f.eql?(0.0)
    user_lat_lng = GeoKit::LatLng.new(self.user_coordinate.first, self.user_coordinate.last)
    restaurant_lat_lng = GeoKit::LatLng.new(self.latitude, self.longitude)
    self.min_preset_distance.to_f >= restaurant_lat_lng.distance_from(user_lat_lng, :units=>:miles) ? "Active" : "Inactive"
  end

  def self.col_list
    Restaurant.column_names.collect { |c| "restaurants.#{c}" }.join(",")
  end

  def self.count_total_member(restaurant_list)
    count = 0

    restaurant_list.each do |restaurant|
        count += restaurant.all_total_member
    end
    return count
  end

  def member_last_month
    dt_zone_created_at = "date(TIMEZONE('UTC', created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
    dt_zone_joined_date = "date(TIMEZONE('UTC', joined_date) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"

    return RestaurantUser.where("(#{dt_zone_joined_date} between  date(?) and date(?) and restaurant_id = ?) or
(#{dt_zone_created_at} between  date(?) and date(?) and restaurant_id = ?)",
           (Time.zone.now - 1.month).beginning_of_month, (Time.zone.now - 1.month).end_of_month,self.id,(Time.zone.now - 1.month).beginning_of_month, (Time.zone.now - 1.month).end_of_month,self.id).count
  end

  def member_this_month
    #receipts = Receipt.restaurants(self.id).
    #where("date(receipts.created_at) between  date(?) and date(?)",
    #      Time.zone.now - 1.month, Time.zone.now)
    #user_ids = receipts.map(&:user_id)

    #return User.where("id in (?) ", user_ids).count
    dt_zone_created_at = "date(TIMEZONE('UTC', created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
    dt_zone_joined_date = "date(TIMEZONE('UTC', joined_date) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"

    return RestaurantUser.where("(#{dt_zone_joined_date} between  date(?) and date(?) and restaurant_id = ?) or
(#{dt_zone_created_at} between  date(?) and date(?) and restaurant_id = ?)",
                                Time.zone.now.beginning_of_month, Time.zone.now,self.id,Time.zone.now.beginning_of_month, Time.zone.now,self.id).count
  end

  def list_users
    Receipt.restaurants(self.id).map(&:user_id)
  end

  def list_user_rewards
    reward_ids = self.rewards.map(&:id)
    reward_wallet = RewardWallet.by_reward_ids(reward_ids).is_claimed.select("distinct(user_id)")

    reward_wallet.map(&:user_id)
  end

  def all_user_ids
    users = list_users | list_user_rewards
    users.compact.flatten.uniq
  end

  def total_members
    Receipt.restaurants(self.id).map(&:user_id).uniq.count
  end

  def avg_coming
    ## using new tables

    return AvgVisit.where("restaurant_id = ? and average > 0 and average < 100", self.id).
        select("30/ avg(average) as avg").first.avg.to_f.round(2)
  end

  def avg_surveys
    receipt_ids = Receipt.by_restaurant_ids([self.id]).map(&:id)
    SurveysUser.where("receipt_id in (?)",receipt_ids).average('scores')
  end

  def calculate_average_user(avg_user, user)

    avg_user = avg_user.uniq
    res = 0
    array_created_at  = []
    avg_array = []
    avg_result = 1
    avg_user.each{|x|array_created_at << x[user].to_date if x[user]}
    avg_array = array_created_at.each_cons(2).collect { |a,b| (b-a).to_i }
    avg_result = avg_array.inject{ |sum, el| sum + el }.to_f / avg_array.size

    res = avg_result
    res = 0.0 if res.blank? || res.to_f.nan? || res.to_f.infinite?
    return res
  end

  def self.completed_survey_restaurants(survey, restaurants)
    # total = Rails.cache.fetch("completed_restaurant_#{restaurants.map(&:id)}",
    #                   :expires_in => 6.hours) do
    #  SurveysUser.select("count(surveys_users.id)").
    #     where("survey_id = ? and restaurant_id in(?)",survey.id, restaurants.map(&:id)).first.count.to_i
    # end
    # return total
    total = REDIS.get "survey_total_chain_#{survey.id}_#{restaurants.map(&:id)}"
    if total.blank?
      total = SurveysUser.select("count(surveys_users.id)").
               where("survey_id = ? and restaurant_id in(?)",survey.id, restaurants.map(&:id)).first.count.to_i
      REDIS.set "survey_total_chain_#{survey.id}_#{restaurants.map(&:id)}", total
    end
    return total
  end

  def completed_survey(survey)
    # total = SurveysUser.
    #     select("count(surveys_users.id)").
    #     where("survey_id = ? and restaurant_id = ?", survey.id, self.id).first.count.to_i
    # return total

    # total = Rails.cache.fetch("completed_survey_per_restaurant_#{survey.id}_#{self.id}",
    #                   :expires_in => 6.hours) do
    #    SurveysUser.
    #             select("count(surveys_users.id)").
    #             where("survey_id = ? and restaurant_id = ?", survey.id, self.id).first.count.to_i
    #   #100
    # end
    # return total

    # using redis

    return REDIS.get "survey_total_#{survey.id}_#{self.id}"
  end

  def survey_avg(survey)
    survey_users = SurveysUser.by_survey_and_restaurant(survey, self).includes(:answers).limit(100).order("surveys_users.created_at desc")
    calculate_all_avg_answer(survey_users)
  end

  def active_members_chart(filter, show_interval, chain_obj)
    return DashboardQuery::ActiveMember.active_members_generator(self,filter, show_interval, chain_obj)
  end



  def all_total_member
    #user_ids = self.chain.users.where("active = ?", true).select("users.id") ## get all user_ids
    #
    #uniq_user = Receipt.where(
    #    "user_id in(?) and receipt_transactions.restaurant_id = ?",
    #     user_ids,self.id
    #    ).select("count(DISTINCT(user_id)) as count_all_user_id").
    #  joins(:receipt_transactions)
    #return uniq_user.first.count_all_user_id.to_i

    #return RestaurantUser.restaurant_members(self).count
    self.db_summary.total_member rescue 0
  end

  def today_user_receipt
    user_ids = self.chain.users.where("active = ?", true).map(&:id).uniq ## get all user_ids

    x = Receipt.where("user_id in(?) and receipt_transactions.restaurant_id = ? and date(users.created_at) = ? ", user_ids,self.id,Date.today).select("count(DISTINCT(user_id)) as count_all_user_id").
        joins(:user,:receipt_transactions)

    return x.first.count_all_user_id.to_i
  end

  def self.member_chart_restaurant_user(filter, chains, restaurants, show_interval, chain_obj, user_ids = nil)
    a = Time.now
      puts "member chart start @ #{a}"
      return self.member_day_chart(restaurants.first.id) if show_interval == "day"
      tz_now = Time.zone.now

      time_zone = Time.zone.now.strftime("%Z")
      date_zone = "date(TIMEZONE('UTC',restaurant_users.created_at AT TIME ZONE '#{time_zone}'))"


      if filter.blank?
        if show_interval == "month"
          filter = "#{date_zone} between date('#{tz_now - 30.days}') and date('#{tz_now}')"
        elsif show_interval == "all"
          filter = "date(restaurant_users.created_at) is not null"
        elsif show_interval == "3month"
          filter = "#{date_zone} between date('#{tz_now - 90.days}') and date('#{tz_now}')"
        else
          filter = "#{date_zone} between date('#{tz_now - 7.days}') and date('#{tz_now}')"
        end
      elsif show_interval == "all"
        filter = "date(restaurant_users.created_at) is not null"
      end

      results = RestaurantUser.where(filter).group("restaurant_id ,date(restaurant_users.created_at)").
          where("restaurant_id = ? and users.active = ?", restaurants.first.id,true).order("date(restaurant_users.created_at) asc").joins(:user).count
    puts "member chart end @ #{Time.now - a}"
      count_before = self.count_before_member(show_interval, restaurants.first.id)# - 1
      #puts "RESTAURANT::MEMBER_CHART count_before_member #{count_before}"
      count_before = 0 if count_before.blank?
     # count_before = 0
      count = 0
      tmp_result = []

      #puts "RESTAURANT::MEMBER_CHART result hash #{results}"
      results.each do |rs|
        count = count_before + rs[1]
        #puts "RESTAURANT::MEMBER_CHART count =  #{count}"
          unless rs[0].blank?
            tmp_result <<  [rs[0].to_datetime.to_i  * 1000, count]
          end
        count_before = count
      end
      #puts "RESTAURANT::MEMBER_CHART TMP_RESULT #{tmp_result}"
    puts "member chart end bgt @ #{Time.now - a}"
      return tmp_result
  end

  def self.count_before_member(interval, restaurant_id)
    time_zone = Time.zone.now.strftime("%Z")
    date_zone = "date(TIMEZONE('UTC',restaurant_users.created_at AT TIME ZONE '#{time_zone}'))"
    if interval == "day"
      RestaurantUser.joins(:user).where("restaurant_id = ? and #{date_zone} < date('#{Time.zone.now.to_date}') and users.active is true", restaurant_id).
          count
    elsif interval == "week" || interval.blank?
      RestaurantUser.joins(:user).where("restaurant_id = ? and #{date_zone} < date('#{Time.zone.now - 7.days}') and users.active is true",restaurant_id).
          count
    elsif interval == "month"
      RestaurantUser.joins(:user).where("restaurant_id = ? and #{date_zone} < date('#{Time.zone.now - 30.days}') and users.active is true",restaurant_id).
          count
    elsif interval == "3month"
      RestaurantUser.joins(:user).where("restaurant_id = ? and #{date_zone} < date('#{Time.zone.now - 90.days}') and users.active is true",restaurant_id).
          count
    elsif interval == "all"
      0
    end
  end


  def self.member_day_chart(restaurant_id)
    time_zone = Time.zone.now.strftime("%Z")
    date_zone = "date(TIMEZONE('UTC',created_at AT TIME ZONE '#{time_zone}'))"
    count_before = RestaurantUser.where("restaurant_id = ? and #{date_zone} < date(now())",restaurant_id).count

    filter = "#{date_zone} = date('#{Time.zone.now.to_date}')"

    results = RestaurantUser.where(filter).group("restaurant_id ,created_at").
        where("restaurant_id = ?", restaurant_id).order("created_at asc").count
    count_before = self.count_before_member('day', restaurant_id)
    tmp_result = []

    puts "RESTAURANT::MEMBER_CHART results =  #{results}"
    results.each do |rs|
      count = count_before + rs[1]
      puts "RESTAURANT::MEMBER_CHART count =  #{count}"
      unless rs[0].blank?
        tmp_result <<  [rs[0].to_datetime.to_i  * 1000, count]
      end
      count_before = count
    end
    puts "RESTAURANT::MEMBER_CHART tmp_result =  #{tmp_result}"
    tmp_result
  end

  def calculate_avg_answer_by_date_and_survey(survey,survey_users,datetime,question_step)
    date_zone = "date(TIMEZONE('UTC', surveys_users.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"

    survey_users = SurveysUser.unscoped.includes(:answers).by_survey_and_restaurant(survey, self).where("#{date_zone} = date(?)", datetime.to_date)
    value_all = []; value_q1 = [];value_q2 = [];value_q3 = [] ;value_q4 =[]; value_q5 = []
    averages = {:all => 0, :q1 => 0,:q2 => 0 , :q3 => 0, :q4 => 0, :q5 => 0 }
    survey_users = [survey_users] if survey_users.kind_of?(SurveysUser)
    survey_users.each do |survey_user|
      ans = survey_user.answers
      values = ans.map{|x| x.value}
      value_all << values if question_step == 0
      value_q1 << values[0] if question_step == 1
      value_q2 << values[1] if question_step == 2
      value_q3 << values[2] if question_step == 3
      value_q4 << values[3] if question_step == 4
      value_q4 << values[4] if question_step == 5
    end
    dt_time = datetime.to_i * 1000
    averages[:all] = calculate_answer_avg(dt_time,value_all) if question_step == 0
    averages[:q1] =  calculate_answer_avg(dt_time,value_q1) if question_step == 1
    averages[:q2] =  calculate_answer_avg(dt_time,value_q2) if question_step == 2
    averages[:q3] =  calculate_answer_avg(dt_time,value_q3) if question_step == 3
    averages[:q4] =  calculate_answer_avg(dt_time,value_q4) if question_step == 4
    averages[:q5] =  calculate_answer_avg(dt_time,value_q5) if question_step == 5
    return averages
  end

  def calculate_avg_per_day(survey,survey_users,datetime,question_step)
    date_zone = "TIMEZONE('UTC', surveys_users.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}'"

    survey_users = SurveysUser.unscoped.includes(:answers).by_survey_and_restaurant(survey, self).
        where("#{date_zone} = ?",datetime.to_date)
    survey_users = survey_users.uniq

    value_all = []; value_q1 = [];value_q2 = [];value_q3 = [] ;value_q4 =[]; value_q5 = []
    averages = {:all => 0, :q1 => 0,:q2 => 0 , :q3 => 0, :q4 => 0, :q5 => 0 }
    survey_users = [survey_users] if survey_users.kind_of?(SurveysUser)
    survey_users.each do |survey_user|
      ans = survey_user.answers
      values = ans.map{|x| x.value}
      value_all << values if question_step == 0
      value_q1 << values[0] if question_step == 1
      value_q2 << values[1] if question_step == 2
      value_q3 << values[2] if question_step == 3
      value_q4 << values[3] if question_step == 4
      value_q4 << values[4] if question_step == 5
    end
    dt_time = datetime.to_i * 1000


    averages[:all] = calculate_answer_avg(dt_time,value_all) if question_step == 0
    averages[:q1] =  calculate_answer_avg(dt_time,value_q1) if question_step == 1
    averages[:q2] =  calculate_answer_avg(dt_time,value_q2) if question_step == 2
    averages[:q3] =  calculate_answer_avg(dt_time,value_q3) if question_step == 3
    averages[:q4] =  calculate_answer_avg(dt_time,value_q4) if question_step == 4
    averages[:q5] =  calculate_answer_avg(dt_time,value_q5) if question_step == 5
    return averages
  end

  def calculate_answer_avg(dt_time,values)
    average = 0.0
    tmp_value = []
    unless values.blank?
      values = values.flatten.compact.delete_if{|x| x.class == String ||  x.to_i == 0}
      values.each{|x| tmp_value << x.to_f}
      average = tmp_value.sum / tmp_value.size.to_f
      average = 0.0 if average.nan? || average.infinite?
    end
    if average.to_i > 0
      return [dt_time,average.round(2)]
    else
      return [nil,nil]
    end
  end

  ## return average survey per restaurant

  def survey_averages_restaurant(survey)
    avg = {:all => "", :q1 => "", :q2 => "", :q3 => "", :q4 => "", :q5 => ""}
    average = RestaurantSurveyAvg.find_by_restaurant_id_and_survey_id(self.id, survey.id)
    begin
      avg =  {:all => average.averages, :q1 => average.q1, :q2 => average.q2, :q3 => average.q3, :q4 => average.q4, :q5 => average.q5}
    rescue
    end
    return  avg
  end

  def self.top5_survey(restaurants,survey)
    return RestaurantSurveyAvg.select('avg(averages) as averages, restaurants.dashboard_display_text').
        where("survey_id = ? and restaurant_id in (?)",survey.id, restaurants.map(&:id)).
        group('restaurants.dashboard_display_text').order('averages desc').joins(:restaurant).limit(5)
  end

  def self.bottom_top5_survey(restaurants,survey)
    return RestaurantSurveyAvg.select('avg(averages) as averages, restaurants.dashboard_display_text').
        where("survey_id = ? and restaurant_id in (?)",survey.id, restaurants.map(&:id)).
        group('restaurants.dashboard_display_text').order('averages asc').joins(:restaurant).limit(5)
  end

  def self.survey_averages(restaurants,survey)
    res = Rails.cache.fetch("survey_#{survey.id}_averages#{restaurants.map(&:id)}",
                      :expires_in => 6.hours) do
     RestaurantSurveyAvg.select('avg(averages) as avg').
        where("survey_id = ? and restaurant_id in (?)",survey.id, restaurants.map(&:id)).
        first.avg.to_f
    end
    return res
  end

  def self.survey_averages_q1(restaurants,survey)
    res = Rails.cache.fetch("survey_#{survey.id}_averages_q1#{restaurants.map(&:id)}",
                            :expires_in => 6.hours) do
     RestaurantSurveyAvg.select('avg(q1) as avg').
        where("survey_id = ? and restaurant_id in (?)",survey.id, restaurants.map(&:id)).
        first.avg.to_f
    end
    return res
  end

  def self.survey_averages_q2(restaurants,survey)
    res = Rails.cache.fetch("survey_#{survey.id}_averages_q2_#{restaurants.map(&:id)}",
                            :expires_in => 6.hours) do
       RestaurantSurveyAvg.select('avg(q2) as avg').
        where("survey_id = ? and restaurant_id in (?)",survey.id, restaurants.map(&:id)).
        first.avg.to_f
    end
    return res
  end

  def self.survey_averages_q3(restaurants,survey)
    res = Rails.cache.fetch("survey_#{survey.id}_averages_q3_#{restaurants.map(&:id)}",
                            :expires_in => 6.hours) do
     RestaurantSurveyAvg.select('avg(q3) as avg').
        where("survey_id = ? and restaurant_id in (?)",survey.id, restaurants.map(&:id)).
        first.avg.to_f
    end
    return res
  end

  def self.survey_averages_q4(restaurants,survey)
    res = Rails.cache.fetch("survey_#{survey.id}_averages_q4_#{restaurants.map(&:id)}",
                            :expires_in => 6.hours) do
     RestaurantSurveyAvg.select('avg(q4) as avg').
        where("survey_id = ? and restaurant_id in (?)",survey.id, restaurants.map(&:id)).
        first.avg.to_f
    end
    return res
  end

  def self.survey_averages_q5(restaurants,survey)
    res = Rails.cache.fetch("survey_#{survey.id}_averages_q5_#{restaurants.map(&:id)}",
                            :expires_in => 6.hours) do
     RestaurantSurveyAvg.select('avg(q5) as avg').
        where("survey_id = ? and restaurant_id in (?)",survey.id, restaurants.map(&:id)).
        first.avg.to_f
    end
    return res
  end

  ## return average survey several surveys
  def survey_averages_restaurants(surveys)
    avg = {:all => 0, :q1 => 0, :q2 => 0, :q3 => 0, :q4 => 0, :q5 => 0}
    average = RestaurantSurveyAvg.where("restaurant_id = ? and survey_id in (?)", self.id, surveys.map(&:id)).select('avg(averages) as avg')
    begin
      avg =  {:all => average.first.avg.to_f, :q1 => 0, :q2 => 0, :q3 => 0, :q4 => 0, :q5 => 0}
    rescue
    end
    return  avg
  end

  def survey_avg_chart(filter,survey, show_interval)
    val = if filter.blank?
            SurveysUser.unscoped.by_survey_and_restaurant(survey, self).where("date(surveys_users.created_at) between date(?) and date(?)", Time.zone.now - 7.days, Time.zone.now).group("date(surveys_users.created_at)").select("count(surveys_users.id),date(surveys_users.created_at) as date").order("date(surveys_users.created_at) asc")
          else
            filter = nil if filter == "all"
            SurveysUser.unscoped.by_survey_and_restaurant(survey, self).where(filter).group("date(surveys_users.created_at)").select("count(surveys_users.id),date(surveys_users.created_at) as date").order("date(surveys_users.created_at) asc")
          end

    res =  val.map{|x| calculate_avg_answer_by_date_and_survey(survey,x,x.date.to_datetime,0)[:all]}.select{|m| m[1] != nil }
    return res
  end

  def survey_question_chart(filter, survey, question, show_interval)
    val = if filter.blank?
            SurveysUser.unscoped.by_survey_and_restaurant(survey, self).where("date(surveys_users.created_at) between date(?) and date(?)", (Time.zone.now - 7.days).to_date, (Time.zone.now).to_date).group("date(surveys_users.created_at)").select("count(surveys_users.id),date(surveys_users.created_at) as date").order("date(surveys_users.created_at) asc")
          else
            filter = nil if filter == "all"
            SurveysUser.unscoped.by_survey_and_restaurant(survey, self).
                where(filter).group("date(surveys_users.created_at)").select("count(surveys_users.id),date(surveys_users.created_at) as date").order("date(surveys_users.created_at) asc")

          end

    if show_interval != "day"
      res =  val.map{|x|
        calculate_avg_answer_by_date_and_survey(survey,x,x.date.to_datetime,question)["q#{question}".to_sym]}.select{|m| m[1] != nil
      }
    else
      res = val.map{|x| calculate_avg_per_day(survey,x,x.date.to_datetime,question)["q#{question}".to_sym]}.select{|m| m[1] != nil }
      1.upto(24) do |hour|
        res << [Time.zone.now.change(:hour => hour).to_i * 1000, nil]
      end

      res = res.sort
    end

    return res
  end

  def set_default_restaurant_hours
    0.upto(6) do |i|
      self.restaurant_hours.create(:day_of_week => i, :open_at => "09:00AM",	:close_at => "10:00PM"	)
    end
  end

  def add_to_chain_owner
    #if from_import.blank?
    chain = self.chain
    chain.owners.each do |owner|
      if owner.is_chain_owner?
        if owner.restaurants.where("restaurants.id = #{self.id}").blank?
          owner.restaurants << self
          owner.save
        end
      end
    end
    #end
  end


  def self.get_id_by_lat_lng(reward, lat, lng)
    chain = reward.chain rescue nil
    restaurants = Restaurant.active.geo_scope(:origin => [lat, lng])
    .where('restaurants.chain_id = ?', chain.id).group(Restaurant.col_list)
    .order('distance asc') rescue []
    p "--------------get the nearest restaurant- ---------------------"
    restaurants.compact!
    restaurants.each do |restaurant|
      p restaurant
      p reward
      restaurant_reward = RestaurantReward.where(:restaurant_id => restaurant.id, :reward_id => reward.id).first rescue nil
      unless restaurant_reward.blank?
        return restaurant_reward.restaurant_id
        break
      end
    end
    return nil
  end

  def participate_to_rewards_and_offers
    Delayed::Job.enqueue(RestaurantJob.new(self))
  end

  ##### TAGGING FOR REGION
  def self.tagged_with(name, condition_sql=nil)
    if name.class == String
      tag = Tag.find_by_name(name)

      unless tag.blank?
        return tag.restaurants.where(condition_sql).order('restaurants.dashboard_display_text asc')
      else
        return []
      end
    else
      unless name.blank?
        tags = Tag.find_all_by_name(name)
        restaurants = []
        tags.each do |tag|
          restaurants << tag.restaurants.where(condition_sql).order('restaurants.dashboard_display_text asc')
        end
        return restaurants.flatten.compact.uniq
      else
        return []
      end
    end
  end

  def self.tag_counts
    Tag.select("tags.*, count(taggings.tag_id) as count").
        joins(:taggings).group("taggings.tag_id")
  end

  def tag_list
    tags.map(&:name).join(", ")
  end

  #def tag_list=(names)
    #if names.class == Array
    #  names = names.join(', ')
    #end
    #tg = []
    #  names.split(",").map do |n|
    #    #Tag.where(name: n.strip).first_or_create!
    #    if Tag.where(name: n.strip).first.blank?
    #      tg << Tag.create(:name => n.strip)
    #    else
    #      tg << Tag.where(name: n.strip).first
    #    end
    #  end
    #puts tg
    #names.each do |n|
    #self.tag_list.add(n)# = tg.flatten
    #  end
    #else
    #  unless names.blank?
    #  self.tags = names.map do |n|
    #    Tag.where(name: n.strip).first.blank? ? Tag.create(:name => n.strip)  : Tag.where(name: n.strip).first
    #  end
    #  end
    #end
  #end

  #def as_json(options={})
  #  super(:only => [:id, :name, :address, :app_display_text, :zipcode, :phone_number, :latitude, :longitude ],
  #        :include =>[:city, :chain],
  #        :methods=>:available_offers)
  #end

  def first_active_offer
    self.offers.where("\"effectiveDate\" <= '#{Time.current.strftime("%Y-%m-%d")}' AND \"expiryDate\" >= '#{Time.current.strftime("%Y-%m-%d")}'").order("created_at DESC").first
  end

  def self.aloha_restaurant_sync_hours
    Restaurant.where("online_order_support_status IS TRUE AND aloha_online_id != '' AND aloha_online_id IS NOT NULL").each do |restaurant|
      chain = restaurant.chain
      next if chain && chain.chain_setting && !chain.chain_setting.olo_restaurant_hour_sync
      site_info = AooCustomer.sites_info(chain, restaurant.aloha_online_id)
      unless site_info.blank?
        RestaurantHour.sync_hour_with_aloha(restaurant, site_info)
      end
    end

  end

  def is_not_preview_mode?
    if self.preview_mode.blank? || self.preview_mode == false
      true
    else
      false
    end
  end

  def is_open
    cur_d = Date.current
    rest = self.open_close_restaurant_days.where("day_time = ?", cur_d).first rescue nil
    rest_hours = self.restaurant_hours.find_by_day_of_week(cur_d.wday) rescue nil

    if !rest.blank? || rest_hours.try(:open_at).blank? || rest_hours.try(:close_at).blank? || rest_hours.try(:open_at).to_s == "0" || rest_hours.try(:close_at).to_s == "0"
      false
    # elsif rest
    #   rest.open
    else
      true
    end
  end

  def operating_hours
    restaurant_hours = []
    self.restaurant_hours.select("day_of_week, open_at, close_at").each do |restaurant_hour|
      restaurant_hours << restaurant_hour
    end if self.restaurant_hours
    return restaurant_hours
  end

  require 'net/ftp'
  def self.import_from_ftp_folder(chain)
    selected_files_executed= []
    if chain.ftp_url && chain.ftp_port && chain.ftp_username && chain.ftp_password
      ftp =Net::FTP.new
      ftp.passive = true
      ftp.connect(chain.ftp_url,chain.ftp_port)
      ftp.login(chain.ftp_username,chain.ftp_password)
      @files = ftp.nlst("*.csv") | ftp.nlst("*.xls")
      file = @files.sort_by{|f| ftp.mtime(f).to_datetime }.last
      return false if file.blank?
      last_modified = ftp.mtime(file)
      last_modified_in_hst = last_modified.in_time_zone

      if Date.current == last_modified_in_hst.to_date || (Date.current - 1.day) == last_modified_in_hst.to_date
        last_import = RestaurantImportHistory.where(:chain_id => chain.id, :log_type => "automaticly")
        if last_import.blank?
          file_executed = {}
          rih = RestaurantImportHistory.create(:chain_id => chain.id, :last_modification => last_modified_in_hst.to_datetime, :status => "in queue", :log_type => "automaticly", :file_name => "#{file}")
          file_executed[:history] = rih.id
          file_executed[:file] =file
          selected_files_executed << file_executed
        else
          if last_import.last.last_modification < last_modified_in_hst.to_datetime
            file_executed = {}
            rih = RestaurantImportHistory.create(:chain_id => chain.id, :last_modification => last_modified_in_hst.to_datetime, :status => "in queue", :log_type => "automaticly", :file_name => "#{file}")
            file_executed[:history] = rih.id
            file_executed[:file] =file
            selected_files_executed << file_executed
          end
        end
      end

=begin
      @files.each do |file|
        last_modified = ftp.mtime(file)
        last_modified_in_hst = last_modified.in_time_zone
        if Date.current == last_modified_in_hst.to_date
          file_executed = {}
          rih = RestaurantImportHistory.create(:chain_id => chain.id, :log_type => "", :status => "in queue")
          file_executed[:history] = rih.id
          file_executed[:file] =file
          selected_files_executed << file_executed
        end
      end
=end
      ftp.close
      if selected_files_executed.present?
        Delayed::Job.enqueue(ImportRestaurantsJob.new(chain, selected_files_executed.first[:file], selected_files_executed.first[:history]), :delayable_type => "Import Restaurants")
      end
    end
  end

  def create_or_update_ldap
    if false
      ldap = Ldap.new
      ldap.create_or_update_restaurant(self) rescue nil
    end
  end

  def generate_status
    # restaurant status detail
    # open, closed, coming soon, temp closed
    if (self.skip_generate_status.eql?(false) || self.skip_generate_status.blank?)
      if self.restaurant_detail.blank?
        @res_detail = RestaurantDetail.find_or_initialize_by_restaurant_id(self.id)
        @res_detail.status = self.status ? 0 : 1
        @res_detail.save
      end
    end
  end

  def online_partner
    if self.restaurant_detail.blank?
      restaurant_detail = RestaurantDetail.new(restaurant_id:  self.id, online_order_partner: 0)
      restaurant_detail.save
      oo_partner = restaurant_detail.try(:online_order_partner)
    else
      oo_partner = self.restaurant_detail.try(:online_order_partner)
    end
    return oo_partner
  end

  def location_id
    return self.restaurant_detail.external_location_store_id rescue nil
  end

  def set_hybrid_reward_redemption
    chain = self.chain
    if chain.pos_used_type.eql?(Chain::POS_USED_TYPE["FOCUS"]) && chain.try(:user_reward_redeemption_flow).eql?(3)
      HybridRewardRedemptionFlow.create(chain_id: chain.id, restaurant_id: self.id, flow: "app_based")
    elsif chain.try(:user_reward_redeemption_flow).eql?(3)
      HybridRewardRedemptionFlow.create(chain_id: chain.id, restaurant_id: self.id, flow: "pos_based")
    end
  end

  def city_label
    self.city.try(:name)
  end

  def state_label
    self.city.try(:region).try(:name)
  end

  def country_label
    self.city.try(:region).try(:country).try(:name)
  end

  def clear_non_mandatory
    restaurant = self
    if restaurant.status == false
      restaurant.update_attributes(contact: "999999", own_coordinates: false, status: false, region: "999999",
                                   ncr_store_id: "999999", online_order_link: "999999", fishbowl_restaurant_identifier: "999999",
                                   ncr_aloha_loyalty_store_id: "999999", online_order_support_status: false, aloha_online_id: "999999",
                                   ncr_aoo_menu_level: "999999", ncr_aoo_price_level: "999999", delivery_radius_miles: "0", ldap_identity: nil,
                                   beacon_serial_number: "999999", beacon_uuid: "999999", location_qrcode_identifier: "999999", external_id: "999999",
                                   social_link: "999999", agm_support: false, olo_store_id: "999999", external_partner_id: "999999", ncr_provisioned_rewards: false,
                                   ncr_provisioned_loyalty: false)
    end
  end

  def self.move_restaurant_owner_chain(res,old_chain_id, new_chain_id)
    ## cause by updating chain on restaurant admin pages
    old_owner_ids = ChainsOwner.where(:chain_id => old_chain_id).map(&:owner_id)

    ## remove from old ones
    unless old_owner_ids.blank?
      old_owner_ids = Owner.where("id in(?)", old_owner_ids)
      old_owner_ids.each do |old_owner|
        old_owner.restaurants_owners.where(:restaurant_id => res.id).each do |x|
          x.delete
        end
      end
    end

    new_owner_ids = ChainsOwner.where(:chain_id => new_chain_id).map(&:owner_id)
# add new one
    unless new_owner_ids.blank?
      new_owner_ids = Owner.where("id in(?)", new_owner_ids)
      new_owner_ids.each do |new_owner|
        if new_owner.restaurants_owners.where(:restaurant_id => res.id).blank?
          RestaurantsOwner.create(:owner_id => new_owner.id, :restaurant_id => res.id)
        end
      end
    end

    RestaurantsOwner.where(:restaurant_id => res.id).each do |x|
      if x.owner.role_id == 2
        x.delete
      end
    end
  end


  # for restaurant owner
end
