class UserFishbowl < ActiveRecord::Base

  belongs_to :chain
  belongs_to :user
  belongs_to :restaurant

  STATUS = {
      "NEW"  => 1,
      "CHANGED" => 2,
      "FAILED_SYNC" => 3,
      "SYNCED" => 4,
      "REPEATED" => 5,
      "MARK OPT OUT" => 6

  }

  def status_in_color
    color = case self.status
              when 1
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:blue'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[1]}"
              when 2
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:yellow'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[2]}"
              when 3
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:red'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[3]}"
              when 4
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:green'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[4]}"
              when 5
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:purple'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[5]}"
              when 6
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:black'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[6]}"
            end
  end


end
