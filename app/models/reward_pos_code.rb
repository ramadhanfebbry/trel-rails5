class RewardPosCode < ActiveRecord::Base

  belongs_to :user
  belongs_to :reward
  belongs_to :restaurant

  attr_accessor :csv

  validates :code, :presence => true
end