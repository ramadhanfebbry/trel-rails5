class QuestionChoiceTextLocale < ActiveRecord::Base

  belongs_to  :question_choice
  belongs_to :locale

  validates :label, :length => {:maximum=>50}, :presence => true

  def self.import_existing_data
    Chain.all.each do |chain|
      chain.surveys.each do |survey|
        survey.questions.each do |question|
          question.question_choices.each do |question_choice|
            chain.locales.each do |locale|
              question_choice_text = QuestionChoiceTextLocale.where(:question_choice_id => question_choice.id, :locale_id => locale.id).first
              if question_choice_text.blank?
                question_choice_text = QuestionChoiceTextLocale.create(:question_choice_id => question_choice.id, :locale_id => locale.id, :label => question_choice.label)
              else
                question_choice_text.update_attributes(:question_choice_id => question_choice.id, :locale_id => locale.id, :label => question_choice.label)
              end
            end
          end
        end
      end
    end
  end

end
