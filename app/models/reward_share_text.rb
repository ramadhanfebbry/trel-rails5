class RewardShareText < ActiveRecord::Base

  belongs_to :locale
  belongs_to :chain

  validates :share_text_facebook, :presence => true
  validates :share_text_twitter, :presence => true

end