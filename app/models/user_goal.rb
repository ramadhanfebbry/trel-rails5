class UserGoal < ActiveRecord::Base

  belongs_to :goal
  belongs_to :user
  belongs_to :week

  def goal_desc
     self.goal.description rescue nil
  end
  
end
