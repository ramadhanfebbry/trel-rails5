class ApplicationSweeper < ActionController::Caching::Sweeper

  observe Application

  def after_save(record)
    expire_application_cache(record)
  end

  def after_destroy(record)
    expire_application_cache(record)
  end

  private

  def expire_application_cache(record)
    record.application_keys.each do |appkey|
      Rails.cache.delete("obj_appkey_#{appkey.appkey}")
      Rails.cache.delete("appkey_#{appkey.appkey}")
    end
  end

end
