class BucketPath < ActiveRecord::Base

  has_many :steppingstone_steps, :as => :stepable, :dependent => :destroy
  belongs_to :chain
  belongs_to :tag

  attr_accessor :receipt_transaction

  validates :name, :tag_id, :presence => true

  def self.tag_choices
    options = {}
    Tag.user_tags.group_by{|a| a.category_type}.each do |key, value|
      options["#{key}"] = {}
      options["#{key}"][:label] = key
      options["#{key}"][:options] = []
      value.each do |opt|
        options["#{key}"][:options] << {:label => opt.name, :value => opt.id}
      end
    end
    options
  end

  def receipt_approved_process
    p "----------------- start receipt approved BUCKET----------------"
    receipt = receipt_transaction.receipt
    user = receipt.user
    p "user is #{user}"
    user_step = UserBucketStep.find_or_create_by_user_id_and_bucket_path_id(user.id, self.id)
    step = self.steppingstone_steps.where("step_number > ?", user_step.current_step).order("step_number ASC").first
    p " user step is : "
    p user_step
    p "step is : "
    p step
    if step.blank?
      p "-------- here ----------"
      if self.resettable || user_step.current_step == 0
        user_step.update_attributes(:current_step => 0, :current_count => 0)
        p user_step.current_step
        step = self.steppingstone_steps.where("step_number > ?", user_step.current_step).order("step_number ASC").first
        p step
        receipt_approved_reward(user, receipt_transaction, step, user_step) if step
      end
    else
      receipt_approved_reward(user, receipt_transaction, step, user_step)
    end
  end

  def receipt_approved_reward(user, receipt_transaction, step, user_step)
    reward = step.reward
    p "reward is #{reward}"
    user_receipt_transaction_approved = ReceiptTransaction.includes(:receipt).where("receipts.user_id = ? AND receipt_transactions.status = ?", user.id, Receipt::STATUS[:APPROVED]).count
    p "user receipt trans approved : #{user_receipt_transaction_approved}"
    p "count step receipt approved : #{step.property_one}"
    p "minimum subtotal : #{step.property_two}"
    p "transction subtotal : #{receipt_transaction.subtotal.to_f}"
    if step.property_one <= user_receipt_transaction_approved and step.property_two <= receipt_transaction.subtotal.to_f
      user_step.increment!(:current_count, 1)
      user_step.update_attribute(:current_step, step.step_number)
      RewardWallet.create_with_delay({:user_id => user.id, :reward_id => reward.id}) if reward
    elsif step.property_two > receipt_transaction.subtotal.to_f and !reward.blank?
      return unless user.active
      #p "receipt subtotal less than subtotal step"
      steppingstone_notif = user.chain.steppingstone_notifications.where(:locale_id => user.locale_id).first
      return if steppingstone_notif.blank?
      mail = setup_mail(user.email, Setting.email.default_from,
                        (steppingstone_notif.subject % {:receipt_id => receipt_transaction.receipt_id}),
                        (steppingstone_notif.content % {
                            :receipt_id => receipt_transaction.receipt_id,
                            :reward_name => reward.name,
                            :min_subtotal => sprintf( "%0.02f", step.property_two),
                            :reward_fine_print => reward.fineprint
                        }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />"),
                        "multipart/alternative", false) unless steppingstone_notif.blank?
      mail.deliver!
      p '----------send email--------------'
    end
  end

  protected

  def setup_mail(user_email, from, subject, body, content_type, delay)
    mail = if delay
             Mail.new do #TODO : looking for the way how to delay send email with mail gem
               to user_email
               from from
               subject subject
             end
           else
             Mail.new do
               to user_email
               from from
               subject subject
             end
           end

    if content_type.eql?("multipart/alternative") or content_type.eql?("text/plain")
      text_part = Mail::Part.new do
        body body
      end
      mail.text_part = text_part
    end

    if content_type.eql?("multipart/alternative") or content_type.eql?("text/html")
      html_part = Mail::Part.new do
        content_type 'text/html'
        body body
      end
      mail.html_part = html_part
    end
    mail
  end

end