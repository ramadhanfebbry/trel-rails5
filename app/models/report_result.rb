require 'rest-client'
require 'net/ftp'
require 'stringio'
require 'net/sftp'

class ReportResult < ActiveRecord::Base
  serialize :jasper_session, JSON
  serialize :emails
  attr_accessor :retry_date
  belongs_to :report_subscription, :foreign_key => "subscription_id"

  ## this is where the callback for sending email , etc are execute
  after_save :process_report

  def write_to_file
    @retry = 1
    if self.report_subscription.report_definition.asynchronous == true
      response_get_report = RestClient.get "#{Setting.jasper.server}/rest/report/#{self.uuid}?file=report", self.jasper_session
    else
      begin
        cookies = self.report_subscription.authenticate_jasper
        response_get_report = self.report_subscription.v2_get_report(cookies, self.retry_date)
          #response_get_report  = self.report_subscription.get_response_resource(cookies)
      rescue
        if @retry <= 3
          @retry = @retry + 1
          retry
        end
      end
    end
    ## write to files

    File.open("#{Rails.root}/tmp/#{custom_filename}", "w+") { |f| f << response_get_report.force_encoding("utf-8") } unless response_get_report.blank?
  end

  def sending_method_use
    ## hits is where logic to send email are executed   , right now only email that are support this

    write_to_file ## write to tmp file first
    send_method = ReportSubscription::SEND_TYPES.key(self.report_subscription.sending_method)
    p "send method #{send_method}"
    if send_method == "email" && !self.emails.blank?
      return testing_emails
    end
    p send_method == "email"
    p send_method == "link"
    p send_method == "calendar"
    p send_method == "ftp"

    rs = self.report_subscription

    if send_method == "email" ## if its sending via email
      owners = rs.owners.map(&:email) + rs.additional_emails_array
      sending_via_email(owners) unless owners.blank?
    elsif send_method == "link" ## if its sending the link only

    elsif send_method == "calendar" ## if its show only on the calendar
      send_to_calendar
    elsif send_method == "ftp"
      ## using the email first before
      begin
      owners = ["tb@dailygobble.com, inoe.bainur@gmail.com"] + rs.owners.map(&:email) + rs.additional_emails_array rescue []
      sending_via_email(owners) unless owners.blank?
      if !self.emails.blank?
        testing_emails
      end
      rescue
        end
      ### end
      send_via_ftp
    elsif send_method == "sftp"
      begin
        owners = ["tb@dailygobble.com, inoe.bainur@gmail.com"] + rs.owners.map(&:email) + rs.additional_emails_array rescue []
        sending_via_email(owners) unless owners.blank?
        if !self.emails.blank?
          testing_emails
        end
      rescue
      end
      send_via_sftp
    else
      ## all methods
    end

    self.report_subscription.update_attribute(:last_executed_at, Date.today)
  end

  def send_via_sftp
    p "-----------------------    sending report file via ftp-------------------"
    rs = self.report_subscription
    return if rs.blank?
    return if rs.ftp_host.blank?
    return if rs.ftp_username.blank?
    return if rs.ftp_password.blank?
    return if rs.ftp_upload_path.blank?
    s3_file = write_to_file
    port = rs.ftp_port
    rep = write_to_report rescue nil

    f_name = custom_filename
    port = rs.ftp_port

    unless rs.subscription_format.blank?
      file_type = ReportSubscription::FILE_TYPES.key(rs.file_type.to_i)# rescue "pdf"
      f_name = "#{rs.subscription_format.filename({"start_date" => rep.start_date, "end_date" =>  rep.end_date})[1]}" + ".#{file_type}"
    end

    begin
      f_name = filename_for_papa_murphy(rs,rep) if rs.report_definition.jasper_path.include?("papa_murphy")
      f_name = filename_for_dicks(rs,rep)  if rs.report_definition.jasper_path.include?("dicks_demographic")
    rescue
    end

    if port.blank?
      Net::SFTP.start(rs.ftp_host, rs.ftp_username, :password => rs.ftp_password) do |sftp|
        @sftp = sftp # I've got a session object so that seems to work

        # upload a file or directory to the remote host
        puts File.exist?("#{Rails.root}/tmp/#{custom_filename}")
        puts "eaaaaaaaaaaaaaaaaaaaaaaaaa"
        sftp.upload!("#{Rails.root}/tmp/#{custom_filename}", rs.ftp_upload_path+"/"+f_name)
      end
    else
      puts "SFTP FILE TRANSFER ----"
      Net::SFTP.start(rs.ftp_host, rs.ftp_username, :password => rs.ftp_password, :port => port) do |sftp|
        @sftp = sftp # I've got a session object so that seems to work
        puts "FILE NAME SFTP ISSS:::"
        puts f_name
        # upload a file or directory to the remote host
        puts File.exist?("#{Rails.root}/tmp/#{custom_filename}")
        puts "---------- using PORT"
        sftp.upload!("#{Rails.root}/tmp/#{custom_filename}", rs.ftp_upload_path+"/"+f_name)
      end
    end
  end

  def testing_emails
    write_to_file
    s3_url = write_to_s3
    report = write_to_report
    ow = Owner.first
    is_base_email = self.report_subscription.report_definition.is_email_content rescue false

    if is_base_email == false
      ChainMailer.report_owner(ow.email, s3_url, report, self.emails, self.report_subscription,self).deliver! rescue nil
    else
      ChainMailer.report_owner_base64(ow.email, s3_url, report, self.emails, self.report_subscription,self).deliver! rescue nil
    end
  end

  def custom_filename
    self.id.to_s + self.filename
  end

  def write_to_s3
    rs = self.report_subscription
    key = key_for_s3
    aws_url = rs.stored_in_aws("#{Rails.root}/tmp/#{custom_filename}", key) rescue nil
    return aws_url
  end

  def sending_via_email(owner_emails)
    write_to_file
    s3_url = write_to_s3
    report = write_to_report

    owner_emails.each do |ow|
      is_base_email = self.report_subscription.report_definition.is_email_content rescue false

      if is_base_email === false
        ## for non base 64 email
        ChainMailer.report_owner(ow, s3_url, report, [], self.report_subscription,self).deliver! rescue nil
      else
        if self.report_subscription.report_definition.is_email_content == true
          ## for non base 64 email
          ChainMailer.report_owner_base64(ow, s3_url, report, [], self.report_subscription,self).deliver! rescue nil
        end
      end
    end
  end

  def write_to_report
    rs = self.report_subscription
    key = key_for_s3
    report_subscription.stored_to_report(rs.chain_id, key, ReportSubscription::TYPES.key(rs.report_kind)) rescue nil
  end


  def key_for_s3
    rs = self.report_subscription

    key = "/report/#{rs.chain_id}/#{Date.today.to_s}/#{custom_filename}"
    return key
  end

  def process_report
    ## adding it as a delayed Job.to execute def sending_method_use
    Delayed::Job.enqueue(ReportJob::ReportResultJob.new(self),
                         :delayable_type => "ReportSubscription",
                         :delayable_id => self.report_subscription.id,
                         :chain_id => self.report_subscription.chain_id
    )
  end

  def send_via_ftp
    p "-----------------------    sending report file via ftp-------------------"
    rs = self.report_subscription
    return if rs.blank?
    return if rs.ftp_host.blank?
    return if rs.ftp_username.blank?
    return if rs.ftp_password.blank?
    return if rs.ftp_upload_path.blank?
    s3_file = write_to_file
    rep = write_to_report rescue nil
    @retry = 1 # retry 3 time if there was time out error
    f_name = custom_filename
    port = rs.ftp_port

    unless rs.subscription_format.blank?
      file_type = ReportSubscription::FILE_TYPES.key(rs.file_type.to_i)# rescue "pdf"
      f_name = "#{rs.subscription_format.filename({"start_date" => rep.start_date, "end_date" =>  rep.end_date})[1]}" + ".#{file_type}"
    end

    begin
      f_name = filename_for_papa_murphy(rs,rep) if rs.report_definition.jasper_path.include?("papa_murphy")
      f_name = filename_for_dicks(rs,rep)  if rs.report_definition.jasper_path.include?("dicks_demographic")
    rescue
    end
    #begin
    if port.blank?
    ftp = Net::FTP.new(rs.ftp_host)
    ftp.passive = true
    ftp.login(rs.ftp_username, rs.ftp_password)
    ftp.chdir(rs.ftp_upload_path)
    ftp.storbinary("STOR #{f_name}", open(s3_file.path, 'rb'), 1024)
    puts "put file"
    else
      ftp = Net::FTP.new#(rs.ftp_host)
      ftp.connect(rs.ftp_host,port)
      ftp.passive = true
      ftp.login(rs.ftp_username, rs.ftp_password)
      ftp.chdir(rs.ftp_upload_path)
      ftp.storbinary("STOR #{f_name}", open(s3_file.path, 'rb'), 1024)
      puts "put file"

    end
    #rescue
    #  @retry = @retry + 1
    #  if @retry <= 3
    #    retry
    #  end
    #ensure
    #  ftp.close unless ftp.blank?
    #end
    p " ------------------- sent report to ftp----------"
  end

  def filename_for_papa_murphy(rs,report)
    start_date = nil
    report_end_date = nil
    #report = self
    chain = report.chain
    file_type = ReportSubscription::FILE_TYPES.key(rs.file_type.to_i)# rescue "pdf"
    start_date = report.start_date.strftime("%Y-%m-%d") if report.start_date


    subject = (rs.report_definition.email_subject % {
        :chain => (chain.name rescue ""),
        :start_date => start_date.gsub('-','')
    }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />") rescue nil

    f_name = "#{subject}.#{file_type}"
    return f_name
  end

  def filename_for_dicks(rs,report)
    start_date = nil
    report_end_date = nil
    #report = self
    chain = report.chain
    file_type = ReportSubscription::FILE_TYPES.key(rs.file_type.to_i)# rescue "pdf"
    start_date = report.start_date.strftime("%Y-%m-%d") if report.start_date


    subject = (rs.report_definition.email_subject % {
        :chain => (chain.name rescue ""),
        :start_date => (start_date.gsub('-','') rescue nil)
    }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />") rescue nil

    f_name = "#{subject}.#{file_type}"
    return f_name
  end
end

