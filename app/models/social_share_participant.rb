class SocialShareParticipant < ActiveRecord::Base
  belongs_to :social_share
  belongs_to :user

  MEDIUM = {
      1 =>   "Facebook",
      2 =>   "Twitter",
      3 =>   "Instagram"
  }


  scope :search, -> (params) { 
    unless params.blank?
    users(params['user_id']).incentive_status(params['incentive_status']).by_media(params['medium'])
    end
  }

  scope :users, -> (value) { 
    if (!value.blank?)
      where("email like ? or user_id=?",'%'+value+'%',value.to_i)
    end
  }

  scope :incentive_status, -> (value) { 
    if (!value.blank?)
      where("received = ?",value)
    end
  }
  scope :by_media, -> (value) { 
    if (!value.blank?)
      where("medium_id = ?",value)
    end
  }


  ### SocialShareParticipant.get_reward_or_point()
  def self.get_reward_or_point(user_social,social_share,medium)
    incentive = social_share.incentive
    success = true
    tmp_cap = validate_cap(social_share, user_social)
    pass_cap = tmp_cap[1]
    pass_kind = tmp_cap[0]
    #p "-" * 80
    #p pass_cap
    ## check the daily / weekly / monthly cap
    #return false if pass_cap == false

    ### if there is an incentive for this
    unless incentive.blank?
      if incentive.class == SocialOffer
         # p "!" * 80
          log_msg = "Pushed #{incentive.points} to #{user_social.email} from Social Share #{social_share.id}"
          log_msg = "Reached #{pass_kind} cap !" if pass_cap == false
          SocialShareParticipant.create({
            :social_share_id => social_share.id,
            :user_id => user_social.id,
            :reward_id => nil,
            :points =>  incentive.points,
            :email => user_social.email,
            :medium_id => medium,
            :received => pass_cap,
            :log => log_msg ,
            :medium => SocialShareParticipant::MEDIUM[medium.to_i]
          })

         if pass_cap == true
          user_social.earn(incentive.points, nil, nil)
          PointHistory.add_to_history(user_social.id, "social share incentive", incentive.points)
         end
        else
        #  p "x" * 80
          log_msg = "Pushed #{incentive.name} to #{user_social.email} from Social Share #{social_share.id}"
          log_msg = "Reached #{pass_kind} cap !" if pass_cap == false

          SocialShareParticipant.create({
                                            :social_share_id => social_share.id,
                                            :user_id => user_social.id,
                                            :reward_id => incentive.id,
                                            :points =>  nil,
                                            :email => user_social.email,
                                            :medium_id => medium,
                                            :received => pass_cap,
                                            :log => log_msg,
                                            :medium => SocialShareParticipant::MEDIUM[medium.to_i]
                                        })
        #  p "Pushing the reward"
          RewardWallet.create_with_delay({:reward_id => incentive.id, :user_id => user_social.id}) if pass_cap == true
        #  p  "done"
      end
    end
    success = true

    return success
  end


  def self.validate_cap(social_share, user_social)
    ## validate caps
    daily_cap = social_share.daily_cap
    weekly_cap = social_share.weekly_cap
    monthly_cap = social_share.monthly_cap

    ## check the daily first

    date_zone = "date(TIMEZONE('UTC',created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') "
    conditions = "#{date_zone} = date('#{Time.zone.now.to_date}') AND log Not like '%cap%'"
    #p conditions
    pass_cap = user_social.social_share_participants.where(conditions).count < daily_cap if daily_cap > 0
    #puts user_social.social_share_participants.where(conditions).count
    #puts "daily_cap = #{daily_cap}"
    #p "xxxxxxxxxxxxxxxxx"
    #puts pass_cap
    #xxxx
    return ["daily",pass_cap] if pass_cap == false
    ## end daily

    # check the weekly
    conditions = "#{date_zone} >= date('#{Time.zone.now.beginning_of_week}') and
                 #{date_zone} <= date('#{Time.zone.now.end_of_week}') AND log Not like '%cap%'"
    pass_cap = user_social.social_share_participants.where(conditions).count < weekly_cap if weekly_cap > 0
    return ['weekly',pass_cap] if pass_cap == false
    # monthly
    conditions = "#{date_zone} >= date('#{Time.zone.now.beginning_of_month}') and
                 #{date_zone} <= date('#{Time.zone.now.end_of_month}') AND log Not like '%cap%'"
    pass_cap = user_social.social_share_participants.where(conditions).count < monthly_cap if monthly_cap > 0
    return  ['monthly',pass_cap]  if pass_cap == false

    return [nil, true]
  end
end
