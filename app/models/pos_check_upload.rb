class PosCheckUpload < ActiveRecord::Base

  belongs_to :pos_location
  belongs_to :chain
  belongs_to :user
  has_one :receipt
  belongs_to :location, :class_name => "Restaurant", :foreign_key => "restaurant_id"

  # after_save :set_barcode_32

  after_save :set_micros_check_data , :if => [:is_micros]

  #validates :check_id, :uniqueness => {:scope => :sequence_number}

  STATUS = {
      :NEW => 1,
      :UPDATED => 2,
      :CONVERTED => 3,
      :PROCESSED => 3
  }

  SERVICE = {
      :TABLE => 1,
      :COUNTER => 2
  }

  CHECK_STATE_STATUS = {
      :CHECK_RECEIVED_UNPAID => 1,
      :CHECK_RECEIVED_PAID => 2,
      :CHECK_SCANNED_UNPAID => 3,
      :CHECK_SCANNED_PAID => 4
  }

  scope :unpulled_payment, -> { where(:check_state => nil) }
  scope :pulled_payment, -> { where(:check_state => 1) }

  def is_micros
    return true if self.pos_used_type == Chain::POS_USED_TYPE["MICROS/NORTHKEY"]
  end

  def pull_payment!
   self.update_column(:check_state, 1)
  end

  def set_barcode_32
    if self.chain.pos_used_type == Chain::POS_USED_TYPE["NETPOS"]
      p "000----- set_barcode_32-----"
      self.update_column(:barcode, Chain.barcode_32(self))
    end
  end

  def status_name
    PosCheckUpload::STATUS.invert[self.status]
  end

  def service_name
    PosCheckUpload::SERVICE.invert[self.service]
  end

  def self.convert_to_receipt(pos_check_upload, user, restaurant_id, offer_id, receipt_id)
    ReceiptTransaction.observers.disable :all do
      puts "start parse xml data from pos check upload"

      puts "create receipt received record"

      p receipt_id
      receipt = Receipt.find receipt_id

      pos_xml = PosReceiptXml.new(pos_check_upload.xml_data)
      chain = user.chain
      location = Restaurant.find(restaurant_id)
      restaurant_offer = RestaurantOffer.where(:restaurant_id => restaurant_id, :offer_id => offer_id).first rescue nil

      "**************--------- get reward redeem discount ---------- ***********"
      check_id = pos_xml.check_id
      seq_num = pos_xml.seq_num
      p "-- check id is #{check_id}"
      p "--- sequence number is #{seq_num}"
      total_reward_discount = RewardDiscount.sum(:discount, :conditions => {:check_id => check_id, :seq_num => seq_num, :restaurant_id => restaurant_id})
      p total_reward_discount
      p "----- total discount selected is #{total_reward_discount}"
      receipt_transaction = ReceiptTransaction.new
      receipt_transaction.restaurant_offer_id = (restaurant_offer.id rescue nil)
      receipt_transaction.subtotal = pos_xml.sub_total
      receipt_transaction.tax = pos_xml.tax
      receipt_transaction.restaurant_id = restaurant_id
      receipt_transaction.total_discount = total_reward_discount

      valid_total = pos_xml.valid_total?
      valid_date = pos_xml.valid_date?

      p "validation ;;;;;;;;"
      p valid_total
      p valid_date

      if !valid_total and location and location.rest_tax > 0.0
        p " total not valid check rest tax process --------------"
        p pos_xml.sub_total.to_f
        p pos_xml.tax
        tax_sub_total = pos_xml.sub_total.to_f.eql?(0.0) ? 0.0 : (pos_xml.tax.to_f/pos_xml.sub_total.to_f).round(3)
        p tax_sub_total
        p location.rest_tax

        if (location.rest_tax * 0.98) <= tax_sub_total  && tax_sub_total <= (location.rest_tax * 1.02)
          valid_total = true
          pos_xml.error_messages.delete(:total)
        end
      elsif valid_total and location
        p "total is valid so update the rest tax for restaurant"
        p pos_xml.sub_total.to_f
        p pos_xml.tax
        tax_sub_total = pos_xml.sub_total.to_f.eql?(0.0) ? 0.0 : (pos_xml.tax.to_f/pos_xml.sub_total.to_f).round(3)
        p tax_sub_total
        p location.update_attribute(:rest_tax, tax_sub_total)
        p location.errors
      end

      if valid_total and valid_date
        if unique_receipt?(pos_xml)
          if !chain.blank? and chain.today_max_user_limit?(receipt, pos_xml.converted_date)
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            pos_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"

          else
            receipt_transaction.status = Receipt::STATUS[:APPROVED]
            receipt.status = Receipt::STATUS[:APPROVED]
          end
        else
          if location
            if unique_receipt?(pos_xml, location)
              if !chain.blank? and chain.today_max_user_limit?(receipt, pos_xml.converted_date)
                receipt_transaction.status = Receipt::STATUS[:REJECTED]
                receipt.status = Receipt::STATUS[:REJECTED]
                pos_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
              else
                receipt_transaction.status = Receipt::STATUS[:APPROVED]
                receipt.status = Receipt::STATUS[:APPROVED]
              end
            elsif unique_receipt?(pos_xml, location, pos_xml.check_id)
              if !chain.blank? and chain.today_max_user_limit?(receipt, pos_xml.converted_date)
                receipt_transaction.status = Receipt::STATUS[:REJECTED]
                receipt.status = Receipt::STATUS[:REJECTED]
                pos_xml.error_messages[:user_maxed] = "PER DAY RECEIPT MAXED"
              else
                receipt_transaction.status = Receipt::STATUS[:APPROVED]
                receipt.status = Receipt::STATUS[:APPROVED]
              end
            else
              receipt_transaction.status = Receipt::STATUS[:REJECTED]
              receipt.status = Receipt::STATUS[:REJECTED]
              pos_xml.error_messages[:receipt] = "Receipt should be uniq, this receipt have submitted before"
              ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, pos_xml.error_messages, receipt).deliver
            end
          else
            pos_xml.error_messages[:restaurant] = "Restaurant/Location is invalid."
            receipt_transaction.status = Receipt::STATUS[:REJECTED]
            receipt.status = Receipt::STATUS[:REJECTED]
            p "-----ocred 2------"
            # ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, pos_xml.error_messages, receipt).deliver
            p "--- sent ocred mail 2---"
          end
        end
      else
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.status = Receipt::STATUS[:REJECTED]
        p "-----ocred 3------"
        # ReceiptMailer.receipt_validation_failed(Setting.email.reviewer, pos_xml.error_messages, receipt).deliver
        p "--- sent ocred mail 3---"
      end
      receipt_transaction.instructions = pos_xml.error_messages
      receipt_transaction.receipt_number = pos_xml.check_id
      receipt_transaction.issue_date = pos_xml.converted_date
      receipt_transaction.time_stamp = (pos_xml.converted_time.strip rescue nil)
      receipt_transaction.receipt_date = pos_xml.converted_date
      receipt_transaction.receipt_time = (pos_xml.converted_time.strip rescue nil)
      receipt_transaction.restaurant_id = location ? location.id : nil
      p "this is receipt transaction object"
      p receipt_transaction
      receipt.receipt_transactions << receipt_transaction
      receipt.save
      return receipt
      puts "------ end ------"
    end
  end

  def self.unique_receipt?(ocr_xml, location = nil, receipt_number = nil)
    if location.blank? and receipt_number.blank?
      receipt_transactions = ReceiptTransaction.where("subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND status = ?", ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, Receipt::STATUS[:APPROVED])
      return true if receipt_transactions.blank?
      return false
    elsif location and receipt_number.blank?
      receipt_transactions = ReceiptTransaction.where("subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ? AND status = ?", ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id, Receipt::STATUS[:APPROVED])
      return true if receipt_transactions.blank?
      return false
    elsif receipt_number
      receipt_transactions = ReceiptTransaction.where("subtotal = ? AND DATE(issue_date) = ? AND time_stamp = ? AND restaurant_id = ? AND receipt_number = ? AND status = ?", ocr_xml.sub_total, ocr_xml.converted_date, ocr_xml.converted_time.strip, location.id, receipt_number, Receipt::STATUS[:APPROVED])
      return true if receipt_transactions.blank?
      return false
    else
      return false
    end
  end

  def get_total_payment
    pos_xml = PosReceiptXml.new(self.xml_data)
    pos_xml.total
  end

  def show_all_barcodes
    if self.barcode.eql?(self.old_barcode) || self.old_barcode.blank?
      self.barcode
    else
      "1. #{self.old_barcode} <br />
       2. #{self.barcode}"
    end
  end

  def set_micros_check_data
    # check if data is exist on micros_check_data table
    p "MICROS CHECK DATA"*10
    check_data = self
    exist = MicrosCheckData.where(:pos_check_upload_id => check_data.id).select('id').last

    json_check = YAML.load(check_data.xml_data) rescue nil
    pos_xml = PosReceiptXml.new(check_data.xml_data) rescue nil
    p "pos_xml"
    p pos_xml

    unless pos_xml.blank?
      return if pos_xml.check_id.blank?
      check_hash = {
          :pos_check_upload_id => check_data.id,
          :chain_id => check_data.chain_id,
          :pos_location_id => check_data.pos_location_id,
          :check_id => pos_xml.check_id,
          :seq_num => pos_xml.seq_num,
          :rvc_num => pos_xml.revenue_center,
          :emp_num => pos_xml.employee_num,
          :check_state => check_data.check_state,
          :created_at => check_data.created_at,
          :receipt_date => DateTime.strptime(pos_xml.receipt_date, "%m/%d/%y %H:%M:%S"),
          :receipt_number => pos_xml.receipt_number,
          :table_num => pos_xml.table_num,
          :check_open_time => pos_xml.check_open_time,
          :subtotal => pos_xml.sub_total,
          :sales_total => pos_xml.sales_total,
          :total => pos_xml.total,
          :tax => pos_xml.tax,
          :discount_total => pos_xml.discount_total,
          :direct_tips => pos_xml.direct_tips,
          :tip_total => pos_xml.tip_total,
          :due_total => pos_xml.due_total
      }

      if exist
        exist.update_attributes(check_hash)
      else
        MicrosCheckData.create(check_hash)
      end
    end
  end


end