class DbSummary < ActiveRecord::Base
  belongs_to :restaurant

  # DbSummary.prepulate_data(chain_id)
  def self.prepulate_data(chain_id)
    chain = Chain.find(chain_id)
    chain.restaurants.each do |restaurant|
      #total_member = restaurant.all_total_member
      # user_ids = restaurant.chain.users.where("active = ?", true).select("users.id") ## get all user_ids
      #
      # uniq_user = Receipt.where(
      #    "user_id in(?) and receipt_transactions.restaurant_id = ?",
      #     user_ids,restaurant.id
      #    ).select("count(DISTINCT(user_id)) as count_all_user_id").
      # joins(:receipt_transactions)
      # total_member =  uniq_user.first.count_all_user_id.to_i

      total_member = RestaurantUser.restaurant_members(restaurant).count
      member_this_month = restaurant.member_this_month
      member_last_month = restaurant.member_last_month
      ## survey
      sur = SurveysUser.where("restaurant_id in (?)", [restaurant]).first.survey rescue nil
      all_survey_avg = restaurant.survey_averages_restaurant(sur) rescue 0

      if all_survey_avg == 0
        @surveys = Survey.where("chain_id in (?)", chain_id)
        all_survey_avg = restaurant.survey_averages_restaurants(@surveys)
      end

      avg_survey = all_survey_avg[:all] rescue 0
      # end survey

      coming = restaurant.avg_coming.round(1)

      summary = self.find_or_create_by_chain_id_and_restaurant_id(chain_id, restaurant.id)

      summary.update_attributes(
          :total_member => total_member,
          :member_this_month => member_this_month,
          :member_last_month => member_last_month,
          :avg_general => avg_survey,
          :avg_visit_month => coming
      )
    end
  end
end
