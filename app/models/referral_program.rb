class ReferralProgram < ActiveRecord::Base
  include KeyUtil
  belongs_to :incentive, :polymorphic => true
  belongs_to :incentive_referer, :polymorphic => true
  belongs_to :chain
  has_many :referral_users
  validates :min_approve, :numericality => {:greater_than => -1} , :allow_nil => true, :if => Proc.new { |f| f.approve }
  #validates :min_approve,:presence => true
  #validates :min_approve, numericality: {only_integer: true}
  #has_many :referral_codes, :dependent => :destroy

  scope :active , -> { where("is_active = ? and start_date <= ? and expiry_date >= ?",true,Date.today,Date.today) }
  scope :by_chain, -> (c) { where("chain_id = ?", c.id)}
  before_save :set_min_approve

  TYPES = {
      "REFEREEANDREFERRER" => 1,
      "REFEREEONLY" => 2,
      "REFERRERONLY" => 3
  }

  def set_min_approve
    if self.approve == false
      self.min_approve = 0
    end
  end

  def generate_referral_codes(chain_obj)
    size = Setting.referral_code.code_size || 10
    chars = Setting.referral_code.code_chars 
    ## referall code uniq / user
    ## delete first
    ReferralCode.where("user_id in (?)", chain_obj.users.map(&:id)).delete_all
    users = self.chain.users
    
    users.each do |u|
      begin
        code = generate_random_string(size, chars)
        ReferralCode.create(:user_id => u.id, :code => code.downcase)        
      rescue => e
        p e
      end
    end
  end

  def set_active_program
    ReferralProgram.by_chain(self.chain).update_all("is_active = false")
    self.update_attribute(:is_active, true)
  end

  def is_active?
    self.start_date <= Date.today and self.expiry_date >= Date.today
  end

  def self.reset_referral(chain)
    rf = ReferralProgram.find_by_chain_id(chain.id)
    rf.destroy if rf
    rc = ReferralCode.by_chain(chain)
    rc.each {|x| x.delete}
    return true
  end

  def get_referral_program_type
    ref_prog = self
    type = nil
    if ref_prog.incentive && ref_prog.incentive_referer
      type = ReferralProgram::TYPES["REFEREEANDREFERRER"]
    elsif ref_prog.incentive
      type = ReferralProgram::TYPES["REFEREEONLY"]
    elsif ref_prog.incentive_referer
      type = ReferralProgram::TYPES["REFERRERONLY"]
    end
    return type
  end
  
end
