class Raffle < ActiveRecord::Base
  belongs_to :chain
  has_many :raffle_additional_informations, :dependent => :destroy
  has_many :raffle_steps, :dependent => :destroy
  has_many :raffle_user_informations

  attr_accessor :all_user, :user_temp_ids, :xls, :selected_step, :push_type

  def change_status
    if self.active
      # make it deactive
      self.update_attribute(:active, false)
    else
      # make it active
      self.update_attribute(:active, true)
    end
  end
end
