require 'mail'
require 'mandrill'

class Reward < ActiveRecord::Base
  include AASM

  aasm :column => :status do
    state :active
    state :inactive

    event :activate do
      transitions :to => :active, :from => [:active, :inactive ]
    end

    event :deactive do
      transitions :to => :inactive, :from => [:active, :inactive ]
    end
  end

  belongs_to :chain
  belongs_to :survey
  has_many :survey_users

  has_many :promotions, :as => :promotable, :dependent => :destroy
  has_many :restaurant_rewards
  has_many :restaurants, :through => :restaurant_rewards
  has_many :reward_wallets
  has_many :delayed_jobs, :as => :delayable, :dependent => :destroy

  has_one :chain_reward_event
  has_one :notification_reward
  has_one :fishbowl_reward_promotion_setting
  has_many :notification_locales, :as => :notifiable, :dependent => :destroy
  has_many :reward_menu_items
  has_many :general_menu_items, :through => :reward_menu_items
  has_many :reward_pos_codes, :dependent => :destroy
  has_many :reward_informations
  has_many :reward_info_locales

  has_one :pos_discount_type
  has_one :partner_reward

  accepts_nested_attributes_for :reward_informations, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :reward_info_locales, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :notification_locales, :promotions
  accepts_nested_attributes_for :pos_discount_type
  accepts_nested_attributes_for :fishbowl_reward_promotion_setting

  TYPES = {
    "ONE_TIME" => 1, # only once per user, good for welcome bonus reward
    "MILESTONE" => 2, # options for milestone bonus reward
    "REGULAR" => 3, # multiple rewards
    "PUSH_REWARD" => 4, # multiple rewards
    "PROMOTION" => 5, # reward for promotions
    "INCENTIVE" => 6,
    "GIFTABLE" => 7,
    "DASHBOARD" => 8,
    "BIRTHDAY" => 9,
    "ANNIVERSARY" => 10,
    "PARTNER" => 11
  }

  default_scope { where(:deleted_at => nil) }
  scope :dashboard, -> { where("rewards.visible_dashboard = ?",true) }
  scope :in_effect, -> { where('"effectiveDate" <= ?', Date.today) }
  scope :unexpired, -> { where('("expiryDate" > ? OR "expiryDate" IS NULL) AND (date("effectiveDate") <= date(?))', Date.today, Date.today) }
  scope :one_time, -> { where("reward_type = ?", TYPES["ONE_TIME"]) }
  scope :chain_one_time_rewards, -> (c) {where("reward_type = ? and chain_id = ?", TYPES["ONE_TIME"],c.id)}
  scope :by_chain, -> (x) { where("chain_id =?", x)}
  scope :regular , -> { where("reward_type = ?", TYPES["REGULAR"]) }
  scope :active, -> { where("rewards.status = ?",'active') }
  scope :user_qualified, -> (points){ where("points <= ?", points)}
  scope :not_expired, -> { where("(\"expiryDate\" IS NULL OR date(TIMEZONE('UTC', \"expiryDate\") AT TIME ZONE '#{Time.current.strftime('%Z')}') >= ?) AND date(TIMEZONE('UTC', \"effectiveDate\") AT TIME ZONE '#{Time.current.strftime('%Z')}') <= ?", Time.current.to_date, Time.current.to_date) }
  #scope :active, where('"isActive" = true')

  validates :name,:effectiveDate, :expiryDate, :presence => true
  validate :expiration_date_cannot_be_greater_than_effective_date
  validate :executed_at_must_greater_than_now
  validates :points, :numericality => { :greater_than_or_equal_to => 0 }, :presence => true
  validates :number_of_times_gifted, :numericality => { :greater_than_or_equal_to => 0 }, :presence => true, :if => Proc.new{|a| a.reward_type.eql?(Reward::TYPES["GIFTABLE"])}
  validates :POSCode,:format => {:with => /\A[a-z0-9]+[-a-z0-9]*[a-z0-9]+\z/i} ,
    :if => Proc.new { |reward| !reward.POSCode.blank? }#alphanumeric max 20

  validates :hours_delay, :numericality => {
    :greater_than_or_equal_to => 0,
    :less_than_or_equal_to => 200
  } , :if => Proc.new {|reward| reward.reward_type != Reward::TYPES["REGULAR"]}

  validates :expired_day_user, :numericality => {
    :greater_than_or_equal_to => 0,
    :only_integer => true,
    :less_than_or_equal_to => 500
  } , :if => Proc.new {|reward| reward.reward_type != Reward::TYPES["REGULAR"]}
  validates :fineprint,:length => { :maximum => 300 }

  validates :priority_number, :numericality => {
    :greater_than_or_equal_to => 0,
    :only_integer => true,
    :message => "Integer number only and must be greater than 0"
  }
  before_validation :number_valid_validation
  after_save :create_reward_info_locale

  attr_accessor :isExpired, :isEffective, :sort_by_id, :number_valid, :all_users, :gifter ,:img_from_mobile ,
                :image_content_type, :new_reward, :device_expired

  ### paperclip
  has_attached_file :attachment,
                    :styles => { :small => Setting.paperclip.styles.small,
                                 :large => Setting.paperclip.styles.large },
                    :path => ":class/:attachment_reward/:year/:month/:reward_:chain_:timestamp_:style.:img_content_type",
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                        :access_key_id => Setting.storage.s3_access_key_id,
                        :secret_access_key => Setting.storage.s3_secret_access_key
                    }

  has_attached_file :thumbnail,
                    :path => ":class/:attachment_reward_thumbnail/:year/:month/:reward_:chain_:timestamp_:style.:img_content_type",
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                        :access_key_id => Setting.storage.s3_access_key_id,
                        :secret_access_key => Setting.storage.s3_secret_access_key
                    }


  #validates_attachment_presence :image, :on=>:create
  validates_attachment_content_type :attachment, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :on=>:create
  validates_attachment_content_type :thumbnail, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :on=>:create


  Mail.defaults do
    delivery_method :smtp, {
      :address   => Setting.smtp.host,
      :port      => Setting.smtp.port,
      :user_name => Setting.smtp.username,
      :password  => Setting.smtp.password,
      :authentication => 'plain',
      :enable_starttls_auto => true
    }
  end

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def executed_at_must_greater_than_now
    unless self.executed_at.blank?
      if self.executed_at < Time.now.utc  and self.reward_type == TYPES["PUSH_REWARD"]
        errors.add(:executed_at, "must be greater than time right now #{Time.now.utc}")
      end
    end
  end

  def number_valid_validation
    if !number_valid.blank? and number_valid.to_i > 0 and expiryDate.blank?
      self.expiryDate = self.effectiveDate + number_valid.to_i.days
    elsif !number_valid.blank? and number_valid.to_i == 0
      errors.add(:number_valid, "must be a number")
    end
  end

  def expiration_date_cannot_be_greater_than_effective_date
    if number_valid.blank?
      unless effectiveDate.blank? and expiryDate.blank?
        begin
          if effectiveDate > expiryDate
            errors.add(:effectiveDate, "must be less that expired date")
            errors.add(:expiryDate, "must be greater than effective date")
          end
        rescue
        end
      end
    end
  end

  def pullout(restaurants)
    self.restaurant_rewards.where("restaurant_id in (?)", restaurants).delete_all
  end

  def participate(restaurants)
    restaurants.each do |restaurant_id|
      existing_deleted_restaurant_reward = RestaurantReward.unscoped.where("reward_id = ? AND restaurant_id = ? AND deleted_at IS NOT NULL", self.id, restaurant_id).first
      if existing_deleted_restaurant_reward.blank?
        existing_restaurant_reward = RestaurantReward.where("reward_id = ? AND restaurant_id = ?", self.id, restaurant_id).first
        RestaurantReward.create(:reward_id => self.id, :restaurant_id => restaurant_id) if existing_restaurant_reward.blank?
      else
        existing_deleted_restaurant_reward.revive
      end
    end
  end

  def self.push_reward(event, chain, user, receipt_type = "submission")
    chain_events = ChainRewardEvent.includes(:reward).where(:chain_id => chain.id, :event => event)
    unless chain_events.blank?
      chain_events.each do |chain_event|
        if chain_event.reward && chain_event.reward.status == "active"
          if event.eql?(ChainRewardEvent::EVENTS["FIRST_RECEIPT"])
            puts "this is first receipt event"
            receipt_approved_count = user.receipts.select("user_id, status").where(:status => Receipt::STATUS[:APPROVED]).reload.count
            puts "receipt approved count = #{receipt_approved_count}"
            RewardWallet.create_with_delay(:reward_id => chain_event.reward_id, :user_id => user.id) if receipt_approved_count.eql?(1)
          else
            RewardWallet.create_with_delay(:reward_id => chain_event.reward_id, :user_id => user.id)
          end
        end
      end
    end
  end

  def self.of_type(type, only_active=false, only_in_effect=false, only_unexpired=false)
    relation= unscoped
    relation= relation.in_effect if only_in_effect
    relation= relation.active if only_active
    relation= relation.unexpired if only_unexpired
    relation.where('reward_type = ?', type)
  end

  def self.of_chain(chain_id, only_active=false, only_in_effect=false, only_unexpired=false)
    of_type(TYPES["REGULAR"], only_active, only_in_effect, only_unexpired).where('chain_id = ? AND status = ?', chain_id, 'active')
  end

  def create_notifications(chain,text_params)
    unless text_params.blank?
      locales = chain.locales
      text_params.each_with_index do |text, index|
        NotificationLocale.create(:notifiable_id => self.id.to_i, :notifiable_type => "Reward", :notice => text, :locale_id => locales[index].id)
      end
    end
  end

  def send_schedule_reward(restaurant_ids)
    activate_wallet(restaurant_ids)
  end

  def activate_wallet(restaurant_ids)
    user_ids = []
    restaurants = Restaurant.where("id in (?)", restaurant_ids)

    if all_users.blank?
      restaurants.each do |restaurant|
        user_ids = restaurant.all_user_ids
      end
      user_ids = user_ids.flatten.uniq
    else
      user_ids = self.chain.users.map(&:id)
    end
    emails_and_device_tokens_group = group_emails_and_device_token_by_locale(user_ids)
    notification_by_locale = group_message_pn_by_locale
    Delayed::Job.enqueue(PushRewardJob.new(self, user_ids), chain_id: self.chain_id, delayable_type: self.class.to_s,	delayable_id: self.id)
    Delayed::Job.enqueue(GcmAndroidJob.new(self, emails_and_device_tokens_group, notification_by_locale,nil), chain_id: self.chain_id, delayable_type: self.class.to_s,	delayable_id: self.id, run_at: self.executed_at) if is_push_phone
    Delayed::Job.enqueue(PushIphoneJob.new(self, emails_and_device_tokens_group, notification_by_locale,nil), chain_id: self.chain_id, delayable_type: self.class.to_s,	delayable_id: self.id, run_at: self.executed_at) if is_push_phone

    chain = self.chain
    notif_setting = chain.notification_settings.where(email_template: "rewards_notification_user").first rescue nil
    unless notif_setting.blank?
      if notif_setting.send_type.eql?(1)
        Delayed::Job.enqueue(PushRewardMailJob.new(self, emails_and_device_tokens_group, notification_by_locale,nil), chain_id: self.chain_id, delayable_type: self.class.to_s,	delayable_id: self.id, run_at: self.executed_at) if is_push_email
      elsif notif_setting.send_type.eql?(2)
        Delayed::Job.enqueue(PushRewardMandrillJob.new(self, emails_and_device_tokens_group, notification_by_locale, nil, notif_setting), chain_id: self.chain_id, delayable_type: self.class.to_s,	delayable_id: self.id, run_at: self.executed_at) if is_push_email
      elsif notif_setting.send_type.eql?(3)
        #for fishbowl
      end
    else
      Delayed::Job.enqueue(PushRewardMailJob.new(self, emails_and_device_tokens_group, notification_by_locale,nil), chain_id: self.chain_id, delayable_type: self.class.to_s,	delayable_id: self.id, run_at: self.executed_at) if is_push_email
    end

    user_ids.each do |user_id|
      user = User.find(user_id) rescue nil
      RewardWallet.create_with_delay({
          :reward_id => self.id, :user_id => user_id,
          :status => RewardWallet::STATUS[:ACTIVE],
          :expiry_date => self.expiryDate}, true
      ) if user
    end
  end

  def group_message_pn_by_locale
    hash_map = {}
    self.chain.locales.each do |locale|
      notification = self.notification_locales.where("locale_id = ?", locale.id).first
      hash_map[locale.id] = {} if hash_map[locale.id].blank?
      hash_map[locale.id]["email_subject"] = notification.blank? ? "" : notification.email_subject
      hash_map[locale.id]["email_content"] = notification.blank? ? "" : notification.email_content
      hash_map[locale.id]["email_content_html"] = notification.blank? ? "" : notification.email_content_html
      hash_map[locale.id]["notification"] = notification.blank? ? "" : notification.notification
    end
    return hash_map
  end

  def gcm_push(reg_ids_by_locale, hash_notification,exp_at)
    reg_ids_by_locale.each do |key, value|
      unless value[:android_device_tokens].blank?
        puts "---Started sending PN---"
        msg = key.blank? ? "" : hash_notification[key]["notification"]
        push_to_android(value[:android_device_tokens], (msg % {
            :reward_name => self.name,
            :chain_name => self.chain.name,
            :expired_at => exp_at.nil?? self.expiryDate : exp_at,
            :reward_fine_print => self.fineprint
        }) )
        puts "---SENT---"
      end
      end
  end

  def push_email_group(reg_ids_by_locale, hash_notification, exp_at = nil)
    reg_ids_by_locale.each do |key, value|
      email_subject = key.blank? ? "" : hash_notification[key]["email_subject"]
      email_content = key.blank? ? "" : hash_notification[key]["email_content"]
      email_content_html = key.blank? ? "" : hash_notification[key]["email_content_html"]
      from = self.chain.email || Setting.email.default_from
      subject = email_subject % {
          :reward_name => self.name,
          :chain_name => self.chain.name,
          :expired_at => exp_at,
          :reward_fine_print => self.fineprint,
          :receipt_id_link => ''
      }
      body_content = (email_content % {
          :reward_name => self.name,
          :chain_name => self.chain.name,
          :expired_at => exp_at,
          :reward_fine_print => self.fineprint,
          :receipt_id_link => ''
        })

      body_content_html = (email_content_html % {
        :reward_name => self.name,
        :chain_name => self.chain.name,
        :expired_at => exp_at,
        :reward_fine_print => self.fineprint,
        :receipt_id_link => ''
      } rescue email_content_html)

      value[:emails].each do |email|
        mail =  Mail.new do
          to      email
          from    from
          subject subject
        end
        text_part = Mail::Part.new do
          body body_content
        end
        mail.text_part = text_part

        html_part = Mail::Part.new do
          content_type 'text/html'
          body body_content_html
        end
        mail.html_part = html_part
        mail.deliver!
      end
    end
  end

  def push_email_group_using_mandrill(reg_ids_by_locale, hash_notification, exp_at = nil, notif_setting)
    reg_ids_by_locale.each do |key, value|
      email_subject = key.blank? ? "" : hash_notification[key]["email_subject"]
      email_content = key.blank? ? "" : hash_notification[key]["email_content"]
      email_content_html = key.blank? ? "" : hash_notification[key]["email_content_html"]
      from = self.chain.email || Setting.email.default_from
      subject = email_subject % {
          :reward_name => self.name,
          :chain_name => self.chain.name,
          :expired_at => exp_at,
          :reward_fine_print => self.fineprint,
          :receipt_id_link => ''
      }
      body_content = (email_content % {
          :reward_name => self.name,
          :chain_name => self.chain.name,
          :expired_at => exp_at,
          :reward_fine_print => self.fineprint,
          :receipt_id_link => ''
      })

      body_content_html = (email_content_html % {
          :reward_name => self.name,
          :chain_name => self.chain.name,
          :expired_at => exp_at,
          :reward_fine_print => self.fineprint,
          :receipt_id_link => ''
      })

      mandrill_api = notif_setting.apikey rescue nil
      value[:emails].each do |email|

        unless mandrill_api.blank?
          begin
            mandrill = Mandrill::API.new(mandrill_api)
            template_content = []
            message = {
                "merge_vars"=> [{"vars"=>[{"content"=>"merge2 content", "name"=>"merge2"}], "rcpt"=>"recipient.email@example.com"}],
                "to"=> [{"type"=>"to", "email"=> email, "name"=> email}],
                "global_merge_vars" => [
                    { "name" => "APPNAME", "content" => self.chain.name },
                    { "name" => "CH_ADDR", "content" => "35 Hugus Aly Suite 300 Pasadena, USA" },
                    { "name" => "CH_PHONE", "content" => "411-111-111" },
                    { "name" => "REWARD_TIT", "content" => '' },
                    { "name" => "REWARD_EXP", "content" => exp_at },
                    { "name" => "REW_FINE_P", "content" => self.fineprint },
                    { "name" => "FNAME", "content" => '' },
                    { "name" => "LNAME", "content" => '' }
                ],
                "tracking_domain"=>nil,
                "subject"=> email_subject
            }
            async = false
            ip_pool = nil
            send_at = Time.now.utc
            mandrill.messages.send_template notif_setting.name, template_content, message, async, ip_pool, send_at

          rescue Mandrill::InvalidKeyError => e
            puts "A mandrill error occurred: #{e.class} - #{e.message}"
            mail =  Mail.new do
              to      email
              from    from
              subject subject
            end
            text_part = Mail::Part.new do
              body body_content
            end
            mail.text_part = text_part

            html_part = Mail::Part.new do
              content_type 'text/html'
              body body_content_html
            end
            mail.html_part = html_part
            mail.deliver!
          end
        else
          mail =  Mail.new do
            to      email
            from    from
            subject subject
          end
          text_part = Mail::Part.new do
            body body_content
          end
          mail.text_part = text_part

          html_part = Mail::Part.new do
            content_type 'text/html'
            body body_content_html
          end
          mail.html_part = html_part
          mail.deliver!
        end

      end
    end
  end

  def group_emails_and_device_token_by_locale(user_ids)
    hash_map = {}
    if !user_ids.blank?
      users = User.where("id in (?)", user_ids)
      users.each do |user|
        hash_map[user.locale.id] = {} if hash_map[user.locale.id].blank?
        hash_map[user.locale.id][:emails] = [] if hash_map[user.locale.id][:emails].blank?
        hash_map[user.locale.id][:android_device_tokens] = [] if hash_map[user.locale.id][:android_device_tokens].blank?
        hash_map[user.locale.id][:iphone_device_tokens] = [] if hash_map[user.locale.id][:iphone_device_tokens].blank?
        hash_map[user.locale.id][:emails] << user.email
        hash_map[user.locale.id][:android_device_tokens] << user.device_token if user.sign_in_device_type.to_s.downcase.eql?("android") and !user.device_token.blank?
        hash_map[user.locale.id][:iphone_device_tokens] << user.device_token if user.sign_in_device_type.to_s.downcase.eql?("iphone") and !user.device_token.blank?
      end
      hash_map
    end
  end

  def push_to_android(reg_ids, notification)
    if application = self.chain.applications.by_device_type(1).first
      return if application.key.blank?
      message = GcmHelper::Message.new
      message.delay_while_idle = true
      message.add_data('alert', notification)
      message.add_data('timestamp', "#{Time.now}")
      #      key="AIzaSyCHAQW9i1vGKNUD83jSsSn8Jfgb2TPFGKc"
      sender = GcmHelper::Sender.new(application.key)
      response = sender.multicast_with_retry(message, reg_ids, 3)
    end
  end

  def push_to_iphone2(reg_ids_by_locale, hash_notification,exp_at = nil)
    if application = self.chain.applications.by_device_type(2).first
      reg_ids_by_locale.each do |key, value|
        unless value[:iphone_device_tokens].blank?
          msg = key.blank? ? "" : hash_notification[key]["notification"]
          value[:iphone_device_tokens].each do |device_token|
            rapns_app = application.rapns_app
            return if rapns_app.blank?
            puts "push to iphone2 Reward " * 100

            n = Rpush::Apns::Notification.new
            n.app = rapns_app
            n.device_token = device_token
            n.alert = msg % {
                :reward_name => self.name,
                :chain_name => self.chain.name,
                :expired_at => exp_at,
                :reward_fine_print => self.fineprint
            }

            n.sound = "1.aiff"
            n.badge = 1
            n.save!
          end
        end
      end
    end
  end

  def fetch_message_user_locale(user)
    unless user.locale.blank?
      msg = self.notification_locales.where("locale_id = ?", user.locale.id).first.notice
    end
    return msg
  end

  def fetch_push_reward_template(chain_id, user_locale, email_name)
    body = REDIS.hget "chain_#{chain_id}", "#{user_locale}_#{email_name}_body"
    return body
  end

  def push_user_reward(users)
    unless users.blank?
      users = [users] if users.class == String
      users.each do |user|
        RewardWallet.create(
          :reward_id => self.id, :user_id => user,
          :status => RewardWallet::STATUS[:INACTIVE],
          :expiry_date => self.expiryDate
        )
      end
    end

    def push_restaurant_reward(restaurant_ids)
      restaurants = Restaurant.where("restaurant_id in (?)", restaurant_ids)
    end
  end

  def self.of_user(user_id, include_removed=true, include_claimed=true, include_gifted=true, twenty_day_exp = false, include_redeeming=true)

    status = [RewardWallet::STATUS[:ACTIVE]]
    status << RewardWallet::STATUS[:REMOVED] if include_removed
    status << RewardWallet::STATUS[:CLAIMED] if include_claimed
    status << RewardWallet::STATUS[:GIFTED] if include_gifted
    status << RewardWallet::STATUS[:REDEEMING] if include_redeeming

    ## rewards that are expired more than 20 days, is not shown on app
    if twenty_day_exp == true
      reward_wallets= RewardWallet.includes(:reward).by_status(status).by_user_id(user_id).
          where("date(expiry_date) > ?", (Time.zone.now.utc - 20.days).to_date)
    else
      reward_wallets= RewardWallet.includes(:reward).by_status(status).by_user_id(user_id)
    end

    rewards = []

    reward_wallets.each do |rw|
      reward= rw.reward.dup
      reward.id = rw.reward.id
      reward.isExpired ||= rw.expiry_date.to_date < Time.zone.now.to_date if rw.expiry_date # expired in wallet
      reward.expiryDate = rw.expiry_date  if rw.expiry_date
      reward.gifter = rw.gifter

      ## new reward notif, only non regular rewards
      reward.new_reward = rw.is_new_reward? if reward.reward_type != Reward::TYPES["REGULAR"]
      puts reward
      if rw.gifter && !reward.expired
        rewards << reward
      elsif !rw.gifter
        rewards << reward
      end
    end
    puts rewards
    rewards
  end

  def self.expired_reward_and_not_redeemed_of_user(user_id)
    reward_wallets= RewardWallet.includes(:reward).by_user_id(user_id).expired_reward.not_claimed
    rewards = []

    reward_wallets.each do |rw|
      reward= rw.reward.dup
      reward.id = rw.reward.id
      reward.isExpired ||= rw.expiry_date.to_date < Time.zone.now.to_date if rw.expiry_date # expired in wallet
      reward.expiryDate = rw.expiry_date  if rw.expiry_date
      reward.gifter = rw.gifter

      ## new reward notif, only non regular rewards
      reward.new_reward = rw.is_new_reward? if reward.reward_type != Reward::TYPES["REGULAR"]
      puts reward
      if rw.gifter && !reward.expired
        rewards << reward
      elsif !rw.gifter
        rewards << reward
      end
    end
    puts rewards
    rewards
  end

  def expired
    isExpired.nil? ?  (expiryDate.nil? ? false : expiryDate.to_date < Date.today) : isExpired
  end

  def is_expired?
    if expiryDate.nil?
      return false
    elsif expiryDate.to_date > Date.today
      return false
    else
      return true
    end
  end

  def in_effect
    isEffective.nil? ? (effectiveDate.nil? ? false : effectiveDate.to_date <= Date.today) : isEffective
  end

  ## search scoping
  scope :date_from, -> (value) { 
    if (!value.blank?)
      # where("'rewards.effectiveDate' >= ?", value.to_datetime)
      where("DATE(\"effectiveDate\") >= ?", value.to_date)
    end
  }

  scope :combined, -> (value) { 
    # intvalue = value.to_i
    if (!value.blank?)
      value = value.to_s.strip
      if /\D/.match(value)
        where("lower(rewards.name) ilike ?", '%'+value.to_s.downcase+'%')
      else
        where("rewards.id = ? OR lower(rewards.name) ilike ?", value.to_i, '%'+value.to_s.downcase+'%')
      end
    end
  }

  scope :date_to, -> (value) { 
    if (!value.blank?)
      # where('"rewards"."expiryDate"' <= ?", value.to_datetime)
      where("DATE(\"expiryDate\") <= ?", value.to_date)
    end
  }

  scope :by_type, -> (value) { 
    if (!value.blank?)
      where('"rewards"."reward_type" in (?)', value)
    end
  }

  scope :search, -> (params) { 
    date_from(params[:from]).date_to(params[:to]).combined(params[:q]).by_type(params[:reward_type])
  }

  def chart_redeemed_or_not
    chain = self.chain
    user_count = chain.users.count
    claimed_user = self.reward_wallets.is_claimed.count
    redemeed_percentage = (claimed_user / user_count) * 100
    not_redemeed_percentage = ((user_count - claimed_user)/user_count) * 100
    return [
      {:name => "Redeemed", :y => redemeed_percentage, :color => "#A5AC89"},
      {:name => "Not Yet Redeemed", :y => not_redemeed_percentage, :color => "#8B9AB6"},
    ].to_json
  end

  def custom_name
    if self.reward_type.eql?(Reward::TYPES["GIFTABLE"]) and self.gifter
      "TAP TO GIFT - #{self.name}"
    elsif self.reward_type.eql?(Reward::TYPES["GIFTABLE"]) and !self.gifter
      "GIFT - #{self.name}"
    else
      self.name
    end
  end

  def giftable?
    self.reward_type.eql?(Reward::TYPES["GIFTABLE"])
  end

  def saving_images(id)
    begin
      id = nil if id.blank?
      aw, bucket = Reward.initialize_aws
      url_s3 = aw.buckets[bucket].objects["#{id}_rewards_temporary"].url_for(:read).to_s

      if !aw.buckets[bucket].objects["#{id}_rewards_temporary"].exists?
        #puts "saving images == Blank"
        #url_s3 = aw.buckets[bucket].objects["_rewards_temporary"].url_for(:read).to_s
        # move the keys
        obj = aw.buckets[bucket].objects["_rewards_temporary"]
        obj.move_to("#{id}_rewards_temporary",:bucket_name => bucket)
        aw.buckets[bucket].objects["_rewards_temporary"].delete
        url_s3 = aw.buckets[bucket].objects["#{id}_rewards_temporary"].url_for(:read).to_s
      end
      File.open("tmp/reward_images.jpg","wb"){|f| f.write(open(url_s3).read)}
      File.open("tmp/reward_images.jpg") do |f|
        self.attachment = f # just assign the logo attribute to a file
      end #file gets closed automatically here
    #end
    rescue => e
      puts "#{e.inspect}"
      end
    self.save!
  end

  def saving_images_thumbnail(id)
    begin
      id = nil if id.blank?
      aw, bucket = Reward.initialize_aws
      url_s3 = aw.buckets[bucket].objects["#{id}_rewards_thumbnail_temporary"].url_for(:read).to_s

      if !aw.buckets[bucket].objects["#{id}_rewards_thumbnail_temporary"].exists?
        #puts "saving images == Blank"
        #url_s3 = aw.buckets[bucket].objects["_rewards_temporary"].url_for(:read).to_s
        # move the keys
        obj = aw.buckets[bucket].objects["_rewards_thumbnail_temporary"]
        obj.move_to("#{id}_rewards_thumbnail_temporary",:bucket_name => bucket)
        aw.buckets[bucket].objects["_rewards_thumbnail_temporary"].delete
        url_s3 = aw.buckets[bucket].objects["#{id}_rewards_thumbnail_temporary"].url_for(:read).to_s
      end
      File.open("tmp/reward_thumbnail_images.jpg","wb"){|f| f.write(open(url_s3).read)}
      File.open("tmp/reward_thumbnail_images.jpg") do |f|
        self.thumbnail = f # just assign the logo attribute to a file
      end #file gets closed automatically here
        #end
    rescue => e
      puts "#{e.inspect}"
    end
    self.save!
  end


  def self.initialize_aws
    ## establish connection to AWS, return the Aws obj, and bucket
    if Rails.env != "production"
      secret_key = Setting.storage.s3_access_key_id
      access_key = Setting.storage.s3_secret_access_key
      bucket = Setting.storage.s3_bucket
    else
      secret_key = ENV["S3_KEY"]
      access_key = ENV["S3_SECRET"]
      bucket = ENV["S3_BUCKET"]
    end


    aw = AWS::S3.new(
        :access_key_id => secret_key,
        :secret_access_key => access_key
    )
    return aw, bucket
  end

  def new_reward_hash
    timer = self.chain.reward_timer
    if timer > 0
      ##
    else
      return false
    end
  end

  def additional_informations
    self.reward_informations.select("id, title,app_text, description")
  end

  def giftable_notification_template(user)
    gift_notif = self.notification_locales.where(:locale_id => user.locale_id).first
    {:warn_message => (gift_notif.notification rescue ""), :subject => (gift_notif.email_subject rescue ""), :content => (gift_notif.email_content rescue "")}
  end

  def check_uniq_birthday_reward
    self.chain.birthday_rewards.each do |r|
      puts "#{r.id}"
      puts "#{self.id}"
      puts "-----------------------"
      if r.id != self.id
        self.update_attribute(:status, "inactive")
      end
    end
  end

  def is_net_pos_reward?
    if self.chain.pos_used_type == Chain::POS_USED_TYPE["NETPOS"]
      return true
    else
      return false
    end
  end


  def self.pos_claim_staffcode_generate(chain, user)
    valid = false
    barcode, length = Barcode.generate_reward_code(chain)
    time_start = Time.zone.now
    while valid == false
      if RewardCodeUser.where(:chain_id => chain.id, :staffcode => barcode).count > 0
        barcode, length = Barcode.generate_reward_code(chain)
      else
        begin
          RewardCodeUser.create!(:chain_id => chain.id, :staffcode => barcode, :user_id => user.id)
          valid = true
        rescue ActiveRecord::RecordNotUnique => e
          valid = false
          barcode, length = Barcode.generate_reward_code(chain)
        end
      end
      time_end = Time.zone.now
      if time_end - time_start > 25
        barcode = "NOCODE"
        length = 0
        valid = true
      end
    end
    [barcode, length]
  end

  def static_pos_code_setup?
    !self.POSCode.blank?
  end

  def claimed_by(user)
    reward = self
    case reward.reward_type
      when Reward::TYPES["REGULAR"], Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"],Reward::TYPES["BIRTHDAY"]
        user.burn(reward.points)
        rw = RewardWallet.where("user_id = ? AND reward_id = ? AND status <> ? AND date(expiry_date) >= ?", user.id, reward.id, RewardWallet::STATUS[:CLAIMED], Date.current).order("created_at asc").first
        rw.update_attributes(:status => RewardWallet::STATUS[:CLAIMED]) unless rw.blank?
    end
  end

  def void_redeem_reward(user)
    reward = self
    case reward.reward_type
      when Reward::TYPES["REGULAR"], Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"],Reward::TYPES["BIRTHDAY"]
        user.earn(reward.points)
        PointHistory.add_to_history(user.id, "Reward ##{reward.id} Void", reward.points )
        rw = RewardWallet.where("user_id = ? AND reward_id = ? AND status = ? AND date(expiry_date) >= ?", user.id, reward.id, RewardWallet::STATUS[:CLAIMED], Date.current).order("created_at asc").first
        rw.update_attributes(:status => RewardWallet::STATUS[:ACTIVE]) unless rw.blank?
    end
  end


  def get_fishbowl_promotion_code(user, restaurant)
    unused_count = FishbowlPromotionCode.select("id").where("reward_id = #{self.id} AND user_id IS NULL").count
    barcode = FishbowlPromotionCode.find_by_sql("select id, code from fishbowl_promotion_codes where reward_id = #{self.id} AND user_id IS NULL offset random() * #{unused_count} limit 1").first
    if barcode
      FishbowlPromotionCode.update_all({:user_id => user.id, :used_at => Time.current, :restaurant_id => (restaurant.id rescue nil)}, "id = #{barcode.id}")
      barcode.code
    else
      "NOCODE"
    end
  end

  def generate_fishbowl_promotion_code
    chain = self.chain
    fb_coupon = Fishbowl::Api::Coupon.new(
        :username => chain.try(:fishbowl_promotion_setting).try(:username),
        :password => chain.try(:fishbowl_promotion_setting).try(:password),
        :host => "https://promotionsmanagerapi.fishbowl.com",
        :promotion_id => self.try(:fishbowl_reward_promotion_setting).try(:promotion_id),
        :brandid => chain.try(:fishbowl_promotion_setting).try(:brand_id),
        :count => self.try(:fishbowl_reward_promotion_setting).try(:code_batch_size),
        :client_id => chain.try(:fishbowl_promotion_setting).try(:client_id),
        :secret_id => chain.try(:fishbowl_promotion_setting).try(:client_secret)
    )
    fb_coupon.login
    coupons_batch = fb_coupon.batch(self)
    coupons_batch["coupons"].each do |coupon|
      FishbowlPromotionCode.create(
          :code => coupon["coupon_code"],
          :reward_id => self.id,
          :chain_id => chain.id
      )
    end if !coupons_batch.blank? && !coupons_batch["coupons"].blank?
  end

  def valid_days_of_week_and_time
    hours = []
    self.partner_reward.partner_hours.each do |hour|
      hours << {dow: hour.day_of_week, start: hour.start, end: hour.end}
    end if self.partner_reward
    return hours
  end

  def valid_all_locations
    self.partner_reward.try(:valid_all_locations)
  end

  def valid_locations
    self.partner_reward.try(:valid_locations).parameterize.split("-") rescue []
  end

  def discount_formula
    formula = {}
    formula["order_minimum"] = self.partner_reward.try(:order_minimum)
    formula["discount_type"] = self.partner_reward.try(:discount_type)
    formula["discount_value"] = self.partner_reward.try(:discount_value)
    formula["minimum_count_order_from_category"] = self.partner_reward.try(:minimum_count_order_from_category)
    formula["discount_count_from_category"] = self.partner_reward.try(:discount_count_from_category)
    formula["discount_order"] = self.partner_reward.try(:discount_order)
    formula["discount_source_price"] = self.partner_reward.try(:discount_source_price)
    return formula rescue []
  end

  def order_from_category
    categories = []
    self.partner_reward.partner_reward_categories.each do |cat|
      categories << {"categoryId" => cat.category_id, "type" => cat.mandatory ? 1 : 0}
    end if self.partner_reward && self.partner_reward.partner_reward_categories
    return categories
  end

  def discount_from_category
    discounts = []
    self.partner_reward.partner_reward_discounts.each do |disc|
      discounts << {"categoryId" => disc.category_id, "type" => disc.mandatory ? 1 : 0}
    end if self.partner_reward && self.partner_reward.partner_reward_discounts
    return discounts rescue []
  end

  def start_date
    self.effectiveDate
  end

  def end_date
    self.expiryDate
  end

  def title
    self.name
  end

  def create_reward_info_locale
    chain = self.chain
    chain.locales.each do |locale|
      reward_info_locale = RewardInfoLocale.where(:reward_id => self.id, :locale_id => locale.id).first
      if reward_info_locale.blank?
        p "create new reward locale #{locale.id} for reward #{self.id}"
        reward_info_locale = RewardInfoLocale.create(:reward_id => self.id, :locale_id => locale.id, :name => self.name, :fineprint => self.fineprint, :description => self.description)
      end
    end unless chain.blank?
  end
end
