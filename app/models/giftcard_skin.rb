class GiftcardSkin < ActiveRecord::Base

  belongs_to :chain

  attr_accessor :img_from_mobile
  validates :title, :length => {:maximum => 200}, allow_blank: true
  validates :description, :length => {:maximum => 500}, allow_blank: true

  default_scope { order('updated_at DESC') }

  ### paperclip
  has_attached_file :app_image,
                    :styles => { :small => Setting.paperclip.styles.small,
                                 :large => Setting.paperclip.styles.large },
                    :path => ":class/:app_image/:year/:month/:chain_:timestamp_:style.:img_content_type",
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                        :access_key_id => Setting.storage.s3_access_key_id,
                        :secret_access_key => Setting.storage.s3_secret_access_key
                    }

  has_attached_file :email_header_image,
                    :styles => { :small => Setting.paperclip.styles.small,
                                 :large => Setting.paperclip.styles.large },
                    :path => ":class/:email_header_image/:year/:month/:chain_:timestamp_:style.:img_content_type",
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                        :access_key_id => Setting.storage.s3_access_key_id,
                        :secret_access_key => Setting.storage.s3_secret_access_key
                    }

  validates_attachment_content_type :app_image, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :on=>:create
  validates_attachment_content_type :email_header_image, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :on=>:create

end
