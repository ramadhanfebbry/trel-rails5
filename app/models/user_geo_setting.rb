class UserGeoSetting < ActiveRecord::Base
  belongs_to :user
  belongs_to :geo_data_setting

  DEVICE_TYPES = {1 => "android",2 =>  "iphone"}
end