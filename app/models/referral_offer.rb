class ReferralOffer < ActiveRecord::Base
  has_one :referral_program, :as => :incentive
  has_one :referral_program, :as => :incentive_referer
  belongs_to :chain
  
  default_scope { where("referral_offers.deleted_at" => nil) }

  validates :title, :presence => true
  validates :points, :presence => true,
      :numericality => {:greater_than => -1, :message => "Must be a number and greater than or equal 0"},
      :if => Proc.new { |deal_of| deal_of.kind ==  1 }
  
  TYPES = {
    "FIXED_POINTS" => 1 # only once per user, good for welcome bonus reward    
  }
end
