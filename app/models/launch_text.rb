class LaunchText < ActiveRecord::Base

  belongs_to :locale

  validates :locale_id, :title, :subtitle, :presence => true
end
