class Checkin < ActiveRecord::Base
  has_one :deal, :as => :activity
  has_one :deal_asset_android, :as => :activity
  has_one :deal_asset_iphone, :as => :activity
 
  accepts_nested_attributes_for :deal
  accepts_nested_attributes_for :deal_asset_android
  accepts_nested_attributes_for :deal_asset_iphone

  validates :max_user_distance, :presence => true, :numericality => {:greater_than => -1, :message => "Must be a number and greater than 0"}
  validates :deal_asset_android, :presence => true
  validates :deal_asset_iphone, :presence => true
  
  # attr_accessible :deal_attributes, :deal_asset_android_attributes, :deal_asset_iphone_attributes, :minimum_distance, :max_user_distance, :fine_print

  alias_attribute :max_user_distance, :minimum_distance

  default_scope { where("checkins.deleted_at" => nil) }
  def self.deleted
    self.unscoped.where('checkins.deleted_at IS NOT NULL')
  end
  
end
