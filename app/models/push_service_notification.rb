class PushServiceNotification < ActiveRecord::Base
  # serialize  :receiver_user_ids, Array

  belongs_to :chain
  belongs_to :admin

  attr_accessor :xls, :xls_path, :select_type, :time_zone_default, :job_summary, :temp_receiver_user_ids
  # attr_accessible :xls, :xls_path, :select_type, :time_zone_default, :job_summary, :temp_receiver_user_ids,
                  # :chain_id, :date_text_ttd, :admin_id, :recipient_count, :receiver_user_ids, :sent,
                  # :sent_at, :owner_email, :is_all_user, :pentaho_promo_name, :pentaho_promo_id, :pentaho_schedule_id,
                  # :schedule_id, :created_from

  validates :service_type, :presence => true

  SERVICE_TYPES = {"Points" => 1, "Rewards" => 2, "Notification" => 3, "Points + Notification" => 4, "Rewards + Notification" => 5 }

  def get_job_summary
    @job_summary ||= PsnJobSummary.where(:job_id => self.id)
  end

  def success_user_count
    get_job_summary
    @job_summary.map(&:success_count).sum
  end

  def failed_user_count
    get_job_summary
    @job_summary.map(&:failure_count).sum
  end

  def waiting_user_count
    get_job_summary
    @job_summary.map(&:waiting_count).sum rescue "counting..."
  end

  def live_progress_percent
    get_job_summary
    "#{((success_user_count.to_f)/self.recipient_count.to_f)*100}%"
  end

  def parsed?
    get_job_summary
    @job_summary.size != 0
  end

  def finished?
    get_job_summary
    @job_summary.size == @job_summary.map(&:control_flag).select{|a| a == 2} || @job_summary.map(&:finished_at).select{|a| a.blank?}.size == 0
  end

  def paused?
    get_job_summary
    !@job_summary.map(&:control_flag).select{|a| a == 0}.blank?
  end

  def canceled?
    get_job_summary
    !@job_summary.map(&:control_flag).select{|a| a == 3}.blank?
  end

  def resumed?
    get_job_summary
    !@job_summary.map(&:control_flag).select{|a| a == 1}.blank?
  end

  def scheduled?
    parsed? ? false : true
  end

  def cancel
    PsnJobSummary.select("control_flag").where("job_id = ? AND control_flag != ?", self.id, 2 ).update_all("control_flag = 3")
  end

  def resume
    PsnJobSummary.select("control_flag").where("job_id = ? AND control_flag != ?", self.id, 2 ).update_all("control_flag = 1")
    self.update_column(:sent_at, self.sent_at + 2.minutes > Time.zone.now ? self.sent_at + 2.minutes : Time.zone.now + 2.minutes )
  end

  def pause
    PsnJobSummary.select("control_flag").where("job_id = ? AND control_flag != ?", self.id, 2 ).update_all("control_flag = 0")
  end

  def processed?
    get_job_summary.present?
  end

  def status?
    get_job_summary
    status_type = "job running, no api response yet"
    !@job_summary.map(&:status).each do |status|
      if status.eql?(0) || status.eql?(1)
        status_type = "job running, no api response yet"
      elsif status.eql?(2)
        status_type = "job success"
      elsif status.eql?(3)
        status_type = "job canceled"
      end
    end
    status_type
  end

end
