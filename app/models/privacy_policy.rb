class PrivacyPolicy < ActiveRecord::Base

  validates_uniqueness_of :locale_id
  belongs_to :locale

  def self.content_by_locale(locale_key)
    privacy_policies = PrivacyPolicy.includes(:locale).where('locales.key=?', locale_key).first
    "#{privacy_policies.try(:content)}"
  end

end
