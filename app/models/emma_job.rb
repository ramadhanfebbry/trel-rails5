class EmmaJob < Struct.new(:chain, :log)

  def perform
    p "perform----------"
    log.update_attributes(:status => "processing", :initiate_at => Time.now)
    EmmaMarketing.adding_process(chain.reload, log.reload)
  end

  def error(job, exception)
    p "---------email marketing erroor handling-------"
    struct = YAML::load(job.handler)
    chain = struct.chain
    log = struct.log
    log.update_attributes(:status =>  "failed", :completed_at => Time.now, :error_report => exception.to_s)  unless log.blank?
    p log
    ChainMailer.emma_sync_log_error_report(chain, exception.to_s).deliver
    p "-----------------finishhhhh------"
  end


end