class ReportDefinition < ActiveRecord::Base
  require 'rest-client'

  serialize :query  , JSON

  validates :query, :jasper_path,:name, :presence => true

  TYPES = {
      "daily" => 1,
      "weekly" => 2,
      "periodic" => 3
  }

  SEND_TYPES = {
      "email" => 1,
      "link" => 2,
      "calendar" => 3,
      "ftp" => 4
  }

  has_one :report_subscription, :dependent => :destroy


  #def report_url(format_type = nil)
  #    return Setting.jasper.server + "/rest/resource"
  #end
  #
  #def response_resource_url
  #  return Setting.jasper.server + "/rest/report"
  #end

  #def report_url(format_type = nil)
  #  return Setting.jasper.server + "/rest/resource"
  #end
  #
  #def self.server_information
  #  RestClient.get Setting.jasper.server + "/rest_v2/serverInfo"
  #end
  #
  #def v2_rest
  #  return "/rest_v2/reports/reports"
  #end
  #
  #def v2_job
  #  return "/rest_v2/jobs"
  #end
  #
  #def format(file_type)
  #  file_type  ||= "pdf"
  #  return "."+file_type
  #end
  #
  #def report_job_urls
  #  return Setting.jasper.server + v2_job + v2_job_parameters
  #end
  #
  #def self.running_reports
  #  ## def to see the running reports on jasper server
  #
  #end

  def get_s3_information
    if Rails.env != "production"
      secret_key = Setting.storage.s3_access_key_id
      access_key = Setting.storage.s3_secret_access_key
      bucket = Setting.storage.s3_bucket
    else
      secret_key = ENV["S3_KEY"]
      access_key = ENV["S3_SECRET"]
      bucket = ENV["S3_BUCKET"]
    end
    return secret_key, access_key, bucket
  end

  def show_parameters
    arr = self.query.gsub(' ','').split(',')
    params = {}
    arr.each do |ar|
      pr = ar.split(':')
      params.merge!({pr[0].to_sym => pr[1]})
    end
    return params
  end

  def v2_path
    return "#{Setting.jasper.server}/rest_v2/reports"
  end

  def jasper_input_control
    v2_path + self.jasper_path
    cookies =  auth_jasper
    res = RestClient.get v2_path , cookies
    return res
  end

  def input_control_path
    v2_path + self.jasper_path
  end

  def jasper_input_control
    cookies = auth_jasper
    res = RestClient.get input_control_path, cookies
    puts res.code
    puts res
  end

  def auth_jasper
      report_server = Setting.jasper.server
      report_user = Setting.jasper.username
      report_password = Setting.jasper.password

      response_login =
          RestClient.post "#{report_server}/login.html",
                          :j_username => report_user,
                          :j_password => report_password

      cookies = {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}
      return cookies
  end

  #def report_uuid_url(uuid)
  #  "#{Setting.jasper.server}/rest/report/#{uuid}?file=report"
  #end
end
