class UserLocation < ActiveRecord::Base
    reverse_geocoded_by :latitude, :longitude do |obj,results|
      if geo = results.first
        p "GEO IS #{geo}"
        obj.city    = geo.city
        obj.postal_code = geo.postal_code
        obj.country_code = geo.country_code
        obj.address = geo.address
      end
    end
    #attr_accessible :address, :latitude, :longitude, :user_id


    after_validation :reverse_geocode
  end

