class SurveyResponseAlert < ActiveRecord::Base

  belongs_to :chain
  has_many :notification_locales, :as => :notifiable, :dependent => :destroy

  accepts_nested_attributes_for :notification_locales

  validate :validate_for_alert_comment_and_negative_review

  def validate_for_alert_comment_and_negative_review
    if send_email_alert
      notification_locales.each do |notification_locale|
        if notification_locale.email_subject.blank?
          errors.add("", " : Email Subject for #{notification_locale.locale.name} should not be blank ")
        end
        if notification_locale.email_content.blank?
          errors.add("", " : Email Content for #{notification_locale.locale.name} should not be blank ")
        end

      end
    end
  end

  def get_email_subject_by_locale(locale)
    notification_locale = self.notification_locales.where(:locale_id => locale).first
    notification_locale.email_subject
  end

  def get_email_content_by_locale(locale)
    notification_locale = self.notification_locales.where(:locale_id => locale).first
    notification_locale.email_content
  end

end