class NoScanScheduler < ActiveRecord::Base
  belongs_to :no_job_definitions, :foreign_key => 'job_id'
  #belongs_to :no_scan_definition, :foreign_key => 'no_scan_id'

  def self.set_scheduler(params)
    owner_schedule_build = {}

    case params["no_scan_definition"]["schedule_type"].to_i
      when 1 # one time
        hour = params["by_one_time_time"]['(4i)'].to_i
        min = params["by_one_time_time"]['(5i)'].to_i
        time_to_run = Time.zone.now.change(:hour => hour, :min => min)

        task_schedule_build = {
            :run_type => 'onetime',
            :executed_at => time_to_run,
            :next_run => time_to_run + 1.days,
        }
      when 2 # daily
        hour = params["by_daily_time"]['(4i)'].to_i
        min = params["by_daily_time"]['(5i)'].to_i
        time_to_run = Time.zone.now.change(:hour => hour, :min => min)

        task_schedule_build = {
            :run_type => 'daily',
            :executed_at => time_to_run,
            :next_run => time_to_run + 1.days,
        }
      when 3 # weekly
        hour = params["by_weekly_time"]['(4i)'].to_i
        min = params["by_weekly_time"]['(5i)'].to_i
        day = params["by_weekly_day"].to_i
        task_schedule_build = {
            :run_type => 'weekly',
            :executed_at => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days).change(:hour => hour, :min => min),
            :next_run => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days) + 1.weeks
        }
      when 4 # monthly
        hour = params["by_monthly_time"]['(4i)'].to_i
        min = params["by_monthly_time"]['(5i)'].to_i
        day = params["by_monthly_day"].to_i

        week_day = Time.zone.now.beginning_of_month.wday
        days_params = {}
        diff = day - week_day
        if day >= week_day and Time.zone.now.beginning_of_month >= Time.zone.now
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + diff.days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + diff.days)..change(:hour => hour, :min => min)
          }

        elsif day < week_day and Time.zone.now.beginning_of_month.to_date < Time.zone.now.to_date
          next_wday = (Time.zone.now.beginning_of_month + 1.months).wday
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + ((7 - week_day) + day).days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + ((7 - next_wday) + day).days).change(:hour => hour, :min => min),
          }
        else
          next_wday = (Time.zone.now.beginning_of_month + 1.months).wday
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + ((7 - week_day) + day).days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + ((7 - next_wday) + day).days).change(:hour => hour, :min => min),
          }
        end

        #start_month =
        task_schedule_build = {
            :run_type => 'monthly' #,
            # :executed_at => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days).change(:hour => hour, :min => min),
            # :next_run => (Time.zone.now.beginning_of_month.in_time_zone + (30.days)).change(:hour => hour, :min => min),
        }.merge!(days_params)
    end
    os = NoScanScheduler.new(task_schedule_build.merge!(:start_at => params["start_date"], :end_at => params["end_date"]))
  end
end
