class PartnerCategory < ActiveRecord::Base

  belongs_to :chain

  has_many :partner_reward_categories, :foreign_key => 'category_id'
  has_many :partner_reward_discounts, :foreign_key => 'category_id'

  has_many :partner_sub_categories

  validates :chain_id, :name, presence: true

  def title
    self.name
  end

end