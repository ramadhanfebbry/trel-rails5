class Answer < ActiveRecord::Base

  belongs_to :question
  belongs_to :surveys_user
  has_many :answer_text

  TYPES = {"Comments"=>1, "Multiple Choice"=>2, "Dropdown" => 3, "Slider" => 4, "Multiple Dropdown" => 5}

  default_scope { where(:deleted_at => nil) }
  scope :multiple_choices, -> { where(:answer_type => TYPES["Multiple Choice"]) }
  scope :text_answers, -> { where(:answer_type => TYPES["Comments"]) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def value
    case self.answer_type
      when TYPES['Comments']
        AnswerText.find(self.value_id).try(:text) rescue ""
      when TYPES['Multiple Choice'], TYPES['Slider']
        value_type = Question.find(self.question_id).label_or_value rescue "value"
        QuestionChoice.find(self.value_id).try(value_type.to_sym) rescue 0
      when TYPES['Dropdown']
        value_type = Question.find(self.question_id).label_or_value rescue "value"
        QuestionChoice.find(self.value_id).try(value_type.to_sym) rescue ""
      when TYPES["Multiple Dropdown"]
        value_type = Question.find(self.question_id).label_or_value rescue "value"
        QuestionChoice.find(self.value_id).try(value_type.to_sym) rescue ""
      else
        raise 'unknown question/answer type'
    end
  end

  def self.saveAnswer(question, value, survey_user_id)
    answer = Answer.new
    answer.answer_type= question.question_type
    answer.question= question

    surveys_user = SurveysUser.find survey_user_id
    user_rotating_question = surveys_user.survey.max_rotating_questions.eql?(0) ? nil : UserRotatingQuestion.where(:question_id => question.id, :user_id => surveys_user.user_id).first

    answer.transaction do
      case answer.answer_type
        when TYPES['Multiple Choice']
          answer.value_id=value.to_i
          answer.surveys_user_id = survey_user_id
          answer.save
          user_rotating_question.update_attributes(:completed => true, :answer_id => answer.id) if user_rotating_question
        when TYPES['Dropdown']
          answer.value_id=value.to_i
          answer.surveys_user_id = survey_user_id
          answer.save
          user_rotating_question.update_attributes(:completed => true, :answer_id => answer.id) if user_rotating_question
        when TYPES['Comments']
#        answer.save
          answerText= AnswerText.new
          answerText.answer= answer
          ## make value nil , handle null string from mobile app
          value = nil if value.eql?("(null)") || value.eql?("null")
          answerText.text= value
          answerText.save
          answer.value_id= answerText.id
          answer.surveys_user_id = survey_user_id
          answer.save
          user_rotating_question.update_attributes(:completed => true, :answer_id => answer.id) if user_rotating_question && value
        when TYPES['Slider']
          answer.value_id=value.to_i
          answer.surveys_user_id = survey_user_id
          answer.save
          user_rotating_question.update_attributes(:completed => true, :answer_id => answer.id) if user_rotating_question
        when TYPES['Multiple Dropdown']
          values = value.split("-")
          values.each do |val|
            answer = Answer.new
            answer.answer_type= question.question_type
            answer.question= question
            answer.value_id = val.to_i
            answer.surveys_user_id = survey_user_id
            answer.save
          end if values.is_a?(Array) && !values.blank?
          user_rotating_question.update_attributes(:completed => true, :answer_id => answer.id) if user_rotating_question
        else
          #raise 'unknown question/answer type'
          return false
      end

    end

  end

end