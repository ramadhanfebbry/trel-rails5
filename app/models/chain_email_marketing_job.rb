class ChainEmailMarketingJob < Struct.new(:chain)

  def perform
    EmailMarketing.import_to_email_marketing(chain)
  end

end