class ActiveRewardNotification < ActiveRecord::Base

  belongs_to :chain
  belongs_to :locale

  validates :notification, :cancel_button_text, :continue_button_text, :chain_id, :locale_id, presence: true
end