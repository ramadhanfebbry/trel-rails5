class RevelLog < ActiveRecord::Base

  serialize :success_users
  serialize :failed_users

  belongs_to :chain

  attr_accessor :img_from_mobile

  has_attached_file :attachment,
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                        :access_key_id => Setting.storage.s3_access_key_id,
                        :secret_access_key => Setting.storage.s3_secret_access_key
                    }

end
