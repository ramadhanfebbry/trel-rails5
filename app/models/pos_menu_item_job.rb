class PosMenuItemJob < Struct.new(:chain, :xml_data, :is_olo)

  def perform
    if is_olo.to_s == "true"
      chain.pos_locations.each do |pos_location_id|
        pos_location = PosLocation.find(pos_location_id)
        pos_menu = PosMenu.create(
            :pos_location_id => pos_location.id,
            :chain_id => pos_location.chain_id,
            :menu_data => xml_data
        )
        Delayed::Job.enqueue(MenuExtractionJobOlo.new(pos_menu))
        PosMenuUpload.create(
            :pos_location_id => pos_location.id,
            :chain_id => pos_location.chain_id,
            :xml_data => xml_data
        )
      end
    else
      chain.pos_locations.each do |pos_location_id|
        pos_location = PosLocation.find(pos_location_id)
        pos_menu = PosMenu.create(
            :pos_location_id => pos_location.id,
            :chain_id => pos_location.chain_id,
            :menu_data => xml_data
        )
        Delayed::Job.enqueue(MenuExtractionJob.new(pos_menu))
        PosMenuUpload.create(
            :pos_location_id => pos_location.id,
            :chain_id => pos_location.chain_id,
            :xml_data => xml_data
        )
      end
    end

  end

end