class ChainImageShareLocale < ActiveRecord::Base
  belongs_to :chain_image_share_locale
  belongs_to :locale

  attr_accessor :locale_key
  validates :locale_id, :presence => true
end
