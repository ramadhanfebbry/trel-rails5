class OloWebhook < ActiveRecord::Base
  serialize :content
  belongs_to :chain

  scope :search_chain, lambda { |chain_id|  where(chain_id: chain_id) if chain_id.present?}
  scope :search_email, lambda { |email| where(email: email) if email.present?}
  scope :search_restaurant, lambda { |restaurant| where(restaurant: restaurant) if restaurant.present?}

  before_save :update_email_and_restaurant

  def update_email_and_restaurant
    self.restaurant = self.content["location"]["name"] rescue nil
    self.email = self.content["customer"]["email"] rescue nil
    if chain
      self.is_match_restauran = true if !chain.restaurants.where(name: self.restaurant ).empty?
    end

  end

  def delay_receipt
    self.delay.receipt_process
  end

  def receipt_process
    #    user = User.find(x.user_id) rescue nil
    o = self
    x = OloParser::ParserOlo.new(o.content)
    restaurant = Restaurant.where("LOWER(name) like LOWER(?) and chain_id = #{o.chain_id}", "%#{x.store_name}%").first rescue nil
    chain  = Chain.find(o.chain_id) rescue nil
    user = chain.users.where("users.email = ?", x.get_email.downcase).first rescue nil

    #offer = restaurant.offers.unscoped.online_offers.last rescue nil
    unless restaurant.blank?
      #offer = Offer.unscoped.joins(:restaurant_offers).where("restaurant_id = #{restaurant.id} and is_online_order is true").last rescue nil
      #offer = Offer.unscoped.joins(:restaurant_offers).where("restaurant_id = #{restaurant.id} and is_online_order is false").last rescue nil
      offer = restaurant.first_active_offer rescue nil
      rest_offer = RestaurantOffer.where(:offer_id => offer.id, :restaurant_id => restaurant.id).last.id rescue nil
    end
    #rest_offer = RestaurantOffer.where(:offer_id => offer.id).last.id rescue nil


    #if !chain.blank? &&  !restaurant.blank?

    receipt = Receipt.new(
     :chain_id => (chain.id rescue nil),
     :user_id => (user.id rescue nil),
     :status => Receipt::STATUS[:RECEIVED],
     :is_olo => true
     )

    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.status = Receipt::STATUS[:RECEIVED]
    receipt_transaction.subtotal = x.subtotal
    receipt_transaction.tax = x.tax
    receipt_transaction.issue_date = x.issue_date  #x.time_wanted.blank? ? x.time_placed : x.time_wanted
    receipt_transaction.time_stamp = x.issue_time #x.date_pickup.split(' ')[1..2].join(' ') rescue nil
    receipt_transaction.receipt_date = x.time_wanted.blank? ? x.time_placed : x.time_wanted
    receipt_transaction.receipt_time = x.issue_time

    receipt_transaction.restaurant_id = restaurant.id rescue nil
    receipt_transaction.restaurant_offer_id = rest_offer
    receipt_transaction.receipt_number = x.order_id
    receipt.receipt_transactions << receipt_transaction

    receipt.save

    self.update_column(:receipt_id, receipt.id) rescue nil

    # if user.blank?
    #   UserMailer.email_parse_rejected(chain, x.email).deliver!
    #   puts "SENDING EMAILLS"
    #   return true
    # end

    if !offer.blank? && !user.blank?
      today_maxed = false

      if today_maxed == false
        ## check max submit
        receipt.status = Receipt::STATUS[:APPROVED]
        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.status = Receipt::STATUS[:APPROVED]
        receipt_transaction.subtotal = x.subtotal
        receipt_transaction.tax = x.tax
        receipt_transaction.receipt_id = receipt.id
        receipt_transaction.issue_date = x.issue_date #x.time_wanted.blank? ? x.time_placed : x.time_wanted
        receipt_transaction.time_stamp = x.issue_time #x.date_pickup.split(' ')[1..2].join(' ') rescue nil
        receipt_transaction.receipt_date = x.receipt_date #x.time_wanted.blank? ? x.time_placed : x.time_wanted
        receipt_transaction.receipt_time = x.issue_time

        receipt_transaction.restaurant_id = restaurant.id rescue nil
        receipt_transaction.restaurant_offer_id = rest_offer
        receipt_transaction.receipt_number = x.order_id
        receipt_transaction.restaurant_offer_id = rest_offer
      end

      is_date = receipt_transaction.issue_date.blank? ? receipt.created_at : receipt_transaction.issue_date


      if restaurant
        if !offer.blank? && offer.min_subtotal_criteria_for_receipt_approval > 0 && offer.min_subtotal_criteria_for_receipt_approval.to_f > receipt_transaction.subtotal.to_f
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          receipt_transaction.instructions = {:min_subtotal => "MIN SUBTOTAL LIMIT CROSSED"}
        elsif !chain.available_offer_points_earned_per_day?(receipt, receipt_transaction, receipt_transaction.subtotal, receipt_transaction.issue_date, offer)
          receipt_transaction.status = Receipt::STATUS[:REJECTED]
          receipt.status = Receipt::STATUS[:REJECTED]
          receipt_transaction.instructions = {:maximum_offer_points_earned_per_day => "PER DAY OFFER POINTS LIMIT CROSSED"}
        else
          receipt_transaction.status = Receipt::STATUS[:APPROVED]
          receipt.status = Receipt::STATUS[:APPROVED]
        end
      end

      if receipt.chain.today_olo_max_user_limit?(receipt, is_date)
        today_maxed = true
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.status = Receipt::STATUS[:REJECTED]
        receipt_transaction.instructions = {:user_maxed => "user_maxed"}
      end

      if  Receipt.joins(:receipt_transactions).
        where("chain_id = ? and is_olo = ? and receipt_number = '#{x.order_id}' and receipts.status = 3", chain.id, true).
        select('count(distinct(receipts.*))').first.count.to_i == 1
        puts
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.status = Receipt::STATUS[:REJECTED]
        receipt_transaction.instructions = {:receipt => "rejected"}
      end

      # max reached approved receipt for chain


      puts "OLO WEBHOOK receipt transaction #{receipt_transaction.inspect}"

      receipt_transaction.save
      receipt.update_column(:status,receipt_transaction.status)
      utc_offset = x.data_request["storeUtcOffset"].to_i
      p "OLO webhook OFFSET #{utc_offset}"
      tmp_time_zone = ActiveSupport::TimeZone.us_zones.select{|tz| tz.formatted_offset.to_i == utc_offset}.first.tzinfo.current_period.abbreviation.to_s rescue nil
      ReceiptProcess::ReceiptSetIssuedate.set_date(receipt.id,{:issue_date => x.issue_date, :issue_time => x.issue_time, :timezone => tmp_time_zone})

      # self.update_column(:receipt_id, receipt.id) rescue nil
      # if ENV['DEFAULT_HOST_EMAIL'].to_s.include?('trelevant')
      #   receipt.chain.update_dashboard_instant if receipt and receipt.chain
      # end
    end
    #end
  end

  def get_issue_date
    x = OloParser::ParserOlo.new(self.content)
    tmp = x.issue_date.to_s + " " + x.issue_time.strftime("%H:%M") rescue x.issue_date
    return tmp
  end
end
