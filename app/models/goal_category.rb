class GoalCategory < ActiveRecord::Base

  belongs_to :chain
  has_many :goal_category_locales, :foreign_key => "category_id"
  has_many :goals, :foreign_key => "category_id"

  STATUS = {ACTIVE: 1, INACTIVE: 2}
  GOAL_CATEGORY_TEMPLATE = {template3: [3,3,2], template2: [4,4], template1: [8], template0: [0] }

  accepts_nested_attributes_for :goal_category_locales

  validates :description, :chain_id, :presence => true

  def get_locale(locale = 'en')
    selected_locale = Locale.where(:key => locale).first rescue nil
    self.goal_category_locales.where(:locale_id => selected_locale.id).first.text rescue nil
  end
  
end
