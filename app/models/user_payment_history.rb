class UserPaymentHistory < ActiveRecord::Base

  belongs_to :user
  belongs_to :pos_check_upload
  belongs_to :receipt

  has_attached_file :signature,
                    :styles => { :small => Setting.paperclip.styles.small,
                                 :large => Setting.paperclip.styles.large },
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                        :access_key_id => Setting.storage.s3_access_key_id,
                        :secret_access_key => Setting.storage.s3_secret_access_key
                    }

  validates_attachment_content_type :signature, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :on => :create

  PAYMENT_PROCESSOR = {
      "Braintree" => 1,
      "NCR" => 2
  }

end
