class Region < ActiveRecord::Base
  has_many :cities#, :counter_cache => :cities_count
  has_many :restaurants
  belongs_to :country

  validates :abbreviation, :length => {:maximum => 4}
  validates :name,:abbreviation, :presence => true
  validates_uniqueness_of :name

#  default_scope where("deleted_at" => nil).order("name ASC")
  scope :active_regions, -> { where(:active => true) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
  before_create :upcase_words
  before_validation :upcase_words

  def label
    "%s, %s" % [self.abbreviation, self.country.label] rescue ""
  end

  def upcase_words
    self.name = self.name.titleize
    self.abbreviation = abbreviation.upcase rescue "N/A"
  end
end
