class GcmUnsentHistory < ActiveRecord::Base

  def remove_reg_id_from_user
    users = User.where(:device_token => self.reg_id)
    users.each do |user|
      RemovedRegid.create(:user => user, :reg_id => user.device_token)
      user.update_attributes(:device_token => nil, :sign_in_device_type => nil, :device_id => nil)
    end
    self.destroy
  end

  def self.clear_unsent_histories
    GcmUnsentHistory.all.each do |unsent|
      users = User.where(:device_token => unsent.reg_id)
      users.each do |user|
        user.update_attributes(:device_token => nil)
      end
      unsent.destroy
    end
  end
end
