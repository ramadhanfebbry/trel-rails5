class PushRewardJob < Struct.new(:reward, :user_ids)
  
  def perform
    puts "perform"
    puts "send email reward to #{user_ids}"
    user_tmp_ids = User.where("id in (?)", user_ids).map(&:id)
    RewardMailer.reward_detail(reward, user_tmp_ids).deliver
    puts "send email"
  end

  def error(job, exception)
    reward = job.delayable_type.classify.constantize.find(job.delayable_id)
    error_text = job.last_error
    RewardMailer.reward_error(reward, error_text).deliver
  end
  
end
