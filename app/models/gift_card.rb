class GiftCard

  def self.get_credit_card_number_and_detail(apikey, user)
    begin
      giftcode_root_url = ENV["GIFTCODE_ROOT_URL"] || "http://fundy.relevantmobile.com:8090"
      uri = URI.parse(giftcode_root_url + "/giftcard-rest/api/v1/card?apikey=#{apikey}&userid=#{user.try(:id)}")
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      request["Content-Type"] = "application/json; charset=utf-8"
      response = http.request(request)
      if response.code == "200" || response.code == "201"
        p "response nya ---------------"
        json_response = JSON.parse(response.body)
        p json_response
        if json_response["code"] == 0
          return [true, json_response["code"], "success", json_response["cardNumber"], json_response["balance"].to_f]
        else
          return [false, json_response["code"], json_response["message"], nil, 0]
        end
      else
        p "REQUEST FAILED --------------"
        json_response = JSON.parse(response.body) rescue nil
        p json_response
        return [false, (json_response["code"] rescue nil), (json_response["message"] rescue nil), nil, 0]
      end
    rescue => e
      return [false, 1000, e.message, nil, 0]
    end
  end

  def self.redemption(apikey, card_number, amount)
    begin
      p "AMOUNT NYA -----#{amount}------"
      giftcode_root_url = ENV["GIFTCODE_ROOT_URL"] || "http://fundy.relevantmobile.com:8090"
      uri = URI.parse(giftcode_root_url + "/giftcard-rest/api/v1/card/redemption?cardNumber=#{card_number}&apikey=#{apikey}&amount=#{amount}")
      p uri.to_s
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Put.new(uri.request_uri)
      request["Content-Type"] = "application/json; charset=utf-8"
      response = http.request(request)
      if response.code == "200" || response.code == "201"
        p "response nya ---------------"
        json_response = JSON.parse(response.body)
        p json_response
        if json_response["code"] == 0
          return [true, json_response["code"], "success", json_response["transactionId"]]
        else
          return [false, json_response["code"], json_response["message"], nil]
        end
      else
        p "REQUEST FAILED --------------"
        json_response = JSON.parse(response.body) rescue nil
        p json_response
        return [false, (json_response["code"] rescue nil), (json_response["message"] rescue nil), nil]
      end
    rescue => e
      return [false, 1000, e.message, nil]
    end
  end

  def self.void_redeem(apikey, user, card_number, transaction_id)
    begin
      p "TRANSACTION ID NYA -----#{transaction_id}------"
      giftcode_root_url = ENV["GIFTCODE_ROOT_URL"] || "http://fundy.relevantmobile.com:8090"
      uri = URI.parse(giftcode_root_url + "/giftcard-rest/api/v1/card/void/redemption?cardNumber=#{card_number}&transactionId=#{transaction_id}")
      p uri.to_s
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Put.new(uri.request_uri)
      request["Content-Type"] = "application/json; charset=utf-8"
      request["apikey"] = apikey
      request["user_id"] = user.id
      response = http.request(request)
      if response.code == "200" || response.code == "201"
        p "response nya ---------------"
        json_response = JSON.parse(response.body)
        p json_response
        if json_response["code"] == 0
          return [true, json_response["code"], "success"]
        else
          return [false, json_response["code"], json_response["message"]]
        end
      else
        p "REQUEST FAILED --------------"
        json_response = JSON.parse(response.body) rescue nil
        p json_response
        return [false, (json_response["code"] rescue nil), (json_response["message"] rescue nil)]
      end
    rescue => e
      return [false, 1000, e.message]
    end
  end

end