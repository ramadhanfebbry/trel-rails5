class ApplicationKeySweeper < ActionController::Caching::Sweeper

  observe ApplicationKey

  def after_save(record)
    expire_application_cache(record)
  end

  def after_destroy(record)
    expire_application_cache(record)
  end

  private

  def expire_application_cache(record)
    Rails.cache.delete("obj_appkey_#{record.appkey}")
    Rails.cache.delete("appkey_#{record.appkey}")
  end

end
