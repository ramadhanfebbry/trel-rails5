class SubmitReceipt < ActiveRecord::Base
  has_one :deal, :as => :activity

  accepts_nested_attributes_for :deal

  validates :minimum_subtotal, :numericality => {:greater_than_or_equal => 0}, :format => {:with => /^\d+\.*\d{0,2}$/, :message => 'Decimal number upto max two decimal digits.'}, :presence => true


  default_scope { where("submit_receipts.deleted_at" => nil) }
  def self.deleted
    self.unscoped.where('submit_receipts.deleted_at IS NOT NULL')
  end
end
