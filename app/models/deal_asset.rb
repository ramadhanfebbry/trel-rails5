class DealAsset < ActiveRecord::Base

  belongs_to :activity, :polymorphic => true

  validates_attachment_presence :image
end
