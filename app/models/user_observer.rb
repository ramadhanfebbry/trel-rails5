class UserObserver < ActiveRecord::Observer
  observe User

  def after_create(record)
    UserTag.create(:user_id => record.id, :tag_id => (Tag.find_by_name("Medium User").id rescue 0))
  end

  def after_save(record)
    new_record = record.id_changed?
    unless new_record
      p "---user fishbowl callback after save----"
      #Delayed::Job.enqueue(FishbowlDirectSyncJob.new(record))
      record.chain.add_to_fishbowl_user_without_direct_sync(record) rescue nil
    end
  end

  def after_update(record)
    p "checking after update user"
    Delayed::Job.enqueue(SendEmailForgotPasswordJob.new(record, record.password)) if record.forgot_password
  end
  
end
