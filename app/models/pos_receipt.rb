class PosReceipt < ActiveRecord::Base

  serialize :menu_items

  STATUS = {
      :RECEIVED => 1,
      :CLAIMED => 2
  }

  def status_name
    PosReceipt::STATUS.invert[self.status]
  end

end