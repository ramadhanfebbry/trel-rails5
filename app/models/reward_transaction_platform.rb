class RewardTransactionPlatform < ActiveRecord::Base
  # attr_accessible :title, :body
  belongs_to :reward_transaction

  TYPES = {
      "APP" => 1,
      "OLO" => 2,
      "ONOSYS" => 3,
      "NUORDER" => 4
  }

  ## RewardTransactionPlatform.set_trans_type(rt,trs)
  def self.set_trans_type(reward_transaction,trans_type)
    p "RewardTransactionPlatform -- set_trans_type"
    platform = RewardTransactionPlatform.where(:reward_transaction_id => reward_transaction.id).first

    if platform.blank?
      RewardTransactionPlatform.create({:reward_transaction_id => reward_transaction.id, :platform => select_platform(trans_type)})
    else
      platform.update_attributes({:reward_transaction_id => reward_transaction.id, :platform => select_platform(trans_type)})
    end
    p "RewardTransactionPlatform -- set_trans_type END"
  end

  def self.select_platform(trans_type)
    return RewardTransactionPlatform::TYPES[trans_type] rescue RewardTransactionPlatform::TYPES["APP"]
  end

  def self.show_platform(rt)
     return "NATIVE APP" if RewardTransactionPlatform.where(:reward_transaction_id => rt.id).blank?
     return RewardTransactionPlatform::TYPES.invert[RewardTransactionPlatform.where(:reward_transaction_id => rt.id).first.platform]
  end
end
