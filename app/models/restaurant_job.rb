class RestaurantJob < Struct.new(:restaurant)

  def perform
    chain = restaurant.chain
    if chain
      chain.offers.each do |offer|
        offer.participate([restaurant.id])
      end

      chain.rewards.each do |reward|
        reward.participate([restaurant.id])
      end
    end

    if chain
      #online offer
      ## online order
      offers  = chain.offers.unscoped.where(:chain_id => chain.id , :is_online_order => true)
      unless offers.blank?
        offers.each do |offer|
          offer.participate([restaurant.id])
        end
      end
    end
  end

end