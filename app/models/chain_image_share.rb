class ChainImageShare < ActiveRecord::Base
  belongs_to :chain
  has_many :chain_image_share_locales, :dependent => :destroy

  accepts_nested_attributes_for :chain_image_share_locales

  has_attached_file :image, :styles => { :medium => "400x400>", :thumb => "150x150>" },
    :storage => :s3,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => "chain_image_share/chain/:id/:style/:filename"
  
  validates_attachment_presence :image, :on =>:create
  #validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :on=>:create

  validates :start_date, :expiry_date, :presence => true
  validate :start_date_cannot_be_greater_than_expiry_date, :on => :create
  ## scope
  scope :by_chain_and_locale ,lambda{|ch, lc|
    where("chain_image_shares.chain_id = ? and chain_image_share_locales.locale_id = ?", ch,lc).joins(:chain_image_share_locales)
  }

  scope :active_image, lambda {|dt| where("? >= chain_image_shares.start_date and  ? <= chain_image_shares.expiry_date", dt, dt)}

  ## custom validation
  def start_date_cannot_be_greater_than_expiry_date
      return errors.add(:start_date, "must be greater than right now date") if start_date && start_date < Date.today
      unless start_date.blank? and expiry_date.blank?
        begin
          if start_date > expiry_date
            errors.add(:start_date, "must be less that expiry date")
            errors.add(:expiry_date, "must be greater than start date")
          end
        rescue
        end      
    end
  end

  def image_url
    self.image.url
  end
end
