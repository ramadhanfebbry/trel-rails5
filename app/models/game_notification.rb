class GameNotification < ActiveRecord::Base

  belongs_to :chain
  belongs_to :locale

  def self.get_message(user)
    notif = GameNotification.where(:chain_id => user.chain_id, :locale_id => user.locale_id).first
    notif.notification rescue ""
  end

end