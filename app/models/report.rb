class Report < ActiveRecord::Base
  belongs_to :chain
  belongs_to :report_subscription

  scope :today, -> { where("date(created_at) = ?", Date.today) }


  def get_report_url
    if Rails.env != "production"
      secret_key = Setting.storage.s3_access_key_id
      access_key = Setting.storage.s3_secret_access_key
      bucket = Setting.storage.s3_bucket
    else
      secret_key = ENV["S3_KEY"]
      access_key = ENV["S3_SECRET"]
      bucket = ENV["S3_BUCKET"]
    end
    aw = AWS::S3.new(
      :access_key_id     =>secret_key ,
      :secret_access_key =>access_key
    )

    url = aw.buckets[bucket].objects[self.s3_path].url_for(:read).to_s
    return url
  end
end