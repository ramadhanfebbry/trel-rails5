class RestaurantHour < ActiveRecord::Base

  WEEKDAY_NAMES = %w<Sunday Monday Tuesday Wednesday Thursday Friday Saturday>
  WEEKDAY_NAMES_ABBR = %w<Sun Mon Tue Wed Thu Fri Sat>
  
  belongs_to :restaurant

end
