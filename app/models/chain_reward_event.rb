class ChainRewardEvent < ActiveRecord::Base
  belongs_to :reward
  belongs_to :chain

  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end


  EVENTS = {
    "SIGN_UP"=>1,
    "FIRST_RECEIPT"=>2,
    "HOHOHO"=>3,
    "PUNCHH_MIGRATION" => 4
  }
end
