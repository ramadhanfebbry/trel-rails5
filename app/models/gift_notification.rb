class GiftNotification < ActiveRecord::Base

  belongs_to :chain
  belongs_to :locale

  def self.get_email_template(user)
    gift_notif = GiftNotification.where(:chain_id => user.chain_id, :locale_id => user.locale_id).first
    {:warn_message => (gift_notif.warning_message rescue ""), :subject => (gift_notif.email_subject rescue ""), :content => (gift_notif.email_content rescue "")}
  end

end