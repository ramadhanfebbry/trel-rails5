class RemovePoint < ActiveRecord::Base
  has_many :user_remove_points
  has_many :delayed_jobs, :as => :delayable, :dependent => :destroy
  belongs_to :chain
  belongs_to :admin, :foreign_key => "pusher_id"

  serialize :user_ids
  serialize :error_logs
  serialize :success_logs

  attr_accessor :user_temp_ids

  validates :points, :numericality => { :greater_than => 0 }
  validates :chain_id, :presence => true
  validates :user_temp_ids, :presence => true
  
  def send_remove_point_job
    dl = Delayed::Job.enqueue(RemovePointJob.new(self))
    dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id, :chain_id => self.chain_id)
  end

end
