require 'mail'
class SurveyResponseAlertJob < Struct.new(:chain, :survey_user, :answer_submitted_at)

  def perform
    # chain = Chain.find 14
    # survey_user = SurveysUser.where(:survey_id => 15).last
    # answer_submitted_at = Time.now

    if survey_response_alert = chain.survey_response_alert
      if survey_response_alert.send_email_alert  and !chain.survey_email.blank?
        contain_negative_review = false
        contain_comment = false
        answer_lists = Answer.where(:surveys_user_id => survey_user.id)
        answer_lists.each do |answer|
          value = answer.value rescue ""
          if answer.answer_type.eql?(Answer::TYPES['Comments']) && !value.blank?
            question = answer.question
            if question && question.is_considered_for_negative_response
              contain_comment = true
            end
          else
            choice = QuestionChoice.find(answer.value_id) rescue nil
            if choice && choice.is_considered_for_negative_response
              contain_negative_review = true
            end
          end
        end
        if contain_comment || contain_negative_review
          selected_survey_user = survey_user
          answered_at = answer_submitted_at
          selected_chain = chain
          subject_email = survey_response_alert.get_email_subject_by_locale(selected_survey_user.user.locale_id)
          content_email = survey_response_alert.get_email_content_by_locale(selected_survey_user.user.locale_id)
          content_email.gsub!(/\r\n?/, "<br />")
          content_email.gsub!(/\n/, "<br />")

          receipt_type =  selected_survey_user.receipt.receipt_type_name rescue nil
          receipt_id_tmp = selected_survey_user.receipt.id rescue nil
          # receipt_id_tmp = 3822568
          link = Base64.strict_encode64(receipt_id_tmp.to_s)
          address = "<a href='dashboard.relevantmobile.com/receipt/#{link.to_s}' target='_blank'>#{receipt_id_tmp}</a>"
          address = "<a href='dashboard.relevantmobile.com/receipt/#{link.to_s}' target='_blank'>#{receipt_id_tmp}</a>" if ENV["DEFAULT_HOST_EMAIL"].to_s.include?('prelevant')
          # receipt_type = "barcode"
          unless receipt_type.blank?
            if receipt_type.include?('image')
              receipt_url = ("<a href='#{selected_survey_user.receipt.image.url}' target='_blank'>#{selected_survey_user.receipt.id}</a>" rescue "")
            elsif receipt_type.include?('barcode') || receipt_type.include?('ncr')
              receipt_url = address
            end
          end

          puts "receipt_type ======== #{receipt_type}"


          Mail.defaults do
            delivery_method :smtp, {
                :address => Setting.smtp.host,
                :port => Setting.smtp.port,
                :user_name => Setting.smtp.username,
                :password => Setting.smtp.password,
                :authentication => 'plain',
                :enable_starttls_auto => true
            }
          end
          timezone ||= Setting.default_time_zone["zone"] || Time.zone.now.zone

          mail = Mail.new do
            from Setting.email.default_from
            to selected_chain.survey_email
            subject (subject_email % {:survey_id => selected_survey_user.survey_id })
            html_part do
              content_type 'text/html; charset=UTF-8'
              body (content_email % {
                  :user_email => selected_survey_user.user.email,
                  :survey_location => (selected_survey_user.receipt.receipt_transactions.first.restaurant.dashboard_display_text rescue ""),
                  :survey_date_time => "#{answered_at.in_time_zone(timezone).strftime("%m/%d/%Y %H:%M")} #{timezone}",
                  :survey_id => selected_survey_user.survey_id,
                  :receipt_id_link => receipt_url,
                  :survey_response => selected_survey_user.survey_detail_in_table_html_view
              }).html_safe
            end
          end
          mail.deliver!
        end

      end
    end
    puts "Completed survey response alert job"
  end
end