class RestaurantOffer < ActiveRecord::Base
  belongs_to :restaurant
  belongs_to :offer
  has_many :receipt_transactions

  scope :by_restaurant_ids , -> (x) { where("restaurant_id in (?)",x) if x.present? }
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

end
