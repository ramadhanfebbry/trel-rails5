class GeneralMenuItem < ActiveRecord::Base

  belongs_to :chain
  has_many :reward_menu_items
  has_many :rewards, :through => :reward_menu_items
  has_many :general_pos_menu_items
  has_many :pos_menu_items, :through => :general_pos_menu_items

  has_one :general_menu_item_range_value

  validates :item_name, :description, :item_keywords, :chain_id, :presence => true

  attr_accessor :required

  accepts_nested_attributes_for :general_menu_item_range_value


  def self.add_olo_menu_items(general_menu_item_id, menu_item_ids)

  	menu_item_ids.each do |menu_item_id|
	    general_menu_item = GeneralMenuItem.find(general_menu_item_id)
	    chain = general_menu_item.chain
	    menu_item = PosMenuItem.find menu_item_id
	    chain.pos_menu_items.where(:item_number_olo => menu_item.item_number_olo, :level_size_id => menu_item.level_size_id).each do |pos_menu_item|
	      GeneralPosMenuItem.create(:general_menu_item_id => general_menu_item.id, :pos_menu_item_id => pos_menu_item.id) if GeneralPosMenuItem.where(:general_menu_item_id => general_menu_item.id, :pos_menu_item_id => pos_menu_item.id).blank?
	    end
	    menu_item_ids = general_menu_item.pos_menu_items.map(&:id)
	  end
  end

end