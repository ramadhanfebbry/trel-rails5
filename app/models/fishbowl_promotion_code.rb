class FishbowlPromotionCode < ActiveRecord::Base

  belongs_to :chain
  belongs_to :reward
  belongs_to :user
  belongs_to :restaurant
end
