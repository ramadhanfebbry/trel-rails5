class RewardExpNotification < ActiveRecord::Base

  belongs_to :locale
  belongs_to :chain

  validates :chain_id, :locale_id, :email_subject, :email_body_html, :email_body_text, :notification, :presence => true

  def send_to_user(reward_wallet, user)
    if self.chain.ren_email_notification && user.push_to_email?
      user.send_reward_expired_notification_email(reward_wallet, self)
    end

    if self.chain.ren_device_notification && user.push_to_device?
      user.send_reward_expired_notification_device(reward_wallet, self)
    end

  end
end