class SendEmailForgotPasswordJob < Struct.new(:user, :password)

  def perform
    p "-----SendEmailForgotPasswordJob---"
    p "password nya #{password}"
    chain = user.chain
    notif_setting = chain.notification_settings.where(email_template: "forgot_password").first rescue nil
    unless notif_setting.blank?
      if notif_setting.send_type.eql?(1)
        if Setting.email.turned_on
          if(Setting.email.delayed)
            user.send_forgot_password_email(password, true)
          else
            user.send_forgot_password_email(password, false)
          end
        end
      elsif notif_setting.send_type.eql?(2)
        user.password = password
        user.send_forgot_password_email_using_mandrill(user, notif_setting)
      elsif notif_setting.send_type.eql?(3)
        #for fishbowl
      end
    else
      if Setting.email.turned_on
        if(Setting.email.delayed)
          user.send_forgot_password_email(password, true)
        else
          user.send_forgot_password_email(password, false)
        end
      end
    end
  end

end