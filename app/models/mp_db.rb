class MpDb < ActiveRecord::Base
  MPDB_CONFIG = {
      :adapter  => "postgresql",
      :host     => "fundy.relevantmobile.com",
      :username => "jenkinsuser",
      :password => "jenkinsuserPWD",
      :database => "mobilepay"
  }
  self.abstract_class = true
  establish_connection MPDB_CONFIG
end