class UserDevice < ActiveRecord::Base

	def self.set_keychain_for_user(keychain, user)
		user_device = UserDevice.where(:keychain_value => keychain).first
		unless user_device.blank?
			user_device.update_column(:user_id, user.id)
		end	
	end
end
