class ReceiptApprovedProcess < Struct.new(:receipt_transaction)

  def perform
    record = receipt_transaction
    puts "--- 1. start milestone receipt process"
    receipt = record.receipt
    chain = receipt.chain

    receipt.push_referral_code_reward if receipt.is_online_order == false and receipt.is_olo == false

    #move to dj
    ## insert to restaurant user if user has submit one receipt to restaurant"
    #puts "*************************************** Restaurant User process ************************"
    #RestaurantUser.update_member(record)
    #puts "*************************************** END Restaurant User process ************************"

    if record.status.eql?(Receipt::STATUS[:APPROVED])
      puts "---------------------- start observer receipt transaction ---------------------"

      ## move to dj
      if receipt.is_receipt_image?
        receipt_xml_data = Receipt.get_associated_xml_receipt_data(receipt)
        if receipt_xml_data
          ocr_xml = OcrXml.new(receipt_xml_data)
          record.line_items = ocr_xml.line_items
        end
      end
      record.save_line_items

      puts "--- start stepping stone receipt process"
      if chain.activate_milestone_receipt
        chain.steppingstone_process(record, "receipt_approved")
      else
        chain.bucket_process(record, receipt.user)
      end

      puts "--- end stepping stone receipt process"
      puts "*****************************************"

      puts "--- 2. start milestone point reward"
      record.update_user_points
      puts "-- end milestone point reward"
      puts "*****************************************"

      puts "--- 3. start referral reward program"
      receipt.push_referral_code_reward
      puts "--- end referral reward program"
      puts "*****************************************"

      puts "--- 4. start push reward first receipt"
      receipt_type = receipt.is_receipt_barcode ? "barcode" : "submission"
      Reward.push_reward(ChainRewardEvent::EVENTS["FIRST_RECEIPT"], chain, receipt.user, receipt_type)
      puts "-- end push reward first receipt"
      puts "*****************************************"

      ActivitySummary.update_count_by_transaction(record) rescue nil

    end

    puts "------------ 5 -------- Delayed Job"
    # Delayed::Job.enqueue(
    a = ReceiptProcess::ReceiptTransactionCallback.new(record)
    a.perform
    #     :queue => "receipt_updated_job",
    #     :priority => nil,
    #     :run_at => Time.now + 5.minutes
    # )



  end

end