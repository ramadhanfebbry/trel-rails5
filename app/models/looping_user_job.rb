class LoopingUserJob < Struct.new(:chain, :log, :pages, :query, :synch_job_type)

  def perform
    p "perform----------"
    log.update_attributes(:status => "processing", :initiate_at => Time.now)
    p "LoopingUserJob: #{chain.try(:name)}"

    loop_page = 0
    pages.to_a.in_groups_of(50, false) do |page|
     p "marketing process job page: #{loop_page = loop_page + 1} ======"

      if synch_job_type == "EmmaMarketing"
        ems = EmmaMarketing.includes(:user).where(query).order("user_id DESC").paginate(:page => loop_page.to_i, :per_page => 50)
        dl = Delayed::Job.enqueue(EmmaJobProcess.new(chain, log, ems), :queue => 'synch_marketing_job')
      end
    end
  end

  def error(job, exception)
    p "---------email marketing erroor handling-------"
    struct = YAML::load(job.handler)
    chain = struct.chain
    log = struct.log
    log.update_attributes(:status =>  "failed", :completed_at => Time.now, :error_report => exception.to_s)  unless log.blank?
    p log
    ChainMailer.emma_sync_log_error_report(chain, exception.to_s).deliver
    p "-----------------finishhhhh------"
  end


end