class UserActivityNotificationField < ActiveRecord::Base

  belongs_to :user_activity_notification

  validates :name, :description, :identifier, :presence => true


end
