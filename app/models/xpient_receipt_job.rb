class XpientReceiptJob < Struct.new(:pos_check_upload, :user, :restaurant_id, :offer_id, :receipt_id)

  def perform
    xpient_parser = XpientCheckParser.new(pos_check_upload, user, restaurant_id, offer_id, receipt_id)
    xpient_parser.do_parse
  end

end