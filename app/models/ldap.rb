require 'digest'
class Ldap
  attr_accessor :connection, :host, :port, :username, :password
  def initialize(host = nil, port= nil, username = nil, password = nil)
    @port = port || 10389
    @host = host || "kraken.relevantmobile.com"
    @username = username || "uid=relevadm,ou=system"
    @password = password || "R459f0T"

    @connection = Net::LDAP.new :host => @host,
                                :port => @port,
                                :auth => {
                                    :method => :simple,
                                    :username => @username,
                                    :password => @password
                                }
  end

  def create_new_restaurant(chain, restaurant)
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)
    if @connection  && @connection.bind
      dn = "cn=#{restaurant.ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      unique_members = []
      chain.owners.where("owners.is_active = ? and owners.role_id != 3",true).each do |owner|
        unique_members << "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      end if chain.owners.present? && false

      restaurant.owners.where("owners.is_active = ? and owners.role_id = 2",true).each do |owner|
        unique_members << "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      end if restaurant.owners.present? && false

      #add one uniq member for requirement one member
      unique_members << "test uniq #{restaurant}"

      attr = {
          :cn => restaurant.ldap_identity,
          :objectclass => ["top", "groupOfUniqueNames"],
          :description => restaurant.address,
          :uniqueMember => unique_members.uniq
      }
      @connection.add(:dn => dn, :attributes => attr) unless unique_members.blank?

      #create chain owner ..
      chain.owners.where("owners.is_active = ? and owners.role_id != 3",true).each do |owner|
        filter = Net::LDAP::Filter.eq( "uid", "#{owner.email}")
        treebase = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        search = @connection.search(:base => treebase, :filter => filter)

        if search.blank?
          digest = Net::LDAP::Password.generate(:md5, "rel12345")
          dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          attr = {
              :cn => owner.first_name,
              :sn => owner.first_name,
              :objectclass => ["top", "inetOrgPerson","person","organizationalPerson"],
              :description => "Chain Owner",
              :givenName => owner.first_name,
              :uid => owner.email,
              :mail => owner.email,
              :userPassword => digest
          }
          attr[:title] = owner.title unless owner.title.blank?
          attr[:postalAddress] = owner.address unless owner.address.blank?
          attr[:postalCode] = owner.zipcode unless owner.zipcode.blank?
          attr[:st] = owner.city.try(:name) unless owner.city.try(:name).blank?
          attr[:telephoneNumber] = owner.work_contact_number.to_i.to_s unless owner.work_contact_number.blank?
          attr[:mobile] = owner.cell_contact_number.to_i.to_s unless owner.cell_contact_number.blank?

          @connection.add(:dn => dn, :attributes => attr)
        end
      end if chain.owners.present?

      #create restaurant owner ..
      restaurant.owners.where("owners.is_active = ? and owners.role_id = 2",true).each do |owner|
        filter = Net::LDAP::Filter.eq( "uid", "#{owner.email}")
        treebase = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        search = @connection.search(:base => treebase, :filter => filter)

        if search.blank?
          digest = Net::LDAP::Password.generate(:md5, "rel12345")
          dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          attr = {
              :cn => owner.first_name,
              :sn => owner.first_name,
              :objectclass => ["top", "inetOrgPerson","person","organizationalPerson"],
              :description => "Restaurant Owner",
              :givenName => owner.first_name,
              :uid => owner.email,
              :mail => owner.email,
              :userPassword => digest
          }
          attr[:title] = owner.title unless owner.title.blank?
          attr[:postalAddress] = owner.address unless owner.address.blank?
          attr[:postalCode] = owner.zipcode unless owner.zipcode.blank?
          attr[:st] = owner.city.try(:name) unless owner.city.try(:name).blank?
          attr[:telephoneNumber] = owner.work_contact_number.to_i.to_s unless owner.work_contact_number.blank?
          attr[:mobile] = owner.cell_contact_number.to_i.to_s unless owner.cell_contact_number.blank?

          @connection.add(:dn => dn, :attributes => attr)
        end
      end if restaurant.owners.present?

    end
  end

  def create_or_update_restaurant(restaurant)
    chain = restaurant.chain
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)
    if @connection  && @connection.bind
      filter = Net::LDAP::Filter.eq( "cn", "#{restaurant.ldap_identity}")
      treebase = "ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      search = @connection.search(:base => treebase, :filter => filter)
      if search.blank? && restaurant.status
        create_new_restaurant(chain, restaurant)
      else
        if restaurant.status
          new_ldap_identity = restaurant.try(:name).try(:parameterize, "_").try(:classify)
          old_ldap_identity = restaurant.ldap_identity
          dn = "cn=#{old_ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          @connection.rename(:olddn =>dn , :newrdn => "cn=#{new_ldap_identity}")
          restaurant.update_column(:ldap_identity, new_ldap_identity)
        else
          dn = "cn=#{restaurant.ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          @connection.delete(:dn => dn)

          restaurant.owners.where("owners.is_active = ? and owners.role_id = 2",true).each do |owner|
            if owner.restaurants.count == 1
              dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
              @connection.delete(:dn => dn)
            end
          end if restaurant.owners.present?
        end
      end
    end
  end

  def remove_chain(chain)
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)
    if @connection  && @connection.bind
      #search existing chain
      filter = Net::LDAP::Filter.eq( "ou", "#{chain.ldap_identity}")
      treebase = "ou=Chains,o=Relevant"
      search = @connection.search(:base => treebase, :filter => filter)
      unless search.blank?
        chain.owners.each do |owner|
          dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          @connection.delete(:dn => dn)
        end

        chain.restaurants.each do |restaurant|
          dn = "cn=#{restaurant.ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          @connection.delete(:dn => dn)
        end

        dn="ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        @connection.delete(:dn => dn)

        dn="ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        @connection.delete(:dn => dn)

        dn="ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        @connection.delete(:dn => dn)
      end
    end
  end

  def add_chain(chain)
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)
    if @connection  && @connection.bind
      #search existing chain
      filter = Net::LDAP::Filter.eq( "ou", "#{chain.ldap_identity}")
      treebase = "ou=Chains,o=Relevant"
      search = @connection.search(:base => treebase, :filter => filter)
      if search.blank?
        #create ou chain name
        dn = "ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        attr = {
            :ou => "#{chain.ldap_identity}",
            :objectclass => ["top", "organizationalUnit"],
        }
        @connection.add(:dn => dn, :attributes => attr)

        #create ou restaurants for that chain
        dn = "ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        attr = {
            :ou => "Restaurants",
            :objectclass => ["top", "organizationalUnit"],
        }
        #@connection.add(:dn => dn, :attributes => attr)

        #create ou users for that chain
        dn = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        attr = {
            :ou => "Users",
            :objectclass => ["top", "organizationalUnit"],
        }
        @connection.add(:dn => dn, :attributes => attr)

        chain.restaurants.each do |restaurant|
          #create_or_update_restaurant(restaurant)
        end

        chain.owners.each do |owner|
          create_or_update_owner(owner, "rel12345")
        end

      end
    end
  end

  def create_or_update_owner(owner, password)
    chain = owner.chains.first
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)
    return if chain.blank?
    unless password.blank?
      digest = Net::LDAP::Password.generate(:md5, password)
    end
    #search user first
    filter = Net::LDAP::Filter.eq( "uid", "#{owner.email}")
    treebase = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
    search = @connection.search(:base => treebase, :filter => filter)
    if search.blank?
      #add users here..
      dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      attr = {
          :cn => owner.first_name,
          :sn => owner.first_name,
          :objectclass => ["top", "inetOrgPerson","person","organizationalPerson"],
          :description => "#{owner.chain_owner? ? "Chain" : "Restaurant"} Owner",
          :givenName => owner.first_name,
          :uid => owner.email,
          :mail => owner.email
      }
      if false
        attr[:title] = owner.title unless owner.title.blank?
        attr[:postalAddress] = owner.address unless owner.address.blank?
        attr[:postalCode] = owner.zipcode unless owner.zipcode.blank?
        attr[:st] = owner.city.try(:name) unless owner.city.try(:name).blank?
        attr[:telephoneNumber] = owner.work_contact_number.to_i.to_s unless owner.work_contact_number.blank?
        attr[:mobile] = owner.cell_contact_number.to_i.to_s unless owner.cell_contact_number.blank?
      end

      attr[:userPassword] = digest unless password.blank?
      @connection.add(:dn => dn, :attributes => attr)
    else
      result = search.first
      dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      ops = [
          [:delete, :givenName, nil],
          [:delete, :sn, nil],
          [:delete, :cn, nil],
          [:add, :givenName, owner.first_name],
          [:add, :sn, owner.last_name],
          [:add, :cn, owner.first_name]
      ]
      @connection.modify :dn => dn, :operations => ops

      unless password.blank?
        ops = [[:replace, :userPassword, digest]]
        @connection.modify :dn => dn, :operations => ops
      end

    end
  end


  def create_new_owner(chain, owner, password)
    p "LDAP ----------------------------------create_new_owner"
    p "password:  ------------- #{password} ---------------------"
    p "email:  ------------- #{owner.email} ---------------------"
    digest = Net::LDAP::Password.generate(:md5, password)
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)

    if @connection  && @connection.bind
      #search existing chain
      filter = Net::LDAP::Filter.eq( "ou", "#{chain.ldap_identity}")
      treebase = "ou=Chains,o=Relevant"
      search = @connection.search(:base => treebase, :filter => filter)
      if search.blank?
        #create ou chain name
        dn = "ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        attr = {
            :ou => "#{chain.ldap_identity}",
            :objectclass => ["top", "organizationalUnit"],
        }
        @connection.add(:dn => dn, :attributes => attr)

        #create ou restaurants for that chain
        dn = "ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        attr = {
            :ou => "Restaurants",
            :objectclass => ["top", "organizationalUnit"],
        }
        #@connection.add(:dn => dn, :attributes => attr)

        #create ou users for that chain
        dn = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        attr = {
            :ou => "Users",
            :objectclass => ["top", "organizationalUnit"],
        }
        @connection.add(:dn => dn, :attributes => attr)
      end if false

      #search user first
      filter = Net::LDAP::Filter.eq( "uid", "#{owner.email}")
      treebase = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      search = @connection.search(:base => treebase, :filter => filter)

      if search.blank?
        #add users here..
        dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        attr = {
            :cn => owner.first_name,
            :sn => owner.first_name,
            :objectclass => ["top", "inetOrgPerson","person","organizationalPerson"],
            :description => "#{owner.chain_owner? ? "Chain" : "Restaurant"} Owner",
            :givenName => owner.first_name,
            :uid => owner.email,
            :mail => owner.email,
            :userPassword => digest
        }
        if false
          attr[:title] = owner.title unless owner.title.blank?
          attr[:postalAddress] = owner.address unless owner.address.blank?
          attr[:postalCode] = owner.zipcode unless owner.zipcode.blank?
          attr[:st] = owner.city.try(:name) unless owner.city.try(:name).blank?
          attr[:telephoneNumber] = owner.work_contact_number.to_i.to_s unless owner.work_contact_number.blank?
          attr[:mobile] = owner.cell_contact_number.to_i.to_s unless owner.cell_contact_number.blank?
        end

        @connection.add(:dn => dn, :attributes => attr)

      else
        dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        ops = [
                [:replace, :givenName, owner.first_name],
                [:replace, :sn, owner.last_name],
                [:replace, :cn, owner.first_name],
                [:replace, :uid, owner.email],
                [:replace, :mail, owner.email],
        ]
        if false
          ops.push([:replace, :title, owner.title]) unless owner.title.blank?
          ops.push([:replace, :postalAddress, owner.address]) unless owner.address.blank?
          ops.push([:replace, :postalCode, owner.zipcode]) unless owner.zipcode.blank?
          ops.push([:replace, :st, owner.city.try(:name)]) unless owner.city.try(:name).blank?
          ops.push([:replace, :telephoneNumber, owner.work_contact_number.to_i.to_s]) unless owner.work_contact_number.blank?
          ops.push([:replace, :mobile, owner.cell_contact_number.to_i.to_s]) unless owner.cell_contact_number.blank?
        end

        @connection.modify :dn => dn, :operations => ops

        unless password.blank?
          digest = Net::LDAP::Password.generate(:md5, password)
          ops = [[:replace, :userPassword, digest]]
          dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          @connection.modify :dn => dn, :operations => ops
        end
      end

      #search group first
      owner.restaurants.where("chain_id = #{chain.id}").each do |restaurant|
        #search existing restaurant
        filter = Net::LDAP::Filter.eq( "cn", "#{restaurant.ldap_identity}")
        treebase = "ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        search = @connection.search(:base => treebase, :filter => filter)
        if search.blank?
          unique_members = []
          unique_members << "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"

          restaurant.owners.where("owners.is_active = ? and owners.role_id = 2",true).each do |owner|
            unique_members << "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          end if restaurant.owners.present?
          dn = "cn=#{restaurant.ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          attr = {
              :cn => restaurant.ldap_identity,
              :objectclass => ["top", "groupOfUniqueNames"],
              :description => restaurant.address,
              :uniqueMember => unique_members.uniq
          }
          @connection.add(:dn => dn, :attributes => attr) unless unique_members.blank?
        else
          uniq_member = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          unless search[0]["uniquemember"].include?(uniq_member)
            dn = "cn=#{restaurant.ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
            @connection.add_attribute dn, :uniqueMember, uniq_member
          end
        end
      end if false #disable feature add owner to group member

    end
  end

  def create_or_update_chain(chain)
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)
    filter = Net::LDAP::Filter.eq( "ou", "#{chain.ldap_identity}")
    treebase = "ou=Chains,o=Relevant"
    search = @connection.search(:base => treebase, :filter => filter)
    unless search.blank?
      new_ldap_identity = chain.try(:name).try(:parameterize, "_").try(:classify)
      old_ldap_identity = chain.ldap_identity
      result = search.first
      dn = "ou=#{old_ldap_identity},ou=Chains,o=Relevant"
      @connection.rename(:olddn =>dn , :newrdn => "ou=#{new_ldap_identity}")
      chain.update_column(:ldap_identity, new_ldap_identity)
    else
      #create ou chain name
      dn = "ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      attr = {
          :ou => "#{chain.ldap_identity}",
          :objectclass => ["top", "organizationalUnit"],
      }
      @connection.add(:dn => dn, :attributes => attr)

      #create ou restaurants for that chain
      dn = "ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      attr = {
          :ou => "Restaurants",
          :objectclass => ["top", "organizationalUnit"],
      }
      #@connection.add(:dn => dn, :attributes => attr)

      #create ou users for that chain
      dn = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      attr = {
          :ou => "Users",
          :objectclass => ["top", "organizationalUnit"],
      }
      @connection.add(:dn => dn, :attributes => attr)
    end

  end


  def import_from_relevant
    Chain.all.each do |chain|
      chain.owners.where("owners.is_active = ? and role_id != 3",true).each do |owner|
        owner.generate_password
        create_new_owner(chain, owner, owner.password_generated)
      end if chain.owners.present?
    end
  end

  def add_or_remove_chain_owner(owner)
    p "LDAP ---------------------- add_or_remove_chain_owner"
    chain = owner.chains.first
    return if chain.blank?
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)

    #search user first
    filter = Net::LDAP::Filter.eq( "uid", "#{owner.email}")
    treebase = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
    search = @connection.search(:base => treebase, :filter => filter)

    if owner.is_active == false #search.present?
      dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      @connection.delete(:dn => dn)

      restaurants = owner.restaurants
      restaurants.each do |restaurant|
        #dn = "cn=#{restaurant.ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        #unique_member = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        #ops = [[:delete, :uniqueMember, unique_member]]
        #@connection.modify :dn => dn, :operations => ops

        if restaurant.owners.where("owners.is_active = ? and owners.role_id = 2",true).blank?
          dn = "cn=#{restaurant.ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
          @connection.delete(:dn => dn)
        end if restaurant.owners.present?
      end if false
    elsif owner.is_active #search.blank?
      #add users here..
      dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      digest = Net::LDAP::Password.generate(:md5, "rel12345")
      attr = {
          :cn => owner.first_name,
          :sn => owner.first_name,
          :objectclass => ["top", "inetOrgPerson","person","organizationalPerson"],
          :description => "#{owner.chain_owner? ? "Chain" : "Restaurant"} Owner",
          :givenName => owner.first_name,
          :uid => owner.email,
          :mail => owner.email,
          :userPassword => digest
      }

      if false
        attr[:title] = owner.title unless owner.title.blank?
        attr[:postalAddress] = owner.address unless owner.address.blank?
        attr[:postalCode] = owner.zipcode unless owner.zipcode.blank?
        attr[:st] = owner.city.try(:name) unless owner.city.try(:name).blank?
        attr[:telephoneNumber] = owner.work_contact_number.to_i.to_s unless owner.work_contact_number.blank?
        attr[:mobile] = owner.cell_contact_number.to_i.to_s unless owner.cell_contact_number.blank?
      end

      @connection.add(:dn => dn, :attributes => attr)
      p @connection

      restaurants = owner.restaurants
      restaurants.each do |restaurant|
        #dn = "cn=#{restaurant.ldap_identity},ou=Restaurants,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        #unique_member = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
        #ops = [[:add, :uniqueMember, unique_member]]
        #@connection.modify :dn => dn, :operations => ops
      end if false
    elsif search.present? && owner.is_active
      dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      ops = [
          [:replace, :givenName, owner.first_name],
          [:replace, :sn, owner.last_name],
          [:replace, :cn, owner.first_name],
          [:replace, :uid, owner.email],
          [:replace, :mail, owner.email]
      ]
      if false
        ops.push([:replace, :title, owner.title]) unless owner.title.blank?
        ops.push([:replace, :postalAddress, owner.address]) unless owner.address.blank?
        ops.push([:replace, :postalCode, owner.zipcode]) unless owner.zipcode.blank?
        ops.push([:replace, :st, owner.city.try(:name)]) unless owner.city.try(:name).blank?
        ops.push([:replace, :telephoneNumber, owner.work_contact_number.to_i.to_s]) unless owner.work_contact_number.blank?
        ops.push([:replace, :mobile, owner.cell_contact_number.to_i.to_s]) unless owner.cell_contact_number.blank?
      end

      @connection.modify :dn => dn, :operations => ops
    end
  end

  def update_owner_password(owner, password)
    digest = Net::LDAP::Password.generate(:md5, password)
    chain = owner.chains.first
    return if chain.blank?
    chain.ldap_identity = chain.try(:name).try(:parameterize, separator: "_").try(:classify)

    #search user first
    filter = Net::LDAP::Filter.eq( "uid", "#{owner.email}")
    treebase = "ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
    search = @connection.search(:base => treebase, :filter => filter)

    if search.present?
      ops = [[:replace, :userPassword, digest]]
      dn = "uid=#{owner.email},ou=Users,ou=#{chain.ldap_identity},ou=Chains,o=Relevant"
      @connection.modify :dn => dn, :operations => ops
    end
  end

end