class Goal < ActiveRecord::Base

  belongs_to :chain
  belongs_to :category, :class_name => "GoalCategory", :foreign_key => "category_id"
  belongs_to :incentive, :polymorphic => true

  STATUS = {ACTIVE: 1, INACTIVE: 2}

  has_many :goal_locales
  has_many :message_incentive_locales

  accepts_nested_attributes_for :goal_locales, :message_incentive_locales

  validates :description, :chain_id, :category_id, :presence => true

  def get_text_by_locale(locale = 'en')
    selected_locale = Locale.where(:key => locale).first rescue nil
    self.goal_locales.where(:locale_id => selected_locale.id).first.text rescue nil
  end

  def get_message_incentive(locale= 'en')
    selected_locale = Locale.where(:key => locale).first rescue nil
    self.message_incentive_locales.where(:locale_id => selected_locale.id).first.message rescue nil
  end
end
