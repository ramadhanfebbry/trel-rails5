class UserGoalCategory < ActiveRecord::Base

  belongs_to :user
  belongs_to :category, :class_name => "GoalCategory", :foreign_key => "category_id"
  belongs_to :chain

  attr_accessor :category_ids
  
  validates :user_id, :category_id, :presence => true

  validates_uniqueness_of :category_id, :scope => :user_id
  
end
