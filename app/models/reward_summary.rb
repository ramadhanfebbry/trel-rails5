class RewardSummary < ActiveRecord::Base
  #RewardSummary.execute_first
  def self.execute_first
    self.generate_first_data
  end

  #RewardSummary.generate_first_data
  def self.generate_first_data
    RewardSummary.delete_all
    Chain.select('id,created_at').all.each do |ch|
      first_chain_created = ch.created_at
      diff = ((Time.zone.now - first_chain_created).to_i / 1.days) + 1

      ch.restaurants.each do |rs|
        ## loop each restaurants
        0.upto(diff).each do |day|
          cnt = RewardTransaction.by_restaurant_and_date(rs.id, ((first_chain_created + day.to_i.days))).count

          self.create(:transaction_date => (first_chain_created + day.to_i.days).to_date,
                      :total => cnt.to_i, :restaurant_id => rs.id, :chain_id => ch.id
          ) if cnt.to_i > 0
        end
      end
    end
  end

  def self.update_data(restaurant_id, date_input)
    begin
      cnt = RewardTransaction.by_restaurant_and_date(restaurant_id, date_input).count
      data = self.where("restaurant_id = ? and transaction_date = date(?)", restaurant_id,
                        date_input).first
      chain = Restaurant.find(restaurant_id).chain
      if data.blank?
        self.create(:transaction_date => date_input,
                    :total => 1, :restaurant_id => restaurant_id, :chain_id => chain.id)
      else
        data.update_attributes(:total => cnt)
      end
    rescue => e
      puts "REWARD_SUMMARY::UPDATE_DATA #{e.inspect}"
    end
  end

  ## RewardSummary.schedule_daily
  ## for prepopulate the reward activity per restaurant
  def self.scheduler_daily
    Chain.select('id,created_at').all.each do |ch|
      diff = 2 ## 3 days before

      ch.restaurants.each do |rs|
        ## loop each restaurants
        0.upto(diff).each do |day|
          #cnt = RewardTransaction.by_restaurant_and_date(rs.id, ((Time.zone.now - day.days))).count
          date_zone = "date(TIMEZONE('UTC', reward_transactions.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
          #cnt = RewardTransaction.where("restaurant_id = ? and #{date_zone} = ?",rs.id,
          #                              (Time.zone.now - day.days).to_date).count
          cnt = RewardTransaction.select("
          distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
reward_transactions.restaurant_id,
 reward_transactions.user_id)  reward_transactions.*").where("
 date(timezone('EST',
timezone('UTC', reward_transactions.created_at))) = ?
AND  redeeming is FALSE AND restaurant_id = ? " ,
               (Time.zone.now.in_time_zone('EST') - day.days).to_date,rs.id).length

          data = RewardSummary.where(:transaction_date => (Time.zone.now.in_time_zone('EST') - day.days).to_date,
                            :restaurant_id => rs.id, :chain_id => ch.id
          )
          if data.size > 1
            # delete duplicate row
            data.delete_all
            data = nil
          else
            data = data.first
          end



          if data.blank?
            RewardSummary.create(:transaction_date => ((Time.zone.now - day.days)),
                        :total => cnt.to_i, :restaurant_id => rs.id, :chain_id => ch.id
            ) if cnt.to_i > 0
          else
            data.update_attributes(:total => cnt.to_i)
          end
        end
      end
    end
  end
end
