class ChainLocale < ActiveRecord::Base
  #after_create :save_email_template_to_redis, :save_warning_claim_template_to_redis
  after_create :save_email_template_to_redis, :save_warning_claim_template_to_redis, :save_email_template_to_db

  before_destroy :remove_email_template_from_redis, :remove_warning_claim_template_from_redis

  attr_accessor :shouldnt_email_template_to_redis
  belongs_to :chain
  belongs_to :locale

  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  # EMAILS=['welcome', 'receipt_reject', 'forgot_password', 'welcome_chain_owner', 'welcome_rest_owner']
  EMAILS=['receipt_reject', 'forgot_password', 'welcome_chain_owner', 'welcome_rest_owner', 'signup_activation_email']
  NCR_EMAIL= ['device_signup_welcome', 'browser_signup_welcome']

  def save_email_template_to_redis
    if self.shouldnt_email_template_to_redis.blank?
      chain_id = self.chain.id
      chain_name = self.chain.name
      locale_key = self.locale.key

      template_module = case locale_key
                          when 'en'
                            ChainEmailTemplates::En
                          when 'es'
                            ChainEmailTemplates::Es
                          else
                            ChainEmailTemplates::En
                        end

      (EMAILS + NCR_EMAIL).each do |e|
        unless REDIS.hget "chain_#{chain_id}", "#{locale_key}_#{e}_email_subject"
          REDIS.hset "chain_#{chain_id}", "#{locale_key}_#{e}_email_subject", template_module.send("#{e}_email_subject", chain_name)
        end

        unless REDIS.hget "chain_#{chain_id}", "#{locale_key}_#{e}_email_body"
          REDIS.hset "chain_#{chain_id}", "#{locale_key}_#{e}_email_body", template_module.send("#{e}_email_body", chain_name)
        end
      end

    end
  end

  def save_email_template_to_db
    chain_id = self.chain.id
    chain_name = self.chain.name
    locale_key = self.locale.key

    template_module = case locale_key
                        when 'en'
                          ChainEmailTemplates::En
                        when 'es'
                          ChainEmailTemplates::Es
                        else
                          ChainEmailTemplates::En
                      end

    (EMAILS + NCR_EMAIL).each do |e|
      email_template = EmailTemplate.new
      email_template.template_name = e
      email_template.chain_id = chain_id
      email_template.locale_id = self.locale.id
      email_template.mail_subject = template_module.send("#{e}_email_subject", chain_name)
      email_template.mail_content_text = template_module.send("#{e}_email_body", chain_name)
      html_body = template_module.send("#{e}_email_body", chain_name)
      html_body.gsub!(/\r\n?/, "<br />")
      html_body.gsub!(/\n/, "<br />")
      email_template.mail_content_html = html_body
      email_template.send_as = "multipart/alternative"
      email_template.save
    end
  end

  def save_warning_claim_template_to_redis
    if self.shouldnt_email_template_to_redis.blank?
      begin
        chain_id = self.chain.id
        chain_name = self.chain.name
        locale_key = self.locale.key

        template_module = case locale_key
                            when 'en'
                              WarningClaimTemplates::En
                            when 'es'
                              WarningClaimTemplates::Es
                            else
                              WarningClaimTemplates::En
                          end

        question = template_module.title_warning(chain_name)

        #def self.body_warning(chain_name, confirm_text = 'confirm', validity_period = '2')
        content = template_module.body_warning(chain_name,'confirm', '2')
        stored = {:question => question , :body => content}
        REDIS.hdel "chain_#{chain_id}_warning_claim", locale_key+"_warning_claim"
        REDIS.hset "chain_#{chain_id}_warning_claim", locale_key+"_warning_claim", stored.to_json
      rescue
        false
      end
    end
  end

  def show_warning_template
    locale_key = self.locale.key
    REDIS.hget "chain_#{self.chain.id}_warning_claim", locale_key+"_warning_claim"
  end

  def remove_email_template_from_redis
    chain_id = self.chain.id
    locale_key = self.locale.key

    EMAILS.each do |e|
      REDIS.hdel "chain_#{chain_id}", "#{locale_key}_#{e}_email_subject"
      REDIS.hdel "chain_#{chain_id}", "#{locale_key}_#{e}_email_body"
    end
  end

  def remove_warning_claim_template_from_redis
    chain_id = self.chain.id
    locale_key = self.locale.key
    REDIS.hdel "chain_#{chain_id}_warning_claim", locale_key+"_warning_claim"
  end

  def self.add_device_browser_welcome_email_template
    Chain.all.each do |chain|
      chain.chain_locales.each do |chain_locale|
        chain_id = chain_locale.chain.id
        chain_name = chain_locale.chain.name
        locale_key = chain_locale.locale.key
        welcome_email = EmailTemplate.where(:chain_id => chain.id, :locale_id => chain_locale.locale_id, :template_name => "welcome").first

        unless welcome_email.blank?
          mail_subject = welcome_email.mail_subject
          mail_body = welcome_email.mail_content_text
          html_body = welcome_email.mail_content_html
        else
          template_module = case locale_key
                              when 'en'
                                ChainEmailTemplates::En
                              when 'es'
                                ChainEmailTemplates::Es
                              else
                                ChainEmailTemplates::En
                            end
          mail_subject = template_module.send("welcome_email_subject", chain_name)
          mail_body = template_module.send("welcome_email_body", chain_name)
          html_body = template_module.send("welcome_email_body", chain_name)
          html_body.gsub!(/\r\n?/, "<br />")
          html_body.gsub!(/\n/, "<br />")
        end

        ['device_signup_welcome', 'browser_signup_welcome'].each do |e|
          email_template = EmailTemplate.new
          email_template.template_name = e
          email_template.chain_id = chain.id
          email_template.locale_id = chain_locale.locale_id
          email_template.mail_subject = mail_subject
          email_template.mail_content_text = mail_body
          email_template.mail_content_html = html_body
          email_template.send_as = "multipart/alternative"
          email_template.save
        end
      end
    end

  end
end
