class Promotion < ActiveRecord::Base
  belongs_to :promotable, :polymorphic => true
  belongs_to :chain

  has_many :promo_codes, :dependent => :destroy
  has_many :delayed_jobs, :as => :delayable, :dependent => :destroy
  has_one :promotion_notification, :dependent => :destroy

  validate :must_be_presence
  validate :expiration_date_cannot_be_greater_than_effective_date
  validates :name , :presence => true
  validates :chain_id, :presence => {:message => "---- You Must Selected the Reward / Perk / Points "}

  attr_accessor :shared_value, :gift_promoteable

  before_save :set_limit
 # after_save :set_promo_code

  default_scope { where("deleted_at" => nil) }

  scope :active_chain, -> { includes(:chain).where("chains.status = 'active'") }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def set_limit
    unless self.quantity.blank? ## if its a shared type promocode
      self.limit = 0
    end
  end

  def must_be_presence
    return if self.gift_promoteable.eql?("1")
    if quantity.blank? and shared_value.blank?
      errors.add(:quantity, "must input the shared code / quantity")
    else
      unless shared_value.blank?
        if shared_value.match(/^\w*$/).blank?
          errors.add(:shared_value, "Must not have Special Character on it")
        end
      end
    end
  end

  def apply_for(user)
    case self.promotable_type
    when "Perk"
      #PerkWallet.find_or_create_by_user_id_and_perk_id(user_id: user.id, perk_id: self.promotable_id, status: PerkWallet::STATUS[:ACTIVE],
      #  expiry_date: self.expiry_date)
      pr = PerkWallet.where(:user_id => user.id, :perk_id => self.promotable_id).first
      if pr.blank?
        PerkWallet.create(user_id: user.id, perk_id: self.promotable_id, status: PerkWallet::STATUS[:ACTIVE],
           expiry_date: self.expiry_date)
      end
    when "Reward"
      RewardWallet.create_with_delay(user_id: user.id, reward_id: self.promotable_id, status: RewardWallet::STATUS[:ACTIVE],
        expiry_date: self.expiry_date)
      when "PromotionOffer"
        point = self.promotable.points
        user.earn(point)
        PointHistory.add_to_history(user.id, "Promotion Incentive", point)

    end
  end
  
  def expiration_date_cannot_be_greater_than_effective_date    
    unless effective_date.blank? and expiry_date.blank?
      begin
        if effective_date > expiry_date
          errors.add(:effective_date, "must be less that expired date")
          errors.add(:expiry_date, "must be greater than effective date")
        end
      rescue
      end
    end
  end

  def send_promo_job(current_user)
    if self.delayed_jobs.count > 0 ## if there are still jobs in queue
      self.delayed_jobs.delete_all          
    end
    #    self.delay.generate_promotion_codes ## send the new / update jobs
    Delayed::Job.enqueue(PromoCodeJob.new(self.quantity, self.chain_id, self.id,current_user), chain_id: self.chain_id, delayable_type: self.class.to_s,	delayable_id: self.id)
  end

  def generate_promotion_codes
    PromoCode.generate_codes(self.quantity, self.chain_id, self.id)
  end

  def set_promo_code(current_user)
    p "*set promocode"*30
    return if self.promotable.class.eql?(Reward) && self.promotable.giftable?
    p "* promocode send "
    unless quantity.blank?
      #using delayed jobs
      self.send_promo_job(current_user)
    else
      # if updating, replace the old promo code value
      codes = self.promo_codes
      unless codes.blank?
        codes.each do |promocode|
          promocode.update_attributes(:code => shared_value)
          ## update the chain id
          promocode.update_column(:chain_id, self.chain_id)
        end
      else
        promocode = PromoCode.new(:code => shared_value, :shared => true, :promotion_id => self.id, :chain_id => self.chain_id)
        promocode.save
      end

      self.send_promo_job(current_user)
    end
  end

  def set_chain_id
    self.promotable_type.constantize.find(self.promotable_id).chain_id rescue nil
  end

  def gift_promotion?
    self.promotable_type.eql?("Reward") && self.promotable.giftable?
  end

  def set_notification
    self.promotable_type.constantize.find(self.promotable_id).chain_id rescue nil
  end


  ## searching
  scope :chains, -> (value) {  where("chain_id" => value) if (!value.blank?) }
  scope :date_from, -> (value) {  where("created_at >= '#{value}'") if (!value.blank?) }

  scope :date_to, -> (value) {  where("created_at <= '#{value}'") if (!value.blank?) }

  scope :by_promo, -> (value) {  where("promotions.promotable_type = ?", value) if (!value.blank?) }

  scope :by_name, -> (value) { where("promotions.name LIKE '%#{value}%'") if (!value.blank?) }
  scope :search, -> (params) { 
    chains(params[:chains]).date_from(params[:from]).date_to(params[:to]).by_name(params[:q]).by_promo(params[:promo_type])
  }
end
