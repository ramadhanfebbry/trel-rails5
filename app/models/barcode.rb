class Barcode < ActiveRecord::Base

  validates :code, :presence => true, :uniqueness => true
  belongs_to :barcode_batch

  def self.generate(batch, chain)
    receipt_code = REDIS.get "receiptcode_chain_#{chain.id}"
    receipt_code_setting = JSON.parse(receipt_code) rescue nil
    receipt_code_setting ||= AppcodeSetting::DEFAULT_SETTING_RECEIPTCODE
    if receipt_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      barcode =  generate_numeric_code(receipt_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        existing = Barcode.where(:code => barcode, :chain_id => chain.id).first
        unless existing
          bc = Barcode.new(:code => barcode, :barcode_batch_id => batch.id, :chain_id => chain.id)
          save =  bc.save rescue false
          barcode =  generate_numeric_code(receipt_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_numeric_code(receipt_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return receipt_code_setting["prefix"].to_s + barcode + receipt_code_setting["postfix"].to_s
    elsif receipt_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      barcode = generate_alphanumeric_code(receipt_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        existing = Barcode.where(:code => barcode, :chain_id => chain.id).first
        unless existing
          bc = Barcode.new(:code => barcode, :barcode_batch_id => batch.id, :chain_id => chain.id)
          save =  bc.save rescue false
          barcode = generate_alphanumeric_code(receipt_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode = generate_alphanumeric_code(receipt_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return receipt_code_setting["prefix"].to_s + barcode + receipt_code_setting["postfix"].to_s
    end
  end

  def self.generate_user_code(user,chain, application_id = nil, lat = nil, long = nil)
    user_code_setting = chain.user_code_setting
    user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
    user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
    if user_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"])
      return user_code_setting["prefix"].to_s + user.id.to_s.add_zero_padding(user_code_setting["zero_padding"].to_i) + user_code_setting["postfix"].to_s
    end
    if user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      barcode =  generate_numeric_code(user_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where(:code => barcode, :chain_id => chain.id).count
        if code_count == 0
          expired_at = user_code_setting["timer"].to_i == 0 ? nil : Time.current + user_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :user_id => user.id,:application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["USERCODE"],
              :expired_at => expired_at
          )

          save =  bc.save rescue false
          barcode =  generate_numeric_code(user_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_numeric_code(user_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return user_code_setting["prefix"].to_s + barcode.add_zero_padding(user_code_setting["zero_padding"].to_i) + user_code_setting["postfix"].to_s
    elsif user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      barcode = generate_alphanumeric_code(user_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where(:code => barcode, :chain_id => chain.id).count
        if code_count == 0
          expired_at = user_code_setting["timer"].to_i == 0 ? nil : Time.current + user_code_setting["timer"].to_i.seconds
          bc = UserSession.new(:user_id => user.id,:application_id => application_id,
                               :code => barcode, :chain_id => chain.id,
                               :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["USERCODE"],
                               :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode = generate_alphanumeric_code(user_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode = generate_alphanumeric_code(user_code_setting["pos_digit_barcode"].to_i)if save == false
        end
      end
      return user_code_setting["prefix"].to_s + barcode.add_zero_padding(user_code_setting["zero_padding"].to_i) + user_code_setting["postfix"].to_s
    end
  end

  #this is hotfix for CORNER BACERY FIRST TO AVOID ALL CHAIN BREAK, WE WILL REMOVE THIS AND MAKE ALL SAME IN THE FUTURE
  def self.generate_user_code_v2(user,chain, user_code_setting, application_id = nil, lat = nil, long = nil)
    if user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      max_code_possibilites = Barcode.get_max_codes_possibilites(user_code_setting["pos_digit_barcode"].to_i)
      presentase_counter = (((1/AppcodeSetting::CODE_TYPES.values.size.to_f)*100)/100)*0.25
      max_code_possibilites_limit = (max_code_possibilites*presentase_counter).to_i
      max_code_possibilites_limit = max_code_possibilites if max_code_possibilites_limit == 0
      max_code_possibilites_limit = 50000 if max_code_possibilites_limit > 50000
      numeric_code_count = chain.user_code_count
      if numeric_code_count >= max_code_possibilites_limit
        random_sequence = rand(max_code_possibilites_limit)
        regex = '^\d{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
        barcode = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["USERCODE"]} AND code_sequence = #{random_sequence}").first
        if barcode
          barcode = barcode.try(:code)
          return [barcode.dup, user_code_setting["prefix"].to_s + barcode.add_zero_padding(user_code_setting["zero_padding"].to_i) + user_code_setting["postfix"].to_s]
        end
      end
      barcode =  Barcode.generate_numeric_code(user_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.select("code").where("code = ? AND chain_id = ?", barcode, chain.id).count
        if code_count == 0
          expired_at = user_code_setting["timer"].to_i == 0 ? Time.current + 30.minutes : Time.current + user_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["USERCODE"],
              :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode =  generate_numeric_code(user_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_numeric_code(user_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
    elsif user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      max_code_possibilites = Barcode.get_max_codes_possibilites(user_code_setting["pos_digit_barcode"].to_i, "alphanumeric")
      presentase_counter = (((1/AppcodeSetting::CODE_TYPES.values.size.to_f)*100)/100)*0.0001
      max_code_possibilites_limit = (max_code_possibilites*presentase_counter).to_i
      max_code_possibilites_limit = max_code_possibilites if max_code_possibilites_limit == 0
      max_code_possibilites_limit = 50000 if max_code_possibilites_limit > 50000

      alphanumeric_code_count =  chain.user_code_count
      if alphanumeric_code_count >= max_code_possibilites_limit
        random_sequence = rand(max_code_possibilites_limit)
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
        barcode = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["USERCODE"]} AND code_sequence = #{random_sequence}").first
        if barcode
          barcode = barcode.try(:code)
          return [barcode.dup, user_code_setting["prefix"].to_s + barcode.add_zero_padding(user_code_setting["zero_padding"].to_i) + user_code_setting["postfix"].to_s]
        end
      end

      barcode = Barcode.generate_alphanumeric_code(user_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.select("code").where("code = ? AND chain_id = ?", barcode, chain.id).count
        if code_count == 0
          expired_at = user_code_setting["timer"].to_i == 0 ? Time.current + 30.minutes : Time.current + user_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["USERCODE"],
              :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode =  generate_alphanumeric_code(user_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_alphanumeric_code(user_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
    end
    return [barcode.dup, user_code_setting["prefix"].to_s + barcode.add_zero_padding(user_code_setting["zero_padding"].to_i) + user_code_setting["postfix"].to_s]
  end

  def self.generate_pay_code_for_btree(user,chain, application_id = nil, lat = nil, long = nil)
    pay_code = REDIS.get "paycode_chain_#{chain.id}"
    pay_code_setting = JSON.parse(pay_code) rescue nil
    pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
    if pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      barcode =  generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where(:code => barcode, :chain_id => chain.id).count
        if code_count == 0
          expired_at = pay_code_setting["timer"].to_i == 0 ? nil : Time.current + pay_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :user_id => user.id,:application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["PAYCODE"],
              :expired_at => expired_at,
              :active => true
          )
          save =  bc.save rescue false
          barcode = generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return barcode
    elsif pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      barcode = generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where(:code => barcode, :chain_id => chain.id).count
        if code_count == 0
          expired_at = pay_code_setting["timer"].to_i == 0 ? nil : Time.current + pay_code_setting["timer"].to_i.seconds
          bc = UserSession.new(:user_id => user.id,:application_id => application_id,
                               :code => barcode, :chain_id => chain.id,
                               :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["PAYCODE"],
                               :expired_at => expired_at,
                               :active => true
          )
          save =  bc.save rescue false
          barcode = generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode = generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return barcode
    end
  end

  def self.generate_pay_code(user,chain, application_id = nil, lat = nil, long = nil)
    pay_code = REDIS.get "paycode_chain_#{chain.id}"
    pay_code_setting = JSON.parse(pay_code) rescue nil
    pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
    if pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      barcode =  generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where(:code => barcode, :chain_id => chain.id).count
        if code_count == 0
          expired_at = pay_code_setting["timer"].to_i == 0 ? nil : Time.current + pay_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :user_id => user.id,:application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["PAYCODE"],
              :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode = generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return pay_code_setting["prefix"].to_s + barcode.add_zero_padding(pay_code_setting["zero_padding"].to_i) + pay_code_setting["postfix"].to_s
    elsif pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      barcode = generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where(:code => barcode, :chain_id => chain.id).count
        if code_count == 0
          expired_at = pay_code_setting["timer"].to_i == 0 ? nil : Time.current + pay_code_setting["timer"].to_i.seconds
          bc = UserSession.new(:user_id => user.id,:application_id => application_id,
                               :code => barcode, :chain_id => chain.id,
                               :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["PAYCODE"],
                               :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode = generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode = generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return pay_code_setting["prefix"].to_s + barcode.add_zero_padding(pay_code_setting["zero_padding"].to_i) + pay_code_setting["postfix"].to_s
    end
  end

  def self.generate_pay_code_v2(user,chain, pay_code_setting, application_id = nil, lat = nil, long = nil)
    if pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      max_code_possibilites = Barcode.get_max_codes_possibilites(pay_code_setting["pos_digit_barcode"].to_i)
      presentase_counter = (((1/AppcodeSetting::CODE_TYPES.values.size.to_f)*100)/100)*0.25
      max_code_possibilites_limit = (max_code_possibilites*presentase_counter).to_i
      max_code_possibilites_limit = max_code_possibilites if max_code_possibilites_limit == 0
      max_code_possibilites_limit = 50000 if max_code_possibilites_limit > 50000
      numeric_code_count = chain.pay_code_count
      if numeric_code_count >= max_code_possibilites_limit
        random_sequence = rand(max_code_possibilites_limit)
        regex = '^\d{' + pay_code_setting["pos_digit_barcode"].to_s + '}$'
        barcode = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND active IS FALSE AND code_type = #{AppcodeSetting::CODE_TYPES["PAYCODE"]} AND code_sequence = #{random_sequence}").first
        if barcode
          barcode = barcode.try(:code)
          return [barcode.dup, pay_code_setting["prefix"].to_s + barcode.add_zero_padding(pay_code_setting["zero_padding"].to_i) + pay_code_setting["postfix"].to_s]
        end
      end
      barcode =  Barcode.generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.select("code").where("code = ? AND chain_id = ?", barcode, chain.id).count
        if code_count == 0
          expired_at = pay_code_setting["timer"].to_i == 0 ? Time.current + 30.minutes : Time.current + pay_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["PAYCODE"],
              :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode =  generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_numeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
    elsif pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      max_code_possibilites = Barcode.get_max_codes_possibilites(pay_code_setting["pos_digit_barcode"].to_i, "alphanumeric")
      presentase_counter = (((1/AppcodeSetting::CODE_TYPES.values.size.to_f)*100)/100)*0.0001
      max_code_possibilites_limit = (max_code_possibilites*presentase_counter).to_i
      max_code_possibilites_limit = max_code_possibilites if max_code_possibilites_limit == 0
      max_code_possibilites_limit = 50000 if max_code_possibilites_limit > 50000

      alphanumeric_code_count =  chain.pay_code_count
      if alphanumeric_code_count >= max_code_possibilites_limit
        random_sequence = rand(max_code_possibilites_limit)
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + pay_code_setting["pos_digit_barcode"].to_s + '}$'
        barcode = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["PAYCODE"]} AND code_sequence = #{random_sequence}").first
        if barcode
          barcode = barcode.try(:code)
          return [barcode.dup, pay_code_setting["prefix"].to_s + barcode.add_zero_padding(pay_code_setting["zero_padding"].to_i) + pay_code_setting["postfix"].to_s]
        end
      end

      barcode = Barcode.generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.select("code").where("code = ? AND chain_id = ?", barcode, chain.id).count
        if code_count == 0
          expired_at = pay_code_setting["timer"].to_i == 0 ? nil : Time.current + pay_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["PAYCODE"],
              :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode =  generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_alphanumeric_code(pay_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
    end
    return [barcode.dup, pay_code_setting["prefix"].to_s + barcode.add_zero_padding(pay_code_setting["zero_padding"].to_i) + pay_code_setting["postfix"].to_s]
  end

  def self.generate_gift_code(user,chain, application_id = nil, lat = nil, long = nil)
    gift_code = REDIS.get "giftcode_chain_#{chain.id}"
    gift_code_setting = JSON.parse(gift_code) rescue nil
    gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
    if gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      barcode =  generate_numeric_code(gift_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where(:code => barcode, :chain_id => chain.id).count
        if code_count == 0
          expired_at = gift_code_setting["timer"].to_i == 0 ? nil : Time.current + gift_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :user_id => user.id,:application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["GIFTCODE"],
              :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode = generate_numeric_code(gift_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_numeric_code(gift_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return gift_code_setting["prefix"].to_s + barcode.add_zero_padding(gift_code_setting["zero_padding"].to_i) + gift_code_setting["postfix"].to_s
    elsif gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      barcode = generate_alphanumeric_code(gift_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where(:code => barcode, :chain_id => chain.id).count
        if code_count == 0
          expired_at = gift_code_setting["timer"].to_i == 0 ? nil : Time.current + gift_code_setting["timer"].to_i.seconds
          bc = UserSession.new(:user_id => user.id,:application_id => application_id,
                               :code => barcode, :chain_id => chain.id,
                               :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["GIFTCODE"],
                               :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode = generate_alphanumeric_code(gift_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode = generate_alphanumeric_code(gift_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
      return gift_code_setting["prefix"].to_s + barcode.add_zero_padding(gift_code_setting["zero_padding"].to_i) + gift_code_setting["postfix"].to_s
    end
  end

  def self.generate_gift_code_v2(user,chain, application_id = nil, lat = nil, long = nil)
    gift_code = chain.gift_code_setting
    gift_code_setting = JSON.parse(gift_code.to_json) rescue nil
    gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
    if gift_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"])
      return [user.id, gift_code_setting["prefix"].to_s + user.id.to_s.add_zero_padding(gift_code_setting["zero_padding"].to_i) + gift_code_setting["postfix"].to_s]
    end
    if gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      max_code_possibilites = Barcode.get_max_codes_possibilites(gift_code_setting["pos_digit_barcode"].to_i)
      presentase_counter = (((1/AppcodeSetting::CODE_TYPES.values.size.to_f)*100)/100)*0.25
      max_code_possibilites_limit = (max_code_possibilites*presentase_counter).to_i
      max_code_possibilites_limit = max_code_possibilites if max_code_possibilites_limit == 0
      max_code_possibilites_limit = 50000 if max_code_possibilites_limit > 50000
      numeric_code_count = chain.gift_code_count
      if numeric_code_count >= max_code_possibilites_limit
        random_sequence = rand(max_code_possibilites_limit)
        regex = '^\d{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        barcode = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["GIFTCODE"]} AND code_sequence = #{random_sequence}").first
        if barcode
          barcode = barcode.try(:code)
          return [barcode.dup, gift_code_setting["prefix"].to_s + barcode.add_zero_padding(gift_code_setting["zero_padding"].to_i) + gift_code_setting["postfix"].to_s]
        end
      end
      barcode =  generate_numeric_code(gift_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where("code = ? AND chain_id = ?", barcode, chain.id).count
        if code_count == 0
          expired_at = gift_code_setting["timer"].to_i == 0 ? Time.current + 30.minutes : Time.current + gift_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["GIFTCODE"],
              :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode =  generate_numeric_code(gift_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_numeric_code(gift_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
    elsif gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      max_code_possibilites = Barcode.get_max_codes_possibilites(gift_code_setting["pos_digit_barcode"].to_i, "alphanumeric")
      presentase_counter = (((1/AppcodeSetting::CODE_TYPES.values.size.to_f)*100)/100)*0.0001
      max_code_possibilites_limit = (max_code_possibilites*presentase_counter).to_i
      max_code_possibilites_limit = max_code_possibilites if max_code_possibilites_limit == 0
      max_code_possibilites_limit = 50000 if max_code_possibilites_limit > 50000
      alphanumeric_code_count = chain.gift_code_count
      if alphanumeric_code_count >= max_code_possibilites_limit
        random_sequence = rand(max_code_possibilites_limit)
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        barcode = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["GIFTCODE"]} AND code_sequence = #{random_sequence}").first
        if barcode
          barcode = barcode.try(:code)
          return [barcode.dup, gift_code_setting["prefix"].to_s + barcode.add_zero_padding(gift_code_setting["zero_padding"].to_i) + gift_code_setting["postfix"].to_s]
        end
      end

      barcode = Barcode.generate_alphanumeric_code(gift_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where("code = ? AND chain_id = ?", barcode, chain.id).count
        if code_count == 0
          expired_at = gift_code_setting["timer"].to_i == Time.current + 30.minutes ? nil : Time.current + gift_code_setting["timer"].to_i.seconds
          bc = UserSession.new(
              :application_id => application_id,
              :code => barcode, :chain_id => chain.id,
              :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["GIFTCODE"],
              :expired_at => expired_at
          )
          save =  bc.save rescue false
          barcode =  generate_alphanumeric_code(gift_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_alphanumeric_code(gift_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
    end
    return [barcode.dup, gift_code_setting["prefix"].to_s + barcode.add_zero_padding(gift_code_setting["zero_padding"].to_i) + gift_code_setting["postfix"].to_s]
  end

  def self.generate_reward_code(chain, application_id = nil, lat = nil, long = nil)
    reward_code = REDIS.get "rewardcode_chain_#{chain.id}"
    reward_code_setting = JSON.parse(reward_code) rescue nil
    reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
    if reward_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      regex = '^\d{' + reward_code_setting["pos_digit_barcode"].to_s + '}$'
      numeric_code_count = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["REWARDCODE"]}").count
      #to handle more codes in table so let's limit 2000, if some chain already more than 2000, because executed before this code added.
      if numeric_code_count >= 2000
        random_sequence = rand(2000)
        # barcode = UserSession.where("code ~ ? AND chain_id = ? AND code_type = ? AND LENGTH(code) = ?", '^[1-9][0-9]*$', chain.id, AppcodeSetting::CODE_TYPES["REWARDCODE"], reward_code_setting["pos_digit_barcode"].to_i).offset(rand(2000)).limit(100)[rand(100)] rescue nil
        barcode = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["REWARDCODE"]} AND code_sequence = #{random_sequence}").first
        if barcode
          barcode = barcode.try(:code)
          return [barcode, reward_code_setting["zero_padding"].to_i]
        end
      end
      barcode =  generate_numeric_code(reward_code_setting["pos_digit_barcode"].to_i, false)
      save = false
      while save == false
        code_count = UserSession.where("code = ? AND chain_id = ? AND code_type IN(?)", barcode, chain.id, [AppcodeSetting::CODE_TYPES["USERCODE"], AppcodeSetting::CODE_TYPES["PAYCODE"], AppcodeSetting::CODE_TYPES["GIFTCODE"], AppcodeSetting::CODE_TYPES["RECEIPTCODE"]]).count
        if code_count == 0
          reward_code_count = UserSession.where("code = ? AND chain_id = ? AND code_type = ?", barcode, chain.id, AppcodeSetting::CODE_TYPES["REWARDCODE"]).count
          if reward_code_count == 0
            bc = UserSession.new(
                :application_id => application_id,
                :code => barcode, :chain_id => chain.id,
                :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["REWARDCODE"]
            )
            save =  bc.save rescue false
          else
            save = true
          end
          barcode =  generate_numeric_code(reward_code_setting["pos_digit_barcode"].to_i, false) if save == false
        else
          barcode =  generate_numeric_code(reward_code_setting["pos_digit_barcode"].to_i, false) if save == false
        end
      end
    elsif reward_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + reward_code_setting["pos_digit_barcode"].to_s + '}$'
      # alphanumeric_code_count = UserSession.where("code ~ ? AND chain_id = ? AND code_type = ? AND LENGTH(code) = ?", '[a-zA-Z]', chain.id, AppcodeSetting::CODE_TYPES["REWARDCODE"], reward_code_setting["pos_digit_barcode"].to_i).count rescue 0
      alphanumeric_code_count = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["REWARDCODE"]}").count
      #to handle more codes in table so let's limit 2000, if some chain already more than 2000, because executed before this code added.
      if alphanumeric_code_count >= 2000
        random_sequence = rand(2000)
        # barcode = UserSession.where("code ~ ? AND chain_id = ? AND code_type = ? AND LENGTH(code) = ?", '[a-zA-Z]', chain.id, AppcodeSetting::CODE_TYPES["REWARDCODE"], reward_code_setting["pos_digit_barcode"].to_i).offset(rand(2000)).limit(100)[rand(100)]
        barcode = UserSession.find_by_sql("select code from user_sessions where code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = #{AppcodeSetting::CODE_TYPES["REWARDCODE"]} AND code_sequence = #{random_sequence}").first
        if barcode
          barcode = barcode.try(:code)
          return [barcode, reward_code_setting["zero_padding"].to_i, reward_code_setting["prefix"].to_s, reward_code_setting["postfix"].to_s]
        end
      end

      barcode = Barcode.generate_alphanumeric_code(reward_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = UserSession.where("code = ? AND chain_id = ? AND code_type IN(?)", barcode, chain.id, [AppcodeSetting::CODE_TYPES["USERCODE"], AppcodeSetting::CODE_TYPES["PAYCODE"], AppcodeSetting::CODE_TYPES["GIFTCODE"], AppcodeSetting::CODE_TYPES["RECEIPTCODE"]]).count
        if code_count == 0
          reward_code_count = UserSession.where("code = ? AND chain_id = ? AND code_type = ?", barcode, chain.id, AppcodeSetting::CODE_TYPES["REWARDCODE"]).count
          if reward_code_count == 0
            bc = UserSession.new(
                :application_id => application_id,
                :code => barcode, :chain_id => chain.id,
                :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["REWARDCODE"]
            )
            save =  bc.save rescue false
          else
            save = true
          end
          barcode =  generate_alphanumeric_code(reward_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_alphanumeric_code(reward_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
    end
    return [barcode, reward_code_setting["zero_padding"].to_i, reward_code_setting["prefix"].to_s, reward_code_setting["postfix"].to_s]
  end


  def self.generate_numeric_code(length, include_zero = true)
    o = [(0..9)]
    o = [(1..9)] unless include_zero
    o = o.map { |i| i.to_a }.flatten
    (0...length).map { o[rand(o.length)] }.join
  end

  def self.get_max_codes_possibilites(length, type="numeric")
    o = [(0..9)]
    o << ('A'..'Z') if type=="alphanumeric"
    o = o.map { |i| i.to_a }.flatten
    max_codes = 1
    1.upto(length) do |i|
      max_codes = max_codes*o.length
    end
    max_codes
  end


  def self.generate_alphanumeric_code(length, include_down_case_character = false)
    o = [(0..9),('A'..'Z')]
    o << ('a'..'z') if include_down_case_character
    o = o.map { |i| i.to_a }.flatten
    (0...length).map { o[rand(o.length)] }.join
  end

  def self.convert_and_get_code_type(chain, code)
    user_code_setting = chain.user_code_setting
    user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
    user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE

    converted_usercode = AppcodeSetting.converted_usercode_based_on_code_setting(chain, code, user_code_setting)
    converted_paycode = AppcodeSetting.converted_paycode_based_on_code_setting(chain, code)
    converted_giftcode = AppcodeSetting.converted_giftcode_based_on_code_setting(chain, code)
    converted_rewardcode = AppcodeSetting.converted_rewardcode_based_on_code_setting(chain, code)

    user_session = UserSession.where("code = ? AND chain_id = ? AND code_type = ?", converted_usercode, chain.id, AppcodeSetting::CODE_TYPES["USERCODE"]).first
    user_session = UserSession.where("code = ? AND chain_id = ? AND code_type = ?", converted_paycode, chain.id, AppcodeSetting::CODE_TYPES["PAYCODE"]).order("created_at DESC").first if user_session.blank?
    user_session = UserSession.where("code = ? AND chain_id = ? AND code_type = ?", converted_rewardcode, chain.id, AppcodeSetting::CODE_TYPES["REWARDCODE"]).first if user_session.blank?
    user_session = UserSession.where("code = ? AND chain_id = ? AND code_type = ?", converted_giftcode, chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).first if user_session.blank?

    if user_session.blank?
      if user_code_setting["appcode_type"].eql?(AppcodeSetting::TYPES["STATIC"])
        if user_code_setting["user_metadata"].eql?(1) && (user = User.where(:id => converted_usercode, :chain_id => chain.id).first)
          return [converted_usercode, AppcodeSetting::CODE_TYPES["USERCODE"], user, "STATIC"]
        elsif user_code_setting["user_metadata"].eql?(2) && (user = User.where(:phone_number => converted_usercode, :chain_id => chain.id).first)
          return [converted_usercode, AppcodeSetting::CODE_TYPES["USERCODE"], user, "STATIC"]
        else
          return [code, nil, nil, nil]
        end
      else
        return [code, nil, nil, nil]
      end
    else
      user = nil
      appcode_type = nil
      if user_session.code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"])
        user = user_session.user
        appcode_type = "DYNAMIC"
      elsif user_session.code_type.eql?(AppcodeSetting::CODE_TYPES["GIFTCODE"])
        user = user_session.user
      end
      return [user_session.code, user_session.code_type, user, appcode_type, user_session]
    end
  end

  def self.save_btree_customer_id_as_paycode(user, chain, application_id = nil, lat = nil, long = nil)
    pay_code = REDIS.get "paycode_chain_#{chain.id}"
    pay_code_setting = JSON.parse(pay_code) rescue nil
    pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
    expired_at = pay_code_setting["timer"].to_i == 0 ? nil : Time.current + pay_code_setting["timer"].to_i.seconds
    UserSession.update_all("active = false",["user_id = ? AND chain_id = ? AND code_type = ?", user.id, chain.id, AppcodeSetting::CODE_TYPES["PAYCODE"]])
    UserSession.create(
        :user_id => user.id,:application_id => application_id,
        :code => user.braintree_identifier, :chain_id => chain.id,
        :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["PAYCODE"],
        :expired_at => expired_at,
        :active => true
    )
  end

  def self.save_mercury_paycode_as_paycode(user, chain, application_id = nil, lat = nil, long = nil)
    pay_code = REDIS.get "paycode_chain_#{chain.id}"
    pay_code_setting = JSON.parse(pay_code) rescue nil
    pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
    expired_at = pay_code_setting["timer"].to_i == 0 ? nil : Time.current + pay_code_setting["timer"].to_i.seconds
    UserSession.update_all("active = false",["user_id = ? AND chain_id = ? AND code_type = ?", user.id, chain.id, AppcodeSetting::CODE_TYPES["PAYCODE"]])
    UserSession.create(
        :user_id => user.id,:application_id => application_id,
        :code => "MER#{user.id}PAY", :chain_id => chain.id,
        :lat => lat, :long => long, :code_type => AppcodeSetting::CODE_TYPES["PAYCODE"],
        :expired_at => expired_at,
        :active => true
    )
  end

  def self.remove_unused_reward_codes
    Chain.select("id, timer").each do |chain|
      reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
      reward_code_setting = JSON.parse(reward_code_setting) rescue nil
      reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
      expire_secs = reward_code_setting["timer"].to_i
      # expire_secs = 300 if expire_secs == 0
      if expire_secs != 0
        RewardCodeUser.where("chain_id = ? AND created_at < ?", chain.id, Time.current - expire_secs.seconds).destroy_all
      end
    end
  end

  def self.remove_unused_user_codes
    Chain.select("id, timer").each do |chain|
      user_code_setting = chain.user_code_setting
      user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
      user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
      expire_secs = user_code_setting["timer"].to_i
      expire_secs = 300 if expire_secs == 0
      if expire_secs != 0
        UserCodeUser.where("chain_id = ? AND created_at < ?", chain.id, Time.current - expire_secs.seconds).destroy_all
      end
    end
  end

  def self.remove_unused_pay_codes
    Chain.select("id, timer").each do |chain|
      pay_code_setting = chain.pay_code_setting
      pay_code_setting = JSON.parse(pay_code_setting.to_json) rescue nil
      pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
      expire_secs = pay_code_setting["timer"].to_i
      expire_secs = 300 if expire_secs == 0
      if expire_secs != 0
        PayCodeUser.where("chain_id = ? AND created_at < ?", chain.id, Time.current - expire_secs.seconds).destroy_all
      end
    end
  end

  def self.remove_unused_gift_codes
    Chain.select("id, timer").each do |chain|
      gift_code_setting = chain.gift_code_setting
      gift_code_setting = JSON.parse(gift_code_setting.to_json) rescue nil
      gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
      expire_secs = gift_code_setting["timer"].to_i
      expire_secs = 300 if expire_secs == 0
      if expire_secs != 0
        GiftCodeUser.where("chain_id = ? AND created_at < ?", chain.id, Time.current - expire_secs.seconds).destroy_all
      end
    end
  end


  def self.update_btree_user_id_with_paycode(limit = 10)
    failed_users = []
    user_lists = User.where("braintree_user_created IS TRUE AND users.id::text = users.braintree_identifier").limit(limit).each do |user|
      begin
        chain = user.chain
        btree_setting = chain.btree_setting
        btree_setting.set_env_setting
        paycode = Barcode.generate_pay_code_for_btree(user, chain)
        status, message = BtreeSetting.update_braintree_customer_id_with_paycode(user, paycode)
        if status
          user.update_column(:braintree_identifier, paycode)
          mp_user = MpUser.where(:payment_customer_id => user.id.to_s).first
          mp_user.update_column(:payment_customer_id, paycode) if mp_user
          MpPayment.update_all("customer_id = '#{paycode}'",["customer_id = ?", user.id.to_s])
        else
          failed_users << {:user_id => user.id, :message => message}
        end
      rescue => e
        failed_users << {:user_id => user.id, :message => e.message}
      end
    end
    failed_users
  end

  def self.take_available_reward_code_uploaded(reward, restaurant, chain, user)
    if !chain.use_generated_code && reward.reward_pos_codes.count > 0
      #selected = reward.reward_pos_codes.where("user_id IS NULL").order("RANDOM()").first rescue nil
      selected = reward.reward_pos_codes.where("user_id IS NULL").limit(100).sample(1).first rescue nil
      unless selected.blank?
        selected.update_attributes(:user_id => user.id, :used_at => Time.current, :restaurant_id => (restaurant.id rescue nil))
        selected.code
      else
        "NOCODE"
      end
    elsif !chain.use_generated_code && reward.reward_pos_codes.count == 0
      "NOCODE"
    end
  end

  def self.generate_partner_reward_code(chain, reward_id, user_id, champain_id, number_of_valid_days)
    reward_code_setting = PartnerCodeSetting.where("chain_id = #{chain.id}").first rescue nil
    reward_code_setting = JSON.parse(reward_code_setting.to_json) rescue nil
    reward_code_setting ||= PartnerCodeSetting::DEFAULT_SETTING_REWARDCODE

    reward = Reward.find reward_id
    code_exp = Time.now + number_of_valid_days.days
    code_exp = reward.expiryDate if reward.expiryDate < code_exp
    if reward_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
      barcode =  generate_numeric_code(reward_code_setting["pos_digit_barcode"].to_i, false)
      save = false
      while save == false
        p barcode
        code_count = PartnerRewardCode.where("code = ? AND chain_id = ? AND used IS FALSE", barcode, chain.id).count
        if code_count == 0
            bc = PartnerRewardCode.new( :chain_id => chain.id, :reward_id => reward_id, :code => barcode,
                                        :user_id => user_id, :champain_id => champain_id,
                                        :number_of_valid_days => number_of_valid_days,
                                        :expiration_date => code_exp)
            save =  bc.save rescue false
           barcode =  generate_numeric_code(reward_code_setting["pos_digit_barcode"].to_i, false) if save == false
        else
          barcode =  generate_numeric_code(reward_code_setting["pos_digit_barcode"].to_i, false) if save == false
        end
      end
    elsif reward_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
      barcode = Barcode.generate_alphanumeric_code(reward_code_setting["pos_digit_barcode"].to_i)
      save = false
      while save == false
        code_count = PartnerRewardCode.where("code = ? AND chain_id = ? AND used IS FALSE", barcode, chain.id).count
        if code_count == 0
          bc = PartnerRewardCode.new( :chain_id => chain.id, :reward_id => reward_id, :code => barcode,
                                      :user_id => user_id, :champain_id => champain_id,
                                      :number_of_valid_days => number_of_valid_days,
                                      :expiration_date => code_exp)
          save =  bc.save rescue false
          barcode =  generate_alphanumeric_code(reward_code_setting["pos_digit_barcode"].to_i) if save == false
        else
          barcode =  generate_alphanumeric_code(reward_code_setting["pos_digit_barcode"].to_i) if save == false
        end
      end
    end
    return [barcode, reward_code_setting["zero_padding"].to_i, reward_code_setting["prefix"].to_s, reward_code_setting["postfix"].to_s]
  end

end

