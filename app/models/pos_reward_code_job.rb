class PosRewardCodeJob < Struct.new(:reward_id, :code_collection_on_csv_format)

  def perform
    code_collection_on_csv_format.each do |row|
      RewardPosCode.create(:reward_id => reward_id, :code => row.first)
    end
  end

end