class QuestionChoice < ActiveRecord::Base
  belongs_to :question
  validates :label, :length => {:maximum=>50}, :presence => true
  validates :value, :numericality => true
  NON_APP_DISPLAY = {"value" => 1 , "label" => 2}

  scope :by_ids, -> {|ids| where("id in (?)", ids) }
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
  
end
