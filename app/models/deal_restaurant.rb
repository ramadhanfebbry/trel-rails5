class DealRestaurant < ActiveRecord::Base
  belongs_to :deal
  belongs_to :restaurant


  default_scope { where("deal_restaurants.deleted_at" => nil) }
  def self.deleted
    self.unscoped.where('deal_restaurants.deleted_at IS NOT NULL')
  end
end
