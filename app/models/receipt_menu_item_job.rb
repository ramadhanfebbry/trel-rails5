class ReceiptMenuItemJob < Struct.new(:poster_email, :chain_id, :start_date, :end_date, :keyword)

  def perform
    p "start sending email receipt menu item"
    start_time = Time.current
    email_to = poster_email
    p email_to
    start_date_selected = start_date
    end_date_selected = end_date
    keyword_entered = keyword
    chain = Chain.find chain_id

    search_condition = {:chain_id => chain_id,
                        :receipt_date.gte => start_date_selected,
                        :receipt_date.lte => end_date_selected,
                        :description => /.*#{keyword_entered}.*/i, :receipt_status => 3}
    #search_condition.merge!({:receipt_id => receipt_id}) unless receipt_id.blank?

    query = ReceiptLineItem.where(search_condition)
    result = query.to_a
    end_time = Time.current
    query_time = end_time - start_time

    Mail.defaults do
      delivery_method :smtp, {
          :address => Setting.smtp.host,
          :port => Setting.smtp.port,
          :user_name => Setting.smtp.username,
          :password => Setting.smtp.password,
          :authentication => 'plain',
          :enable_starttls_auto => true
      }
    end

    unless result.blank?
      @filename = "receipt_menu_item.csv"
      CSV.open("#{Rails.root.to_s}/tmp/#{@filename}", "wb") do |csv|
        csv << ["User ID", "User Email", "Receipt Date", "Receipt ID", "Location ID", "Location Title"]
        result.each do |e|
          user = User.find(e.user_id)
          restaurant = Restaurant.find(e.restaurant_id) rescue nil
          csv << [user.id, user.email, e.receipt_date, e.receipt_id, e.restaurant_id, (restaurant.name rescue nil)]
        end
      end
      csv_file_content = File.read("#{Rails.root.to_s}/tmp/#{@filename}")

      mail = Mail.new do
        from Setting.email.default_from
        to email_to
        subject "Receipt Menu Item Search"
        add_file :filename => 'search_menu_item_result.csv', :content =>  csv_file_content
        html_part do
          content_type 'text/html; charset=UTF-8'
          body "Query Result : <br />
              <ul>
                <li>Number of Matches : #{result.size}</li>
                <li>Search Query Posted :
                  <ul>
                    <li>Chain : #{chain.name}</li>
                    <li>Start and End Date : #{start_date_selected} to #{end_date_selected}</li>
                    <li>Keyword Entered : #{keyword_entered}</li>
                    <li>Query Time : #{query_time} seconds </li>
                  </ul>
                </li>
              </ul>".html_safe
        end
      end
      mail.deliver!
    else
      mail = Mail.new do
        from Setting.email.default_from
        to email_to
        subject "Receipt Menu Item Search"
        html_part do
          content_type 'text/html; charset=UTF-8'
          body "Query Result : <br />
              <ul>
                <li>Number of Matches : <b><i>Your query did not match with any available data.</i></b></li>
                <li>Search Query Posted :
                <ul>
                    <li>Chain : #{chain.name}</li>
                    <li>Start and End Date : #{start_date_selected} to #{end_date_selected}</li>
                    <li>Keyword Entered : #{keyword_entered}</li>
                    <li>Query Time : #{query_time} seconds </li>
                  </ul>
                </li>
              </ul>".html_safe
        end
      end
      mail.deliver!
    end
    p "end------------------"
  end

end
