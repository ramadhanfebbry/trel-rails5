class ReferralNotification < ActiveRecord::Base
  belongs_to :notifiable, :polymorphic => true
  belongs_to :locale

  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
end
