class MenuItemReport
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :chain_id, :reward_id, :email, :start_date, :end_date

  validates :chain_id, :reward_id, :start_date, :end_date, :presence => true
  validates :email, :presence => true, :format => {:with =>  /^[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}$/i}
  validate :end_date_cannot_greater_than_start_date

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def end_date_cannot_greater_than_start_date
    unless self.start_date.blank? and self.end_date.blank?
      begin
        if self.start_date > self.end_date
          errors.add(:start_date, "must be less that end date")
          errors.add(:end_date, "must be greater than start date")
        end
      rescue => e
        puts "#{e.inspect}"
      end
    end
  end

  def persisted?
    false
  end
end