class ReceiptMenuSearch

  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend  ActiveModel::Naming

  attr_accessor :chain_id, :start_date, :end_date, :keyword

  validates :chain_id, :start_date, :end_date, :keyword, :presence => true

  validate :custom_search_validation

  def initialize(param)
    @chain_id = param[:chain_id] rescue nil
    @start_date = param[:start_date] rescue nil
    @end_date = param[:end_date] rescue nil
    @keyword = param[:keyword] rescue nil
  end

  def custom_search_validation
    start_d = Date.parse(@start_date) rescue nil
    end_d = Date.parse(@end_date) rescue nil
    if start_d && end_d
      if start_d >= end_d
        self.errors.add(:start_date, "Must greater than end date")
      end

      if @keyword.size < 3
        self.errors.add(:keyword, "Minimum 3 character to search")
      end
    end
  end

  def persisted?
    false
  end

end