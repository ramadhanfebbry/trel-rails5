class MenuItemReportJob < Struct.new(:reward_id, :user_email, :start_date, :end_date)

  def perform
    p "----------perform menu item report job------"
    reward = Reward.find(reward_id)
    p reward
    p user_email
    p start_date
    p end_date
    path = "#{Rails.root}/tmp/reward_item_user_support_report_#{reward.id}_#{Time.now}.csv"
    chain = reward.chain
    count = chain.users.count
    loop = (count.to_f / 500.to_f).ceil
    p "total loop #{loop}"
    File.new(path, 'w')
    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      csv << ["User ID",
              "User Email",
              "Location",
              "Receipt Id",
              "#{reward.name} Redemption day",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit"
      ]

      1.upto(loop) do |page|
        p "loop ke #{page}"
        users = chain.users.paginate(page: page, per_page: 500)
        users.each do |user|
          reward_transactions = RewardTransaction.where("user_id = ? AND reward_id = ? AND date_trunc('day', (created_at::timestamp AT TIME ZONE '-05')) >= ? AND date_trunc('day', (created_at::timestamp AT TIME ZONE '-05')) <=  ?", user.id, reward.id, start_date, end_date)
          reward_transactions.each do |rt|
            csv_content = nil
            restaurant = rt.restaurant
            claim_date = rt.created_at.to_date
            receipts = Receipt.where(:user_id => user.id, :status => Receipt::STATUS[:APPROVED])
            last_transactions = receipts.map{|r| r.last_transaction}

            last_transactions_gte_claim_date = last_transactions.select{|a| a.issue_date ? a.issue_date.to_date >= claim_date : a.created_at.to_date >= claim_date }
            another_receipts = []
            on_winter_claim_date = last_transactions_gte_claim_date.select{|a| a.issue_date ? a.issue_date.to_date.eql?(claim_date) : a.created_at.to_date.eql?(claim_date)}.first
            if on_winter_claim_date
              another_receipts = last_transactions_gte_claim_date.delete_if{|a| a.id == on_winter_claim_date.id}
            else
              another_receipts = last_transactions_gte_claim_date
            end

            # data rows
            csv_content = [
                user.id,
                user.email,
                (on_winter_claim_date.receipt.id rescue nil),
                (restaurant.name.gsub(",", " ") rescue "null"),
                (on_winter_claim_date.subtotal.to_s rescue "0")
            ]
            another_receipts.sort{|a, b| (a.issue_date ? a.issue_date : a.created_at) <=> (b.issue_date ? b.issue_date : b.created_at)}.each do |rt|
              csv_content << (rt.subtotal.to_s rescue "0")
            end
            csv << csv_content
          end
        end
      end
    end
    OwnerMailer.send_reward_redeemed(nil, user_email, path, "#{reward.name} and purchase report_#{start_date}_#{end_date}.csv","Menu Item Report #{reward.name}").deliver!
    File.delete(path)
    puts "Success EXECUTING report"
  end

end