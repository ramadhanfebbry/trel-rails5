class SteppingstoneStep < ActiveRecord::Base

  attr_reader :point_as_incentive
  attr_accessor :point_as_incentive

  belongs_to :stepable, :polymorphic => true
  belongs_to :reward

  validates :step_number, :presence => true, :uniqueness => {:scope => [:stepable_id, :stepable_type]}, :numericality => {:greater_than => 0, :only_integer => true, :message => "Must be a integer number and greater than 0"}
  validates :property_one, :presence => true, :numericality => {:greater_than => 0, :only_integer => true, :message => "Must be a integer number and greater than 0"}
  validates :property_two, :presence => true, :numericality => {:greater_than => 0, :message => "Must be a number and greater than 0"}

   validates :point, :numericality => {
    :greater_than => 0,
    :only_integer => true
   }, :if => :validate_point_required?

   def validate_point_required?
   	self.point_as_incentive
   end



end