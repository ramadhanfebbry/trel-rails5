class MomentfeedSetting < ActiveRecord::Base
	belongs_to :chain
  # attr_accessible :api_key, :base_url, :update_interval, :update_interval_option, :update_type, :chain_id
  attr_accessor :update_interval_option_1, :update_interval_option_2

  validates :api_key, presence: true, if: :base_url?
  validates :base_url, presence: true, if: :api_key?
end
