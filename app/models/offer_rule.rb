class OfferRule < ActiveRecord::Base
  belongs_to :offer
  before_save :set_day_week
  serialize :day_of_week_converter
  TYPE = {
      "POINTS MULTIPLIER" =>  1,
      "CONSTANT POINTS" => 2,
      "MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER" =>  3,
      "MENU ITEM BASED- CONSTANT POINTS" => 4,
      "MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER, IGNORE SUBTOTAL" => 5,
      "MENU ITEM BASED - IGNORE MENU ITEMS" => 6,
      "MENU ITEM BASED - IGNORE/REJECT RECEIPT" => 7,
      "MENU ITEM BASED - QUALIFYING ORDERED ITEMS SUBTOTAL, MULTIPLIER" => 8
  }

  validates_numericality_of :multiplier, :greater_than_or_equal_to=>1, :less_than_or_equal_to => 50, :presence => true, :if => :is_val_multiplier?

  validates_numericality_of :min_amount, :greater_than_or_equal_to=>0, :presence => true,
                            :if => [:is_not_subtotal_zero?]

  validates_numericality_of :max_amount, :greater_than_or_equal_to=>0, :presence => true,
                            :if => [:is_not_subtotal_zero?]


  validates_presence_of :start_date
  validates_presence_of :start_date, :format => {:with => /(^\d{2}-\d{2}-\d{4}$)/},  :if => :validate_expiry_date

  validate :validate_week_selected
  validates_presence_of :time_start,
                        :format => {:with => /^([01]\d:[0-5]\d(AM|PM))?$/, :message => "Incorrect time format for timeStart"}
  validates_presence_of :time_end,
                        :format => {:with => /^([01]\d:[0-5]\d(AM|PM))?$/, :message => "Incorrect time format for timeEnd"}
  validates_presence_of :name

  validates :constant_value, :numericality => {
      :greater_than_or_equal_to => 0,
      :allow_blank => false
  }, :if => :is_constant_value?

  has_many :offer_rule_menu_items, :dependent => :destroy
  has_many :general_menu_items, :through => :offer_rule_menu_items

  scope :active , -> { where("status IS TRUE") }

  after_save :update_rule_flag

  def include_day_of_week(day)
     #self.days_of_week[day]==1 unless self.days_of_week.nil?
    self.day_of_week_converter.include?(day.to_s)
  end

  def validate_week_selected
    if self.day_of_week_converter.size == 1  or self.day_of_week_converter.blank?
      errors.add(:day_of_week_converter, "Days Cannot Be Blank ! ")
    end
  end

  def set_day_week
    dow = 0b0000000
       self.day_of_week_converter.each do |day|
         dow = dow + (1 << day.to_i)
       end
       write_attribute(:days_of_week, dow)
  end

  def is_val_multiplier?
    self.is_multiplier
  end

  def is_constant_value?
    !self.is_multiplier
  end

  def is_multiplier
    self.rule_type == 1 or self.rule_type == 3 or self.rule_type == 5 or  self.rule_type == 8
  end

  def is_multiplier?
    self.rule_type == 1 or self.rule_type == 3 or self.rule_type == 5 or self.rule_type == 8
  end


  def multiplier_constant_value
    if self.is_multiplier
      self.multiplier
    else
      self.constant_value
    end
  end

  def validate_expiry_date
    if self.end_date && self.start_date
      errors.add( :end_date, "must be after the From Date") if self.start_date > self.end_date
    end
  end

  def is_general_items?
    self.rule_type == OfferRule::TYPE["MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER"] or
        self.rule_type ==  OfferRule::TYPE["MENU ITEM BASED- CONSTANT POINTS"] or
        self.rule_type ==  OfferRule::TYPE["MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER, IGNORE SUBTOTAL"] or
        self.rule_type ==  OfferRule::TYPE["MENU ITEM BASED - IGNORE MENU ITEMS"] or
        self.rule_type == OfferRule::TYPE["MENU ITEM BASED - IGNORE/REJECT RECEIPT"] or
        self.rule_type == OfferRule::TYPE["MENU ITEM BASED - QUALIFYING ORDERED ITEMS SUBTOTAL, MULTIPLIER"]
  end

  def is_subtotal_zero?
    self.rule_type ==  OfferRule::TYPE["MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER, IGNORE SUBTOTAL"]
  end

  def is_not_subtotal_zero?
    self.rule_type !=  OfferRule::TYPE["MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER, IGNORE SUBTOTAL"]
  end

  def update_rule_flag
    begin
      rules = self.offer.offer_rules
      s_date = rules.map(&:start_date).sort.first
      e_date = rules.map(&:end_date).sort.last

      self.offer.update_column(:effectiveDate,s_date)
      self.offer.update_column(:expiryDate,e_date)
    rescue => e
      puts "OFFERRULE::ERROR::update_rule_flag=#{e.inspect}"
    end
  end

end