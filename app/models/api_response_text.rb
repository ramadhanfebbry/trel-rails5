# encoding: utf-8
class ApiResponseText < ActiveRecord::Base

	belongs_to :chain
	belongs_to :locale

	MESSAGE_FOR = {
		"Receipt Submission Success" => 1,
		"Receipt Submission Fail" => 2,
		"Signup Success With Referral Code" => 3,
		"Signup Success Without Referral Code" => 4,
    "Survey Submission Success" => 5,
    "Survey Submission Fail" => 6,
    "Barcode Scanned Successfully" => 7,
    "Forgot Password Error" => 8,
    "Error Scanned TS receipts" => 9
	}

	DEFAULT_MESSAGE = {
    "en" => {
    	"Receipt Submission Success" => "Thank you. We've received your receipt. You will receive a status update email within 24 hours.",
		  "Receipt Submission Fail" => "Sorry, something went wrong. Could not upload receipt. Please try again.",
		  "Signup Success With Referral Code" => "Your account has been created and referral code has been submitted.",
		  "Signup Success Without Referral Code" => "Your account has been created.",
      "Survey Submission Success" => "Thank you. We appreciate hearing from you.",
      "Survey Submission Fail" => "unknown question / answer type",
      "Barcode Scanned Successfully" => "Groovy! Your code was successfully scanned. You will receive an email notification from us in the next 24 hours.",
      "Forgot Password Error" => "Invalid email address, please check for any typos.  If you created your account using Facebook please go back and use the Sign In With Facebook button to access your account.",
      "Error Scanned TS receipts" => "We are sorry. The code scanned is either invalid or expired. Please contact customer support from the info section of the app for further assistance."
    },
    "es" => {
    	"Receipt Submission Success" => "Gracias, ya recibimos tu cuenta. Dentro de 24 horas recibirás un correo con el estatus actualizado de tu cuenta.",
		  "Receipt Submission Fail" => "Se esta creando un error al subir su recibo. Por favor intente nuevamente.",
		  "Signup Success With Referral Code" => "Nueva cuenta creada. Por favor consulta la sección de recompensas para ver qué puedes ganar.",
		  "Signup Success Without Referral Code" => "Nueva cuenta creada. Por favor consulta la sección de recompensas para ver qué puedes ganar.",
      "Survey Submission Success" => "Gracias por tus comentarios.",
      "Survey Submission Fail" => "Tipo de pregunta o respuesta no reconocido de la encuesta.",
      "Barcode Scanned Successfully" => "Groovy! Your code was successfully scanned. You will receive an email notification from us in the next 24 hours.",
      "Forgot Password Error" => "Email o contraseña invalida.",
      "Error Scanned TS receipts" => "We are sorry. The code scanned is either invalid or expired. Please contact customer support from the info section of the app for further assistance."
    }

	}

	validates :message, :message_for, :locale_id, :presence => true


	def self.get_message_chain_by_locale(chain_id, locale_id, type)
    response_text = ApiResponseText.where(:chain_id => chain_id, :locale_id => locale_id, :message_for => type).first
    message = response_text.message rescue nil
    if message.blank?
      locale = Locale.find(locale_id)
      message = ApiResponseText::DEFAULT_MESSAGE[locale.key][ApiResponseText::MESSAGE_FOR.invert[type]] rescue nil
    end
    message
	end
end
