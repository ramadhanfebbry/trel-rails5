class NotificationSetting < ActiveRecord::Base
  AdminPointPushNotification = AdminNotificationLocale

  TYPES = {
      "Relevant" => 1,
      "Mandrill" => 2,
      "Fishbowl" => 3,
      "Bronto" => 4,
      "Myemma" => 5
  }

  TEMPLATE_NAMES =["welcome", "forgot_password", "points_notification_user", "rewards_notification_user", "admin_push_points_notification", "admin_push_reward_notification", "gifting_card_gift_notification", "billing_notification_add_balance_using_cc", "billing_notification_gifter_billing_notification", "birthday_notification", "we_miss_you_reward_notification", "no_scan_promotion_notification"]

  TYPE_DISPLAY_NAMES = {
    "welcome"=>"WELCOME EMAIL",
    "forgot_password"=>"FORGOT PASSWORD",
    "points_notification_user"=>"POINTS NOTIFICATION - USER",
    "rewards_notification_user"=>"REWARDS NOTIFICATION - USER",
    "admin_push_points_notification"=>"ADMIN PUSH POINTS NOTIFICATION",
    "admin_push_reward_notification"=>"ADMIN PUSH REWARD NOTIFICATION",
    "gifting_card_gift_notification"=>"GIFTING CARD - GIFT NOTIFICATION",
    "billing_notification_add_balance_using_cc"=>"BILLING NOTIFICATION - ADD BALANCE USING CC",
    "billing_notification_gifter_billing_notification"=>"BILLING NOTIFICATION - GIFTER BILLING NOTIFICATION",
    "birthday_notification"=>"BIRTHDAY NOTIFICATION",
    "we_miss_you_reward_notification"=>"We Miss You Reward Notification",
    "no_scan_promotion_notification"=>"No Scan Promotion notification"}

  TYPE_MODELS = {"welcome"=> "EmailTemplate",
   "forgot_password"=> "EmailTemplate",
   "points_notification_user"=> "PointPushNotification",
   "rewards_notification_user"=> "NotificationLocale",
   "admin_push_points_notification"=> "AdminNotificationLocale",
   "admin_push_reward_notification"=> "AdminNotificationLocale",
   "gifting_card_gift_notification"=> "GiftcardNotificationEmail",
   "billing_notification_add_balance_using_cc"=> "",
   "billing_notification_gifter_billing_notification"=> "",
   "birthday_notification"=> "BirthdayRewardNotification",
   "we_miss_you_reward_notification"=> "Reward",
   "no_scan_promotion_notification"=> "NoScanDefinition"}

    TYPE_QUERY = {
     "welcome"=>"chain.email_templates.where(template_name: \"welcome\").first",
     "forgot_password"=>"chain.email_templates.where(template_name: \"forgot_password\").first",
     "points_notification_user"=>"chain.point_push_notifications.last",
     "rewards_notification_user"=>"chain.notification_locales.last",
     "admin_push_points_notification"=>"chain.admin_push_point_notifications.first",
     "admin_push_reward_notification"=>"chain.admin_push_reward_notifications.first",
     "gifting_card_gift_notification"=>"chain.giftcard_notification_emails.first",
     "billing_notification_add_balance_using_cc"=>nil,
     "billing_notification_gifter_billing_notification"=>nil,
     "birthday_notification"=>"chain.birthday_reward_notifications.first",
     "we_miss_you_reward_notification"=>nil,
     "no_scan_promotion_notification"=>nil
   }

  belongs_to :notifiable, :polymorphic => true
  belongs_to :chain

  default_scope { order("id ASC") }

  def self.set_default(chain)
    TEMPLATE_NAMES.each do |name|

      query = eval(TYPE_QUERY[name]) rescue nil
      self.create({
        chain_id: chain.id,
        notifiable_id: (query.id rescue nil),
        notifiable_type: TYPE_MODELS[name],
        email_template: name,
        send_type: 1
        })
    end
  end

  def name
    custom_email_template || TYPE_DISPLAY_NAMES[email_template]
  end

  def notifiable
    self.notifiable_type.constantize.find(notifiable_id) rescue nil
  end

  def update_email_template(params)
    my_chain =  chain.update_attributes(params[:chain])
    if my_chain
      return update_attributes(params[:notification_setting])
    end
    return  my_chain
  end

  def type_send_email
    type_sends = ['', "Relevant", "Mandrill", "Fishbowl"]
    type_sends[send_type]
  end

  def generate_setting
    my_chain = chain
    if notifiable_type.eql?("EmailTemplate")
      # return self.build_email_setting
      # template = my_chain.email_templates.where(template_name: email_template)
      #  (my_chain.locales - template.map{|gl| gl.locale}).each do |locale|
      #   my_chain.point_push_notifications.build(:locale => locale)
      # end
    elsif  notifiable_type.eql?("PointPushNotification")
      (my_chain.locales - my_chain.point_push_notifications.map{|gl| gl.locale}).each do |locale|
        my_chain.point_push_notifications.build(:locale => locale)
      end
    elsif  notifiable_type.eql?("NotificationLocale")
      (my_chain.locales - my_chain.notification_locales.map{|gl| gl.locale}).each do |locale|
        my_chain.notification_locales.build(:locale => locale)
      end
     elsif  notifiable_type.eql?("AdminNotificationLocale") && email_template.eql?("admin_push_points_notification")
      (my_chain.locales - my_chain.admin_push_point_notifications.map{|gl| gl.locale}).each do |locale|
        my_chain.admin_push_point_notifications.build(:locale => locale)
      end
      elsif notifiable_type.eql?("AdminNotificationLocale") && email_template.eql?("admin_push_reward_notification")
      (my_chain.locales - my_chain.admin_push_reward_notifications.map{|gl| gl.locale}).each do |locale|
        my_chain.admin_push_reward_notifications.build(:locale => locale)
      end
    elsif notifiable_type.eql?("BirthdayRewardNotification")
      (my_chain.locales - my_chain.birthday_reward_notifications.map{|gl| gl.locale}).each do |locale|
        my_chain.birthday_reward_notifications.build(:locale => locale)
      end
    elsif notifiable_type.eql?("BirthdayRewardNotification")

    end
      return my_chain
  end

  # def build_email_setting
  #   email = notifiable_id.nil? ? notifiable_type.constantize.new : notifiable_type.constantize.find(notifiable_id)
  #   email.chain_id = chain_id if  notifiable_id.nil?
  #   return email
  # end

  def name_key_nested
    nested = notifiable_type.pluralize.underscore
    if notifiable_type.eql?("AdminNotificationLocale") && email_template.eql?("admin_push_points_notification")
      nested = "admin_push_point_notifications"
    elsif  notifiable_type.eql?("AdminNotificationLocale") && email_template.eql?("admin_push_reward_notification")
      nested = "admin_push_reward_notifications"
    end
    return nested
  end

end
