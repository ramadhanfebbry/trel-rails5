class Term < ActiveRecord::Base
  validates_uniqueness_of :locale_id
  belongs_to :locale

  def self.content_by_locale(locale_key)
    terms = Term.includes(:locale).where('locales.key=?', locale_key).first
    "#{terms.try(:content)}"
  end
end