class NuorderConnectSetting < ActiveRecord::Base
  belongs_to :chain

  def self.get_nuorder_connect_setting(chain_id)
    nuorder_connect_setting = NuorderConnectSetting.find_by_chain_id(chain_id)
    if nuorder_connect_setting
      {
          "api_root" => nuorder_connect_setting.api_root,
          "requester_id" => nuorder_connect_setting.requester_id,
          "license_code" => nuorder_connect_setting.license_code,
          "passcode" => nuorder_connect_setting.passcode,
          "subscriber_id" => nuorder_connect_setting.subscriber_id,
          "location_id" => nuorder_connect_setting.location_id
      }
    else
      nil
    end
  end

end
