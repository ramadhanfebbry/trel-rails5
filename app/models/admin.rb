class Admin < ActiveRecord::Base
  devise :database_authenticatable, :trackable, :timeoutable

  # Setup accessible (or protected) attributes for your model
  # attr_accessible :email, :password, :password_confirmation, :role

  validates :email, :presence => true, :uniqueness => true
  validates :role, :presence => true
  validates :password, :password_confirmation, :presence => true, :on => :create
  validates_confirmation_of :password, :message => "should match confirmation"
  has_many :plain_push_notifications
  has_many :push_service_notifications
  has_many :push_service_points

  def has_role?(role)
    return false if self.role.blank?
    return false if role.blank?
    self.role.eql?(role)
  end
 
end
