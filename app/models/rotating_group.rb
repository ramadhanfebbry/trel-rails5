class RotatingGroup < ActiveRecord::Base

  after_save :change_to_uncomplete_for_answered_questions

  belongs_to :chain
  belongs_to :survey

  has_many :questions

  validates :name, :chain_id, :group_type, :presence => true
  validates :max_renders, :presence => true

  GROUP_TYPE = {:DEMOGRAPHIC => 1, :SEASONAL => 2}


  def group_type_name
    if RotatingGroup::GROUP_TYPE[:DEMOGRAPHIC].eql?(self.group_type)
      "one time/profile/demographic"
    elsif RotatingGroup::GROUP_TYPE[:SEASONAL].eql?(self.group_type)
      "event/seasonal"
    else
      "None"
    end
  end

  def change_to_uncomplete_for_answered_questions
    if self.max_renders_changed?
      self.questions.each do |question|
        UserRotatingQuestion.where(:question_id => question.id).each do |rotating_question|
          if rotating_question.answer_id.blank?
            rotating_question.update_attributes(:completed => false, :render_count => 0)
          end
        end
      end
    end
  end

end