class Owner < ActiveRecord::Base

  validates :first_name, :presence => true, :length => {:minimum => 2, :maximum=>50}, :format => {:with => /\A[a-z A-Z]+\z/,
    :message => "(Only letters allowed)"}
  validates :chain_id, :presence => true, :if => Proc.new{|x| x.new_record?}
  validates :restaurant_collections, :presence => true, :if => Proc.new{|x| x.new_record? and x.role_id.eql?(2)}

  #  validates :address, :presence => true, :length => {:maximum=>100}, :format => {:with => /\A[-a-zA-Z0-9 ()-]+\z/,
  #    :message => "(Only alphanumeric and special(-) characters allowed)"}

  #  validates :zipcode, :presence => true, :format => {:with => /(^\d{5}$)|(^\d{5} \d{4}$)/,
  #    :message => "should be like (12345 or 12345 3233)"}

  #  validates :work_contact_number, :cell_contact_number, :presence => true, :length => {:maximum => 20}, :format => {:with => /\A[-0-9 ()-+]+\z/,
  #    :message => "(Only numbers and special(-+) characters allowed)"}

  default_scope { where("deleted_at" => nil) }
  scope :active, -> { where(:is_active => true) }

  #after_save :create_or_update_ldap

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
  #belongs_to :chain
  belongs_to :city
  belongs_to :region
  has_many :chains_owners, :dependent => :destroy
  has_many :restaurants_owners, :dependent => :destroy
  has_many :report_owners
  has_many :chains, :through => :chains_owners
  has_many :restaurants, :through => :restaurants_owners

  has_many :owner_jobs, :dependent => :destroy
  has_many :guest_relations , :dependent => :destroy
  accepts_nested_attributes_for :restaurants_owners

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :password_generated, :chain_id, :restaurant_collections
  
  # Setup accessible (or protected) attributes for your model
  # attr_accessible :email, :password, :password_confirmation, :remember_me,
    # :first_name, :last_name, :chain_id, :password, :city_id, :zipcode,
    # :work_contact_number, :address, :cell_contact_number, :title,	:daily,	:weekly,
    # :monthly,	:periodic, :parent_id, :restaurants_owners_attributes, :role_id, :restaurant_ids, :restaurant_collections

  before_create :set_role_chain

  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token.extra.raw_info
    if owner = Owner.where(:email => data.email).first
      owner
    else # Create a user with a stub password.
      Owner.create!(:email => data.email, :password => Devise.friendly_token[0, 20])
    end
  end

  def self.new_with_session(params, session)
    super.tap do |owner|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        owner.email = data["email"]
      end
    end
  end

  def restaurant_list
      if role_id.blank? || chain_owner?## the old user before get the new logic
        return Restaurant.where("chain_id in (?)",
                                self.chains.map(&:id)).
            select("distinct(restaurants.*)").where('restaurants.status is TRUE').
            order('restaurants.dashboard_display_text asc') rescue self.restaurants.select("distinct(restaurants.*)").
            where('restaurants.status is TRUE').
            order('restaurants.dashboard_display_text asc')
      else
        return self.restaurants.select("distinct(restaurants.*)").
            order('restaurants.dashboard_display_text asc')
      end
  end

  def active_restaurant_list
    if role_id.blank? || chain_owner?## the old user before get the new logic
      return Restaurant.where("chain_id in (?) and restaurants.status = 't'",
                              self.chains.map(&:id)).
          select("distinct(restaurants.*)").
          order('restaurants.dashboard_display_text asc') rescue self.restaurants.active.select("distinct(restaurants.*)").order('restaurants.dashboard_display_text asc')
    else
      return self.restaurants.active.select("distinct(restaurants.*)").
          order('restaurants.dashboard_display_text asc')
    end
  end

  def set_role_chain
    if role_id == 1 || role_id.blank? ## if its chain owner
      role = "email_process"
    else
      role = nil
    end
  end


  def chain_list
    if role_id.blank? || role_id == 1
      self.chains
    else
      [self.chain]
    end
  end

  def chain_ids
    [restaurant_list.first.chain.id] rescue []
  end

  def is_chain_owner?
    self.role_id.eql?(1) || self.role_id.blank?
  end

  def chain_owner?
    self.role_id == 1 || self.role_id.blank?
  end

  def chain
      return self.restaurant_list.first.chain rescue nil
  end

  def self.update_chains
    Owner.all.each do |ow|
      begin
      ow.chains = []
      ow.chains <<  ow.chain
      ow.save
      rescue
        next
      end
    end
  end

  def self.update_permission
    ##update permission for the first time
    Owner.all.each do |ow|
      if ow.role_id == 1 || ow.role_id.blank?
        ow.update_column(:role, 'all_setting')
      end
    end
  end

  ## dashboard 2.3
  def has_role?(role)
    return false if self.role.blank?
    return false if role.blank?
    self.role.eql?(role.to_s)
  end

  def visible_create_request?
    return role.to_s.include?('create') || role.to_s.include?('process')
  end

  #def restaurant_ids
  #  chains = self.chains.includes(:restaurants)
  #  restaurant_ids = []
  #  chains.each do |ch|
  #    restaurant_ids << ch.restaurants.map(&:id)
  #  end
  #  restaurant_ids.flatten if restaurant_ids.size > 0
  #end

  #before_validation :set_password

  def full_name
    [first_name, last_name].join(' ')
  end

  def set_password
    chars = ("a".."z").to_a + ("1".."9").to_a
    self.password = Array.new(8, '').collect { chars[rand(chars.size)] }.join
  end

  def is_owner?
    self.parent_id.blank? rescue true
  end

  def is_sub_owner?
    !is_owner? rescue false
  end

  def chain_obj
    self.restaurants.first.chain rescue nil
  end

  #def self.update_chains(owner, params)
  #  chains = params[:chains];
  #  ids = params[:ids].split
  #
  #  if !chains.blank?
  #    chains.each do |chain_id|
  #
  #      if ids.include?(chain_id)
  #        ids.delete(chain_id)
  #      else
  #        ChainsOwner.create(:chain_id=>chain_id, :owner_id=>owner.id)
  #      end
  #    end
  #  end
  #
  #  if !ids.blank?
  #    ChainsOwner.where("owner_id=#{params[:id]} and chain_id in (?)", ids).delete_all
  #  end
  #end

  def owner_users
    Owner.where("parent_id = ?", self.id)
  end

  def set_restaurant_owner
    self.restaurant_ids = Restaurant.select("id, chain_id").where(chain_id: self.chain_id).map(&:id) unless self.chain_id.blank?
  end

  def set_chain(ch_id)
    self.chains << Chain.find(ch_id)
  end

  def update_participation(chains, old_participants)
    ids = old_participants.split
    intersection = chains & ids;
    if (!ids.blank?)
      pullout((ids-intersection))
    end
    if (!chains.blank?)
      participate((chains-intersection));
    end
  end

  def update_participation_restaurant(restaurants, old_participants)
    ids = old_participants.split
    intersection = restaurants & ids;
    if (!ids.blank?)
      pullout_restaurant((ids-intersection))
    end
    if (!restaurants.blank?)
      participate_restaurant((restaurants-intersection));
    end
  end

  def pullout(chains)
    self.chains_owners.where("chain_id in (?)", chains).destroy_all
  end

  def participate(chains)
    chains.each { |chain_id| ChainsOwner.create(:owner_id => self.id, :chain_id => chain_id) }
  end

  def pullout_restaurant(restaurants)
    self.restaurants_owners.where("restaurant_id in (?)", restaurants).destroy_all
  end
  
  def participate_restaurant(restaurants)
    restaurants.each { |res_id| RestaurantsOwner.create(:owner_id => self.id, :restaurant_id => res_id) }
  end

  def bulk_participate_restaurant(restaurants)
    p "bulk_participate_restaurant start"
    obj = self
    res_owners = []
    restaurants.each do |res_id|
      res_owners << RestaurantsOwner.new(:owner_id => obj.id, :restaurant_id => res_id)
    end
    Owner.import res_owners
    p "bulk_participate_restaurant end"
  end

  def generate_password
    self.password_generated = (('a'..'z').to_a + ('0'..'9').to_a).shuffle.first(8).join
    self.password = self.password_generated
    self.password_confirmation = self.password_generated
  end

  def take_action?
    if self.is_chain_owner?
      setting = REDIS.get "db_take_action_setting_#{self.id}"
      return setting.blank? ? "ON" : "OFF"
    else
      return "OFF"
   end
  end

  def self.chain_take_action?(chain)
    setting = REDIS.get "db_take_action_setting_chain_#{chain.id}"
    return setting.blank? ? "ON" : "OFF"
  end

  def self.owner_chain_take_action?(owner)
    #setting = REDIS.get "db_take_action_setting_chain_owner_#{owner.id}"
    setting = REDIS.get "db_take_action_setting_#{owner.chain.id}_#{owner.id}"
    if owner.is_chain_owner?
      return setting.blank? ? "ON" : "OFF"
    else
      return setting.blank? ? "OFF" : "ON"
    end
  end

  def is_manager?
    self.role_id == 3
  end

  def can_approve?
    self.role.include?('process')
  end

  def set_role(mode, val)
    if val == "ON"
      a = self.role.to_s + '_' + mode.to_s
      a = a.split('_').sort.join('_')
      a = a.parameterize("_")
      a = a.split('_').uniq.join('_')
      a = a.gsub('_','') if a.split('_') == 0
      puts "a ========= #{a}"
      self.update_column(:role, a )
    else
      puts "val == #{val}"
      a = self.role.to_s + '_' + mode.to_s
      a = a.split('_').sort.join('_').gsub(mode,'')
      a = a.remove('_') if a.split('_') == 0
      a = a.split('_').uniq.join('_')
      a = a.parameterize("_")
      puts "a ========= #{a}"
      self.update_column(:role, a )
      end
  end

  def create_or_update_ldap
    puts "create_or_update_ldap"
    ldap = Ldap.new
    ldap.add_or_remove_chain_owner(self)
  end

end
