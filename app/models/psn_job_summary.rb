class PsnJobSummary < ActiveRecord::Base
  MPDB_CONFIG = {
      :adapter  => "postgresql",
      :host     => ENV["JAVA_PN_DB_HOST"] || "104.236.198.63",
      :port     => ENV["JAVA_PN_DB_PORT"] || "5432",
      :username => ENV["JAVA_PN_DB_USER"] || "postgres",
      :password => ENV["JAVA_PN_DB_PWD"]  || "t0m1s123",
      :database => ENV["JAVA_PN_DB_NAME"] || "pndb"
  }
  self.abstract_class = true
  establish_connection MPDB_CONFIG
  self.table_name = "job_summary"
end
