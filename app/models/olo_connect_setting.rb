class OloConnectSetting < ActiveRecord::Base
  belongs_to :chain

  def self.get_olo_connect_setting(chain_id)
    olo_connect_setting = OloConnectSetting.find_by_chain_id(chain_id)
    if olo_connect_setting
      {
          "api_root" => olo_connect_setting.api_root,
          "api_key" => olo_connect_setting.apikey,
          "iphone_api_key" => olo_connect_setting.iphone_api_key,
          "android_api_key" => olo_connect_setting.android_api_key,
          "client_id" => olo_connect_setting.client_id,
          "client_secret" => olo_connect_setting.client_secret,
          "provider" => olo_connect_setting.provider
      }
    else
      nil
    end
  end

  def self.api_key_based_on_user_device(olo_setting, user)
    if user.register_device_type.to_s.downcase == "android"
      p "ANDROID APIKEY"
      olo_setting["android_api_key"]
    elsif user.register_device_type.to_s.downcase == "iphone"
      p "IPHONE APIKEY"
      olo_setting["iphone_api_key"]
    else
      p "GLOBAL APIKEY"
      olo_setting["api_key"]
    end
  end

  def api_key

  end

end
