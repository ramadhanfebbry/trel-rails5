class PartnerMenuItemParticipatingJob < Struct.new(:chain, :xml_menu_data, :restaurants)

  def perform
    p " = = = =  perform PartnerMenuItemParticipatingJob = = = = "
    @chain = chain
    @restaurants = restaurants rescue nil
    @xml_menu_data = xml_menu_data rescue nil

    require 'nokogiri'
    xml_in_nokogiri_format = Nokogiri::XML(xml_menu_data)
    xml_in_nokogiri_format.xpath('//menu-item').each do |menu_item|
      size = menu_item.xpath("size").text rescue nil
      existing_menu_item = PartnerMenuItem.where(chain_id: @chain.id, item_name: menu_item.xpath("name").text, item_number: menu_item.attr("id")).first
      unless existing_menu_item
        existing_menu_item = PartnerMenuItem.create(
            :item_name => menu_item.xpath("name").text,
            :description => menu_item.xpath("short-description").text,
            :item_number => menu_item.attr("id"),
            :chain_id => @chain.id
        )
        existing_menu_item.participate(@restaurants) rescue nil
      else
        existing_menu_item.update_attributes(
            :item_name => menu_item.xpath("name").text,
            :description => menu_item.xpath("short-description").text,
            :item_number => menu_item.attr("id"),
            :chain_id => @chain.id
        )
        existing_menu_item.participate(@restaurants) rescue nil
      end
    end

    p " = = = =  END PartnerMenuItemParticipatingJob = = = = "
  end

end