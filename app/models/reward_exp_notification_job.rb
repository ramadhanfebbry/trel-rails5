class RewardExpNotificationJob < Struct.new(:chain, :date, :loop)

  def perform
    RewardWallet.includes(:user, :reward).where("reward_wallets.status = ? AND date(TIMEZONE('UTC', reward_wallets.expiry_date) AT TIME ZONE '#{date.strftime('%Z')}') = ? AND rewards.points <= users.points AND rewards.reward_type != 3 AND users.chain_id = ?", 1, date.strftime("%Y-%m-%d"), chain.id).paginate(page: loop, :per_page => 100).each do |rw|
      user = rw.user
      reward_exp_notification = chain.reward_exp_notifications.where(:locale_id => user.locale_id).first
      reward_exp_notification.send_to_user(rw, user) if reward_exp_notification
    end
  end

end