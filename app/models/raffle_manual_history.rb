class RaffleManualHistory < ActiveRecord::Base

  belongs_to :raffle
  belongs_to :raffle_step
  belongs_to :chain
  belongs_to :user

end
