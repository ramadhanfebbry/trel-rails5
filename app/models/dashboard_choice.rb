class DashboardChoice < ActiveRecord::Base
  belongs_to :dashboard_question #, :foreign_key => :question_id

 # validates :label, :length => {:maximum=>50}, :presence => true
  validates :value, :numericality => true, :presence => true , :inclusion => { :in => 1..1000, :message => "number must be between 1 and 100" }
end
