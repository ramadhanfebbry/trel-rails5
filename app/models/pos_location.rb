class PosLocation < ActiveRecord::Base
  include KeyUtil

  belongs_to :restaurant
  belongs_to :chain

  has_many :pos_check_uploads
  has_many :pos_menu_uploads
  has_many :pos_menu_items
  has_many :pos_menus
  validates :restaurant_id, :chain_id, :presence => true
  validates :apikey, :presence => true, :uniqueness => true

  after_create :copy_the_detail_of_pos_location

  def set_apikey
    size = Setting.application.appkey_size
    chars = Setting.application.appkey_chars

    key = generate_random_string(size, chars)
    while !PosLocation.where(:apikey => key).blank?
      key = generate_random_string(size, chars)
    end
    self.apikey = key
  end

  def last_check_upload_received
    self.pos_check_uploads.order("id DESC").first
  end

  def last_check_upload_received_at(format = "%m/%d/%Y %H:%M%p")
    self.pos_check_uploads.order("id DESC").first.created_at.strftime(format) rescue nil
  end

  def last_menu_upload_received
    self.pos_menu_uploads.order("id DESC").first
  end

  def last_menu_upload_received_at(format = "%m/%d/%Y %H:%M%p")
    self.pos_menu_uploads.order("id DESC").first.created_at.strftime(format) rescue nil
  end

  def copy_the_detail_of_pos_location
    chain = self.chain
    pos_location_reference = nil
    chain.pos_locations.each do |pos_location|
      if pos_location.pos_menu_items.count > 0
        pos_location_reference = pos_location
        break
      end
    end
    Delayed::Job.enqueue(PosMenuItemCopyJob.new(self, pos_location_reference)) if pos_location_reference
  end

end