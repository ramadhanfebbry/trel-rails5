class UserSignupJob < Struct.new(:chain, :resource, :params)

  def perform
    #execute real time sync fishbowl
    real_type_sync = chain.fishbowl_setting.real_time_sync rescue false
    user_fb_direct_sync = chain.add_to_fishbowl_user(resource, real_type_sync) if chain.fishbowl_setting

    #set relevant template setting default
    if resource.register_device_type.eql?("browser") || resource.register_device_type.eql?("browser_compatible")
      email_template = "browser_signup_welcome"
    else
      email_template = "device_signup_welcome"
    end

    notif_setting = chain.notification_settings.where(email_template: "welcome").first rescue nil
    if notif_setting.blank? || ((notif_setting && notif_setting.send_type.eql?(1)) || (notif_setting && notif_setting.send_type.eql?(3)))
      if(Setting.email.turned_on && !resource.php_user)
        p user_fb_direct_sync
        if user_fb_direct_sync && user_fb_direct_sync.status == UserFishbowl::STATUS["SYNCED"] && chain.fishbowl_setting && chain.fishbowl_setting.welcome_email_mailing_id
          p "send welcome email on fishbowl mailing here----"
          fishbowl_email_sent = chain.send_welcome_email(resource)
          unless fishbowl_email_sent
            resource.send_welcome_email(true, false, email_template)
          end
        else
          resource.send_welcome_email(true, false, email_template)
        end
      end
    # elsif notif_setting.send_type.eql?(2)
    #   MandrillNotification.send_welcome(resource, notif_setting)
    #   # resource.send_welcome_email_using_mandrill(notif_setting)
    # elsif notif_setting.send_type.eql?(4)
    #   BrontoNotification.send_welcome(resource, notif_setting, email_template)
    # elsif notif_setting.send_type.eql?(5)
    #   EmmaNotification.send_welcome(resource, notif_setting)
    end

    p "---php user ----"
    p resource.php_user

    #update device_token and sign_in device type when user successfully login
    set_device_token_as_unique_for_user(resource, (params[:sign_in_device_type] || params[:register_device_type]), params[:device_token], params[:device_id])
    resource.update_phone_info(params[:phone_model], params[:os], params[:appkey])

    #user device stuff
    #check first what device type used
    device_type_selected = (params[:sign_in_device_type] || params[:register_device_type])
    if device_type_selected.downcase.eql?("android")
      key_checked = params[:android_id]
    elsif device_type_selected.downcase.eql?("iphone")
      key_checked = params[:keychain]
    end
    p "device detected ---"
    p device_type_selected
    p key_checked
    unique_signed_up_user = UserDeviceLog.check_unique_signup_user(resource.chain_id, key_checked, device_type_selected, params[:user_type])
    p "per uniq uniq kan"
    p unique_signed_up_user
    p resource.chain.required_unique_device_info

    log_description = if params[:user_type].to_s == "onosys"
                        "Onosys Signup"
                      elsif params[:user_type].to_s == "olo"
                        "Olo Signup"
                      else
                        "Signup"
                      end
    UserDeviceLog.add_log(resource, params[:android_id], params[:keychain], (params[:sign_in_device_type] || params[:register_device_type]).downcase.tr(' ', '_'), log_description, Time.current)

    #track the device info
    if key_checked.blank?
      resource.update_columns(:signup_device_status => 3, :selected_device_info => key_checked)
    elsif unique_signed_up_user
      resource.update_columns(:signup_device_status => 1, :selected_device_info => key_checked)
    else
      resource.update_columns(:signup_device_status => 2, :selected_device_info => key_checked)
    end

    # Perform SIGN_UP reward logic
    if (resource.chain.required_unique_device_info && unique_signed_up_user) || !resource.chain.required_unique_device_info
      p "get reward as a uniq user signed up"
      Reward.push_reward(ChainRewardEvent::EVENTS["SIGN_UP"], chain, resource)
      if resource.chain.require_point_signup_incentive
        p "get signup point incentive as unique user"
        resource.earn(resource.chain.point_signup_incentive)
        PointHistory.add_to_history(resource.id, "Signup", resource.chain.point_signup_incentive)
      end
    end

    if unique_signed_up_user || (resource.reload.register_device_type.try(:downcase).eql?("browser") || resource.reload.register_device_type.try(:downcase).eql?("browser_compatible"))
      #push birthday reward if user created at  == user birthday
      p "birthday reward----"*10
      birthday_annualy_reward = BirthdayAnnualyReward.new(resource.chain_id)
      birthday_annualy_reward.push_imadiately_to_user_since_signup_and_dob_same(resource)

      anniversary_annualy_reward = AnniversaryAnnualyReward.new(resource.chain_id)
      anniversary_annualy_reward.push_imadiately_to_user_since_signup_and_anniversary_same(resource)
    end

    #perform push referall code
    Receipt.push_referral_code(resource,chain) unless params[:referral_code].blank?

    #perform fbsignup incentive
    if resource.chain.fb_signup_incentive && params[:register_type].eql?("2")
      p "get fb signup point incentive -----"
      resource.earn(resource.chain.fb_signup_incentive_point)
      PointHistory.add_to_history(resource.id, "FB sign up incentive", resource.chain.fb_signup_incentive_point)
    end
  end

  private
  def set_device_token_as_unique_for_user(user, device_type, device_token, device_id)
    p "*******************DEVICE ID**********************************"
    p "device id is #{device_id}"
    p "device type is #{device_type}"
    p "device token is #{device_token}"

    p "*************************************************************"
    User.transaction do
      if device_type.downcase.eql?("android")
        if !device_id.blank? && !device_token.blank?
          users = User.where(:chain_id => user.chain_id, :device_id => device_id, :device_token => device_token, :sign_in_device_type => device_type)
        else
          users = []
        end
      elsif device_type.downcase.eql?("iphone")
        unless device_token.blank?
          users = User.where(:chain_id => user.chain_id, :device_token => device_token)
        else
          users = []
        end
      end
      users.each do |u|
        u.update_columns(device_token: nil, sign_in_device_type: nil) unless u.eql?(user)
      end unless users.blank?

      user.update_columns(
          sign_in_device_type: device_type,
          device_token: device_token,
          device_id: device_type.downcase.eql?("android") ? device_id : nil
      )

    end
  end

end