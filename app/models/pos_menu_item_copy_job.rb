class PosMenuItemCopyJob < Struct.new(:pos_location, :pos_location_reference)

  def perform
    count = pos_location_reference.pos_menu_items.count
    loop = (count.to_f / 500.to_f).ceil
    1.upto(loop) do |page|
      p "loop ke #{page}"
      pos_menu_items = pos_location_reference.pos_menu_items.paginate(page: page, per_page: 500)
      pos_menu_items.each do |pos_menu_item|
        existing_menu_item = PosMenuItem.where(:item_number => pos_menu_item.item_number, :chain_id => pos_location.chain_id, :pos_location_id => pos_location.id).first
        unless existing_menu_item
          pos_location.pos_menu_items.create(
              :item_name => pos_menu_item.item_name,
              :item_number => pos_menu_item.item_number,
              :description => pos_menu_item.description,
              :chain_id => pos_location.chain_id,
              :general_menu_item_id => pos_menu_item.general_menu_item_id
          )
        else
          existing_menu_item.update_attributes(
              :item_name => pos_menu_item.item_name,
              :item_number => pos_menu_item.item_number,
              :description => pos_menu_item.description,
              :chain_id => pos_location.chain_id,
              :general_menu_item_id => pos_menu_item.general_menu_item_id
          )
        end
      end
    end
  end
end