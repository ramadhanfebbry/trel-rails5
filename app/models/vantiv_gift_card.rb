class VantivGiftCard < ActiveRecord::Base
  belongs_to :chain

  scope :available, -> { where(:used => false).limit(100).order("RANDOM()") }
end
