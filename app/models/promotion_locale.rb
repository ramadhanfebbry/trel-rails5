class PromotionLocale < ActiveRecord::Base

  belongs_to :chain
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
  
end
