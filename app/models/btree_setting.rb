class BtreeSetting < ActiveRecord::Base

  belongs_to :chain

  validates :environment, :merchant_id, :public_key, :private_key, :presence => true


  def set_env_setting
    Braintree::Configuration.environment = self.environment.to_sym
    Braintree::Configuration.merchant_id = self.merchant_id
    Braintree::Configuration.public_key = self.public_key
    Braintree::Configuration.private_key = self.private_key
  end

  def self.create_new_customer_from_mobile_pay(apikey, user, paycode)
    begin
      braintree_url = ENV["BRAINTREE_URL"] || "https://fundy.relevantmobile.com:8453/mobilepay/api/v1/"
      btree_path = "user?apikey=#{apikey}&userid=#{user.id}&paycode=#{paycode}"
      uri = URI.parse(braintree_url+btree_path)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new(uri.request_uri)
      response = http.request(request)
      json_response = JSON.parse(response.body)
      if json_response
        [json_response["status"], json_response["message"]]
      else
        [false, "Something went wrong when creating new braintree customer"]
      end
    rescue => e
     [false, e.message]
    end

  end

  def create_new_braintree_customer(user)
    begin
      result = Braintree::Customer.create(
          :id => user.id,
          :first_name => user.first_name,
          :last_name => user.last_name,
          :email => user.email
      )
      if result.success?
        [true, "Created customer #{result.customer.id}"]
      else
        messages = []
        puts "Validations failed"
        result.errors.for(:customer).each do |error|
          messages <<  error.message
        end
        [true, messages.join(" and ")]
      end
    rescue => e
      [false, e.message]
    end

  end

  def self.update_braintree_customer_id_with_paycode(user, paycode)
    begin
      result = Braintree::Customer.update(
          user.braintree_identifier, # id of customer to update
          :id => paycode
      )
      if result.success?
        [true, "updated customer #{result.customer.id}"]
      else
        messages = []
        puts "Validations failed"
        result.errors.for(:customer).each do |error|
          messages <<  error.message
        end
        [true, messages.join(" and ")]
      end
    rescue => e
      [false, e.message]
    end

  end

  def self.delete_old_credit_card(user)
    if user.braintree_identifier.blank?
      return {:status => false, :message => "Delete error, User is not registered on Braintree."}
    else
      customer = Braintree::Customer.find(user.braintree_identifier) rescue nil
      if customer.blank?
        return {:status => false, :message => "Delete error, Could not find user in Braintree"}
      else
        payment_methods = customer.payment_methods rescue []
        if payment_methods.blank?
          return {:status => false, :message => "Delete error, User does not have any payment methods registered."}
        else
          return {:status => false, :message => "Delete error, User only has one payment method registered."} if payment_methods.size == 1
          payment_methods = payment_methods.sort{|a,b| a.created_at <=> b.created_at}
          old_payment_method = payment_methods[0]
          success = Braintree::PaymentMethod.delete(old_payment_method.token)
          if success
            return {:status => true, :message => "Delete success, Old Payment Method has been successfully deleted."}
          else
            return {:status => false, :message => "Delete error"}
          end
        end

      end

    end
  end

end