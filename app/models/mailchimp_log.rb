class MailchimpLog < ActiveRecord::Base

  serialize :success_users
  serialize :failed_users

  belongs_to :chain

end