class EmmaMarketing < ActiveRecord::Base
  belongs_to :chain
  belongs_to :user
  belongs_to :restaurant

  EMMASTATUS = {
      "NEW"  => 1,
      "CHANGED" => 2,
      "FAILED_SYNC" => 3,
      "SYNCED" => 4,
      "REPEATED" => 5,
      "MARK OPT OUT" => 6
  }

  def self.import_from_users
    chains = Chain.joins(:emma_setting)
    chains.each do |chain|
      import_to_emma_marketing(chain)
    end
  end

  def emma_add_sync_job(run_type = "Sync Now", execute_time = nil)
    if self.emma_setting
      fl = EmmaLog.create(:chain_id => self.id, :log_type => run_type, :status => "in queue")
      if execute_time
        dl = Delayed::Job.enqueue(EmmaJob.new(self, fl), :queue => 'synch_marketing_job', :run_at => execute_time)
      else
        dl = Delayed::Job.enqueue(EmmaJob.new(self, fl), :queue => 'synch_marketing_job')
      end
    end
  end

  def self.import_to_emma_marketing(chain)
    total_pages = chain.users.select("id").paginate(:page => 1, :per_page => 1000).total_pages

    (1..total_pages).each do |page|
      users = chain.users.select("email, id, chain_id, marketing_optin, created_at").paginate(:page => page, :per_page => 1000)
      results = []
      users.each do |user|
        rs = user.restaurant_users.order("created_at DESC").first
        em = EmmaMarketing.where(:chain_id =>  user.chain_id, :user_id => user.id).first rescue nil
        if em.present?
          if (em.email != user.email && !em.member_identifier.blank?) || em.status == EmmaMarketing::EMMASTATUS["REPEATED"]
            em.update_column(:status, EmmaMarketing::EMMASTATUS["CHANGED"])
            em.update_column(:email, user.email)
          end
        else
          status = user.marketing_optin ? EmmaMarketing::EMMASTATUS["NEW"] : EmmaMarketing::EMMASTATUS["MARK OPT OUT"]
          new_user = {chain_id: user.chain_id, user_id: user.id, email: user.email, status: status, restaurant_name: rs.try(:restaurant_id), signup_date: user.try(:created_at)}
          results << new_user
        end
      end

      self.insert_users(results) unless results.blank?
    end
  end

  def self.insert_users(results)
    p "============================ Insert or updates processing ============================"
    a = Time.now
    users = []
    results.each do |res|
      users << EmmaMarketing.new(:chain_id => res[:chain_id], :user_id => res[:user_id], :email => res[:email], :status => res[:status], :restaurant_id => res[:restaurant_name], :signup_date => res[:signup_date])
    end
    EmmaMarketing.import(users) unless users.blank?
    puts "inserting or updating users finish in #{Time.now - a}"
  end

  def self.import_user_to_emma_marketing(user)
    rs = user.restaurant_users.order("created_at DESC").first
    em = EmmaMarketing.where(:chain_id =>  user.chain_id, :email => user.email).first
    if em
      if (em.email != user.email && !em.member_identifier.blank?) || em.status == EmmaMarketing::EMMASTATUS["REPEATED"]
        em.update_column(:status, EmmaMarketing::EMMASTATUS["CHANGED"])
        em.update_column(:email, user.email)
      end
    else
      p status = user.marketing_optin ? EmmaMarketing::EMMASTATUS["NEW"] : EmmaMarketing::EMMASTATUS["MARK OPT OUT"]
      em = EmmaMarketing.new(:chain_id => user.chain_id, :user_id => user.id, :email => user.email, :status => status, :restaurant_id => (rs.restaurant_id rescue nil), :signup_date => (user.created_at rescue nil))
      em.save
    end
  end

  def self.adding_process(chain, emma_log)
    p "adding_process #{chain.try(:name)}"
    import_to_emma_marketing(chain)
    default_emma_marketing_status = [EmmaMarketing::EMMASTATUS["NEW"], EmmaMarketing::EMMASTATUS["CHANGED"], EmmaMarketing::EMMASTATUS["FAILED_SYNC"]]
    default_emma_marketing_status << EmmaMarketing::EMMASTATUS["MARK OPT OUT"] if chain.emma_ignore_marketing_optin

    if chain.emma_ignore_marketing_optin
      query = "emma_marketings.status in (#{default_emma_marketing_status.join(",")}) AND emma_marketings.chain_id = #{chain.id}"
    else
      query = "users.marketing_optin IS TRUE AND emma_marketings.status in (#{default_emma_marketing_status.join(",")}) AND emma_marketings.chain_id = #{chain.id}"
    end
    #total_pages = EmmaMarketing.includes(:user).where(query).order("user_id DESC").paginate(:page => 1, :per_page => 50).total_pages
    total_pages = EmmaMarketing.includes(:user).where(query).count
    synch_job_type = "EmmaMarketing"
    unless total_pages.blank?
      loop_page = 0
      (1..total_pages).to_a.in_groups_of(1000, false) do |pages|
        p "looping page: #{loop_page = loop_page + 1} ====== total page of:#{total_pages / 1000} ====="
        Delayed::Job.enqueue(LoopingUserJob.new(chain, emma_log, pages, query, synch_job_type), :queue => 'synch_marketing_job')
      end
    end
  end

  def self.adding_process_import(chain, emma_log, ems)
    require 'emma'
    emma_setting = chain.emma_setting
    account_id = emma_setting.account_id
    public_key = emma_setting.public_api_key
    private_key = emma_setting.private_api_key
    group_id = emma_setting.group_id
    emma = Emma::Setup.new account_id, public_key, private_key, 'debug_true_or_false'

    success_users = []
    failed_users = []

    p "ems ----------------------------------: #{ems}"
    ems.each do |em|
      restaurant_name = em.user.favorite_restaurant.try(:name) || "-" rescue "-"
      if em.status == EmmaMarketing::EMMASTATUS["CHANGED"]
        unless em.member_identifier.blank?
          emma.update_member(em.member_identifier, email: em.email, fields: {first_name: em.user.first_name, last_name: em.user.last_name, loyaltypoints_rm: em.user.points.to_s, signupdate_rm:  em.user.created_at, favstore_rm: restaurant_name})
          p "update member success on update"
          success_users << em.user_id
        else
          req = emma.add_member(email: em.email, fields: {first_name: em.user.first_name, last_name: em.user.last_name, loyaltypoints_rm: em.user.points.to_s, signupdate_rm:  em.user.created_at, favstore_rm: restaurant_name }) rescue nil
          if req.nil?
            em.update_attributes(:status => EmmaMarketing::EMMASTATUS["FAILED_SYNC"], :member_identifier => nil) unless em.blank?
            failed_users << em.user_id
          elsif req["added"] == false
            new_req = emma.get_member_by_email(em.email) rescue nil
            if new_req.nil?
              em.update_attributes(:status => EmmaMarketing::EMMASTATUS["FAILED_SYNC"], :member_identifier => nil) unless em.blank?
              failed_users << em.user_id
            elsif new_req["member_id"].present?
              p "adding member success on update"
              em.update_attributes(:status => EmmaMarketing::EMMASTATUS["SYNCED"], :member_identifier => new_req["member_id"].to_s)
              success_users << em.user_id
            else
              p "creating member failed on update"
              em.update_attributes(:status => EmmaMarketing::EMMASTATUS["FAILED_SYNC"], :member_identifier => nil) unless em.blank?
              failed_users << em.user_id
            end
          else
            p "creating member success on update"
            em.update_attributes(:status => EmmaMarketing::EMMASTATUS["SYNCED"], :member_identifier => req["member_id"].to_s)
            success_users << em.user_id
          end
        end
      else
        new_req = emma.add_member(email: em.email, fields:
            {first_name: em.try(:user).try(:first_name),
             last_name: em.try(:user).try(:last_name),
             loyaltypoints_rm: em.try(:user).try(:points).to_s,
             signupdate_rm:  em.try(:user).try(:created_at).to_s,
             favstore_rm: restaurant_name
            })
        emma.add_member_to_groups(new_req["member_id"].to_s, :group_ids => [group_id])

        if new_req.nil?
          em.update_attributes(:status => EmmaMarketing::EMMASTATUS["FAILED_SYNC"], :member_identifier => new_req["member_id"].to_s) unless em.blank?
          failed_users << em.user_id
        elsif new_req["member_id"].present?
          p "adding member success"
          em.update_attributes(:status => EmmaMarketing::EMMASTATUS["SYNCED"], :member_identifier => new_req["member_id"].to_s) unless em.blank?
          success_users << em.user_id
        else
          p "creating member failed"
          em.update_attributes(:status => EmmaMarketing::EMMASTATUS["FAILED_SYNC"], :member_identifier => new_req["member_id"].to_s) unless em.blank?
          failed_users << em.user_id
        end
      end
    end

    emma_log_success_users = emma_log.success_users || []
    emma_log_failed_users = emma_log.failed_users || []

    emma_log.update_attributes(
        :success_users => (success_users + emma_log_success_users).uniq,
        :failed_users => (failed_users + emma_log_failed_users).uniq,
        :status => "completed",
        :completed_at => Time.now
    )
  end

  def status_in_color
    color = case self.status
              when 1
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:blue'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[1]}"
              when 2
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:yellow'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[2]}"
              when 3
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:red'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[3]}"
              when 4
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:green'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[4]}"
              when 5
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:purple'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[5]}"
              when 6
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:black'></div> &nbsp;&nbsp;&nbsp;#{UserFishbowl::STATUS.invert[6]}"
            end
  end

end
