class Steppingstone < ActiveRecord::Base

  has_many :steppingstone_steps, :as => :stepable, :dependent => :destroy

  attr_accessor :receipt_transaction
  
  accepts_nested_attributes_for :steppingstone_steps

  validates :step_type, :presence => true, :uniqueness => { :scope => :chain_id, :case_sensitive => false } ,
            :if => Proc.new{|f| f.step_type != "points_earned"}

  SPEND_LEVEL = {"ALL USER" =>  1, "Low" =>  2, "Medium" => 3 ,"High" =>  4}

  validates :min_amount , :numericality =>  {:greater_than => 0},:if => Proc.new{|f| f.category_type.to_i != 1}
  validates :max_amount , :numericality =>  {:greater_than => 0},:if => Proc.new{|f| f.category_type.to_i != 1}

  validate :amount_validation ,:if => Proc.new{|f| f.category_type.to_i != 1}
  validate :points_earned_valid, :on => :create
  after_save :set_min_max


  def points_earned_valid
    if Steppingstone.where(:chain_id => self.chain_id, :step_type => "points_earned").count >= 4
      errors.add(:step_type, "Points Earned Only Have max 4")
    end
  end

  def set_min_max
    if self.category_type == 1 || self.category_type.blank?
      self.update_column(:min_amount, nil)
      self.update_column(:max_amount, nil)
    end
  end

  def receipt_approved_process
    p "----------------- start receipt approved process here----------------"
    receipt = receipt_transaction.receipt
    user = receipt.user
    p "user is #{user}"
    receipt_transactions_approved = receipt.receipt_transactions.where(:status => Receipt::STATUS[:APPROVED])
    if receipt_transactions_approved.size.eql?(1)
      user_step = UserStep.find_or_create_by_user_id_and_steppingstone_id(user.id, self.id)
      step = self.steppingstone_steps.where("step_number > ?", user_step.current_step).order("step_number ASC").first
      p " user step is : "
      p user_step
      p "step is : "
      p step
      if step.blank?
        if self.resetable
          user_step.update_attributes(:current_step => 0, :current_count => 0)
          step = self.steppingstone_steps.where("step_number > ?", user_step.current_step).order("step_number ASC").first
          receipt_approved_reward(user, receipt_transaction, step, user_step) if step
        end
      else
        receipt_approved_reward(user, receipt_transaction, step, user_step)
      end
    end
  end

  def  amount_validation
    if min_amount > max_amount
      errors.add(:min_amount, "Min Amount Cannot greater than max amount")
    end
  end

  def receipt_approved_reward(user, receipt_transaction, step, user_step)
    reward = step.reward
    # q = "select count(distinct(c.id)) from receipts c left join receipt_transactions t1 on t1.receipt_id = c.id left join
    #      receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id where  c.user_id = #{user.id} and c.status = 3"
    # res = Receipt.find_by_sql(q)
    # user_receipt_transaction_approved = res.first.count.to_i
    user_receipt_transaction_approved = ReceiptTransaction.includes(:receipt).where("receipts.user_id = ? AND receipt_transactions.status = ?", user.id, Receipt::STATUS[:APPROVED]).count

    p "milestone receipt add reward or reward because approved receipt ----"
    p user_receipt_transaction_approved
#    p "user receipt trans approved : #{user_receipt_transaction_approved}"
#    p "count step receipt approved : #{step.property_one}"
#    p "minimum subtotal : #{step.property_two}"
#    p "transction subtotal : #{receipt_transaction.subtotal.to_f}"
    if step.property_one <= user_receipt_transaction_approved && step.property_two <= receipt_transaction.subtotal.to_f
      user_step.increment!(:current_count, 1)
      user_step.update_attribute(:current_step, step.step_number)
      if reward
        RewardWallet.create_with_delay({:user_id => user.id, :reward_id => reward.id})
      elsif step.point
        p "add point ----"
        user.earn(step.point)
        PointHistory.add_to_history(user.id, "Milestone Incentive", step.point)
      end  
    elsif step.property_two > receipt_transaction.subtotal.to_f 
      return unless user.active
      #p "receipt subtotal less than subtotal step"
      milestone_notif = user.chain.milestone_notifications.where(:locale_id => user.locale_id).first
      #p milestone_notif
      return if milestone_notif.blank?
      if !reward.blank?
        mail = setup_mail(user.email, Setting.email.default_from,
        milestone_notif.email_subject % {:receipt_id => receipt_transaction.receipt_id},
        (milestone_notif.email_content % {
              :reward_name => reward.name,
              :min_subtotal => sprintf( "%0.02f", step.property_two),
              :reward_fine_print => reward.fineprint,
              :points_incentive_set => (step.point rescue "-")
            }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />"),
           "multipart/alternative", false) unless milestone_notif.blank?
        mail.deliver!
        p '----------send email--------------'
      elsif step.point
        mail = setup_mail(user.email, Setting.email.default_from,
        milestone_notif.email_subject % {:receipt_id => receipt_transaction.receipt_id},
        (milestone_notif.email_content % {
              :reward_name => "-",
              :min_subtotal => sprintf( "%0.02f", step.property_two),
              :reward_fine_print => "-",
              :points_incentive_set => (step.point rescue "-")
            }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />"),
           "multipart/alternative", false) unless milestone_notif.blank?
        mail.deliver!
        p '----------send email--------------'
      end  
    end
  end


   def setup_mail(user_email, from, subject, body, content_type, delay)
    mail = if delay
             Mail.new do #TODO : looking for the way how to delay send email with mail gem
               to user_email
               from from
               subject subject
             end
           else
             Mail.new do
               to user_email
               from from
               subject subject
             end
           end

    if content_type.eql?("multipart/alternative") or content_type.eql?("text/plain")
      text_part = Mail::Part.new do
        body body
      end
      mail.text_part = text_part
    end

    if content_type.eql?("multipart/alternative") or content_type.eql?("text/html")
      html_part = Mail::Part.new do
        content_type 'text/html'
        body body
      end
      mail.html_part = html_part
    end
    mail
  end

end
