class ChainObserver < ActiveRecord::Observer
  observe Chain

  def after_create(record)
    if record.mailchimp_sync && record.user_sync_required
      Delayed::Job.enqueue(ChainEmailMarketingJob.new(record))
    end
    if record.fishbowl_setting && record.user_sync_required
      Delayed::Job.enqueue(ChainFishbowlUserJob.new(record, "add"))
    end
#    ChainMailer.welcome_email(record).deliver
  end

  def after_update(record)
    if record.mailchimp_sync && record.user_sync_required
      Delayed::Job.enqueue(ChainEmailMarketingJob.new(record))
    end

    if record.fishbowl_setting && record.user_sync_required
      Delayed::Job.enqueue(ChainFishbowlUserJob.new(record, "add"))
    end

  end

end
