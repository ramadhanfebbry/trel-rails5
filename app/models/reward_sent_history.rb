class RewardSentHistory < ActiveRecord::Base
  belongs_to :reward
  belongs_to :chain
  belongs_to :user
end
