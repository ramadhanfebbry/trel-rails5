class PartnerSubCategory < ActiveRecord::Base

  belongs_to :chain
  belongs_to :partner_category

  has_many :restaurant_categories
  has_many :restaurants, :through => :restaurant_categories

  has_many :partner_sub_category_menu_items

  validates :chain_id, :partner_category_id, :name, presence: true

  def title
    self.name
  end

  def pullout(restaurants)
    self.restaurant_categories.where("restaurant_id in (?)", restaurants).delete_all
  end

  def participate(restaurants)
    p "participate -- PartnerSubCategory"
    restaurants.each do |restaurant_id|
      existing_deleted_restaurant_category = RestaurantCategory.unscoped.where("partner_sub_category_id = ? AND restaurant_id = ? ", self.id, restaurant_id).first
      if existing_deleted_restaurant_category.blank?
        existing_restaurant_category = RestaurantCategory.where("partner_sub_category_id = ? AND restaurant_id = ?", self.id, restaurant_id).first
        RestaurantCategory.create(:partner_sub_category_id => self.id, :restaurant_id => restaurant_id) if existing_restaurant_category.blank?
      else
        existing_deleted_restaurant_category.revive
      end
    end
  end

end