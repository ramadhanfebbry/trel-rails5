class UserDealSurvey < ActiveRecord::Base
  belongs_to :user
  belongs_to :deal
  belongs_to :surveys_user, :foreign_key => "survey_user_id"

  def user_latitude
    self.latitude
  end

  def user_longitude
    self.longitude
  end
end
