class PushServicePoint < ActiveRecord::Base
  # serialize  :receiver_user_ids, Array

  belongs_to :chain
  belongs_to :admin
  attr_accessor :xls, :xls_path, :select_type, :time_zone_default, :job_summary, :temp_receiver_user_ids
  # attr_accessible :xls, :xls_path, :select_type, :time_zone_default, :job_summary, :temp_receiver_user_ids,
                  # :chain_id, :date_text_ttd, :admin_id, :recipient_count, :receiver_user_ids, :sent, :sent_at,
                  # :owner_email, :is_all_user, :pentaho_promo_name, :pentaho_promo_id, :pentaho_schedule_id,
                  # :schedule_id, :created_from

end
