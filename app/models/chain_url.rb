class ChainUrl < ActiveRecord::Base
  belongs_to :chain

  validates :name, :presence => true, :uniqueness => {:scope => :chain_id, :case_sensitive => false}
  validates :url, :presence => true, :format => /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix

  before_validation :set_slug_column
  validate :slug_must_be_uniq

  def set_slug_column
    self.slug = self.name.parameterize(separator: "-")
  end

  def generated_url(host)
    "#{host}url/#{self.chain.id}/#{self.slug}"
  end

  def slug_must_be_uniq
    unless chain.chain_urls.where(:slug => self.slug).blank?
      self.errors.add(:name, "Should be uniq, please try another name")
    end
  end
end

