class GameJob < Struct.new(:user, :point_earned)

  include PushNotificationData

  def perform
    puts "start game job"
    chain = user.chain

    emails_and_device_tokens_group = PointNotification.group_emails_and_device_token_by_locale([user.id])
    notification_by_locale = group_message_pn_by_locale(chain)

    if chain.game_point_notif_send_to_device
      p "game point send to device"
      gcm_push(chain, emails_and_device_tokens_group, notification_by_locale, point_earned)
      push_to_iphone(chain, emails_and_device_tokens_group, notification_by_locale, point_earned)
    end

    if chain.game_point_notif_send_to_email
      p "game point send to email"
      push_email_group(chain, emails_and_device_tokens_group, notification_by_locale, point_earned)
    end
    puts "Completed game job"
  end

  def group_message_pn_by_locale(chain)
    hash_map = {}
    chain.locales.each do |locale|
      notification = chain.game_point_notifications.where("locale_id = ?", locale.id).first
      hash_map[locale.id] = {} if hash_map[locale.id].blank?
      hash_map[locale.id]["email_subject"] = notification.blank? ? "" : notification.email_subject
      hash_map[locale.id]["email_content"] = notification.blank? ? "" : notification.email_content
      hash_map[locale.id]["email_content_html"] = notification.blank? ? "" : notification.email_content_html
      hash_map[locale.id]["notification"] = notification.blank? ? "" : notification.notification

    end
    return hash_map
  end

  def gcm_push(chain, reg_ids_by_locale, hash_notification, point)
    p reg_ids_by_locale
    p hash_notification
    reg_ids_by_locale.each do |key, value|
      unless value[:android_device_tokens].blank?
        puts "---Started sending PN---"
        p key
        p value
        p hash_notification[key.to_i]["notification"]
        msg = key.blank? ? "" : hash_notification[key.to_i]["notification"]
        push_to_android(chain, value[:android_device_tokens], (msg % {
          :point_earned => point
        }))
        puts "---SENT---"
      end
    end
  end

  def push_to_android(chain, reg_ids, notification)
    if application = chain.applications.by_device_type(1).first
      return if application.key.blank?
      message = GcmHelper::Message.new
      message.delay_while_idle = true
      message.add_data('alert', notification)
      message.add_data('timestamp', "#{Time.now}")
      #      key="AIzaSyCHAQW9i1vGKNUD83jSsSn8Jfgb2TPFGKc"
      sender = GcmHelper::Sender.new(application.key)
      response = sender.multicast_with_retry(message, reg_ids, 3)
    end
  end

  def push_to_iphone(chain, reg_ids_by_locale, hash_notification, point)
    if application = chain.applications.by_device_type(2).first
      reg_ids_by_locale.each do |key, value|
        unless value[:iphone_device_tokens].blank?
          msg = key.blank? ? "" : hash_notification[key]["notification"]

          value[:iphone_device_tokens].each do |device_token|
            rapns_app = application.rapns_app
            return if rapns_app.blank?
            n = Rpush::Apns::Notification.new
            n.app = rapns_app
            n.device_token = device_token
            n.alert = msg % {
              :point_earned => point
            }

            n.sound = "1.aiff"
            n.badge = 1
            n.save
          end
        end
      end
    end
  end

  def push_email_group(chain, reg_ids_by_locale, hash_notification, point)
    reg_ids_by_locale.each do |key, value|
      unless value[:emails].blank?
        email_subject = key.blank? ? "" : hash_notification[key]["email_subject"]
        email_content = key.blank? ? "" : hash_notification[key]["email_content"]
        email_content_html = key.blank? ? "" : hash_notification[key]["email_content_html"]
        value[:emails].each do |email|
          GameMailer.point_notification(chain, email, (email_subject % {
              :point_earned => point
          }),(email_content % { :point_earned => point }), (email_content_html % { :point_earned => point }) ).deliver
        end
      end
    end
  end


end