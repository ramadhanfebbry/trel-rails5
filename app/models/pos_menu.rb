require 'nokogiri'

class PosMenu < ActiveRecord::Base

  belongs_to :pos_location
  belongs_to :chain
  has_many :pos_menu_items

  def extract!
    xml_in_nokogiri_format = Nokogiri::XML(self.menu_data)
    xml_in_nokogiri_format.xpath('//menu-item').each do |menu_item|
      size = menu_item.xpath("size").text rescue nil
      existing_menu_item = PosMenuItem.where(:item_number => menu_item.attr("id"), :chain_id => self.chain_id, :pos_location_id => self.pos_location.id, :level_size_id => size).first
      unless existing_menu_item
        self.pos_menu_items.create(
            :item_name => menu_item.xpath("name").text,
            :item_number => menu_item.attr("id"),
            :pos_location_id => self.pos_location.id,
            :chain_id => self.chain_id,
            :level_size_id => size
        )
      else
        existing_menu_item.update_attributes(
            :item_name => menu_item.xpath("name").text,
            :item_number => menu_item.attr("id"),
            :pos_location_id => self.pos_location.id,
            :pos_menu_id => self.id,
            :level_size_id => size
        )
      end
    end

  end

  def extract_olo!
    xml_in_nokogiri_format = Nokogiri::XML(self.menu_data)
    xml_in_nokogiri_format.xpath('//menu-item').each do |menu_item|
      size = menu_item.xpath("size").text rescue nil
      existing_menu_item = PosMenuItem.where(:item_number_olo => menu_item.attr("id"), :chain_id => self.chain_id, :pos_location_id => self.pos_location.id, :level_size_id => size).first
      unless existing_menu_item
        self.pos_menu_items.create(
            :item_name => menu_item.xpath("name").text,
            :item_number => nil,
            :pos_location_id => self.pos_location.id,
            :chain_id => self.chain_id,
            :level_size_id => size,
            :item_number_olo =>  menu_item.attr("id")
        )
      else
        existing_menu_item.update_attributes(
            :item_name => menu_item.xpath("name").text,
            :item_number => nil,
            :pos_location_id => self.pos_location.id,
            :pos_menu_id => self.id,
            :level_size_id => size,
            :item_number_olo =>  menu_item.attr("id")
        )
      end
    end
  end
end