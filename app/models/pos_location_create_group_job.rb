class PosLocationCreateGroupJob < Struct.new(:restaurant_ids, :menu_upload_status, :check_status)

  def perform
    restaurant_ids.each do |restaurant_id|
      restaurant = Restaurant.find(restaurant_id)
      pos_location = PosLocation.new
      pos_location.set_apikey
      pos_location.restaurant_id = restaurant.id
      pos_location.menu_upload_status = true unless menu_upload_status.blank?
      pos_location.check_status = true unless check_status.blank?
      pos_location.chain_id = restaurant.chain_id
      pos_location.save
    end unless restaurant_ids.blank?
  end
end