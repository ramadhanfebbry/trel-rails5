class PushRewardMandrillJob < Struct.new(:reward, :reg_ids_by_locale, :hash_notification, :expired_at, :notif_setting)

  def perform
    p "perform PushRewardMandrillJob "
    tmp_expired = nil
    if expired_at.blank?
      tmp_expired = reward.expiryDate.strftime("%m/%d/%Y")
    else
      tmp_expired = expired_at#.strftime("%d %B %Y 23:59:59")
    end

    reward.push_email_group_using_mandrill(reg_ids_by_locale, hash_notification, tmp_expired, notif_setting)
    puts "Completed sending PN to Mail"
  end

  def error(job, exception)
    reward = job.delayable_type.classify.constantize.find(job.delayable_id)
    error_text = job.last_error
    RewardMailer.send_pn_email_error(reward, error_text, exception).deliver
  end
  
end
