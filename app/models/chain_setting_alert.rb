class ChainSettingAlert < ActiveRecord::Base
  belongs_to :chain
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}

  RECEIPT_TYPES =  {APPROVED: 1, ALT: 2}
  ONLINE_ORDER_TYPES = {:NCR => 1, :OLO => 2, :ONOSYS => 3}

  # ChainSettingAlert.run_on_execution_time
  def self.run_on_execution_time
    #puts self.all.count
    self.all.each do |alert|
      time_in_zone = Time.now.in_time_zone(alert.send_zone)
      time_to_execute = alert.hour_format_date
      hour = time_in_zone.hour
      puts "hour === #{hour}"
      puts "hour alert === #{time_to_execute}"
      if hour == time_to_execute
        # execute the chain setting alert
        Delayed::Job.enqueue(NoScanAlert.new(alert.chain_id))
      end
    end
  end

  def hour_format_date
    hour_start = nil
    hour_end = nil
    unless self.send_time.blank?
      a = self.send_time
      if a.downcase.include?("am")
        if a.to_i == 12
          a = 0
        end
        hour_start = a.to_i
      else
        hour_start = a.to_i + 12
      end
    end

    return hour_start
  end

  def self.is_aloha_program(ch)
    if ch.connect_to_aloha_program == true
      return true
    end
    return false
  end

  def self.receipt_selector(ch)
    return ChainSettingAlert::RECEIPT_TYPES if is_aloha_program(ch) == true
    return {APPROVED: 1}
  end

  def self.online_order_selector(ch)
    res = {:NCR => 1} if is_aloha_program(ch) == true
    res = {} if res.blank?
    res.merge!(:OLO => 2) if is_olo_receipt(ch) == true
    res = {} if res.blank?
    res.merge!(:ONOSYS => 3) if is_onosys_receipt(ch) == true
    res = [] if res.blank?
    return res
  end

  def self.is_olo_receipt(ch)
    r=Receipt.where("chain_id = ? and is_olo IS TRUE",ch.id).first
    return true if !r.blank?
    return false
  end

  def self.is_onosys_receipt(ch)
    if is_aloha_program(ch) == false && Receipt.where("is_online_order is true and chain_id = ?", ch.id).order("id desc").first
      return true
    else
      return false
    end
  end

  def self.is_ncr_online_receipt(ch)
    if is_aloha_program(ch) == false && Receipt.where("is_ncr_online_order_receipt is true and chain_id = ?", ch.id).order("id desc").first
      return true
    else
      return false
    end
  end

  def self.is_mobile_payment(ch)
    r = UserPaymentHistory.joins(:user).where("users.chain_id = ?", ch.id).first
    return false if r.blank?
    return true
  end
end
