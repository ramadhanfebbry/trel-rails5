class Offer < ActiveRecord::Base

  #validates :chain_id, :restaurants, :presence => true
  validates :name, :length => {:maximum=>100}, :presence => true, :format => {:with => /\A[-a-zA-Z0-9 ()-]+\z/,
    :message => "(Only alphanumeric and special(-) characters allowed)"}
  validates_numericality_of :multiplier, :greater_than_or_equal_to=>0, :less_than_or_equal_to => 5, :presence => true,
                            :if => [:is_val_multiplier?, :is_not_having_rule?]

  validates_presence_of :effectiveDate, :expiryDate, :if => :is_not_having_rule?
  validates_presence_of :expiryDate, :format => {:with => /(^\d{2}-\d{2}-\d{4}$)/},  :if => [:validate_expiry_date, :is_not_having_rule?]

  validates_presence_of :timeStart,
    :format => {:with => /^([01]\d:[0-5]\d(AM|PM))?$/, :message => "Incorrect time format for timeStart"} , :if =>  :is_not_having_rule?
  validates_presence_of :timeEnd,
    :format => {:with => /^([01]\d:[0-5]\d(AM|PM))?$/, :message => "Incorrect time format for timeEnd"}, :if =>  :is_not_having_rule?

  validates :constant_value, :numericality => {
      :greater_than_or_equal_to => 0,
      :allow_blank => false
  }, :if => [:is_constant_value?, :is_not_having_rule?]


  #default_scope where("deleted_at" => nil)
  default_scope { where("is_online_order = ?",false) }

  scope :online_offers, -> { where("is_online_order = ?",true) }
  scope :active_chain, -> { joins(:chain).where("chains.status = 'active'") }
  scope :no_rules, -> { where("having_rule IS FALSE") }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end


  belongs_to :chain
  belongs_to :survey
  has_many :restaurant_offers
  has_many :restaurants, :through => :restaurant_offers
  has_one :surveys_user
  has_many :perk_restaurants, :dependent => :destroy
  has_many :perk, :through => :perk_restaurants
  has_many :offer_menu_items
  has_many :general_menu_items, :through => :offer_menu_items
  has_many :offer_rules

  #accepts_nested_attributes_for :offer_rules, :reject_if => lambda { |a| a[:rule_type].blank? }, :allow_destroy => true
  accepts_nested_attributes_for :offer_rules, :allow_destroy => true

  #after_create :update_restaurants
  #
  #def update_restaurants
  #  unless restaurant_ids.nil?
  #    restaurant_ids.each do |r|
  #      self.restaurant_offers.create(:restaurant_id => r) unless r.blank? # Now insert records as selected by user
  #    end
  #  end
  #end

  def validate_expiry_date
    if self.expiryDate && self.effectiveDate
      errors.add( :expiryDate, "must be after the From Date") if self.effectiveDate > self.expiryDate
    end
  end

  def day_of_week=(days)
    dow = 0b0000000
    days.each do |day|
      dow = dow + (1 << day.to_i)
    end
    write_attribute(:daysOfWeek, dow)
  end

  def include_day_of_week(day)
    self.daysOfWeek[day]==1 unless self.daysOfWeek.nil?
  end


  def update_participation(restaurants, old_participants)
    ids = old_participants.split
    intersection = restaurants & ids;
    if (!ids.blank?)
      pullout((ids-intersection))
    end
    if (!restaurants.blank?)
      participate((restaurants-intersection));
    end
  end

  def pullout(restaurants)
    self.restaurant_offers.where("restaurant_id in (?)", restaurants).destroy_all
  end

  def participate(restaurants)
    restaurants.each do |restaurant_id|
      existing_deleted_restaurant_offer = RestaurantOffer.unscoped.where("offer_id = ? AND restaurant_id = ? AND deleted_at IS NOT NULL", self.id, restaurant_id).first
      if existing_deleted_restaurant_offer.blank?
        existing_restaurant_offer = RestaurantOffer.where("offer_id = ? AND restaurant_id = ?", self.id, restaurant_id).first
        RestaurantOffer.create(:offer_id => self.id, :restaurant_id => restaurant_id) if existing_restaurant_offer.blank?
      else
        existing_deleted_restaurant_offer.revive
      end
    end
  end

  def is_val_multiplier?
    self.is_multiplier
  end

  def is_not_having_rule?
    puts self.having_rule

    self.having_rule == false
  end

  def is_constant_value?
    !self.is_multiplier
  end

  def multiplier_constant_value
    if self.is_multiplier
      self.multiplier
    else
      self.constant_value
    end
  end

  def is_zero_subtotal?
    if self.offer_rules.size > 0
      rule_types = self.offer_rules.map(&:rule_type)
      return rule_types.include?(OfferRule::TYPE["MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER, IGNORE SUBTOTAL"])
    else
      return false
    end
  end

  def offer_rules_subtotal_zeros
    self.offer_rules.where("rule_type = ?",OfferRule::TYPE["MENU ITEM BASED - ORDERED ITEM COUNT, MULTIPLIER, IGNORE SUBTOTAL"])
  end


  #def self.set_bonus_points(params)
  #   Rails.logger.debug "oooooooo11#{params}"
  #  if(params[:bonus_points]=='no')
  #    params[:offer]['bonus_points']=0;
  #  end
  #  if(params[:bonus_points_ftu]=='no')
  #    params[:offer]['bonus_points_ftu']=0;
  #  end
  #
  #  return params;
  #end

end
