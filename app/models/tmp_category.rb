class TmpCategory < ActiveRecord::Base
  
  validate :validates_category_ids

  def validates_category_ids
    if self.category_ids.blank?
      self.errors.add(:category, "Please select at least 1 area to focus on.")
    elsif self.category_ids.split(",").size > 3
      self.errors.add(:category, "The maximum number of areas you can choose is 3.")
    end
  end
  
end
