class GcmAndroidJob < Struct.new(:reward, :reg_ids_by_locale, :hash_notification,:exp_at)

  def perform
    p "perfom GCM Android job"
    tmp_expired = nil
    if exp_at.blank?
      #tmp_expired = reward.expiryDate.strftime("% %B %Y 23:59:59") rescue Time.now + 1.weeks
      tmp_expired = reward.expiryDate.strftime("%m/%d/%Y") rescue Time.now + 1.weeks
    else
      tmp_expired = exp_at#.strftime("%d %B %Y 23:59:59")
    end
    reward.gcm_push(reg_ids_by_locale, hash_notification, tmp_expired)
    #    reward.push_to_android_and_email(reg_ids_by_locale, hash_notification)
    puts "Completed sending PN to android"
  end

  def error(job, exception)
    reward = job.delayable_type.classify.constantize.find(job.delayable_id)
    error_text = job.handler
    RewardMailer.send_pn_android_error(reward, error_text, exception).deliver
  end
  
end
