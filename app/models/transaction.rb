class Transaction < ActiveRecord::Base
  belongs_to :reward_transaction
  belongs_to :receipt_transaction
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
end
