class ReportOwner < ActiveRecord::Base
  belongs_to :owner
  belongs_to :report_subscription
end
