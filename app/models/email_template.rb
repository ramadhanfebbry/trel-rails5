class EmailTemplate < ActiveRecord::Base

  belongs_to :chain
  belongs_to :locale

  validates :locale_id, :template_name, :chain_id, :mail_subject, :mail_content_html, :mail_content_text, :presence => true
  validates :template_name, :presence => true, :uniqueness => {:scope => [:chain_id, :locale_id]}

end