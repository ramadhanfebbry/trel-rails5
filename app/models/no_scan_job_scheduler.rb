class NoScanJobScheduler < ActiveRecord::Base
  belongs_to :no_scan_job, :foreign_key => 'job_id'


  scope :run_today, -> { where("date(no_scan_job_schedulers.executed_at)
       = date(now())
      and schedule_type != 1 and
      (no_scan_jobs.status=2 or no_scan_jobs.status=3) and date(now()) >=
      date(start_at)  and
      date(now())  <= date(end_at)").
                      joins(:no_scan_job) }

  scope :run_daily, -> { where("date(no_scan_job_schedulers.executed_at)
       <= date(now())
      and schedule_type != 1 and
      (no_scan_jobs.status=2 or no_scan_jobs.status=3 or no_scan_jobs.status=0) and date(now()) >=
      date(start_at)  and
      date(now())  <= date(end_at)").
                      joins(:no_scan_job) }

  scope :admin_only, -> { where("no_scan_jobs.owner_id IS NULL") }

  def self.set_scheduler(params)
    owner_schedule_build = {}

    case params["no_scan_job"]["schedule_type"].to_i
      when 1 # onte time
        hour = params["daily"]['(4i)'].to_i
        min = params["daily"]['(5i)'].to_i
        time_to_run = Time.zone.now.change(:hour => hour, :min => min)

        owner_schedule_build = {
            :run_type => 'onetime',
            :executed_at => time_to_run,
            :next_run => time_to_run + 1.days,
        }
      when 2 # daily
        hour = params["daily"]['(4i)'].to_i
        min = params["daily"]['(5i)'].to_i
        time_to_run = Time.zone.now.change(:hour => hour, :min => min)

        owner_schedule_build = {
            :run_type => 'daily',
            :executed_at => time_to_run,
            :next_run => time_to_run + 1.days,
        }
      when 3 # weekly
        hour = params["time_stamp_weekly"]['(4i)'].to_i
        min = params["time_stamp_weekly"]['(5i)'].to_i
        day = params["weekly"].to_i
        owner_schedule_build = {
            :run_type => 'weekly',
            :executed_at => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days).change(:hour => hour, :min => min),
            :next_run => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days) + 1.weeks
        }
      when 4 # monthly
        hour = params["time_stamp_montly"]['(4i)'].to_i
        min = params["time_stamp_montly"]['(5i)'].to_i
        day = params["monthly"].to_i

        week_day = Time.zone.now.beginning_of_month.wday
        days_params = {}
        diff = day - week_day
        if day >= week_day and Time.zone.now.beginning_of_month >= Time.zone.now
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + diff.days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + diff.days)..change(:hour => hour, :min => min)
          }

        elsif day < week_day and Time.zone.now.beginning_of_month.to_date < Time.zone.now.to_date
          next_wday = (Time.zone.now.beginning_of_month + 1.months).wday
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + ((7 - week_day) + day).days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + ((7 - next_wday) + day).days).change(:hour => hour, :min => min),
          }
        else
          next_wday = (Time.zone.now.beginning_of_month + 1.months).wday
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + ((7 - week_day) + day).days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + ((7 - next_wday) + day).days).change(:hour => hour, :min => min),
          }
        end

        #start_month =
        owner_schedule_build = {
            :run_type => 'monthly' #,
            # :executed_at => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days).change(:hour => hour, :min => min),
            # :next_run => (Time.zone.now.beginning_of_month.in_time_zone + (30.days)).change(:hour => hour, :min => min),
        }.merge!(days_params)
    end
    os = OwnerScheduler.new(owner_schedule_build.merge!(:start_at => params["start_date"], :end_at => params["end_date"]))
  end

  def monthly_selection
    beginning = Time.zone.now.beginning_of_month
  end

  ## OwnerScheduler.running_owner_scheduler
  def self.running_owner_scheduler
    testing = REDIS.get("testing_owner_job")
    if testing.blank? ## for testing purpose
      running_tasks = NoScanJobScheduler.run_today.admin_only
      running_tasks = running_tasks + NoScanJobScheduler.run_daily.admin_only
    else
      # running_tasks = NoScanScheduler.where("schedule_type != 1 and
      # (owner_jobs.status=2 or owner_jobs.status=3 or owner_jobs.status=0) and date(now()) >=
      # date(start_at)  and
      # date(now())  <= date(end_at) and owner_jobs.owner_id is NULL").
      #     joins(:owner_job)
      running_tasks = NoScanJobScheduler.joins(:no_scan_job).where("no_scan_jobs.owner_id is null and no_scan_jobs.status != 4")
    end
    unless running_tasks.blank?
      aloha_chains = Chain.active.select('id').where(:connect_to_aloha_program => true)

      running_tasks.each do |task|
        begin
          aloha_chain_id = task.no_scan_job.chain.id rescue nil
          # in aloha chain next it# since its for prelevant
          if aloha_chains.map(&:id).include?(aloha_chain_id)
            next
          end
          # handle duplication job
          dj_owner = DelayedJob.where('handler like ? and handler like ?', '%DashboardJob%',"%#{task.job_id}%")
          if dj_owner.size > 1
            DelayedJob.where('handler like ? and handler like ?', '%DashboardJob%',"%#{task.job_id}%").delete_all
          end
          #if dj_owner.first.blank?
          task.no_scan_job.execute_jobs

          if testing.blank?  ## if its live, update the column, if test , do not update the column
            n_run = case task.run_type.downcase
                      when "monthly"
                        task.executed_at + 1.month
                      when "weekly"
                        task.executed_at + 1.week
                      when "daily"
                        task.executed_at + 1.day
                    end
            task.update_column(:last_executed, Time.zone.now)
            task.update_column(:next_run, n_run)
            task.update_column(:executed_at, n_run)
          end
            #end
        rescue => e
          puts "OWNERJOBSCHEDULER :: RUNNING_OWNER_SCHEDULER => #{e.inspect}"
        end
      end
    end
  end

  def self.set_expired
    NoScanScheduler.
        where("schedule_type != 1").
        joins(:no_scan_job).each do |ow|

      if ow.expired?
        puts ow.id
        ow.no_scan_job.update_column(:status, 3)
      end
    end
  end

  def expired?
    self.end_at < Date.today  if self.end_at
  end
end
