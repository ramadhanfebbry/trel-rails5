class TextTileContent < ActiveRecord::Base

  validates :locale_id, :uniqueness => {:scope => [:chain_id, :content_type]}
  belongs_to :chain
  belongs_to :locale

  def self.content_by_chain_and_locale(chain, locale_key, type)
    text_tile = TextTileContent.joins(:locale).where('locales.key=? and chain_id = ? and content_type = ?', locale_key, chain.id, type).first
    "#{text_tile.try(:content)}"
  end
end
