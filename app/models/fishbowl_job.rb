class FishbowlJob < Struct.new(:chain, :log)

  def perform
    p "perform----------"
    log.update_attributes(:status => "processing", :initiate_at => Time.now)
    if log.log_type == "Initial Sync"
      chain.initial_sync_user_with_members_fishbowl(log.reload)
    else
      chain.sync(log.reload)
    end
  end

  def error(job, exception)
    p "---------email marketing erroor handling-------"
    struct = YAML::load(job.handler)
    chain = struct.chain
    log = struct.log
    log.update_attributes(:status =>  "failed", :completed_at => Time.now, :error_report => exception.to_s)  unless log.blank?
    p log
    ChainMailer.mailchimp_sync_log_error_report(chain, exception.to_s, "Fishbowl").deliver
    p "-----------------finishhhhh------"
  end

end