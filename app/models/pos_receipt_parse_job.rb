class PosReceiptParseJob < Struct.new(:pos_check_upload, :user, :restaurant_id, :offer_id, :receipt_id)

  def perform
    pos_receipt_parser = PosReceiptParser.new(pos_check_upload, user, restaurant_id, offer_id, receipt_id)
    pos_receipt_parser.do_parse
  end

end