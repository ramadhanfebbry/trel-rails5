class GoalUserIncentive < ActiveRecord::Base

  belongs_to :user
  belongs_to :goal
  belongs_to :week
  belongs_to :incentive, :polymorphic => true

  def self.add_incentive_to_user(incentive, user, goal, week)
    goal_user_incentive = GoalUserIncentive.new
    goal_user_incentive.incentive = incentive
    goal_user_incentive.user_id = user.id
    goal_user_incentive.goal_id = goal.id
    goal_user_incentive.week_id = week.id
    goal_user_incentive.save
  end
  
end
