class Membership < ActiveRecord::Base
  belongs_to :chain
  belongs_to :user
  has_many :reward_transactions
  has_many :receipt_transactions
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
  
end
