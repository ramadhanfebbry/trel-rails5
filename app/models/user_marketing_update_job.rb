class UserMarketingUpdateJob < Struct.new(:user)

  def perform
    chain = user.chain
    #MAILCHIMP
    em = EmailMarketing.where(:chain_id =>  user.chain_id, :user_id => user.id).first
    if em && user.marketing_optin
      em.update_column(:status, EmailMarketing::STATUS["CHANGED"]) unless em.blank?
    elsif em
      em.update_column(:status, EmailMarketing::STATUS["MARK OPT OUT"]) unless em.blank?
      em.unsubscribe_member unless em.blank?
    end

    #FISHBOWL
    uf = UserFishbowl.where(:chain_id =>  user.chain_id, :user_id => user.id).first
    # if uf && user.marketing_optin
    #   chain.update_fishbowl_user_profile(user) unless uf.blank?
    # elsif uf
    #   uf.update_column(:status, EmailMarketing::STATUS["MARK OPT OUT"])  unless uf.blank?
    #   user.chain.unsubscribe_member(user)  unless uf.blank?
    # end
    if user.marketing_optin
      if chain.fishbowl_setting && uf
        chain.update_fishbowl_user_profile(user)
      end
    else
      if chain.fishbowl_setting && uf
        uf.update_column(:status, EmailMarketing::STATUS["MARK OPT OUT"])
        user.chain.unsubscribe_member(user)
      end
    end
  end

end
