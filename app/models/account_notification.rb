class AccountNotification < ActiveRecord::Base

  belongs_to :chain
  belongs_to :locale
end
