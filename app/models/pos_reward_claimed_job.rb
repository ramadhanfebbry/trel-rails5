class PosRewardClaimedJob < Struct.new(:reward_transaction)

  def perform
    reward = reward_transaction.reward
    user = reward_transaction.user
    restaurant_lat_lng_id = reward_transaction.restaurant_id
    EmailMarketing.update_email_marketing_location(user, restaurant_lat_lng_id)
    Chain.update_user_fishbowl_location(user, restaurant_lat_lng_id)
    RestaurantUser.update_last_redeem(user)

    transaction_platform = reward_transaction.reward_transaction_platform
    p "transaction_platform #{transaction_platform}"
    if transaction_platform && transaction_platform.platform.eql?(3)
      p "onosys reward"
      reward_transaction.update_attributes(:pos_used => true, :redeeming => false )
      PointHistory.add_to_history(user.id, "Reward ##{reward.id} Claim - Onosys Reward", (reward.points * -1))
    else
      PointHistory.add_to_history(user.id, "Reward ##{reward.id} Claim", (reward.points * -1))
    end
  end

end