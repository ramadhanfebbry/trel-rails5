class RelevantAuth < ActiveRecord::Base

  validates :username, format: { with: /\A[a-zA-Z0-9]+\Z/ }, :length => {:minimum => 6}, :uniqueness => true
  validates :password, :length => {:minimum => 6}

  STATE = {
      :ACTIVE => 1,
      :INACTIVE => 2
  }

  def self.authenticate(username, password)
    auth = self.where(:username => username, :password => password).first
    auth && auth.active?
  end

  def active?
    self.status.eql?(RelevantAuth::STATE[:ACTIVE])
  end



end
