class ReceiptLineItem
  include Mongoid::Document

  field :chain_id, type: Integer
  field :receipt_id, type: Integer
  field :receipt_date, type: Date
  field :qty, type: Integer
  field :description, type: String
  field :amount, type: Float
  field :receipt_status, type: Integer
  field :user_id, type: Integer
  field :restaurant_id, type: Integer

end