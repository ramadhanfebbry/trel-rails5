class VantivNoAvailableGiftcardEmailJob < Struct.new(:chain)

  def perform
    ChainMailer.giftcard_error_report(chain, exception.to_s).deliver
  end
end