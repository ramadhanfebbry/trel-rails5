class ResetEmailNotification < ActiveRecord::Base

  belongs_to :chain
  belongs_to :locale

  validates :subject, :content, :presence => true

  def self.get_email_template(user)
    notif = ResetEmailNotification.where(:chain_id => user.chain_id, :locale_id => user.locale_id).first
    {:subject => (notif.subject rescue ""), :content => (notif.content rescue "")}
  end
end