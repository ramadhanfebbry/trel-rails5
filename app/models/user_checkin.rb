class UserCheckin < ActiveRecord::Base

  belongs_to :user
  belongs_to :deal
  belongs_to :restaurant
  has_one :activity, :as => :user_activity
end
