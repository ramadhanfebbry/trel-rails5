class UserTotalReceipt < ActiveRecord::Base


  after_save :calculate_average


  def calculate_average
    avg = (self.subtotal.to_f / self.receipt_count.to_f).to_f
    if avg.nan?
      self.update_column(:average, 0)
    else
      self.update_column(:average, avg)
    end
  end
end