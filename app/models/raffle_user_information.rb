class RaffleUserInformation < ActiveRecord::Base
  belongs_to :raffle
  belongs_to :user
  has_many :raffle_participation_histories
end
