class PushReward < ActiveRecord::Base

  #apply_simple_captcha

  has_many :delayed_jobs, :as => :delayable, :dependent => :destroy

  belongs_to :chain
  belongs_to :reward
  belongs_to :admin, :foreign_key => "pusher_id"

  serialize :user_ids
  serialize :success_logs
  serialize :error_logs


  attr_accessor :xls, :user_temp_ids, :all_user

  validates :chain_id, :push_type, :reward_id, :presence => true
  validates :xls, :presence => true, :if => :xls_required?
  validates :user_temp_ids, :presence => true, :if => :user_ids_required?


  def send_push_reward_job(owner)
    if !self.user_ids.blank? or self.is_all_user == true
      unless ENV["DEFAULT_HOST_EMAIL"].to_s.blank?
        RewardMailer.admin_push_notification("Push Reward from admin ID:#{self.id}",
                                             "Push Reward from admin ID:#{self.id}",
                                             self, "STARTING @").deliver! rescue nil
      end

      user_collection = self.user_ids[:user_ids] rescue nil

      if self.is_all_user?
        user_collection = []
        count_email = User.where("chain_id = ?",self.chain_id).count
        loop = (count_email.to_f / 4000.to_f).ceil
        1.upto(loop) do |page|
          user_collection += User.where("chain_id = ?", self.chain_id).select('id').paginate(page: page, per_page: 4000).map(&:id)
        end
        self.user_ids = {:user_ids => user_collection.compact}
        self.save
      end

      unless testing_mode.blank?
        first_data = self.user_ids[:user_ids].first rescue nil
        user_collection = []
        1.upto(testing_mode).each{|x| user_collection << first_data}
        self.user_ids = {:user_ids => user_collection.compact}
        self.save
        user_collection = user_collection.compact
      end

      user_collection.in_groups_of(Setting.delayed_job.push_reward_limit) do |u_ids|

        dl = Delayed::Job.enqueue(PushRewardJob.new(self.id, u_ids.compact, owner.email, self.id),
                                  :chain_id => self.chain_id, :delayable_type => self.class.to_s, :delayable_id => self.id, :queue => 'prel_job')
        dl.update_column(:run_at, self.executed_at) unless self.executed_at.blank?
      end
    end
  end


  def xls_required?
    self.push_type.to_i.eql?(1)
  end

  def user_ids_required?
    self.push_type.to_i.eql?(2)
  end

  def success_log_entries
    log_entries = []
    PushLogEntry.where(:pushable_id => self.id, :pushable_type => self.class.to_s, :log_type => 1).each do |push_log|
      log_entries << push_log.logs
    end
     log_entries.flatten
  end

  def success_log_entries_count
    PushLogEntry.find_by_sql("
      SELECT SUM(array_length(string_to_array(logs, ':user_id'), 1) - 1) as log_count FROM push_log_entries
where pushable_id=#{self.id} and pushable_type = '#{self.class.to_s}' and log_type = 1").first.log_count.to_i rescue 0
  end

  def error_log_entries
    log_entries = []
    PushLogEntry.where(:pushable_id => self.id, :pushable_type => self.class.to_s, :log_type => 2).each do |push_log|
      log_entries << push_log.logs
    end
    log_entries.flatten
  end

  def error_log_entries_count
    #PushLogEntry.where(:pushable_id => self.id, :pushable_type => self.class.to_s, :log_type => 2).count
    PushLogEntry.find_by_sql("
      SELECT SUM(array_length(string_to_array(logs, ':user_id'), 1) - 1) as log_count FROM push_log_entries
where pushable_id=#{self.id} and pushable_type = '#{self.class.to_s}' and log_type = 2").first.log_count.to_i
  end

end
