class ApplicationKeyDetail < ActiveRecord::Base
	belongs_to :application_key

	def self.generate
		destroy_all
		ApplicationKey.pluck(:id).each {|x| create(application_key_id: x)}
	end
end
