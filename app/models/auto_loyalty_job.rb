class AutoLoyaltyJob < Struct.new(:chain, :pos_location, :user, :user_session, :user_code_user, :location, :xml_receipt, :check_id, :sequence_number, :revenue_center, :payment_included, :code_type, :barcode, :employee_num, :tbl_num)

  def perform
    loyalty_response = Micros::Loyalty.execute(chain, pos_location, user, user_session, user_code_user, location, xml_receipt, check_id, sequence_number, revenue_center, payment_included, code_type, barcode, employee_num, tbl_num)
    p "this is auto loyalty response for #{user.id} - #{user.email}, check_id = #{check_id}, sequence_number = #{sequence_number} = #{loyalty_response}"
  end

end