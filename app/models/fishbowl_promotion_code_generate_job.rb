class FishbowlPromotionCodeGenerateJob < Struct.new(:reward)

  def perform
    reward.generate_fishbowl_promotion_code
  end

end
