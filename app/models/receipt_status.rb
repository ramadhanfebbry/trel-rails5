class ReceiptStatus < ActiveRecord::Base
   belongs_to :receipt_transaction

   DJ_STATUS = {CREATED: 1,ONPROGRESS:2, COMPLETED: 3, FAILED: 4}
end