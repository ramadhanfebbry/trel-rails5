class Comment < ActiveRecord::Base
  set_table_name :online_orders
  serialize :content
  belongs_to :receipt
  has_one :online_parse_result, :foreign_key => 'online_order_id'

  def nekter_parse
    #x = self
    #x.
    #begin
    nok_obj = nokogiri_obj
    result = OpenStruct.new

    result.location = parse_location rescue nil
    result.email = parse_email rescue nil
    result.date_placed = parse_date_placed rescue nil
    result.date_pickup = parse_date_pickup rescue nil
    result.order_id = parse_order_id rescue nil
    result.subtotal = parse_subtotal rescue nil
    result.total = parse_total rescue nil
    result.tax = parse_tax rescue nil
    result.order_no = parse_order_no rescue nil
    result.user_id = parse_user_id
    result.customer_no = parse_customer_no rescue nil
    result.restaurant = parse_restaurant
    result.chain = parse_chain

    result.restaurant_id = parse_restaurant_id rescue nil
    result.chain_id = parse_chain_id rescue nil
    result
    #rescue

    #end
    return result
  end

  def generate_parse_email
    result = parse_switching
    #self.update_column(:content ,result)
    self.update_column(:chain_id, result.chain_id)

    result_parse = OnlineParseResult.where(:online_order_id => self.id).first

    if result_parse.blank?
    result_parse = OnlineParseResult.new
    result_parse.online_order_id = self.id
    end
    result_parse.location = result.location rescue nil
    result_parse.email = result.email rescue nil
    result_parse.date_placed = DateTime.strptime(result.date_placed, "%m/%d/%Y %I:%M %p") rescue nil
    result_parse.issue_time = result.date_pickup.split(' ')[1..2].join(' ') rescue nil
    result_parse.date_pickup = DateTime.strptime(result.date_pickup, "%m/%d/%Y %I:%M %p") rescue nil rescue nil
    result_parse.order_id = result.order_id rescue nil
    result_parse.subtotal = result.subtotal.gsub('$', '').to_f  rescue result.subtotal.to_f
    result_parse.total = result.total.gsub('$', '').to_f  rescue result.total.to_f
    result_parse.tax = result.tax.gsub('$', '') rescue result.tax.to_f
    result_parse.order_no = result.order_no rescue nil
    result_parse.user_id = result.user_id
    result_parse.customer_no = result.customer_no rescue nil
    result_parse.restaurant = result.restaurant
    result_parse.chain = result.chain

    result_parse.restaurant_id = result.restaurant_id rescue nil
    result_parse.chain_id = result.chain_id rescue nil
    p result_parse.issue_time
    result_parse.save
  end

  def nokogiri_obj
    x = self
    x = Nokogiri.parse(x.body)
  end

  def parse_location
    x = nokogiri_obj
    location = x.xpath('//h2[contains(.,"Nekter")]').text
    return location
  end

  def parse_email
    x = nokogiri_obj
    email_address = x.xpath('//td//a[contains(.,"@")]').text
  end

  def parse_user_id
    User.where("lower(email) = ? and chain_id = ?", self.parse_email.downcase, self.parse_chain_id).first.id rescue nil
  end

  def parse_date_placed
    x = nokogiri_obj
    x.xpath('//td[contains(.,"Placed")]/b').text
  end

  def parse_date_pickup
    x = nokogiri_obj
    x.xpath('//td[contains(.,"Pickup")]//b').last.text rescue nil
  end

  def parse_order_id
    x = nokogiri_obj
    x.xpath('//h2[contains(.,"Order No")]').first.text.split(': ').last
  end

  def parse_orders
    x = nokogiri_obj
    x.xpath('//td[contains(.,@)]')
  end

  def parse_subtotal
    x = nokogiri_obj
    #x.xpath("//table/tr[@valign='top']/td[@align='right']//table/tr/td[@align='right']").first.next_element.text rescue nil
    #x.xpath('//strong[@id="cartTotal"]').text
    x.search("//td[contains(text(), 'Sub Total:')]")[0].next_element.text rescue nil
  end

  def parse_total
    x = nokogiri_obj
    x.xpath('//strong[@id="cartTotal"]').text
  end

  def parse_tax
    x = nokogiri_obj
    x.xpath("//table/tr[@valign='top']/td[@align='right']//table/tr/td")[2].next_element.text rescue nil
  end

  def parse_order_no
    x = nokogiri_obj
    m = x.xpath('//h2[contains(.,"Order")]').last.text.split(': ').last

    if m.to_i == 0
      temp = x.search("[text()*='Order No']").first.text.split(' ').delete_if{|x| !x.include?("No")}[0].split(':').last
      temp = temp[1..temp.size] if temp[0].blank?
      m = temp
    end
    return m
  end

  def parse_customer_no
    x = nokogiri_obj
    x.xpath('//h2[contains(.,"Customer")]').text.split(': ').last
  end

  def parse_restaurant
    parse_location.split('- ').last
  end

  def parse_chain
    parse_location.split(' -').first
  end

  def parse_chain_id
    x = parse_chain
    Chain.where("lower(name) like '%#{x.downcase.strip}%'").first.id rescue nil
  end

  def parse_restaurant_id
    x = parse_restaurant
    r_id = Restaurant.active.where("lower(name) like '%#{x.downcase.strip}%' and chain_id = #{parse_chain_id}").first.id rescue nil
    return r_id
  end

  def parse_switching
    ### cbc parser first
    x = Parser::CbcOloParser.new(self.body)
    x.parse_html
    x = x.open_struct_result

    if !x.chain_id.blank? and !x.restaurant_id.blank?
      return x
    end
    ## end

    # try for nekter
    x = nekter_parse
    if x.chain.blank?
      x = MailParseLpqVer2.new(self.body)
      x = x.parse_email
    end
    return x
  end

  def receipt_process
    x=  parse_switching
#    user = User.find(x.user_id) rescue nil
    restaurant = Restaurant.find(x.restaurant_id) rescue nil
    chain = Chain.find(x.chain_id) rescue nil
    user = chain.users.where("users.id = #{x.user_id}").first rescue nil

    #offer = restaurant.offers.unscoped.online_offers.last rescue nil
    #self.offers.where("\"effectiveDate\" <= '#{Time.current.strftime("%Y-%m-%d")}' AND \"expiryDate\" >= '#{Time.current.strftime("%Y-%m-%d")}' and having_rule IS FALSE").order("created_at DESC").first
    unless restaurant.blank?
      offer = Offer.unscoped.joins(:restaurant_offers).where("\"effectiveDate\" <= '#{Time.current.strftime("%Y-%m-%d")}' AND \"expiryDate\" >= '#{Time.current.strftime("%Y-%m-%d")}' and having_rule IS FALSE").where("restaurant_id = #{x.restaurant_id} and is_online_order is true").order("created_at DESC").first rescue nil
    end
    rest_offer = RestaurantOffer.where(:offer_id => offer.id).last.id rescue nil


    #if !chain.blank? &&  !restaurant.blank?

    receipt = Receipt.new(
        :chain_id => x.chain_id,
        :user_id => x.user_id,
        :status => Receipt::STATUS[:RECEIVED],
        :is_online_order => true
    )

    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.status = Receipt::STATUS[:RECEIVED]
    receipt_transaction.subtotal = x.subtotal.gsub('$', '').to_f  rescue x.subtotal.to_f
    receipt_transaction.tax = x.tax.gsub('$', '').to_f rescue x.tax.to_f
    receipt_transaction.issue_date =  DateTime.strptime(x.date_pickup, "%m/%d/%Y %I:%M %p")  + 10.hours rescue nil
    receipt_transaction.time_stamp = x.date_pickup.split(' ')[1..2].join(' ') rescue nil
    receipt_transaction.receipt_date = DateTime.strptime(x.date_pickup, "%m/%d/%Y %I:%M %p") rescue nil
    receipt_transaction.receipt_time = x.date_pickup.split(' ')[1..2].join(' ') rescue nil

    receipt_transaction.restaurant_id = x.restaurant_id
    receipt_transaction.restaurant_offer_id = rest_offer
    receipt_transaction.receipt_number = x.order_no
    receipt.receipt_transactions << receipt_transaction
    #Receipt.skip_callbacks = false
    receipt.save #(:callback => false)
    self.update_column(:receipt_id, receipt.id) rescue nil

    if user.blank?
      user_on=REDIS.get "content_email_parse_user_on_off_#{x.chain_id}" rescue nil
      subject = REDIS.get "subject_email_parse_user_reject_#{x.chain_id}" rescue nil
      content = REDIS.get "content_email_parse_user_reject_#{x.chain_id}" rescue nil

      if !user_on.blank? and !subject.blank? and !content.blank? # if all the conditions , setting, subject is not blank, and content is not blank
        UserMailer.email_parse_rejected(chain, x.email).deliver! rescue nil
        puts "SENDING EMAILLS"
        return true
      end
    end
    puts "Receipt::OrderOnline STARTED -=-==========================="

    if !offer.blank? && !user.blank?
      today_maxed = false
      ## "pay at restaurant" means that this order hasnt been payed
      is_payed = false
      is_payed = x.is_payed rescue true
      is_payed = true if x.is_payed.blank?
      # end
      puts "is_payed = #{is_payed}"

      if today_maxed == false and (is_payed == true or is_payed.blank?)
        ## check max submit
        puts "Receipt::OrderOnline checked -=-==========================="
        receipt.status =  Receipt::STATUS[:APPROVED]
        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.status = Receipt::STATUS[:APPROVED]
        receipt_transaction.subtotal = x.subtotal.gsub('$', '').to_f  rescue x.subtotal.to_f
        receipt_transaction.tax = x.tax.gsub('$', '').to_f rescue x.tax.to_f
        receipt_transaction.issue_date = DateTime.strptime(x.date_pickup, "%m/%d/%Y %I:%M %p") + 10.hours rescue nil
        receipt_transaction.time_stamp =  x.date_pickup.split(' ')[1..2].join(' ') rescue nil
        receipt_transaction.receipt_date = DateTime.strptime(x.date_pickup, "%m/%d/%Y %I:%M %p") rescue nil
        receipt_transaction.receipt_time = x.date_pickup.split(' ')[1..2].join(' ') rescue nil
        receipt_transaction.restaurant_id = x.restaurant_id
        receipt_transaction.receipt_number = x.order_no
        receipt_transaction.restaurant_offer_id = rest_offer
      end

      is_date = receipt_transaction.issue_date.blank?? receipt.created_at : receipt_transaction.issue_date
      if receipt.chain.today_olo_max_user_limit?(receipt, is_date)
        today_maxed = true
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.status = Receipt::STATUS[:REJECTED]
        receipt_transaction.instructions = {:user_maxed => "user_maxed"}
      end

      if  Receipt.joins(:receipt_transactions).where("chain_id = ? and is_online_order = ? and receipt_number = '#{ x.order_no}' and receipts.status = 3",chain.id, true).
          select('count(distinct(receipts.*))').first.count.to_i ==  1
        puts
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.status = Receipt::STATUS[:REJECTED]
        receipt_transaction.instructions = {:receipt => "rejected"}
      end

      # max reached approved receipt for chain


      receipt.receipt_transactions << receipt_transaction
      receipt.save

      if ENV['DEFAULT_HOST_EMAIL'].to_s.include?('trelevant')
       receipt.chain.update_dashboard_instant if receipt and receipt.chain
      end
    end
    #end
  end

end
