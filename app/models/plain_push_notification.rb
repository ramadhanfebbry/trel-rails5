class PlainPushNotification < ActiveRecord::Base
  has_many :delayed_jobs, :as => :delayable, :dependent => :destroy
  has_many :history_notifications, :foreign_key => 'plain_id'
  has_many :notification_locales, :as => :notifiable, :dependent => :destroy
  belongs_to :admin

  serialize :email
  serialize :chains
  serialize :content

  scope :in_progress, -> { where("delayed_jobs.locked_at is not null and last_error is null").joins(:delayed_jobs) }
  scope :failed, -> { where("delayed_jobs.locked_at is not null and last_error is not null").joins(:delayed_jobs) }

  attr_accessor :run_at


  validates :content, :presence => true
  validates_each :email do |record, attr, value|
    problems = ''
    if value
      if value[:emails].class == Array
        value[:emails].each { |em|
          problems << "Emails Cannot blank" unless em
        }
      else
        problems << "Cannot Be Blank "
      end
    else
      problems = 'Please supply at least one name and url'
    end
    record.errors.add(:email, problems) unless problems.blank?
  end

  #validates_each :chains do |record, attr, value|
  #  problems = []
  #  if value
  #    if value[:chain_ids].class == Array
  #      value[:chain_ids].each{|em|
  #        problems << "Chain Cannot blank" unless em
  #      }
  #    else
  #      problems << "Cannot Be blank"
  #    end
  #  else
  #    problems << 'Please supply at least one chain'
  #  end
  #  record.errors.add(:chains, problems) unless problems.blank?
  #end

  validates_each :content do |record, attr, value|
    problems = ''
    if value
      if value[:content]["content"].class == ActiveSupport::HashWithIndifferentAccess
        value[:content]["content"].each { |key, val|
          if val.blank? && key.include?('device')
            problems << "| #{key} Cannot blank" if record.is_push_phone == true
          elsif val.blank?
            problems << "| #{key} Cannot blank" if record.is_push_email == true
          end

        }
      else
        problems << "Cannot Be blank"
      end
    end
    record.errors.add(:content, problems) if problems != ''
  end

  def job_status
    if self.delayed_jobs.count == 0
      return "<a class='btn btn-small btn-success'>Completed</a>"
    elsif self.delayed_jobs.where("delayed_jobs.locked_at is not null and last_error is null and run_at IS NOT NULL")
      return "<a class='btn btn-small btn-warning'>Schedulled</a>"
    elsif self.delayed_jobs.where("delayed_jobs.locked_at is not null and last_error is null")
      return "<a class='btn btn-small btn-info'>Executing ...</a>"
    elsif self.delayed_jobs.where("delayed_jobs.locked_at is not null and last_error is not null")
      return "<a class='btn btn-small btn-danger'>Failed</a>"
    else
      return "N/A"
    end
  end

  def send_plain_email
    ### push_email
    unless ENV["DEFAULT_HOST_EMAIL"].to_s.blank?
      RewardMailer.admin_push_notification("Push Notif/ email from admin ID:#{self.id}",
                                           self.content[:content],
                                           self, "STARTING @").deliver! rescue nil
    end
    if self.is_push_email
      dl = Delayed::Job.enqueue(PushPlainNotificationJob.new(self.email[:emails], self.content[:content]["content"],
                                                            self.chains[:chain_ids].to_i, self,"last_email"), :queue => 'prel_job')
      #dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id)
      dl.update_column(:delayable_type,self.class.to_s)
      dl.update_column(:delayable_id , self.id)
      #dl.update_column(:chain_id, self.chain_id)
      dl.update_column(:run_at, self.executed_at) unless self.executed_at.blank?
    end
    ## push_notification device ( android and IPhone
    if self.is_push_phone
      unless self.email[:emails].blank?
        email_collection = self.email[:emails]
        unless testing_mode.blank?
          first_data = self.email[:emails].first rescue nil
          email_collection = []
          1.upto(testing_mode).each{|x| email_collection << first_data}
        end

        email_collection.in_groups_of(Setting.delayed_job.push_notification_limit) do |u_id|
          last_job = nil
          dl = Delayed::Job.enqueue(
              PushPlainNotificationDeviceJob.new(self.id,
                                                 u_id,
                                                 self.content[:content]["content"],
                                                 self.chains[:chain_ids].to_i, last_job), :queue => 'prel_job')
          #dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id)
          dl.update_column(:delayable_type,self.class.to_s)
          dl.update_column(:delayable_id , self.id)
          dl.update_column(:run_at, self.executed_at) unless self.executed_at.blank?
        end
      end
    end
    return true
  end

  def push_plain_to_android(reg_ids, notification, url = '')
    chain = Chain.find(self.chains[:chain_ids].to_i)
    if application = chain.applications.by_device_type(1).first
      return if application.key.blank?
      message = GcmHelper::Message.new
      message.delay_while_idle = true
      message.add_data('alert', notification)
      message.add_data('data', { 'url' => url })
      message.add_data('timestamp', "#{Time.now}")
      #      key="AIzaSyCHAQW9i1vGKNUD83jSsSn8Jfgb2TPFGKc"
      sender = GcmHelper::Sender.new(application.key)
      response = sender.multicast_with_retry(message, reg_ids, 3)

      ## logging

      success_reg_ids = reg_ids#response.success
      begin
        success_reg_ids.each do |sc|
          begin
            unless sc.blank?
              user = User.find_by_device_token(sc) rescue nil
              if user && user.chain.id == chain.id
                user_id = user.id rescue nil
                p user.id
                p user.email
                HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 1)
                puts "ANDROID PUSHPLAINNOTIFICATION SUCCESS LOG CREATED"
                p "-----success------"
              elsif user
                p user.id
                p user.email
                p "android failed user----"
                HistoryNotification.create(:user_id => user.id, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "User not included on chain selected.")
              else
                p "---- android dt not found---"
                HistoryNotification.create(:user_id => 0, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "User not found device token #{sc}")
              end
            else
              p "---- android dt is blank"
              HistoryNotification.create(:user_id => 0, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "blank device token")
            end
          rescue => e
            user_id = User.find_by_device_token(sc).id rescue nil
            if user_id.blank?
              user_id = 0
              error_text = "User not found device token #{sc}"
            else
              error_text = e.inspect
            end
            puts "PLAINPUSHNOTIFICATION::PUSH_PLAIN_TO_ANDROID LOGGING #{e.inspect}"
            HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2,
                                       :status => 2, :error_text => error_text)
            next
          end
        end
      rescue => e
        puts "ANDROID PUSHPLAINNOTIFICATION Success Reg ids  #{e.inspect}"
      end
    end
  end

  def push_plain_to_iphone(device_tokens, message, url = '')
    chain = Chain.find(self.chains[:chain_ids].to_i)
    if application = chain.applications.by_device_type(2).first
      message = message.blank? ? "" : message
      device_tokens.each do |device_token|
        ## logging
        begin
          unless device_token.blank?
            user = User.find_by_device_token(device_token) rescue nil
            user_id = user.id rescue nil
            if user && user.chain_id == chain.id
              rapns_app = application.rapns_app
              return if rapns_app.blank?
              n = Rpush::Apns::Notification.new
              n.app = rapns_app
              n.device_token = device_token
              n.alert = message
              n.data = {
                  'url' => url
              } unless url.blank?
              #n.uri = url unless url.blank?
              n.sound = "1.aiff"
              n.save!
              p user.id
              p user.email
              HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 1)
              p "iphone success user -------"
            elsif user
              p user.id
              p user.email
              HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "User not included on chain selected.")
              p "iphone failed user not in chain-----"
            else
              HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "User not found device token #{device_token}")
              p "ihpne failed user not found"
            end
          else
            HistoryNotification.create(:user_id => 0, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "blank device token")
          end
        rescue => e
          puts "IPHONE PLAINPUSHNOTIFICATION::History Plain logging #{e.inspect}"
          user_id = User.find_by_device_token(device_token)
          if user_id.blank?
            user_id = 0
            user_text = "User not found device token #{device_token}"
          else
            user_id = user_id.id
            user_text = e.inspect
          end
          HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2,
                                     :status => 2, :error_text => user_text)
          p "iphone failed user from begin rescue"
          next
        end
      end
    end
  end

#  def push_to_iphone2(reg_ids_by_locale, hash_notification)
#    if application = self.chain.applications.by_device_type(2).first
#      reg_ids_by_locale.each do |key, value|
#        msg = key.blank? ? "" : hash_notification[key]["notification"]
#        value[:iphone_device_tokens].each do |device_token|
#          rapns_app = application.rapns_app
#          return if rapns_app.blank?
#          n = Rapns::Notification.new
#          n.app = rapns_app.key
#          n.device_token = device_token
#          n.alert = msg
#          n.sound = "1.aiff"
#          n.save!
#        end
#      end
#    end
#  end
end
