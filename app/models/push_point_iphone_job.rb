class PushPointIphoneJob < Struct.new(:user, :reg_ids_by_locale, :hash_notification,:point, :receipt)

  def perform
    image_url = nil
    image_url = "<a href='#{receipt.image.url}' target='_blank'>#{receipt.id}</a>" unless receipt.blank?
    user.push_to_iphone2(reg_ids_by_locale, hash_notification,point, image_url)
    #    reward.push_to_android_and_email(reg_ids_by_locale, hash_notification)
    puts "Completed sending PN to Iphone"
  end
  
end
