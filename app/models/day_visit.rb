class DayVisit < ActiveRecord::Base
  # To change this template use File | Settings | File Templates.
  belongs_to :user
  belongs_to :restaurant

  #DayVisit.generate_all_data
  def self.generate_all_data
    DayVisit.delete_all
    User.where("active = ?", true).each do |user| ## get all users
      user.receipts.order("date(receipts.created_at) asc").
          select("distinct date(receipts.created_at),id").
          where("receipts.status = 3").each do |rc|
        begin
          l_trans = rc.last_transaction
          date_tmp = l_trans.issue_date.nil? ? rc.created_at : l_trans.issue_date
          if date_tmp.year != 2013 and date_tmp.year != 2012
            date_tmp = date_tmp.change(:year => rc.created_at.year)
          end

          DayVisit.create(
              :user_id => user.id, :chain_id => user.chain.id,
              :visit_at => date_tmp,
              :restaurant_id => l_trans.restaurant_id
          )
        rescue => e
          #puts "duplicate for restaurant #{e.inspect}"
        end
      end
    end
  end

  #DayVisit.update_day_visit(l_trans)
  def self.update_day_visit(l_trans)
    begin
      rc = l_trans.receipt
      res_id =  l_trans.restaurant_id#.blank? ? (l_trans.restaurant_offer.blank?? nil : l_trans.restaurant_offer.restaurant_id) : l_trans.restaurant_id

      user = rc.user
      dv = DayVisit.where(
          :user_id => rc.user_id,
          :chain_id => rc.chain_id,
          :restaurant_id =>res_id ,
          :visit_at => l_trans.issue_date.nil? ? rc.created_at.to_date : l_trans.issue_date.to_date
      )

      p "day_visit ------**********"
      p dv


      if dv.blank? and res_id ## if this day havent submit any receipt , else do nothing
        p "executes dv blank conditions -----"
        hash_params = {:user_id => user.id, :chain_id => user.chain.id,
                       :visit_at => l_trans.issue_date.nil? ? rc.created_at.to_date : l_trans.issue_date.to_date,
                       :restaurant_id => res_id}
        p hash_params
        p DayVisit.create(hash_params)
        day_visits = DayVisit.where(:user_id => user.id, :chain_id => user.chain.id,
                                    :restaurant_id => res_id)
        p day_visits

        AvgVisit.update_avg_visit(day_visits, user.id, res_id)
      end
    end
  rescue ActiveRecord::RecordNotUnique => e
    #puts "Day Visit :: update_day_visit #{e.inspect}"
  end
end