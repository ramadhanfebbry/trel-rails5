class Deal < ActiveRecord::Base
  belongs_to :activity, :polymorphic => true
  belongs_to :incentive, :polymorphic => true
  belongs_to :chain
  has_many :deal_restaurants, :dependent => :destroy
  has_many :restaurants, :through => :deal_restaurants
  has_many :user_checkins
  has_many :user_deal_surveys

  validates :title, :start_date, :end_date, :restaurants, :deal_type, :presence => true
  validates :max_winner, :presence => true, :numericality => {:greater_than => -1, :only_integer => true, :message => "Must be a integer number and greater than or equal 0"},  :if => :is_lottery_deal?
  validates :limit_user_per_day, :numericality => {:greater_than => -1, :only_integer => true, :message => "Must be a integer number and greater than or equal 0"}
  validates :max_user, :numericality => {:greater_than => -1, :only_integer => true, :message => "Must be a integer number and greater than or equal 0"}

  default_scope { where("deals.deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deals.deleted_at IS NOT NULL')
  end
  
  def deal_type_name
    case self.deal_type
    when 1
      "Guaranteed"
    when 2
      "Lottery"
    end
  end

  def associated_survey
    act = self.activity
    act.survey rescue nil
  end

  def fine_print
    act = self.activity
    act.fine_print
  end

  def is_lottery_deal?
    self.deal_type.eql?(2)
  end

  def deal_image_listing_url_iphone
    act = self.activity
    act.deal_asset_iphone.image.url rescue nil
  end

  def deal_image_listing_url_android
    act = self.activity
    act.deal_asset_android.image.url rescue nil
  end

  def update_participation(restaurants, old_participants)
    ids = old_participants.split
    intersection = restaurants & ids;
    if (!ids.blank?)
      pullout((ids-intersection))
    end
    if (!restaurants.blank?)
      participate((restaurants-intersection));
    end
  end

  def pullout(restaurants)
    self.deal_restaurants.where("restaurant_id in (?)", restaurants).destroy_all
  end

  def participate(restaurants)
    restaurants.each do |restaurant_id|
      existing_deleted_restaurant_offer = DealRestaurant.unscoped.where("deal_id = ? AND restaurant_id = ? AND deleted_at IS NOT NULL", self.id, restaurant_id).first
      if existing_deleted_restaurant_offer.blank?
        existing_restaurant_offer = DealRestaurant.where("deal_id = ? AND restaurant_id = ?", self.id, restaurant_id).first
        DealRestaurant.create(:offer_id => self.id, :restaurant_id => restaurant_id) if existing_restaurant_offer.blank?
      else
        existing_deleted_restaurant_offer.revive
      end
    end
  end
end
