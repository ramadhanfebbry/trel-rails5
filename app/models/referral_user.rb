class ReferralUser < ActiveRecord::Base
  belongs_to :referral_code, :foreign_key => "code_id"
  belongs_to :user
  belongs_to :referral_program

  def get_incentive?(user_referee, user_referer,ref_prog,ref_code)
    ## referee only
    exist = "<span class='badge badge-success'>YES</span>"
    not_exist = "<span class='badge badge-warning'>NO</span>"
    result = []
    referee_reward = self.referral_program.incentive
    referer_reward = self.referral_program.incentive_referer

    if referee_reward.class == ReferralOffer
      result << if PointHistory.where(:description => "Referee Incentive", :user_id => user_referee.id).first.blank? then not_exist  else exist end
    else
      result << if RewardWallet.where(:reward_id => (referee_reward.id rescue -1), :user_id => user_referee.id).first.blank? then not_exist else exist end
    end

    if referer_reward.class == ReferralOffer
      result << if PointHistory.where(:description => "Referer Incentive", :user_id => user_referer.id).first.blank? then not_exist else exist end
    else
      #result << if RewardWallet.where(:reward_id => referee_reward.id, :user_id => user_referer.id).first.blank?  not_exist else exist end
      if RewardWallet.where(:reward_id => (referee_reward.id rescue -1), :user_id => user_referer.id).first.blank?
        result << not_exist
      else
        result << exist
      end
    end
    ## if referee doesnt have the reward and off the limit
    if result.first.include?("NO")
      ref_user_count = ReferralUser.where(:code_id => ref_code.id,:referral_program_id  => ref_prog.id).count
       if ref_prog.max_incentive > 0 and ref_user_count >= ref_prog.max_incentive
         result[1] = not_exist
       end
    end
    return result
  end

  def get_incentive_new?(user_referee, user_referer,ref_prog,ref_code)
    p "get_incentive user_referer:#{user_referer.id}: ref_prog:#{ref_prog} ref_code:#{ref_code}"
    status = []
    users = ref_code.referral_users.where(user_id: user_referee.id, is_get_incentive: true)
    refereeandreferrer = users.where(referral_program_type: ReferralProgram::TYPES["REFEREEANDREFERRER"]).present?
    referee = users.where(referral_program_type: ReferralProgram::TYPES["REFEREEONLY"]).present?
    referrer = users.where(referral_program_type: ReferralProgram::TYPES["REFERRERONLY"]).present?

    if refereeandreferrer
      status[0] = "<span class='badge badge-success'>YES</span>"
      status[1] = "<span class='badge badge-success'>YES</span>"
    elsif referee
      status[0] = "<span class='badge badge-success'>YES</span>"
      status[1] = "<span class='badge badge-warning'>NO</span>"
    elsif referrer
      status[0] = "<span class='badge badge-warning'>NO</span>"
      status[1] = "<span class='badge badge-success'>YES</span>"
    else
      status[0] = "<span class='badge badge-warning'>NO</span>"
      status[1] = "<span class='badge badge-warning'>NO</span>"
    end

    return status
  end

end
