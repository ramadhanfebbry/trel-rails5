class Perk < ActiveRecord::Base
  TYPE = {MULTIPLY: 1, BONUS: 2}

  belongs_to :chain
  has_many :perk_restaurants,:dependent => :destroy
  has_many :promotions, :as => :promotable, :dependent => :destroy

  validates :multiplier, :presence => true ,:if => :is_multiply?
  validates :bonus_points, :bonus_threshold, :presence => true ,:if => :is_bonus?
  validates :multiplier, :inclusion => 0..1,:if => :is_multiply?
  validates :chain_id , :presence => true
  validates_associated :perk_restaurants

  scope :by_ids, -> (x) { where("id in (?)", x) }
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  

  def is_multiply?
    self.perk_type == Perk::TYPE[:MULTIPLY]
  end

  def is_bonus?
    self.perk_type == Perk::TYPE[:BONUS]
  end

  def boost(points)
    if is_multiply?
      points *= self.multiplier
    elsif points >= self.bonus_threshold
        points += self.bonus_points      
    end
    points
  end

end
