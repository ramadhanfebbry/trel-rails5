class RewardInformation < ActiveRecord::Base
  belongs_to :reward
  has_many :reward_transaction_details
end
