class GiftcardTransaction < ActiveRecord::Base

  ### paperclip
  has_attached_file :barcode,
                    :styles => { :small => Setting.paperclip.styles.small,
                    :large => Setting.paperclip.styles.large },
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                      :access_key_id => Setting.storage.s3_access_key_id,
                      :secret_access_key => Setting.storage.s3_secret_access_key
                    }

end