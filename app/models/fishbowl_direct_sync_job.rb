class FishbowlDirectSyncJob < Struct.new(:user, :direct_sync)

  def perform
    if user.chain.fishbowl_setting
      user.chain.add_to_fishbowl_user(user, direct_sync)
    end
  end
end