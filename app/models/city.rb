class City < ActiveRecord::Base
  has_many :restaurants
  belongs_to :region
  
  validates :name, :region, :presence => true

  scope :active_cities, -> { where(:active => true) }
  scope :by_input, -> (input) { 
    if (!input.blank?)
      where("lower(cities.name) LIKE lower(?)", "%#{input}%")
    end
  }

  # before_create :upcase_words
  # before_validation :upcase_words
  
  def label
    "%s, %s" % [self.name, self.region.label]
  end

  def upcase_words
    self.name = self.name.titleize    
  end  
end
