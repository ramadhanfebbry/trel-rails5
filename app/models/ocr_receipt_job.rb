class OcrReceiptJob < Struct.new(:message_collection)

  def perform
    p " **************** PERFORM JOB OCR RECEIPT ******************"
    message_collection.each do |message|
      begin
        xml_format = ActiveSupport::JSON.decode(message)
        receipt =  Receipt.find(xml_format["ReceiptId"]) rescue nil
        if receipt && receipt.status == Receipt::STATUS[:PENDING_OCR]
          archived_xml_receipt = ArchivedXmlReceipt.where(:receipt_id => receipt.id).first
          if archived_xml_receipt.blank?
            ArchivedXmlReceipt.create(:receipt_id => receipt.id, :xml_data_receipt => xml_format["Xml"])
          end
          Receipt.read_xml_response_ocr(xml_format)
        end
      rescue => e
        receipt = Receipt.find(xml_format["ReceiptId"]) rescue nil
        [Setting.email.sys_admin, Setting.email.reviewer].each do |recipient|
          if recipient.eql?(Setting.email.reviewer)
            ReceiptMailer.receipt_validation_failed(recipient, e.message, receipt).deliver
          else
            ReceiptMailer.receipt_validation_failed(recipient, e.message, receipt, message).deliver
          end
        end
      end
    end
    p " ******************** END JOB OCR RECEIPT ********************"
  end

end