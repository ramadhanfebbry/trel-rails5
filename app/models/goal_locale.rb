class GoalLocale < ActiveRecord::Base

  attr_accessor :locale_key
  belongs_to :goal
  belongs_to :locale

  validates :locale_id, :text,  :presence => true
  validates_uniqueness_of :goal_id, :scope => :locale_id
  
end
