class RewardTransaction < ActiveRecord::Base
  belongs_to :membership
  belongs_to :restaurant_reward
  belongs_to :user

  belongs_to :reward
  belongs_to :restaurant

  has_one :transaction
  has_one :reward_transaction_platform
  has_many :reward_discounts
  has_many :details, :class_name => "RewardTransactionDetail"
  # after_save :reward_summary_calculated

  default_scope { where("deleted_at" => nil) }
  date_zone = "date(TIMEZONE('UTC', reward_transactions.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
  scope :by_restaurant_and_date, -> (x, y ) { where("#{date_zone} =  date(?) and restaurant_id = ?",y.to_date, x) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def restaurant_and_reward_by_lat_lng(lat, lng)
    output = ""
    unless self.restaurant_reward.blank?
      reward = self.restaurant_reward.reward
      chain = reward.chain rescue nil
      restaurants = Restaurant.geo_scope(:origin => [lat, lng])
      .where('restaurants.chain_id = ?', chain.id).group(Restaurant.col_list)
      .order('distance asc') rescue []
      restaurant = restaurants.first
      old_restaurant = self.restaurant_reward.restaurant
      distance_before = old_restaurant.distance_from([self.latitude, self.longitude]) rescue ''
      different = old_restaurant.eql?(restaurant) ? "FALSE" : "TRUE"
      output = [
        self.id, "#{self.user.email rescue ''}","#{reward.name rescue ''} #{reward.id rescue ''}", "#{chain.name rescue ''} ",
        "RESTAURANT REWARD", "#{self.latitude}  #{self.longitude}", "#{old_restaurant.name rescue ''}", "#{restaurant.name rescue ''}", "#{distance_before}", "#{restaurant.distance rescue ''}", "#{different}", ''
      ]
    else
      reward_wallet = RewardWallet.is_claimed.where("user_id = ? AND updated_at >= ? AND updated_at <= ?", self.user_id, self.created_at - 1.seconds, self.created_at + 1.seconds).first
      reward = reward_wallet.reward rescue nil
      chain = self.user.chain rescue nil
      restaurant = Restaurant.geo_scope(:origin => [lat, lng])
      .where('restaurants.chain_id = ?', chain.id).group(Restaurant.col_list)
      .order('distance asc').first rescue nil

      distance_before = 0
      different = ((distance_before.eql?(restaurant.distance) ? "FALSE" : "TRUE") rescue '')

      output = [
        self.id, "#{self.user.email rescue ''}","#{reward.name rescue ''} #{reward.id rescue ''}", "#{chain.name rescue ''}",
        "REWARD WALLET", "#{self.latitude}  #{self.longitude}", '', "#{restaurant.name rescue ''}", "#{distance_before}", "#{restaurant.distance rescue ''}", "#{different}", "#{reward_wallet.id rescue ''}"
      ]
    end
    output
  end

  def fix_restaurant_and_reward_by_lat_lng(lat, lng)
    unless self.restaurant_reward.blank?
      reward = self.restaurant_reward.reward
      chain = reward.chain rescue nil
      restaurants = Restaurant.geo_scope(:origin => [lat, lng])
      .where('restaurants.chain_id = ?', chain.id).group(Restaurant.col_list)
      .order('distance asc') rescue []
    else
      reward_wallet = RewardWallet.is_claimed.where("user_id = ? AND updated_at >= ? AND updated_at <= ?", self.user_id, self.created_at - 1.seconds, self.created_at + 1.seconds).first
      reward = reward_wallet.reward rescue nil
      chain = self.user.chain rescue nil
      restaurants = []
      restaurant = Restaurant.geo_scope(:origin => [lat, lng])
      restaurants << restaurant
      .where('restaurants.chain_id = ?', chain.id).group(Restaurant.col_list)
      .order('distance asc').first rescue nil
    end
    p "--------------get the nearest restaurant- ---------------------"
    p self
    restaurants.compact!
    restaurants.each do |restaurant|
      p restaurant
      p reward
      new_restaurant_reward = RestaurantReward.where(:restaurant_id => restaurant.id, :reward_id => reward.id).first rescue nil
      p new_restaurant_reward
      unless new_restaurant_reward.blank?
        p "updates restaurant id"
        p self.restaurant_reward = new_restaurant_reward
        p self.save
        break
      end
    end
  end

  def reward_item_user_support_report(reward)
    path = "#{Rails.root}/tmp/reward_item_user_support_report_#{reward.id}_#{Time.now}.csv"
    chain = reward.chain
    count = chain.users.count
    loop = (count.to_f / 500.to_f).ceil
    p "total loop #{loop}"
    File.new(path, 'w')
    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      csv << ["User ID",
              "User Email",
              "Location",
              "Winter Menu item Redmption day",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit"
      ]

      1.upto(loop) do |page|
        p "loop ke #{page}"
        users = chain.users.paginate(page: page, per_page: 500)
        users.each do |user|
          reward_transactions = RewardTransaction.where(:user_id => user.id, :reward_id => reward.id)
          reward_transactions.each do |rt|
            csv_content = nil
            restaurant = rt.restaurant
            claim_date = rt.created_at.to_date
            receipts = Receipt.where(:user_id => user.id, :status => Receipt::STATUS[:APPROVED])
            last_transactions = receipts.map{|r| r.last_transaction}

            last_transactions_gte_claim_date = last_transactions.select{|a| a.issue_date ? a.issue_date.to_date >= claim_date : a.created_at.to_date >= claim_date }
            another_receipts = []
            on_winter_claim_date = last_transactions_gte_claim_date.select{|a| a.issue_date ? a.issue_date.to_date.eql?(claim_date) : a.created_at.to_date.eql?(claim_date)}.first
            if on_winter_claim_date
              another_receipts = last_transactions_gte_claim_date.delete_if{|a| a.id == on_winter_claim_date.id}
            else
              another_receipts = last_transactions_gte_claim_date
            end

            # data rows
            csv_content = [
                user.id,
                user.email,
                (restaurant.name.gsub(",", " ") rescue "null"),
                (on_winter_claim_date.subtotal.to_s rescue "0")
            ]
            another_receipts.sort{|a, b| a.created_at <=> b.created_at}.each do |rt|
              csv_content << (rt.subtotal.to_s rescue "0")
            end
            csv << csv_content
          end
        end
      end
    end
    OwnerMailer.send_reward_redeemed(nil, "sm@dailygobble.com", path, "ieu_banget_#{reward.id}_winter_menu_item_user_support_#{Time.now}.csv","Buat si ganteng syafik dari relevant").deliver!
    File.delete(path)
    puts "Success EXECUTING report"
  end

  def reward_summary_calculated
    if self.status == RewardWallet::STATUS[:CLAIMED]
      restaurant = self.restaurant
      restaurant_id = restaurant.id
      date_input = self.created_at.in_time_zone("EST").to_date
      data = RewardSummary.where("restaurant_id = ? and transaction_date = date(?)", restaurant_id,
                                 date_input).first
      chain_id = restaurant.chain_id

      if data.blank?
        RewardSummary.create(:transaction_date => date_input,
                             :total => 1, :restaurant_id => restaurant_id, :chain_id => chain_id)
      else
        cnt = data.total + 1
        data.update_attributes(:total => cnt)
      end
    end
  end
end

