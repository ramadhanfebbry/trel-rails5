class PosMenuItem < ActiveRecord::Base

  has_many :general_pos_menu_items
  has_many :general_menu_items, :through => :general_pos_menu_items
  belongs_to :chain
  belongs_to :pos_location

end