class PosDiscountType < ActiveRecord::Base

  attr_accessor :discount_price, :discount_percentage, :discount_fixed_price
  before_save :set_selected_discount_amount

  belongs_to :reward

  validates :title, :presence => true
  validates :discount_type, :presence => true

  validates :discount_percentage,:numericality => {
      :greater_than_or_equal_to => 0
  }, :presence => true, :if => :is_discount_percentage?

  validates :discount_price,:numericality => {
      :greater_than_or_equal_to => 0
  }, :presence => true, :if => :is_discount_price?

  validates :discount_fixed_price,:numericality => {
      :greater_than_or_equal_to => 0
  }, :presence => true, :if => :is_discount_fixed_price?

  validates :discount_fixed_price,:numericality => {
      :greater_than_or_equal_to => 0
  }, :presence => true, :if => :is_non_menu_item_based_discount_fixed_price?

  validates :min_no_items_ordered,:numericality => {
      :greater_than_or_equal_to => 0
  }, :presence => true, :if => :is_menu_item_based?

  validates :min_no_items_discounted,:numericality => {
      :greater_than_or_equal_to => 0
  }, :presence => true, :if => :is_menu_item_based?

  TYPES = {
      "MENU ITEM BASED PERCENTAGE OFF" => 1,
      "MENU ITEM BASED FINAL PRICE DISCOUNT" => 2,
      "MENU ITEM BASED FIXED PRICE DISCOUNT" => 3,
      "NON MENU ITEM BASED FIXED DISCOUNT" => 4,
      "NON MENU ITEM BASED PERCENTAGE OFF" => 7
  }

  NETPOS_TYPES = {
     "open" => 5,
     "fixed" => 6
  }

  def is_discount_percentage?
    self.discount_type.to_i.eql?(1)
  end

  def is_discount_price?
    self.discount_type.to_i.eql?(2)
  end

  def is_discount_fixed_price?
    self.discount_type.to_i.eql?(3)
  end

  def is_non_menu_item_based_discount_fixed_price?
    self.discount_type.to_i.eql?(4)  || self.discount_type.to_i.eql?(5) || self.discount_type.to_i.eql?(6)
  end

  def is_non_menu_item_based_percentage_off?
    self.discount_type.to_i.eql?(7)
  end

  def is_menu_item_based?
    [1,2,3].include?(self.discount_type.to_i)
  end

  def set_selected_discount_amount
    if self.is_discount_percentage?
      self.discount_amount = self.discount_percentage
    elsif self.is_discount_price?
      self.discount_amount = self.discount_price
    elsif self.is_discount_fixed_price?
      self.discount_amount = self.discount_fixed_price
    elsif self.is_non_menu_item_based_discount_fixed_price?
      self.discount_amount = self.discount_fixed_price
    elsif self.is_non_menu_item_based_percentage_off?
      self.discount_amount = self.discount_percentage
    end
  end

end
