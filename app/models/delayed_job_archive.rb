class DelayedJobArchive < ActiveRecord::Base

  # DelayedJobArchive.move_archived
  def self.move_archived
    puts "DELAYED JOB , MOVING TO ARCHIVE"
    if Time.zone.now.beginning_of_week == 1
      puts "INSERTING TO delayed_job_archives"
      sql = "
     INSERT INTO delayed_job_archives ('delayed_job_id', 'priority', 'attempts', 'handler', 'last_error',
     'run_at', 'locked_at', 'failed_at', 'locked_by', 'created_at', 'updated_at', 'chain_id', 'delayable_type', 'delayable_id', 'queue')
     (select * from delayed_jobs where last_error is not null and date(created_at) < Date('#{Time.zone.now - 1.month}'))
     "

      ActiveRecord::Base.connection.execute(sql)
      puts "Done inserting To Delayed_job_archives"


      sql = "DELETE FROM delayed_jobs where last_error is not null and date(created_at) < Date('#{Time.zone.now - 1.month}')"
      ActiveRecord::Base.connection.execute(sql)
      puts "DELETING ---------- From Delayed Jobs"
    else
      puts "DELAYED JOB , MOVING TO ARCHIVE NOT TODAY"
    end
  end
end
