class DealOffer < ActiveRecord::Base
  has_one :deal, :as => :incentive
  
  default_scope { where("deal_offers.deleted_at" => nil) }

  validates :title, :presence => true
  validates :points, :presence => true,
    :numericality => {:greater_than => -1, :message => "Must be a number and greater than or equal 0"},
    :if => Proc.new { |deal_of| deal_of.kind ==  1 }
  #validates_numericality_of :multiplier, :greater_than_or_equal_to => 0
  TYPES = {
    "POINTS" => 1, # only once per user, good for welcome bonus reward
    "OFFLINE" => 2, # options for milestone bonus reward
  }
end
