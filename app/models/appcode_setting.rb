class AppcodeSetting < ActiveRecord::Base

  after_save :reset_sequence_when_setting_updated

  CODE_TYPES = {"USERCODE" => 1, "PAYCODE" => 2, "RECEIPTCODE" => 3, "REWARDCODE" => 4, "GIFTCODE" => 5}
  TYPES = {"STATIC" => 1, "DYNAMIC" => 2}
  USER_METADATA = {"Relevant ID" => 1, "Phone Number" => 2}

  belongs_to :chain


  validates :pos_digit_barcode, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true,
      :less_than_or_equal_to => 100
  }, :if => Proc.new{|f| [2,5].include?(f.appcode_type.to_i) || (f.chain && f.chain.use_generated_code && f.code_type.to_i.eql?(4))}

  validates :timer, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }, :if => Proc.new{|f| [2,4,5].include?(f.appcode_type.to_i)|| (f.chain && f.chain.use_generated_code && f.code_type.to_i.eql?(4))}

  validates :zero_padding, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }, :if => Proc.new{|f|  [1,2,5].include?(f.code_type.to_i) || (f.chain && f.chain.use_generated_code && f.code_type.to_i.eql?(4))}

  validates :prefix, :length => {:maximum => 100}, allow_blank: true
  validates :postfix, :length => {:maximum => 100}, allow_blank: true

  validates :barcode_format_type, :barcode_format, :presence => true, :if => Proc.new{|f| [2,5].include?(f.appcode_type.to_i)|| (f.chain && f.chain.use_generated_code && f.code_type.to_i.eql?(4))}

  DEFAULT_SETTING_USERCODE = {
    "code_type" => AppcodeSetting::CODE_TYPES["USERCODE"],
    "appcode_type" => AppcodeSetting::TYPES["STATIC"],
    "zero_padding" => 0,
    "user_metadata" => 1
  }

  DEFAULT_SETTING_PAYCODE = {
    "code_type" => AppcodeSetting::CODE_TYPES["PAYCODE"],
    "appcode_type" => AppcodeSetting::TYPES["DYNAMIC"],
    "barcode_format" => Chain::BARCODE_FORMAT["Alphanumeric"],
    "pos_digit_barcode" => 12,
    "barcode_format_type" => Chain::BARCODE_FORMAT_TYPES["Code 39"],
    "zero_padding" => 0
  }

  DEFAULT_SETTING_RECEIPTCODE = {
      "code_type" => AppcodeSetting::CODE_TYPES["RECEIPTCODE"],
      "appcode_type" => AppcodeSetting::TYPES["DYNAMIC"],
      "barcode_format" => Chain::BARCODE_FORMAT["Alphanumeric"],
      "pos_digit_barcode" => 6,
      "barcode_format_type" => Chain::BARCODE_FORMAT_TYPES["Code 39"],
      "zero_padding" => 0
  }

  DEFAULT_SETTING_REWARDCODE = {
      "code_type" => AppcodeSetting::CODE_TYPES["REWARDCODE"],
      "appcode_type" => AppcodeSetting::TYPES["DYNAMIC"],
      "barcode_format" => Chain::BARCODE_FORMAT["Numeric"],
      "pos_digit_barcode" => 3,
      "barcode_format_type" => Chain::BARCODE_FORMAT_TYPES["Code 39"],
      "zero_padding" => 0,
      "timer" => 300
  }

  DEFAULT_SETTING_GIFTCODE = {
      "code_type" => AppcodeSetting::CODE_TYPES["GIFTCODE"],
      "appcode_type" => AppcodeSetting::TYPES["DYNAMIC"],
      "barcode_format" => Chain::BARCODE_FORMAT["Alphanumeric"],
      "pos_digit_barcode" => 12,
      "barcode_format_type" => Chain::BARCODE_FORMAT_TYPES["Code 39"],
      "zero_padding" => 0,
      "default_code" => false
  }


  def self.usercode_convert(chain, usercode)
    user_code_setting = chain.user_code_setting
    user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
    user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
    user = nil
    user_session = nil
    barcode = nil
    static = false
    if user_code_setting.blank? || (user_code_setting && user_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"]))
      usercode = usercode.remove_prepostfix(user_code_setting["prefix"].to_s)
      usercode = usercode.remove_prepostfix(user_code_setting["postfix"].to_s)
      barcode = usercode.upcase.remove_zero_padding(user_code_setting["zero_padding"].to_i)
      user_id = barcode
      if user_code_setting["user_metadata"].eql?(1)
        user = User.where(:id => user_id, :chain_id => chain.id).first rescue nil
      elsif user_code_setting["user_metadata"].eql?(2)
        user = User.where(:phone_number => user_id, :chain_id => chain.id).first
      end
      static = true
    elsif user_code_setting && user_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && user_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["USERCODE"])
      if user_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && user_code_setting["barcode_format"].to_i == 1
        barcode = usercode.to_ean_13_format
      else
        usercode = usercode.remove_prepostfix(user_code_setting["prefix"].to_s)
        usercode = usercode.remove_prepostfix(user_code_setting["postfix"].to_s)
        barcode = usercode.upcase.remove_zero_padding(user_code_setting["zero_padding"].to_i)
      end
      user_session = UserSession.where("code = ? AND chain_id = ? AND code_type = ? AND used IS FALSE",barcode , chain.id, AppcodeSetting::CODE_TYPES["USERCODE"]).first
      user = User.find(user_session.user_id) rescue nil
    end
    return [barcode, user, user_session, static]
  end

  def self.paycode_convert(chain, paycode)
    pay_code_setting = REDIS.get "paycode_chain_#{chain.id}"
    pay_code_setting = JSON.parse(pay_code_setting) rescue nil
    pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
    user = nil
    user_session = nil
    barcode = nil
    if pay_code_setting.blank? || (pay_code_setting && pay_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"]))
      paycode = paycode.remove_prepostfix(pay_code_setting["prefix"].to_s)
      paycode = paycode.remove_prepostfix(pay_code_setting["postfix"].to_s)
      barcode = paycode.upcase.remove_zero_padding(pay_code_setting["zero_padding"].to_i)
      user_id = barcode
      user = User.find(user_id) rescue nil
    elsif pay_code_setting && pay_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && pay_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["PAYCODE"])
      if pay_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && pay_code_setting["barcode_format"].to_i == 1
        barcode = paycode.to_ean_13_format
      else
        paycode = paycode.remove_prepostfix(pay_code_setting["prefix"].to_s)
        paycode = paycode.remove_prepostfix(pay_code_setting["postfix"].to_s)
        barcode = paycode.upcase.remove_zero_padding(pay_code_setting["zero_padding"].to_i)
      end
      user_session = UserSession.where("code = ? AND chain_id = ? AND code_type = ? AND executed IS FALSE AND active IS TRUE", barcode, chain.id, AppcodeSetting::CODE_TYPES["PAYCODE"]).order("created_at DESC").first
      user = User.find(user_session.user_id) rescue nil
    end
    return [barcode, user, user_session]
  end

  def self.giftcode_convert(chain, giftcode)
    gift_code_setting = chain.gift_code_setting
    gift_code_setting = JSON.parse(gift_code_setting.to_json) rescue nil
    gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
    user = nil
    user_session = nil
    barcode = nil
    if gift_code_setting.blank? || (gift_code_setting && gift_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"]))
      giftcode = giftcode.remove_prepostfix(gift_code_setting["prefix"].to_s)
      giftcode = giftcode.remove_prepostfix(gift_code_setting["postfix"].to_s)
      barcode = giftcode.upcase.remove_zero_padding(gift_code_setting["zero_padding"].to_i)
      user_id = barcode
      user = User.find(user_id) rescue nil
    elsif gift_code_setting && gift_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && gift_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["GIFTCODE"])
      if gift_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && pay_code_setting["barcode_format"].to_i == 1
        barcode = giftcode.to_ean_13_format
      else
        giftcode = giftcode.remove_prepostfix(gift_code_setting["prefix"].to_s)
        giftcode = giftcode.remove_prepostfix(gift_code_setting["postfix"].to_s)
        barcode = giftcode.upcase.remove_zero_padding(gift_code_setting["zero_padding"].to_i)
      end
      user_session = UserSession.where("code = ? AND chain_id = ? AND code_type = ? AND executed IS FALSE",barcode , chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).first
      user = User.find(user_session.user_id) rescue nil
    end
    return [barcode, user, user_session]
  end

  def self.receipt_code_convert(chain, barcode)
    receipt_code_setting = REDIS.get "receiptcode_chain_#{chain.id}"
    receipt_code_setting = JSON.parse(receipt_code_setting) rescue nil
    receipt_code_setting ||= AppcodeSetting::DEFAULT_SETTING_RECEIPTCODE
    barcode = barcode.remove_prepostfix(receipt_code_setting["prefix"].to_s)
    barcode = barcode.remove_prepostfix(receipt_code_setting["postfix"].to_s)
    barcode = barcode.upcase.remove_zero_padding(receipt_code_setting["zero_padding"].to_i)
    return barcode
  end


  def self.converted_usercode_based_on_code_setting(chain, code, code_setting = nil)
    user_code_setting = code_setting
    if user_code_setting.blank?
      user_code_setting = chain.user_code_setting
      user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
      user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
    end
    barcode = code
    if user_code_setting.blank? || (user_code_setting && user_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"]))
      code = code.remove_prepostfix(user_code_setting["prefix"].to_s)
      code = code.remove_prepostfix(user_code_setting["postfix"].to_s)
      barcode = code.upcase.remove_zero_padding(user_code_setting["zero_padding"].to_i)
    elsif user_code_setting && user_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && user_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["USERCODE"])
      if user_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && user_code_setting["barcode_format"].to_i == 1
        barcode = code.to_ean_13_format
      else
        code = code.remove_prepostfix(user_code_setting["prefix"].to_s)
        code = code.remove_prepostfix(user_code_setting["postfix"].to_s)
        barcode = code.upcase.remove_zero_padding(user_code_setting["zero_padding"].to_i)
      end
    end
    barcode
  end

  def self.converted_paycode_based_on_code_setting(chain, code, code_setting = nil)
    pay_code_setting = code_setting
    if pay_code_setting.blank?
      pay_code_setting = REDIS.get "paycode_chain_#{chain.id}"
      pay_code_setting = JSON.parse(pay_code_setting) rescue nil
      pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
    end
    barcode = code
    if pay_code_setting.blank? || (pay_code_setting && pay_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"]))
      code = code.remove_prepostfix(pay_code_setting["prefix"].to_s)
      code = code.remove_prepostfix(pay_code_setting["postfix"].to_s)
      barcode = code.upcase.remove_zero_padding(pay_code_setting["zero_padding"].to_i)
    elsif pay_code_setting && pay_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && pay_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["PAYCODE"])
      if pay_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && pay_code_setting["barcode_format"].to_i == 1
        barcode = code.to_ean_13_format
      else
        code = code.remove_prepostfix(pay_code_setting["prefix"].to_s)
        code = code.remove_prepostfix(pay_code_setting["postfix"].to_s)
        barcode = code.upcase.remove_zero_padding(pay_code_setting["zero_padding"].to_i)
      end
    end
    barcode
  end

  def self.converted_giftcode_based_on_code_setting(chain, code, code_setting = nil)
    gift_code_setting = code_setting
    if gift_code_setting.blank?
      gift_code_setting = chain.gift_code_setting
      gift_code_setting = JSON.parse(gift_code_setting.to_json) rescue nil
      gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
    end
    barcode = code
    if gift_code_setting.blank? || (gift_code_setting && gift_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"]))
      code = code.remove_prepostfix(gift_code_setting["prefix"].to_s)
      code = code.remove_prepostfix(gift_code_setting["postfix"].to_s)
      barcode = code.upcase.remove_zero_padding(gift_code_setting["zero_padding"].to_i)
    elsif gift_code_setting && gift_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && gift_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["GIFTCODE"])
      if gift_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && gift_code_setting["barcode_format"].to_i == 1
        barcode = code.to_ean_13_format
      else
        code = code.remove_prepostfix(gift_code_setting["prefix"].to_s)
        code = code.remove_prepostfix(gift_code_setting["postfix"].to_s)
        barcode = code.upcase.remove_zero_padding(gift_code_setting["zero_padding"].to_i)
      end
    end
    barcode
  end

  def self.converted_rewardcode_based_on_code_setting(chain, code, code_setting = nil)
    reward_code_setting = code_setting
    if reward_code_setting.blank?
      reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
      reward_code_setting = JSON.parse(reward_code_setting) rescue nil
      reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
    end
    reward_code = code
    if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
      if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
        reward_code = reward_code.to_ean_13_format
      end
    end
    code = code.remove_prepostfix(reward_code_setting["prefix"].to_s)
    code = code.remove_prepostfix(reward_code_setting["postfix"].to_s)
    reward_code = code.upcase.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
    reward_code
  end

  def reset_sequence_when_setting_updated
    unless self.new_record?
      if self.pos_digit_barcode_changed? || self.barcode_format_changed?
        UserSession.create_and_update_sequence(self.chain, self.code_type)
        UserSession.set_sequence_number(self.chain, self.code_type)
      end
    end
  end

end