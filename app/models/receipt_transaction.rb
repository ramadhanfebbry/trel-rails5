class ReceiptTransaction < ActiveRecord::Base

  belongs_to :receipt
  belongs_to :restaurant_offer
  has_one :offer, :through => :restaurant_offer
  has_one :restaurant, :through => :restaurant_offer
  belongs_to :reject_reason
  belongs_to :reviewer, :class_name => "Admin", :foreign_key => "admin_id"
  belongs_to :current_restaurant, :class_name => "Restaurant", :foreign_key => "restaurant_id"
  has_many :receipt_statuses
  has_one :receipt_transaction_detail

  validates :issue_date, :subtotal, :time_stamp, :presence => true, :if => :reviewer_validate?

  attr_accessor :reviewer_validation, :line_items, :xml_data, :updated_manually, :updated_discount_from_pos

  default_scope { where(:deleted_at => nil) }

  #  scope :average_check , ->{|restaurant_id|
  #    where("restaurant_id in  (?) and status = ?", restaurant_id, Receipt::STATUS[:APPROVED]).
  #      group("date(created_at)").select("avg(subtotal) as average, date(created_at) as created_at").
  #      order('date(created_at) asc')
  #  }
  scope :average_check , -> (restaurant_id) { where("restaurant_offers.restaurant_id = ? and receipts.status = ?",
          restaurant_id, Receipt::STATUS[:APPROVED]).group("date(receipts.created_at)").
        select("avg(subtotal) as average, date(receipts.created_at) as created_at").order('date(receipts.created_at) asc').
        joins(:receipt).joins(:restaurant_offer) }

  scope :frequency_ratio,-> (restaurant_ids){
    where("restaurant_id in (?) and date(receipts.created_at) between date(?) and date(?)",
          restaurant_ids,(Time.now - 1.years), Time.now ).
        group("receipts.user_id,receipts.created_at").
        select("count(receipts.user_id) as count_user,receipts.created_at").
        joins(:receipt)
  }

  scope :today_receipt_approved, -> (receipt, issue_date){
    joins(:receipt).
        select("receipt_transactions.id, receipt_transactions.status, receipt_transactions.receipt_id, receipts.chain_id, receipts.user_id, receipt_transactions.issue_date").
        where("receipts.id != ? AND receipts.user_id = ? AND receipts.chain_id = ? AND receipts.status = ? AND receipt_transactions.status = ? AND DATE(receipt_transactions.issue_date) = ?", receipt.id, receipt.user_id, receipt.chain_id, Receipt::STATUS[:APPROVED], Receipt::STATUS[:APPROVED], issue_date.to_date)
  }

#  def self.today_receipt_approved_count(receipt, issue_date)
#    lt_collections = ReceiptTransaction.joins(:receipt).select("distinct on(receipt_transactions.receipt_id)
#receipt_transactions.receipt_id, receipt_transactions.status, receipt_transactions.id, receipts.user_id").
#        where("receipt_id <> ? and receipts.user_id = ? and DATE(receipt_transactions.issue_date) = ?", receipt.id, receipt.user_id, issue_date.to_date).
#        order("receipt_transactions.receipt_id,receipt_transactions.id desc")
#    lt_collections.select { |a| a.status == Receipt::STATUS[:APPROVED] }.count
#  end

  def self.today_receipt_approved_count(receipt, issue_date)
     p "TODAY RECEIPT APPROVED COUNT"
     p "ISSUE_DATE #{issue_date}"
     p "RECEIPT ID #{receipt.id}"
#     lt_collections = ReceiptTransaction.joins(:receipt).select("distinct on(receipt_transactions.receipt_id)
# receipt_transactions.receipt_id, receipts.status, receipt_transactions.id, receipts.user_id").
#         where("receipt_id <> ? and receipts.user_id = ? and DATE(receipt_transactions.issue_date) = ?", receipt.id, receipt.user_id, issue_date.to_date).
#         order("receipt_transactions.receipt_id,receipt_transactions.id desc")
#     lt_collections.select { |a| a.status == Receipt::STATUS[:APPROVED] }.count
    ReceiptApprovedDetail.select("id").where("id <> ? AND chain_id = ? AND DATE(issue_date) = ? AND user_id = ?", receipt.id, receipt.chain_id, issue_date, receipt.user_id).count
  end

  def self.today_online_receipt_approved_count(receipt, issue_date)
    lt_collections = ReceiptTransaction.joins(:receipt).
        select("distinct on(receipt_transactions.receipt_id) receipt_transactions.receipt_id, receipts.status, receipt_transactions.id, receipts.user_id").
        where("receipt_id <> ? and receipts.user_id = ? and DATE(receipt_transactions.issue_date) = ?",
              receipt.id, receipt.user_id, issue_date.to_date).
        order("receipt_transactions.receipt_id,receipt_transactions.id desc")
    lt_collections.select { |a| a.status == Receipt::STATUS[:APPROVED] }.count
  end

  def self.calculate_offer_points_earned_per_day(receipt, receipt_transaction, total_points_earned, issue_date)
    user    = receipt.user
    chain   = receipt.chain
    issue_date = issue_date.to_datetime
    point   = 0
    total_points_earned_after = 0

    lt_collections = ReceiptTransaction.select("receipt_transactions.id, receipt_transactions.receipt_id, receipt_transactions.total_points_earned").joins(:receipt).where("receipts.user_id = ? AND receipts.is_online_order = ? AND receipt_transactions.status = ? AND receipt_transactions.issue_date BETWEEN ? AND ? ", user.id, false, 3, issue_date.beginning_of_day, issue_date.end_of_day)
    lt_collections.each do |rt|
      if !rt.blank?
          point = point + rt.total_points_earned.to_f
      end
    end

    if point >= chain.maximum_offer_points_earned_per_day
      total_points_earned_after = 0
    elsif (point + total_points_earned.to_f).to_f > chain.maximum_offer_points_earned_per_day
      total_points_earned_after =  chain.maximum_offer_points_earned_per_day.to_f - point
    elsif (point + total_points_earned.to_f).to_f <= chain.maximum_offer_points_earned_per_day
      total_points_earned_after = total_points_earned.to_f
    else
      total_points_earned_after = 0
    end
    return total_points_earned_after
  end

  #attr_accessible :admin_id, :receipt_id, :restaurant_offer_id, :user_id, :restaurant_id, :status, :image

  #before_create :calculate_total_points_earned
  before_save :perk_and_calculate_points#, :if => :is_ncr_data_replicate?
  #after_save :update_rule_not_met
  #after_save :update_user_points

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def self.average_receipt(res_id, date, params_date)
    if params_date == "day"
      ## get the data per hours
      return average_receipt_day(res_id, date, params_date)
    end
    ## sorting day # with hours
    t_now = Time.zone.now + 1.days
    tmp_date = date
    ##default
    if date.blank? and params_date.blank?
      tmp_date = "receipt_date between '#{(t_now - 7.days).to_date}' and '#{(t_now + 1.days).to_date}'"
    elsif params_date == "month" and date.blank?
      tmp_date = "receipt_date between '#{(t_now - 30.days).to_date}' and '#{(t_now + 1.days).to_date}'"
    end

    tmp_date = nil if tmp_date == "all"


    charts = AverageChartTable.where(:restaurant_id => res_id).where(tmp_date).order("receipt_date asc").
            select('distinct on(receipt_date) average,receipt_date')
    #puts "datas =#{charts}"
    result = []
    charts.each do |chart|
      avg = chart.average.to_f.round(2)
      result << [chart.receipt_date.to_datetime.to_i * 1000, avg]  if avg > 0.0
    end

    return result
  end

  def self.calculate_average_receipt(restaurants, date, params_date)
    ## sorting day # with hours
    t_now = Time.zone.now + 1.days
    tmp_date = date
    ##default

    if date.blank? and params_date.blank?
      tmp_date = "receipt_date between '#{(t_now - 7.days).to_date}' and '#{(t_now + 1.days).to_date}'"
    elsif params_date == "month" and date.blank?
      tmp_date = "receipt_date between '#{(t_now - 30.days).to_date}' and '#{(t_now + 1.days).to_date}'"
    end

    tmp_date = nil if tmp_date == "all"


    charts =  AverageChartTable.where("restaurant_id in (?)", restaurants).where(tmp_date).order("receipt_date asc").select("avg(average) as average,receipt_date").group("receipt_date")
    #puts "datas =#{charts}"
    result = []
    charts.each do |chart|
      avg = chart.average.to_f.round(2)
      result << [chart.receipt_date.to_datetime.to_i * 1000, avg]  if avg > 0.0
    end

    return {:name => "AVG", :data => result}
  end

  def self.calculate_average_receipt_day(restaurants, date, params_date)
    charts =  AverageChartTable.where("restaurant_id in (?)", restaurants).where("receipt_date = ?", Date.today).
        order("date_trunc('hour',created_at)").select("avg(average) as average,date_trunc('hour',created_at) as updated_at").group("date_trunc('hour',created_at)")
    #puts "datas =#{charts}"
    result = []
    charts.each do |chart|
      avg = chart.average.to_f.round(2)
      result << [chart.updated_at.to_i * 1000, avg]  if avg > 0.0
    end

    return {:name => "AVG", :data => result}
  end

  def self.average_receipt_day(res_id, date, params_date)
    result = []; res = []
    tmp_date = date
    #case_issue_date = "(case when receipt_transactions.issue_date
    # is null then receipts.created_at
    #else receipt_transactions.issue_date
    #end)"
    #case_issue_date = "receipts.created_at"

    case_issue_date = "(
  case when exists(select receipt_transactions.issue_date)
  then receipt_transactions.issue_date
    else receipts.created_at
  end
  )"
    puts "Chart::AverageDayFilter restaurant_id = #{res_id}"   if res_id == 1
    time_zone = Time.zone.now.strftime('%Z')
    condition_date = "date(TIMEZONE('UTC',
      #{case_issue_date})
      AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') = date('#{(Time.zone.now).to_date}')
      "

    puts "Chart::AverageDayFilter case issue date = #{case_issue_date}" if res_id == 1
    receipts = Receipt.by_restaurant_ids(res_id).where("receipts.status = 3 and #{condition_date} ")
    receipts.each do |r|
      l_trans = r.last_transaction
      count = l_trans.subtotal.to_f #rescue 0
      count = count.nil? ? 0 : count
      begin
        res << {
            "id" => l_trans.id,
            "date" => l_trans.issue_date.nil? ? r.created_at : l_trans.issue_date,
            "total" => count,
            "stamp" => l_trans.time_stamp
        }
      rescue
        next
      end
    end

    tmp_result = []
    0.upto(24) do |hour|
      tmp_result << [Time.zone.now.utc.change(:hour => hour).to_i * 1000, nil]
    end

    res.each_with_index do |r, m|
      dt = r["date"].to_datetime.in_time_zone((Time.zone_offset(time_zone)/3600))
      hour = dt.hour
      min = dt.min
      #zn = dt.strftime("%p")
      #hour = hour.to_i + 12 if zn.downcase == "pm"  and hour.to_i < 12
      #hour = 0 if hour.to_i == 12 and zn.downcase == "am"
      tmp_avg = (r["total"].to_f).round(2)

      unless r["stamp"].blank?
        time_stamp = r["stamp"]
        split_time = time_stamp.split(':')
        hour = split_time[0]
        #zn = split_time[1][2..3]
        zn = if time_stamp.downcase.include?('am')
               "am"
             else
               "pm"
             end
        min = split_time[1][0..1]
        hour = hour.to_i + 12 if zn and zn.downcase == "pm"  and hour.to_i < 12
        hour = 0 if hour.to_i == 12 and zn.downcase == "am"
      end
     # puts "Average Check Frequency 1 day : HOUR = #{hour.to_i}"
     # puts "Average Check Frequency 1 day : Min = #{min.to_i}"
     # puts "Average Check Frequency 1 day : Datetime = #{dt}"
      next if hour.to_i > 24
      dt = Time.parse(dt.strftime('%Y-%m-%d %I:%M:%S UTC')).to_datetime.change(:hour => hour.to_i, :min => min.to_i)
      dt = dt.to_datetime.change(:hour => hour.to_i, :min => min.to_i)
      #puts dt
      #puts "-------------------------------------"
      if tmp_avg > 0.0
        result = [dt.to_i * 1000, tmp_avg]
      else
        result = [dt.to_i * 1000, nil]
      end
      tmp_result << result
    end
    tmp_result.sort! {|a,b| a[0] <=> b[0] }
  end

  def self.new_average_receipt(res_id, date, show_interval, chain_obj)
    #result = Rails.cache.fetch("new_avg_receipt_#{res_id}_#{date}_#{show_interval}", :expires_in =>  10.minutes) do
    #
    #  result = []; res = []
    #  tmp_date = date
    #  dt_zone = "date(TIMEZONE('UTC', receipts.updated_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
    #  if tmp_date == "all"
    #    tmp_date = nil
    #  elsif tmp_date.blank?
    #    tmp_date = "#{dt_zone} between '#{(Time.zone.now - 30.days).to_date}' and '#{Time.zone.now.to_date}'"
    #  end
    #
    #  receipts = Receipt.by_restaurant_ids(res_id).where(tmp_date).where("receipts.status = 3")#.order("receipt desc")#.group("date(receipts.updated_at)")
    #  receipts.each_with_index do |r, index|
    #    l_trans = r.last_transaction
    #    count =  l_trans.subtotal.to_f #rescue 0
    #    count = count.nil?? 0 : count
    #    begin
    #      res <<  {"id" => l_trans.id, "date" => l_trans.issue_date.to_s, "total" => count}
    #    rescue
    #      next
    #    end
    #  end
    #
    #  date_list = res.map{|x| x["date"].to_date}.uniq.compact.sort
    #
    #  date_list.each do |list|
    #    total = 0
    #    index = 0
    #    res.each_with_index do |r,m|
    #      if r["date"].to_date == list
    #        total = r["total"] + total
    #        index += 1
    #      end
    #    end
    #    tmp_avg = (total.to_f / index).round(2)
    #    result << [list.to_datetime.to_i * 1000, tmp_avg] if list && tmp_avg > 0.0
    #  end
    #  result
    #end
    #return result
  end

  def self.average_frequency_check_chart(restaurant, filter)
    m = [[],[],[]]
    unless filter.blank?
      filter = nil if filter == "all"
      a =  Receipt.by_restaurant_ids(restaurant.id).where(filter).where("receipts.status = #{Receipt::STATUS[:APPROVED]}")
    else
      a = Receipt.by_restaurant_ids(restaurant.id).where("date(receipts.updated_at) between date(?) and date(?)",
                                                         Time.now - 30.days, Time.now).where("receipts.status = #{Receipt::STATUS[:APPROVED]}")
    end

    user_receipts = a.group_by(&:user_id)

    user_receipts.each do |key,val|
      if val.size == 1
        m[0] << key
      elsif val.size == 2
        m[1] << key
      elsif val.size >= 3
        m[2] << key
      end
    end
    return  m
  end

  def self.update_wrong_issue_date
    ReceiptTransaction.where("EXTRACT(YEAR FROM issue_date) != 2013 and EXTRACT(YEAR FROM issue_date) != 2012").each do |rt|
      puts "start process receipt transaction"
      p "current issue date #{rt.issue_date}"
      receipt = rt.receipt
      created_at_for_refer = receipt ? receipt.created_at : rt.created_at
      current_issue_date = rt.issue_date
      fixed_issue_date = current_issue_date.change(:year => created_at_for_refer.year)
      p "fixed issue date #{fixed_issue_date}"
      rt.update_column(:issue_date, fixed_issue_date)
      p rt
    end
  end

  def calculate_total_points_earned
    p "calculate total points earned====================="
    if (!self.offer.blank? and self.offer.offer_rules.active.blank?)   #without rules
      multiplier = self.offer.multiplier_constant_value # get offer multiplier
      if self.receipt.is_receipt_barcode
        #receipt_subtotal = self.subtotal.to_f - self.total_discount.to_f
        receipt_subtotal = self.subtotal.to_f
      else
        receipt_subtotal = self.subtotal.to_f
      end
      dolar_spent = receipt_subtotal * self.receipt.chain.points.to_f
      p multiplier.to_f
      p dolar_spent
      chain = self.receipt.chain rescue nil
      if self.offer.is_multiplier
        tmp_total_points_earned = dolar_spent * multiplier
      else
        tmp_total_points_earned = multiplier
      end
    end #end #without rules
    ## using rules here
    if !self.offer.blank? and !self.offer.offer_rules.active.blank? #and self.receipt.chain.pos_used_type != 0
      off_rule = OfferRuleCalculation::CalculateOfferRule.new(self.offer,self)
      off_rule.result
      tmp_total_points_earned = off_rule.total_points_get
      p "with rules === #{tmp_total_points_earned}"
      if tmp_total_points_earned.to_f == 0.0 || tmp_total_points_earned == -1 || tmp_total_points_earned == -2
        receipt_transaction = self
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt_transaction.instructions = {:offer_rules_not_met => "This receipt does not met any rules"}
        receipt_transaction.instructions = {:ignore_item_list => "This receipt get all the ignore items listed"} if tmp_total_points_earned == -1
        receipt_transaction.instructions = {:ignore_item_reject => "Rejected due the item"} if tmp_total_points_earned == -2
        receipt.status = Receipt::STATUS[:REJECTED]
      end
    end
    p self
    p self.receipt
    p "receipt === =#{self} ------ #{self.receipt}"
    chain = self.receipt.chain rescue nil
    p tmp_total_points_earned.to_f
    p chain
    if chain
      self.total_points_earned = tmp_total_points_earned.send(Chain::ROUNDING_OPTIONS[chain.point_rounding_preference])
    else
      self.total_points_earned = tmp_total_points_earned
    end

    if chain.maximum_offer_points_earned_per_day > 0 && self.receipt.is_online_order == false
      self.total_points_earned = ReceiptTransaction.calculate_offer_points_earned_per_day(self.receipt,self, self.total_points_earned, self.issue_date)
    end

    p self.total_points_earned.to_f
  end

  def update_rule_not_met
    if !self.offer.blank? and !self.offer.offer_rules.active.blank?
      begin
        off_rule = OfferRuleCalculation::CalculateOfferRule.new(self.offer, self)
        off_rule.result
        tmp_total_points_earned = off_rule.total_points_get
        if tmp_total_points_earned.to_f == 0.0
          self.update_column(:status, Receipt::STATUS[:REJECTED])
          self.receipt.update_column(:status, Receipt::STATUS[:REJECTED])
          self.update_column(:instructions, [:offer_rules_not_met => "This receipt does not met any rules"])
        end
      rescue => e
        puts "CALCULATE::OFFERRULES ----> #{e.inspect}"

      end
    end
  end



  def update_user_points
    if !self.receipt_id.blank?
      user = self.receipt.user
      if (self.status.to_i==Receipt::STATUS[:APPROVED])
        #perk_and_calculate_points(self.receipt.user)
        #self.receipt.user.points += self.total_points_earned.to_i
        #user.update_attribute(:points, self.receipt.user.points)
        puts "yyyyy #{self.total_points_earned}"
        chain = self.receipt.chain
        if chain
          point_sent =  self.total_points_earned.send(Chain::ROUNDING_OPTIONS[chain.point_rounding_preference])
        else
          point_sent =  self.total_points_earned
        end

        send_pn = nil
        send_pn = "OLO" if self.receipt.is_olo == true
        puts "oloreceipttransactions #{send_pn}"

        user.earn(point_sent, self.receipt, send_pn)
        puts "oloreceipttransactions #{send_pn}"
        if receipt.is_receipt_barcode
          description =  "Receipt ##{self.receipt.id} barcode scanned."
        elsif receipt.is_olo
          description =  "Receipt Olo ##{self.receipt.id} approved."
        elsif receipt.is_online_order
          description =  "Online Order ##{self.receipt.id} approved."
        else
          description =  "Receipt ##{self.receipt.id} Approved"
        end  
        
        #description += "( edited receipt )" if self.receipt.receipt_transactions.where(:status => Receipt::STATUS[:APPROVED]).reload.count > 1
        PointHistory.add_to_history(user.id, description, point_sent)
        #self.update_column(:total_points_earned, self.total_points_earned)
      end
    end
  end

  def perk_and_calculate_points#(user)
    p "perk_and_calculate_points executed"
                               ## if receipt approved
    if is_approved?
      user = self.receipt.user
      wallets = user.perk_wallets.active

      ## if there are perk on user wallets
      unless wallets.blank?
        perk_ids = wallets.map(&:perk_id)
        #perk_ids = PerkRestaurant.where("perk_id in(?) and restaurant_id = ?", perk_ids, self.restaurant_id).map(&:perk_id)

        unless perk_ids.blank?
          perks = Perk.where("id in (?)", perk_ids)
          calculate_all_perks_points_earned(perks)
        end
      else
        ## calculate based on offer
        calculate_total_points_earned
      end
    end
  end

  def is_approved?
    self.status == Receipt::STATUS[:APPROVED]
  end

  def is_received?
    self.status == Receipt::STATUS[:RECEIVED]
  end

  #  def calculate_perk_points(perks)
  #    perks.each do |perk|
  #      ## calculation perks#
  #      #self.total_points_earned += perk.boost(self.total_points_earned)
  #      dolar_spent = self.subtotal * self.receipt.chain.points
  #      offer_multiplier = self.offer.multiplier # get offer multiplier
  #
  #      if perk.perk_type == Perk::TYPE[:BONUS] and perk.stackable == false
  #        if perk.bonus_threshold < (dolar_spent * offer_multiplier)
  #          self.total_points_earned += perk.bonus_points
  #        end
  #      elsif perk.perk_type == Perk::TYPE[:MULTIPLY] and perk.stackable == false
  #        self.total_points_earned += dolar_spent * offer_multiplier * (1 + perk.multiplier)
  #      elsif perk.stackable == true
  #        if perk.bonus_threshold < (dolar_spent * offer_multiplier)
  #          self.total_points_earned += (dolar_spent * offer_multiplier * (1 + perk.multiplier)) + perk.bonus_points
  #        end
  #      end
  #    end
  #  end

  ## override for online order
  def offer
    if self.receipt and (self.receipt.is_online_order == true or self.receipt.is_olo == true)
      return Offer.unscoped.joins(:restaurant_offers).where("restaurant_offers.id = #{self.restaurant_offer_id}").first rescue nil
    else
      return RestaurantOffer.try(:find, self.restaurant_offer_id).try(:offer) rescue nil
    end
  end

  def calculate_all_perks_points_earned(perks)
    p "calculate_all_perks_points_earned-----------"
    ## fill on the array, all the perks points and then add them to total points earned
    chain = self.receipt.chain rescue nil
    dolar_spent = self.subtotal * self.receipt.chain.points
    if self.offer.is_multiplier
      offer_multiplier = self.offer.multiplier_constant_value # get offer multiplier
      base_points_earned = (dolar_spent * offer_multiplier)
    else
      offer_multiplier = self.offer.multiplier_constant_value # get offer multiplier
      base_points_earned = offer_multiplier
    end

    result_perk_point = []
    perks.each do |perk|
      if perk.perk_type == Perk::TYPE[:BONUS] #and perk.stackable == false
        if perk.bonus_threshold <= base_points_earned
          result_perk_point << perk.bonus_points
        end
      elsif perk.perk_type == Perk::TYPE[:MULTIPLY] #and perk.stackable == false        
        result_perk_point << base_points_earned * perk.multiplier
      end
    end
    points_get = result_perk_point.compact.inject{|sum,x| sum + x }.to_f
    self.bonus_earned = points_get rescue 0
    temp_total_points_earned = base_points_earned +  points_get
    if chain
      self.total_points_earned = temp_total_points_earned.send(Chain::ROUNDING_OPTIONS[chain.point_rounding_preference])
    else
      self.total_points_earned = temp_total_points_earned
    end

  end

  def second_last_transaction
    transactions = ReceiptTransaction.where("receipt_id=#{self.receipt_id} AND id < #{self.id}").order("id desc").limit(1)
    transactions[0] if transactions.length == 1
  end


  def self.migrate_average_chart_into_table
    Chain.all.each do |ch|
      res_ids = ch.restaurants.map(&:id)
      res_ids.each do |res_id|
        insert_into_average_chart_tables(res_id)
      end
    end
  end

  def self.insert_into_average_chart_tables(restaurant_id)
    AverageChartTable.delete_all(:restaurant_id => restaurant_id)
    receipts = Receipt.where("receipts.status = 3")
    res = []; result = []
    receipts.each do |rc|
      l_trans = rc.last_transaction
      count =  l_trans.subtotal.to_f rescue 0
      count = count.nil?? 0 : count

      begin
        res <<  {"id" => l_trans.id, "date" => l_trans.issue_date.nil? ? l_trans.updated_at.to_s : l_trans.issue_date,
                 "total" => count}
      rescue
        next
      end
    end

    date_list = res.map{|x| x["date"].to_date}.uniq.compact.sort
    date_list.each do |list|
      total = 0
      index = 0
      count_receipt = 0

      res.each_with_index do |r,m|
        if r["date"].to_date == list
          total = r["total"] + total
          index += 1
          count_receipt = count_receipt + 1
        end
      end
      tmp_avg = (total.to_f / index).round(2)
      #result << [list.to_datetime.to_i * 1000, tmp_avg] if list && tmp_avg > 0.0
      if list && tmp_avg > 0.0
        AverageChartTable.create(
            :receipt_date => list.to_date, :average => tmp_avg,
            :chain_id => Restaurant.find(restaurant_id).chain.id,
            :restaurant_id => restaurant_id, :receipt_count => count_receipt
        )

      end
    end
  end

  def reviewer_validate?
    self.status.eql?(Receipt::STATUS[:APPROVED]) && self.reviewer_validation.eql?(true)
  end

  def save_line_items
    p "insert line item into mongodb"
    receipt = self.receipt
    self.line_items.each do |line_item|
      line_item = ReceiptLineItem.new(line_item)
      line_item.receipt_id = receipt.id
      line_item.chain_id = receipt.chain_id
      line_item.receipt_date = self.issue_date
      line_item.receipt_status = self.status
      line_item.user_id = receipt.user_id
      line_item.restaurant_id = self.restaurant_id
      line_item.save
    end unless self.line_items.blank?
    p "------------end save line item into mongodb -----------------------"
  end

end
