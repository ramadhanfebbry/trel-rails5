class OwnerjobLog < ActiveRecord::Base
  belongs_to :owner_job


  STATUS = {"SUCCESS" => 1, "FAILED" => 2}
  NOTIFICATION_STATUS = {"SUCCESS" => 1, "FAILED" => 2}
  scope :success , where(:status => 1)
  scope :failed , where(:status => 2)
end
