class FishbowlPromotionSetting < ActiveRecord::Base

  attr_accessor :reward_code_generate_type

  belongs_to :chain

  validates :password, :username, :client_id, :client_secret, :brand_id, :length => {:maximum => 200}, :presence => true,
      :if => :fishbowl_promotion_coupon?

  def fishbowl_promotion_coupon?
    p "-----"*10
    p self.reward_code_generate_type
    self.reward_code_generate_type.to_i == 3
  end

end
