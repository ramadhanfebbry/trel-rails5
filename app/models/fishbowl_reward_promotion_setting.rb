class FishbowlRewardPromotionSetting < ActiveRecord::Base

  belongs_to :reward

  validates :promotion_id, :length => {:maximum => 200}, :presence => true
  validates :code_batch_size, :numericality => {:less_than_or_equal_to => 10000, :greater_than_or_equal_to => 500}, :presence => true

end
