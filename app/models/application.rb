class Application < ActiveRecord::Base
  include AASM
  extend KeyUtil

  has_many :application_keys

  validates :chain_id, presence: true
  validates_uniqueness_of :chain_id, scope: :device_type
  belongs_to :chain

  default_scope { where("applications.deleted_at" => nil) }
  scope :active, -> { where("applications.status = 'active'") }
  scope :active_chain, -> { joins(:chain).where("chains.status = 'active'") }
  scope :by_device_type, -> (device) { where(:device_type => device) }

  scope :enabled, -> { where("status = 'active'") }


  aasm :column => :status do
    state :active, :initial => true
    state :inactive

    event :activate do
      transitions :to => :active, :from => :inactive
    end

    event :deactivate do
      transitions :to => :inactive, :from => :active
    end

  end
  def self.deleted
    self.unscoped.where('applications.deleted_at IS NOT NULL')
  end

  def rapns_app
    Rpush::Apns::App.where(:name => self.id.to_s).first
  end

  def self.getChain(appkey)
    app = getApplication(appkey)
    if app
      app.chain
    end
  end

  def self.getApplication(appkey)
    return Rails.cache.read("appkey_#{appkey}") unless Rails.cache.read("appkey_#{appkey}").blank?
    app_key = ApplicationKey.includes(:application => :chain).find_by_appkey(appkey)
    if app_key
      Rails.cache.write("appkey_#{appkey}", app_key.application)
      app_key.application
    else
      nil
    end
  end

  def self.generate_new_app_key()
    size = Setting.application.appkey_size
    chars = Setting.application.appkey_chars

    key = generate_random_string(size, chars)
    while getApplication(key)
      key = generate_random_string(size, chars)
    end
    key
  end

  def self.get_available_app_key(key)
    if Rails.cache.read("obj_appkey_#{key}").blank?
      app_key = ApplicationKey.where(:appkey => key).first
      Rails.cache.write("obj_appkey_#{key}", app_key)
    else
      app_key = Rails.cache.read("obj_appkey_#{key}")
    end if false
    app_key = ApplicationKey.where(:appkey => key).first

    valid = true
    status = ''
    notice_text = ''
    known_key = ''
    case app_key.status
    when APPLICATION_KEY_STATUS[:INACTIVE]
      valid = false
      status = 'inactive'
      notice_text = :app_is_currently_unavailable
    when APPLICATION_KEY_STATUS[:DISABLED]
      valid = false
      status = 'disable'
      notice_text = :app_is_no_longer_supported
    when APPLICATION_KEY_STATUS[:DISCONTINUED]
      valid = false
      status = 'discontinue'
      notice_text = :app_no_longer_available
    end
    return [valid, status, notice_text, known_key]
  end

  def self.get_the_latest_key(key)
    app_key = ApplicationKey.where(appkey: key).first
    status = false
    notice = ''
    known_key = ''
    unless app_key.blank?
      if app_key.is_newest
        status = true
      else
        the_latest = ApplicationKey.where(:application_id => app_key.application_id, :is_newest => true).first
        status = false
        notice = :newer_version_is_available
        known_key = the_latest.blank? ? '' : the_latest.appkey
      end
    end
    return [status, notice, known_key]
  end

  def self.get_the_latest_version(key)
    app_key = ApplicationKey.where(appkey: key).first
    newest_app_key = ApplicationKey.where(appkey: key, is_newest: true).first
    status = false
    notice = ''
    is_required = false
    app_version = nil

    if app_key
      app_version = app_key.version
      if newest_app_key
        new_app_version = newest_app_key.version
        status = true
        if(app_version.to_f < new_app_version.to_f)
          is_required = newest_app_key.application_key_detail.is_required
          notice = is_required ? 'There is new version of the app, In order to continue using this app you must update to the latest version.' : 'New app update is available'
        elsif(app_version.to_f >= new_app_version.to_f)
          notice = ''
        end
      else
        status = true
        notice = ''
      end
    else
      notice = nil
    end


    return [status, app_version, is_required, notice]
  end


end
