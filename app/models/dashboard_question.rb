class DashboardQuestion < ActiveRecord::Base
  belongs_to :chain
  has_many :dashboard_choices, :dependent => :destroy
  validates :text, :presence => true


  attr_accessor  :dashboard_choices_attributes  , :count_deleted
  validate :on => :create do
    check_choice_number
  end

  validate :check_choice, :on => :update


  accepts_nested_attributes_for :dashboard_choices,:reject_if => :all_blank, :allow_destroy => true
  validates_associated :dashboard_choices


  private

  def check_choice
    count_deleted = 0 if count_deleted.blank?
    a = dashboard_choices.size - count_deleted
    puts "count ============== #{a}"
    if a >= 2
      true
    else
      errors.add(:base, "You have to fill min 2 choices, max 6 choices, choice cannot be blank")
    end
  end

  def choice_count_valid?
    puts "dashboard_validation = #{dashboard_choices}"
    dashboard_choices.size >= 2 and  dashboard_choices.size <= 6
  end

  def check_choice_number
    unless choice_count_valid?
      errors.add(:base, "You have to fill min 2 choices, max 6 choices, choice cannot be blank")
    end
  end

  #def check_choice
  #  a = dashboard_choices.size - 1
  #  errors.add(:base, "You have to fill min 2 choices, max 6 choices") if a < 2
  #end
end
