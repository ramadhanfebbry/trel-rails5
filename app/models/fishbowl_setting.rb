class FishbowlSetting < ActiveRecord::Base

  validates :api_password, :api_username, :site_id, :presence => true

  belongs_to :fishbowl_log

  serialize :list_ids, Array
  validates_each :list_ids do |record, attr, value|
    problems = ''
    if value
      value.each do |item|
        problems = 'Please fill the value within all of the ListID fields' if item.blank? || item == ''
      end
    else
      problems = 'Please supply at least one ListID'
    end
    record.errors.add(:list_ids, problems) unless problems.empty?
  end
end
