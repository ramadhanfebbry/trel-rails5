class EmailMarketing < ActiveRecord::Base

  belongs_to :chain
  belongs_to :user
  belongs_to :restaurant

  STATUS = {
      "NEW"  => 1,
      "CHANGED" => 2,
      "FAILED_SYNC" => 3,
      "SYNCED" => 4,
      "REPEATED" => 5,
      "MARK OPT OUT" => 6

  }

  def self.import_from_users
    chains = Chain.where("mailchimp_sync IS TRUE")
    chains.each do |chain|
      import_to_email_marketing(chain)
    end
  end

  def self.add_as_subscribers(run_type = "Daily Sync")
    chains = Chain.where("mailchimp_sync IS TRUE")
    chains.each do |chain|
      ml = MailchimpLog.create(:chain_id => chain.id, :log_type => run_type, :status => "in queue")
      dl = Delayed::Job.enqueue(EmailMarketingJob.new(chain, ml))
    end
  end

  def self.import_to_email_marketing(chain)
    users = chain.users.select("email, id, chain_id, marketing_optin")
    users.each do |user|
      import_user_to_email_marketing(user)
    end
  end

  def self.import_user_to_email_marketing(user)
    rs = user.restaurant_users.select("id, restaurant_id, created_at").order("created_at DESC").first
    em = EmailMarketing.select("email_marketings.id, email_marketings.chain_id, email_marketings.user_id, email_marketings.email, email_marketings.leid, email_marketings.status, email_marketings.restaurant_id").where(:chain_id =>  user.chain_id, :user_id => user.id).first
    if em
      #unless user.marketing_optin
      #  return if em.status.eql?(EmailMarketing::STATUS["SYNCED"])
      #  em.update_column(:status, EmailMarketing::STATUS["MARK OPT OUT"])
      #  return if !user.chain.mailchimp_ignore_marketing_optin
      #end
      if (em.email != user.email && !em.leid.blank?) || em.status == EmailMarketing::STATUS["REPEATED"] || ( rs && em.restaurant_id != rs.restaurant_id)
        em.update_attributes(:status => EmailMarketing::STATUS["CHANGED"], :email => user.email, :restaurant_id => (rs.restaurant_id rescue nil))
      else
        em.update_attributes(:email => user.email)
      end
    else
      status = user.marketing_optin ? EmailMarketing::STATUS["NEW"] : EmailMarketing::STATUS["MARK OPT OUT"]
      em = EmailMarketing.new(:chain_id => user.chain_id, :user_id => user.id, :email => user.email, :status => status, :restaurant_id => (rs.restaurant_id rescue nil), :signup_date => (rs.created_at rescue nil))
      em.save
    end
  end

  def self.update_email_marketing_location(user, restaurant_id)
    return if user.blank?
    return if !user.marketing_optin
    if user.chain.mailchimp_sync
      return if !user.chain.mailchimp_ignore_marketing_optin
      p "---- update email marketing location ---"
      p restaurant_id
      em = EmailMarketing.where(:chain_id =>  user.chain_id, :user_id => user.id).first
      em.update_attributes(:status => EmailMarketing::STATUS["CHANGED"], :email => user.email, :restaurant_id => restaurant_id ) unless em.blank?
    end
  end


  def self.adding_process(chain, mailchimp_log)
    success_users = []
    failed_users = []
    #import_to_email_marketing(chain) #no need this again because everytimes user changed, this code already fired.
    default_email_marketing_status = [EmailMarketing::STATUS["NEW"], EmailMarketing::STATUS["CHANGED"], EmailMarketing::STATUS["FAILED_SYNC"]]
    default_email_marketing_status << EmailMarketing::STATUS["MARK OPT OUT"] if chain.mailchimp_ignore_marketing_optin
    #gb = Gibbon::API.new(chain.mailchimp_api_key, :timeout => 100000, :throws_exceptions =>  false)
    if chain.mailchimp_ignore_marketing_optin
      query = "email_marketings.status in (#{default_email_marketing_status.join(",")}) AND email_marketings.chain_id = #{chain.id}"
    else
      query = "email_marketings.status in (#{default_email_marketing_status.join(",")}) AND email_marketings.chain_id = #{chain.id} AND users.marketing_optin IS TRUE"
    end
    EmailMarketing.select("email_marketings.id, email_marketings.chain_id, email_marketings.user_id, email_marketings.email, email_marketings.leid, email_marketings.status, email_marketings.restaurant_id").includes(:user).where(query).order("user_id DESC").find_in_batches(:batch_size => 50) do |ems|
      batch_users = []
      ems.each do |em|
        birthday = Date.new(em.user.dob_year, em.user.dob_month, em.user.dob_day) rescue ""

        if em.status ==  EmailMarketing::STATUS["CHANGED"]
          unless em.leid.blank?
            batch_users << {:EMAIL => {:leid => em.leid}, :MERGE_VARS => {"new-email" => em.email, "FNAME" => EmailMarketing.valid_text_mailchimp(em.user.first_name), "LNAME" => EmailMarketing.valid_text_mailchimp(em.user.last_name), "USERID" => em.user_id, "RESTAURANT" => em.restaurant_id, "SIGNUPDATE" => em.user.created_at, "BIRTHDAY" => birthday}}
          else
            batch_users << {:EMAIL => {:email => em.email}, :MERGE_VARS => {"FNAME" => EmailMarketing.valid_text_mailchimp(em.user.first_name), "LNAME" => EmailMarketing.valid_text_mailchimp(em.user.last_name), "USERID" => em.user_id, "RESTAURANT" => em.restaurant_id, "SIGNUPDATE" => em.user.created_at, "BIRTHDAY" => birthday}}
          end
        else
          batch_users << {:EMAIL => {:email => em.email}, :MERGE_VARS => {"FNAME" => EmailMarketing.valid_text_mailchimp(em.user.first_name), "LNAME" => EmailMarketing.valid_text_mailchimp(em.user.last_name), "USERID" => em.user_id, "RESTAURANT" => em.restaurant_id, "SIGNUPDATE" => em.user.created_at, "BIRTHDAY" => birthday}}
        end
      end
      gb = Gibbon::API.new(chain.mailchimp_api_key, :timeout => 100000, :throws_exceptions =>  false)
      mailchimp_result = gb.lists.batch_subscribe(:id => chain.mailchimp_list_id, :batch => batch_users , :update_existing => true, :double_optin => false)
      p mailchimp_result
      if mailchimp_result["status"] == "error"
        mailchimp_log.update_attributes(
            :success_users => success_users,
            :failed_users => failed_users,
            :status =>  "failed",
            :completed_at => Time.now,
            :error_report => mailchimp_result["error"]
        )
        return
      else
        result_added = mailchimp_result["adds"]
        result_updated = mailchimp_result["updates"]
        result_error = mailchimp_result["errors"]

        result_added.each do |added|
          em = EmailMarketing.select("email_marketings.id, email_marketings.chain_id, email_marketings.user_id, email_marketings.email, email_marketings.leid, email_marketings.status, email_marketings.restaurant_id").where(:email => added["email"], :chain_id => chain.id).order("user_id DESC").first
          if em
            em.update_attributes(:status => EmailMarketing::STATUS["SYNCED"], :leid => added["leid"])
            success_users << em.user_id
          end
        end unless result_added.blank?

        updated_member_info = []
        result_updated.each do |updated|
          updated_member_info << {:email => updated["email"]}
        end unless result_updated.blank?

        updated_result_info = {}
        unless updated_member_info.blank?
          updated_member_info.each_slice(35).each do |batch|
            infos = gb.lists.member_info({:id => chain.mailchimp_list_id, :emails => batch})
            infos["data"].each do |info|
              updated_result_info[info["merges"]["EMAIL"]] = {}
              updated_result_info[info["merges"]["EMAIL"]]["USERID"] = info["merges"]["USERID"]
              updated_result_info[info["merges"]["EMAIL"]]["leid"] = info["leid"]
            end unless infos["data"].blank?
          end
        end
        p updated_result_info
        unless updated_result_info.blank?
          em_results = EmailMarketing.select("email_marketings.id, email_marketings.chain_id, email_marketings.user_id, email_marketings.email, email_marketings.leid, email_marketings.status, email_marketings.restaurant_id").where(:email => updated_result_info.keys, :chain_id => chain.id).order("user_id DESC")
          p em_results
          em_results.each do |em|
            p updated_result_info[em.email]["USERID"].to_s
            p em.user_id.to_s
            if updated_result_info[em.email]["USERID"].to_s == em.user_id.to_s
              em.update_attributes(:status => EmailMarketing::STATUS["SYNCED"], :leid => updated_result_info[em.email]["leid"])
              success_users << em.user_id
            else
              em.update_attributes(:status => EmailMarketing::STATUS["REPEATED"], :leid => nil) unless em.blank?
            end
          end
        end


        result_error.each do |error|
          base_conditions = ["chain_id = #{chain.id}"]
          conditions = nil
          conditions = "email = '#{error["email"]["email"]}'" unless error["email"]["email"].blank?
          conditions = "leid = '#{error["email"]["leid"]}'" unless error["email"]["leid"].blank?
          unless conditions.blank?
            base_conditions << conditions
            em = EmailMarketing.select("email_marketings.id, email_marketings.chain_id, email_marketings.user_id, email_marketings.email, email_marketings.leid, email_marketings.status, email_marketings.restaurant_id").where(base_conditions.join(" AND ")).first
            em.update_attributes(:status => EmailMarketing::STATUS["FAILED_SYNC"]) unless em.blank?
            failed_users << em.user_id  unless em.blank?
          end
        end unless result_error.blank?
      end

    end
    mailchimp_log.update_attributes(
        :success_users => success_users,
        :failed_users => failed_users,
        :status => "completed",
        :completed_at => Time.now
    )

  end

  def self.valid_text_mailchimp(text_m)
    return nil if text_m.blank?
    a = text_m.match(/^[A-Za-z0-9.&]*\z/)
    if a.blank?
      return nil
    else
      return text_m
    end
  end


  def status_in_color
    color = case self.status
              when 1
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:blue'></div> &nbsp;&nbsp;&nbsp;#{EmailMarketing::STATUS.invert[1]}"
              when 2
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:yellow'></div> &nbsp;&nbsp;&nbsp;#{EmailMarketing::STATUS.invert[2]}"
              when 3
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:red'></div> &nbsp;&nbsp;&nbsp;#{EmailMarketing::STATUS.invert[3]}"
              when 4
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:green'></div> &nbsp;&nbsp;&nbsp;#{EmailMarketing::STATUS.invert[4]}"
              when 5
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:purple'></div> &nbsp;&nbsp;&nbsp;#{EmailMarketing::STATUS.invert[5]}"
              when 6
                "<div style=' width: 20px;height: 20px;border-radius: 50%;display: inline-block;background-color:black'></div> &nbsp;&nbsp;&nbsp;#{EmailMarketing::STATUS.invert[6]}"
            end
  end

  def unsubscribe_member
    user = em.user
    chain = em.chain
    user_struct = {:email => em.email, :leid => em.leid}
    gb = Gibbon::API.new(chain.mailchimp_api_key, :timeout => 100000, :throws_exceptions =>  false)
    mailchimp_result = gb.lists.unsubscribe(:id => chain.mailchimp_list_id, :email => user_struct, :delete_member => true)
  end

  def member_info
    user = em.user
    chain = em.chain
    user_struct = [{:email => em.email, :leid => em.leid}]
    gb = Gibbon::API.new(chain.mailchimp_api_key, :timeout => 100000, :throws_exceptions =>  false)
    gb.lists.member_info(:id => chain.mailchimp_list_id, :emails => user_struct)
  end

end