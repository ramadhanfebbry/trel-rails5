class PromotionOffer < ActiveRecord::Base
  has_many :promotions, :as => :promotable, :dependent => :destroy
  belongs_to :chain

  validates :points, :numericality => {:greater_than => 0}, :presence => true
  validates :title, :presence => true
end
