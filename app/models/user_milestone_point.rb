class UserMilestonePoint < ActiveRecord::Base

  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
end
