class OwnerScheduler < ActiveRecord::Base
  belongs_to :owner_job, :foreign_key => 'job_id'


  scope :run_today, -> { where("date(owner_schedulers.executed_at)
       = date(now())
      and schedule_type != 1 and
      (owner_jobs.status=2 or owner_jobs.status=3) and date(now()) >=
      date(start_at)  and
      date(now())  <= date(end_at)").
      joins(:owner_job)
    }

  scope :run_daily, -> { where("date(owner_schedulers.executed_at)
       <= date(now())
      and schedule_type != 1 and
      (owner_jobs.status=2 or owner_jobs.status=3 or owner_jobs.status=0) and date(now()) >=
      date(start_at)  and
      date(now())  <= date(end_at)").
      joins(:owner_job)
    }

  scope :admin_only, -> { where("owner_jobs.owner_id IS NULL") }

  def self.set_scheduler(params)
    owner_schedule_build = {}

    case params["owner_job"]["schedule_type"].to_i
      when 1 # onte time
        hour = params["daily"]['(4i)'].to_i
        min = params["daily"]['(5i)'].to_i
        time_to_run = Time.zone.now.change(:hour => hour, :min => min)

        owner_schedule_build = {
            :run_type => 'onetime',
            :executed_at => time_to_run,
            :next_run => time_to_run + 1.days,
        }
      when 2 # daily
        hour = params["daily"]['(4i)'].to_i
        min = params["daily"]['(5i)'].to_i
        time_to_run = Time.zone.now.change(:hour => hour, :min => min)

        owner_schedule_build = {
            :run_type => 'daily',
            :executed_at => time_to_run,
            :next_run => time_to_run + 1.days,
        }
      when 3 # weekly
        hour = params["time_stamp_weekly"]['(4i)'].to_i
        min = params["time_stamp_weekly"]['(5i)'].to_i
        day = params["weekly"].to_i
        owner_schedule_build = {
            :run_type => 'weekly',
            :executed_at => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days).change(:hour => hour, :min => min),
            :next_run => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days) + 1.weeks
        }
      when 4 # monthly
        hour = params["time_stamp_montly"]['(4i)'].to_i
        min = params["time_stamp_montly"]['(5i)'].to_i
        day = params["monthly"].to_i

        week_day = Time.zone.now.beginning_of_month.wday
        days_params = {}
        diff = day - week_day
        if day >= week_day and Time.zone.now.beginning_of_month >= Time.zone.now
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + diff.days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + diff.days)..change(:hour => hour, :min => min)
          }

        elsif day < week_day and Time.zone.now.beginning_of_month.to_date < Time.zone.now.to_date
          next_wday = (Time.zone.now.beginning_of_month + 1.months).wday
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + ((7 - week_day) + day).days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + ((7 - next_wday) + day).days).change(:hour => hour, :min => min),
          }
        else
          next_wday = (Time.zone.now.beginning_of_month + 1.months).wday
          days_params = {
              :executed_at => (Time.zone.now.beginning_of_month + ((7 - week_day) + day).days).change(:hour => hour, :min => min),
              :next_run => ((Time.zone.now.beginning_of_month + 1.months) + ((7 - next_wday) + day).days).change(:hour => hour, :min => min),
          }
        end

        #start_month =
        owner_schedule_build = {
            :run_type => 'monthly' #,
            # :executed_at => (Time.zone.now.beginning_of_week.in_time_zone + (day.to_i - 1).days).change(:hour => hour, :min => min),
            # :next_run => (Time.zone.now.beginning_of_month.in_time_zone + (30.days)).change(:hour => hour, :min => min),
        }.merge!(days_params)
    end
    os = OwnerScheduler.new(owner_schedule_build.merge!(:start_at => params["start_date"], :end_at => params["end_date"]))
  end

  def monthly_selection
    beginning = Time.zone.now.beginning_of_month
  end

  ## OwnerScheduler.running_owner_scheduler
  def self.running_owner_scheduler
    testing = REDIS.get("testing_owner_job")
    if testing.blank? ## for testing purpose
      running_tasks = OwnerScheduler.run_today
      running_tasks = running_tasks + OwnerScheduler.run_daily
    else
      running_tasks = OwnerScheduler.where("schedule_type != 1 and
      (owner_jobs.status=2 or owner_jobs.status=3 or owner_jobs.status=0) and date(now()) >=
      date(start_at)  and
      date(now())  <= date(end_at)").
          joins(:owner_job)
    end
    unless running_tasks.blank?
      aloha_chains = Chain.active.select('id').where(:connect_to_aloha_program => true)
      running_tasks.each do |task|
        aloha_chain_id = task.owner_job.chain.id rescue nil

        # if in aloha chain next it
        if aloha_chains.map(&:id).include?(aloha_chain_id)
          next
        end

        begin
          #if dj_owner.first.blank?
            task.owner_job.execute_jobs if task.is_time_to_running == true or !testing.blank?

            if testing.blank?  ## if its live, update the column, if test , do not update the column
              n_run = case task.run_type.downcase
                        when "monthly"
                          task.executed_at + 1.month
                        when "weekly"
                          task.executed_at + 1.week
                        when "daily"
                          task.executed_at + 1.day
                      end
              task.update_column(:last_executed, Time.zone.now)
              task.update_column(:next_run, n_run)
              task.update_column(:executed_at, n_run)
            end
          #end
        rescue => e
          puts "OWNERJOBSCHEDULER :: RUNNING_OWNER_SCHEDULER => #{e.inspect}"
        end
      end
    end
  end

  def is_time_to_running
    o = self

    return true if o.owner_job and o.owner_job.zone.blank?
    unless o.executed_at.blank?
      exact_hour = o.executed_at.strftime("%H")
      zone_tmp = Time.zone.now.in_time_zone(o.owner_job.zone) rescue Time.zone.now.in_time_zone('EST')
      zone_hour = zone_tmp.hour

      if exact_hour.to_i == zone_hour # same with current time zone
        return true
      else
        return false
      end
    end
  end

  def self.set_expired
    OwnerScheduler.
        where("schedule_type != 1").
        joins(:owner_job).each do |ow|

      if ow.expired?
        puts ow.id
        ow.owner_job.update_column(:status, 3)
      end
    end
  end

  def expired?
    self.end_at < Date.today  if self.end_at
  end
end
