class UserActivityNotificationJob < Struct.new(:chain_id, :device_token, :notification, :device_type)

  def perform
    chain = Chain.find chain_id
    if device_type == "android"
      PushNotificationData::PointNotification.push_to_android(chain, [device_token], notification)
    elsif device_type == "iphone"
      if application = chain.applications.by_device_type(2).first
        rapns_app = application.rapns_app
        return if rapns_app.blank?
        n = Rpush::Apns::Notification.new
        n.app = rapns_app
        n.device_token = device_token
        n.alert = notification

        n.sound = "1.aiff"
        n.badge = 1
        n.save
      end
    end
  end

end