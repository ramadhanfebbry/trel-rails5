class OwnerJobHistory < ActiveRecord::Base
  belongs_to :owner_job

  serialize :success
  serialize :failed


  def move_to_logs
    #OwnerjobLog.delete_all
    total = OwnerJobHistory.count
    page = 10
    total_pages = OwnerJobHistory.paginate(:page => 1, :per_page => 10).total_pages

    1.upto(total_pages) do |i|
      histories = OwnerJobHistory.paginate(:page => i, :per_page => 10)
      puts "EO"
      histories.each do |h|
        total_success_pages = h.success.paginate(:page => 1, :per_page => 100).total_pages
        puts "EA"
        1.upto(total_success_pages) do |j|

          success_pages = h.success.paginate(:page => j, :per_page => 100)

          success_pages.each do |sc|
            begin
              a = sc[:date_sent].split('/')
              puts "ea"
              date = Date.new((("20"+a[2]).to_i), a[1].to_i, a[0].to_i)
              puts "eror"
              log = OwnerjobLog.new(:user_id => sc[:id],
                                    :email => sc[:email],
                                    :date_sent => date,
                                    :notification_status => (sc[:success_notif] == "TRUE" ? 1 : 2),
                                    :owner_job_id => h.owner_job_id,
                                    :status => OwnerjobLog::STATUS["SUCCESS"]
              )

              log.save
            rescue
              next
            end
          end
        end
      end
    end
  end

  #def self.move_to_logs_sql
  def perform
    #OwnerjobLog.delete_all
    total = OwnerJobHistory.count
    page = 10
    total_pages = OwnerJobHistory.paginate(:page => 1, :per_page => 10).total_pages

    1.upto(total_pages) do |i|
      histories = OwnerJobHistory.paginate(:page => i, :per_page => 10)
      histories.each do |his|
        inserts = []

        his.success.each do |sc|
          begin
            suc_notice = sc[:success_notif] == 'TRUE' ? 1 : 2
            a = sc[:date_sent].split('/')
            status = OwnerjobLog::STATUS["SUCCESS"]

            if a[0].size > 2 || a[1].size > 2  || a[2].size > 2
              date = DateTime.parse(sc[:date_sent])
            else
              date = Date.new(("20"+a[2]).to_i, a[0].to_i, a[1].to_i)
            end



            inserts.push "(#{sc[:id]}, '#{sc[:email]}', '#{date}', #{suc_notice},#{his.owner_job.id},#{status})"
            puts inserts.size
          rescue   => e
            puts "#{e.inspect}"
            next
          end
        end
        if inserts.size > 0
        sql = "INSERT INTO ownerjob_logs (user_id, email, date_sent, notification_status, owner_job_id,
              status )
               VALUES #{inserts.join(", ")}"
        Receipt.find_by_sql(sql)
          end
      end
    end
  end

  def update_owner_job_id
    x = OwnerjobLog.select("distinct(owner_job_id)")
    a = []
    x.each do |k|
      begin
      m = OwnerJobHistory.where(:id => k.owner_job_id).select('owner_job_id').first.owner_job_id
      #puts "owner_job_id = #{m} owner_job_id = #{k.owner_job_id}"
      OwnerjobLog.update_all("owner_job_id = #{m}","owner_job_id = #{k.owner_job_id}")
        puts m
      rescue
        next
        end
    end
    puts a.flatten
  end
end
