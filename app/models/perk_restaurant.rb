class PerkRestaurant < ActiveRecord::Base
  self.table_name = :perks_restaurants
  belongs_to :perk
  belongs_to :restaurant

  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  

end
