class LdapJob < Struct.new(:chain, :owner, :password)

  def perform
    puts "perform LdapJob"
    ldap = Ldap.new
    ldap.create_new_owner(chain, owner, password)
  end

end