class RaffleJob < Struct.new(:user_ids, :raffle_id, :raffle_step_id)

  def perform
    puts "perform raffle job"
    group = Time.zone.now.to_i
    raffle = Raffle.where("id = ? AND start_date <= ? AND end_date >= ? AND active IS TRUE", raffle_id, Time.zone.now, Time.zone.now).first rescue nil
    chain = raffle.chain rescue nil
    raffle_step = raffle.raffle_steps.find(raffle_step_id) rescue nil
    user_ids.each do |user_id|
      success = true
      message = ""

      if success && raffle.blank?
        success = false
        message = "We are sorry! Looks like something went wrong. Please try again later."
      end

      if success && (raffle_step.blank? || raffle_step.active == false)
        success = false
        message = "We are sorry! Looks like something went wrong. Please try again later."
      end

      user = chain.users.find(user_id) rescue nil
      if success && user.blank?
        success = false
        message = "Could not find user associated."
      end

      if success
        program = raffle.raffle_user_informations.where("user_id = ?", user.id).first rescue nil
        if program.blank?
          program = RaffleUserInformation.create(:raffle_id => raffle.id, :user_id => user.id, :user_email => user.email)
        end

        participation = program.raffle_participation_histories.where("raffle_step_id = ?", raffle_step.id) rescue nil
        if !participation.blank?
          success = false
          message = "Looks like you have already completed this step."
        else
          participation = program.raffle_participation_histories.build(:raffle_step_id => raffle_step.id)
          is_saved = participation.save
          unless is_saved
            success = false
            message = "We are sorry! Looks like something went wrong. Please try again later."
          end
        end
      end
      RaffleManualHistory.create(:group => group, :submitted_user_id => user_id, :user_id => user.try(:id), :chain_id => raffle.try(:chain_id), :raffle_id => raffle.try(:id), :raffle_step_id => raffle_step.try(:id), :success => success, :message => message)
    end
    puts "performed raffle"
  end

end
