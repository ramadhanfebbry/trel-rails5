class RestaurantCategory < ActiveRecord::Base

  belongs_to :restaurant
  has_many :partner_sub_category_menu_items

end
