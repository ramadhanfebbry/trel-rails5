class RewardWalletPushNotificationJob < Struct.new(:params, :set_run_at, :exp_at)

  def perform
    RewardWallet.send_pn_to_user(params,set_run_at, exp_at)
  end

end