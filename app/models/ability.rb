class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #puts "#{user.class}"
    #puts "ABILITY >RB"
    user ||= Admin.new(:role => nil) # guest user (not logged in)

    ############### ADMIN ROLE AND ABILITY ######################################
    if user.class == Admin
      case user.role
        when ADMIN_ROLES[:ADMIN]
          can :manage, :all
        #can :manage, [Application, Chain, Restaurant, Offer, Reward, Receipt, Owner,
        #        User, Survey, Country, Region, City, Answer, SurveysUser, Admin, Question, QuestionChoice]
        when ADMIN_ROLES[:REVIEWER]
          can :edit, Receipt
          can :show, Receipt
          can :update, Receipt
          can :index, Receipt
          can :url, Receipt
          can :update, Admin, :id => user.id
          can :read, Admin, :id => user.id
        when ADMIN_ROLES[:BANNED]
          cannot :all
        when ADMIN_ROLES[:TESTER]
          can :test_locate, :location
          can :test_claim, :claim
          can :test_answer_survey, :answer_survey
          can :test_submit_receipt, :submit_receipt
          can :getOfferRestaurants, :offer_restaurant
          can :test_promocode, :promocode
        when ADMIN_ROLES[:CS]
          can :manage, Receipt
          can :manage, PushPoint
          can :manage, ReferralProgram
          can :manage, ChainImageShare
          can :manage, PushReward
          can :push_reward, Reward
          can :manage, User
          can :chain_share_images, Chain
          can :update, Admin, :id => user.id
          can :read, Admin, :id => user.id
      end

    ############### Owner ROLE AND ABILITY ######################################
    elsif user.class == Owner

    end

    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
