class PartnerReward < ActiveRecord::Base

  belongs_to :reward

  has_many :partner_hours
  has_many :partner_reward_categories
  has_many :partner_reward_discounts
  has_many :partner_categories, :through => :partner_reward_categories

end
