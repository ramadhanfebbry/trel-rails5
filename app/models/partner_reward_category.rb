class PartnerRewardCategory < ActiveRecord::Base

  belongs_to :reward
  belongs_to :partner_category, :foreign_key => 'category_id'

end
