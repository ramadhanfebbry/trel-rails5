class ActivitySummary < ActiveRecord::Base
  #ActivitySummary.execute_first
  def self.execute_first
    self.generate_first_data
  end

  #RewardSummary.generate_first_data
  def self.generate_first_data
    ActivitySummary.delete_all
    Chain.select('id,created_at').all.each do |ch|
      first_chain_created = ch.created_at
      diff = ((Time.zone.now - first_chain_created).to_i / 1.days) + 1

      ch.restaurants.each do |rs|
        ## loop each restaurants
        0.upto(diff).each do |day|
          cnt = Receipt.new_dashboard_receipt_activity(rs.id, (first_chain_created + day.days)).count(distinct: true)
          #puts first_chain_created + day.days
          self.create(:transaction_date => (first_chain_created + day.to_i.days).to_date,
                      :total => cnt.to_i, :restaurant_id => rs.id, :chain_id => ch.id
          ) if cnt.to_i > 0
        end
      end
    end
  end

  #ActivitySummary.update_data
  def self.update_data(r_trans)
    date_input = r_trans.receipt.get_date
    cnt = Receipt.receipt_date_dashboard_receipt_activity(r_trans.restaurant_id, date_input).count(distinct: true)
    data = self.where("restaurant_id = ? and transaction_date = date(?)", restaurant_id, date_input).first
    chain_id = Restaurant.find(restaurant_id).chain.id
    if data.blank?
      self.create(:transaction_date => date_input,
                  :total => 1, :restaurant_id => restaurant_id, :chain_id => chain_id)
    else
      data.update_attributes(:total => cnt)
    end
  end


  def self.update_count_by_transaction(r_trans)
    receipt  = r_trans.receipt
    date_input = receipt.get_date
    restaurant_id = r_trans.restaurant_id
    chain_id = receipt.chain_id

    data = self.where("restaurant_id = ? and transaction_date = date(?)", restaurant_id, date_input).first
    if data.blank?
      self.create(:transaction_date => date_input,
                  :total => 1, :restaurant_id => restaurant_id, :chain_id => chain_id)
    else
      cnt = data.total + 1
      data.update_attributes(:total => cnt)
    end
  end

  ## ActivitySummary.schedule_daily
  ## for prepopulate the receipt activity per restaurant
  def self.scheduler_daily
    Chain.all.each do |ch|
      ActivitySummary.scheduler_daily_by_chain(ch,2)
    end
    #Chain.select('id,created_at').all.each do |ch|
    #  diff = 11 ## 11 days before
    #
    #  ch.restaurants.each do |rs|
    #    ## loop each restaurants
    #    diff.downto(0).each do |day|
    #      # check if its images / barcodes
    #      begin
    #        rc = ReceiptTransaction.where(:restaurant_id => rs.id).last.receipt
    #        if rc.is_receipt_image?
    #          receipt_images = true
    #        else
    #          receipt_images = false
    #        end
    #      rescue
    #        receipt_images = false
    #      end
    #      if receipt_images == false
    #        cnt = Receipt.receipt_date_dashboard_receipt_activity(rs.id, (Time.zone.now - day.days)).count(distinct: true)
    #        data = self.where(:transaction_date => (Time.zone.now - day.days).to_date,
    #                        :restaurant_id => rs.id, :chain_id => ch.id
    #        ).first
    #      else
    #        cnt = Receipt.new_dashboard_receipt_activity(rs.id, (Time.zone.now - day.days)).count(distinct: true)
    #        data = self.where(:transaction_date => (Time.zone.now - day.days).to_date,
    #                          :restaurant_id => rs.id, :chain_id => ch.id
    #        ).first
    #
    #      end
    #
    #
    #      if data.blank?
    #        if cnt.to_i > 0
    #          self.create(:transaction_date => (Time.zone.now - day.days).to_date,
    #                    :total => cnt.to_i, :restaurant_id => rs.id, :chain_id => ch.id
    #        )
    #        end
    #      else
    #        self.where("transaction_date = '#{(Time.zone.now - day.days).to_date}' AND
    #                              restaurant_id = #{rs.id} AND chain_id = #{ch.id} AND id != #{data.id}").delete_all
    #        data.update_attributes(:total => cnt.to_i)
    #      end
    #    end
    #  end
    #end
  end

  def self.scheduler_daily_by_chain(ch, diff = nil)
      #diff = 2 ## 11 days before

      diff = 3 if diff.blank?## 11 days before
      distinct_transaction = "DISTINCT on (receipt_transactions.receipt_id) receipts.*"

      case_coalesce_receipt = "date(coalesce(receipt_date, issue_date))"
      date_coalesce = "date(TIMEZONE('UTC',#{case_coalesce_receipt})
   AT TIME ZONE 'EST')"


      ch.restaurants.each do |rs|
        ## loop each restaurants
        diff.downto(0).each do |day|
          #cnt = Receipt.receipt_date_dashboard_receipt_activity(rs.id, (Time.zone.now.utc - day.days)).count(distinct: true)
          #          cnt = Receipt.receipt_date_coalesce_dashboard_receipt_activity(rs.id, (Time.zone.now - day.days)).count(distinct: true)
          cnt = Receipt.select(distinct_transaction).joins(:receipt_transactions).where("receipt_transactions.restaurant_id = ?
and receipts.status = 3 and #{date_coalesce} = date(?)",rs.id,(Time.zone.now - day.days).to_date).count(distinct: true)
          data = ActivitySummary.where(:transaction_date => (Time.zone.now - day.days).to_date,
                                       :restaurant_id => rs.id, :chain_id => ch.id
          ).first

          if data.blank?
            if cnt.to_i > 0
              ActivitySummary.create(:transaction_date => (Time.zone.now - day.days).to_date,
                                     :total => cnt.to_i, :restaurant_id => rs.id, :chain_id => ch.id
              )
            end
          else
            ActivitySummary.where("transaction_date = '#{(Time.zone.now - day.days).to_date}' AND
                                  restaurant_id = #{rs.id} AND chain_id = #{ch.id} AND id != #{data.id}").delete_all
            data.update_attributes(:total => cnt.to_i)
          end
        end
      end
  end
end
