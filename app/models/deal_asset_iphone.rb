class DealAssetIphone < DealAsset
  
  has_attached_file :image, :styles => { :medium => "400x400>", :thumb => "150x150>" },
    :storage => :s3,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :path => "checkin/image_for_iphone/:style/:id/:filename"

end
