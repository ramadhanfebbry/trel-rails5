# encoding: utf-8
class Faqimage < ActiveRecord::Base

	belongs_to :chain

  has_attached_file :image, :styles => { :medium => "400x400>", :thumb => "150x150>" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :path => "checkin/image_for_android/:style/:id/:filename"

  validates_attachment_presence :image

end
