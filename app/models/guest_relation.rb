class GuestRelation < ActiveRecord::Base
  belongs_to :surveys_user
  belongs_to :owner

  STATUS = {
      1 => "waiting",
      2 => "approved",
      3 => "processing",
      4 => "completed",
      5 => "rejected"
  }

  scope :by_chain, lambda { |x|
    if (!x.blank?)
      where("chain_id = ? and surveys_users.id in (guest_relations.surveys_user_id)", x).includes(:surveys_user)
    end
  }

  validates :job_type, :presence => true
  validates :points,:numericality => { :greater_than => 0} , :if => Proc.new{|f| f.job_type == 2 } # if points method
  validate :check_blank

  def show_status
    "for now its not set yet"
  end

  def check_blank
    if job_type == 1
      errors.add(:reward_id, "cannot be blank") if self.reward_id.blank?
    else
      errors.add(:points, "cannot be blank") if self.points.blank?
    end
  end

  def execute
    user = self.surveys_user.user
    if job_type == 1
      begin
        tmp_expired_at = RewardWallet.get_expiry_date(Reward.find(self.reward_id))
        RewardWallet.create(:reward_id => self.reward_id,
                            :user_id => user.id,
                            :expiry_date => tmp_expired_at,
                            :description => "pushed from dashboard #{created_by_user}"
        )
      rescue => e
        tmp_expired_at = RewardWallet.get_expiry_date(Reward.find(self.reward_id))
        RewardWallet.create(:reward_id => self.reward_id,
                            :user_id => user.id,
                            :expiry_date => tmp_expired_at,
                            :description => "pushed from dashboard #{created_by_user}"
        )
        ## itshould work on trel. but this is the code when running on frel
      end
    else
     # begin
        ## itshould work on trel. but this is the code when running on frel
        user = self.surveys_user.user
        user.earn(points, nil, "Owner")
        sender_email = self.owner.email rescue '-'
        PointHistory.add_to_history(user.id, "Dashboard Guest Relation sent by: #{sender_email}", points)
      #rescue => e
      #  user = self.surveys_user.user
      #  user.earn(self.points)
      #end
    end
    puts "Guest Relation EXECUTED"
    su =  self.surveys_user
    su.update_column(:guest_count, su.guest_count - 1) if su.guest_count > 0
  end

  def created_by_user
    return "by #{self.owner.email}" rescue " owner email not found"
  end

  def show_reward_point_string
    if job_type == 1
      return "#{Reward.find(reward_id).name} <br /> Created:#{updated_at.strftime('%D')} <br/> Created by:#{Owner.find(owner_id).email} " rescue "-"
    else
      return "Push #{points} points <br /> Created:#{updated_at.strftime('%D')} <br/>Created by:#{Owner.find(owner_id).email} " rescue "-"
    end
  end

  def self.setup_guest_relation_features
    #### permissions for chain owner
    puts "---chain owner permission----"
    Owner.all.each do |ow|
      if ow.is_chain_owner?
        ow.update_column(:role, 'email_process')
      end
    end
    puts "---Done chain owner permission----"
    ##

    Chain.all.each do |ch|
      REDIS.set "guest_relation_setting_reward_#{ch.id}", "true"
      REDIS.set "guest_relation_setting_points_#{ch.id}", "true"
    end
  end


############## code for dashboard 2.3
#
## dashboard 2.3 admin/chains_controller.rb
#
#    def new_guest_relation_setting
#      @chain = Chain.find params[:id]
#      @cr = REDIS.get "guest_relation_setting_reward_#{@chain.id}"
#      @cr = !@cr.blank?
#      @pr = REDIS.get "guest_relation_setting_points_#{@chain.id}"
#      @pr = !@pr.blank?
#    end
#
#def update_guest_relation_setting
#  @chain= Chain.find params[:id]
#  if params[:val] == "ON"
#    REDIS.set "guest_relation_setting_#{params[:mode]}_#{@chain.id}", "true"
#  else
#    REDIS.set "guest_relation_setting_#{params[:mode]}_#{@chain.id}", nil
#  end
#  redirect_to new_guest_relation_setting_admin_chain_path(@chain)
#end


end
