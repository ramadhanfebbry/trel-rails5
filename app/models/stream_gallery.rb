class StreamGallery < ActiveRecord::Base

  belongs_to :chain

  validates :chain_id, :title, :presence => true
  validates :transition_duration, :numericality => {
      :greater_than_or_equal_to => 0,
      :less_than_or_equal_to => 20,
      :only_integer => true
  }

  validates :display_duration, :numericality => {
      :greater_than_or_equal_to => 1,
      :less_than_or_equal_to => 100,
      :only_integer => true
  }

  has_many :url_embeds, -> {where(:input_source => StreamGalleryImage::EMBED_SOURCE["URL"])}, :class_name => "StreamGalleryImage"
  has_many :xml_embeds, -> {where(:input_source => StreamGalleryImage::EMBED_SOURCE["XML FEED LINK"])}, :class_name => "StreamGalleryImage"
  has_many :json_embeds, -> {where(:input_source => StreamGalleryImage::EMBED_SOURCE["XML/JSON FILE UPLOAD"])}, :class_name => "StreamGalleryImage"
  has_many :image_embeds, -> {where(:input_source => StreamGalleryImage::EMBED_SOURCE["IMAGES AND METADATA UPLOAD"])}, :class_name => "StreamGalleryImage"
  has_many :stream_gallery_images

  accepts_nested_attributes_for :url_embeds
  accepts_nested_attributes_for :xml_embeds
  accepts_nested_attributes_for :json_embeds
  accepts_nested_attributes_for :image_embeds

  ANIMATION_TYPE = {
      "No animation" =>  0,
      "Slide" => 1,
      "Fade"  => 2
  }

end
