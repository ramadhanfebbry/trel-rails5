class PartnerMenuItemsRestaurant < ActiveRecord::Base

  belongs_to :restaurant
  belongs_to :partner_menu_item

end
