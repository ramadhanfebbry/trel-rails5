class RewardWallet < ActiveRecord::Base
  belongs_to :user
  belongs_to :reward

  after_create :set_expired_based_on_setting_expire_and_reward, :set_notification_reward

  STATUS = {ACTIVE: 1, REMOVED: 2, CLAIMED: 3, INACTIVE: 4, GIFTED: 5, REDEEMING: 6}

  default_scope { where(:deleted_at => nil) }

  scope :by_status , lambda {|x|
    unless x.blank?
      where("status in (?)", x)
    end
  }

  scope :twenty_day_expired, -> { where("date(expiry_date) < ?", (Time.zone.now.utc - 20.days).to_date) }

  scope :by_reward_ids,lambda {|reward_ids|
    unless reward_ids.blank?
      where("reward_id in (?)", reward_ids)
    end
  } 

  scope :is_claimed, where("status = ?",RewardWallet::STATUS[:CLAIMED])

  scope :by_user_id, lambda {|user_id|
    unless user_id.blank?
      where("user_id = ?", user_id)
    end
  }

  scope :expired_reward, where("date(expiry_date) < ? ", Time.zone.now.utc.to_date)
  scope :not_claimed, where("status != ?", RewardWallet::STATUS[:CLAIMED])

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def self.create_with_delay(params={}, donot_send_pn = false)
    p "********* PUSH REWARD *************************"
    reward_obj = Reward.find(params[:reward_id])
    params.merge!(:description => 'user activity') if params[:description].blank?
    puts params
    puts "parametersssssssssssssss"
    chain_obj = reward_obj.chain
    tmp_expired_at = RewardWallet.get_expiry_date(reward_obj)
    params.merge!(:expiry_date => tmp_expired_at)
    tmp_expired_at = tmp_expired_at.strftime("%m/%d/%Y")
    user = User.find(params[:user_id]) rescue nil

    ## check if there is any reward from olo orders ( do not send any email / push notification)
    ## check if its from 12pm until 6 am , because the scheduler are going out this way
    #if Time.zone.now.in_time_zone("EST").hour > 20 and Time.zone.now.in_time_zone("EST").hour < 6
      push_notif_olo = user.push_notif_olo rescue false
      if push_notif_olo == true
        donot_send_pn = true
      end
    #end
    ## end check

    ## see if reward active or not
    if !reward_obj.expired and reward_obj.status == "active"
      ## if not expired and reward is active
      if Reward::TYPES["REGULAR"] != reward_obj.reward_type and reward_obj.hours_delay.to_f > 0 and
          reward_obj.status == "active"
        Delayed::Job.enqueue(RewardUserDelayedJob.new(params),
          :delayable_type => reward_obj.class.to_s,
          :delayable_id => reward_obj.id,
          :chain_id => reward_obj.chain_id,
          :run_at => ((Time.zone.now >= reward_obj.effectiveDate ? Time.zone.now : reward_obj.effectiveDate) + reward_obj.hours_delay.hours)
        )
        Delayed::Job.enqueue(RewardWalletPushNotificationJob.new(params,true, tmp_expired_at)) unless donot_send_pn
      else
        p params
        if Time.zone.now >= reward_obj.effectiveDate
          Delayed::Job.enqueue(RewardWalletPushNotificationJob.new(params,true, tmp_expired_at)) unless donot_send_pn
          reward_wallet_user = RewardWallet.create(params)
          reward_wallet_user.set_expired_based_on_setting_expire_and_reward
        else
          Delayed::Job.enqueue(RewardUserDelayedJob.new(params),
                               :delayable_type => reward_obj.class.to_s,
                               :delayable_id => reward_obj.id,
                               :chain_id => reward_obj.chain_id,
                               :run_at => (Time.zone.now >= reward_obj.effectiveDate ? Time.zone.now : reward_obj.effectiveDate)
          )
          Delayed::Job.enqueue(RewardWalletPushNotificationJob.new(params,true, tmp_expired_at)) unless donot_send_pn
        end
      end
    end
  end

  #def self.pushed_from(params)
  #  ### get the description where this reward wallet is coming from
  #  if params[:description].blank?
  #    return params.merge!(:description => 'user activity')
  #  end
  #end

  def self.send_pn_to_user(params = {}, set_run_at = true, exp_at)
    selected_reward = Reward.find params[:reward_id] rescue nil
    selected_user = User.find params[:user_id] rescue nil
    return unless selected_user.active

    p "---------------- data list -----------------"
    p params[:reward_id]
    p params[:user_id]
    p selected_reward
    p selected_user
    return false if selected_reward.blank?
    return false if selected_user.blank?


    executed_time = (Time.zone.now >= selected_reward.effectiveDate ? Time.zone.now : selected_reward.effectiveDate)#.end_of_day
    executed_time = executed_time + selected_reward.hours_delay.hours if set_run_at and selected_reward.hours_delay.to_f > 0
    p executed_time
    p "--------------------------end data list ---------------"

    chain = selected_reward.chain
    emails_and_device_tokens_group = selected_reward.group_emails_and_device_token_by_locale([selected_user.id])
    bday_notif_valid = !chain.birthday_reward_notifications.select("email_content").select{|a| !a.email_content.blank?}.blank?
    if selected_reward.reward_type == Reward::TYPES["BIRTHDAY"] && bday_notif_valid
      notification_by_locale = chain.group_message_pn_birthday_reward_by_locale
      push_to_phone = chain.birthday_reward_push_to_phone
      push_to_email = chain.birthday_reward_push_to_email
    else
      notification_by_locale = chain.group_message_pn_by_locale
      push_to_phone = chain.is_push_to_phone
      push_to_email = chain.is_push_to_email
    end

    if push_to_phone and selected_user.push_to_device?
      Delayed::Job.enqueue(GcmAndroidJob.new(selected_reward, emails_and_device_tokens_group, notification_by_locale,exp_at), chain_id: selected_reward.chain_id, delayable_type: selected_reward.class.to_s,	delayable_id: selected_reward.id, run_at: executed_time) if !selected_user.sign_in_device_type.blank? && selected_user.sign_in_device_type.downcase.eql?("android") && !selected_user.device_token.blank?
      Delayed::Job.enqueue(PushIphoneJob.new(selected_reward, emails_and_device_tokens_group, notification_by_locale,exp_at), chain_id: selected_reward.chain_id, delayable_type: selected_reward.class.to_s,	delayable_id: selected_reward.id, run_at: executed_time) if !selected_user.sign_in_device_type.blank? && selected_user.sign_in_device_type.downcase.eql?("iphone")  && !selected_user.device_token.blank?
    end

    fishbowl_notif_sent = chain.send_reward_notif(selected_user, selected_reward, exp_at)
    return if fishbowl_notif_sent
    if push_to_email and selected_user.push_to_email?
      Delayed::Job.enqueue(PushRewardMailJob.new(selected_reward, emails_and_device_tokens_group, notification_by_locale, exp_at), chain_id: selected_reward.chain_id, delayable_type: selected_reward.class.to_s,	delayable_id: selected_reward.id, run_at: executed_time)
    end
  end

  def set_expired_based_on_setting_expire_and_reward
    p "--- set_expired_based_on_setting_expire_and_reward ---"
    reward_obj = self.reward
    user_obj = self.user
    exp_date = RewardWallet.get_expiry_date(reward_obj)
    ### see if setting expired is lower than reward expired
    ##update expiry date
    self.update_attribute(:expiry_date, exp_date)

    if reward_obj.is_net_pos_reward?
      self.update_attribute(:rest_balance, reward_obj.pos_discount_type.discount_amount) if !reward.blank? and !reward.pos_discount_type.blank?
    end
  end

  def self.get_expiry_date(reward_obj)
    expiry_day = reward_obj.expired_day_user
    exp_date = reward_obj.expiryDate

    ### see if setting expired is lower than reward expired
    if expiry_day == 0 ## its not set then get the reward expired date
      exp_date = exp_date.end_of_day
    elsif expiry_day > 0 ## if expiry day for user is set
      if (Time.zone.now + expiry_day.days).end_of_day.to_date < reward_obj.expiryDate.to_date
        exp_date =  (Time.zone.now + expiry_day.days).end_of_day
      end
    end
    return exp_date
  end

  def set_notification_reward
    self.user.update_column(:new_reward_notification,true)
  end

  def is_new_reward?
    rw_timer = self.user.chain.reward_timer

    if rw_timer == 0
      return false



    else
      if Time.zone.now <= self.created_at + rw_timer.hours
        return true
      else
        return false
      end
    end
  end
  
end
