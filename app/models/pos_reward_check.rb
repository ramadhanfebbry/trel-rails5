class PosRewardCheck < ActiveRecord::Base

  belongs_to :pos_location
  belongs_to :chain
  belongs_to :user

  STATUS = {"SUCCESS" => 1, "ERROR" => 2}
end