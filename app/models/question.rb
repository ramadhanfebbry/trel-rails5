class Question < ActiveRecord::Base
  belongs_to :survey
  belongs_to :rotating_group
  has_many :question_choices, :order => 'created_at'
  validates :question_type, :text, :presence => true
  validates :text, :length => {:maximum => 250}

  validate :validate_slider_question

  accepts_nested_attributes_for :question_choices, :reject_if => :all_blank, :allow_destroy => true

  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end


  #TYPES = {"Comments" => 1, "Multiple Choice" => 2, "Dropdown" => 3, "Slidebar" => 4, "Numeric" => 5, "Modified Multiple Choice" => 6  }
  TYPES = {"Comments" => 1, "Multiple Choice" => 2, "Dropdown" => 3, "Slider" => 4, "Multiple Dropdown" => 5}
  NEW_TYPES = {"Dropdown" => 3, "Slider" => 4, "Multiple Dropdown" => 5}
  ORIENTATION_TYPES = {"Top to Bottom" => 1, "Left to Right" => 2}
  NON_APP_DISPLAY = {"value" => 1 , "label" => 2}

  def as_json(options={})
    super(:only => [:id, :text, :question_type, :label, :value],
          :include => [:question_choices])
  end

  def question_type_name
    Question::TYPES.each do |key, value|
      return key if value.to_i.eql?(self.question_type.to_i)
    end
  end

  def show_rotating_questions_on_position(position)
    rotating_questions = []
    rotating_location = ActiveSupport::JSON.decode(self.survey.rotating_locations) unless self.survey.rotating_locations.blank?
    rotating_location.each do |key, value|
      if value["position"] == position and value["question_id"].to_i == self.id
        rotating_questions << "Question from Rotating Question #{key.split("_").last}"
      end
    end unless rotating_location.blank?
    rotating_questions
  end

  def get_rotating_questions_from_collection(position, collection)
    return [] if collection.blank?
    rotating_questions = []
    rotating_location = ActiveSupport::JSON.decode(self.survey.rotating_locations) unless self.survey.rotating_locations.blank?
    rotating_location.each do |key, value|
      if value["position"] == position and value["question_id"].to_i == self.id
        rotating_questions << collection[key.split("_").last.to_i - 1] rescue nil
      end
    end unless rotating_location.blank?
    rotating_questions
  end

  def question_type_parameterize_name
    selected_type = ""
    Question::TYPES.keys.each do |key|
      selected_type = key.downcase if Question::TYPES[key].eql?(self.question_type.to_i)
    end
    selected_type = "unselected" if selected_type.blank?
    selected_type = selected_type.parameterize("_")
  end

  def label_or_value
    if self.non_app_display.to_i > 0
      if self.non_app_display.to_i == 1
        return "value"
      else
        return "label"
      end
    else
      return self.non_app_display
    end
  end

  def validate_slider_question
    if Question::TYPES.invert[self.question_type.to_i].to_s == "Slider"
      if self.question_choices.size < 2
        self.errors.add(:question_choices, "Minimum should have 2 question choices")
      end
      if self.question_choices.size > 20
        self.errors.add(:question_choices, "Maximum should have 20 question choices")
      end
    end
  end
end