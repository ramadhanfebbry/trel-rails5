class GoalCategoryLocale < ActiveRecord::Base

  belongs_to :locale
  belongs_to :category, :class_name => "GoalCategory", :foreign_key => "category_id"

  validates :locale_id, :text, :presence => true
  validates_uniqueness_of :category_id, :scope => :locale_id
  
end
