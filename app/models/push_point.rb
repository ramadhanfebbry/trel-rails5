class PushPoint < ActiveRecord::Base

  #apply_simple_captcha

  has_many :user_push_points
  has_many :delayed_jobs, :as => :delayable, :dependent => :destroy
  belongs_to :chain
  belongs_to :admin, :foreign_key => "pusher_id"

  serialize :user_ids
  serialize :error_logs
  serialize :success_logs


  attr_accessor :xls, :user_temp_ids, :all_user

  validates :points, :numericality => { :greater_than => 0 }
  validates :chain_id, :push_type, :presence => true
  validates :xls, :presence => true, :if => :xls_required?
  validates :user_temp_ids, :presence => true, :if => :user_ids_required?


  def send_push_point_job
    puts "send push Point Job ExEcuTING"
    unless ENV["DEFAULT_HOST_EMAIL"].to_s.blank?
      RewardMailer.admin_push_notification("Push Point from admin ID:#{self.id}",
                                           "Push Point from admin ID:#{self.id}",
                                           self, "STARTING @").deliver! rescue nil
    end

    if !self.user_ids[:user_ids].blank? or self.is_all_user == true
      user_collection = self.user_ids[:user_ids] rescue nil

      if self.is_all_user?
        user_collection = []
        count_email = User.where("chain_id = ?", self.chain_id).count
        loop = (count_email.to_f / 4000.to_f).ceil
        1.upto(loop) do |page|
          user_collection += User.where("chain_id = ?", self.chain_id).select('id').paginate(page: page, per_page: 4000).map(&:id)
        end
        self.user_ids = {:user_ids => user_collection.compact}
        self.save
      end
      unless testing_mode.blank?
        first_data = self.user_ids[:user_ids].first rescue nil
        user_collection = []
        1.upto(testing_mode).each { |x| user_collection << first_data }
        self.user_ids = {:user_ids => user_collection.compact}
        self.save
        user_collection = user_collection.compact
      end

      user_collection.in_groups_of(Setting.delayed_job.push_point_limit).each do |users|
        dl = Delayed::Job.enqueue(PushPointJob.new(self.id, users.compact, self.id), :queue => 'prel_job')
        dl.update_column(:delayable_type, self.class.to_s)
        dl.update_column(:delayable_id, self.id)
        dl.update_column(:chain_id, self.chain_id)
        dl.update_column(:run_at, self.executed_at) unless self.executed_at.blank?
      end
    end
  end

  def xls_required?
    p "*" * 100
    p self.push_type
    self.push_type.to_i.eql?(1)
  end

  def user_ids_required?
    self.push_type.to_i.eql?(2)
  end

  def success_log_entries
    log_entries = []
    PushLogEntry.where(:pushable_id => self.id, :pushable_type => self.class.to_s, :log_type => 1).each do |push_log|
      log_entries << push_log.logs
    end
    log_entries.flatten
  end

  def success_log_entries_count
    PushLogEntry.find_by_sql("
      SELECT SUM(array_length(string_to_array(logs, ':user_id'), 1) - 1) as log_count FROM push_log_entries
where pushable_id=#{self.id} and pushable_type = '#{self.class.to_s}' and log_type = 1").first.log_count.to_i rescue 0
  end

  def error_log_entries
    log_entries = []
    PushLogEntry.where(:pushable_id => self.id, :pushable_type => self.class.to_s, :log_type => 2).each do |push_log|
      log_entries << push_log.logs
    end
    log_entries.flatten
  end

  def error_log_entries_count
    #PushLogEntry.where(:pushable_id => self.id, :pushable_type => self.class.to_s, :log_type => 2).count
    PushLogEntry.find_by_sql("
      SELECT SUM(array_length(string_to_array(logs, ':user_id'), 1) - 1) as log_count FROM push_log_entries
where pushable_id=#{self.id} and pushable_type = '#{self.class.to_s}' and log_type = 2").first.log_count.to_i
  end

end