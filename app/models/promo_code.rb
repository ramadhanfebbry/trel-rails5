class PromoCode < ActiveRecord::Base
  extend KeyUtil

  belongs_to :promotion
  belongs_to :chain
  belongs_to :owner, :class_name => "User", :foreign_key => "from_user_id"

  has_many :users_promo_codes

  #validates :code, :uniqueness => { :scope => :chain_id, :case_sensitive => false }
 
  scope :shared_promo , -> { where('shared = ?', true) }
  scope :one_time_promo , -> { where('shared = ?', false) }
  scope :used_promo , -> { where('used = ? and shared = ?', true, false) }
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end



  def self.code_process(promocode, user, force)
    PromoCode.transaction do
      promo_code = PromoCode.where(:code => promocode, :chain_id => user.chain.id).lock(true).first

      return [false, :invalid_promo_code, true] if promo_code.blank?
      promotion = promo_code.promotion

      today = Date.today

      if promotion.effective_date.to_date <= today and promotion.expiry_date.end_of_day >= Time.zone.now
        #code process for giftable reward
        if promotion.promotable_type.eql?("Reward") && promotion.promotable.giftable?
          p "*** GIFTABLE PROCESS **************************"
          p "*"*50
          p user
          return [false, :can_not_use_own_code, true] if PromoCode.own_code?(user, promo_code.code)
          return [false, :gift_code_already_used_by_user, true] if PromoCode.user_has_used?(user, promo_code)
          return [false, :gift_code_over_limit_used, true] if PromoCode.over_limit_gift_code_used?(promotion.promotable, promo_code)
          RewardWallet.create_with_delay(:reward_id => promotion.promotable.id, :user_id => user.id, :gifter => false)
          UsersPromoCode.create(:promo_code_id => promo_code.id, :user_id => user.id)
          return [true, :gift_code_success, true]
        end

        #code process for non-giftable reward
        if promo_code.shared
          ## using limit on promotion with shared type
          if self.check_limit_promotion(promotion, user, promo_code)
            return self.reach_limit_promo
          end
          ##
          if promotion.promotable_type.eql?("Perk")
            return self.get_status_promocode_for_perk(promo_code, promotion, user, force, true)
          else
            promotion.apply_for(user)
            UsersPromoCode.create(promo_code_id: promo_code.id, user_id: user.id)
            UserRepeatedPromotion.create(:user_id => user.id, :promotion_id => promotion.id, :applied_at => Date.current) if promotion.is_repeated

            notification = promotion.promotion_notification
            # Send email and/or phone notification
            # to the user who perform submit promocode
            notification.send_plain_email_for_user(user) if notification

            return [true, :promo_code_applied, true]
          end
        elsif !promo_code.used
          if promotion.promotable_type.eql?("Perk")
            return self.get_status_promocode_for_perk(promo_code, promotion, user, force, false)
          else
            promotion.apply_for(user)
            promo_code.update_attributes(used: true)
            UsersPromoCode.create(promo_code_id: promo_code.id, user_id: user.id)
            UserRepeatedPromotion.create(:user_id => user.id, :promotion_id => promotion.id, :applied_at => Date.current)  if promotion.is_repeated

            return [true, :promo_code_applied, true]
          end
        else
          return [false, :promo_code_used, true]
        end
      else
        return [false, :promo_code_has_expired, true]
      end
    end
  end

  def self.check_limit_promotion(promotion,user,promo_code)
    if promo_code.shared && promotion.limit.to_i > 0      ## if promocode shared
      promo_limit = promotion.limit
      promocode_used = UsersPromoCode.where("user_id = ? and promo_code_id = ?",user.id, promo_code.id).count
      return promocode_used >= promo_limit
    else
      return false
    end
  end

  def self. over_limit_gift_code_used?(reward, promo_code)
    promocode_used = UsersPromoCode.where("promo_code_id = ?", promo_code.id).count
    promocode_used >= reward.number_of_times_gifted
  end

  def self.user_has_used?(user, promo_code)
    promocodes_used = UsersPromoCode.where("user_id = ? and promo_code_id = ?", user.id, promo_code.id)
    !promocodes_used.blank?
  end

  def self.own_code?(user, code)
    promocode = PromoCode.where(:code => code, :from_user_id => user.id)
    !promocode.blank?
  end

  def self.reach_limit_promo
     return [false,:limit_promo_code,true]
  end

  def self.get_status_promocode_for_perk(promo_code, promotion, user, force, shared = true)
    perk = promotion.promotable
    existing_perk_wallet = PerkWallet.where(user_id: user.id, perk_id: perk.id, status: PerkWallet::STATUS[:ACTIVE]).first
    if existing_perk_wallet.nil? || (force.to_s.downcase == "true" || force.to_s.downcase == "yes")
      existing_perk_wallet.update_attribute(:status, PerkWallet::STATUS[:REMOVED]) unless existing_perk_wallet.blank?
      promotion.apply_for(user)
      unless shared
        UsersPromoCode.create(promo_code_id: promo_code.id, user_id: user.id)
        promo_code.update_attributes(used: true)
      end
      return [true, :promo_code_applied, true]
    else
      existing_perk = existing_perk_wallet.perk
      if existing_perk.stackable and perk.stackable
        promotion.apply_for(user)
        unless shared
          UsersPromoCode.create(promo_code_id: promo_code.id, user_id: user.id)
          promo_code.update_attributes(used: true)
        end
        return [true, :promo_code_applied, true]
      else
        return [true, :promo_code_overwrite, false]
      end
    end
  end

  # @param [integer] quantity
  def self.generate_codes(quantity,chain_id,promotion_id)
    size = Setting.promocode.code_size
    chars = Setting.promocode.code_chars

    result = []

    #(1..quantity).each do |i|
    #  code = generate_random_string(size, chars)
    #  while self.where("code = '#{code}' and chain_id = #{chain_id}").first
    #    puts "---------- code is #{code} -------------------- "
    #    code = generate_random_string(size, chars)
    #  end
    #  result << code
    #end
    #self.insert_promocode(result,chain_id, promotion_id)
    #result

    (1..quantity).to_a.in_groups_of(500).each do |group|
      result = []
      group.compact.each do |i|
        code = generate_random_string(size, chars)
        puts "CODE == #{code}"
        valid = false
        while valid == false
          # some code here
          code = generate_random_string(size, chars)
          exist = self.where("code = '#{code}' and chain_id = #{chain_id}").first
          valid = true if exist.blank?
        end
        result << code
      end
      self.insert_promocode(result,chain_id, promotion_id)
    end
  end

  def self.insert_promocode(results,chain_id, promotion_id)
    promocodes = []
    results.each do |res|
      promocodes << PromoCode.new(:code => res, :chain_id => chain_id, :promotion_id => promotion_id)
    end
    self.import promocodes
  end


  def self.generate_sample_codes(quantity)
    size = Setting.promocode.code_size
    chars = Setting.promocode.code_chars

    result = []

    (0..quantity).each do |i|
      code = generate_random_string(size, chars)
      while self.find_by_code(code)
        code = generate_random_string(size, chars)
      end
      result << code
    end

    result
  end

end


