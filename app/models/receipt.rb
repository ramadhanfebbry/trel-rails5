class Receipt < ActiveRecord::Base
  include Additions::Ocr

  self.per_page = 10

  #  after_create :sqs_send_message

  validates :chain_id, :presence => true, :on=>:update
  validates :user_id, :presence => true, :if => Proc.new{|f| f.is_online_order == false and f.is_olo == false }
  validates :status, :presence => true, :on=>:update

  # NEED TO FIX SOON
  validates :time_stamp,
    :format => {:with => /\A([01]\d:[0-5]\d(AM|PM))?\z/, :message => "Incorrect time format"}
  
  # Paperclip.interpolates :img_content_type do |attachment, style|
  #   attachment.instance.img_from_mobile ? "jpeg" : (attachment.instance.image_content_type.split("/").last rescue "jpeg")
  # end

  # has_attached_file :image,
  #   :styles => { :small => Setting.paperclip.styles.small,
  #   :large => Setting.paperclip.styles.large },
  #   :path => ":attachment/:year/:month/:receipt_:user_:chain_:timestamp_:style.:img_content_type",
  #   :storage => :s3,
  #   :bucket => Setting.storage.s3_bucket,
  #   :s3_credentials => {
  #   :access_key_id => Setting.storage.s3_access_key_id,
  #   :secret_access_key => Setting.storage.s3_secret_access_key
  # }


  # #validates_attachment_presence :image, :on=>:create
  # validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :on=>:create
# END NEED TO FIX SOON
  STATUS = {RECEIVED: 1, OCRED: 2, APPROVED: 3, REJECTED: 4, PENDING_OCR: 5}
  BARCODE_STATUS = {RECEIVED: 1, APPROVED: 3, REJECTED: 4}
  ORDER_KIND = {
    "Dine in" =>   "Dine In", "Dine Out (To Go)" => "Dine out",
    "Catering" => "Catering",
    "Dine out (Call in)" => "Call in"
    }
  PAYMENT_METHOD = {CASH: 1, CARD: 2}

  default_scope  { where(:deleted_at => nil) }
  scope :one_time_reward , -> (u){ where('status = ? and user_id = ?', STATUS[:APPROVED],u.id) }
  scope :desc_list , -> { order("receipts.created_at desc") }
  scope :active_chain, -> { includes(:chain).where("chains.status = 'active'") }
  scope :receipt_image, -> { where("receipts.is_receipt_barcode IS FALSE AND receipts.is_online_order is FALSE and receipts.is_olo is FALSE") }
  scope :receipt_order_online, -> { where("receipts.is_online_order is TRUE") }
  scope :receipt_olo, -> { where("receipts.is_olo is TRUE") }
  scope :receipt_barcode, -> { where("is_receipt_barcode IS TRUE") }

  belongs_to :chain
  belongs_to :user
  has_many :receipt_transactions
  has_one :surveys_user
  has_one :comment
  has_one :olo_order_detail
  belongs_to :pos_check_upload
  belongs_to :ncr_data_receipt
  has_one :current_transaction, class_name: 'ReceiptTransaction', foreign_key: :receipt_id

  #default_scope order("updated_at desc")

  def self.deleted
    self.where('receipts.deleted_at IS NOT NULL')
  end

  def last_transaction
    @receipt_transactions = ReceiptTransaction.where("receipt_id = ?", self.id).order("id desc").limit(1)
    @receipt_transactions[0] if @receipt_transactions.length == 1
  end

  def the_latest_transaction
    self.receipt_transactions.order("id DESC").first
  end


  def last_transaction_with_date_range(sql)
      sql = sql.gsub("receipts.updated_at", "receipt_transactions.issue_date")
      @receipt_transactions = ReceiptTransaction.where("receipt_id = ?", self.id).where(sql).order("id desc").limit(1)
      @receipt_transactions[0] if @receipt_transactions.length == 1
  end

  def last_transaction_for_user_activity_call
    @receipt_transaction = ReceiptTransaction.where("receipt_id = ?", self.id).order("id desc").first
    return nil if @receipt_transaction.blank?
    @receipt_transaction.status = 1 if [2,5].include?(@receipt_transaction.status)
    json_format = {}
    ReceiptTransaction.column_names.each do |column|
      if column == "total_points_earned"
        json_format[column] = @receipt_transaction.send(column).to_i.to_s
      else
        json_format[column] = @receipt_transaction.send(column)
      end
    end
    json_format
  end

  def push_one_time_reward(user)
    chain_events =  ChainRewardEvent.includes(:reward).where(:chain_id => user.chain.id, :event => ChainRewardEvent::EVENTS["FIRST_RECEIPT"])
    unless chain_events.blank?
      puts chain_events.first.reward

      receipt_approved_count = user.receipts.select("user_id, status").where(:status => Receipt::STATUS[:APPROVED]).reload.count
      chain_events.each {|chain_event|
        if receipt_approved_count > 0  and receipt_approved_count <= 1
          RewardWallet.find_or_create_by_reward_id_and_user_id(chain_event.reward_id, user.id)
        end
      }
    end
  end

  def image_url
    self.image.url
  end

  def self.count_by_user_and_status(user, status)
    where("user_id = ? AND status = ?", user, status).count()
  end

  def find_restaurant(restaurant_id)
    begin
      self.receipt_transactions.where(:restaurant_id => restaurant_id).first.id
    rescue
      nil
    end
  end

  #def self.search_receipts(params)
  #  cond=''
  #  if (!params[:restaurants].blank?)
  #    cond = "AND ro.restaurant_id in(#{params[:restaurants].join(',')})"
  #  end
  #  if (!params[:chains].blank?)
  #    cond += "AND receipts.chain_id in(#{params[:chains].join(',')})"
  #  end
  #  if (!params[:offers].blank?)
  #    cond += "AND ro.offer_id  in(#{params[:offers].join(',')})"
  #  end
  #  if (!params[:review_status].blank?)
  #    cond += "AND receipts.status=#{params[:review_status]}"
  #  end
  #  if (!params[:payment_method].blank?)
  #    cond += "AND rt.payment_method=#{params[:payment_method]}"
  #  end
  #  if (!params[:q].blank?)
  #    intvalue = params[:q].to_i
  #    cond += "AND u.email LIKE '%#{params[:q]}%'"
  #    if (intvalue>0)
  #      cond += "OR rt.receipt_number = #{intvalue} OR ro.offer_id=#{intvalue}"
  #    end
  #  end
  #
  #  if (!params[:from].blank?)
  #    cond += "AND rt.issue_date >= '#{params[:from]}'"
  #  end
  #
  #  if (!params[:to].blank?)
  #    cond += "AND rt.issue_date <= '#{params[:to]}'"
  #  end
  #
  #  @receipts = Receipt.select("DISTINCT on (rt.receipt_id, rt.created_at) receipts.*").joins("inner join users u on u.id=receipts.user_id inner join receipt_transactions rt on rt.receipt_id = receipts.id inner join restaurant_offers ro on rt.restaurant_offer_id = ro.id inner join offers o on ro.offer_id = o.id").where("u.id = receipts.user_id #{cond}").order("rt.created_at DESC")
  #end
  #
  #def uploadImage(upload)
  #  name = upload.original_filename
  #  directory = "public/images/receipts/"
  #  # create the file path
  #  path = File.join(directory, name)
  #  # write the file
  #  File.open(path, "wb") { |f| f.write(upload.read) }
  #end

  attr_accessor :food_total, :beverage_total, :subtotal, :tax, :issue_date, :time_stamp, :card_number, :preps_total, :total_payment, :cashier, :base_points_earned, :bonus_earned, :total_points_earned, :note, :restaurant_offer_id, :img_from_mobile, :receipt_date, :receipt_time, :restaurant_id


  #------------------------Scopes for Searching-----------------------------------------------------

  distinct_transaction = "DISTINCT on (receipt_transactions.receipt_id) receipts.*"
  count_distinct_user_id = "count(DISTINCT (receipts.user_id)) "
  chart_transaction = "DISTINCT on (date(receipts.created_at)) date(receipts.created_at),count(receipts.user_id) as count_id"
  transaction_offer_join = "INNER JOIN restaurant_offers ON receipt_transactions.restaurant_offer_id=restaurant_offers.id"
  restaurant_join = "INNER JOIN restaurants ON restaurant_offers.restaurant_id=restaurants.id"
  count_transaction = "DISTINCT on (receipt_transactions.receipt_id, date(receipt_transactions.created_at)) count(receipt_transactions.receipt_id),date(receipt_transactions.created_at) as created_at"
  #count_transaction = "DISTINCT on (receipt_transactions.receipt_id, receipt_transactions.created_at) count(*), receipts.created_at"

  scope :chains, -> (value) {  where("chain_id" => value) if (!value.blank?) }
  scope :status, -> (value) {  where("status" => value) if (!value.blank?) }

  scope :email, -> (value) {  joins(:user).where("users.email LIKE '%#{value}%'") if (!value.blank?) }

  scope :receipt, -> (value) {  where("id" => value) if (!value.blank?) }

  scope :payment_method, -> (value) {
    if (!value.blank?)
      select(distinct_transaction).joins(:receipt_transactions).where("receipt_transactions.payment_method"=>value)
    end
  }

  scope :submitted_from, -> (value) { where("DATE(receipts.created_at) >='#{value}'") if (!value.blank?) }

  scope :submitted_to, -> (value) {   where("DATE(receipts.created_at) <='#{value}'") if (!value.blank?) }

  scope :date_from, -> (value) { 
    if (!value.blank?)
      select(distinct_transaction).joins(:receipt_transactions).where("receipt_transactions.issue_date >='#{value}'")
    end
  }

  scope :date_to, -> (value) { 
    if (!value.blank?)
      select(distinct_transaction).joins(:receipt_transactions).where("receipt_transactions.issue_date<='#{value}'")
    end
  }

  scope :restaurants, -> (value) { 
    if (!value.blank?)
      select(distinct_transaction).joins(:receipt_transactions, transaction_offer_join).where("restaurant_offers.restaurant_id"=>value)#.group("date(receipts.created_at),receipt_transactions.receipt_id")
    end
  }

  scope :chart_restaurants, -> (value) { 
    if (!value.blank?)
      select(chart_transaction).joins(:receipt_transactions, transaction_offer_join).where("restaurant_offers.restaurant_id"=>value)#.group("date(receipts.created_at),receipt_transactions.receipt_id")
    end
  }

  scope :by_restaurant_ids , -> (value) { 
    if (!value.blank?)
      select(distinct_transaction).joins(:receipt_transactions).where("receipt_transactions.restaurant_id in (?)",value)
    end
  }

  scope :active_membership  , -> (value) { 
    if (!value.blank?)
      select(count_distinct_user_id).joins(:receipt_transactions, transaction_offer_join).
          where("receipt_transactions.restaurant_id in (?) and
                 date(coalesce(receipt_transactions.issue_date,
                 receipts.created_at)) between date(?) and date(?)",
                    value,(Time.now - 180.days).to_date, Time.now)
    end
  }

  scope :by_restaurant , -> (value) { 
    if (!value.blank?)
      select(distinct_transaction).joins(:receipt_transactions).where("receipt_transactions.restaurant_id in (?)",value)
    end
  }

  scope :count_restaurant_ids , -> (value) { 
    if (!value.blank?)
      select(count_transaction).joins(:receipt_transactions, transaction_offer_join).where("restaurant_offers.restaurant_id in (?)",value)
    end
  }

  scope :count_user_join_date, -> { group("receipts.id,receipts.chain_id,receipts.user_id,receipts.status,receipts.image_url,receipts.thumbnail_url,receipts.created_at ,	receipts.updated_at,receipts.image_file_name,receipts.image_content_type,receipts.image_file_size ,receipt_transactions.receipt_id,receipt_transactions.created_at,	receipts.image_updated_at") }

  scope :offers, -> (value) { 
    if (!value.blank?)
      select(distinct_transaction).joins(:receipt_transactions, transaction_offer_join).where("restaurant_offers.offer_id"=>value)
    end
  }

  scope :combined, -> (value) { 
    intvalue = value.to_i
    if (intvalue>0)
      #select(distinct_transaction).joins(:user, :receipt_transactions, transaction_offer_join).where("receipts.id = ? OR users.email LIKE ? OR receipt_transactions.receipt_number = ? OR receipt_transactions.server_name LIKE ? OR receipt_transactions.cashier LIKE ? OR restaurant_offers.offer_id = ?", intvalue, "%#{value}%", intvalue, "%#{value}%", "%#{value}%", intvalue)
      select(distinct_transaction).joins(:user, :receipt_transactions, transaction_offer_join).
        where("receipts.id = ? or restaurant_offers.offer_id = ? or receipt_transactions.receipt_number like ?", intvalue, intvalue, "%"+value+"%")
    else
      select(distinct_transaction).joins(:user, :receipt_transactions).where("receipt_transactions.server_name LIKE ? OR receipt_transactions.cashier LIKE ? OR users.email LIKE ?", "%#{value}%", "%#{value}%", "%#{value}%")
    end
  }

  scope :search, -> (params) { 
    chains(params[:chains]).status(params[:review_status]).combined(params[:q]).restaurants(params[:restaurants]).offers(params[:offers]).payment_method(params[:payment_method]).submitted_from(params[:from]).submitted_to(params[:to])
  }

  scope :custom_search, -> (params) { 
    conditions = []
    conditions << "chains.status = 'active'"
    conditions << "offers.id IN (#{params[:offers].join(",")})" unless params[:offers].blank?
    conditions << "restaurants.id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    conditions << "chains.id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "#{params[:qry_column]} ILIKE '%#{params[:q]}%'" if !params[:qry_column].blank? && !params[:q].blank?
    conditions << "DATE(receipts.created_at) >= '#{params[:from]}'" unless params[:from].blank?
    conditions << "DATE(receipts.created_at) <= '#{params[:to]}'" unless params[:to].blank?
    conditions << "receipts.status = #{params[:review_status]}" unless params[:review_status].blank?
    conditions << "receipt_transactions.payment_method = #{params[:payment_method]}" unless params[:payment_method].blank?
    conditions = conditions.join(" AND ")
    # joins(:chain, :user, :receipt_transactions => [:offer, :restaurant]).where(conditions)
    includes(:chain, :user, :receipt_transactions).where(conditions)
  }

  scope :custom_search_for_reviewer, -> (params) { 
    conditions = Receipt.build_conditions_search(params)
    # joins(:chain, :user, :receipt_transactions => [:offer, :restaurant]).where(conditions)
    includes(:chain, :user, :receipt_transactions).where(conditions)
  }

  scope :custom_search_with_barcode, -> ( params, restaurant_id=nil){
    conditions = []
    conditions << "offers.id IN (#{params[:offers].join(",")})" unless params[:offers].blank?
    if restaurant_id.present?
      conditions << "restaurants.id = (#{restaurant_id})" unless restaurant_id.blank?
    else
      conditions << "restaurants.id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    end
    conditions << "chains.id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "#{params[:qry_column]} ILIKE '%#{params[:q]}%'" if !params[:qry_column].blank? && !params[:q].blank?
    conditions << "DATE(receipts.created_at) >= '#{params[:from]}'" unless params[:from].blank?
    conditions << "DATE(receipts.created_at) <= '#{params[:to]}'" unless params[:to].blank?
    conditions << "receipts.status = #{params[:review_status]}" unless params[:review_status].blank?
    conditions << "receipt_transactions.payment_method = #{params[:payment_method]}" unless params[:payment_method].blank?
    conditions = conditions.join(" AND ")
    includes(:chain, :user, :pos_check_upload, :receipt_transactions => [:offer, :restaurant]).where(conditions)
  }

  scope :custom_search_online_orders, -> (params) { 
    conditions = []
    conditions << "offers.id IN (#{params[:offers].join(",")})" unless params[:offers].blank?
    conditions << "restaurants.id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    conditions << "chains.id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "(LOWER(receipt_transactions.server_name) LIKE '%#{params[:q].downcase}%' OR receipt_transactions.cashier LIKE '%#{params[:q].downcase}%' OR users.email LIKE '%#{params[:q].downcase}%' OR receipt_transactions.receipt_number LIKE '%#{params[:q].downcase}%' OR (lower(restaurants.name)) LIKE '%#{params[:q].downcase}%')" unless params[:q].blank?
    conditions << "DATE(receipts.created_at) >= '#{params[:from]}'" unless params[:from].blank?
    conditions << "DATE(receipts.created_at) <= '#{params[:to]}'" unless params[:to].blank?
    conditions << "receipts.status = #{params[:review_status]}" unless params[:review_status].blank?
    conditions << "receipt_transactions.payment_method = #{params[:payment_method]}" unless params[:payment_method].blank?
    #conditions << "LOWER(pos_check_uploads.barcode) LIKE '%#{params[:barcode].downcase}%'" unless params[:barcode].blank?
    conditions = conditions.join(" AND ")
    includes(:chain, :user, :pos_check_upload).where(conditions)
  }

  scope :average_check_by_frequency, -> (chain_id,restaurant_ids){
    select("count(user_id) as count_user, receipt_transactions.restaurant_id").joins(:receipt_transactions).
      group("receipts.user_id,receipt_transactions.restaurant_id").
      where("receipt_transactions.restaurant_id in (?) and receipts.status = 3", restaurant_ids)
  }
  #case_issue_date = "case when
  #  receipt_transactions.issue_date is null then receipts.created_at
  #  else receipt_transactions.issue_date
  #  end"
  #  case_issue_date = "(
  #case when receipt_transactions.issue_date is null
  #then receipts.created_at
  # else receipt_transactions.issue_date
  # end)"

  case_issue_date = "(
  case when exists(select receipt_transactions.issue_date)
  then receipt_transactions.issue_date
    else receipts.created_at
  end
  )"

  case_issue_date_receipt_date = "(
  case when  receipt_transactions.receipt_date is not null
 then receipt_transactions.receipt_date
 else receipt_transactions.receipt_date
 end
  )"

  case_coalesce_receipt = "coalesce(receipt_date, issue_date)"

  date_zone = "date(TIMEZONE('UTC',#{case_issue_date})
   AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"

  date_zone_receipt = "date(TIMEZONE('UTC',#{case_issue_date_receipt_date})
   AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"

  date_coalesce = "date(TIMEZONE('UTC',#{case_coalesce_receipt})
   AT TIME ZONE 'UTC')"



  scope :dashboard_receipt_activity, -> (value,date){
    select(distinct_transaction).joins(:receipt_transactions).
        where("receipt_transactions.restaurant_id = ? and receipts.status = 3 and #{date_zone} = date(?)",value,date.to_date)
  }

  scope :receipt_date_dashboard_receipt_activity, -> (value,date){
    select(distinct_transaction).joins(:receipt_transactions).
        where("receipt_transactions.restaurant_id = ? and receipts.status = 3 and #{date_zone_receipt} = date(?)",value,date.to_date)
  }

  scope :receipt_date_coalesce_dashboard_receipt_activity, -> (value,date){
    select(distinct_transaction).joins(:receipt_transactions).where("receipt_transactions.restaurant_id = ? and receipts.status = 3 and #{date_coalesce} = date(?)",value,date.to_date)
  }

  scope :new_dashboard_receipt_activity, -> (value,date){
    select(distinct_transaction).joins(:receipt_transactions).
      where("receipt_transactions.restaurant_id = ? and receipts.status = 3 and #{date_zone} = date(?)",value,date.to_date)
  }

  def self.active_members_chart(restaurant, date)
    #x = self.count_restaurant_ids(restaurant.id).where(date).group("receipts.id,receipts.created_at,receipt_transactions.receipt_id, receipt_transactions.created_at").map{|x| [x.created_at.to_i * 1000, x.count]}
    x = self.count_restaurant_ids(restaurant.id).
      where(date).
      group("date(receipt_transactions.created_at) ,receipt_transactions.receipt_id").
      map{|x| [x.created_at.to_i * 1000, x.count]}
  end

  def self.build_conditions_search(search)
    conditions = []
    conditions << "receipts.status = #{search[:review_status]}" unless search[:review_status].blank?

    ## get latest 30 days id
    unless search[:chains].blank?
      if search[:chains].size > 1
        # chain selected more than 1
        conditions << "receipts.chain_id IN (#{search[:chains].join(",")})"
      else
        conditions << "receipts.chain_id = #{search[:chains].first.to_i}"
      end
      latest_id = self.where("chain_id in (#{search[:chains].join(",")}) and date(created_at) > ?", (Time.zone.now - 30.days).to_date).order("id desc").last.id rescue nil
      puts "latest_id ===== #{latest_id}" rescue nil
      puts "#{search[:chains]}"
    else
      latest_id = self.where("date(created_at) > ?", (Time.zone.now - 30.days).to_date).order("id desc").last.id rescue nil
      puts "latest_id ===== #{latest_id}" rescue nil
    end

    conditions << "receipts.id > #{latest_id}" unless latest_id.blank?

    conditions = conditions.join(" AND ")
    conditions
  end

  ## get the restaurant of this receipt
  def restaurant
    self.last_transaction.restaurant_id
  end

  def restaurant_obj
    [self.last_transaction.restaurant]
  end

  def restaurant_list_from_transactions
    current_restaurants = self.receipt_transactions.map{|x| x.current_restaurant}.uniq.compact
    restaurant_offers = self.last_transaction.restaurant_offer.offer.restaurants rescue []
    (current_restaurants + restaurant_offers).uniq.compact
  end


  def push_referral_code_reward
    ## if user has referral code when sign up and this is the first time he /she submit receipt
    puts "---------------- push referral code reward ---------------- "

    active_referral = ReferralProgram.where("chain_id = ?", self.chain_id).active.first

    ## if its set to approved before getting the referral reward
    if active_referral && active_referral.approve == true
      ### at least 1 receipt is approved
      count_receipt = 10
      count_receipt = self.receipt_transactions.where("receipt_transactions.status = ?",Receipt::STATUS[:APPROVED]).count
      count_receipt = count_receipt + 1
      ### set it to zero if first receipt approved, and first receipt submitted
      approved_receipt = Receipt.where("user_id = ? AND status = ?",self.user_id,Receipt::STATUS[:APPROVED]).count
      puts "approved receipt === #{approved_receipt}"
      if approved_receipt  < 2 and count_receipt == 2
        count_receipt = 0
      end
      puts "count_receipt approve = true ============================== #{count_receipt}"
      puts "***************************************************************"
    else
      count_receipt = Receipt.where("user_id = ? AND status = ?", self.user_id, Receipt::STATUS[:APPROVED]).count
      #count_receipt = Receipt.where("user_id = ? AND status = ?", self.user_id, Receipt::STATUS[:RECEIVED]).reload.count
      #count_receipt = 0 if count_receipt == 0
      puts "count_receipt approve = false ============================== #{count_receipt}"
      puts "***************************************************************"
    end
    ## end
    ## if its have minimum subtotal
    min_approve = active_referral.min_approve.to_i  if active_referral
    subtotal_passed = true
    if active_referral and min_approve > 0  and active_referral.approve == true
      ## get the subtotal 1st
      subtotal = self.last_transaction.subtotal rescue 0
      subtotal_passed = false if subtotal < min_approve rescue false
    end


    p "receipt approved count #{count_receipt}"
    p count_receipt
    p self.user.ref_code
    p subtotal_passed
    p active_referral

    if count_receipt == 0 and self.user.ref_code and subtotal_passed and active_referral
      ReferralCode.push_referral_reward(self.chain, self.user.ref_code, self.user, active_referral)
      puts "----------------------- pushed referral code reward --------------------------"
    end
  end


  def self.push_referral_code(user,chain)
    chain_id = chain.id
    user_id = user.id
    p "RECEIPT::PUSH REFERRAL CODE"
    ## if user has referral code when sign up and this is the first time he /she submit receipt
    puts "---------------- push referral code reward ---------------- "

    active_referral = ReferralProgram.where("chain_id = ?", chain_id).active.first

    ## if its set to approved before getting the referral reward
    if active_referral && active_referral.approve == false
      count_receipt = Receipt.where("user_id = ? AND status = ?", user_id, Receipt::STATUS[:APPROVED]).count
      puts "count_receipt approve = false ============================== #{count_receipt}"
      puts "***************************************************************"
    end
    ## end
    ## if its have minimum subtotal
    min_approve = active_referral.min_approve.to_i  if active_referral
    subtotal_passed = true


    p "receipt approved count #{count_receipt}"
    p count_receipt
    p user.ref_code
    p subtotal_passed
    p active_referral

    if count_receipt == 0 and user.ref_code and subtotal_passed and active_referral
      ReferralCode.push_referral_reward(chain, user.ref_code, user, active_referral)
      puts "-----------------------RECEIPT::PUSHREFERRALCODE pushed referral code reward --------------------------"
    end
  end

  def set_order_type(xml_order_type)
    Receipt::ORDER_KIND.keys.each do |key|
      if key.downcase.include?((xml_order_type.downcase rescue "xxxxxxx"))
        self.order_kind = Receipt::ORDER_KIND[key]
        break
      end
    end

  end

  def self.insert_dummy_data(start_date, end_date,chain_id)
    ## insert dummy data for chart testing
    days = (end_date.to_date - start_date.to_date).to_i

    days.downto(0) do |day|
     rec =  Receipt.create(
          :chain_id => chain_id,
          :user_id => User.where("chain_id = ?", chain_id).order("rand()").limit(1),
          :status => Receipt::STATUS[:APPROVED],
          :image_url => nil,
          :thumbnail_url => nil,
      )

      total = rand(100)
      offer = Offer.where("chain_id = ?",chain_id).order("random()").limit(1).first
      res_id = offer.restaurants.first.id

     ReceiptTransaction.create(
          :issue_date => Time.now - day.days,
          :cashier => "dummy data",
          :status => 3,
          :receipt_number => rand(1000),
          :tax => 0,
          :subtotal => total,
          :food_total => 0,
          :beverage_total => 0,
          preps_total: 0 ,
          total_payment: total,
          base_points_earned: total,
          bonus_earned: 0,
          total_points_earned:total,
          receipt_id: rec.id,
          restaurant_offer_id: offer.id,
          note: "dummy test",
          restaurant_id: res_id
      )
    end
  end

  def get_date
    ## issue date / submission_date

    ## check the column first
    r_trans = self.last_transaction
    r_trans = self.update_receipt_date(r_trans)

    if r_trans.issue_date.blank?
      return self.created_at
    else
      ## if there are time stamp
      return r_trans.issue_date if r_trans.time_stamp.blank?
      if r_trans.time_stamp
        time_stamp = r_trans.time_stamp
        split_time = time_stamp.split(':')
        hour = split_time[0]
        zn = if time_stamp.downcase.include?('am')
               "am"
             else
               "pm"
             end
        min = split_time[1][0..1]
        hour = hour.to_i + 12 if zn and  zn.downcase == "pm"   && hour.to_i < 12
        hour = 0 if zn and  zn.downcase == "pm"   && hour.to_i == "12"
        return self.created_at if hour.to_i > 24
        begin
          result = Time.parse(r_trans.receipt_date.strftime('%Y-%m-%d %I:%M:%S UTC')).to_s.
              to_datetime.change(:hour => hour.to_i, :min => min.to_i)

        rescue
          result = Time.parse(r_trans.receipt_date.strftime('%Y-%m-%d %I:%M:%S UTC')).to_s.
              to_datetime.change(:hour => time_stamp[0..1].to_i, :min => min.to_i)
        end
        return result
      end
    end
  end

  def is_receipt_image?
    is_receipt_barcode == false && is_ncr_receipt == false && is_online_order == false && is_olo == false
  end

  def update_receipt_date(r_trans)
    ## issue date blank and receipt_date is exist
    if r_trans.issue_date.blank? and !r_trans.receipt_date.blank?
    ## update the issue date
      r_trans.update_column(:issue_date, r_trans.receipt_date)
    ## issue date exists , receipt date is blank
    elsif !r_trans.issue_date.blank? and r_trans.receipt_date.blank?
      r_trans.update_column(:receipt_date, r_trans.issue_date)

    elsif r_trans.issue_date.blank? and r_trans.receipt_date.blank?
      r_trans.update_column(:receipt_date, r_trans.created_at)
      r_trans.update_column(:issue_date, r_trans.created_at)
    end
    return r_trans
    # time stamp blank?
  end

  def substact_point_due_to_receipt_changed_from_approved
    transactions = self.receipt_transactions.order("id asc").reload
    transaction_status = transactions.map{|a| Receipt::STATUS.invert[a.status].to_s}
    second_last_transaction =  transactions[transactions.length-2] rescue nil
    last_transaction = transactions.last rescue nil
    second_last_transaction_status = transaction_status[transaction_status.length-2] rescue nil
    if !second_last_transaction_status.blank? && !second_last_transaction.blank?
      if second_last_transaction_status == "APPROVED"
        "should substact point = #{second_last_transaction.total_points_earned}"
        #self.receipt.user.points -= transaction.total_points_earned.to_i
        #user.update_attribute(:points, self.receipt.user.points)
        #perk_and_calculate_points(self.receipt.user)
        #user.burn(transaction.total_points_earned.to_f)
        points_before = self.user.points
        puts "points before == #{points_before}"
        puts "transaction before #{second_last_transaction.total_points_earned}"
        point_lost = 0
        if points_before < second_last_transaction.total_points_earned
          puts "if"
          user.burn(points_before)
          point_lost =  points_before
        else
          puts "else"
          user.burn(second_last_transaction.total_points_earned)
          point_lost = second_last_transaction.total_points_earned
        end
        if user.total_points_earned <  second_last_transaction.total_points_earned
          user.decrement!(:total_points_earned, user.total_points_earned)
        else
          user.decrement!(:total_points_earned, second_last_transaction.total_points_earned)
        end

        #self.total_points_earned = ((points_before - transaction.total_points_earned) +
        description = "Receipt ##{self.id} edited "
        if last_transaction.status == Receipt::STATUS[:REJECTED]
          description += "(changed to rejected)"
        elsif last_transaction.status == Receipt::STATUS[:PENDING_OCR]
          description += "(changed to pending ocr)"
        elsif last_transaction.status == Receipt::STATUS[:OCRED]
          description += "(changed to ocred)"
        elsif last_transaction.status == Receipt::STATUS[:APPROVED]
          description += "(data updated)"
        end
        PointHistory.add_to_history(self.user.id, description, (second_last_transaction.total_points_earned * -1))
        puts "xxxxxxxxxxxxxxxxxxxxxxxx #{point_lost}"
      end
    end
  end

  def export_status(chain, date_range_end)
    fname = "export_receipt_status_#{chain.id}_#{date_range_end.strftime("%B")}.csv"
    path = "#{Rails.root}/tmp/#{fname}"
    sql_date =  "TIMEZONE('UTC', receipts.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}'"
    count = chain.receipts.where("receipts.status IN (3,4) AND DATE(#{sql_date}) <= ?", date_range_end).count
    loop = (count.to_f / 500.to_f).ceil
    p "total loop #{loop}"
    File.new(path, 'w')
    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      csv << ["User ID",
              "Receipt status",
              "Date"
      ]

      1.upto(loop) do |page|
        p "loop ke #{page}"
        receipts = chain.receipts.where("receipts.status IN (3,4) AND DATE(#{sql_date}) <= ?", date_range_end).paginate(page: page, per_page: 500)
        receipts.each do |receipt|
          # data rows
          csv_content = [
              receipt.user_id,
              receipt.receipt_status_label,
              receipt.created_at.to_date
          ]
          csv << csv_content
        end
      end
    end
    OwnerMailer.send_reward_redeemed(nil, "sm@dailygobble.com", path, "#{fname}","Export Status Receipt #{chain.id}").deliver!
    File.delete(path)
    puts "Success EXECUTING report"

  end

  def receipt_status_label
    case self.status
      when Receipt::STATUS[:APPROVED]
        'Approved'
      when Receipt::STATUS[:REJECTED]
        'Rejected'
      when Receipt::STATUS[:RECEIVED]
        'Received'
      when Receipt::STATUS[:OCRED]
        if current_admin
          'OCRed'
        elsif current_user
          'Received'
        end
      when Receipt::STATUS[:PENDING_OCR]
        "Pending OCR"
      else
        'Unknown'
    end
  end

  def show_receipt_barcode
    pos_check_upload = self.pos_check_upload
    return "Show" if pos_check_upload.blank?
    return "Show" if pos_check_upload.barcode.blank?
    return pos_check_upload.barcode
  end

  def receipt_type_name
    if !self.image_content_type.blank?
      return "image"
    elsif self.is_receipt_barcode == true
      return "barcode"
    elsif self.is_ncr_receipt == true
      return "ncr"
    end
  end

  def self.get_detail_micros_receipt(chain, barcode)
    barcode = barcode.to_s.upcase
    barcode = AppcodeSetting.receipt_code_convert(chain, barcode)
    p "barcode after converted with selected Setting = #{barcode}"
    pos_check_upload = PosCheckUpload.where("chain_id = ? AND barcode = ?", chain.id, barcode).first rescue nil
    if pos_check_upload.blank?
      pos_check_upload = PosCheckUpload.where("chain_id = ? AND old_barcode = ?", chain.id, barcode).first rescue nil
    end

    p "selected pos check upload"
    p pos_check_upload
    if pos_check_upload
      pos_xml = PosReceiptXml.new(pos_check_upload.xml_data)
      receipt_date = DateTime.strptime(pos_xml.receipt_date, "%m/%d/%y %H:%M:%S")
      if chain.is_expired_micros_receipt?(receipt_date)
        return {:status => false, :notice => "The code expired. Please contact customer support from the info section of the app for further assistance.", :pos_check_upload => pos_check_upload.try(:id)}
      end
      return {:status => true, :check => (Hash.from_xml(pos_check_upload.xml_data) rescue nil), :pos_check_upload => pos_check_upload.try(:id) }
    else
      return {status: false, notice: "We are sorry. The code scanned is either invalid. Please contact customer support from the info section of the app for further assistance."}
    end
  end

  def self.do_pay_ts(apikey, paycode, amount, tip, msg_type, check_num, check_seq, revenue_center, employee_num, check_open_time)
    begin
      body_params = {
        "amount" => amount + tip,
        "pay_code" => paycode,
        "msg_type" => msg_type,
        "msg_body" => {
          "check_num" => check_num,
          "check_seq" => check_seq,
          "revenue_center" => revenue_center,
          "employee_num" => employee_num,
          "check_open_time" => check_open_time #"2015-08-23 19:21:39"
        }
      }
      payment_root_url = "https://fundy.relevantmobile.com:8453/mobilepay/api/v2/"
      uri = URI.parse(payment_root_url + "user/payment")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new(uri.request_uri)
      request.body = body_params.to_json
      request["Content-Type"] = "application/json; charset=utf-8"
      request["apikey"] = apikey
      response = http.request(request)
      if response.code == "200" || response.code == "201"
        p "response payment nya ---------------"
        json_response = JSON.parse(response.body)
        p json_response
        if json_response["code"] == 0
          return [true, 0, "success", json_response["authorization"] ]
        else
          return [false, json_response["code"], json_response["message"]]
        end
      else
        p "REQUEST FAILED --------------"
        json_response = JSON.parse(response.body) rescue nil
        p json_response
        return [false, (json_response["code"] rescue nil), (json_response["message"] rescue nil)]
      end
    rescue => e
      return [false, 1000, e.message]
    end

  end

  def is_future_check?(issue_date)
    if issue_date.present?
      issue_date > self.created_at
    end
  end
end

