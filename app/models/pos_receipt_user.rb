class PosReceiptUser < ActiveRecord::Base

  STATUS = {
      :RECEIVED => 1,
      :CLAIMED => 2
  }

  belongs_to :chain
  belongs_to :user
  belongs_to :restaurant
  belongs_to :offer

  def status_name
    PosReceiptUser::STATUS.invert[self.status]
  end

  def find_match_receipt
    pos_receipt = PosReceipt.where(:barcode => self.barcode, :status => PosReceipt::STATUS[:RECEIVED]).first
    return if pos_receipt.blank?
    receipt = Receipt.new(:chain_id => self.chain_id, :user_id => self.user_id, :status => Receipt::STATUS[:RECEIVED])
    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.issue_date = pos_receipt.receipt_time
    receipt_transaction.time_stamp = pos_receipt.receipt_time.strftime("%I:%M%p")
    receipt_transaction.subtotal = pos_receipt.total
    receipt_transaction.tax = pos_receipt.tax
    receipt_transaction.status = Receipt::STATUS[:RECEIVED]
    receipt_transaction.restaurant_id =  self.restaurant_id
    receipt_transaction.restaurant_offer_id = self.restaurant_offer_id
    receipt.receipt_transactions << receipt_transaction
    if receipt.save
      approved_receipt = receipt_transaction.dup
      approved_receipt.status = Receipt::STATUS[:APPROVED]
      approved_receipt.receipt_id = receipt.id
      approved_receipt.save
      receipt.update_attribute(:status, Receipt::STATUS[:APPROVED])
      pos_receipt.update_attribute(:status, PosReceipt::STATUS[:CLAIMED])
      self.update_attribute(:status, PosReceiptUser::STATUS[:CLAIMED])
      pos_check_upload = PosCheckUpload.where(:barcode => self.barcode).first
      pos_check_upload.update_column(:status, PosCheckUpload::STATUS[:PROCESSED]) unless pos_check_upload.blank?
      puts "success----"
    else
      p receipt.errors
      puts "error ----"
    end
  end
end