class ReceiptObserver < ActiveRecord::Observer
  
  def after_create(receipt)
  	if receipt.status == Receipt::STATUS[:RECEIVED] && !receipt.is_receipt_barcode
      #UserMailer.receipt_email(Setting.email.reviewer,receipt).deliver
      Delayed::Job.enqueue(
          ReceiptProcess::ReceiptCallback.new(receipt.id),
          :priority => nil,:run_at => Time.now + 5.minutes
      )
  	end	
  end
  
end
