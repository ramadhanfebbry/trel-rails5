class PromotionHistoryNotification < ActiveRecord::Base

  belongs_to :promotion_notification
  belongs_to :user

  STATUS = {
      1 => "Success",
      2 => "Error"
  }

  scope :success, -> { where("status = 1") }
  scope :error, -> { where("status = 2") }

  scope :device, -> { where("pn_kind = 2") }
  scope :email, -> { where("pn_kind = 1") }

  def self.pending(promotion_notification_id)
    promotion_notification = PromotionNotification.find(promotion_notification_id)
    user_ids = promotion_notification.email[:emails] rescue []
    exist_user_ids = self.where("user_id in (?)", user_ids).map(&:user_id)
    user_ids - exist_user_ids
  end
end
