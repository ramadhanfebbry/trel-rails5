class Tag < ActiveRecord::Base
  has_many :taggings
  has_many :restaurants, :through => :taggings
  attr_accessor :for_user

  validates :category_type, :presence => true,
            :if => Proc.new { |tg| tg.for_user.eql?(true) }
  validates :name, :presence => true, :uniqueness => {:scope => :category_type}

  scope :user_tags, -> { where('category_type is not null') }
  scope :system_tags, -> { where('category_type is null') }


  #validates :min, :max, :presence => true, :numericality => {
  #    :greater_than_or_equal_to => 0,
  #    :only_integer => true
  #}
end