class ReferralCode < ActiveRecord::Base
  extend KeyUtil
  belongs_to :user
  belongs_to :referral_program

  has_many :referral_users , :foreign_key => "code_id"

  scope :by_chain , -> (c) { where("users.chain_id = ?", c.id).joins(:user)}
  scope :by_code , -> (cd) { where(code: cd)}

  ## referrall reward for referee and referer
  def self.push_referral_reward(chain, ref_code, referee,active_referral)
    ## get the refcode
    ref_code = self.by_chain(chain).by_code(ref_code).first
    max_incent = active_referral.max_incentive.to_i
    p "max incent === #{max_incent}"
    # begin
    # maximum referral for the referrer
    if max_incent > 0  and !ref_code.blank?
      #if there are max incentive
      max_referral = ReferralUser.where(
          :code_id => ref_code.id,
          :referral_program_id => active_referral.id
      ).count
      p "max_referral = #{max_referral}"

      if max_referral >= max_incent
        p "come in"
        ref_user = ReferralUser.where(:code_id => ref_code.id, :user_id => referee.id,:referral_program_id => active_referral.id).first
        ReferralUser.create(:code_id => ref_code.id,
                            :user_id => referee.id,
                            :is_get_incentive => false,
                            :referral_program_id => active_referral.id) if ref_user.blank?
        ## cannot get the incentive again
        return true
      end
    end

    ### same device / no
    browser = referee.register_device_type.try(:downcase).eql?("browser")
    browser_compatible = referee.register_device_type.try(:downcase).eql?("browser_compatible")
    p "device_type browser: #{browser}"
    p "device_type browser_compatible: #{browser_compatible}"
    p "ref_code : #{ref_code}"
    if  active_referral.uniq_device == true || browser || browser_compatible
      referer = ref_code.user  unless ref_code.blank?
      dev_id  = referer.blank? ? nil : referer.device_id
      dev_id_referee = referee.device_id

      if User::SIGNUP_STATUS.invert[referee.signup_device_status]  != "GREEN" && !ref_code.blank?  && (!browser && !browser_compatible)
        ref_user = ReferralUser.where(:code_id => ref_code.id, :user_id => referee.id,:referral_program_id => active_referral.id).first
        ReferralUser.create(:code_id => ref_code.id,
                            :user_id => referee.id,
                            :is_get_incentive => false,
                            :referral_program_id => active_referral.id) if ref_user.blank?
        return true
      end
    end

    # end

    unless ref_code.blank?
      ## find user referer to push the reward to the referer
      referer = ref_code.user
      ref_program = active_referral
      ## get the incentive
      incentive_referee = self.get_incentive(ref_program)
      incentive_referer = self.get_incentive_referer(ref_program)
      p incentive_referee
      p incentive_referer
      p "0000000000000000000000000000000000000000000000000000000000000000000000000"
      p !incentive_referee.blank?
      p !incentive_referer.blank?

      ref_user = ReferralUser.where(:code_id => ref_code.id, :user_id => referee.id,:referral_program_id => active_referral.id).first
      if (!incentive_referee.blank? or !incentive_referer.blank?) and ref_user.blank?
        puts "REFERRAL CODE INCENTIVE PUSH"
        puts "***************************************************************************"
        ReferralUser.create(:code_id => ref_code.id, :user_id => referee.id,:referral_program_id => active_referral.id, :is_get_incentive => true, :referral_program_type => active_referral.get_referral_program_type)
        if incentive_referee.class == Reward  || incentive_referer.class == Reward
          puts "REWARD ============="
          ## push to referer
          unless incentive_referer.blank?
            puts "incentive referreer ==== #{incentive_referer.id}"
            RewardWallet.create_with_delay(:reward_id => incentive_referer.id, :user_id => referer.id) if incentive_referer.class == Reward
          end
          ## push to referee
          unless incentive_referee.blank?
            puts "incentive referee ==== #{incentive_referee.id}"
            RewardWallet.create_with_delay(:reward_id => incentive_referee.id, :user_id => referee.id) if incentive_referee.class == Reward
          end
        end

        if incentive_referee.class == ReferralOffer  || incentive_referer.class == ReferralOffer
          puts "Referral CODE :::::: POINTS "
          ## push point to referee and referer
          ## referer push
          unless incentive_referer.blank?
            puts "Referral CODE :::::: POINTS referer earn#{incentive_referer.points}"
            if incentive_referer.class == ReferralOffer
              referer.earn(incentive_referer.points)
              PointHistory.add_to_history(referer.id, "Referer Incentive", incentive_referer.points)
            end
          end
          ##
          unless incentive_referee.blank?
            puts "Referral CODE :::::: POINTS referee earn#{incentive_referee.points}"
            if incentive_referee.class == ReferralOffer
             referee.earn(incentive_referee.points)
             PointHistory.add_to_history(referee.id, "Referee Incentive", incentive_referee.points)
            end
          end
        end

        ## update the user ref for user signup set to nil
        #referee.update_attribute(:referral_code,nil)
      end
    else

    end
  end

  def self.user_ref_code(ref_program,user)
    code = self.where("user_id = ?", user.id).first.code rescue nil

    if code.blank?
      size = Setting.referral_code.code_size || 10
      chars = Setting.referral_code.code_chars
      ## referall code uniq / user
      ## delete first
      begin
        code = self.generate_random_string(size, chars)
        ReferralCode.create(:user_id => user.id, :code => code.downcase)
      rescue => e
        puts "error => #{e.inspect}"
      end
    end
    return code
  end

  def self.get_incentive(ref_program)
    ## if the ref program has an incentive
    incentive = ref_program.incentive
    unless incentive.blank?
      return incentive
    else
      return nil
    end
  end

  def self.get_incentive_referer(ref_program)
    ## if the ref program has an incentive
    incentive = ref_program.incentive_referer
    unless incentive.blank?
      return incentive
    else
      return nil
    end
  end
end
