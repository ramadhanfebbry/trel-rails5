class StreamGalleryImage < ActiveRecord::Base

  belongs_to :chain
  belongs_to :stream_gallery

  validates :feed_link, :url => true, :presence => true, :if => :is_xml_feed_link?
  validates :title, :description, :presence => true, :if => :is_upload_image_embed?

  serialize :image_feed_lists, Array

  EMBED_SOURCE = {
      "URL" => 1,
      "XML FEED LINK" => 2,
      "XML/JSON FILE UPLOAD" => 3,
      "IMAGES AND METADATA UPLOAD" => 4
  }

  has_attached_file :embed,
                    :styles => {
                        :thumb => Setting.paperclip.styles.small
                    },
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                        :access_key_id => Setting.storage.s3_access_key_id,
                        :secret_access_key => Setting.storage.s3_secret_access_key
                    }
  validates_attachment_content_type :embed, :content_type => /\Aimage\/.*\Z/, :if => :is_upload_image_embed?
  validates_attachment_presence :embed, :if => :is_upload_image_embed?

  def is_xml_feed_link?
    input_source.eql?(StreamGalleryImage::EMBED_SOURCE["XML FEED LINK"])
  end

  def is_upload_image_embed?
    input_source.eql?(StreamGalleryImage::EMBED_SOURCE["IMAGES AND METADATA UPLOAD"])
  end

end
