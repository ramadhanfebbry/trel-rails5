class RejectReason < ActiveRecord::Base
  validates_uniqueness_of :reason_key
  validates_uniqueness_of :description

  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
end
