class PromoCodeJob < Struct.new(:quantity, :chain_id, :promotion_id,:current_admin)
  
  def perform
    if quantity.to_i > 0 ## if its static else if shared just send the email
      PromoCode.generate_codes(quantity, chain_id, promotion_id)
    end
  end

  def success(job)
    promotion = job.delayable_type.classify.constantize.find(job.delayable_id)
    PromotionMailer.promo_code_detail(promotion,current_admin).deliver
  end

  def error(job, exception)
    promotion = job.delayable_type.classify.constantize.find(job.delayable_id)
    error_text = job.last_error
    PromotionMailer.failed_generate_code(promotion, error_text).deliver
  end
  
end
