class OloOrderDetail < ActiveRecord::Base
  belongs_to :olo_order
#  after_save :receipt_process

  scope :not_processed, -> { where("processed = ?", false) }


  def receipt_process
    x = self
#    user = User.find(x.user_id) rescue nil
    chain = Chain.where("lower(name) like '%veggie%'").first rescue nil
    restaurant = chain.restaurants.active.where("LOWER(name) like LOWER(?)", '%'+x.store_name+'%').first rescue nil
    user = chain.users.where("users.email = '#{x.email}'").first rescue nil

    #offer = restaurant.offers.unscoped.online_offers.last rescue nil
    unless restaurant.blank?
      offer = Offer.unscoped.joins(:restaurant_offers).where("restaurant_id = #{restaurant.id} and is_online_order is true").last rescue nil
    end
    rest_offer = RestaurantOffer.where(:offer_id => offer.id).last.id rescue nil


    #if !chain.blank? &&  !restaurant.blank?

    receipt = Receipt.new(
        :chain_id => (chain.id rescue nil),
        :user_id => (user.id rescue nil),
        :status => Receipt::STATUS[:RECEIVED],
        :is_olo => true
    )

    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.status = Receipt::STATUS[:RECEIVED]
    receipt_transaction.subtotal = x.subtotal
    receipt_transaction.tax = x.tax
    receipt_transaction.issue_date = x.time_wanted.blank? ? x.time_placed : x.time_wanted
    receipt_transaction.time_stamp = x.issue_time#x.date_pickup.split(' ')[1..2].join(' ') rescue nil
    receipt_transaction.receipt_date = x.time_wanted.blank? ? x.time_placed : x.time_wanted
    receipt_transaction.receipt_time = x.issue_time

    receipt_transaction.restaurant_id = restaurant.id rescue nil
    receipt_transaction.restaurant_offer_id = rest_offer
    receipt_transaction.receipt_number = x.olo_ref
    receipt.receipt_transactions << receipt_transaction
    #Receipt.skip_callbacks = false
    receipt.save #(:callback => false)
    self.update_column(:receipt_id, receipt.id) rescue nil
    self.update_column(:processed, true)

    if user.blank?
      user_on=REDIS.get "content_email_parse_user_on_off_#{chain.id}" rescue nil
      subject = REDIS.get "subject_email_parse_user_reject_#{chain.id}" rescue nil
      content = REDIS.get "content_email_parse_user_reject_#{chain.id}" rescue nil

      if !user_on.blank? and !subject.blank? and !content.blank? # if all the conditions , setting, subject is not blank, and content is not blank
        UserMailer.email_parse_rejected(chain, x.email).deliver! rescue nil
        puts "SENDING EMAILLS"
        return true
      end
    end

    if !offer.blank? && !user.blank?
      today_maxed = false

      if today_maxed == false
        ## check max submit
        receipt.status = Receipt::STATUS[:APPROVED]
        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.status = Receipt::STATUS[:APPROVED]
        receipt_transaction.subtotal = x.subtotal
        receipt_transaction.tax = x.tax
        receipt_transaction.issue_date = x.time_wanted.blank? ? x.time_placed : x.time_wanted
        receipt_transaction.time_stamp = x.issue_time #x.date_pickup.split(' ')[1..2].join(' ') rescue nil
        receipt_transaction.receipt_date = x.time_wanted.blank? ? x.time_placed : x.time_wanted
        receipt_transaction.receipt_time = x.issue_time

        receipt_transaction.restaurant_id = restaurant.id rescue nil
        receipt_transaction.restaurant_offer_id = rest_offer
        receipt_transaction.receipt_number = x.olo_ref
        receipt_transaction.restaurant_offer_id = rest_offer
      end

      is_date = receipt_transaction.issue_date.blank? ? receipt.created_at : receipt_transaction.issue_date
      if receipt.chain.today_olo_max_user_limit?(receipt, is_date)
        today_maxed = true
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.status = Receipt::STATUS[:REJECTED]
        receipt_transaction.instructions = {:user_maxed => "user_maxed"}
      end

      if  Receipt.joins(:receipt_transactions).
          where("chain_id = ? and is_olo = ? and receipt_number = '#{ x.olo_ref}' and receipts.status = 3", chain.id, true).
          select('count(distinct(receipts.*))').first.count.to_i == 1
        puts
        receipt_transaction.status = Receipt::STATUS[:REJECTED]
        receipt.status = Receipt::STATUS[:REJECTED]
        receipt_transaction.instructions = {:receipt => "rejected"}
      end

      # max reached approved receipt for chain


      receipt.receipt_transactions << receipt_transaction
      receipt.save

      self.update_column(:receipt_id, receipt.id) rescue nil
      if ENV['DEFAULT_HOST_EMAIL'].to_s.include?('trelevant')
        receipt.chain.update_dashboard_instant if receipt and receipt.chain
      end
    end
    #end
  end

  # OloOrderDetail.daily_olo_order
  def self.daily_olo_order
    puts "DAILY ORDER SCHEDULER STARTING"

    ## run @ 6 am, after the olo order gather
    case_time = "(
  case when exists(select time_wanted)
  then time_wanted
    else time_placed
  end
  )"
    total_pages = OloOrderDetail.not_processed.where("#{case_time} between date('#{Time.zone.now.in_time_zone('EST') - 1.days}') and date('#{Time.zone.now}')").paginate(:per_page => 500, :page => 1).total_pages
    if total_pages > 0
      1.upto(total_pages).each do |page|
        olo_orders = self.not_processed.where("#{case_time} between date('#{Time.zone.now.in_time_zone('EST') - 1.days}') and date('#{Time.zone.now}')").paginate(:per_page => 500, :page => page)

        olo_orders.each do |o|
          begin
            o.receipt_process
          rescue
            next
          end
        end
      end
    end
    puts "DAILY ORDER SCHEDULER END"
  end

  ## get the batch from the olo
  #OloOrderDetail.olo_order_gathering
  def self.olo_order_gathering
    execution_time = OloExecution.where(:executed_at => Time.zone.now.in_time_zone('EST').to_date).first

    if execution_time.blank? and (Time.zone.now.in_time_zone("EST").hour == 4 || Time.zone.now.in_time_zone("EST").hour == 5)
    # already executed
    ## get the list of the batch ids from yesterday
    # run @ 4 am
    last_day = Time.zone.now.in_time_zone("EST") - 1.days
    parsed_day = last_day.strftime("%Y%m%d")
    #parsed_day = "20141215"


    @olo = Olo::Order.new
    @result = @olo.orders
    doc = Nokogiri.parse(@result)
    #doc.xpath("//orderbatch//generated[contains(text(),'20141215')]").first.parent
    #batch_ids = doc.xpath("//orderbatch//generated[contains(text(),'20141215')]").first.parent
    #doc.xpath("//orderbatch//generated[contains(text(),'20141215')]").each {|x|x.parent.xpath('//batchId')}
    a = []
    doc.xpath("//orderbatch//generated[contains(text(),'#{parsed_day}')]").map(&:parent).each { |x| a << x.children[1].text }

    batch_ids = a.flatten
    unless batch_ids.blank?
      batch_ids.each do |batch_id|
        @x = Olo::Order.new
        @x.get_batch(batch_id, ENV['OLO_CHAIN_IDS'])
      end
    end

    OloExecution.create(:executed_at => Time.zone.now.in_time_zone('EST').to_date, :status => 3)
    end
  end
end
