class UserProfile < ActiveRecord::Base

  belongs_to :user

  has_attached_file :avatar,
                    :styles => { :small => Setting.paperclip.styles.small,
                                 :large => Setting.paperclip.styles.large },
                    :storage => :s3,
                    :bucket => Setting.storage.s3_bucket,
                    :s3_credentials => {
                        :access_key_id => Setting.storage.s3_access_key_id,
                        :secret_access_key => Setting.storage.s3_secret_access_key
                    }
  validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :on=>:create

  def self.mall_employee_value(value)
    if value
      return true if value.to_s.downcase.eql?("1")
      return false
    else
      false
    end
  end

end
