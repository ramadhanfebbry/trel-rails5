class DelayedJob < ActiveRecord::Base
  unloadable

  belongs_to :chain
  belongs_to :delayable, :polymorphic => true

  scope :with_type ,   -> (t) { where(delayable_type: t) if t.present? }
  scope :reward_jobs,  -> { with_type("Reward") }
  scope :report_jobs,  -> { with_type("ReportSubscription") }
  scope :by_chain_id,  -> (c_id) {where(chain_id:  c_id) if c_id.present? }
  scope :running,      -> { where("locked_at is not null and last_error is null") }
  scope :errors,       -> { where("last_error is not null") }


  def self.reset_handler
    DelayedJob.all.each{|x| x.update_attributes(:attempts => 0, :last_error => nil, :run_at => Time.zone.now, :locked_at => nil)}
  end

end
