class GeoDataSetting < ActiveRecord::Base
  belongs_to :chain

  validates :n_days_to_post_from_today , :presence => true,
           :numericality => {:greater_than => 0 }

  validates :num_days_retry , :presence => true,
           :numericality => {:greater_than => 0 }


  def collecting_start_time
    self.collect_start_time.strftime('%H:%M %P')
  end

  def collecting_end_time
    self.collect_end_time.strftime('%H:%M %P')
  end


  def collecting_post_start_time
    self.post_start_time.strftime('%H:%M %P')
  end

  def collecting_post_end_time
     self.post_end_time.strftime('%H:%M %P')
  end
end
