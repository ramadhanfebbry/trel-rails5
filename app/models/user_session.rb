class UserSession < ActiveRecord::Base
  #reverse_geocoded_by :lat, :long do |obj,results|
  #  if geo = results.first
  #    p "GEO IS #{geo}"
  #    obj.city    = geo.city
  #    obj.postal_code = geo.postal_code
  #    obj.country_code = geo.country_code
  #    obj.address = geo.address
  #  end
  #end
  ##attr_accessible :address, :latitude, :longitude, :user_id
  #
  #
  #after_validation :reverse_geocode
  belongs_to :user
  belongs_to :chain
  belongs_to :receipt

  after_create :set_code_sequence

  def self.initialize_code_sequence
    Chain.all.each do |chain|
      AppcodeSetting::CODE_TYPES.values.each do |code_type|
        create_and_update_sequence(chain, code_type)
      end
    end
  end

  def self.create_and_update_sequence(chain, code_type)
    return if code_type == 3
    begin
      result = ActiveRecord::Base.connection.execute("SELECT last_value FROM code_sequence_#{chain.id}_#{code_type};")
    rescue ActiveRecord::StatementInvalid => e
       if e.message.include?("PG::UndefinedTable")
         ActiveRecord::Base.connection.execute("CREATE SEQUENCE code_sequence_#{chain.id}_#{code_type};")
       end
     end
    if code_type == 1
      #USERCODE COUNT
      user_code_setting = chain.user_code_setting
      user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
      user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
      code_count = 0
      if user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["USERCODE"]).count
      elsif user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["USERCODE"]).count
      end
      ActiveRecord::Base.connection.execute("SELECT setval('code_sequence_#{chain.id}_#{code_type}', #{code_count+1}, TRUE);")
    elsif code_type == 2 #&& eval(ENV["BRAINTREE_PAYMENT_CHAIN_IDS"]).include?(chain.id)
      #PAYCODE
      pay_code_setting = chain.pay_code_setting
      pay_code_setting = JSON.parse(pay_code_setting.to_json) rescue nil
      pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
      code_count = 0
      if pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + pay_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["PAYCODE"]).count
      elsif pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + pay_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["PAYCODE"]).count
      end
      ActiveRecord::Base.connection.execute("SELECT setval('code_sequence_#{chain.id}_#{code_type}', #{code_count+1}, TRUE);")
    elsif code_type == 4
      #REWARD CODE
      reward_code_setting = chain.reward_code_setting
      reward_code_setting = JSON.parse(reward_code_setting.to_json) rescue nil
      reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
      code_count = 0
      if reward_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + reward_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["REWARDCODE"]).count
      elsif reward_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + reward_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["REWARDCODE"]).count
      end
      ActiveRecord::Base.connection.execute("SELECT setval('code_sequence_#{chain.id}_#{code_type}', #{code_count+1}, TRUE);")
    elsif code_type == 5
      #GIFTCODE COUNT
      gift_code_setting = chain.gift_code_setting
      gift_code_setting = JSON.parse(gift_code_setting.to_json) rescue nil
      gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
      code_count = 0
      if gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).count
      elsif gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).count
      end
      ActiveRecord::Base.connection.execute("SELECT setval('code_sequence_#{chain.id}_#{code_type}', #{code_count+1}, TRUE);")
    end
  end

  def set_code_sequence
    # return if self.code_type == 2 && eval(ENV["BRAINTREE_PAYMENT_CHAIN_IDS"]).include?(self.chain_id)
    begin
      result = ActiveRecord::Base.connection.execute("SELECT last_value FROM code_sequence_#{self.chain_id}_#{self.code_type};")
      self.update_column(:code_sequence, result.getvalue(0,0))
      ActiveRecord::Base.connection.execute("SELECT setval('code_sequence_#{self.chain_id}_#{self.code_type}', #{result.getvalue(0,0).to_i+1}, TRUE);")
    rescue ActiveRecord::StatementInvalid => e
      if e.message.include?("PG::UndefinedTable")
        ActiveRecord::Base.connection.execute("CREATE SEQUENCE code_sequence_#{self.chain_id}_#{self.code_type};")
        self.update_column(:code_sequence, 1)
        ActiveRecord::Base.connection.execute("SELECT setval('code_sequence_#{self.chain_id}_#{self.code_type}', 2, TRUE);")
      end
    end
  end

  def self.set_seq_for_existing_code
    Chain.all.each do |chain|
      AppcodeSetting::CODE_TYPES.values.each do |code_type|
        set_sequence_number(chain, code_type)
      end
    end
  end

  def self.set_sequence_number(chain, code_type)
    return if code_type == 3
    if code_type == 1
      #USERCODE COUNT
      user_code_setting = chain.user_code_setting
      user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
      user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
      if user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
        sql = "
            update user_sessions c set code_sequence = c2.seqnum from
              (select c2.*, row_number() over () as seqnum from user_sessions c2 WHERE code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = 1) c2
            where c2.id = c.id;"
        ActiveRecord::Base.connection.execute(sql)
      elsif user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
        sql = "
            update user_sessions c set code_sequence = c2.seqnum from
              (select c2.*, row_number() over () as seqnum from user_sessions c2 WHERE code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = 1) c2
            where c2.id = c.id;"
        ActiveRecord::Base.connection.execute(sql)
      end
    elsif code_type == 2 #&& eval(ENV["BRAINTREE_PAYMENT_CHAIN_IDS"]).include?(chain.id)
      #PAYCODE
      pay_code_setting = chain.pay_code_setting
      pay_code_setting = JSON.parse(pay_code_setting.to_json) rescue nil
      pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
      code_count = 0
      if pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + pay_code_setting["pos_digit_barcode"].to_s + '}$'
        sql = "
            update user_sessions c set code_sequence = c2.seqnum from
              (select c2.*, row_number() over () as seqnum from user_sessions c2 WHERE code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = 2) c2
            where c2.id = c.id;"
        ActiveRecord::Base.connection.execute(sql)
      elsif pay_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + pay_code_setting["pos_digit_barcode"].to_s + '}$'
        sql = "
            update user_sessions c set code_sequence = c2.seqnum from
              (select c2.*, row_number() over () as seqnum from user_sessions c2 WHERE code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = 2) c2
            where c2.id = c.id;"
        ActiveRecord::Base.connection.execute(sql)
      end
    elsif code_type == 4
      #REWARD CODE
      reward_code_setting = chain.reward_code_setting
      reward_code_setting = JSON.parse(reward_code_setting.to_json) rescue nil
      reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
      code_count = 0
      if reward_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + reward_code_setting["pos_digit_barcode"].to_s + '}$'
        sql = "
            update user_sessions c set code_sequence = c2.seqnum from
              (select c2.*, row_number() over () as seqnum from user_sessions c2 WHERE code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = 4) c2
            where c2.id = c.id;"
        ActiveRecord::Base.connection.execute(sql)
      elsif reward_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + reward_code_setting["pos_digit_barcode"].to_s + '}$'
        sql = "
            update user_sessions c set code_sequence = c2.seqnum from
              (select c2.*, row_number() over () as seqnum from user_sessions c2 WHERE code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = 4) c2
            where c2.id = c.id;"
        ActiveRecord::Base.connection.execute(sql)
      end
    elsif code_type == 5
      #GIFTCODE COUNT
      gift_code_setting = chain.gift_code_setting
      gift_code_setting = JSON.parse(gift_code_setting.to_json) rescue nil
      gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
      if gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        sql = "
            update user_sessions c set code_sequence = c2.seqnum from
              (select c2.*, row_number() over () as seqnum from user_sessions c2 WHERE code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = 5) c2
            where c2.id = c.id;"
        ActiveRecord::Base.connection.execute(sql)
      elsif gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        sql = "
            update user_sessions c set code_sequence = c2.seqnum from
              (select c2.*, row_number() over () as seqnum from user_sessions c2 WHERE code ~ '#{regex}' AND chain_id = #{chain.id} AND code_type = 5) c2
            where c2.id = c.id;"
        ActiveRecord::Base.connection.execute(sql)
      end
    end
  end

end