class BirthdayRewardNotification < ActiveRecord::Base

  belongs_to :notifiable, :polymorphic => true
  belongs_to :locale
  
end
