# encoding: utf-8
class Chain < ActiveRecord::Base
  include AASM
  include Fishbowl::Api::Frequest
  include RelevantRitas
  include NetPos::Core
  include RelevantGuestDna
  include TechStudio::Relevant::Connect

  after_save :set_participate_and_locales, :create_or_update_ldap, :create_hybrid_reward_redemption

  #validates :name, :length => {:maximum=>100}, :presence => true, :format => {:with => /\A[-a-zA-Z0-9 ()-]+\z/,
  validates :name, :length => {:maximum=>100}, :presence => true, :format => {:with => /\A[\-a-zA-Z0-9 p{L} ()-]+\z/,
    :message => "(Only alphanumeric and special(-) characters allowed)"}

  #  validates_numericality_of :points, :greater_than_or_equal_to=>1, :presence => true, :on => :create

  validates :points, :numericality => {:greater_than => 0}, :format => {:with => /\A\d+\.*\d{0,2}\z/, :message => "accept decimal value upto 2  decimal value, eg : 1.50 ,0.01 ,11.35"}, :presence => true
  validates :max_subtotal, :numericality => {:greater_than_or_equal_to => 0}, :format => {:with => /\A\d+\.*\d{0,2}\z/, :message => "accept decimal value upto 2  decimal value, eg : 1.50 ,0.01 ,11.35"}, :presence => true
  
  # validates :email, :presence => true, :format => {:with => eval(Setting.chain.email_regex), :message => "Please add valid email address"}, :if => :email_validation_required?
  # validates :survey_email, :format => {:with => eval(Setting.chain.email_regex), :message => "Please add valid email address", :allow_blank => true}

  validates :hours_delay_reward, :numericality => {
    :greater_than_or_equal_to => 0,
    :only_integer => false,
    :less_than_or_equal_to => 280
  }

  validates :report_period, :numericality => {
    :greater_than_or_equal_to => 0,
    :only_integer => true,
    :less_than_or_equal_to => 365,
    :allow_blank => true
  }

  validates :mailchimp_api_key, :mailchimp_list_id, :presence => true, :if => :mailchimp_required?
  validates :point_signup_incentive, :numericality => {
    :greater_than => 0,
    :only_integer => true
  }, :if => :point_added_required?

  validates :fb_signup_incentive_point, :numericality => {
      :greater_than => 0,
      :only_integer => true
  }, :if => :fb_point_added_required?

  validates :survey_incentive_daily_cap, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }
  validates :survey_incentive_weekly_cap, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }
  validates :survey_incentive_monthly_cap, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }

  validates :ren_number_of_days, :numericality => {
      :greater_than => 0,
      :only_integer => true
  }

  validates :pos_receipt_processing_delay, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }

  validate :end_date_cannot_greater_than_start_date

  validates :max_number_of_locations, :numericality => {
      :greater_than_or_equal_to => 10,
      :less_than_or_equal_to => 100,
      :only_integer => true
  }

  validates :no_of_days_micros_table_receipt_generated, :numericality => {
      :greater_than_or_equal_to => 0,
      :less_than_or_equal_to => 500,
      :only_integer => true
  }

  validates :maximum_offer_points_earned_per_day, :numericality => {
      :greater_than_or_equal_to => 0,
  }

  validates :code_scan_range, :numericality => { :greater_than_or_equal_to => 0}
  validates :netpos_batch_check_request, :numericality => {
                                   :greater_than => 0,
                                   :only_integer => true
                               }

  has_attached_file :rewards_image, :styles => { :medium => "400x400>", :thumb => "150x150>" },
                    :storage => :s3,
                    :s3_credentials => "#{Rails.root}/config/s3.yml",
                    :path => "checkin/rewards_image/:style/:id/:filename"

  #validates_attachment_presence :rewards_image

  before_save :destroy_rewards_image?


  attr_accessor :owners_list, :selected_owners, :locales_list, :selected_locales, :email_validation, :user_sync_required, :delete_reward_image

  ROUNDING_OPTIONS = {
      "ROUND" => "round",
      "ROUND UP" => "ceil",
      "ROUND DOWN" => "floor"
  }

  BARCODE_FORMAT = {
      "Numeric" => 1,
      "Alphanumeric" => 2
  }

  BARCODE_FORMAT_TYPES = {
      "Code 39" => 1,
      "Code 39 (Full ASCII)" => 2,
      "Code 39 (HIBC)" => 3,
      "CodaBar" => 4,
      "Code 93" => 5,
      "Code 128" => 6,
      "UCC/EAN 128" => 7,
      "Interleaved 2 Of 5" => 8,
      "PostNET" => 9,
      "UPC-A" => 10,
      "UPC-E" => 11,
      'EAN 8' => 12,
      "EAN 13" => 13,
      "BookLand" => 14,
      "MSI/Plessey" => 15
  }

  REWARD_REDEEM_FLOW = {
      "FROM APP" => 1,
      "FROM POS" => 2,
      "HYBRID SETTING" => 3
  }

  POS_USED_TYPE = {
      "NO POS" => 0,
      "MICROS/NORTHKEY" => 1,
      "XPIENT" => 2,
      "NCR" => 3,
      "TREATWARE" => 4,
      "NETPOS" => 5,
      "FOCUS" => 6,
      "BRIDG" => 7
  }

  POS_DISCOUNT_TYPE = {
      "Discount Least priced qualifying item" => 1,
      "Discount Highest priced qualifying item" => 2
  }

  REWARD_CODE_GENERATED_TYPE = {
      "Generate POS Code" => 1,
      "Upload POS Code" => 2,
      "Fishbowl Promotion Coupons Code" => 3
  }

  PAYMENT_PROCESSOR_TYPE = {
      "BRAINTREE" => 1,
      "MERCURYPAY" => 2
  }

  default_scope { where("deleted_at" => nil) }
  scope :active, -> { where("chains.status = 'active'") }
  scope :inactive, -> { where("chains.status = 'inactive'") }
  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  aasm :column => :status, :whiny_transitions => false do
    state :active, :initial => true
    state :inactive

    event :activate do
      transitions :from => :inactive, :to => :active
    end

    event :deactivate do
      transitions :from => :active, :to => :inactive
    end

  end

  ## relation
  has_many :dashboard_questions, :dependent => :destroy
  has_many :chain_image_shares
  has_many :restaurants
  has_many :offers
  has_many :online_offers
  has_many :perks
  has_many :rewards
  has_many :receipts
  has_many :surveys
  has_many :chain_reward_events
  has_many :referral_programs
  has_many :referral_locales, :as => :notifiable, :dependent => :destroy, :class_name => "ReferralNotification"
  has_one  :report_subscription
  has_one  :geo_data_setting
  has_many :social_shares

  has_many :delayed_jobs, :foreign_key => "chain_id", :dependent => :destroy
  has_many :chain_locales, :dependent => :destroy
  has_many :locales, :through => :chain_locales

  #has_and_belongs_to_many :owners
  has_many :chains_owners
  has_many :owners, :through => :chains_owners

  has_many :users
  has_many :applications
  has_many :deals

  has_many :promotion_locales
  has_many :milestones
  has_many :chain_urls

  has_many :goal_categories
  has_many :goals
  has_many :user_goal_categories
  has_many :chain_urls
  has_many :notification_locales, :as => :notifiable, :dependent => :destroy
  has_many :birthday_reward_notifications, :as => :notifiable, :dependent => :destroy
  has_many :point_push_notifications, :dependent => :destroy
  has_many :steppingstones
  has_many :milestone_notifications, :dependent => :destroy
  has_many :account_notifications, :dependent => :destroy
  has_many :reward_share_texts, :dependent => :destroy
  has_many :gift_notifications, :dependent => :destroy
  has_many :game_notifications, :dependent => :destroy
  has_many :reset_email_notifications, :dependent => :destroy
  has_many :steppingstone_notifications, :dependent => :destroy
  has_many :game_point_notifications, :dependent => :destroy
  has_many :admin_push_reward_notifications, -> { where(send_type: 1) }, :class_name => "AdminNotificationLocale"
  has_many :admin_push_point_notifications, -> { where(send_type: 2) }, :class_name => "AdminNotificationLocale"
  has_many :reward_exp_notifications, :dependent => :destroy
  has_many :active_reward_notifications, :dependent => :destroy
  has_many :notification_settings, :dependent => :destroy
  has_many :giftcard_notification_emails, :dependent => :destroy

  has_one :survey_response_alert
  has_one :game
  has_one :task_definition
  has_one :no_scan_definition
  has_one :appcode_setting
  has_one :partner_code_setting
  has_one :chain_setting_alert
  has_many :giftcard_skins
  has_many :push_service_notification_certificates

  has_many :privacy_policies
  has_many :promotion_offers
  has_many :terms
  has_many :faqs
  has_many :faqimages
  has_many :bucket_paths
  has_many :launch_texts
  has_many :receipt_submission_success_texts, -> { where(message_for: ApiResponseText::MESSAGE_FOR["Receipt Submission Success"]) }, class_name: "ApiResponseText"
  has_many :receipt_submission_fail_texts, -> { where(message_for: ApiResponseText::MESSAGE_FOR["Receipt Submission Fail"]) }, class_name: "ApiResponseText"
  has_many :signup_success_with_referral_code_texts, -> { where(message_for: ApiResponseText::MESSAGE_FOR["Signup Success With Referral Code"]) }, class_name: "ApiResponseText"
  has_many :signup_success_without_referral_code_texts, -> { where(:message_for => ApiResponseText::MESSAGE_FOR["Signup Success Without Referral Code"]) }, class_name: "ApiResponseText"
  has_many :survey_submission_success_texts, -> { where( message_for: ApiResponseText::MESSAGE_FOR["Survey Submission Success"]) }, class_name: "ApiResponseText"
  has_many :survey_submission_fail_texts, -> { where( :message_for => ApiResponseText::MESSAGE_FOR["Survey Submission Fail"]) }, class_name: "ApiResponseText"
  has_many :api_response_texts
  has_many :general_menu_items
  has_many :pos_menu_items
  has_many :email_templates, dependent: :destroy
  has_many :partner_categories
  has_many :partner_sub_categories
  has_many :partner_menu_items

  has_one :fishbowl_setting
  has_one :fishbowl_promotion_setting
  has_many :user_fishbowls
  has_many :pos_locations
  has_one :pay_code_setting, -> { where(:code_type => AppcodeSetting::CODE_TYPES["PAYCODE"])}, class_name: "AppcodeSetting"
  has_one :user_code_setting, -> { where(:code_type => AppcodeSetting::CODE_TYPES["USERCODE"])}, class_name: "AppcodeSetting"
  has_one :receipt_code_setting, -> { where(:code_type => AppcodeSetting::CODE_TYPES["RECEIPTCODE"])}, class_name: "AppcodeSetting"
  has_one :reward_code_setting, -> { where(:code_type => AppcodeSetting::CODE_TYPES["REWARDCODE"])}, class_name: "AppcodeSetting"
  has_one :gift_code_setting, -> { where(:code_type => AppcodeSetting::CODE_TYPES["GIFTCODE"])}, class_name: "AppcodeSetting"
  has_one :btree_setting
  has_one :stream_gallery
  has_many :raffles, :dependent => :destroy
  has_one :netpos_setting, :dependent => :destroy
  has_one :emma_setting
  has_many :emma_logs
  has_one :vantiv_credential
  has_many :vantiv_gift_cards
  has_one :chain_setting
  has_one :olorewards_setting
  has_many :user_activity_notifications
  has_one :momentfeed_setting
  has_one :external_setting

  accepts_nested_attributes_for :promotion_locales
  accepts_nested_attributes_for :notification_locales
  accepts_nested_attributes_for :birthday_reward_notifications
  accepts_nested_attributes_for :point_push_notifications
  accepts_nested_attributes_for :referral_locales
  accepts_nested_attributes_for :milestone_notifications
  accepts_nested_attributes_for :account_notifications
  accepts_nested_attributes_for :reward_share_texts
  accepts_nested_attributes_for :survey_response_alert
  accepts_nested_attributes_for :gift_notifications
  accepts_nested_attributes_for :game_notifications
  accepts_nested_attributes_for :reset_email_notifications
  accepts_nested_attributes_for :steppingstone_notifications
  accepts_nested_attributes_for :game_point_notifications
  accepts_nested_attributes_for :admin_push_reward_notifications
  accepts_nested_attributes_for :admin_push_point_notifications
  accepts_nested_attributes_for :launch_texts
  accepts_nested_attributes_for :receipt_submission_success_texts
  accepts_nested_attributes_for :receipt_submission_fail_texts
  accepts_nested_attributes_for :signup_success_with_referral_code_texts
  accepts_nested_attributes_for :signup_success_without_referral_code_texts
  accepts_nested_attributes_for :survey_submission_success_texts
  accepts_nested_attributes_for :survey_submission_fail_texts
  accepts_nested_attributes_for :fishbowl_setting
  accepts_nested_attributes_for :reward_exp_notifications
  accepts_nested_attributes_for :active_reward_notifications
  accepts_nested_attributes_for :pay_code_setting
  accepts_nested_attributes_for :user_code_setting
  accepts_nested_attributes_for :receipt_code_setting
  accepts_nested_attributes_for :reward_code_setting
  accepts_nested_attributes_for :partner_code_setting
  accepts_nested_attributes_for :gift_code_setting
  accepts_nested_attributes_for :btree_setting
  accepts_nested_attributes_for :netpos_setting
  accepts_nested_attributes_for :email_templates
  accepts_nested_attributes_for :giftcard_notification_emails
  accepts_nested_attributes_for :chain_setting
  accepts_nested_attributes_for :fishbowl_setting
  accepts_nested_attributes_for :fishbowl_promotion_setting

  before_save :set_value_delay_reward
  before_create :set_ldap_identity


  def end_date_cannot_greater_than_start_date
    unless report_end_date.blank? and report_start_date.blank?
      begin
        if report_start_date > report_end_date
          errors.add(:report_start_date, "must be less that end date")
          errors.add(:report_end_date, "must be greater than start date")
        end
      rescue => e
        puts "#{e.inspect}"
      end
    end
  end

  def set_value_delay_reward
    self.hours_delay_reward = self.hours_delay_reward.round(2) unless self.hours_delay_reward.blank?
  end

  def email_validation_required?
    !self.email_validation.blank?
  end

  def update_locales(locales, old_locales)
    ids = old_locales.blank? ? [] : old_locales.split
    intersection = locales & ids;
    if (!ids.blank?)
      reset_locales((ids-intersection))
    end
    if (!locales.blank?)
      add_locales((locales-intersection));
    end
  end

  def update_participation(owners, old_participants)
    owners ||= []
    ids = old_participants.blank? ? [] : old_participants.split
    intersection = owners & ids
    if (!ids.blank?)
      pullout((ids-intersection))
    end
    if (!owners.blank?)
      participate((owners-intersection));
    end
  end

  def pullout(owners)
    self.chains_owners.where("owner_id in (?)", owners).destroy_all
  end

  def participate(owners)
    owners.each { |owner_id| ChainsOwner.create(:owner_id => owner_id, :chain_id => self.id) }
  end

  def reset_locales(locales)
    self.chain_locales.where("locale_id in (?)", locales).destroy_all
  end

  def add_locales(locales)
    locales.each { |locale_id| ChainLocale.create(:locale_id => locale_id, :chain_id => self.id) }
  end

  def activeRewards
    self.rewards.where('"isActive" = ? and "expiryDate" > ?', true, Time.now)
  end

  def self.count_active_members(restaurant_list,total_signup)
    members = Receipt.active_membership(restaurant_list).first.count.to_i
    return (members.to_f / total_signup.to_f) * 100
  end

  # def self.new_count_active(chain,restaurant_list, total_signup)
  #   restaurant_id_sql = restaurant_list.map(&:id).join(',')
  #
  #   members = User.find_by_sql("
  #   SELECT COUNT(*) FROM restaurant_users
  #   inner join users on users.id = restaurant_users.user_id
  #   where users.active = 't' and users.chain_id = #{chain.id} and
  #     restaurant_id in(#{restaurant_id_sql})
  #   and ( last_active >= date('#{Time.now - 120.days}') or last_redeem >= date('#{Time.now - 120.days}') )
  #   ").
  #   first.count.to_i
  #
  #   return (members.to_f / total_signup.to_f) * 100
  #end

  def self.new_count_active(chain,restaurant_list, total_signup)
    total_member = 0
    restaurant_list.each do |rs|
      total_member = total_member + REDIS.get("total_new_count_active_chain_#{chain.id}_#{rs.id}").to_i
    end
    return (total_member.to_f / total_signup.to_f) * 100
  end

#  def self.new_count_active(chain,restaurant_list, total_signup)
#    restaurant_id_sql = restaurant_list.map(&:id).join(',')
#
#    members = User.find_by_sql("select count(users.*) from (
#select user_id , date(max(max)) from (
#select user_id, max from (
# SELECT user_id, max(last_active) FROM restaurant_users
# inner join users on users.id = restaurant_users.id
# WHERE users.active = 't ' and
# (restaurant_id in (#{restaurant_id_sql})) group by user_id
# ) as s
#UNION
#select user_id, max(created_at) from reward_transactions where
#restaurant_id in(#{restaurant_id_sql})
#group by user_id
#) as x group by user_id ) as mm
#inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
#and date >= date('#{Time.now - 120.days}')
#").first.count.to_i
#
#    return (members.to_f / total_signup.to_f) * 100
#  end

  def self.count_members(owner,chains, restaurant_list = nil)
    count = 0

    ## if chain owner
    if owner.role_id == 1 || owner.role_id.blank?
      # chains.each do |c|
      #   count = count + c.users.where("active = ?", true).count
      # end
      #restaurant_list.each do |restaurant|
      #  count += restaurant.all_total_member
      #end
      count = REDIS.get "count_member_chain_#{owner.chain.id}"
    else  ## if restaurant owner
      restaurant_list.each do |restaurant|
        count += restaurant.all_total_member
      end
    end

    return count
  end
  #def self.count_members(chains,restaurant_list = nil)
  #  count = 0
  #  unless restaurant_list.blank?
  #    count = RestaurantUser.where("restaurant_id in (?)", restaurant_list.map(&:id)).select("count(distinct user_id)").first.count.to_i
  #  end
  #
  #  return count
  #end

  def self.new_count_members(chain, restaurant_list = nil)
    user_ids = chain.users.map(&:id)
    user_ids = user_ids.join(',')
    restaurant_ids = restaurant_list.map(&:id).join(',')

    val =  Receipt.find_by_sql("select sum(t.count),  t.restaurant_id  as res_id from
     (SELECT count(distinct receipts.user_id),MIN(date(TIMEZONE('UTC',
     (
    case when receipt_transactions.issue_date is null
    then receipts.created_at
     else receipt_transactions.issue_date
     end)
     AT TIME ZONE 'EST'))) as date
    ,MIN(receipt_transactions.restaurant_id) as restaurant_id
    FROM receipts inner join receipt_transactions on
     receipts.id = receipt_transactions.receipt_id
     inner join restaurants on restaurants.id = receipt_transactions.restaurant_id
     WHERE receipts.deleted_at IS NULL AND (receipt_transactions.restaurant_id in (#{restaurant_ids})
     and
     receipts.user_id in (#{user_ids}))
    GROUP BY receipts.user_id   order by receipts.user_id ) t

     group by  t.restaurant_id ORDER BY restaurant_id  asc")
    res = {}
    unless val.blank?
       val.each do |v|
         res.merge!({"#{v.res_id}"  => v.sum})
       end
    end
    return res
  end

  def set_participate_and_locales
    update_participation(self.owners_list, self.selected_owners)
    update_locales(self.locales_list, self.selected_locales)
  end

  def save_email_template_to_redis
    keys = REDIS.hkeys "chain_#{self.id}"
    keys.each do |key|
      REDIS.hdel "chain_#{self.id}", key
    end
    self.locales.each do |locale|
      template_module = case locale.key
      when 'en'
        ChainEmailTemplates::En
      when 'es'
        ChainEmailTemplates::Es
      else
        ChainEmailTemplates::En
      end

      ['welcome', 'receipt_reject', 'forgot_password', 'welcome_chain_owner', 'welcome_rest_owner'].each do |e|
        REDIS.hset "chain_#{self.id}", "#{locale.key}_#{e}_email_subject", template_module.send("#{e}_email_subject", self.name)
        REDIS.hset "chain_#{self.id}", "#{locale.key}_#{e}_email_body", template_module.send("#{e}_email_body", self.name)
      end
    end
  end

  def members_chart(filter)
    chart_data = self.users.where("date(users.created_at) between ? and ?", (Time.now - 1000.days).to_date, Time.now.to_date).group("users.created_at").select("count(users.id),date(users.created_at)")
    return chart_data.map{|x| [x.date.to_datetime.to_i * 1000, x.count]}
  end

  # move to redis
  # def self.today_users(chains,owner,restaurant_list = nil)
  #   if owner.chain_owner?
  #     dt_zone = "date(TIMEZONE('UTC', users.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
  #     return User.where("#{dt_zone} = date(?) and chain_id in(?)", Date.today, chains.map(&:id)).count
  #   end
  #
  #
  #   dt_zone_created_at = "date(TIMEZONE('UTC', created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
  #   #dt_zone_joined_date = "date(TIMEZONE('UTC', joined_date) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
  #   restaurant_list = owner.restaurant_list
  #
  #   return RestaurantUser.where("restaurant_id in (?) and (#{dt_zone_created_at} = ?)", restaurant_list,Date.today).count
  # end

  def self.today_users(chains,owner,restaurant_list = nil)
      if owner.chain_owner?
        return REDIS.get "total_signup_chain_#{chains.first.id}"
      end

      #return RestaurantUser.where("restaurant_id in (?) and (#{dt_zone_created_at} = ?)", restaurant_list,Date.today).count
    total = 0
    restaurant_list.each do |rs|
      total = total + REDIS.get("total_signup_restaurant_#{rs.id}").to_i
    end
    return total
  end

  def image_share
    img = ChainImageShare.active_image(Date.today).where("chain_id = #{self.id}").first
    img.image_url unless img.blank?
  end

  def group_message_pn_by_locale
    hash_map = {}
    self.locales.each do |locale|
      notification = self.notification_locales.where("locale_id = ?", locale.id).first
      hash_map[locale.id] = {} if hash_map[locale.id].blank?
      hash_map[locale.id]["email_subject"] = notification.blank? ? "" : notification.email_subject
      hash_map[locale.id]["email_content"] = notification.blank? ? "" : notification.email_content
      hash_map[locale.id]["email_content_html"] = notification.blank? ? "" : notification.email_content_html
      hash_map[locale.id]["notification"] = notification.blank? ? "" : notification.notification
    end
    return hash_map
  end

  def group_message_pn_birthday_reward_by_locale
    hash_map = {}
    self.locales.each do |locale|
      notification = self.birthday_reward_notifications.where("locale_id = ?", locale.id).first
      hash_map[locale.id] = {} if hash_map[locale.id].blank?
      hash_map[locale.id]["email_subject"] = notification.blank? ? "" : notification.email_subject
      hash_map[locale.id]["email_content"] = notification.blank? ? "" : notification.email_content
      hash_map[locale.id]["email_content_html"] = notification.blank? ? "" : notification.email_content_html
      hash_map[locale.id]["notification"] = notification.blank? ? "" : notification.notification
    end
    return hash_map
  end

  def steppingstone_process(receipt_transaction, process_name = "receipt_approved")
    p "---------- start steppingstone process-------------------"
    selected_steppingstone = self.steppingstones.where("step_type = ?", process_name).first
    return if selected_steppingstone.blank?
    selected_steppingstone.receipt_transaction = receipt_transaction
    p "selected_steppingstone"
    p selected_steppingstone
    case selected_steppingstone.step_type
    when "receipt_approved"
      selected_steppingstone.receipt_approved_process
    end
    p "----------------- end steppingstone process ------------------------"
  end

  def bucket_process(receipt_transaction, user)
    p "---------- start bucket process-------------------"
    user_tag = user.user_tag
    return if user_tag.blank?
    selected_buckets = self.bucket_paths.where("tag_id = ? AND active is TRUE", user_tag.tag_id)
    return if selected_buckets.blank?
    selected_buckets.each do |selected_bucket|
      selected_bucket.receipt_transaction = receipt_transaction
      p "selected_bucket for #{selected_bucket.name}"
      p selected_bucket
      selected_bucket.receipt_approved_process
      p "****** end bucket process for #{selected_bucket.name}"
    end
    p "----------------- end bucket process ------------------------"
  end

  def count_ref_code
    c = self
    a = Chain.find_by_sql("select referral_codes.id from referral_codes inner join users on users.id = referral_codes.user_id and users.chain_id = #{c.id} limit 1")
    return a.size
  end

  def self.send_report(role_id = 1)
    puts "=================== send daily for chain owner report =============== "
    self.send_daily_report(role_id)
    puts "==================== done =========================== "
    puts "=================== send weekly report for chain owner =============== "
    self.send_weekly_report(role_id)
    puts "==================== done =========================== "
    puts "=================== send monthly report for chain owner =============== "
    self.send_monthly_report(role_id)
    puts "==================== done =========================== "
    # puts "=================== send periodic report =============== "
    # self.send_periodic_report(role_id)
    # puts "==================== done =========================== "
  end

  def self.send_custom_report(owner, report_type, start_date, end_date, force = true)
    chain = owner.chains.first
    report = Report.where(:chain_id => chain.id, :kind => report_type, :start_date => start_date, :end_date => end_date).first
    if force ## rerun again the report
      report.destroy unless report.blank?
      report = []
    end
    unless report.blank?
      ChainMailer.report_owner(owner, report).deliver
    else
      Delayed::Job.enqueue(ReportJob::DailyJob.new(chain.id, report_type, start_date, end_date, owner, "PDF"))
    end
  end

  def self.reset_report(role_id, date, report_type)
    report = Report.where(:kind => report_type, :start_date => date, :end_date => date).delete_all
    owners = Owner.where(:role_id => role_id, :is_active => true)
    owners.each do |owner|
      chain = owner.chains.first
      if chain
        report = Report.where(:chain_id => chain.id, :kind => report_type, :start_date => date, :end_date => date).first
        unless report.blank?
          report.destroy
          report = []
        end

        unless report.blank?
          ChainMailer.report_owner(owner, report).deliver
        else
          Delayed::Job.enqueue(ReportJob::DailyJob.new(chain.id, report_type, date, date, owner, "PDF"))

        end
        owner.update_attribute(:last_report_run, Time.zone.now.to_date) # update column last report run
      end
    end
  end

  def self.send_daily_report(role_id = 1)
    owners = Owner.where(:role_id => role_id, :is_active => true)
    role_id.eql?(1) ? send_email_chain_owner_report(owners, 'daily', Date.today, Date.today) : send_email_restaurant_owner_report(owners, 'daily', Date.today, Date.today)
    #send_email_chain_owner_report(owners, 'daily', Date.today, Date.today)
  end

  def self.send_weekly_report(role_id = 1)
    t_zone = Time.current
    ## if end of week
    if t_zone.monday?
      selected_week = t_zone - 2.days
      start_week = selected_week.beginning_of_week
      end_week = selected_week.end_of_week
      owners = Owner.where(:role_id => role_id, :is_active => true, :weekly => true)
      role_id.eql?(1) ? send_email_chain_owner_report(owners, "weekly", start_week, end_week) : send_email_restaurant_owner_report(owners, "weekly", start_week, end_week)
    end
  end

  def self.send_monthly_report(role_id = 1)
    t_zone = Time.current
    # if end of month
    if t_zone.beginning_of_month.to_date == t_zone.to_date
      selected_month = t_zone - 2.days
      start_month = selected_month.beginning_of_month
      end_month = selected_month.end_of_month
      owners = Owner.where(:role_id => role_id, :is_active => true, :monthly => true)
      role_id.eql?(1) ? send_email_chain_owner_report(owners, "monthly", start_month, end_month) : send_email_restaurant_owner_report(owners, "monthly", start_month, end_month)
    end
  end

  def self.send_periodic_report(role_id = 1)
    owners = Owner.where(:role_id => role_id, :is_active => true, :periodic => true)
    if role_id == 1
      chain = owners.first.chains.first
    else
      chain = owners.first.restaurants.first.chain
    end
    unless chain.report_periode.blank?
      send_email_report(owners, role_id, "periodic")
    end
  end

  def self.send_email_chain_owner_report(owners, report_type, start_date = nil, end_date = nil, force = nil)
    owners.each do |owner|
      chain = owner.chains.first
      if chain and owner.send("#{report_type}") == true
        report = Report.where(:chain_id => chain.id, :kind => report_type, :start_date => start_date, :end_date => end_date).first
        if force ## rerun again the report
          report.destroy
          report = []
        end
        unless report.blank?
          ChainMailer.report_owner(owner, report).deliver
        else
          Delayed::Job.enqueue(ReportJob::DailyJob.new(chain.id, report_type, start_date, end_date, owner, "PDF"))

        end
        owner.update_attribute(:last_report_run, Time.zone.now.to_date) # update column last report run
      end
    end
  end

  def self.send_email_restaurant_owner_report(owners, report_type, start_date = nil, end_date = nil)
    data = {}
    owners.each do |owner|
      owner.restaurants.each do |restaurant|
        #report_body = data["restaurant_#{restaurant.id}"] || restaurant.generate_restaurant_owner_report(report_type, start_date, end_date)
        #data["restaurant_#{restaurant.id}"] = report_body
        #ChainMailer.restaurant_owner_report(restaurant, report_body, owner, start_date, end_date, report_type).deliver
        chain = restaurant.chain
        report = Report.where(
            :chain_id => chain.id,
            :kind => report_type, :start_date => start_date,
            :end_date => end_date,
            :restaurant_id => restaurant.id).first

        unless report.blank?
          ChainMailer.report_owner(owner, report).deliver
        else
          Delayed::Job.enqueue(ReportJob::DailyRestaurantJob.new(chain.id, restaurant.id, report_type, start_date, end_date, owner))
        end
        owner.update_attribute(:last_report_run, Time.zone.now.to_date) # update column last report run
      end
    end
  end

  def self.send_email_periodic(owners)
    t_zone = Time.zone.now

    owners.each do |owner|
      chain = owner.chains.first
      last_run_date = owner.last_report_run.blank? ? false : owner.last_report_run.to_date
      run_at = (last_run_date + chain.report_period.to_i.days) if last_run_date != false


      ## if periodic is set on chain
      if (chain && run_at == t_zone.to_date) || last_run_date == false
        report_body = chain.generate_daily_report_for_chain_owner
        ChainMailer.daily_report(chain, report_body, owner).deliver
        owner.update_attribute(:last_report_run, Time.zone.now.to_date) # update column last report run
      end
    end
  end

  def self.send_pdf_owner(owner, report_type, start_date, end_date, role_id = 1)
    chain = owner.chains.first

    if chain
      report_body = chain.generate_report(role_id, report_type, start_date, end_date)
      owner.update_attribute(:last_report_run, Time.zone.now.to_date) # update column last report run
    end
  end

  def owner_list
    #Owner.where("chains_owners.chain_id = ? and owners.is_active = ? and owners.role_id = 1 and owners.owner_id is not null",x.id,true).joins(:chains_owners).select("distinct first_name,owners.id,last_name")
    self.owners.where("owners.is_active = ?", true)
  end

  def itunes_link
    link_text =  REDIS.get "itunes_link_text_setting_chain_#{self.id}"
    link_itunes = REDIS.get "itunes_link_setting_chain_#{self.id}"
    link_itunes = "<a href='#{link_itunes}'>#{link_text}</a>"
    return link_itunes
  end

  def play_store_link
    link_text = REDIS.get "play_store_link_text_setting_chain_#{self.id}"
    link_play_store = REDIS.get "play_store_link_setting_chain_#{self.id}"
    link_play_store = "<a href='#{link_play_store}'>#{link_text}</a>"
    return link_play_store
  end

  def today_max_user_limit?(receipt, issue_date)
    p "-EE-"*30
    p self.user_limit_receipt_approved_per_day
    p receipt
    p issue_date
    p issue_date.class
    begin
      return false if self.user_limit_receipt_approved_per_day.eql?(0)
      return false if issue_date.blank?
      issue_date_in_date_format = Time.zone.parse(issue_date.to_s).utc
      p issue_date_in_date_format
      today_receipt_approved = ReceiptTransaction.today_receipt_approved_count(receipt, issue_date_in_date_format)
      p "--------#{today_receipt_approved}"
      return false if self.user_limit_receipt_approved_per_day > today_receipt_approved
      return true
    rescue
      return false
    end
  end

  def available_offer_points_earned_per_day?(receipt, receipt_transaction, total_points_earned, issue_date, offer)
    p "maximum_offer_points_earned_per_day"
    p self.maximum_offer_points_earned_per_day
#    begin
      tmp_total_points_earned = 0
      multiplier = offer.multiplier_constant_value

      return true if self.maximum_offer_points_earned_per_day == 0
      return true if self.maximum_offer_points_earned_per_day < 0
      return true if receipt.is_online_order
      # if offer.is_online_order == false

      dolar_spent = total_points_earned.to_f * self.points.to_f
      if offer.offer_rules.size == 0
        if offer.is_multiplier
          tmp_total_points_earned = dolar_spent * multiplier
        else
          tmp_total_points_earned = multiplier
        end
      else
        # with rules

          off_rule = OfferRuleCalculation::CalculateOfferRule.new(receipt_transaction.offer, receipt_transaction,receipt)
          off_rule.result
          tmp_total_points_earned = off_rule.total_points_get

      end
      tmp_total_points_earned = tmp_total_points_earned.send(Chain::ROUNDING_OPTIONS[self.point_rounding_preference])
      rest_of_points = ReceiptTransaction.calculate_offer_points_earned_per_day(receipt, receipt_transaction, tmp_total_points_earned, issue_date)
      return false if rest_of_points == 0
      return true


 #   rescue
 #     rescue false
 #   end

  end

  def is_expired_micros_receipt?(issue_date)
    return false if self.no_of_days_micros_table_receipt_generated <= 0
    return false if issue_date.blank?
    in_est_time =  issue_date.strftime("%Y-%m-%d %H:%M:%S-5")
    in_est_time = DateTime.strptime(in_est_time, "%Y-%m-%d %H:%M:%S %z")
    in_hst_time = in_est_time.in_time_zone
    limit_day_receipt_date = in_hst_time. + self.no_of_days_micros_table_receipt_generated.days
    Time.zone.now > limit_day_receipt_date
  end


  def today_olo_max_user_limit?(receipt, issue_date)
    p "-EE-"*30
    p self.user_limit_receipt_approved_per_day
    p receipt
    p issue_date
    p issue_date.class
    begin
      return false if self.user_limit_receipt_approved_per_day.eql?(0)
      return false if issue_date.blank?
      issue_date_in_date_format = Time.zone.parse(issue_date.to_s).utc
      p issue_date_in_date_format
      today_receipt_approved = ReceiptTransaction.today_online_receipt_approved_count(receipt, issue_date_in_date_format)
      p "--------#{today_receipt_approved}"
      return false if self.user_limit_receipt_approved_per_day > today_receipt_approved
      return true
    rescue
      return false
    end
  end

  def new_reward_notif?(key)
    a = REDIS.hget "chain_#{self.id}_reward_notif", key+"_reward_notif"
    if a.blank?
      return false
    else
      return true
    end
  end

  def get_reward_notif(key)
    a = REDIS.hget "chain_#{self.id}_reward_notif", key+"_reward_notif"
    res = JSON.parse(a)["body"] rescue ""
    return res
  end

  def mailchimp_required?
    self.mailchimp_sync
  end

  def point_added_required?
    self.require_point_signup_incentive
  end

  def fb_point_added_required?
    self.fb_signup_incentive
  end


  def sync_now_status
    mclog = MailchimpLog.find(self.mailchimp_log_id) rescue nil
    if mclog
      if ["in queue", "processing"].include?(mclog.status)
        "close"
      else
        "open"
      end
    else
      "open"
    end
  end

  def fishbowl_sync_now_status
    fblog = FishbowlLog.find(self.fishbowl_setting.fishbowl_log_id) rescue nil
    if fblog
      if ["in queue", "processing"].include?(fblog.status)
        "close"
      else
        "open"
      end
    else
      "open"
    end
  end

  def emma_sync_now_status
    emmalog = EmmaLog.find(self.emma_setting.emma_log_id) rescue nil
    if emmalog
      if ["in queue", "processing"].include?(emmalog.status)
        "close"
      else
        "open"
      end
    else
      "open"
    end
  end

  def update_dashboard_instant

    #activity
    [self].each do |ch|
      diff = 10 ## 11 days before

      ch.restaurants.each do |rs|
        ## loop each restaurants
        diff.downto(0).each do |day|
          cnt = Receipt.receipt_date_dashboard_receipt_activity(rs.id, (Time.zone.now - day.days)).count(distinct: true)
          data = ActivitySummary.where(:transaction_date => (Time.zone.now - day.days).to_date,
                            :restaurant_id => rs.id, :chain_id => ch.id
          ).first

          if data.blank?
            if cnt.to_i > 0
              ActivitySummary.create(:transaction_date => (Time.zone.now - day.days).to_date,
                          :total => cnt.to_i, :restaurant_id => rs.id, :chain_id => ch.id
              )
            end
          else
            data.update_attributes(:total => cnt.to_i)
          end
        end
      end
    end

    # rewards

    [self].each do |ch|
      diff = 10 ## 11 days before

      ch.restaurants.each do |rs|
        ## loop each restaurants
        0.upto(diff).each do |day|
          #cnt = RewardTransaction.by_restaurant_and_date(rs.id, ((Time.zone.now - day.days))).count
          date_zone = "date(TIMEZONE('UTC', reward_transactions.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
          cnt = RewardTransaction.where("restaurant_id = ? and #{date_zone} = ?",rs.id,
                                        (Time.zone.now - day.days).to_date).count
          data = RewardSummary.where(:transaction_date => (Time.zone.now - day.days).to_date,
                                     :restaurant_id => rs.id, :chain_id => ch.id
          )
          if data.size > 1
            # delete duplicate row
            data.delete_all
            data = nil
          else
            data = data.first
          end



          if data.blank?
            RewardSummary.create(:transaction_date => ((Time.zone.now - day.days)),
                                 :total => cnt.to_i, :restaurant_id => rs.id, :chain_id => ch.id
            ) if cnt.to_i > 0
          else
            data.update_attributes(:total => cnt.to_i)
          end
        end
      end
    end

    ## daily chart
    DashboardQuery::ActiveMemberV2.populate_daily_data(self)
    DbSummary.prepulate_data(self.id)
  end

  #LDAP logic here====
  def set_ldap_identity
    self.ldap_identity = self.try(:name).try(:parameterize, "_").try(:classify) if Chain.column_names.include?(:ldap_identity)
  end

  def create_or_update_ldap
    ldap = Ldap.new
    ldap.create_or_update_chain(self) rescue nil
  end

  def remove_ldap_chain
    ldap = Ldap.new
    ldap.remove_chain(self)
  end

  def add_ldap_chain
    ldap = Ldap.new
    ldap.add_chain(self)
  end

  ## birthday reward
  def birthday_rewards
   self.rewards.where("reward_type =?",Reward::TYPES["BIRTHDAY"])
  end

  ## anniversary reward
  def anniversary_rewards
    self.rewards.where("reward_type =?",Reward::TYPES["ANNIVERSARY"])
  end

  def notification_setting_infos
    if notification_settings.blank?
      NotificationSetting.set_default(self)
      reload
    else
      notification_settings.each do |notify|
        if notify.notifiable_id.nil?
          query = eval("notify."+NotificationSetting::TYPE_QUERY[notify.email_template]) rescue nil
          notifiable_id = query.id rescue nil
          if  !notifiable_id.nil?
            notify.update_attribute(:notifiable_id, notifiable_id)
          end
        end
      end
    end
  end

  def create_hybrid_reward_redemption
    p "RUN job create_hybrid_reward_redemption"
    Delayed::Job.enqueue(HybridRewardRedemptionJob.new(self))
  end

  def on_hybrid_setting_reward_redeem_flow?
    self.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["HYBRID SETTING"])
  end

  def on_hybrid_setting_app_based_rule?(restaurant)
    HybridRewardRedemptionFlow.where(chain_id: self.id, restaurant_id: restaurant.id, flow: "app_based").count > 0
  end

  def on_hybrid_setting_pos_based_rule?(restaurant)
    HybridRewardRedemptionFlow.where(chain_id: self.id, restaurant_id: restaurant.id, flow: "pos_based").count > 0
  end

  def reward_code_from_relevant_mode?
    self.reward_code_generated_type == 1
  end

  def reward_code_uploaded_mode?
    self.reward_code_generated_type == 2
  end

  def reward_code_fishbowl_promotion_mode?
    self.reward_code_generated_type == 3
  end

  def get_reward_code_from_uploaded_code(user, reward, restaurant)
    if self.reward_code_uploaded_mode? && reward.reward_pos_codes.count > 0
      #selected = reward.reward_pos_codes.where("user_id IS NULL").order("RANDOM()").first rescue nil
      selected = reward.reward_pos_codes.where("user_id IS NULL").limit(100).sample(1).first rescue nil
      unless selected.blank?
        selected.update_attributes(:user_id => user.id, :used_at => Time.current, :restaurant_id => (restaurant.id rescue nil))
        selected.code
      else
        "NOCODE"
      end
    elsif self.reward_code_uploaded_mode? && reward.reward_pos_codes.count == 0
      "NOCODE"
    end
  end

  def self.revel_to_relevant_sync_from_ftp_files(chain)
    if chain.try(:revel_to_relevant_ftp_url) && chain.try(:revel_to_relevant_ftp_port) && chain.try(:revel_to_relevant_ftp_username) && chain.try(:revel_to_relevant_ftp_password)
      begin
        require 'net/ftp'
        ftp =Net::FTP.new
        ftp.passive = true
        ftp.connect(chain.revel_to_relevant_ftp_url, chain.revel_to_relevant_ftp_port)
        ftp.login(chain.revel_to_relevant_ftp_username,chain.revel_to_relevant_ftp_password)
        begin
          ftp.chdir(chain.revel_to_relevant_ftp_root_dir)
        rescue => e
          puts "Revel to Relevant #{e}"
        end

        @files = ftp.nlst("*.csv") | ftp.nlst("*.xls") | ftp.nlst("*.CSV") | ftp.nlst("*.XLS")
        ftp_files = @files.sort_by{|f| ftp.mtime(f).to_datetime }.last(2)
        revel_logs = RevelLog.select("attachment_file_name").where(chain_id: chain.id, status: "completed").order("created_at desc").limit(5).map(&:attachment_file_name)
        correct_files = ftp_files - revel_logs

        if correct_files.blank?
          p "---------revel Synch missing file for this day or same file name-------"
          RevelLog.create(chain_id: chain.id, log_type: "revel to relevant", status: "failed",
                          success_users: [], failed_users: [], completed_at: Time.now, error_report: "FTP Upload from client is missing" )
          ChainMailer.mailchimp_sync_log_error_report(chain, "FTP Upload from client is missing or Client Uploaded duplicate file", "REVEL TEST ENV").deliver
          ChainMailer.error_report_for_tester(chain, "FTP Upload from client is missing or Client Uploaded duplicate file", "REVEL TEST ENV").deliver
        else
          correct_files.each do |file|
            if ftp.mtime(file).in_time_zone("EST").to_date == (Time.zone.now.in_time_zone("EST").to_date - 1.days)
              rl = RevelLog.create(:chain_id => chain.id, :log_type => "revel to relevant", :status => "in queue")
              Delayed::Job.enqueue(DailySyncRevelToRelevantJob.new(chain, file, rl, "ftp_sync"), :delayable_type => "Sync revel")
            end
          end
        end
        ftp.close
      rescue => e
        RevelLog.create(chain_id: chain.id, log_type: "revel to relevant", status: "failed",
                        success_users: [], failed_users: [], completed_at: Time.now, error_report: e )
        puts "Revel to Relevant #{e}"
        ChainMailer.mailchimp_sync_log_error_report(chain, "Error processing the file: #{e}", "REVEL TEST ENV").deliver
        ChainMailer.error_report_for_tester(chain, "Error processing the file:  #{e}", "REVEL TEST ENV").deliver
        ftp.close
      end
    end
  end

  def self.relevant_to_revel_sync_from_ftp_files(chain)
    if chain.try(:relevant_to_revel_ftp_url) && chain.try(:relevant_to_revel_ftp_port) && chain.try(:relevant_to_revel_ftp_username) && chain.try(:relevant_to_revel_ftp_password)
      Delayed::Job.enqueue(DailySyncRelevantToRevelJob.new(chain), :delayable_type => "Sync revel")
    end
  end

  def update_restaurant_hours_olo_connected(olo_connect_setting = nil)
    olo_connect_setting = OloConnectSetting.where(:chain_id => self.id).first if olo_connect_setting.blank?
    if olo_connect_setting
      p "UPDATE RESTAURANT OLO CONNECT #{self.name}"
      to = Time.zone.now.in_time_zone("EST")
      from = to - 6.days
      from = from.strftime("%Y%m%d")
      to = to.strftime("%Y%m%d")
      self.restaurants.select("id, chain_id, external_partner_id").where("external_partner_id IS NOT NULL AND external_partner_id <> ''").each do |restaurant|
        olo_restaurant = Olo::Api::Restaurant.new(olo_connect_setting["api_root"], (olo_connect_setting["apikey"] || olo_connect_setting["iphone_api_key"]))
        response = olo_restaurant.get_open_hours(restaurant.external_partner_id, from, to)
        if response
          puts "CHAIN: #{self.name} success"
          business_days = response["calendar"].select{|a| a["type"].to_s.upcase == "BUSINESS"}.first rescue nil
          business_days["ranges"].each do |range|
            open_at =  Time.try(:strptime, range["start"], "%Y%m%d %H:%M").try(:strftime, "%I:%M%p") rescue ""
            close_at = Time.try(:strptime, range["end"], "%Y%m%d %H:%M").try(:strftime, "%I:%M%p") rescue ""
            day_of_week = RestaurantHour::WEEKDAY_NAMES_ABBR.index(range["weekday"])
            existing_restaurant_hour = restaurant.restaurant_hours.where(:day_of_week => day_of_week).first
            if existing_restaurant_hour.blank?
              existing_restaurant_hour = RestaurantHour.new(:restaurant_id => restaurant.id, :day_of_week => day_of_week,
                                                            :open_at => open_at, :close_at => close_at)
              existing_restaurant_hour.save
            else
              existing_restaurant_hour.update_attributes(:open_at => open_at, :close_at => close_at)
            end
          end if business_days && !business_days["ranges"].blank?
        end
      end
    end
  end

private
  def destroy_rewards_image?
    self.rewards_image.clear if delete_reward_image == "1"
  end
end
