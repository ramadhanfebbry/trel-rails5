require 'csv'
require 'mail'
require 'resolv'
require 'mandrill'

class User < ActiveRecord::Base
  include RelevantRitas
  include Vantiv::UserGift

  has_many :reward_wallets, :dependent => :destroy
  has_many :perk_wallets, :dependent => :destroy
  has_many :rewards, :through => :reward_wallet
  has_many :perks, :through => :perk_wallets
  has_many :receipts, :dependent => :destroy
  has_many :surveys_users, :dependent => :destroy
  has_many :reward_transactions, :dependent => :destroy
  has_many :user_incentives
  has_many :user_goal_categories, :dependent => :destroy
  has_many :categories, :through => :user_goal_categories
  has_many :user_goals
  has_many :goal_user_incentives
  has_many :history_notifications
  has_one :referral_code
  has_many :social_share_participants
  has_many :referral_users
  has_many :restaurant_users
  has_many :day_visits, :dependent => :destroy
  has_one :tmp_category
  belongs_to :chain
  belongs_to :locale
  has_one :user_tag
  has_many :user_device_logs
  belongs_to :favorite_restaurant, :class_name => "Restaurant", :foreign_key => "favorite_location"
  has_one :user_fishbowl
  has_one :email_marketing
  has_many :raffle_participation_histories
  has_many :raffle_user_informations
  has_one :user_profile
  has_one :onosys_profile
  has_one :onosys_reward_claim
  has_one :olo_profile
  has_one :user_confirmation

  has_many :vext_reward_claims
  has_many :authentications, :dependent => :delete_all
  has_many :access_grants, :dependent => :delete_all


  validates :email, format: {with: eval(Setting.user.email_regex), :message => I18n.t(:email_invalid)}, :if => Proc.new{|f| !f.register_type.eql?(User::USERS_TYPES["Facebook"])}
  validates :email, format: {with: eval(Setting.user.email_regex), :message => I18n.t(:facebook_email_invalid)}, :if => Proc.new{|f| f.register_type.eql?(User::USERS_TYPES["Facebook"])}
  validates :register_type, presence: true
  #validates :locale_id, presence: true
  validates :password,
            :length => {:minimum => Setting.user.password_size, :message => I18n.t(:password_too_long_error2, :password_size => Setting.user.password_size)},
            :format => {:with => eval(Setting.user.password_regex), :message => I18n.t(:password_invalid)}, :if => :password_required?, :on => :create, unless: :skip_validations

  validates_uniqueness_of :chain_id, :scope => [:email], :message => I18n.t(:not_unique_email), :if => :goal_category_unrelated?

  validates_uniqueness_of :phone_number, :scope => [:chain_id], :message => "This phone number is already taken. Please use a secondary phone number to Sign Up", :if => :phone_number_required_and_unique?
  validates :phone_number,
            :format => {:with => /\d{9}/, :message => "Please enter a valid 10-digit phone number"}, :if => :phone_number_required?
  
  validate :dob_validation
  validate :validates_category_ids
  validate :domain_email_validation

  validates :first_name, :last_name, :length => {:minimum => 2}, if: Proc.new{|f| f.olo_registration.eql?(true)}

  USERS_TYPES = {"Email PWD" => 1, "Facebook" => 2, "Twitter" => 3}
  DEVICE_TYPES = ["android", "iphone"]
  SIGNUP_STATUS = {"GREEN" => 1,  "ORANGE" => 2, "GREY" => 3}

  default_scope { where(:deleted_at => nil).order("users.id desc") }


  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, 
         :recoverable, :rememberable, :trackable, :omniauthable,
         :authentication_keys => [:email, :chain_id, :register_type] #removed token_authenticable since it was removed

  after_save :regenerate_goals, :add_to_email_marketing, :add_to_emma_marketing

  def self.find_for_database_authentication(conditions={})
    self.unscoped.where("email = ? AND chain_id = ?",conditions[:email], conditions[:chain_id]).order("points, last_sign_in_at DESC").limit(1).first
  end


  attr_accessor :forgot_password, :category_part, :php_user, :php_user_id, :php_email, :name, :required_domain_email_validation, :olo_registration, :skip_validations
  # Setup accessible (or protected) attributes for your model

  # attr_accessible :first_name, :last_name, :username, :register_type, :register_device_type, :chain_id, :state, :password, :city, :zipcode, :facebookID, :deviceID, :points, :latitude, :longitude, :locale_id, :ref_code,:zipcode
  # attr_accessible :email, :password, :password_confirmation, :remember_me, :authentication_token, :created_at, :updated_at,
  #                 :forgot_password, :sign_in_device_type, :device_token, :device_id, :category_part, :phone_model, :os,
  #                 :php_user, :php_user_id, :php_email, :phone_number, :dob_day, :dob_month, :dob_year, :olo_registration,
  #                 :name, :signup_device_status, :selected_device_info, :required_domain_email_validation, :marketing_optin, :favorite_location, :favorite_menu_item, :app_usage_purpose, :gender, :marketing_optin_texting, :special_occassion, :skip_validations

  validates :name, :length => {:maximum => 100, :message => "The name contains too many characters"}, :format => {:with => /\A[a-z A-Z]+\z/, :message => "The name contains invalid characters"}, :allow_blank => true
  validates :app_usage_purpose, :length => {:maximum => 200, :message => "The app usage purpose contains too many characters"}, :allow_blank => true
  validates :zipcode,  :allow_blank => true,:format => {:with => /(^\d{5}$)|(^\d{5} \d{4}$)/,
                                                     :message => "Please enter valid zipcode"}

  Mail.defaults do
    delivery_method :smtp, {
        :address => Setting.smtp.host,
        :port => Setting.smtp.port,
        :user_name => Setting.smtp.username,
        :password => Setting.smtp.password,
        :authentication => 'plain',
        :enable_starttls_auto => true
    }

  end

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def self.find_for_facebook_oauth(access_token, signed_in_resource=nil)
    data = access_token.extra.raw_info

    if user = User.where(:email => data.email, :register_type => USERS_TYPES[access_token["provider"].capitalize]).first
      user.update_attributes(:first_name => data.first_name, :last_name => data.last_name, :email => data.email, :facebookID => data.id, :register_type => USERS_TYPES['Facebook'])
      user
    else # Create a user with a stub password.
      User.create!(:first_name => data.first_name, :last_name => data.last_name, :email => data.email, :facebookID => access_token["uid"], :register_type => USERS_TYPES['Facebook'])
    end
  end

  def self.create_available_week_and_user_generate_goals(run_day_time = "Sunday")
    t_zone = Time.current
    if t_zone.strftime("%A").downcase.eql?(run_day_time.downcase)  && (REDIS.get("run_at").nil? || Time.parse(REDIS.get("run_at")) < Time.current.midnight)
      REDIS.set("run_at", Time.current)
      start_process = Time.now
      user_errors_list = []
      users_generated_goals_count = 0
      users_not_generated_goal_count = 0
      chain = Chain.find 5
      chain.users.all.each do |user|
        begin
          if !user.tmp_category.blank? or !user.categories.where(:status => GoalCategory::STATUS[:ACTIVE]).blank?
            if user.week_generated_at.blank? or !user.week_generated_at.to_date.eql?(t_zone.to_date)
              user.category_part = true
              user.regenerate_goals
              users_generated_goals_count += 1
            else
              users_not_generated_goal_count += 1
            end
          else
            users_not_generated_goal_count += 1
          end
          user.category_part = false
          user.update_attributes("week_generated_at" => t_zone, :allow_to_update_goals => true)
        rescue => e
          user_errors_list << {:user_id => user.id, :error => "#{e.inspect}"}
        end
      end
      end_process = Time.now
      mail = Mail.new do
        from Setting.email.default_from
        to Setting.email.sys_admin
        subject "#{ENV["NEW_RELIC_APP_NAME"]} - Goal Scheduler Report #{Time.now.strftime("%m/%d/%Y")}"
        html_part do
          content_type 'text/html; charset=UTF-8'
          body "Goal Scheduler Report Detail : <br />
    <ul>
      <li> Users got new goals : #{users_generated_goals_count} </li>
      <li> Users didn't get goals : #{users_not_generated_goal_count} </li>
      <li>Process Time : #{end_process - start_process} seconds</li>

      <li> Error Report : #{user_errors_list.blank? ? 'none' : ''}
          <ul>
             #{user_errors_list.each { |list| "<li>User id : #{list[:user_id]}  -> #{list[:error]}</li>" }}
          </ul>
    </ul>".html_safe
        end
      end
        mail.deliver!
    end
  end

  def goal_category_unrelated?
    self.category_part.nil?
  end

  def regenerate_goals
    if self.category_part
      today = Time.current
      start_date = today.send("custom_beginning_of_#{Setting.goals.unit_time}")
      end_date = today.send("custom_end_of_#{Setting.goals.unit_time}")

      existing_week = Week.find_or_create_by_start_date_and_end_date( start_date, end_date)
      User.create_goal_by_week(self, existing_week)
      self.category_part = false
      self.update_attribute(:allow_to_update_goals, false)
    end
  end

  def self.create_goal_by_week(user, week)
    load_tmp_category = user.tmp_category
    if load_tmp_category.blank?
      user_categories = user.categories.where(:status => GoalCategory::STATUS[:ACTIVE])
    else
      user.category_ids = load_tmp_category.category_ids.split(",")
      user.category_part = false
      user.save
      user_categories = user.categories.where(:status => GoalCategory::STATUS[:ACTIVE])
      load_tmp_category.destroy
    end
    user_goal_ids = []
    user_categories.each_with_index do |category, i|
      category_goals = category.goals.where(:status => Goal::STATUS[:ACTIVE])
      category_goals = category_goals.group_by(&:incentive_id?)
      category_goals = (category_goals[true] || []).sort { |a, b| rand <=> rand } + (category_goals[false] || []).sort { |a, b| rand <=> rand }

      category_goals[0..(User.get_goal_per_category_number(user_categories.count, i) - 1)].each do |goal|
        user_goal = UserGoal.where(:user_id => user.id, :goal_id => goal.id, :week_id => week.id).first
        user_goal = UserGoal.create(:user_id => user.id, :goal_id => goal.id, :week_id => week.id) if user_goal.blank?
        user_goal_ids << user_goal.id
      end

      #deleted unpick category
      ids = user.user_goals.includes(:goal => :category).where("user_goals.week_id = ? AND user_goals.id NOT IN (?)", week.id, user_goal_ids).map(&:id)
      ids.each do |id|
        user_goal = UserGoal.find(id)
        user_goal.destroy if user_goal
      end
    end
  end

  def self.get_goal_per_category_number(user_categories_count, index)
    GoalCategory::GOAL_CATEGORY_TEMPLATE["template#{user_categories_count > 3 ? 3 : user_categories_count }".to_sym][index > 2 ? 2 : index]
  end

  def self.marketing_optin_value(value)
    if value
      return true if value.to_s.downcase.eql?("true")
      return false
    else
      false
    end
  end

  def self.marketing_optin_texting_value(value)
   if value.numeric?
    value.to_i.eql?(1) ? true : false
   elsif value.is_a? String
    value.to_s.downcase.eql?("true") ? true : false
    else
    return false
   end
  end

  def full_name
    [first_name, last_name].join(' ')
  end

  def password_required?
    case self.register_type
      when USERS_TYPES["Email PWD"]
        true
      else
        false
    end
  end

  def dob_validation
    return if chain.blank?
    if chain.birthday_required
      begin
        dob = "#{self.dob_year}-#{self.dob_month}-#{self.dob_day}"
        birth_day = Date.parse(dob)
        if chain.check_age_required
          return if chain.check_age_value.to_i == 0
          if birth_day + (chain.check_age_value.to_i).years > Date.current
            errors.add("dob", "This app is not intended for children under #{chain.check_age_value.to_i} years of age")
          end
        end
      rescue => e
        p e.message
        errors.add("dob", "Please enter a valid date")
      end
    end
  end

  def dob_dates
    return false if self.dob_year.blank?
    dob = "#{self.dob_year}-#{self.dob_month}-#{self.dob_day}"
    puts dob
    return dob
  end

  def phone_number_required?
    return false if chain.blank?
    return true if chain.phone_number_is_required
    return false
  end

  def phone_number_required_and_unique?
    phone_number_required? && chain.unique_phone_number_required
  end

  scope :active_chain, -> { where(:chain_id => Chain.active.map(&:id)) }

  scope :chains, -> (value) { 
    if (!value.blank?)
      where("chain_id" => value)
    end
  }
  scope :email, -> (value) { 
    if (!value.blank?)
      where("email LIKE '%#{value}%'")
    end
  }

  scope :device_type, -> (value) { 
    if (!value.blank? &&  value.size < 2)
      where("register_device_type ILIKE ?", value)
    end
  }

  scope :date_from, -> (value) { 
    if (!value.blank?)
      where("created_at >= '#{value}'")
    end
  }

  scope :date_to, -> (value) { 
    if (!value.blank?)
      where("created_at <= '#{value}'")
    end
  }

  scope :device_info, -> (value) { 
    if (!value.blank?)
      includes(:user_device_logs).
      where("user_device_logs.keychain_value = '#{value}' OR user_device_logs.device_id = '#{value}'")
    end
  }

  scope :phone_number_info, -> (value) { 
    if (!value.blank?)
      where("users.phone_number ILIKE '%#{value}%' ")
    end
  }

  scope :first_name_info, -> (value) { 
    if (!value.blank?)
      where("first_name LIKE '%#{value.downcase}%' ")
    end
  }

  scope :last_name_info, -> (value) { 
    if (!value.blank?)
      where("last_name LIKE '%#{value.downcase}%' ")
    end
  }

  scope :search, -> (params){ chains(params[:chains]).device_type(params[:device_type]).email_or_ref_code(params[:q], params[:pattern]).date_from(params[:from]).date_to(params[:to]).device_info(params[:device_info]).phone_number_info(params[:phone_number]).first_name_info(params[:first_name_info]).last_name_info(params[:last_name_info]) }

  def random_password
    chars = Setting.user.password_chars
    size= Setting.user.password_size
    (1..size).map { chars[rand(chars.length)] }.join
  end

  def earn(points,receipt=nil, transaction_type = nil)
    self.increment!(:points, points)
    self.increment!(:total_points_earned, points)
    self.increment!(:point_threshold, points)
    self.add_milestone_reward
    self.milestone_points_select

    return unless points > 0
    return unless self.active
    if transaction_type.blank?
      emails_and_device_tokens_group = self.group_emails_and_device_token_by_locale([self.id])
      notification_by_locale = self.group_message_pn_by_locale


      if chain.is_send_push_point_notif_to_phone  && self.push_to_device?
        Delayed::Job.enqueue(GcmAndroidPushPointJob.new(self, emails_and_device_tokens_group, notification_by_locale, points, receipt), chain_id: self.chain_id, delayable_type: self.class.to_s, delayable_id: self.id, run_at: Time.current) if self.sign_in_device_type and self.sign_in_device_type.downcase.eql?("android") and !self.device_token.blank?
        Delayed::Job.enqueue(PushPointIphoneJob.new(self, emails_and_device_tokens_group, notification_by_locale, points, receipt), chain_id: self.chain_id, delayable_type: self.class.to_s, delayable_id: self.id, run_at: Time.current) if self.sign_in_device_type and self.sign_in_device_type.downcase.eql?("iphone") and !self.device_token.blank?
      end

      if chain.is_send_push_point_notif_to_email  && self.push_to_email?
        Delayed::Job.enqueue(PushPointMailJob.new(self, emails_and_device_tokens_group, notification_by_locale, points, receipt), chain_id: self.chain_id, delayable_type: self.class.to_s, delayable_id: self.id, run_at: Time.current)
      end
    end

  end

  def burn(points)
    if self.points - points >= 0
      self.decrement!(:points, points)
      if self.chain.chain_setting && self.chain.chain_setting.hybrid_milestone_regular_rewards_enabled
        self.decrement!(:point_threshold, points)
        self.update_column(:point_threshold, 0) if self.point_threshold < 0
      end
    elsif self.points - points < 0
      ### make it zero if there are decrement with (-) result
      self.decrement!(:points, self.points)
      if self.chain.chain_setting && self.chain.chain_setting.hybrid_milestone_regular_rewards_enabled
        self.decrement!(:point_threshold, self.points)
        self.update_column(:point_threshold, 0) if self.point_threshold < 0
      end

      #else
      #raise Exceptions::InsufficientUserPointsError
      # this is commented out due the error of perks points, and offer multiplier.
      # if all user are deleted all , and start with a new fresh user, we could add this back
    end
  end

  #methods to add milestone reward
  def add_milestone_reward
    chain = self.chain
    milestones = chain.milestones.where("threshold_points <= ? AND milestone_number > ?", self.total_points_earned, (self.milestone || 0)).order("milestone_number ASC")
    milestones.each do |milestone|
      p "-----------"
      p milestone.milestone_number
      if milestone.reward
        self.update_attribute(:milestone, milestone.milestone_number)
        RewardWallet.create_with_delay(:reward_id => milestone.reward_id, :user_id => self.id)
      end
    end
  end

  #def milestone_points
  #  puts "User::Milestone Poinst Started"
  #  ch = self.chain
  #  stepstone = ch.steppingstones.where(:step_type => "points_earned").first
  #  ## points earned step stones
  #  unless stepstone.blank?
  #     point_reach = self.point_threshold
  #     user_step = UserStep.where("steppingstone_id = ? and user_id = ?", stepstone, self.id).
  #         order('created_at desc').last
  #
  #    ## if there are step before, go ahead and check for the next step
  #    unless user_step.blank?
  #      cur_step = user_step.current_step
  #      if randomize == false
  #        conditions = ["step_number > #{cur_step}"]
  #        order_step = " ASC "
  #      elsif randomize == true # randomize
  #        rand_steps = ch.steppingstones.where(:step_type => "points_earned").map(&:step_number)
  #        rand_steps.delete_if{|x| x == cur_step}
  #        step = rand_steps[rand(rand_steps.size)]
  #        conditions = ["step_number IN (#{step})"]
  #        order_step = " RANDOM() "
  #      end
  #
  #      conditions << "loop IS TRUE" if self.step_milestone_receipt_loop
  #      p "user looped #{self.step_milestone_receipt_loop}"
  #      p "initial conditions #{conditions}"
  #      next_step = stepstone.steppingstone_steps.where(conditions.join(" AND ")).order("step_number #{order_step}").first
  #      puts "milestone points next step == #{next_step}"
  #      puts "*" * 100
  #
  #      if !next_step.blank?
  #        puts "asdfasfdasfsdafsadf"
  #        points_to_reach = next_step.point_threshold
  #        self.adding_reward_point_step(next_step) if self.reach_threshold?(points_to_reach)
  #        self.update_column(:point_threshold, 0)  if self.reach_threshold?(points_to_reach)
  #      else ## it means step are reached the max
  #        if stepstone.resetable == true
  #          p "resettable"
  #          self.update_column(:step_milestone_receipt_loop, true)
  #          cur_step = 0
  #          if randomize == false
  #            conditions = ["step_number > #{cur_step}"]
  #            order_step = " ASC "
  #          elsif randomize == true # randomize
  #            rand_steps = ch.steppingstones.where(:step_type => "points_earned").map(&:step_number)
  #            rand_steps.delete_if{|x| x == cur_step}
  #            step = rand_steps[rand(rand_steps.size)]
  #            conditions = ["step_number = #{step}"]
  #            order_step = " RANDOM() "
  #          end
  #          conditions << "loop IS TRUE" if self.step_milestone_receipt_loop
  #          p "user looped #{self.step_milestone_receipt_loop}"
  #          p "resettable conditions #{conditions}"
  #          first_step = stepstone.steppingstone_steps.where(conditions.join(" AND ")).order("step_number #{order_step}").first
  #          points_to_reach = first_step.point_threshold
  #          #reset_the_steps
  #          if self.reach_threshold?(points_to_reach)
  #            user_step = UserStep.where(:user_id => self.id,:steppingstone_id => stepstone.id).first
  #            user_step.update_attribute(:current_step , first_step.step_number) ## reset
  #            self.adding_reward_point_step(first_step)
  #            self.update_column(:point_threshold, 0)
  #          end
  #        end
  #      end
  #    else
  #      p "first attemtp"
  #    ## else  first step attempt
  #      cur_step = 0
  #      first_step = stepstone.steppingstone_steps.where("step_number > ?", cur_step).order("step_number ASC").first
  #      reward =  first_step.reward rescue nil
  #      points_to_reach = first_step.point_threshold
  #      puts "POINTS TO REACH FIRST = #{points_to_reach}"
  #      if self.reach_threshold?(points_to_reach)
  #        UserStep.create(:user_id => self.id,:steppingstone_id => stepstone.id, :current_step => first_step.step_number)
  #        unless reward.blank?
  #          RewardWallet.create_with_delay({:reward_id => reward.id,
  #                                        :user_id => self.id,
  #                                        :description => "Stepping stone points incentive step #{first_step.step_number}"
  #                                       })
  #          p "first attempt update column"
  #          self.update_column(:point_threshold, 0)
  #          p "finish first attemp"
  #        end
  #      end
  #    end
  #  end
  #  puts "User::Milestone Poinst Finished"
  #end

  def fall_into_categorize
    steppingstone = nil
    user_total = UserTotalReceipt.where(:user_id => self.id).order('last_receipt_id asc').last
    ## Get the subtotal for the user if last milestone is blank
    if user_total.blank?
      receipts = Receipt.where(:user_id => self.id).joins(:receipt_transactions).where("receipt_transactions.status = ?",
                                                                                       Receipt::STATUS[:APPROVED]).select('distinct(receipts.*)')

      all_subtotal = []
      receipts.each do |r|
        all_subtotal << r.last_transaction.subtotal
      end

      subtotal = all_subtotal.size


      last_receipt = receipts.last.id rescue nil

    else

      receipts = Receipt.where(:user_id => self.id).joins(:receipt_transactions).
          where("receipt_transactions.status = ? AND date(receipts.created_at) >= ? and receipts.id > ?",Receipt::STATUS[:APPROVED], user_total.last_milestone,
                user_total.last_receipt_id.to_i
      ).select('distinct(receipts.*)')

      all_subtotal = []
      receipts.each do |r|
        all_subtotal << r.last_transaction.subtotal
      end
      subtotal = all_subtotal.size
      last_receipt = receipts.last.id rescue nil
    end

    average = 0
    puts "MILESTONE RECEIPTS SUBOTAL = #{subtotal.to_i}"
    if subtotal.to_i > 0
      begin
      user_total = UserTotalReceipt.new(
          :user_id => self.id,
          :receipt_count => subtotal,
          :subtotal => all_subtotal.sum.to_f,
          :last_milestone => Date.today,
          :last_receipt_id => last_receipt
      )
      rescue
      end
      #user_total.save
      p "SAVINGGGGGGGGGGGGGGGGGGGGG USER # MILESTONE"

      average = (all_subtotal.sum.to_f / subtotal).to_f rescue 0
    end
    ## Check whats reward hes get based on category setting
    if average.to_f > 0
      steppingstone = Steppingstone.where(
          "chain_id = ? AND step_type = ? AND  ?  BETWEEN min_amount AND max_amount",
          self.chain_id, 'points_earned', average
      ).first

      if steppingstone.blank?
        steppingstone = Steppingstone.where(
            "chain_id = ? AND step_type = ? AND (category_type = ?  or category_type is null)",
            self.chain_id, 'points_earned', Steppingstone::SPEND_LEVEL['ALL USER']
        ).first
      end
    else
      ## for all users
      steppingstone = Steppingstone.where(
          "chain_id = ? AND step_type = ? AND (category_type = ?  or category_type is null)",
          self.chain_id, 'points_earned', Steppingstone::SPEND_LEVEL['ALL USER']
      ).first
    end
    #puts "Steppingstone id = #{steppingstone.id}"
    puts "THE USER IS ON #{Steppingstone::SPEND_LEVEL.key steppingstone.category_type}" rescue nil
    return steppingstone
  end

  def milestone_points_select
    stepstone = UserStep.where(:user_id => self.id).last.steppingstone rescue nil
    categorized = Steppingstone.where(:chain_id => self.chain_id, :step_type => 'points_earned').count > 1

    stepstone = nil if stepstone && stepstone.step_type == "receipt_approved"

    ch = self.chain
    if stepstone.blank? and categorized == false
      stepstone = ch.steppingstones.where(:step_type => "points_earned").first
      ## fall into categories
    end

    if categorized
      stepstone = fall_into_categorize
    end


    unless stepstone.blank?
      if (stepstone.randomize == false and stepstone.resetable == true) || (stepstone.randomize == false and stepstone.resetable == false)
        # act as default
        milestone_points(stepstone)
      elsif (stepstone.randomize == true and stepstone.resetable == true)
        milestone_points_random(stepstone)
      end
    end
  end


  def milestone_points_random(stepstone)
    puts "User::Milestone Point Started"
    ch = self.chain
    categorized = Steppingstone.where(:chain_id => self.chain_id, :step_type => 'points_earned').count > 1
    if stepstone.blank?
      stepstone = ch.steppingstones.where(:step_type => "points_earned").first
    end
    ## points earned step stones
    unless stepstone.blank?
      point_reach = self.point_threshold
      user_step = UserStep.where("user_id = ? and steppingstone_id = ?", self.id, stepstone).
          order('created_at desc').last

      ## if there are step before, go ahead and check for the next step
      unless user_step.blank?
        cur_step = user_step.current_step
        reward = user_step.reward rescue nil
        ## randomized

        if stepstone.resetable == true
          p "resettable and randomize"
           ## find it if he loop consideration false is pushed to users
            all_reward_loop_false = []
            #ch.steppingstones.where(:step_type => "points_earned").each do |stp|
            #  all_reward_loop_false <<  stp.steppingstone_steps.where("loop is false").map(&:reward)
            #end
            all_reward_loop_false = stepstone.steppingstone_steps.where("loop is false").map(&:reward)
            all_reward_loop_false = all_reward_loop_false.flatten

            puts "REWARD LOOP FALSE = #{all_reward_loop_false.map(&:id)}"

           stepingstone_tmp = stepstone.steppingstone_steps.where("loop is false")
           loop_false_ids = stepingstone_tmp.map(&:reward)## collect the reward ids
           step_false_ids = stepingstone_tmp.map(&:step_number) ## collect step number false
           loop_size = loop_false_ids.size
           rws = RewardWallet.where("user_id = ? and reward_id in(?)", self.id, all_reward_loop_false.map(&:id))
           ## get the reward wallet that false and delete that step

           ## get the latest reward of user do not include in the randsom
           reward_stepstones = stepstone.steppingstone_steps.map(&:reward_id)
           last_reward_id = self.reward_wallets.where("reward_id in(?)",reward_stepstones).order("updated_at desc").limit(1).map(&:reward_id)
           last_step_pushed = stepstone.steppingstone_steps.where("reward_id in (?)", last_reward_id).map(&:step_number)
           puts "=" * 100
           puts "== User current_step is = #{cur_step}"
           puts "User Last Reward pushed id = #{last_reward_id}"
           puts "=" * 100


           reward_ids = rws.map(&:reward_id)
           size = rws.count
           deleted_steps = stepstone.steppingstone_steps.where("loop is false and reward_id in (?)",reward_ids).map(&:step_number)

          if size.to_i == loop_size.to_i
            ## set column to true
            self.update_column(:step_milestone_receipt_loop, true)
          end

          rand_steps = stepstone.steppingstone_steps.map(&:step_number) - deleted_steps
          rand_steps.delete_if { |x| step_false_ids.include?(x) } if self.step_milestone_receipt_loop
          rand_steps = rand_steps - last_step_pushed
          #rand_steps.delete_if { |x| x.to_i == cur_step.to_i }
          if rand_steps.blank?
          ## only 1 step on admin settings
            rand_steps = stepstone.steppingstone_steps.map(&:step_number)
          end

          rand_steps = rand_steps.join(' , ')
          conditions = ["step_number in (#{rand_steps})"]
          order_step = " RANDOM() "

          conditions << "loop IS TRUE" if self.step_milestone_receipt_loop
          conditions << "loop IS TRUE" if self.step_milestone_receipt_loop and categorized == false
          conditions << "loop IS TRUE" if is_milestone_loopable(stepstone) and categorized == true
          p "resettable conditions #{conditions}"
          puts stepstone.id
          #puts "EAAAAAAAAAAAAAA"
          random_step = stepstone.steppingstone_steps.where(conditions.join(" AND ")).order("#{order_step}").first
          points_to_reach = random_step.point_threshold
          #reset_the_steps
          if self.reach_threshold?(points_to_reach)
            #user_step = UserStep.where(:user_id => self.id, :steppingstone_id => stepstone.id).first
            self.adding_reward_point_step(random_step)
            user_step.update_attribute(:current_step, random_step.step_number) ## random again
            if stepstone.carry_over_points
              if random_step.reset_milestone_value_after_qualifying
                self.update_column(:point_threshold, (self.point_threshold - random_step.point_threshold) )
              end
              if stepstone.steppingstone_steps.map(&:reset_milestone_value_after_qualifying).include?(true) == false
                return
              else
                milestone_points_select
              end
            else
              self.update_column(:point_threshold, 0)
            end
          end
        else
          ## if points reached to this step  and loop is set as false, resetable false
          points_to_reach = user_step.point_threshold
          if self.reach_threshold?(points_to_reach)
            unless reward.blank?
              self.adding_reward_point_step(cur_step)
              if stepstone.carry_over_points
                if cur_step.reset_milestone_value_after_qualifying
                  self.update_column(:point_threshold, (self.point_threshold - cur_step.point_threshold) )
                end
                if stepstone.steppingstone_steps.map(&:reset_milestone_value_after_qualifying).include?(true) == false
                  return
                else
                  milestone_points_select
                end
              else
                self.update_column(:point_threshold, 0)
              end
              p "finish attempt"
            end
            ## break here and random again the steps
            random_step = stepstone.steppingstone_steps.order("step_number RANDOM()").first
            UserStep.create(:user_id => self.id,:steppingstone_id => stepstone.id, :current_step => random_step.step_number)
          end
          ## end

          puts "*" * 100
        end
      else
        p "first attemtp"
        ## else  first step attempt
        cur_step = 0
        if categorized == true
          loop_conditions = " AND loop is TRUE" if is_milestone_loopable(stepstone) and categorized == true
        end

        first_random_step = stepstone.steppingstone_steps.where("step_number > ? #{loop_conditions}", cur_step).order("step_number asc ").first
        return true if  first_random_step.blank?
        reward = first_random_step.reward rescue nil
        points_to_reach = first_random_step.point_threshold
        puts "POINTS TO REACH RANDOM FIRST = #{points_to_reach}"

        if self.reach_threshold?(points_to_reach)
          UserStep.create(:user_id => self.id, :steppingstone_id => stepstone.id,
                          :current_step => first_random_step.step_number)
          unless reward.blank?
            self.adding_reward_point_step(first_random_step)
            p "first attempt update column random"
            if stepstone.carry_over_points
              if first_random_step.reset_milestone_value_after_qualifying
                self.update_column(:point_threshold, (self.point_threshold - first_random_step.point_threshold) )
              end
              if stepstone.steppingstone_steps.map(&:reset_milestone_value_after_qualifying).include?(true) == false
                return
              else
                milestone_points_select
              end
            else
              self.update_column(:point_threshold, 0)
            end
            p "finish first attempt random"
          end
        end
      end
    end
    puts "User::Milestone Poinst Finished random"
  end

  def milestone_points(stepstone)
    puts "User::Milestone Poinst Started"
    categorized = Steppingstone.where(:chain_id => self.chain_id, :step_type => 'points_earned').count > 1
    ch = self.chain
    if stepstone.blank?
      stepstone = ch.steppingstones.where(:step_type => "points_earned").first
    end
    ## points earned step stones
    unless stepstone.blank?
      point_reach = self.point_threshold
      user_step = UserStep.where("user_id = ? and steppingstone_id = ?", self.id, stepstone).
          order('created_at desc').last

      ## if there are step before, go ahead and check for the next step
      unless user_step.blank?
        cur_step = user_step.current_step
        conditions = ["step_number > #{cur_step}"]
        #conditions << "loop IS TRUE" if self.step_milestone_receipt_loop
        conditions << "loop IS TRUE" if self.step_milestone_receipt_loop and categorized == false
        conditions << "loop IS TRUE" if is_milestone_loopable(stepstone) and categorized == true
        p "user looped #{self.step_milestone_receipt_loop}"
        p "initial conditions #{conditions}"
        next_step = stepstone.steppingstone_steps.where(conditions.join(" AND ")).order("step_number ASC").first
        puts "milestone points next step == #{next_step}"
        puts "*" * 100

        if !next_step.blank?
          puts "asdfasfdasfsdafsadf"
          points_to_reach = next_step.point_threshold
          self.adding_reward_point_step(next_step) if self.reach_threshold?(points_to_reach)
          if stepstone.carry_over_points
            if next_step.reset_milestone_value_after_qualifying
              self.update_column(:point_threshold, (self.point_threshold - next_step.point_threshold) )
            end
            if stepstone.steppingstone_steps.map(&:reset_milestone_value_after_qualifying).include?(true) == false
              return
            else
              milestone_points_select
            end
          else
            self.update_column(:point_threshold, 0)
          end if self.reach_threshold?(points_to_reach)
        else ## it means step are reached the max
          if stepstone.resetable == true
            p "resettable"
            self.update_column(:step_milestone_receipt_loop, true)
            cur_step = 0
            conditions = ["step_number > #{cur_step}"]
            conditions << "loop IS TRUE" if self.step_milestone_receipt_loop and categorized == false
            conditions << "loop IS TRUE" if is_milestone_loopable(stepstone) and categorized == true
            p "user looped #{self.step_milestone_receipt_loop}"
            p "resettable conditions #{conditions}"
            first_step = stepstone.steppingstone_steps.where(conditions.join(" AND ")).order("step_number ASC").first
            points_to_reach = first_step.point_threshold
            #reset_the_steps
            if self.reach_threshold?(points_to_reach)
              user_step = UserStep.where(:user_id => self.id,:steppingstone_id => stepstone.id).first
              user_step.update_attribute(:current_step , first_step.step_number) ## reset
              self.adding_reward_point_step(first_step)
              if stepstone.carry_over_points
                if first_step.reset_milestone_value_after_qualifying
                  self.update_column(:point_threshold, (self.point_threshold - first_step.point_threshold) )
                end
                if stepstone.steppingstone_steps.map(&:reset_milestone_value_after_qualifying).include?(true) == false
                  return
                else
                  milestone_points_select
                end
              else
                self.update_column(:point_threshold, 0)
              end
            end
          end
        end
      else
        p "first attemtp"
        ## else  first step attempt
        cur_step = 0
        loop_conditions = nil
        if categorized == true
          loop_conditions = " AND loop is TRUE" if is_milestone_loopable(stepstone) and categorized == true
        end

        first_step = stepstone.steppingstone_steps.where("step_number > ? #{loop_conditions}", cur_step).order("step_number ASC").first
        return true if first_step.blank?


        reward =  first_step.reward rescue nil
        points_to_reach = first_step.point_threshold
        puts "POINTS TO REACH FIRST = #{points_to_reach}"
        if self.reach_threshold?(points_to_reach)
          UserStep.create(:user_id => self.id,:steppingstone_id => stepstone.id, :current_step => first_step.step_number)
          unless reward.blank?
            ## if there is no user category
            self.adding_reward_point_step(first_step)
            p "first attempt update column"
            if stepstone.carry_over_points
              if first_step.reset_milestone_value_after_qualifying
                self.update_column(:point_threshold, (self.point_threshold - first_step.point_threshold) )
              end
              if stepstone.steppingstone_steps.map(&:reset_milestone_value_after_qualifying).include?(true) == false
                return
              else
                milestone_points_select
              end
            else
              self.update_column(:point_threshold, 0)
            end
            p "finish first attemp"
          end
        end
      end
    end
    puts "User::Milestone Poinst Finished"
  end


  def is_milestone_loopable(stepstone)
    categorized = Steppingstone.where(:chain_id => self.chain_id, :step_type => 'points_earned').count > 1
    if categorized
      all_reward_loop_false = []
      all_reward_loop_false = stepstone.steppingstone_steps.where("loop is false").map(&:reward)
      all_reward_loop_false = all_reward_loop_false.flatten

      puts "REWARD LOOP FALSE = #{all_reward_loop_false.map(&:id)}"

      stepingstone_tmp = stepstone.steppingstone_steps.where("loop is false")
      loop_false_ids = stepingstone_tmp.map(&:reward) ## collect the reward ids
      step_false_ids = stepingstone_tmp.map(&:step_number) ## collect step number false
      loop_size = loop_false_ids.size
      rws = RewardWallet.where("user_id = ? and reward_id in(?)", self.id, all_reward_loop_false.map(&:id))
                                                      ## get the reward wallet that false and delete that step


      reward_ids = rws.map(&:reward_id).uniq
      size = rws.count
      deleted_steps = stepstone.steppingstone_steps.where("loop is false and reward_id in (?)", reward_ids).map(&:step_number)

      if size.to_i >= loop_size.to_i
        ## set column to true
        return true
      else
        return false
      end
    end
  end


  def reach_threshold?(point_needed)
    #check the point
    return self.point_threshold.to_i >= point_needed.to_i
  end

  def adding_reward_point_step(step_stone)
    ## push points stepstone reward
    ## if there are LOW, MEDIUM, HIGH user category setttings on the applications
    steppingstone = step_stone.stepable
    puts "MILESTONE Stepping stone id = #{steppingstone.id}"
    puts "MILESTONE Stepping stone chain_id = #{steppingstone.chain_id}"
    chain_id = steppingstone.chain_id

    ## if there is categorized
    categorized = Steppingstone.where(:chain_id => steppingstone.chain_id, :step_type => 'points_earned').count > 1
    reward = step_stone.reward
    if categorized
    #
      user_total = UserTotalReceipt.where(:user_id => self.id).order('last_receipt_id asc').last
      ## Get the subtotal for the user if last milestone is blank
      if user_total.blank?
        #receipts = Receipt.where(:user_id => self.id)
        #ReceiptTransaction.joins(:receipt).where("receipts.user_id= #{x.id} and receipt_transactions.status = ?",Receipt::STATUS[:APPROVED])
        receipts = Receipt.where(:user_id => self.id).joins(:receipt_transactions).where("receipt_transactions.status = ?",
                    Receipt::STATUS[:APPROVED]).select('distinct(receipts.*)')


        puts "MILESTONE RECEIPTS ----- #{receipts}"
        puts "MILESTONE RECEIPTS ----- #{receipts.size}"
        puts "MILESTONE RECEIPTS ----- ID = #{receipts.first.id}" rescue nil

        all_subtotal = []
        receipts.each do |r|
          all_subtotal << r.last_transaction.subtotal
        end
        puts "MILESTONE RECEIPTS ALL SUBTOTAL VAR = #{all_subtotal}"
        subtotal = all_subtotal.size
        puts "MILESTONE RECEIPTS SUBTOTAL SIZE = #{subtotal}"

        last_receipt = receipts.last.id rescue nil
        puts "MILESTONE RECEIPTS Last receipt = #{subtotal}"
      else
      # if there is last milestone, collect data again > last milestone
      #  receipts = Receipt.where(
      #      "user_id = ? AND status = ?   AND date(created_at) >= ? AND id > ? ",
      #                           self.id, Receipt::STATUS[:APPROVED], user_total.last_milestone,
      #                           user_total.last_receipt_id.to_i
      #  )

        receipts = Receipt.where(:user_id => self.id).joins(:receipt_transactions).
            where("receipt_transactions.status = ? AND date(receipts.created_at) >= ? and receipts.id > ?",Receipt::STATUS[:APPROVED], user_total.last_milestone,
                                   user_total.last_receipt_id.to_i
        ).select('distinct(receipts.*)')

        all_subtotal = []
        receipts.each do |r|
          all_subtotal << r.last_transaction.subtotal
        end
        subtotal = all_subtotal.size
        last_receipt = receipts.last.id rescue nil
      end

      average = 0
      puts "MILESTONE RECEIPTS SUBOTAL = #{subtotal.to_i}"
      if subtotal.to_i > 0
        user_total = UserTotalReceipt.new(
          :user_id => self.id,
          :receipt_count => subtotal,
          :subtotal => all_subtotal.sum.to_f,
          :last_milestone => Date.today,
          :last_receipt_id => last_receipt
        )
        user_total.save
        p "SAVINGGGGGGGGGGGGGGGGGGGGG USER # MILESTONE"

        average = user_total.average
      end
      ## Check whats reward hes get based on category setting
      if average.to_f > 0
        steppingstone = Steppingstone.where(
            "chain_id = ? AND step_type = ? AND  ?  BETWEEN min_amount AND max_amount",
            self.chain_id, 'points_earned', average
        ).first

        if steppingstone.blank?
          steppingstone = Steppingstone.where(
              "chain_id = ? AND step_type = ? AND (category_type = ?  or category_type is null)",
              self.chain_id, 'points_earned', Steppingstone::SPEND_LEVEL['ALL USER']
          ).first
        end
      else
        ## for all users
        steppingstone = Steppingstone.where(
            "chain_id = ? AND step_type = ? AND (category_type = ?  or category_type is null)",
            self.chain_id, 'points_earned', Steppingstone::SPEND_LEVEL['ALL USER']
        ).first
      end
      puts "Steppingstone id = #{steppingstone.id}"
      puts "THE USER IS ON #{Steppingstone::SPEND_LEVEL.key steppingstone.category_type}" rescue nil
      puts "step stone ID milestone #{step_stone.id}"
      puts "step stone number milestone #{step_stone.step_number}"
      ## check the reward based on this steppingstone, and based on step number
      step_stone_tmp = steppingstone.steppingstone_steps.where(:step_number => step_stone.step_number).first
      step_stone = step_stone_tmp
      reward = step_stone.reward
      puts "STEP STONE AFTERRRRRRRR =========== #{step_stone.id}"
      user_total.update_column(:last_milestone, Date.today) unless user_total.blank?
    end

    puts "stepping stone ==="
    unless steppingstone.category_type.blank?
      cat_desc = "Category : "+Steppingstone::SPEND_LEVEL.key(steppingstone.category_type) rescue "All User"
    end
    puts "reward = ==== #{reward} milestone"

    unless reward.blank?
      RewardWallet.create_with_delay({:reward_id => reward.id,
                                      :user_id => self.id,
                                      :description => "Stepping stone points incentive step #{ step_stone.step_number} #{cat_desc}"
      })
      ## update user step if not randomize
      if step_stone.stepable.randomize == false
        user_step = UserStep.where(:user_id => self.id, :steppingstone_id => step_stone.stepable.id).first if categorized == false
        if categorized == true
          #user_step = UserStep.where("user_id = ? and steppingstone_id in (?)", self.id,
          #            Steppingstone.where(:chain_id => steppingstone.chain_id, :step_type => 'points_earned').map(&:id)
          #).first
          ## delete the other steps thats not the last

          steps = UserStep.where(:user_id => self.id)
          if steps.count > 1
              user_step = UserStep.where(:user_id => self.id, :steppingstone_id => step_stone.stepable.id).last
             UserStep.delete_all("user_id = #{self.id} And id != #{user_step.id}")
          end
        end
        ## next step
        user_step.update_attribute(:current_step, step_stone.step_number) unless user_step.blank?
      else

      end
      ## update the last milestone pushed
      #user_total.update_column(:last_milestone, Date.today)
      puts "bainur step_stone.step_number = #{step_stone.step_number}"
      puts "bainur step_stone = #{step_stone.id}"
      #user_step.update_attribute(:current_step, step_stone.step_number) unless user_step.blank?
      puts "bainur user step = #{user_step.id}" rescue nil
      unless user_step.blank?
        user_step.update_attribute(:current_step , step_stone.step_number)
        user_step.update_attribute(:steppingstone_id, steppingstone.id) #unless user_step.blank?
      else
        user_step = UserStep.where(:user_id => self.id).last
        user_step.update_attribute(:current_step , step_stone.step_number) rescue nil
        user_step.update_attribute(:steppingstone_id, steppingstone.id) rescue nil
      end
    end
  end

  # This method takes a user as parameter and deducts user's points based on the value set on this reward
  #
  def claim(reward, restaurant, latitude, longitude)
    self.transaction do
      case reward.reward_type
        when Reward::TYPES["REGULAR"]
          burn(reward.points)
        when Reward::TYPES["ONE_TIME"]
          burn(reward.points)
          RewardWallet.find_by_user_id_and_reward_id(id, reward.id).update_attributes(:status => RewardWallet::STATUS[:CLAIMED])
        when Reward::TYPES["MILESTONE"]
          burn(reward.points)
          RewardWallet.find_by_user_id_and_reward_id(id, reward.id).update_attributes(:status => RewardWallet::STATUS[:CLAIMED])
        when Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["BIRTHDAY"]
          burn(reward.points)
          RewardWallet.find_by_user_id_and_reward_id(id, reward.id).update_attributes(:status => RewardWallet::STATUS[:CLAIMED])
      end
      restaurant_reward= RestaurantReward.find_by_restaurant_id_and_reward_id(restaurant.id, reward.id)

      reward_transaction= RewardTransaction.new
      reward_transaction.latitude= latitude
      reward_transaction.longitude= longitude
      reward_transaction.restaurant_reward= restaurant_reward
      reward_transaction.user_id = self.id
      reward_transaction.points= reward.points
      reward_transaction.save
      reward_transaction.fix_restaurant_and_reward_by_lat_lng(latitude, longitude)
    end
  end

  def claim_with_staffcode_and_timer(reward, restaurant, latitude, longitude, staffcode, additional_info_reward = nil)
    self.transaction do
      case reward.reward_type
        when Reward::TYPES["REGULAR"]
          burn(reward.points)
        when Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"], Reward::TYPES["BIRTHDAY"]
          burn(reward.points)
          rw = RewardWallet.where("user_id = ? AND reward_id = ? AND status <> ? AND date(expiry_date) >= ?", id, reward.id, RewardWallet::STATUS[:CLAIMED], Date.current).order("created_at asc").first
          rw.update_attributes(:status => RewardWallet::STATUS[:CLAIMED]) unless rw.blank?
      end
      #      restaurant_reward= RestaurantReward.find_by_restaurant_id_and_reward_id(restaurant.id, reward.id)
      #      restaurant_reward = RestaurantReward.get_by_lat_Lng(reward, latitude, longitude)

      restaurant_lat_lng_id = Restaurant.get_id_by_lat_lng(reward, latitude, longitude)
      reward_transaction = RewardTransaction.new
      reward_transaction.staffcode = staffcode
      reward_transaction.latitude = latitude
      reward_transaction.longitude = longitude
      #      reward_transaction.restaurant_reward = restaurant_reward
      reward_transaction.reward_id = reward.id
      reward_transaction.restaurant_id = restaurant_lat_lng_id
      reward_transaction.user_id = self.id
      reward_transaction.points = reward.points
      reward_transaction.save
      ## if there is detail info for shipping / address or etc
      RewardTransactionDetail.save_info(reward_transaction,additional_info_reward) unless additional_info_reward.blank?
      #RewardSummary.update_data(reward_transaction.restaurant_id,Date.today)
      EmailMarketing.update_email_marketing_location(self, restaurant_lat_lng_id)
      Chain.update_user_fishbowl_location(self, restaurant_lat_lng_id)
      RestaurantUser.update_last_redeem(self)
      PointHistory.add_to_history(self.id, "Reward ##{reward.id} Claim", (reward.points * -1))
    end
  end

  def claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, additional_info_reward = nil)
    #self.transaction do
      case reward.reward_type
        when Reward::TYPES["REGULAR"], Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"], Reward::TYPES["BIRTHDAY"]
          rw = RewardWallet.where("user_id = ? AND reward_id = ? AND status <> ? AND date(expiry_date) >= ?", id, reward.id, RewardWallet::STATUS[:CLAIMED], Date.current).order("status desc, created_at asc").first
          rw.update_attributes(:status => RewardWallet::STATUS[:REDEEMING]) unless rw.blank?
      end

      restaurant_lat_lng_id = Restaurant.get_id_by_lat_lng(reward, latitude, longitude)
      reward_transaction = RewardTransaction.new
      reward_transaction.staffcode = staffcode
      reward_transaction.latitude = latitude
      reward_transaction.longitude = longitude
      #      reward_transaction.restaurant_reward = restaurant_reward
      reward_transaction.reward_id = reward.id
      reward_transaction.restaurant_id = restaurant_lat_lng_id
      reward_transaction.user_id = self.id
      reward_transaction.points = reward.points
      reward_transaction.redeeming = true
      reward_transaction.save
      unless additional_info_reward.blank?
        if additional_info_reward.class == String && additional_info_reward == "OLO"
          RewardTransactionPlatform.set_trans_type(reward_transaction, "OLO")
        elsif additional_info_reward.class == String && additional_info_reward == "NUORDER"
          RewardTransactionPlatform.set_trans_type(reward_transaction, "NUORDER")
        elsif additional_info_reward.class == String && additional_info_reward == "ONOSYS"
          RewardTransactionPlatform.set_trans_type(reward_transaction, "ONOSYS")
          onosys_rc = OnosysRewardClaim.where(user_id: self.id, reward_id: reward.id, reward_code: staffcode, location_id: (restaurant.restaurant_detail.try(:external_location_store_id) rescue nil) ).first
          onosys_rc.update_column("reward_transaction_id", reward_transaction.id) if onosys_rc
        else
          RewardTransactionDetail.save_info(reward_transaction,additional_info_reward)
        end
      end
    #end
  end


  def remove(reward, user)
    rw = RewardWallet.where("user_id = ? AND reward_id = ? AND status != ?", user.id, reward.id, RewardWallet::STATUS[:REMOVED]).first
    rw.update_attribute(:status, RewardWallet::STATUS[:REMOVED]) if rw
  end

  def self.update_created_at
    User.all.each_with_index do |x, d|
      m = Time.now + rand(20).days
      x.update_attributes(:created_at => m, :updated_at => m)
    end
  end

  def self.update_total_points_earned_for_existing_user
    User.all.each do |user|
      user_receipts = user.receipts.where("status = 3")
      total = []
      user_receipts.each do |receipt|
        total << receipt.last_transaction.total_points_earned.to_f
        #        receipt.receipt_transactions.where(:status => Receipt::STATUS[:APPROVED]).each do |rt|
        #          user.increment!(:total_points_earned, rt.total_points_earned.to_f)
        #          user.add_milestone_reward
        #        end
      end
      user.update_attribute(:total_points_earned, total.sum)
    end
  end

  ## activate / deactivate account
  def change_status
    if self.active
      # make it deactive
      self.update_attribute(:active, false)
    else
      # make it active
      self.update_attribute(:active, true)
    end
  end

  ## get the deals, that user is participate
  def deal_participants
    self.user_incentives
  end

  #categories part

  def validates_category_ids
    if self.category_part
      if self.category_ids.blank?
        self.errors.add(:category, "Please select at least 1 area to focus on.")
      elsif self.category_ids.size > 3
        self.errors.add(:category, "The maximum number of areas you can choose is 3.")
      end
    end
  end

  def group_emails_and_device_token_by_locale(user_ids)
    hash_map = {}
    if !user_ids.blank?
      users = User.where("id in (?)", user_ids)
      users.each do |user|
        hash_map[user.locale.id] = {} if hash_map[user.locale.id].blank?
        hash_map[user.locale.id][:emails] = [] if hash_map[user.locale.id][:emails].blank?
        hash_map[user.locale.id][:android_device_tokens] = [] if hash_map[user.locale.id][:android_device_tokens].blank?
        hash_map[user.locale.id][:iphone_device_tokens] = [] if hash_map[user.locale.id][:iphone_device_tokens].blank?
        hash_map[user.locale.id][:emails] << user.email
        hash_map[user.locale.id][:android_device_tokens] << user.device_token if user.sign_in_device_type.to_s.downcase.eql?("android") and !user.device_token.blank?
        hash_map[user.locale.id][:iphone_device_tokens] << user.device_token if user.sign_in_device_type.to_s.downcase.eql?("iphone") and !user.device_token.blank?
      end
      hash_map
    end
  end

  def group_message_pn_by_locale
    hash_map = {}
    self.chain.locales.each do |locale|
      notification = self.chain.point_push_notifications.where("locale_id = ?", locale.id).first
      hash_map[locale.id] = {} if hash_map[locale.id].blank?
      hash_map[locale.id]["email_subject"] = notification.blank? ? "" : notification.email_subject
      hash_map[locale.id]["email_content"] = notification.blank? ? "" : notification.email_content
      hash_map[locale.id]["email_content_html"] = notification.blank? ? "" : notification.email_content_html
      hash_map[locale.id]["notification"] = notification.blank? ? "" : notification.notification
    end
    return hash_map
  end

  def gcm_push(reg_ids_by_locale, hash_notification, point, image_url = nil)
    reg_ids_by_locale.each do |key, value|
      puts "---Started sending PN---"
      msg = key.blank? ? "" : hash_notification[key]["notification"]
      msg = msg.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
      msg = msg.gsub('__','')

      push_to_android(value[:android_device_tokens], (msg % {
          :chain_name => self.chain.name,
          :point_added => point,
          :receipt_id_link => image_url}))
      puts "---SENT---"
    end
  end

  def push_email_group(reg_ids_by_locale, hash_notification, point, image_url = nil)
    reg_ids_by_locale.each do |key, value|
      unless value[:emails].blank?
        email_subject = key.blank? ? "" : hash_notification[key]["email_subject"]
        email_content = key.blank? ? "" : hash_notification[key]["email_content"]
        email_content_html = key.blank? ? "" : hash_notification[key]["email_content_html"]
        email_content = email_content.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
        email_content = email_content.gsub('__','')
        email_content_html = email_content_html.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
        email_content_html = email_content_html.gsub('__','')

        value[:emails].each do |email|
          UserMailer.push_point_notice_email(self.chain, email, email_subject,
                                             (email_content % {
                                                 :chain_name => self.chain.name,
                                                 :point_added => point, :receipt_id_link => image_url}),
                                             (email_content_html % {
                                                 :chain_name => self.chain.name,
                                                 :point_added => point, :receipt_id_link => image_url} rescue email_content_html)
          ).deliver
        end
      end
    end
  end

  def push_email_group_using_mandrill(reg_ids_by_locale, hash_notification, point, image_url = nil, notif_setting)
    reg_ids_by_locale.each do |key, value|
      unless value[:emails].blank?
        email_subject = key.blank? ? "" : hash_notification[key]["email_subject"]
        email_content = key.blank? ? "" : hash_notification[key]["email_content"]
        email_content_html = key.blank? ? "" : hash_notification[key]["email_content_html"]
        email_content = email_content.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
        email_content = email_content.gsub('__','')
        email_content_html = email_content_html.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
        email_content_html = email_content_html.gsub('__','')

        mandrill_api = notif_setting.apikey rescue nil
        value[:emails].each do |email|

          p "push_email_group_using_mandrill"
          unless mandrill_api.blank?
            begin
              mandrill = Mandrill::API.new(mandrill_api)
              #  "POINTS NOTIFICATION - USER"
              template_content = [{"content"=>"example content", "name"=>"example name"}]
              message = {
                  "merge_vars"=> [{"vars"=>[{"content"=>"merge2 content", "name"=>"merge2"}], "rcpt"=>"recipient.email@example.com"}],
                  "to"=> [{"type"=>"to", "email"=> email, "name"=> email}],
                  "global_merge_vars" => [
                      { "name" => "APPNAME", "content" => self.chain.name },
                      { "name" => "CH_ADDR", "content" => "35 Hugus Aly Suite 300 Pasadena, USA" },
                      { "name" => "CH_PHONE", "content" => "411-111-111" },
                      { "name" => "FNAME", "content" => first_name },
                      { "name" => "LNAME", "content" => last_name },
                      { "name" => "TEL_NUM", "content" => phone_number },
                      { "name" => "EMAIL", "content" => email },
                      { "name" => "POINTS", "content" => "" },
                      { "name" => "PROMO", "content" => "" },
                      { "name" => "ACCT_NAME", "content" => "" }
                  ],
                  "tracking_domain"=>nil,
                  "subject"=> email_subject}
              async = false
              ip_pool = nil
              send_at = Time.now.utc
              mandrill.messages.send_template notif_setting.name, template_content, message, async, ip_pool, send_at
            rescue Mandrill::InvalidKeyError => e
              puts "A mandrill error occurred: #{e.class} - #{e.message}"
              UserMailer.push_point_notice_email(self.chain, email, email_subject,
                                                 (email_content % {
                                                     :chain_name => self.chain.name,
                                                     :point_added => point, :receipt_id_link => image_url}),
                                                 (email_content_html % {
                                                     :chain_name => self.chain.name,
                                                     :point_added => point, :receipt_id_link => image_url})
              ).deliver
            end
          else
            UserMailer.push_point_notice_email(self.chain, email, email_subject,
                                               (email_content % {
                                                   :chain_name => self.chain.name,
                                                   :point_added => point, :receipt_id_link => image_url}),
                                               (email_content_html % {
                                                   :chain_name => self.chain.name,
                                                   :point_added => point, :receipt_id_link => image_url})
            ).deliver
          end

        end
      end
    end
  end

  def push_to_android(reg_ids, notification)
    if application = self.chain.applications.by_device_type(1).first
      return if application.key.blank?
      message = GcmHelper::Message.new
      message.delay_while_idle = true
      message.add_data('alert', notification)
      message.add_data('timestamp', "#{Time.now}")
      #      key="AIzaSyCHAQW9i1vGKNUD83jSsSn8Jfgb2TPFGKc"
      sender = GcmHelper::Sender.new(application.key)
      response = sender.multicast_with_retry(message, reg_ids, 3)
    end
  end

  def push_to_iphone2(reg_ids_by_locale, hash_notification, point, image_url=nil)
    if application = self.chain.applications.by_device_type(2).first
      reg_ids_by_locale.each do |key, value|
        msg = key.blank? ? "" : hash_notification[key]["notification"]
        msg = msg.gsub(/(?<=__)(.*\n?)(?=__)/,'') if image_url.blank?
        msg = msg.gsub('__','')

        value[:iphone_device_tokens].each do |device_token|
          rapns_app = application.rapns_app
          return if rapns_app.blank?
          puts "push to iphone2 User " * 100
          n = Rpush::Apns::Notification.new
          n.app = rapns_app
          n.device_token = device_token
          n.alert = msg % {
              :chain_name => self.chain.name,
              :point_added => point,
              :receipt_id_link => image_url
          }

          n.sound = "1.aiff"
          n.badge = 1
          n.save!
        end
      end
    end
  end

  #mail function with mail gem
  def send_welcome_email(delay = false, preview = false, email_template = "welcome")
    selected_user = self
    from = selected_user.chain.email.blank? ? "\"#{selected_user.chain.name}\" <#{Setting.email.default_from}>" : selected_user.chain.email
    #subject, body, html_content, send_as = fetch_email_template(selected_user.chain.id, selected_user.locale.key, email_template)

    content_template = get_email_template_from_db(email_template)
    content_template = get_email_template_from_db("welcome") if content_template.blank?
    return if content_template.blank?

    mail = setup_mail(selected_user.email, from, "#{content_template.mail_subject}",
                      (content_template.mail_content_text % {:relevant => "Relevant",
                               :itunes_link => selected_user.chain.itunes_link,
                               :play_store_link => selected_user.chain.play_store_link
                      }),
                      content_template.send_as, delay, (content_template.mail_content_html.gsub('%', '%%') % {:relevant => "Relevant",
                                               :itunes_link => selected_user.chain.itunes_link,
                                               :play_store_link => selected_user.chain.play_store_link
        }))
    mail.body.charset = mail.charset
    if preview
      return mail
    else
      mail.deliver!
    end
  end

  def send_welcome_email_using_mandrill(notif_setting)
    p "send_welcome_email_using_mandrill"
    selected_user = self
    mandrill_api = notif_setting.apikey rescue nil
    unless mandrill_api.blank?
      begin
        mandrill = Mandrill::API.new(mandrill_api)
        # "WELCOME EMAIL"
        template_content = [{"content"=>"example content", "name"=>"example name"}]
        message = {
            "merge_vars"=> [{"vars"=>[{"content"=>"merge2 content", "name"=>"merge2"}], "rcpt"=>"recipient.email@example.com"}],
            "to"=> [{"type"=>"to", "email"=> selected_user.email, "name"=> selected_user.full_name}],
            "global_merge_vars" => [
                { "name" => "APPNAME", "content" => notif_setting.chain.name },
                { "name" => "CH_ADDR", "content" => "35 Hugus Aly Suite 300 Pasadena, USA" },
                { "name" => "CH_PHONE", "content" => "411-111-111" },
                { "name" => "FNAME", "content" => selected_user.first_name },
                { "name" => "LNAME", "content" => selected_user.last_name },
                { "name" => "TEL_NUM", "content" => selected_user.last_name },
                { "name" => "EMAIL", "content" => selected_user.email },
                { "name" => "F_SIGN_UP", "content" => "" },
                { "name" => "SIGNUP", "content" => "" },
                { "name" => "WELCOME", "content" => ""},
                { "name" => "PROMO", "content" => "" }
            ],
            "tracking_domain"=>nil,
            "subject"=>"Welcome to #{notif_setting.chain.name.upcase rescue nil}"}
        async = false
        ip_pool = nil
        send_at = Time.now.utc
        mandrill.messages.send_template notif_setting.name, template_content, message, async, ip_pool, send_at
      rescue Mandrill::InvalidKeyError => e
        puts "A mandrill error occurred: #{e.class} - #{e.message}"
        selected_user.send_welcome_email
      end
    else
      selected_user.send_welcome_email
    end
  end

  def send_reward_expired_notification_email(reward_wallet, template)
    selected_user = self
    from = self.chain.email.blank? ? "\"#{selected_user.chain.name}\" <#{Setting.email.default_from}>" : selected_user.chain.email
    #subject, body, html_content, send_as = fetch_email_template(selected_user.chain.id, selected_user.locale.key, "forgot_password")

    content_template = template
    return if content_template.blank?

    mail = setup_mail(selected_user.email, from,
                      (content_template.email_subject % {
                          :reward_title => reward_wallet.reward.name,
                          :reward_exp_date => (reward_wallet.expiry_date.strftime("%Y-%m-%d") rescue nil),
                          :reward_fine_print => reward_wallet.reward.fineprint,
                          :chain_name => self.chain.name
                      }),
                      (content_template.email_body_text % {
                          :reward_title => reward_wallet.reward.name,
                          :reward_exp_date => (reward_wallet.expiry_date.strftime("%Y-%m-%d") rescue nil),
                          :reward_fine_print => reward_wallet.reward.fineprint,
                          :chain_name => self.chain.name
                      }),
                      nil, false,
                      (content_template.email_body_html % {
                          :reward_title => reward_wallet.reward.name,
                          :reward_exp_date => (reward_wallet.expiry_date.strftime("%Y-%m-%d") rescue nil),
                          :reward_fine_print => reward_wallet.reward.fineprint,
                          :chain_name => self.chain.name
                      }))

    mail.deliver!
  end

  def send_reward_expired_notification_device(reward_wallet, template)
    notification = (template.notification % {
        :reward_title => reward_wallet.reward.name,
        :reward_exp_date => (reward_wallet.expiry_date.strftime("%Y-%m-%d") rescue nil),
        :reward_fine_print => reward_wallet.reward.fineprint,
        :chain_name => self.chain.name
    })
    if self.sign_in_device_type.to_s.downcase.eql?("android") && !self.device_token.blank?
      push_to_android([self.device_token], notification)
      puts "---SENT to android---"
    elsif self.sign_in_device_type.to_s.downcase.eql?("iphone") and !self.device_token.blank?
      if application = self.chain.applications.by_device_type(2).first
        rapns_app = application.rapns_app
        return if rapns_app.blank?
        n = Rpush::Apns::Notification.new
        n.app = rapns_app
        n.device_token = self.device_token
        n.alert = notification

        n.sound = "1.aiff"
        n.badge = 1
        n.save!
      end
    end
  end

  def send_forgot_password_email(pass, delay = false, preview = false)
    selected_user = self
    from = self.chain.email.blank? ? "\"#{selected_user.chain.name}\" <#{Setting.email.default_from}>" : selected_user.chain.email
    #subject, body= fetch_email_template(selected_user.chain.id, selected_user.locale.key, "forgot_password")

    content_template = get_email_template_from_db("forgot_password")
    return if content_template.blank?

    mail = setup_mail(selected_user.email, from, "#{content_template.mail_subject}",
                      (content_template.mail_content_text % {:temp_pwd => pass,:itunes_link => selected_user.chain.itunes_link,
                                                             :play_store_link => selected_user.chain.play_store_link}),
                      content_template.send_as, delay, (content_template.mail_content_html % {:temp_pwd => pass,:itunes_link => selected_user.chain.itunes_link,
                                                                                              :play_store_link => selected_user.chain.play_store_link}))

    if preview
      return mail
    else
      mail.deliver!
    end
  end

  def send_forgot_password_email_using_mandrill(record, notif_setting)
    p 'send_forgot_password_email_using_mandrill'
    selected_user = self
    mandrill_api = notif_setting.apikey rescue nil
    unless mandrill_api.blank?
      begin
        mandrill = Mandrill::API.new(mandrill_api)
        template_name = "FORGOT PASSWORD"
        template_content = [{"content"=>"example content", "name"=>"example name"}]
        message = {
            "merge_vars"=> [{"vars"=>[{"content"=>"merge2 content", "name"=>"merge2"}], "rcpt"=>"recipient.email@example.com"}],
            "to"=> [{"type"=>"to", "email"=> selected_user.email, "name"=> selected_user.full_name}],
            "global_merge_vars" => [
                { "name" => "APPNAME", "content" => notif_setting.chain.name },
                { "name" => "CH_ADDR", "content" => "35 Hugus Aly Suite 300 Pasadena, USA" },
                { "name" => "CH_PHONE", "content" => "411-111-111" },
                { "name" => "FNAME", "content" => selected_user.first_name },
                { "name" => "LNAME", "content" => selected_user.last_name },
                { "name" => "TEL_NUM", "content" => selected_user.last_name },
                { "name" => "EMAIL", "content" => selected_user.email },
                { "name" => "SECURITY", "content" => "" },
                { "name" => "SIGNUP", "content" => "" },
                { "name" => "PASSWORD", "content" => record.password},
                { "name" => "PROMO", "content" => "" }
            ],
            "tracking_domain"=>nil,
            "subject"=>"Password reset request - #{notif_setting.chain.name.upcase rescue nil}",
        }
        async = false
        ip_pool = nil
        send_at = Time.now.utc
        mandrill.messages.send_template template_name, template_content, message, async, ip_pool, send_at
      rescue Mandrill::InvalidKeyError => e
        puts "A mandrill error occurred: #{e.class} - #{e.message}"
        if Setting.email.turned_on and record.password
          if(Setting.email.delayed)
            record.send_forgot_password_email(record.password, true)
          else
            record.send_forgot_password_email(record.password, false)
          end
        end
      end
    else
      if Setting.email.turned_on and record.password
        if(Setting.email.delayed)
          record.send_forgot_password_email(record.password, true)
        else
          record.send_forgot_password_email(record.password, false)
        end
      end
    end
  end

  def reject_receipt_email(receipt, reject_reason, delay = false, preview = false)
    selected_user = self
    #subject, body= fetch_email_template(receipt.chain.id, selected_user.locale.key, "receipt_reject")
    content_template = get_email_template_from_db("receipt_reject")
    return if content_template.blank?

    mail = setup_mail(selected_user.email, Setting.email.default_from, "#{content_template.mail_subject % {:receipt_id => receipt.id}}",
                      (content_template.mail_content_text % {:reject_reason => reject_reason,
                               :receipt_id_link => "<a href='#{receipt.image.url}' target='_blank'>#{receipt.id}</a>",
                               :itunes_link => selected_user.chain.itunes_link,
                               :play_store_link => selected_user.chain.play_store_link
                      }),
                      content_template.send_as, delay, (content_template.mail_content_html % {:reject_reason => reject_reason,
                                                                        :receipt_id_link => "<a href='#{receipt.image.url}' target='_blank'>#{receipt.id}</a>",
                                                                        :itunes_link => selected_user.chain.itunes_link,
                                                                        :play_store_link => selected_user.chain.play_store_link
        }))
    if preview
      return mail
    else
      mail.deliver!
    end
  end

  def get_email_template_from_db(template)
    content_template = EmailTemplate.where(:chain_id => self.chain.id, :locale_id => self.locale_id, :template_name => template).first
    return content_template
  end

  def setup_mail(user_email, from, subject, body, content_type, delay, html_content = "")
    mail = if delay
             Mail.new do #TODO : looking for the way how to delay send email with mail gem
               to user_email
               from from
               subject subject
             end
           else
             Mail.new do
               to user_email
               from from
               subject subject
             end
           end

    if content_type.blank? || content_type.eql?("multipart/alternative") || content_type.eql?("text/plain")
      text_part = Mail::Part.new do
        body body
      end
      mail.text_part = text_part
    end

    if content_type.blank? || content_type.eql?("multipart/alternative") || content_type.eql?("text/html")
      html_part = Mail::Part.new do
        content_type 'text/html'
        body html_content
      end
      mail.html_part = html_part
    end
    mail
  end


  def fetch_email_template(chain_id, user_locale, email_name)
    subject= REDIS.hget "chain_#{chain_id}", "#{user_locale}_#{email_name}_email_subject"
    body= REDIS.hget "chain_#{chain_id}", "#{user_locale}_#{email_name}_email_body"

    return subject, body
  end

  def push_notif_olo
    #last_olo_created = self.receipts.where(:is_olo => true).limit(1).order("id desc").first.created_at rescue nil
    receipt_olo = self.receipts.order('id desc').first rescue nil
    if !receipt_olo.blank? and receipt_olo.is_olo == true
      last_olo_created = receipt_olo.created_at

      last_olo_created = last_olo_created.in_time_zone("EST") rescue nil
      if !last_olo_created.blank? #and last_olo_created.hour > 23 and last_olo_created.hour < 6 and last_olo_created.date == Time.zone.now.in_time_zone("EST").to_date
        return true
      else
        return false
      end
    end
  end

  ## return true if user active and push email is true
  def push_to_email?
    return active == true && push_email == true
  end

  ## return true if user active and push email is true
  def push_to_device?
    return active == true && push_device == true
  end

  def update_phone_info(phone_model, os_version, chain_app_key)
    self.phone_model = phone_model
    self.os_version = os_version
    self.chain_app_key = chain_app_key
    self.category_part = false
    self.save
  end

  def generate_password
    password_generated = (('a'..'z').to_a + ('0'..'9').to_a).shuffle.first(8).join
    self.password = password_generated
    self.password_confirmation = password_generated
  end

  def add_to_email_marketing
    if self.chain.mailchimp_sync
      EmailMarketing.import_user_to_email_marketing(self)
    end
  end

  def add_to_emma_marketing
    if self.chain.emma_setting
      EmmaMarketing.import_user_to_emma_marketing(self)
    end
  end

  def add_to_fishbowl_user
    if self.chain.fishbowl_setting
      self.chain.add_to_fishbowl_user(self)
    end
  end


  def top_5_logs
    UserDeviceLog.where(:user_id => self.id).order("created_at DESC").limit(5)
  end

  def domain_email_validation
    if required_domain_email_validation
      error_key = "email_invalid"
      error_key = "facebook_email_invalid" if self.register_type.eql?(User::USERS_TYPES["Facebook"])
      begin
        domain = Mail::Address.new(self.email).domain
        Resolv::DNS.open do |dns|
          @mx = dns.getresources(domain.to_s, Resolv::DNS::Resource::IN::MX)
        end
        if @mx.empty?
          self.errors.add(:email, I18n.t(error_key))
        end
      rescue
        self.errors.add(:email, I18n.t(error_key))
      end
    end
  end

  def self.send_notification_expired_certificate_mail(application, notice)
    chain = application.chain
    body = notice
    if Setting.apns.add_env_on_subject
      set_subject = "[TRELEVANT] #{application.name} certificate expired notice"
    else
      set_subject = "#{application.name} certificate expired notice"
    end
    mail = Mail.new do
      to Setting.apns.send_to
      from Setting.email.default_from
      subject set_subject
    end


    text_part = Mail::Part.new do
      body body
    end
    mail.text_part = text_part


    html_part = Mail::Part.new do
      content_type 'text/html'
      body body
    end
    mail.html_part = html_part
    mail.deliver!
  end

  def reset_authentication_token!
    self.update_column(:authentication_token, self.class.authentication_token)
  end

  def aoo_synced?
    begin
      as = connection.execute("SELECT customer_id FROM aoo_customers WHERE user_id = #{self.id}")
      !as.values.blank?
    rescue
      false
    end
  end

  def user_milestone_points
    self.point_threshold.to_f/(self.try(:chain).try(:user_points) || 1)
  end

  def profile_pic
    profile = self.try(:user_profile).try(:reload)
    profile.try(:reload).try(:avatar).try(:url)
  end

  def favorite_location_name
    self.try(:favorite_restaurant).try(:app_display_text)
  end

  def self.web_and_mobile_signup_check(app, user, new_sign_in_device_type)
    if ["android", "iphone", "browser_compatible"].include?(user.sign_in_device_type) && (app.device_type.eql?(3) || new_sign_in_device_type.eql?("browser"))
      return {:status => false, notice: "Please access the loyalty app through our iPhone and android apps and Compatible Browser apps."}
    else
      if user.sign_in_device_type.eql?("browser") && app.device_type.eql?(3) && ["android", "iphone", "browser_compatible"].include?(new_sign_in_device_type)
        return {:status => false, notice: "You login into Browser app but use  mobile device type or browser compatible device type"}
      elsif user.sign_in_device_type.eql?("browser") && [1,2,4].include?(app.device_type)
        if new_sign_in_device_type.eql?("browser")
          return {:status => false, notice: "You login into Mobile or Compatible Browser app but use browser device type."}
        elsif ["android", "iphone", "browser_compatible"].include?(new_sign_in_device_type)
          return "CONT"
        else
          return "CONT"
        end
      else
        return "CONT"
      end
    end
  end

  def approved_receipt_count(type = "weekly")
    now = Time.current
    case type
      when "daily"
        start_date = now.beginning_of_day
        end_date = now.end_of_day
      when "weekly"
        start_date = now.beginning_of_week
        end_date = now.end_of_week
      when "monthly"
        start_date = now.beginning_of_month
        end_date = now.end_of_month
      when "yearly"
        start_date = now.beginning_of_year
        end_date = now.end_of_year
    end
    self.receipts.where(:status => Receipt::STATUS[:APPROVED], :created_at => start_date..end_date).count
  end

  def set_birthday(date)
    self.dob_day = date.day rescue nil
    self.dob_month = date.month rescue nil
    self.dob_year = date.year rescue nil
  end

  def gender_title
    case self.gender
      when '0'
        "Not Provided"
      when '1'
        "Male"
      when '2'
        "Female"
      when '3'
        "Other"
      else
        "-"
    end
  end

  def self.is_valid_email_address?(email)
    begin
      domain = Mail::Address.new(email).domain
      Resolv::DNS.open do |dns|
        @mx = dns.getresources(domain.to_s, Resolv::DNS::Resource::IN::MX)
      end
      return false if @mx.empty?
      return true
    rescue
      return false
    end
  end

  def self.update_users_fishbowl_from_ftp(chain)
    begin
      if chain.fishbowl_setting.try(:ftp_url) && chain.fishbowl_setting.try(:ftp_port) && chain.fishbowl_setting.try(:ftp_username) && chain.fishbowl_setting.try(:ftp_password)
        require 'net/ftp'
        ftp =Net::FTP.new
        ftp.passive = true
        ftp.connect(chain.fishbowl_setting.ftp_url, chain.fishbowl_setting.ftp_port)
        ftp.login(chain.fishbowl_setting.ftp_username,chain.fishbowl_setting.ftp_password)
        begin
          ftp.chdir(chain.fishbowl_setting.ftp_path_directory)
        rescue => e
          puts "update_users_fishbowl_from_ftp #{e}"
        end
        @files = ftp.nlst("*.csv") | ftp.nlst("*.xls") | ftp.nlst("*.CSV") | ftp.nlst("*.XLS")
        @files.each do |file|
          if ftp.mtime(file).to_datetime >= (Time.zone.now.utc.to_date - 1.days)
            fl = FishbowlLog.create(:chain_id => chain.id, :log_type => "Rever-Daily Sync", :status => "in queue")
            chain.fishbowl_setting.update_column(:fishbowl_log_id, fl.id)
            Delayed::Job.enqueue(DailyUpdateUsersFishbowlJob.new(chain, file, fl), :delayable_type => "Update users fishbowl")
          end
        end if @files.present?

        ftp.close
      end
    rescue => e
      puts "update_users_fishbowl_from_ftp #{e}"
      ftp.close
    end
  end

  def check_fake_reward_transactions(chain, reward)
    return "CONT" if reward.reward_type == Reward::TYPES["REGULAR"]
    reward_code = REDIS.get "rewardcode_chain_#{chain.id}"
    reward_code_setting = JSON.parse(reward_code) rescue nil
    reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE

    request_time = Time.zone.now
    active_reward_count = RewardWallet.where(:user_id => self.id, :reward_id => reward.id, :status => [ RewardWallet::STATUS[:ACTIVE], RewardWallet::STATUS[:REDEEMING]]).count
    reward_transactions = RewardTransaction.where(:user_id => self.id, :reward_id => reward.id, :redeeming => true, :created_at => [request_time.beginning_of_day..request_time.end_of_day]).order("id DESC")

    if active_reward_count > reward_transactions.size
      return "CONT"
    elsif active_reward_count <= reward_transactions.size
      reward_transactions.first.update_column(:created_at, Time.zone.now)
      return [reward_transactions.first.staffcode, reward_code_setting["zero_padding"].to_i]
    else
      return "CONT"
    end
  end

  def email_activate
    confirm_user = self.user_confirmation
    confirm_user.email_confirmed = true
    confirm_user.confirm_token = nil
    confirm_user.save
  end

  def is_activated_email?
    return self.user_confirmation.email_confirmed == true
  end

  def set_confirmation_token
    p "set confirmation token"
    user_conf = self.user_confirmation
    if user_conf.blank?
      UserConfirmation.create(:user_id => self.id, :confirm_token => SecureRandom.urlsafe_base64.to_s)
    else
      user_conf.update_column(:confirm_token, SecureRandom.urlsafe_base64.to_s)
    end
    p "set confirmation token finished"
  end

end
