class Milestone < ActiveRecord::Base
  #  has_many :milestone_points
  belongs_to :chain
  belongs_to :reward

  default_scope { where("deleted_at" => nil) }

  validates :chain, :name, :presence => true
  validates :milestone_number, :presence => true,
    :numericality => {:greater_than => 0, :only_integer => true, :message => "Must be a integer number and greater than 0"}
  validates :threshold_points, :presence => true,
    :numericality => {:greater_than => 0, :only_integer => true, :message => "Must be a integer number and greater than 0"}

  validates_uniqueness_of :milestone_number, :scope => :chain_id
  validate :validate_number_and_points
  
  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def validate_number_and_points
    start_value, end_value = self.get_milestone_number_to_compare
    if !start_value.eql?(0) or !end_value.eql?(0)
      if !start_value.eql?(0) and end_value.eql?(0)
        if self.threshold_points.to_i <= start_value
          self.errors.add(:threshold_points, "Please add points greater than #{start_value}")
        end
      elsif start_value.eql?(0) and !end_value.eql?(0)
        if self.threshold_points.to_i >= end_value
          self.errors.add(:threshold_points, "Please add points less than #{end_value}")
        end
      else
        if (start_value > self.threshold_points.to_i) or end_value < self.threshold_points.to_i
          self.errors.add(:threshold_points, "Please add points greater than #{start_value} and less than #{end_value}")
        end
      end
    end
  end

  protected

  def get_milestone_number_to_compare
    milestones = self.chain.milestones
    if self.new_record?
      start_milestones = milestones.where("milestone_number < ?", self.milestone_number)
      end_milestones = milestones.where("milestone_number > ?", self.milestone_number)
    else
      start_milestones = milestones.where("milestone_number < ? AND id != ?", self.milestone_number, self.id)
      end_milestones = milestones.where("milestone_number > ? AND id != ?", self.milestone_number, self.id)
    end
    start_value = start_milestones.map(&:threshold_points).max || 0
    end_value = end_milestones.map(&:threshold_points).min || 0
    [start_value, end_value]
  end

end