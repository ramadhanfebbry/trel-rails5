class PartnerRewardJob < Struct.new(:chain, :reward, :params)

  def perform
    p "perform PartnerRewardJob"
    @chain = chain
    @reward = reward

    all_location = (params[:valid_all_locations] || false rescue false)
    pr = {}
    pr[:partner_reward] = {:order_minimum => params[:discount_formula][:order_minimum],
                               :discount_type => params[:discount_formula][:discount_type],
                               :discount_order => params[:discount_formula][:discount_order],
                               :discount_value => params[:discount_formula][:discount_value],
                               :discount_source_price => params[:discount_formula][:discount_source_price],
                               :minimum_count_order_from_category => params[:discount_formula][:minimum_count_order_from_category],
                               :discount_count_from_category => params[:discount_formula][:discount_count_from_category]
    }
    partner = PartnerReward.new(pr[:partner_reward])
    partner.valid_all_locations = all_location
    partner.valid_locations = "{#{params[:valid_locations].join(",") rescue nil}}"
    partner.reward_id = @reward.id
    partner.save

    if all_location
      valid_category_ids = PartnerCategory.where("chain_id = #{@chain.id}").map(&:id)
    else
      valid_category_ids = []
      restaurant_ids = params[:valid_locations] rescue []
      restaurant_ids.each do |rest_id|
        partner_categories = PartnerSubCategory.includes(:restaurant_categories).where("partner_sub_categories.chain_id = #{@chain.id}  and restaurant_categories.restaurant_id = #{rest_id}")
        partner_categories.each do |category|
          valid_category_ids << category.partner_category.id.to_i if category.partner_category
        end if partner_categories
      end
    end

    params[:valid_days_of_week_and_time].each do |partner_hour|
      hour = partner_hour[1]
      PartnerHour.create(partner_reward_id: partner.id, day_of_week: hour["dow"], start: hour["start"],  end: hour["end"] ) if hour["start"].present? && hour["end"].present?
    end if params[:valid_days_of_week_and_time].present?

    order_category_params = params[:discount_formula][:order_from_category] rescue []
    order_category_params.each do |category|
      if valid_category_ids.include?(category["categoryId"].to_i)
        mandatory = category["type"] == 0 ? false : true rescue false
        PartnerRewardCategory.create(partner_reward_id: partner.id, category_id: category["categoryId"], mandatory: mandatory)
      end
    end if order_category_params

    order_discount_params = params[:discount_formula][:discount_from_category] rescue []
    order_discount_params.each do |disc|
      if valid_category_ids.include?(disc["categoryId"].to_i)
        mandatory = disc["type"] == 0 ? false : true rescue false
        PartnerRewardDiscount.create(partner_reward_id: partner.id, category_id: disc["categoryId"], mandatory: mandatory)
      end
    end if order_discount_params

    p "End Perform PartnerRewardJob"
  end

end