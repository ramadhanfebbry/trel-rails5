class PerkWallet < ActiveRecord::Base
  belongs_to :user
  belongs_to :perk

  STATUS= {ACTIVE: 1, REMOVED: 2, CLAIMED: 3, INACTIVE: 4}

  default_scope { where(:deleted_at => nil) }
  scope :active, -> { where("expiry_date > ?", Time.now) }
  scope :by_user_id , -> (x) { where("user_id = ?", x) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def is_expired?
    self.expiry_date < Time.now 
  end
end
