class UserActivityNotification < ActiveRecord::Base

  has_many :user_activity_notification_fields, :order => "id ASC"

  validates :description, :length => {:maximum=> 200 }, :presence => true
  validates :notification, :length => {:maximum=> 500 }, :presence => true
  validates :unique_identifier, :presence => true
  validates :delay_time, :numericality => {
                           :greater_than_or_equal_to => 0,
                           :only_integer => true,
                           :less_than_or_equal_to => 1000
                       }

  accepts_nested_attributes_for :user_activity_notification_fields, allow_destroy: true, limit: 5

end
