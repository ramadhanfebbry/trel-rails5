class Survey < ActiveRecord::Base
  belongs_to :chain

  has_one :submit_survey
  has_many :questions, -> { order 'order_number asc' }
  has_many :offers
  has_many :rewards
  has_many :question_choices, :through => :questions
  has_many :surveys_users
  has_many :rotating_groups, -> { order 'priority asc' }
  has_many :rotating_questions, :class_name => "Question", :through => :rotating_groups, :source => :questions

  default_scope { where("deleted_at" => nil) }
  scope :by_chain_ids , -> (x) { where("chain_id in(?)", x) }


  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end

  def self.have_average(chain_ids)
      surveys=Survey.where("chain_id in (?)", chain_ids)
      chain = Chain.where("id in (?)", chain_ids).first
      restaurants = chain.restaurants
      surveys.each do |sr|
        if RestaurantSurveyAvg.where("survey_id = #{sr.id} and restaurant_id in (?) and averages > 0 and (q1 > 0 or q2 >0 or q3>0)", restaurants).count > 0
          return sr
        end
      end
  end

  validates :title, :chain_id, :valid_from_date, :valid_to_date, :presence => true
  validates :valid_to_date, :presence => true,  :if => :validate_from_date

  attr_accessor :point_validation

  validates :point, :numericality => {
      :greater_than => 0,
      :only_integer => true
  }, :if => :point_validation_required?


  STATUS = {"Active"=>1, "InActive"=>0}

  def validate_from_date
    if self.valid_to_date && self.valid_from_date
      errors.add( :valid_to_date, "must be after the From Date") if self.valid_from_date > self.valid_to_date
    end
  end

  def restaurant_collections
    restaurants = []
    unless self.offers.blank?
      self.offers.each do |offer|
        restaurants << offer.restaurants
      end
    end
    restaurants = restaurants.flatten
    restaurants = restaurants.delete_if{|x| x.status == false} unless restaurants.blank?
    return restaurants
  end

  def all_questions_collection(user, application_key)
    all_type = Question::TYPES.values
    all_type.delete(Question::NEW_TYPES["Dropdown"]) if application_key && !application_key.show_new_question_types
    all_type.delete(Question::NEW_TYPES["Slider"]) if application_key && !application_key.show_new_question_slider
    all_type.delete(Question::NEW_TYPES["Multiple Dropdown"]) if application_key && !application_key.show_new_question_multiple_dropdown
    conditions = "question_type IN (#{all_type.join(",")})"
    ordered_questions = []
    render_questions = []
    questions_list_from_group = []
    self.rotating_groups.each { |group| questions_list_from_group << group.questions.where(conditions) }
    questions_list_from_group = questions_list_from_group.flatten
    questions_list_from_group.each do |question|
      unless user.blank?
        user_rotating_question = UserRotatingQuestion.where(:question_id => question.id, :user_id => user.id).first
        if user_rotating_question.blank?
          UserRotatingQuestion.create(:user_id => user.id, :question_id => question.id, :render_count => 1)
          render_questions << question
        elsif !user_rotating_question.completed
          user_rotating_question.increment!(:render_count, 1)
          user_rotating_question.update_attribute(:completed, true) if question.rotating_group.max_renders <= user_rotating_question.reload.render_count
          render_questions << question
        end
        break if render_questions.size.eql?(self.max_rotating_questions)
      end
    end
    self.questions.where(conditions).each do |question|
      question.get_rotating_questions_from_collection('before', render_questions).each do |que|
        ordered_questions << que
      end
      ordered_questions << question
      question.get_rotating_questions_from_collection('after', render_questions).each do |que|
        ordered_questions << que
      end
    end
    ordered_questions.compact
  end

  def point_validation_required?
    if self.point_validation
      true
    else
      false
    end
  end

  def having_averages?
    self.questions.map(&:question_type).include?(Question::TYPES["Multiple Choice"])
  end

  def send_incentive_to(user, survey_user)
    chain = self.chain
    return if self.point.blank? && self.reward_id.blank?
    if chain
      send_incentive = false
      today = Time.current.utc.to_date
      week_start = today.beginning_of_week
      week_end =   today.end_of_week
      month_start = today.beginning_of_month
      month_end =   today.end_of_month
      daily_user_survey_answered_count = SurveysUser.includes(:survey).where("surveys.chain_id = ? AND surveys_users.user_id = ? AND surveys_users.qualified_survey_incentive IS TRUE AND date(surveys_users.created_at) = ?", chain.id, user.id, today).count
      weekly_user_survey_answered_count = SurveysUser.includes(:survey).where("surveys.chain_id = ? AND surveys_users.user_id = ? AND surveys_users.qualified_survey_incentive IS TRUE AND surveys_users.created_at >= ? AND surveys_users.created_at <= ?", chain.id, user.id, week_start, week_end).count
      monthly_user_survey_answered_count = SurveysUser.includes(:survey).where("surveys.chain_id = ? AND surveys_users.user_id = ? AND surveys_users.qualified_survey_incentive IS TRUE AND surveys_users.created_at >= ? AND surveys_users.created_at <= ?", chain.id, user.id, month_start, month_end).count

      if chain.survey_incentive_daily_cap > 0
        if chain.survey_incentive_daily_cap > daily_user_survey_answered_count
          send_incentive = true
        else
          send_incentive = false
        end
      else
        send_incentive = true
      end

      if send_incentive
        if chain.survey_incentive_weekly_cap > 0
          if chain.survey_incentive_weekly_cap > weekly_user_survey_answered_count
            send_incentive = true
          else
            send_incentive = false
          end
        else
          send_incentive = true
        end
      end

      if send_incentive
        if chain.survey_incentive_monthly_cap > 0
          if chain.survey_incentive_monthly_cap > monthly_user_survey_answered_count
            send_incentive = true
          else
            send_incentive = false
          end
        else
          send_incentive = true
        end
      end

      if send_incentive
        if !self.point.blank?
          user.earn(self.point)
          PointHistory.add_to_history(user.id, "Survey Incentive", self.point)
        elsif !self.reward_id.blank?
          RewardWallet.create_with_delay(:reward_id => self.reward_id, :user_id => user.id)
        end
        survey_user.update_column(:qualified_survey_incentive, true)
      end
    end

  end

  #after_update :update_status
  #
  #def update_status
  #  if(self.status==STATUS["Active"])
  #    Survey.update_all({:status => STATUS["InActive"]}, "chain_id = #{self.chain_id} AND id != #{self.id}")
  #  end
  #end

  accepts_nested_attributes_for :questions
end