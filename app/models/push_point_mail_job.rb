class PushPointMailJob < Struct.new(:user, :reg_ids_by_locale, :hash_notification, :point, :receipt)

  def perform
    p "Perform PUsh Point Mail Job"
    chain = user.chain
    notif_setting = chain.notification_settings.where(email_template: "points_notification_user").first rescue nil
    image_url = nil
    image_url = "<a href='#{receipt.image.url}' target='_blank'>#{receipt.id}</a>" unless receipt.blank?

    if notif_setting.blank? || ((notif_setting && notif_setting.send_type.eql?(1)) || (notif_setting && notif_setting.send_type.eql?(3)))
      p 'send point notification email on fishbowl here for single user----'
      fishbowl_email_sent = chain.send_point_notif_for_single_user(user, point)
      unless fishbowl_email_sent
        user.push_email_group(reg_ids_by_locale, hash_notification, point, image_url)
      end
    elsif notif_setting.send_type.eql?(2)
      user.push_email_group_using_mandrill(reg_ids_by_locale, hash_notification, point, image_url, notif_setting)
    end

    #    reward.push_to_android_and_email(reg_ids_by_locale, hash_notification)
    puts "Completed sending PN to Mail"
  end

  
end
