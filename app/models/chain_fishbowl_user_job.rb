class ChainFishbowlUserJob < Struct.new(:chain, :action_type)

  def perform
    if action_type == "add"
      chain.add_users_to_fishbowl_user
    elsif action_type == "remove"
      chain.remove_users_from_fishbowl_user
    end
  end

end