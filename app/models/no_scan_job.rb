# encoding: utf-8
require "mail"
class NoScanJob < ActiveRecord::Base
  TYPES = {
      'Percentage' => 1,
      'Activity' => 2,
      'User' => 3
  }

  REWARD_TYPES = {
      'Reward' => 1,
      'Point' => 2,
      'Notification' => 3,
  }

  STATUS = {
      "In Progress" => 0,
      "Queue" => 1,
      "Approved" => 2,
      "Completed" => 3,
      "Failed" => 4,
      "Rejected" => 5
  }

  SCHEDULE = {
      "One Time" => 1,
      "Daily" => 2,
      "Weekly" => 3,
      "Monthly" => 4
  }

  Mail.defaults do
    delivery_method :smtp, {
                             :address => Setting.smtp.host,
                             :port => Setting.smtp.port,
                             :user_name => Setting.smtp.username,
                             :password => Setting.smtp.password,
                             :authentication => 'plain',
                             :enable_starttls_auto => true
                         }
  end

  has_many :delayed_jobs
  #has_many :ownerjob_logs, :class => "NoscanjobLog"
  has_many :noscanjob_logs
  has_many :owner_job_histories, :dependent => :destroy
  has_one :no_scan_job_scheduler, :foreign_key => 'job_id', :dependent => :destroy
  belongs_to :owner
  belongs_to :chain

  scope :queue, -> { includes(:owner_scheduler).where("status = 1") }
  scope :inprogress, -> { includes(:owner_scheduler).where("status = 0") }
  scope :approved, -> { includes(:owner_scheduler).where("status = 2") }
  scope :completed, -> { includes(:owner_scheduler).where("status = 3") }
  scope :failed, -> { includes(:owner_scheduler).where("status = 4") }
  scope :rejected, -> { includes(:owner_scheduler).where("status = 5") }
  scope :admin_only, -> { where("no_scan_jobs.owner_id is null") }

  after_create :mail_to_owner, :if => Proc.new { |f| f.individual == false }
  #after_create :individual_execution , if => Proc.new { |f| f.individual == true }
  after_update :update_total_users, :if => Proc.new { |f| f.individual == false }

  validate :validate_executed_at, :if => Proc.new { |f| f.schedule_type == 1 }

  def individual_execution
    unless individual_user_id.blank?
      ## when push reward
      result = nil
      result = RewardWallet.create_with_delay(:user_id => self.individual_user_id, :reward_id => self.reward_id,
                                              :description => "pushed from dashboard #{created_by_user}") if self.reward_id
      user = User.find(individual_user_id)
      result = user.earn(self.points) if user and user.active == true and self.points
      PointHistory.add_to_history(user.id, "Dashboard Individual Job #{created_by_user}", self.points) if user and user.active == true
      puts "results ================"
      puts result
      self.update_column(:status, 3)
    else
      self.update_column(:status, 4)
    end
  end

  def ind_description
    #if self.points.blank?
    #  if self.status == 3  || self.status == nil
    #    return "#{Reward.find(self.reward_id).name}  successfully pushed "#,Points : #{self.points}"
    #  else
    #    return "failed to push #{Reward.find(self.reward_id).name}"#,Points : #{self.points}"
    #  end
    #else
    #  if self.status == 3  || self.status == nil
    #    return "#{self.points} points successfully sent "#,Points : #{self.points}"
    #  else
    #    return "failed to send #{self.points} points"#,Points : #{self.points}"
    #  end
    #end
    return self.description
  end

  def individual_description
    if self.points.blank?
      return "#{self.individual_email} " #,Reward : #{Reward.find(self.reward_id).name}"
    else
      return "#{self.individual_email} " #,Points : #{self.points}"
    end
  end

  def validate_executed_at
    puts executed_at.utc
    #puts "validation"
    #puts Time.zone.now.utc
    if executed_at.utc < Time.zone.now.utc
      errors.add(:executed_at, "Date is cannot lower that now time #{Time.zone.now}")
    end
  end

  ## scope
  scope :search, -> (params) { type(params[:type]).chains(params[:chain_id]) }

  scope :chains, -> (value) { where("chain_id" => value) if (!value.blank?) }

  scope :type, -> (value) { where("status" => value.to_i) if (!value.blank?) }

  #===========================================

  def self.top_users(percent, chain_id)
    users_count = count_top_users(chain_id)
    limit = users_count
    limit = ((percentage.to_f / 100.to_f) * users_count).to_f.ceil.to_i if users_count > 10 ## get the percentage from all users
    sql = "
           select u.id as u_id, u.email,
           u.register_type as u_type, timezone('EST', timezone('UTC', u.created_at))
           as signup_date, count(case when t1.status=3 then c.id end)
           as r_count, sum(coalesce(case when t1.status=3 then t1.tax else 0 end,0) +
           coalesce(case when t1.status=3 then t1.subtotal else 0 end,0)) as spent
           from users u left join receipts c on u.id=c.user_id left join
           receipt_transactions t1 on t1.receipt_id = c.id left join
           receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
           where  u.chain_id=#{chain_id} group by u.id, u.email, u.created_at order by spent limit #{limit}"
    res = ActiveRecord::Base.connection.execute(sql)
    user_ids = res.map { |x| x['u_id'] } rescue []
    users = User.where("id in(?)", user_ids)
    return users
  end

  def self.count_top_users(chain_id)
    sql = "select count(*) from (
                         select u.id as u_id, u.email,
                          u.register_type as u_type, timezone('EST', timezone('UTC', u.created_at))
                         as signup_date, count(case when t1.status=3 then c.id end)
                         as r_count, sum(coalesce(case when t1.status=3 then t1.tax else 0 end,0) +
                         coalesce(case when t1.status=3 then t1.subtotal else 0 end,0)) as spent
                         from users u left join receipts c on u.id=c.user_id left join
                         receipt_transactions t1 on t1.receipt_id = c.id left join
                         receipt_transactions t2 on t1.receipt_id=t2.receipt_id and t1.id < t2.id
                         where  u.chain_id=#{chain_id} group by u.id, u.email, u.created_at) as trans"
    res = ActiveRecord::Base.connection.execute(sql)
    return res.first["count"].to_i rescue 0
  end

  def execute_jobs
    Delayed::Job.enqueue(NoScanLib::NoScanLib.new(self),
                         :delayable_type => self.class.to_s,
                         :delayable_id => self.id,
                         :chain_id => self.chain_id,
                         :run_at => (Time.zone.now + 2.minutes)
    )
  end

  def update_total_users
    #count = select_users_count
    ### update total users on table
    #puts "updating total users ---"
    #self.update_column(:total_users, count)
  end

  def sum_users
    (self.total_users == 0 || self.total_users.blank?) ? self.select_users_count : self.total_users
  end

  def time_to_execute
    a = Time.zone.now
    sched = self.no_scan_job_scheduler
    case schedule_type
      when 1, nil
        a = "One Time :<br/>#{date_format(self.executed_at)} @ #{hour_format(self.executed_at)} #{self.zone}"
      when 2
        a = "Daily <br /> From #{date_format(sched.start_at)} to #{date_format(sched.end_at)} <br /> @ #{hour_format_without_zone(sched.executed_at)} #{self.zone}"
      when 3
        a = "Weekly <br /> From #{date_format(sched.start_at)} to #{date_format(sched.end_at)} <br /> every #{sched.next_run.strftime('%A')} @ #{hour_format_without_zone(sched.executed_at)} #{self.zone}"
      when 4
        b = sched.next_run.strftime('%A') rescue ""
        a = "Monthly <br /> From #{date_format(sched.start_at)} to #{date_format(sched.end_at)} <br />  <br />
             #{b} #{hour_format(sched.executed_at)} #{self.zone}"
    end
    return a
  end


  def time_to_execute_job
    testing = REDIS.get("testing_owner_job")
    if testing.blank? ## for testing purpose
      a = Time.zone.now #+ 1.minutes # to test it should be rollback again
      case schedule_type
        when 1, nil
          a = self.executed_at
        else
          exec_at = self.no_scan_job_scheduler.executed_at
          if exec_at < Time.zone.now
            a = Time.zone.now + 2.minutes
          else
            a = exec_at
          end
      end
    else

      if self.schedule_type == 1
        a = Time.zone.now
      elsif self.schedule_type == 2
        a= Time.zone.now + 1.minutes
      elsif self.schedule_type == 3
        a= Time.zone.now + 20.minutes
      elsif  self.schedule_type == 4
        a= Time.zone.now + 35.minutes
      else
        a = Time.zone.now
      end
    end
    return a
  end

  def mail_to_owner
    ## sending email to other chain owner
    #updating total_users
    #users = select_users
    ### update total users on table
    #self.update_column(:total_users, users.length)
    chain = Chain.find chain_id
    #chain_owners = chain.owners.where("owners.is_active = ? and owners.role_id = 1 or owners.role_id is null", true)
    #chain_owners.each do |co|
    #  OwnerJobMailer.owner_job_confirmation(co.email, self.owner_id, chain, self).deliver
    #end
    OwnerJobMailer.owner_job_confirmation(chain.email, self.owner_id, chain, self).deliver unless self.owner_id.blank?
  end

  def hour_format_without_zone(dt)
    begin
      "#{dt.strftime('%I:%M%p')}" unless dt.blank?
    rescue
      ""
    end
  end

  def setup_params
    case job_type
      when 1
        ## by scpesific percentage
        return {:percentage => percentage, :amount_max => amount_max, :amount_min => amount_min}
      when 2
        ## by users activity
        return {:activity_days => activity_days}
      when 3
        ## all users
        return {:chain_id => chain_id}
    end
  end

  #def select_users
  #  owner = Owner.find(owner_id)
  #  chain = owner.chain
  #  restaurant_ids = owner.chain.restaurants.map(&:id)
  #
  #  case job_type
  #    when 1
  #      #users = User.select('distinct on(users.id) users.*').joins(:restaurant_users).where(
  #      #    "date(last_active) <= date(?) and restaurant_id in(?) and users.active = 't'", Time.now - percentage.weeks, restaurant_ids
  #      #)
  #      users = User.find_by_sql("
  #         select * from (
  #          SELECT user_id, max(last_active) FROM restaurant_users
  #          inner join users on users.id = restaurant_users.id
  #          WHERE (restaurant_id in (#{restaurant_ids.join(',')}))
  #          and users.active = 't' group by user_id
  #         ) as s
  #
  #         inner join users on s.user_id = users.id
  #         where date(max) < date('#{Time.now.utc - percentage.weeks}')
  #      ")
  #    when 2
  #      #users = User.select('distinct on(users.id) users.*').joins(:restaurant_users).where(
  #      #    "date(last_active) > date(?) and restaurant_id in(?) and users.active = 't'", Time.now - activity_days.weeks, restaurant_ids
  #      #)
  #
  #      users = User.find_by_sql("
  #         select * from (
  #          SELECT user_id, max(last_active) FROM restaurant_users
  #          inner join users on users.id = restaurant_users.id
  #          WHERE (restaurant_id in (#{restaurant_ids.join(',')}))
  #          and users.active = 't' group by user_id
  #         ) as s
  #
  #         inner join users on s.user_id = users.id
  #         where date(max) > date('#{Time.now.utc - activity_days.weeks}')
  #      ")
  #    when 3
  #      users = User.where("chain_id = ? and users.active = 't'", chain_id)
  #  end
  #  return users
  #end

  def show_select_user
    # if self.job_type == nil #and self.job_type == 3
    #   #individual return 1
    #   #self.update_column(:job_type , 3)
    #   return 1
    # else
    #   x = OwnerJob::JobFromOwner.new(self)
    #   return x.count_for_show(self) rescue 0
    # end
    users = User.find_by_sql("SELECT users.id, (SELECT count(receipts.id) FROM receipts where receipts.user_id = users.id) as
        count_receipt FROM users WHERE  no_scan_date is NULL AND DATE(users.created_at AT TIME ZONE 'EST') <= DATE(now() - interval '#{self.percentage} days')
        AND users.chain_id = #{self.chain.id} and (SELECT count(receipts.id) FROM receipts where receipts.user_id = users.id) = 0 and users.deleted_at is null")

    return users.size
  end

  def select_users
    owner = Owner.find(owner_id)
    chain = owner.chain
    restaurant_ids = owner.chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    case job_type
      when 1
        #users = User.select('distinct on(users.id) users.*').joins(:restaurant_users).where(
        #    "date(last_active) <= date(?) and restaurant_id in(?) and users.active = 't'", Time.now - percentage.weeks, restaurant_ids
        #)
        users = User.find_by_sql("select users.* from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date <= date('#{Time.now - percentage.weeks}')
")
      when 2
        #users = User.select('distinct on(users.id) users.*').joins(:restaurant_users).where(
        #    "date(last_active) > date(?) and restaurant_id in(?) and users.active = 't'", Time.now - activity_days.weeks, restaurant_ids
        #)

        users = User.find_by_sql("select users.* from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date > date('#{Time.now - activity_days.weeks}')
")
      when 3
        users = User.where("chain_id = ? and users.active = 't'", chain_id)
    end
    return users
  end

  def user_miss_you
    ### selecting users except the one that get the reward in 200 days
    owner = Owner.find(owner_id) rescue nil
    chain = owner.chain rescue self.chain
    restaurant_ids = owner.chain.restaurants.map(&:id) rescue chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    case job_type
      when 1
        oj = OwnerJob::JobFromOwner.new(self)
        sql = oj.sql_for_miss_you_new(self)
        User.find_by_sql(sql)
#         users = User.find_by_sql("select distinct(users.*) from (
# select user_id , date(max(max)) from (
# select user_id, max from (
#  SELECT user_id, max(last_active) FROM restaurant_users
#  inner join users on users.id = restaurant_users.id
#  WHERE users.active = 't ' and (users.miss_you_date is null )
#  and
#  (restaurant_id in (#{restaurant_id_sql})) group by user_id
#  ) as s
# UNION
# select reward_transactions.user_id, max(reward_transactions.created_at) from
# reward_transactions
# left join restaurant_users on reward_transactions.user_id = restaurant_users.user_id
# where
# reward_transactions.restaurant_id in(#{restaurant_id_sql})
# group by reward_transactions.user_id
# ) as x group by user_id) as mm
# inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
# and (users.miss_you_date is null )
# and date <= date('#{Time.now - percentage.weeks}')
# ")
      when 2
        oj = OwnerJob::JobFromOwner.new(self)
        sql = oj.sql_for_miss_you_new(self)
        User.find_by_sql(sql)
#         users = User.find_by_sql("select distinct(users.*) from (
# select user_id , date(max(max)) from (
# select user_id, max from (
#  SELECT user_id, max(last_active) FROM restaurant_users
#  inner join users on users.id = restaurant_users.id
#  WHERE users.active = 't ' and (users.miss_you_date is null )
#   and
#  (restaurant_id in (#{restaurant_id_sql})) group by user_id
#  ) as s
# UNION
# select reward_transactions.user_id, max(reward_transactions.created_at) from reward_transactions
# left join restaurant_users on reward_transactions.user_id = restaurant_users.user_id
# where
# reward_transactions.restaurant_id in(#{restaurant_id_sql})
# group by reward_transactions.user_id
# ) as x group by user_id ) as mm
# inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
# and (users.miss_you_date is null )
# and date > date('#{Time.now - activity_days.weeks}')
# ")
      when 3
        users = User.where("chain_id = ? and users.active = 't'", chain_id)
    end
    return users
  end

  #def user_miss_you
  #  ### selecting users except the one that get the reward in 200 days
  #  owner = Owner.find(owner_id)
  #  chain = owner.chain
  #  restaurant_ids = owner.chain.restaurants.select('restaurants.id')
  #
  #  case job_type
  #    when 1
  #      users = User.select('distinct on(users.id) users.*').joins(:restaurant_users).where(
  #          "date(last_active) <= date(?) and restaurant_id in(?)
  #           and users.active = 't'
  #           and
  #           (miss_you_date is null or miss_you_date < date(?))",
  #          Time.now - percentage.weeks, restaurant_ids, Time.now - 200.days
  #      )
  #    when 2
  #      users = User.select('distinct on(users.id) users.*').joins(:restaurant_users).where(
  #          "date(last_active) > date(?) and restaurant_id in(?)
  #          and users.active = 't'
  #          and
  #         (miss_you_date is null or miss_you_date < date(?))",
  #          Time.now - activity_days.weeks, restaurant_ids
  #      )
  #    when 3
  #      users = User.where("chain_id = ? and users.active = 't'", chain_id)
  #  end
  #  return users
  #end

  def hour_format(dt)
    timezone ||= Setting.default_time_zone["zone"] || Time.zone.now.zone
    begin
      "#{dt.in_time_zone(timezone).strftime('%I:%M%p')} #{dt.in_time_zone(timezone).strftime('%Z')}" unless dt.blank?
    rescue
      ""
    end
  end

  def date_format(dt)
    timezone ||= Setting.default_time_zone["zone"] || Time.zone.now.zone
    begin
      dt.in_time_zone(timezone).strftime('%D') unless dt.blank?
    rescue
      ""
    end
  end

  #def select_users
  #  owner = Owner.find(owner_id)
  #  chain = owner.chain
  #  restaurant_ids = owner.chain.restaurants.map(&:id)
  #
  #  case job_type
  #    when 1
  #      users = OwnerJob.top_users(percentage, chain.id)
  #    when 2
  #      users = User.where("restaurant_id in (?) and receipts.status = 3 and date(receipts.created_at) >= date(?)",
  #                         restaurant_ids, Time.now - activity_days.days).
  #          joins(:receipts => :receipt_transactions).
  #          select('distinct(users.id)')
  #      if users.size >0
  #        users = User.where("id not in(?) and chain_id = ?", users, self.chain_id)
  #      else
  #        users = User.where("chain_id = ?", self.chain_id)
  #      end
  #    when 3
  #      users = User.where("chain_id = ?", chain_id)
  #  end
  #  return users
  #end


  def execute_job
    if miss_you?
      users = user_miss_you
    else
      users = select_users
    end
    ### update total users on table
    self.update_column(:total_users, users.length)
    success_user_ids = []
    failed_user_ids = []

    case reward_type
      when 1
        ## push reward
        users.each do |user|
          begin
            puts "success job push reward for #{user.id}"
            RewardWallet.create_with_delay({:reward_id => self.reward_id, :user_id => user.id, :description => "pushed from dashboard #{created_by_user}"}, true) ## do not send pn for this type reward
            if notification and push_email
              self.owner_job_push_email([user])
            end

            if notification and push_phone
              success_notif = self.push_notification([user], self.content_phone)
            end

            success_user_ids << {:id => user.id, :email => user.email,
                                 :date_sent => Time.zone.now.strftime('%D'),
                                 :success_notif => success_notif
            }
            update_miss_you(user) ## update miss you column
          rescue => e
            puts "OWNERJOB::EXECUTEJOB::PUSHREWARD => #{e.inspect}"
            failed_user_ids << user.id
            next
          end
        end
      when 2
        ## points
        unless users.blank?
          users.each do |user|
            begin
              user.earn(points, nil, "Owner")
              PointHistory.add_to_history(user.id, "Dashboard Push Point #{created_by_user}", points)
              if notification and push_email
                self.owner_job_push_email([user])
              end

              if notification and push_phone
                success_notif = self.push_notification([user], self.content_phone)
              end
              #puts "earch #{points} point to user #{user.email} from owner dashboard "
              #success_user_ids << user.id
              success_user_ids << {:id => user.id, :email => user.email,
                                   :date_sent => Time.zone.now.strftime('%D'),
                                   :success_notif => success_notif
              }
              update_miss_you(user) ## update miss you column
            rescue
              failed_user_ids << user.id
              next
            end
          end
        end
      when 3
        puts "------------------"
    end

    ## push notification
    #if notification
    #  if push_email
    #    result = owner_job_push_email(users)
    #    puts "result =================== #{result}"
    #    success_user_ids = result[0]
    #    puts "success user id #{success_user_ids}"
    #    failed_user_ids = result[1]
    #    puts "failed user id #{failed_user_ids}"
    #  end
    #
    #  if push_phone
    #    owner_job_push_phone(users)
    #  end
    #end
    ## logging
    history = OwnerJobHistory.where(:owner_job_id => self.id).first rescue []

    if history.blank?
      OwnerJobHistory.create(:owner_job_id => self.id,
                             :success => success_user_ids,
                             :failed => failed_user_ids
      )
    else
      data_success_before = history.success

      history.update_attributes(:success => data_success_before + success_user_ids,
                                :failed => failed_user_ids)
    end
    #self.update_attribute(:status, 3)
    puts "Done processing #{self.description} Jobs"
    puts "*" * 50
  end

  def update_miss_you(user)
    # if miss_you? ## update last miss you sent
    #   res_users = RestaurantUser.where("user_id = ?", user.id)
    #   res_users.each do |ru|
    #     #ru.update_column(:miss_you_date, Date.today)
    #   end
    #   #user.update_column(:miss_you_date, Date.today)
    # end

    ## update the no_scan_date
    user.update_column(:no_scan_date, Time.zone.now.in_time_zone('EST').to_date)
  end

  def created_by_user
    return "by #{self.owner.email}" rescue "by Admin Miss you"
  end

  def is_job_complete?
    if self.schedule_type != 1
      os = self.no_scan_job_scheduler
      begin
        return os.executed_at > os.end_at if self.schedule_type != 2
        return Time.zone.now > os.end_at.change(:hour => os.executed_at.hour, :min => os.executed_at.min) if self.schedule_type == 2
      rescue
        return false
      end
    end
  end

  def owner_job_push_email(users, reward_exp_at = nil)
    puts "Owner job push email starting ========="
    success_user_ids = []
    failed_user_ids = []
    ch = Chain.find(chain_id)
    tmp_point = self.points
    reward = Reward.find(self.reward_id) rescue nil
    reward_name = reward.name  rescue nil
    chain_name = ch.name rescue nil
    locales = ch.locales
    locales.each do |lc|
      # user_emails = User.
      #     where("users.id in (?) and locales.key = ? and chain_id in (?) and
      #   users.active is true and users.push_email is true", users, lc.key, chain_id).
      #     joins(:locale)
      user_emails = User.
          where("users.id in (?) and locales.key = ? and chain_id in (?)
        ", users, lc.key, chain_id).
          joins(:locale)
      puts "sending"

      mail_list = user_emails.map(&:email)
      user_list = user_emails.map(&:id)
      unless mail_list.blank?
        mail_list.each_with_index do |mail, index|
          begin
            selected_user = User.find(user_list[index])
            #is_pushed = selected_user.miss_you_date#RestaurantUser.where(:user_id => user.id).map(&:miss_you_date) rescue []
            #next if is_pushed.present? and self.miss_you? # only once for this miss you task

            fishbowl_send_email = false

                tmp_point = ""
                if selected_user.marketing_optin == true
                  fishbowl_send_email = ch.send_no_scan_after_7_days_notif(selected_user) if !ch.fishbowl_setting.no_scans_7_days_mailing_id.blank?  and  self.is_fishbowl == true
                end


            next unless self.push_email

            unless fishbowl_send_email
              email_subject = subject_email
              from = ch.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : ch.email
              emails_to = mail

              signature = REDIS.get "chain_#{chain.id}_signature"
              if signature
                text_body = content_email + "<br/><br/>#{signature}"
              else
                text_body = content_email + "<br/><br/>Thank you<br/><br/>#{chain.name}"
              end

              text_body.gsub!(/\r\n?/, "<br />")
              text_body.gsub!(/\n/, "<br />")
              mail = Mail.new do
                to emails_to
                from from
                subject email_subject % {
                            :reward_name => reward_name,
                            :chain_name => chain_name
                        }
              end

              text_part = Mail::Part.new do
                body text_body  % {
                         :reward_name => reward_name,
                         :chain_name => chain_name,
                         :points_number => tmp_point
                     }
              end
              mail.text_part = text_part
              html_part = Mail::Part.new do
                content_type 'text/html'
                body text_body  % {
                         :reward_name => reward_name,
                         :chain_name => chain_name,
                         :points_number => tmp_point
                     }
              end
              mail.html_part = html_part
              mail.deliver!

              #OwnerJobMailer.push_chain_plain_notification(subject_email, [mail], content_email, ch).deliver unless user_list.blank?
              #success_user_ids << user_list[index]
              success_user_ids << {
                  :id => user_list[index],
                  :email => mail_list[index],
                  :date_sent => Time.zone.now.strftime('%D')
              }

            end
          rescue => e
            puts "#{e.inspect}"
            failed_user_ids << user_list[index]
            next
          end
        end
        puts "#{lc.key} sending emails success from owner job"
      end
    end
    puts "Owner job push email End ========="
    return success_user_ids, failed_user_ids
  end

  #def owner_job_push_phone(user_tmp)
  #  puts "perform owner dashboard device job"
  #  locales = Chain.find(chain_id).locales
  #  locales.each do |lc|
  #    ## android Push notif
  #    #users = User.where("users.id IN (?) and users.active is true and users.push_device is true and locales.key = ? AND chain_id in (?) AND users.sign_in_device_type = 'android'",
  #    #                   users, lc.key, chain_id).joins(:locale)
  #
  #    users = User.where("users.id IN (?) and users.active is true and locales.key = ? AND chain_id in (?) AND users.sign_in_device_type = 'android'",
  #                       user_tmp, lc.key, chain_id).joins(:locale)
  #
  #    android_tokens = users.map(&:device_token).compact
  #    #push_for_android(android_tokens, self.content_phone) unless android_tokens.blank?
  #    ## split it
  #    puts "------ here -------"
  #    android_tokens.in_groups_of(Setting.delayed_job.push_notification_limit).each do |u_id|
  #      dl = Delayed::Job.enqueue(
  #          OwnerJobNotificationDeviceJob.new('android',
  #                                             u_id,
  #                                             self.content_phone,
  #                                             self),
  #          :run_at => Time.zone.now)
  #      dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id)
  #    end
  #
  #
  #
  #    ## iphone push notif
  #    #users = User.where("users.id IN (?) and users.active is true and users.push_device is true and locales.key = ? AND chain_id in (?) AND lower(users.sign_in_device_type) = 'iphone'", users, lc.key, chain_id).
  #    #    joins(:locale)
  #    users = User.where("users.id IN (?) and users.active is true  and locales.key = ?
  #                      AND chain_id in (?) AND lower(users.sign_in_device_type) =
  #                      'iphone'", user_tmp, lc.key, chain_id).
  #        joins(:locale)
  #
  #    iphone_tokens = users.map(&:device_token).compact
  #    #push_for_iphone(iphone_tokens, content_phone) unless iphone_tokens.blank?
  #
  #    iphone_tokens.in_groups_of(Setting.delayed_job.push_notification_limit).each do |u_id|
  #      dl = Delayed::Job.enqueue(
  #          OwnerJobNotificationDeviceJob.new('iphone',
  #                                            u_id,
  #                                            self.content_phone,
  #                                            self),
  #          :run_at => Time.zone.now)
  #      dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id)
  #    end
  #  end
  #
  #  puts "push notification success"
  #end

  def push_single_iphone(device_tokens, message)
    success = nil
    if application = Chain.find(self.chain_id).applications.by_device_type(2).first
      message = message.blank? ? "" : message
      device_tokens.each do |device_token|
        ## logging
        begin
          unless device_token.blank?
            rapns_app = application.rapns_app
            return if rapns_app.blank?
            n = Rpush::Apns::Notification.new
            n.app = rapns_app
            n.device_token = device_token
            n.alert = message

            n.sound = "1.aiff"
            n.save!

            user_id = User.find_by_device_token(device_token).id
            success = "success"
            # HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 1)
          else
            # HistoryNotification.create(:user_id => 0, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "user not found because blank device token")
          end

        rescue => e
          success = "failed"
        end
      end
    end
    return success
  end


  def push_single_android(reg_ids, notification)
    success= nil
    if application = Chain.find(self.chain_id).applications.by_device_type(1).first
      return if application.key.blank?
      message = GcmHelper::Message.new
      message.delay_while_idle = true
      message.add_data('alert', notification)
      message.add_data('timestamp', "#{Time.now}")
      sender = GcmHelper::Sender.new(application.key)
      response = sender.multicast_with_retry(message, reg_ids, 3)
      success = 'success'
      ## logging
      begin
        success_reg_ids = reg_ids #response.canonical_ids
        success_reg_ids.each do |sc|
          user_id = User.find_by_device_token(sc).id rescue nil
        end
      rescue => e
        puts ""
        success = 'failed'
      end
    end
    return success
  end

  def push_notification(user_tmp, content)
    puts "perform owner dashboard device job"
    locales = Chain.find(chain_id).locales
    success = nil
    locales.each do |lc|
      ## android Push notif
      users = User.where("users.id IN (?) and users.active is true and locales.key = ? AND chain_id in (?) AND users.sign_in_device_type = 'android'",
                         user_tmp, lc.key, chain_id).joins(:locale)

      android_tokens = users.map(&:device_token).compact
      #push_for_android(android_tokens, self.content_phone) unless android_tokens.blank?
      puts "------ here ------- #{android_tokens}"
      success = self.push_single_android(android_tokens, content) unless android_tokens.blank?


      ## iphone push notif
      users = User.where("users.id IN (?) and users.active is true  and locales.key = ?
                        AND chain_id in (?) AND lower(users.sign_in_device_type) =
                        'iphone'", user_tmp, lc.key, chain_id).
          joins(:locale)

      iphone_tokens = users.map(&:device_token).compact

      success = self.push_single_iphone(iphone_tokens, content) unless iphone_tokens.blank?

      puts "push notification success"

    end
    return success
  end

  def push_for_android(reg_ids, notification)
    if application = Chain.find(self.chain_id).applications.by_device_type(1).first
      return if application.key.blank?
      message = GcmHelper::Message.new
      message.delay_while_idle = true
      message.add_data('alert', notification)
      message.add_data('timestamp', "#{Time.now}")
      sender = GcmHelper::Sender.new(application.key)
      response = sender.multicast_with_retry(message, reg_ids, 3)

      ## logging
      begin
        success_reg_ids = reg_ids #response.canonical_ids
        success_reg_ids.each do |sc|
          user_id = User.find_by_device_token(sc).id rescue nil
        end
      rescue => e
        puts ""
      end
    end
  end

  #def push_for_iphone(device_tokens, message)
  #  if application = Chain.find(self.chain_id).applications.by_device_type(2).first
  #    message = message.blank? ? "" : message
  #    device_tokens.each do |device_token|
  #      rapns_app = application.rapns_app
  #      return if rapns_app.blank?
  #      n = Rpush::Apns::Notification.new
  #      n.app = rapns_app
  #      n.device_token = device_token
  #      n.alert = message
  #      n.sound = "1.aiff"
  #      n.save!
  #
  #      ## logging
  #      begin
  #        #user_id = User.find_by_device_token(device_token).id
  #      rescue => e
  #        puts "PLAINPUSHNOTIFICATION::History Plain logging #{e.inspect}"
  #        next
  #      end
  #    end
  #  end
  #end

  def push_for_iphone(device_tokens, message)
    if application = Chain.find(self.chain_id).applications.by_device_type(2).first
      message = message.blank? ? "" : message
      device_tokens.each do |device_token|
        ## logging
        begin
          unless device_token.blank?
            rapns_app = application.rapns_app
            return if rapns_app.blank?
            n = Rpush::Apns::Notification.new
            n.app = rapns_app
            n.device_token = device_token
            n.alert = message

            n.sound = "1.aiff"
            n.save!

            user_id = User.find_by_device_token(device_token).id
            # HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2, :status => 1)
          else
            # HistoryNotification.create(:user_id => 0, :plain_id => self.id, :plain_kind => 2, :status => 2, :error_text => "user not found because blank device token")
          end

        rescue => e
          puts "PLAINPUSHNOTIFICATION::History Plain logging #{e.inspect}"
          user_id = User.find_by_device_token(device_token)
          if user_id.blank?
            user_id = 0
            user_text = "User not found device token #{device_token}"
          else
            user_id = user_id.id
            user_text = e.inspect
          end
          # HistoryNotification.create(:user_id => user_id, :plain_id => self.id, :plain_kind => 2,
          #                           :status => 2, :error_text => user_text)
          next
        end
      end
    end
  end

  def step1_description
    case OwnerJob::TYPES[job_type].downcase
      when "percentage"
        return " I want to reach the top  #{percentage} (fill percentage) of my users who have
      spent between #{amount_min} and #{amount_max}"
      when "Activity"
        return "Blablabla #{activity_days}"
      else
        return "Total User #{Chain.find(chain_id).users.count} that will get."
    end
  end

  def step2_description
    case OwnerJob::REWARD_TYPES[reward_type].downcase
      when "reward"
        return " I want to reach the top  #{percentage} (fill percentage) of my users who have
      spent between #{amount_min} and #{amount_max}"
      when "points"
        return "Blablabla #{activity_days}"
      else
        return "Total User #{Chain.find(chain_id).users.count} that will get."
    end
  end

  def step3_description
    exec = executed_at.nil? ? Time.zone.now : executed_at
    "It will be executed on #{exec.strftime('%y-%m-%d : %H:%M')}"
  end

  def self.percentage_users(chain)
    restaurants = chain.restaurants.map(&:id)
    #RestaurantUser.where("restaurant_id in(?)", restaurants)
    res = RestaurantUser.select("COALESCE(SUM(total_transaction), 0) AS sum_total_transaction, user_id").
        where("restaurant_id in (?)", restaurants).group(:user_id).
        order('sum_total_transaction desc')
    user_count = User.where("id in(?)", res.map(&:user_id)).count
    return user_count
  end

  def success_users
    unless self.owner_job_histories.blank?
      num = self.owner_job_histories.last.success.uniq.size rescue 0
    else
      num = self.total_users
    end
    return num
  end


  ## miss you task section

  def miss_you?
    self.miss_you
  end

  def self.create_miss_you_task(chain, description, owner_id)
    td = chain.task_definition ## get the task definition

    if td.blank?
      return {:error => "The definition of miss you task could not be found, please set it on admin ."}
    else
      oj = OwnerJob.new(td.attributes.except('created_at', 'updated_at').merge!(:owner_id => owner_id, :miss_you => true, :description => description))
      oj.owner_scheduler = OwnerScheduler.new(td.task_scheduler.attributes.except('id')) rescue nil
      begin
        return oj.save!
      rescue ActiveRecord::RecordInvalid
        oj.executed_at = Time.zone.now + 5.minutes
        oj.save!
      end
    end

  end

  def self.admin_create_miss_you_task(chain, description, owner_id,definition_id)
    td = chain.task_definition ## get the task definition

    if td.blank?
      return {:error => "The definition of miss you task could not be found, please set it on admin ."}
    else
      oj = OwnerJob.new(td.attributes.except('created_at', 'updated_at').merge!(:owner_id => owner_id, :miss_you => true, :description => description, :task_definition_id => definition_id))
      oj.owner_scheduler = OwnerScheduler.new(td.task_scheduler.attributes.except('id')) rescue nil
      begin
        return oj.save!
      rescue ActiveRecord::RecordInvalid
        oj.executed_at = Time.zone.now + 5.minutes
        oj.save!
      end
    end

  end

  def self.update_miss_you_task(chain, description, owner_id, definition_id, owner_job_id)
    td = chain.task_definition ## get the task definition
    #dlete it first
    oj = OwnerJob.find owner_job_id rescue nil
    # oj.delete rescue nil
    if td.blank?
      return {:error => "The definition of miss you task could not be found, please set it on admin ."}
    else
      if oj.blank?
        oj = OwnerJob.new(td.attributes.except('created_at', 'updated_at').merge!(:owner_id => owner_id, :miss_you => true, :description => description, :task_definition_id => definition_id))
        oj.owner_scheduler = OwnerScheduler.new(td.task_scheduler.attributes.except('id'))

        return oj.save
      else

        oj.update_attributes(td.attributes.except('created_at', 'updated_at').merge!(:owner_id => owner_id, :miss_you => true, :description => description, :task_definition_id => definition_id))
        if oj.owner_scheduler.blank?
          # aaaa
          oj.owner_scheduler = OwnerScheduler.new(td.task_scheduler.attributes.except('id'))
          oj.save!
        else
          p oj.owner_scheduler
          oj.owner_scheduler.update_attributes(td.task_scheduler.attributes.except('id'))
          oj.save!
        end


        return oj
      end
    end

  end

  def self.update_no_scan_task(chain, description, owner_id, definition_id, owner_job_id)
    td = chain.no_scan_definition ## get the task definition
    #dlete it first
    puts "a" * 1000

    oj = NoScanJob.find owner_job_id rescue nil

    ## if there is any redundant delete it !
    NoScanJob.where("id != ? and chain_id = ? ", oj.id, oj.chain.id).delete_all rescue nil
    puts oj
    puts "b" * 1000
    #oj.delete
    if td.blank?
      return {:error => "The definition of miss you task could not be found, please set it on admin ."}
    else
      if oj.blank?
        oj = NoScanJob.new(td.attributes.except('created_at', 'updated_at').merge!(:owner_id => owner_id, :miss_you => false, :description => description, :task_definition_id => definition_id))
        oj.no_scan_job_scheduler = NoScanJobScheduler.new(td.task_scheduler.attributes.except('id'))
        oj.save
        return oj
      else

        oj.update_attributes(td.attributes.except('created_at', 'updated_at').merge!(:owner_id => owner_id, :miss_you => false, :description => description, :task_definition_id => definition_id))
        if oj.no_scan_job_scheduler.blank?
          # aaaa
          oj.no_scan_job_scheduler = NoScanJobScheduler.new(td.task_scheduler.attributes.except('id'))
          oj.save
          return oj
        else
         # p oj.owner_scheduler
          oj.no_scan_job_scheduler.update_attributes(td.task_scheduler.attributes.except('id'))
          oj.save
          return oj
        end


        return oj
      end
    end

  end

  def self.admin_create_no_scan_task(chain, description, owner_id,definition_id)
    td = chain.no_scan_definition ## get the task definition

    if td.blank?
      return {:error => "The definition of miss you task could not be found, please set it on admin ."}
    else
      oj = NoScanJob.new(td.attributes.except('created_at', 'updated_at').merge!(:owner_id => owner_id, :miss_you => true, :description => description, :task_definition_id => definition_id))
      oj.no_scan_job_scheduler = NoScanJobScheduler.new(td.task_scheduler.attributes.except('id')) rescue nil
      begin
        return oj.save
      rescue ActiveRecord::RecordInvalid
        oj.executed_at = Time.zone.now + 5.minutes
        oj.save
      end
    end
    oj
  end

  def select_users_count
    owner = Owner.find(owner_id) rescue nil
    chain = owner.chain rescue self.chain
    restaurant_ids = owner.chain.restaurants.map(&:id) rescue chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')

    users = User.find_by_sql("SELECT users.id, (SELECT count(receipts.id) FROM receipts where receipts.user_id = users.id) as
        count_receipt FROM users WHERE DATE(users.created_at) = DATE(now() - interval '8 days')
        AND users.chain_id = #{chain.id} and (SELECT count(receipts.id) FROM receipts where receipts.user_id = users.id) = 0 and users.deleted_at is null")

    return users
  end

  def select_user_miss_you_count
    owner = Owner.find(owner_id)
    chain = owner.chain
    restaurant_ids = owner.chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')
    res = 0

    case job_type
      when 1
        res = User.find_by_sql("select count(distinct(users.*)) from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and (users.miss_you_date is null)
 and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select reward_transactions.user_id, max(reward_transactions.created_at) from
reward_transactions
left join restaurant_users on reward_transactions.user_id = restaurant_users.user_id
where
reward_transactions.restaurant_id in(#{restaurant_id_sql})
group by reward_transactions.user_id
) as x group by user_id) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and (users.miss_you_date is null)
and date <= date('#{Time.now - self.percentage.weeks}')
").first.count.to_i
      when 2
        res = User.find_by_sql("select count(distinct(users.*)) from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and (users.miss_you_date is null)
  and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select reward_transactions.user_id, max(reward_transactions.created_at) from reward_transactions
left join restaurant_users on reward_transactions.user_id = restaurant_users.user_id
where
reward_transactions.restaurant_id in(#{restaurant_id_sql})
group by reward_transactions.user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and (users.miss_you_date is null)
and date > date('#{Time.now - self.activity_days.to_i.weeks}')
").first.count.to_i
      when 3
        res = User.where("chain_id = ? and users.active = 't'", chain_id).count
    end
    return res
  end

  def count_user_for_job
    if miss_you?
      select_user_miss_you_count
    else
      select_users_count
    end
  end
end
