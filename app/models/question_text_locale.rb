class QuestionTextLocale < ActiveRecord::Base

  belongs_to :locale
  belongs_to :question

  validates :text, :length => {:maximum => 250}

  def self.import_existing_data
    Chain.all.each do |chain|
      chain.surveys.each do |survey|
        survey.questions.each do |question|
          chain.locales.each do |locale|
            question_text = QuestionTextLocale.where(:question_id => question.id, :locale_id => locale.id).first
            if question_text.blank?
              question_text = QuestionTextLocale.create(:question_id => question.id, :locale_id => locale.id, :text => question.text)
            else
              question_text.update_attributes(:question_id => question.id, :locale_id => locale.id, :text => question.text)
            end
          end
        end
      end
    end
  end



end
