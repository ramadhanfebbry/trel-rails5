class SubmitSurvey < ActiveRecord::Base
  has_one :deal, :as => :activity
  has_one :deal_asset_android, :as => :activity
  has_one :deal_asset_iphone, :as => :activity

  accepts_nested_attributes_for :deal
  accepts_nested_attributes_for :deal_asset_android
  accepts_nested_attributes_for :deal_asset_iphone
  
  belongs_to :survey

  validates :survey_id, :presence => true
  validates :deal_asset_android, :presence => true
  validates :deal_asset_iphone, :presence => true
  #  validates_attachment_presence :image

  default_scope { where("submit_surveys.deleted_at" => nil) }
  def self.deleted
    self.unscoped.where('submit_surveys.deleted_at IS NOT NULL')
  end
end
