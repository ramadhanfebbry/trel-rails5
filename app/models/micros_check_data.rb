class MicrosCheckData < ActiveRecord::Base
  self.table_name = "micros_check_data"

  scope :unpulled_payment, where(:check_state => nil)
  scope :pulled_payment, where(:check_state => 1)
end