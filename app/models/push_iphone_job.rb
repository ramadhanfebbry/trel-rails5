class PushIphoneJob < Struct.new(:reward, :reg_ids_by_locale, :hash_notification,:exp_at)

  def perform
       
    tmp_expired = nil
    if exp_at.blank?
      tmp_expired = reward.expiryDate.strftime("%m/%d/%Y") rescue Time.now + 1.weeks
    else
      tmp_expired = exp_at#.strftime("%d %B %Y 23:59:59")
    end
    
    reward.push_to_iphone2(reg_ids_by_locale, hash_notification,tmp_expired)
    #    reward.push_to_android_and_email(reg_ids_by_locale, hash_notification)
    puts "Completed sending PN to Iphone"
  end

  def error(job, exception)
    reward = job.delayable_type.classify.constantize.find(job.delayable_id)
    error_text = job.last_error
    RewardMailer.send_pn_iphone_error(reward, error_text, exception).deliver
  end
  
end
