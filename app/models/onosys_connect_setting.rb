class OnosysConnectSetting < ActiveRecord::Base
  belongs_to :chain

  def self.get_onosys_connect_setting(chain_id)
    onosys_connect_setting = OnosysConnectSetting.find_by_chain_id(chain_id) rescue nil
    if onosys_connect_setting
      {
          "api_root" => onosys_connect_setting.api_root,
          "api_key" => onosys_connect_setting.api_key,
          "iphone_api_key" => onosys_connect_setting.iphone_api_key,
          "android_api_key" => onosys_connect_setting.android_api_key,
          "client_id" => onosys_connect_setting.client_id,
          "client_secret" => onosys_connect_setting.client_secret,
          "provider" => onosys_connect_setting.provider
      }
    else
      nil
    end
  end
end
