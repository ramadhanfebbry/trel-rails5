class TreatwareReceiptJob < Struct.new(:pos_check_upload, :user, :restaurant_id, :offer_id, :receipt_id)

  def perform
    treatware_parser = TreatwareCheckParser.new(pos_check_upload, user, restaurant_id, offer_id, receipt_id)
    treatware_parser.do_parse
  end

end