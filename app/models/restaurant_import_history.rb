class RestaurantImportHistory < ActiveRecord::Base

  serialize :success_restaurants
  serialize :failed_restaurants

  belongs_to :chain

end