class PromotionNotification < ActiveRecord::Base

  has_many :delayed_jobs, :as => :delayable, :dependent => :destroy
  has_many :history_notifications
  has_many :notification_locales, :as => :notifiable, :dependent => :destroy
  belongs_to :admin
  belongs_to :promotion

  serialize :email
  serialize :chains
  serialize :content

  scope :in_progress , -> { where("delayed_jobs.locked_at is not null and last_error is null").joins(:delayed_jobs) }
  scope :failed , -> { where("delayed_jobs.locked_at is not null and last_error is not null").joins(:delayed_jobs) }

  attr_accessor :run_at


  validates :content, :presence => true

  validates_each :content do |record, attr, value|
    problems = ''
    if value
      if value[:content]["content"].class == ActiveSupport::HashWithIndifferentAccess
        value[:content]["content"].each { |key, val|
          if val.blank? && key.include?('device')
            problems << "| #{key} Cannot blank" if record.is_push_phone == true
          elsif val.blank?
            problems << "| #{key} Cannot blank" if record.is_push_email == true
          end

        }
      else
        problems << "Cannot Be blank"
      end
    end
    record.errors.add(:content, problems) if problems != ''
  end

  def job_status
    if self.delayed_jobs.count == 0
      return "<a class='btn btn-small btn-success'>Completed</a>"
    elsif self.delayed_jobs.where("delayed_jobs.locked_at is not null and last_error is null and run_at IS NOT NULL")
      return "<a class='btn btn-small btn-warning'>Schedulled</a>"
    elsif self.delayed_jobs.where("delayed_jobs.locked_at is not null and last_error is null")
      return "<a class='btn btn-small btn-info'>Executing ...</a>"
    elsif self.delayed_jobs.where("delayed_jobs.locked_at is not null and last_error is not null")
      return "<a class='btn btn-small btn-danger'>Failed</a>"
    else
      return "N/A"
    end
  end

  def send_plain_email
    ### push_email
    if self.is_push_email
      dl = Delayed::Job.enqueue(PromotionNotificationJob.new(self.email[:emails], self.content[:content]["content"],
                                                             self.chain_id, self), :run_at => (self.run_at.blank? ? Time.zone.now : self.run_at))
      dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id)
    end
    ## push_notification device ( android and IPhone
    if self.is_push_phone
      unless self.email[:emails].blank?
        self.email[:emails].in_groups_of(Setting.delayed_job.push_notification_limit).each do |u_id|
          dl = Delayed::Job.enqueue(
              PushPromotionNotificationDeviceJob.new(self.id,
                                                 u_id,
                                                 self.content[:content]["content"],
                                                 self.chain_id),
              :run_at => (self.executed_at.blank? ? Time.zone.now : self.executed_at))
          dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id)
        end
      end
    end
    return true
  end


  def send_plain_email_for_user(user)
    ### push_email
    # Send email to the user who perform submit promocode
    if self.is_push_email
      dl = Delayed::Job.enqueue(PromotionNotificationJob.new([user.id], self.content[:content]["content"],
                                                             self.chain_id, self), :run_at => (self.run_at.blank? ? Time.zone.now : self.run_at))
      dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id)
    end
    ## push_notification device ( android and IPhone
    # Send phone notification to the user who perform submit promocode
    if self.is_push_phone

      dl = Delayed::Job.enqueue(
          PushPromotionNotificationDeviceJob.new(self.id,
                                                 [user.id],
                                                 self.content[:content]["content"],
                                                 self.chain_id),
          :run_at => (self.executed_at.blank? ? Time.zone.now : self.executed_at))
      dl.update_attributes(:delayable_type => self.class.to_s, :delayable_id => self.id)
    end
    return true
  end

  def push_plain_to_android(reg_ids, notification)
    chain = Chain.find(self.chain_id)
    if application = chain.applications.by_device_type(1).first
      return if application.key.blank?
      message = GcmHelper::Message.new
      message.delay_while_idle = true
      message.add_data('alert', notification)
      message.add_data('timestamp', "#{Time.now}")
      #      key="AIzaSyCHAQW9i1vGKNUD83jSsSn8Jfgb2TPFGKc"
      sender = GcmHelper::Sender.new(application.key)
      response = sender.multicast_with_retry(message, reg_ids, 3)

      ## logging

        success_reg_ids = reg_ids#response.success
      begin
        success_reg_ids.each do |sc|
          begin
            unless sc.blank?
              user = User.find_by_device_token(sc) rescue nil
              if user && user.chain.id == chain.id
                user_id = user.id rescue nil
                PromotionHistoryNotification.create(:user_id => user_id, :promotion_notification_id => self.id, :pn_kind => 2, :status => 1)
                puts "PROMOTIONNOTIFICATION SUCCESS LOG CREATED"
              elsif user
                PromotionHistoryNotification.create(:user_id => user.id, :promotion_notification_id => self.id, :pn_kind => 2, :status => 2, :error_text => "User not included on chain selected.")
              else
                PromotionHistoryNotification.create(:user_id => 0, :promotion_notification_id => self.id, :pn_kind => 2, :status => 2, :error_text => "User not found device token #{sc}")
              end
            else
              PromotionHistoryNotification.create(:user_id => 0, :promotion_notification_id => self.id, :pn_kind => 2, :status => 2, :error_text => "blank device token")
            end
          rescue => e
            user_id = User.find_by_device_token(sc).id rescue nil
            if user_id.blank?
              user_id = 0
              error_text = "User not found device token #{sc}"
            else
              error_text = e.inspect
            end
            puts "PROMOTIONNOTIFICATION::PUSH_PLAIN_TO_ANDROID LOGGING #{e.inspect}"
            PromotionHistoryNotification.create(:user_id => user_id, :promotion_notification_id => self.id, :pn_kind => 2,
                                       :status => 2, :error_text => error_text)
            next
          end
        end
      rescue => e
        puts "PROMOTIONNOTIFICATION Success Reg ids  #{e.inspect}"
      end
    end
  end

  def push_plain_to_iphone(device_tokens, message)
    chain = Chain.find(self.chain_id)
    if application = chain.applications.by_device_type(2).first
      message = message.blank? ? "" : message
      device_tokens.each do |device_token|
        ## logging
        begin
          unless device_token.blank?
            user = User.find_by_device_token(device_token) rescue nil
            user_id = user.id rescue nil
            if user && user.chain_id == chain.id
              rapns_app = application.rapns_app
              return if rapns_app.blank?
              n = Rpush::Apns::Notification.new
              n.app = rapns_app
              n.device_token = device_token
              n.alert = message
              n.sound = "1.aiff"
              n.save!
              PromotionHistoryNotification.create(:user_id => user_id, :promotion_notification_id => self.id, :pn_kind => 2, :status => 1)
            elsif user
              PromotionHistoryNotification.create(:user_id => user_id, :promotion_notification_id => self.id, :pn_kind => 2, :status => 2, :error_text => "User not included on chain selected.")
            else
              PromotionHistoryNotification.create(:user_id => user_id, :promotion_notification_id => self.id, :pn_kind => 2, :status => 2, :error_text => "User not found device token #{device_token}")
            end

          else
            PromotionHistoryNotification.create(:user_id => 0, :promotion_notification_id => self.id, :pn_kind => 2, :status => 2, :error_text => "blank device token")
          end
        rescue => e
          puts "PROMOTIONNOTIFICATION::History Plain logging #{e.inspect}"
          user_id = User.find_by_device_token(device_token)
          if user_id.blank?
            user_id = 0
            user_text = "User not found device token #{device_token}"
          else
            user_id = user_id.id
            user_text = e.inspect
          end
          PromotionHistoryNotification.create(:user_id => user_id, :promotion_notification_id => self.id, :pn_kind => 2,
                                     :status => 2, :error_text => user_text)
          next
        end
      end
    end
  end
end
