class PsnPushNotificationJob < ActiveRecord::Base
  MPDB_CONFIG = {
      :adapter  => "postgresql",
      :host     => ENV["JAVA_PN_DB_HOST"] || "159.203.84.199",
      :port     => ENV["JAVA_PN_DB_PORT"] || "5432",
      :username => ENV["JAVA_PN_DB_USER"] || "postgres",
      :password => ENV["JAVA_PN_DB_PWD"]  || "GCM!apns?",
      :database => ENV["JAVA_PN_DB_NAME"] || "pndb"
  }
  self.abstract_class = true
  establish_connection MPDB_CONFIG
  self.table_name = "push_notification_jobs"
end
