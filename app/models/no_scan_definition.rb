class NoScanDefinition < ActiveRecord::Base
  belongs_to :chain#, :dependent => :destroy
  has_one :task_scheduler,:class_name => 'NoScanScheduler',:foreign_key => 'job_id',  :dependent => :destroy
  has_many :no_scan_jobs, :foreign_key => 'task_definition_id'
  LAPSED_TYPE = {MISS_YOU: 0, NO_SCAN: 2}

  scope :no_scan_jobs, -> { where("owner_jobs.kind = ?",TaskDefinition::LAPSED_TYPE[:NO_SCAN]).joins(:owner_jobs) }


  def time_string
    a = Time.zone.now
    sched = self.task_scheduler
    case schedule_type
      when 1, nil
        a = "One Time :<br/>#{date_format(self.executed_at)} @ #{hour_format(self.executed_at)}"
      when 2
        a = "Daily <br /> From #{date_format(sched.start_at)} to #{date_format(sched.end_at)} <br /> @ #{hour_format_without_zone(sched.executed_at)}  #{self.zone}"
      when 3
        a = "Weekly <br /> From #{date_format(sched.start_at)} to #{date_format(sched.end_at)} <br /> every #{sched.next_run.strftime('%A')} @ #{hour_format_without_zone(sched.executed_at)} #{self.zone} "
      when 4
        b = sched.next_run.strftime('%A') rescue ""
        a = "Monthly <br /> From #{date_format(sched.start_at)} to #{date_format(sched.end_at)} <br />  <br />
             #{b} #{hour_format_without_zone(sched.executed_at)} #{self.zone}"
    end
  end

  def date_format(dt)
    begin
      dt.strftime('%D') unless dt.blank?
    rescue
      ""
    end
  end

  def hour_format(dt)
    begin
      "#{dt.strftime('%I:%M%p')} #{dt.strftime('%Z')}" unless dt.blank?
    rescue
      ""
    end
  end

  def hour_format_without_zone(dt)
    begin
      "#{dt.strftime('%I:%M%p')}" unless dt.blank?
    rescue
      ""
    end
  end


  def step2_info
    case reward_type
      when 1
        "Users will get #{Reward.find(reward_id).name}"
      when 2
        "Users will get #{self.points} point"
    end
  end

  def step1_info
    if activity_days.blank?
      return "Users Who <b>have not</b> visited in #{percentage} Weeks"
    else
      return "Users Who <b>have </b> visited in #{activity_days} Weeks"
    end
  end
end
