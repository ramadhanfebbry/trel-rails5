class MenuItemReportJobv2 < Struct.new(:reward_id, :user_email, :start_date, :end_date)

  def perform
    p "----------perform menu item report job------"
    # reward_id = 352
    # start_date = '2015-03-01'
    # end_date = '2015-07-31'
    # reward = Reward.find(reward_id)
    # user_email = 'inoe.bainur@gmail.com'
    reward = Reward.find(reward_id)
    p reward
    p user_email
    p start_date
    p end_date
    path = "#{Rails.root}/tmp/reward_item_user_support_report_#{reward.id}_#{Time.now}.csv"
    chain = reward.chain
    p "total loop #{loop}"
    pos_used = RewardTransaction.where(:reward_id => reward.id).last.pos_used rescue false
    if pos_used == true
      pos_query = " AND pos_used is true"
    end
    #File.new(path, 'w')
    transaction_csv = CSV.open(path, "wb") do |csv|
      # header row
      csv << ["User ID",
              "User Email",
              "Location",
              "#{reward.name} Redemption day",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit",
              "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit", "Visit"
      ]


      # total_pages = RewardTransaction.
      #     where("reward_id = ? AND date_trunc('day', (created_at::timestamp AT TIME ZONE '-05')) >= ?
      #            AND date_trunc('day', (created_at::timestamp AT TIME ZONE '-05')) <=  ?",
      #           reward.id, start_date, end_date).order("user_id desc").paginate(:page => 1, :per_page => 500).
      #     total_pages

      res_total = ActiveRecord::Base.connection.execute("select
 count(*) from ( select
 distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
reward_transactions.restaurant_id,
 reward_transactions.user_id) reward_transactions.* FROM reward_transactions WHERE reward_transactions.deleted_at
 IS NULL AND (reward_id = #{reward.id} AND
date_trunc('day', (created_at::timestamp AT TIME ZONE '-05')) >= '#{start_date}'
 AND date_trunc('day', (created_at::timestamp AT TIME ZONE '-05')) <= '#{end_date}' and redeeming is false #{pos_query} ) ORDER BY user_id
) a ;
")
      total_pages = (res_total.first["count"].to_f / 500.to_f).ceil
      total_pages = 1 if total_pages == 0

      puts total_pages
      puts "aaaaaaaaaaaaaaaaa"


 #     1.upto(total_pages) do |page|
#         reward_transactions = RewardTransaction.select("
#           distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
# reward_transactions.restaurant_id,
#  reward_transactions.user_id)  reward_transactions.*").
#             where("reward_id = ? AND date_trunc('day', (created_at::timestamp AT TIME ZONE '-05')) >= ?
#                  AND date_trunc('day', (created_at::timestamp AT TIME ZONE '-05')) <=  ? AND redeeming is false #{pos_query}",
#                   reward.id, start_date, end_date)

      reward_transactions = RewardTransaction.select("
          distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
reward_transactions.restaurant_id,
reward_transactions.user_id)  reward_transactions.*").
            where("reward_id = ? AND date(created_at - interval '5 hour') >= ?
                 AND date(created_at - interval '5 hour') <=  ? AND redeeming is false #{pos_query}",
                  reward.id, start_date, end_date)



        reward_transactions.each do |rt|
          csv_content = nil
          restaurant = rt.restaurant
          claim_date = rt.created_at.to_date
          receipts = Receipt.where(:user_id => rt.user_id, :status => Receipt::STATUS[:APPROVED])

          last_transactions = receipts.map { |r| r.last_transaction }


          last_transactions_gte_claim_date = last_transactions.select do |a|
              #a.issue_date ? a.issue_date.to_date >= claim_date : a.created_at.to_date >= claim_date
            if a.receipt_date.present?
              a.receipt_date >= claim_date
            elsif a.issue_date.present?
              a.issue_date.in_time_zone('EST').to_date >= claim_date
            else
              a.created_at.in_time_zone('EST').to_date >= claim_date
            end
          end
          another_receipts = []
          on_winter_claim_date = last_transactions_gte_claim_date.select do |a|
            #a.issue_date ? a.issue_date.to_date.eql?(claim_date) : a.created_at.to_date.eql?(claim_date)
            if a.receipt_date.present?
              a.receipt_date.eql?(claim_date)
            elsif a.issue_date.present?
              a.issue_date.in_time_zone('EST').to_date.eql?(claim_date)
            else
              a.created_at.in_time_zone('EST').to_date.eql?(claim_date)
            end
          end

          on_winter_claim_date = on_winter_claim_date.first

          if on_winter_claim_date
            another_receipts = last_transactions_gte_claim_date.delete_if { |a| a.id == on_winter_claim_date.id }
          else
            another_receipts = last_transactions_gte_claim_date
          end

          # data rows
          csv_content = [
              rt.user_id,
              (User.find(rt.user_id).email rescue "-"),
              (restaurant.name.gsub(",", " ") rescue "null"),
              (on_winter_claim_date.subtotal.to_s rescue "0")
          ]
          another_receipts.sort { |a, b| use_date(a) <=> use_date(b) }.each do |rt|
            csv_content << (rt.subtotal.to_s rescue "0")
          end
          csv << csv_content
        end
#      end
    end
    OwnerMailer.send_reward_redeemed(nil, user_email, path, "#{reward.name} and purchase report_#{start_date}_#{end_date}.csv", "Menu Item Report #{reward.name}").deliver!
    File.delete(path)
    puts "Success EXECUTING report"
  end

  def use_date(lt)
    return lt.receipt_date if lt.receipt_date.present?
    return lt.issue_date.in_time_zone('EST').to_date if lt.issue.present?
    return lt.created_at.in_time_zone('EST').to_date if lt.created_at.present?
  end

end
