class MessageIncentiveLocale < ActiveRecord::Base

  belongs_to :goal
  belongs_to :locale

  validates :message, :goal_id, :locale_id, :presence => true
  
end
