class BarcodeBatch < ActiveRecord::Base

  belongs_to :pos_location
  has_many :barcodes

  STATUS = {
      :NEW => 1,
      :USER_SUBMITTED => 2,
      :CHECK_SUBMITTED => 3,
      :PROCESSED => 4
  }


  def status_name
    BarcodeBatch::STATUS.invert[self.status]
  end
end