class PartnerMenuItem < ActiveRecord::Base

  belongs_to :chain
  has_many :restaurant_category_menu_items
  has_many :partner_menu_items_restaurants
  has_many :restaurants, :through => :partner_menu_items_restaurants

  validates :chain_id, :item_name, presence: true

  def pullout(restaurants)
    self.partner_menu_items_restaurants.where("restaurant_id in (?)", restaurants).delete_all
  end

  def participate(restaurants)
    p "partner Menu Item"
    restaurants.each do |restaurant_id|
      existing_deleted_restaurant_category = PartnerMenuItemsRestaurant.unscoped.where("partner_menu_item_id = ? AND restaurant_id = ? ", self.id, restaurant_id).first
      if existing_deleted_restaurant_category.blank?
        existing_restaurant_category = PartnerMenuItemsRestaurant.where("partner_menu_item_id = ? AND restaurant_id = ?", self.id, restaurant_id).first
        PartnerMenuItemsRestaurant.create(:partner_menu_item_id => self.id, :restaurant_id => restaurant_id) if existing_restaurant_category.blank?
      else
        existing_deleted_restaurant_category.revive
      end
    end
  end

end