class VextRewardClaim < ActiveRecord::Base
  belongs_to :user
  belongs_to :chain
  belongs_to :reward
end
