class PointHistory < ActiveRecord::Base

  belongs_to :user

  before_save :set_group_no

  GROUP = {
      'Referer Incentive'      => 1,
      'Referee Incentive'      => 2,
      'Game Point'             => 3,
      'social share incentive' => 4,
      'Milestone Incentive'    => 5,
      'Promotion Incentive'    => 6,
      'Signup'                 => 7,
      'FB sign up incentive'   => 8,
      'Dashboard'              => 9,
      'Admin Push'             => 10,
      'barcode scanned'        => 11,
      'Survey Incentive'       => 12,
      'Olo Receipt'            => 13,
      'Online Order'           => 14,
      'Revel Incentive'        => 18,
      'Migration point'        => 19,
      'Void'                   => 20
  }

  def self.add_to_history(user_id, description, point)
    PointHistory.create(:user_id => user_id, :description => description, :point => point)
  end

  def set_group_no
    keys = PointHistory::GROUP.keys
    key = keys.select{|a| self.description.downcase.include?(a.downcase)}.first
    if key.blank?
      self.group_no = 0
    else
      self.group_no = PointHistory::GROUP[key]
    end
  end
end
