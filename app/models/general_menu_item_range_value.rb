class GeneralMenuItemRangeValue < ActiveRecord::Base

  belongs_to :general_menu_item

  before_save :set_chain

  validates :start_value, :end_value, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }

  validate :start_must_be_greater_end_value


  private

  def start_must_be_greater_end_value
    return if start_value.blank? || end_value.blank?
    errors.add(:start_value, "must be greated than end value") unless
        start_value.to_i < end_value.to_i
  end

  def set_chain
    self.chain_id = self.general_menu_item.chain_id
  end

end
