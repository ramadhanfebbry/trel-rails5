class FocusPosRewardClaimedJob < Struct.new(:reward_transaction_id)

  def perform
    reward_transaction = RewardTransaction.find(reward_transaction_id)
    reward = reward_transaction.reward
    user = reward_transaction.user
    restaurant_lat_lng_id = reward_transaction.restaurant_id
    EmailMarketing.update_email_marketing_location(user, restaurant_lat_lng_id)
    Chain.update_user_fishbowl_location(user, restaurant_lat_lng_id)
    RestaurantUser.update_last_redeem(user)
    PointHistory.add_to_history(user.id, "Reward ##{reward.id} Claim", (reward.points * -1))
  end

end