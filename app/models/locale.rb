class Locale < ActiveRecord::Base
	
  has_many :users
  has_many :chain_locales
  has_many :chains, :through => :chain_locales
  has_one :term
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
  
end
