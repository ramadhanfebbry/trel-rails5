class SubscriptionFormat < ActiveRecord::Base
    belongs_to :report_subscription, :foreign_key => 'subscription_id'


  def filename(params)
    start_date = params["start_date"].blank? ? Date.today : params["start_date"].to_date
    end_date = params["end_date"].blank? ? Date.today : params["end_date"].to_date

    start_date = start_date.strftime(self.date_format)
    end_date = end_date.strftime(self.date_format)
    chain = self.report_subscription.chain rescue nil

    p start_date
    p "xx" * 100

    subject = (self.email_subject % {
        :chain => (chain.name rescue ""),
        :start_date => start_date,
        :end_date => end_date
    }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")


    filename_name = (self.file_name % {
        :chain => (chain.name rescue ""),
        :start_date => start_date,
        :end_date => end_date
    }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")
   # filename_name = self.file_name
    return [subject,filename_name]
  end
end