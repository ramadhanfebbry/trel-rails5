class EmailTemplateSweeper < ActionController::Caching::Sweeper

  observe EmailTemplate

  def after_save(record)
    expire_email_template_cache(record)
  end

  def after_destroy(record)
    expire_email_template_cache(record)
  end

  private

  def expire_email_template_cache(record)
    Rails.cache.delete("email_template_#{record.locale.id}_#{record.chain_id}_#{record.template_name}")
  end

end