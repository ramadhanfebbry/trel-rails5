class TreatwareReceiptJobV2 < Struct.new(:pos_check_upload_id, :user_id, :restaurant_id, :offer_id, :receipt_id)

  def perform
    treatware_parser = TreatwareCheckParserV2.new(pos_check_upload_id, user_id, restaurant_id, offer_id, receipt_id)
    treatware_parser.do_parse
  end

end