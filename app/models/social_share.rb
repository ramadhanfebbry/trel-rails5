class SocialShare < ActiveRecord::Base
  belongs_to :incentive, :polymorphic => true
  has_many :social_share_participants

  belongs_to :chain

  validates :daily_cap, :weekly_cap, :monthly_cap,
            :numericality => {:greater_than => -1, :message => "Must be a number greater than or equal 0"}
  scope :by_chain, lambda {|c| where("chain_id = ?", c.id)}
  scope :active ,  where("is_default = ?", true)
  MEDIUM = {
      1 =>   "Facebook",
      2 =>   "Twitter",
      3 =>   "Instagram"
  }


  def status
    if self.start_date <= Date.today and self.end_date >= Date.today
      return true
    else
      return false
    end
  end

  def is_active?
    self.start_date <= Date.today and self.end_date >= Date.today rescue false
  end


  def set_active_program
    SocialShare.where(:chain_id=> self.chain_id).update_all("is_default = false")
    self.update_attribute(:is_default, true)
  end
end
