class AverageChartTable < ActiveRecord::Base
  #run this AverageChartTable.migrate_average_chart_into_table
  def self.migrate_average_chart_into_table
    Chain.all.each do |ch|
      res_ids = ch.restaurants.map(&:id)
      res_ids.each do |res_id|
        insert_into_average_chart_tables(res_id)
      end
    end
  end

  def self.insert_into_average_chart_tables(restaurant_id)
    AverageChartTable.delete_all(:restaurant_id => restaurant_id)
    receipts = Receipt.by_restaurant_ids(restaurant_id).where("receipts.status = 3")
    res = []; result = []
    receipts.each do |rc|
      l_trans = rc.last_transaction
      count = l_trans.subtotal.to_f rescue 0
      count = count.nil? ? 0 : count

      begin
        res << {"id" => l_trans.id, "date" => rc.issue_date.nil? ? rc.created_at.to_s : rc.issue_date.to_s, "total" => count}
      rescue
        next
      end
    end

    date_list = res.map { |x| x["date"].to_date }.uniq.compact.sort
    date_list.each do |list|
      total = 0
      index = 0
      count_receipt = 0

      res.each_with_index do |r, m|
        if r["date"].to_date == list
          total = r["total"] + total
          index += 1
          count_receipt = count_receipt + 1
        end
      end
      tmp_avg = (total.to_f / index).round(2)
      #result << [list.to_datetime.to_i * 1000, tmp_avg] if list && tmp_avg > 0.0
      if list && tmp_avg > 0.0
        AverageChartTable.create(
            :receipt_date => list.to_date, :average => tmp_avg,
            :chain_id => Restaurant.find(restaurant_id).chain.id,
            :restaurant_id => restaurant_id, :receipt_count => count_receipt
        )

      end
    end
  end


  def self.update_chain_data(chain)
    Receipt.where("chain_id = ? and status = 3", chain.id).each do |rc|
      begin
        self.update_average(rc.last_transaction)
      rescue => e
        puts "UPDATE CHAIN DATA AVERAGE CHART TABLE ERROR #{e.inspect}"
        next
      end
    end
  end

  #AverageChartTable.update_average(r_trans)
  def self.update_average(r_trans)
    rc = r_trans.receipt
    res_id = r_trans.restaurant_id.nil? ? r_trans.restaurant_offer.restaurant.id : r_trans.restaurant_id
    query_date = r_trans.receipt_date.nil? ? rc.issue_date.to_date : r_trans.receipt_date.to_date

    data = AverageChartTable.where("restaurant_id = ? and receipt_date = ?", res_id, query_date).first

    if data.blank?
      chain_id = r_trans.restaurant.chain.id rescue nil
      date_tmp = r_trans.receipt_date.nil? ? rc.issue_date : r_trans.receipt_date
      if date_tmp.year != Time.now.year
        date_tmp = date_tmp.change(:year => rc.created_at.year)
      end

      avg_tbl = AverageChartTable.create(
          :receipt_date => date_tmp.to_date,
          :average => r_trans.subtotal,
          :chain_id => chain_id,
          :restaurant_id => res_id, :receipt_count => 1
      )
      avg_tbl.update_column(:created_at, rc.get_date)
    else
      #case_issue_date = "(
      #  case when receipt_transactions.issue_date is null
      #    then receipts.created_at
      #    else receipt_transactions.issue_date
      #    end)"
      case_issue_date = "(
  case when exists(select receipt_transactions.receipt_date)
  then receipt_transactions.receipt_date
    else receipt_transactions.receipt_date
  end
  )"
      date_zone = "date(TIMEZONE('UTC',#{case_issue_date})
                  AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') "
      receipts = Receipt.by_restaurant_ids(res_id).where("receipts.status = 3 and #{date_zone} = ?",
                                                         query_date)
      subtotal = 0
      count_index = 0
      receipts.each_with_index do |rc, index|
        subtotal += rc.last_transaction.subtotal rescue 0
        count_index = index + 1
      end
      puts "--------------------AVERAGE CHART TABLES --------------------"
      puts receipts.size
      puts subtotal
      puts count_index
      puts "--------------------END AVERAGE CHART TABLES --------------------"

      avg = (subtotal.to_f / count_index).to_f
      avg = 0 unless avg.finite?
      data.update_attributes(:receipt_count => count_index, :average => avg.round(2))
      data.update_column(:created_at, rc.get_date)
    end
  end
end