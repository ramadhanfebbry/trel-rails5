class PartnerCodeSetting < ActiveRecord::Base

  TYPES = {"STATIC" => 1, "DYNAMIC" => 2}

  belongs_to :chain

  validates :pos_digit_barcode, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true,
      :less_than_or_equal_to => 100
  }
  validates :timer, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }
  validates :zero_padding, :numericality => {
      :greater_than_or_equal_to => 0,
      :only_integer => true
  }
  validates :prefix, :length => {:maximum => 100}, allow_blank: true
  validates :postfix, :length => {:maximum => 100}, allow_blank: true
  validates :barcode_format_type, :barcode_format, :presence => true

  DEFAULT_SETTING_REWARDCODE = {
      "appcode_type" => PartnerCodeSetting::TYPES["DYNAMIC"],
      "barcode_format" => Chain::BARCODE_FORMAT["Numeric"],
      "pos_digit_barcode" => 3,
      "barcode_format_type" => Chain::BARCODE_FORMAT_TYPES["Code 39"],
      "zero_padding" => 0,
      "timer" => 2
  }

end