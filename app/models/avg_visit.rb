class AvgVisit < ActiveRecord::Base
  # To change this template use File | Settings | File Templates.
  #AvgVisit.calculate_avg_from_day_visit
  def self.calculate_avg_from_day_visit
    AvgVisit.delete_all
    user_ids = User.where("active = ?",true).map(&:id)
    user_ids.each do |u_id|
      day_visits = DayVisit.where("user_id = #{u_id}").order("visit_at,restaurant_id asc")
      unless day_visits.blank?
        day_visits.group_by(&:restaurant_id).each do |dv|
          dates = dv[1].map(&:visit_at) ## collect date
          puts dates
          date_size = dates.size
          if date_size > 1
            arr = dates.uniq.compact.each_cons(2).map { |a, b| ((b-a)/1.day).to_f * 100000 } rescue [0] ## calculate diff between days
          else
            arr = [0]
          end
          dividen = (date_size - 1) if date_size > 1
          dividen = 1 if dividen == 0 || date_size <= 1
          sum = (arr.sum.to_f / dividen.to_f) ## sum the diff
          puts "average ==== #{sum}"
          AvgVisit.create(:restaurant_id => dv[0], :user_id => dv[1].first.user_id, :average => sum.round(2))
        end
      end
    end
  end

  def self.update_avg_visit(day_visits,id_user, id_restaurant)
    unless day_visits.blank?
      dates = day_visits.map(&:visit_at).compact.sort ## collect date
      arr = dates.uniq.compact.each_cons(2).map { |a, b| ((b-a)/1.day).to_f * 100000 } rescue [0]## calculate diff between days
      dividen = (dates.size - 1)
      dividen = 1 if dividen == 0
      p "update avg visit -------*****-----"
      p arr
      p dividen
      p arr.sum.to_f
      sum = (arr.sum.to_f / dividen.to_f) ## sum the diff
      avg_list = AvgVisit.where("restaurant_id = ? and user_id = ?", id_user, id_restaurant).first
      p avg_list
      p "--------------"
      if avg_list.blank?
        AvgVisit.create(:restaurant_id => id_restaurant, :user_id => id_user, :average => sum.round(2))
      else## update the current average
        avg_list.update_attributes(:avg =>  sum.round(2))
      end
      #AvgVisit.create(:restaurant_id => day_visits.first.restaurant_id, :user_id => u_id, :average => sum.round(2))
    end
  end

  ## AvgVisit.fix_update_bugs_2020
  def self.fix_update_bugs_2020
    DayVisit.where("EXTRACT(YEAR FROM visit_at) != 2013 and EXTRACT(YEAR FROM visit_at) != 2012").each do |dv|
      begin
        year = dv.visit_at
        year = year.change(:year => dv.created_at.year)
        dv.update_attribute(:visit_at, year)
      rescue => e
        puts "cause there are duplicates, we should delete the row on it."
        dv.delete ## this is because the 2020 year, it has to be uniq receipt for one user, each day
      end
    end

    calculate_avg_from_day_visit
  end
end