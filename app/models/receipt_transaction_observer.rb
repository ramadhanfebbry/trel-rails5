class ReceiptTransactionObserver < ActiveRecord::Observer

  observe ReceiptTransaction

  def after_save(record)
    return if record.updated_discount_from_pos
    a = Time.now
    puts "Time Start @ Receipt Transaction #{a}"
    receipt = record.receipt
    ## stored on the receipt_statuses table for logging
    ReceiptStatus.create(
        :receipt_transaction_id => record.id,
        :status => record.status,
        :delayed_status => ReceiptStatus::DJ_STATUS[:CREATED]
    )
    ### this method will execute from delayed Job

    ## move to dj
    #if Setting.email.turned_on and record.status.eql?(4) and !record.reject_reason_id.blank?
    #  user, receipt, reject_reason = setup_params_for_reject_receipt(record)
    #
    #  ## get the receipt transactions, handle if the duplicate receipts (approved first) and then rejected after
    #  # that, dont send an email reject, because the receipt is already approved
    #  #list_transactions = record.receipt.receipt_transactions
    #
    #  list_transactions = ReceiptTransaction.where(
    #      "receipt_id != ? AND DATE(issue_date) = ? AND
    #      restaurant_id = ? AND receipt_number = ? AND status = ?",
    #      receipt.id, record.issue_date.to_date,
    #      record.restaurant_id, record.receipt_number, Receipt::STATUS[:APPROVED]
    #  ) rescue []
    #
    #  send_reject_email = true
    #  from_same_user = true
    #  ## if this receipt is already been approved, and then user send the receipt again
    #  ## this will rejected ( duplicate ) and we should not send the user an email for this reject
    #  ## unless the receipt is come from another user
    #  unless list_transactions.blank? ## there are record already approved,
    #    send_reject_email = false
    #    ## if the transactions is came from different user, then we should send them email
    #    from_same_user = false if !list_transactions.map(&:receipt).map(&:user_id).include?(receipt.user_id)
    #  end
    #  ####
    #
    #  if Setting.email.delayed and user.push_to_email?
    #    user.reject_receipt_email(receipt, reject_reason, true) if send_reject_email or !from_same_user
    #  elsif user.push_to_email?
    #    user.reject_receipt_email(receipt, reject_reason, false) if send_reject_email or !from_same_user
    #  end
    #end

    #substrach point due to receipt updated
    puts '---- subtract point due to receipt changed from approved'
    receipt.substact_point_due_to_receipt_changed_from_approved
    puts '------ end substract'
    receipt = record.receipt.reload

    if record.status !=  Receipt::STATUS[:RECEIVED] and record.status != Receipt::STATUS[:PENDING_OCR]
      if record.status ==  Receipt::STATUS[:APPROVED]
        details = {
            :chain_id => receipt.chain_id,
            :receipt_id => record.receipt_id,
            :restaurant_id => record.restaurant_id,
            :subtotal => record.subtotal,
            :issue_date => (record.issue_date || record.created_at),
            :time_stamp => record.time_stamp,
            :receipt_number => record.receipt_number,
            :user_id => receipt.user_id

        }
        dt = ReceiptApprovedDetail.select("id").where("receipt_id = ?", receipt.id).first
        if dt.blank?
          dt = ReceiptApprovedDetail.new(details)
          dt.save
        else
          dt.update_attributes(details)
        end
      end
      # ReceiptApprovedProcessWorker.perform_in(15.seconds, record)
      # ReceiptApprovedProcessWorker.sidekiq_delay.new(record)
      # ReceiptApprovedProcessWorker.perform_at(10.seconds.from_now, record.id)
      Delayed::Job.enqueue(ReceiptApprovedProcess.new(record), :run_at => Time.now + 15.seconds, :queue => 'receipt_updated_job')
    end
    ## move to dj
    #if record.status.eql?(Receipt::STATUS[:OCRED])
    #  Receipt.store_ocred_receipt_xml_data_to_s3(receipt, record.xml_data) unless record.xml_data.blank?
    #end


  end

  def setup_params_for_reject_receipt(receipt_transaction)
    reject_reason = receipt_transaction.reject_reason
    user = receipt_transaction.receipt.user
    locale = user.locale
    reject_reason_content = get_reject_reason_by_locale(reject_reason.reason_key, locale.key)
    return user, receipt_transaction.receipt, reject_reason_content
  end

  # get_reject_reason_by_locale("invalid_receipt", "en")
  def get_reject_reason_by_locale(key, locale = "en")
    REDIS.get "#{locale}.receipt_reject_reason.#{key}" unless key.blank?
  end

  def error(job, exception)

  end

end
