class AnswerText < ActiveRecord::Base
  belongs_to :answer
  default_scope { where("answer_texts.deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('answer_texts.deleted_at IS NOT NULL')
  end
  
end
