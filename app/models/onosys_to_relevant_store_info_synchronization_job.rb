class OnosysToRelevantStoreInfoSynchronizationJob < Struct.new(:setting)

  def perform
    p "perform OnosysToRelevantStoreInfoSynchronization"
    begin
      chain = setting.chain
      onosys_setting = setting
      res_success = []
      res_fail = []
      rih = RestaurantImportHistory.create(chain_id: chain.id, status: "processing", log_type: "#{chain.try(:name)} ONOSYS STORE Synchronization")
      restaurans = chain.restaurants.where("external_partner_id is not null")
      restaurans.each do |restaurant|
        p restaurant.external_partner_id

        url = onosys_setting.api_root #"https://pennstation.staging.onosys.com"
        is_ssl = url.include?("https")
        p uri = URI.parse("#{url}/restaurant/#{restaurant.external_partner_id}")
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = is_ssl
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Get.new(uri.request_uri)
        request["Content-Type"] = "application/json; charset=utf-8"
        request["apikey"] = onosys_setting.api_key #"1JJgAqDV9rijirR4iJRKg4v+60YndZG+C42xYIHxHHs="
        response = http.request(request)
        if response.is_a?(Net::HTTPOK) || response.code.to_s == "204"
          p "SUKSES ONOSYS Update RESTAURANTS - #{response.body}"
          response_json = JSON.load(response.body)
          unless response_json.blank?
            restaurant.update_column("external_partner_id", response_json["id"])
            restaurant.restaurant_detail.update_column("external_location_store_id", response_json["store_number"])
            restaurant.restaurant_hours.each do |hour|
              day = Date::DAYNAMES[hour.day_of_week]
              time = response_json["business_hours"].find {|x| x["start_day"] == day}
              hour.update_column("open_at", Time.parse("#{time["start_time"]}").strftime("%I:%M %p"))
              hour.update_column("close_at", Time.parse("#{time["end_time"]}").strftime("%I:%M %p"))
            end
            res_success << restaurant.name
          else
            res_fail << restaurant.name
          end
        else
          res_fail << restaurant.name
        end
        rih.update_attributes(:status => "completed", :success_restaurants => res_success, :failed_restaurants => res_fail )

      end
    rescue => e
      p "ERROR Update restaurant ONOSYS --- #{e.message}"
      rih.update_attributes(:status => "failed", :success_restaurants => res_success, :failed_restaurants => res_fail )
    end
  end

end