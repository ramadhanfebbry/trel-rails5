class RestaurantUser < ActiveRecord::Base

  belongs_to :restaurant
  belongs_to :user

  validates :user_id, :uniqueness => {:scope => :restaurant_id}

  scope :restaurant_members, -> {|restaurant|
    select("user_id, restaurant_id").
        where("restaurant_id = ? and users.active = ?", restaurant.id,true).joins(:user)
  }

  def self.distinct_users(owner,chains)
    if owner.chain_owner?
      self.find_by_sql("select count(*) from
        (select distinct user_id from restaurant_users r
         join users u on r.user_id=u.id and u.chain_id in (#{chains.map(&:id).join(',')}) and u.active=true)
        as u").first.count.to_i
    else
      Restaurant.count_total_member(owner.restaurant_list)
    end
  end

  def self.update_member(receipt_trans)
    begin
    puts "EXECUTE UPDATE MEMBER"
    receipt = receipt_trans.receipt
    issue_date = format_receipt_date(receipt_trans)
    user = receipt.user
    restaurant_id = receipt_trans.restaurant_id#.blank? ? receipt_trans.restaurant_offer.restaurant_id : receipt_trans.restaurant_id
    if user.receipts  and user.receipts.count == 1 and receipt_trans.restaurant_id ## first time submittion a receipt
      puts "IFFF "
      record = RestaurantUser.where("user_id = ? and restaurant_id = ?", user.id, restaurant_id)
      if record.blank?
        puts "IF UPDATE MEMBER"
        puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        res_user = RestaurantUser.new(:user_id => user.id, :restaurant_id => restaurant_id, :joined_date => receipt.created_at.to_date)
        res_user.save!
        res_user.update_column(:created_at, issue_date)
        res_user.update_column(:last_active, Date.today)
      else
        #puts "ELSE  1 #{record.first.id}"
        puts "ELSE UPDATE MEMBER"
        puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        record.first.update_column(:last_active, Date.today)
      end
    else
      puts "RESTAURANT USER LAST ACTIVE DATE UPDATINGGGG...."
      puts "record = #{record}"
      ## update last active column
      record = RestaurantUser.where("user_id = ? and restaurant_id = ?", user.id, restaurant_id).first
      record.update_column(:last_active, Date.today) unless record.blank?
      puts "RESTAURANT USER LAST ACTIVE DATE UPDATINGGGG.... Done"
    end
    rescue => e
      puts "RestaurantUser update_member #{e.inspect}"
    end
  end

  #RestaurantUser.populate_joined_data_column
  def self.populate_joined_data_column
    ReceiptTransaction.joins(:receipt).select("receipts.user_id, receipt_transactions.restaurant_id,receipts.created_at, receipt_transactions.restaurant_offer_id").each do |receipt_transaction|
      res_id =  receipt_transaction.restaurant_id#.blank? ? (receipt_transaction.restaurant_offer.blank?? nil :receipt_transaction.restaurant_offer.restaurant_id ) : receipt_transaction.restaurant_id

      unless res_id.blank?
        data = RestaurantUser.where(
            :user_id => receipt_transaction.user_id,
            :restaurant_id => res_id
        ).first

        data.update_attribute(:joined_date,receipt_transaction.created_at.to_date) unless data.blank?
      end
    end
  end

  ##  fix member joined date column
  def self.update_the_created_at
     RestaurantUser.select("user_id").where("date(created_at) < '2013-03-01'").each do |ru|
       u = User.find(ru.user_id).receipts.first
       ru.update_column(:created_at,u.created_at.to_date)
       ru.update_column(:joined_date,u.created_at.to_date)
     end
  end

  def self.update_created_at_using_issue_date
     RestaurantUser.select("id,user_id").each do |ru|
       begin
       rc = ru.user.receipts.order("created_at desc").first
       date = rc.get_date
       ru.update_column(:created_at,date)
       ru.update_column(:joined_date,date.to_date)
       rescue
         next
         end
     end
  end

  ## RestaurantUser.delay_update_created_at_using_issue_date
  def self.delay_update_created_at_using_issue_date
     self.delay.update_created_at_using_issue_date
  end

  def reload_current_data  #reload data which havent pull out correctly
    receipt = User.find(self.user_id).receipts.order("created_at desc").first

    unless receipt.blank?
      receipt_trans = receipt.last_transaction
      restaurant_id = receipt_trans.restaurant_id.blank? ? receipt_trans.restaurant_offer.restaurant_id : receipt_trans.restaurant_id

      self.update_attributes(:restaurant_id => restaurant_id, :joined_date => receipt.created_at)
    end
  end

  def self.format_receipt_date(r_trans)
    ## issue date / submission_date
    if r_trans.issue_date.blank?
      return r_trans.created_at
    else
      ## if there are time stamp
      return r_trans.issue_date if r_trans.time_stamp.blank?
      if r_trans.time_stamp
        time_stamp = r_trans.time_stamp
        split_time = time_stamp.split(':')
        hour = split_time[0]
       # zn = split_time[1][2..3]
        zn = if time_stamp.downcase.include?('am')
               "am"
             else
               "pm"
             end
        min = split_time[1][0..1]
        hour = hour.to_i + 12 if zn and  zn.downcase == "pm"   && hour.to_i < 12
        hour = 0 if zn and  zn.downcase == "am"   && hour.to_i == 12
        return r_trans.created_at if hour.to_i > 24
        begin
          result = r_trans.issue_date.to_datetime.change(:hour => hour.to_i, :min => min.to_i)
        rescue
          result =  r_trans.issue_date
        end
        return result
      end
    end
  end

  def self.update_last_redeem(user)
    RestaurantUser.update_all("last_redeem = date('#{Date.now}')", "user_id = #{user.id}") rescue nil
  end

  # RestaurantUser.insert_last_redeem
  def self.insert_last_redeem
    total_pages = User.paginate(:page => 1, :per_page => 1000).total_pages
    1.upto(total_pages) do |i|
      users = User.paginate(:page => i, :per_page => 1000)

      users.each do |u|
        puts u.id
        begin
          rt = RewardTransaction.where("user_id = ?", u.id).order("created_at desc").first.created_at
          RestaurantUser.update_all("last_redeem = date('#{rt}')", "user_id = #{u.id}")
        rescue => e
          puts "#{u.id} cannot find #{e.inspect}"
          next
        end
      end
    end
  end

  # RestaurantUser.update_survey_and_member_for_each restaurant
  # for dashboard purpose, stored in redis in every 1 hour
  def self.update_survey_and_member_for_each
    Chain.active.all.each do |ch|
      restaurants = ch.restaurants.active
      surveys = ch.surveys
      surveys.each do |sr|
        # updating for all restaurant in each chain

        #chain_total = SurveysUser.select("count(surveys_users.id)").
        #         where("survey_id = ? and restaurant_id in(?)",sr.id, restaurants.map(&:id)).first.count.to_i
        chain_total = SurveysUser.survey_count_chain(sr.id,restaurants.map(&:id))
        REDIS.set "survey_total_chain_#{sr.id}_#{restaurants.map(&:id)}", chain_total

        puts "updating survey count for each restaurant"
        restaurants.each do |rs|
          #total = SurveysUser.
          #    select("count(surveys_users.id)").
          #    where("survey_id = ? and restaurant_id = ?", sr.id, rs.id).first.count.to_i
          total = RestaurantSurveyCount.where("survey_id = ? and restaurant_id = ?", sr.id,rs.id).first.total rescue 0
          puts total
          puts "survey_total_#{sr.id}_#{rs.id}"
          REDIS.set "survey_total_#{sr.id}_#{rs.id}", total
          puts "OK"
        end
      end

     ## updating sort for restaurant_id based on greetings controller line 129

      restaurant_ids = RestaurantUser.select("count(restaurant_users.user_id),restaurant_id").
          where("restaurant_id in(?) and users.active = ?",restaurants,true).group("restaurant_id").
          joins(:user).order("count desc")
      restaurant_ids = restaurant_ids.map(&:restaurant_id)
      restaurant_ids = (restaurant_ids).uniq

      REDIS.set "chain_#{ch.id}_restaurant_desc", restaurant_ids.to_s
    end
  end

  # dahsboard
  def self.update_member_chart_area
    Chain.active.all.each do |ch|
      restaurants = ch.restaurants.active

      total = ch.users.where("active = ?", true).count
      REDIS.set "count_member_chain_#{ch.id}", total

      # today users for each chain
      dt_zone = "date(TIMEZONE('UTC', users.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
      total_chain_signup_today = User.where("#{dt_zone} = date(?) and chain_id = #{ch.id}", Date.today).count
      REDIS.set "total_signup_chain_#{ch.id}", total_chain_signup_today

      # today users signup for each restaurant
      restaurants.each do |rs|
        dt_zone_created_at = "date(TIMEZONE('UTC', created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}')"
        total_signup = RestaurantUser.where("restaurant_id = ? and (#{dt_zone_created_at} = ?)", rs.id,Date.today).count
        REDIS.set "total_signup_restaurant_#{rs.id}", total_signup

        total_members = User.find_by_sql("
            SELECT COUNT(*) FROM restaurant_users
            inner join users on users.id = restaurant_users.user_id
            where users.active = 't' and users.chain_id = #{ch.id} and
              restaurant_id = #{rs.id}
            and ( last_active >= date('#{Time.now - 120.days}') or last_redeem >= date('#{Time.now - 120.days}') )
            ").first.count.to_i

        REDIS.set "total_new_count_active_chain_#{ch.id}_#{rs.id}", total_members
      end

    end
  end
end
