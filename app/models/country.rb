class Country < ActiveRecord::Base
  has_many :regions
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
  
  scope :active_countries, -> { where(:active => true) }

  validates :abbreviation,:ISO, :length => {:maximum => 4}
  validates :name,:abbreviation, :presence => true
  validates_uniqueness_of :name, :abbreviation, :ISO

  before_create :upcase_words
  before_validation :upcase_words
  
  def label
    "%s" % [self.abbreviation]
  end

  def upcase_words
    self.name = self.name.titleize
    self.abbreviation = self.abbreviation.upcase rescue "N/A"
    self.ISO = self.ISO.upcase rescue "N/A"
  end
end
