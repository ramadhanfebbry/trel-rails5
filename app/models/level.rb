class Level < ActiveRecord::Base

  belongs_to :game

  validates :game_id, :presence => true
  validates :number, :uniqueness => {:scope => :game_id} ,:numericality => {:greater_than => 0, :only_integer => true, :message => "Must be a number greater than 0"}
  validates :ratio, :numericality => {:greater_than => 0, :message => "Must be a number greater than 0"}
end