class UserStep < ActiveRecord::Base
  
  belongs_to :activity, :polymorphic => true
  belongs_to :user
  belongs_to :steppingstone
  
end
