class PaymentHistory < ActiveRecord::Base

  belongs_to :user
  belongs_to :chain
  belongs_to :restaurant
  belongs_to :receipt
  belongs_to :user_session

end
