class HybridRewardRedemptionJob < Struct.new(:chain)

  def perform
    p "perform---------- HybridRewardRedemptionJob"
    p "chain: #{chain.try(:name)}"
    if chain.user_reward_redeemption_flow.eql?(3)
      chain.restaurants.each do |restaurant|
        hybrid_reward = HybridRewardRedemptionFlow.where(chain_id: chain.id, restaurant_id: restaurant.id).first rescue nil
        HybridRewardRedemptionFlow.create(chain_id: chain.id, restaurant_id: restaurant.id, flow: "pos_based") if hybrid_reward.blank?
      end
    else
      rest_id = chain.restaurants.map{|r| r.id}
      HybridRewardRedemptionFlow.where(chain_id: chain.id, restaurant_id: rest_id).delete_all unless rest_id.blank?
    end
    p "End ---------- HybridRewardRedemptionJob"
  end

  def error(job, exception)
    p "---------Error HybridRewardRedemptionJob-------"
    p "job: #{job}"
    p "exception: #{exception.to_s}"
    p "-----------------finishhhhh------"
  end


end