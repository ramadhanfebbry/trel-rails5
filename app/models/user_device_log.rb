class UserDeviceLog < ActiveRecord::Base

	belongs_to :user
	belongs_to :chain

	def self.add_log(user, device_id, keychain, device_type, access_type, access_time)
    p "add log nihh---------"
		p device_id
		UserDeviceLog.create(:user_id => user.id, :chain_id =>  user.chain_id,:device_id => device_id, :keychain_value => keychain, 
			:device_type => device_type, :access_type => access_type, :access_date_time => access_time )
	end

	def self.check_unique_signup_user(chain_id, keychain, device_type, user_type = nil)
		log_description = user_type.to_s == "onosys" ? "Onosys Signup" : "Signup"
		if device_type.downcase.eql?("iphone")
			existing_signed_up_users_count_based_on_keychain = UserDeviceLog.where(:chain_id => chain_id, :keychain_value => keychain, :access_type => log_description).count
		  if existing_signed_up_users_count_based_on_keychain == 0
			  return true
		  else	
        return false
		  end
		elsif device_type.downcase.eql?("android")
			existing_signed_up_users_count_based_on_keychain = UserDeviceLog.where(:chain_id => chain_id, :device_id => keychain, :access_type => log_description).count
		  if existing_signed_up_users_count_based_on_keychain == 0
			  return true
		  else	
        return false
		  end
		end
	end
end
