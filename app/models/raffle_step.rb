class RaffleStep < ActiveRecord::Base
  has_many :raffle_participation_histories
  belongs_to :raffle

  def change_status
    if self.active
      # make it deactive
      self.update_attribute(:active, false)
    else
      # make it active
      self.update_attribute(:active, true)
    end
  end
end
