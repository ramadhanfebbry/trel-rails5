class ApplicationKey < ActiveRecord::Base
  include AASM
  include PermanentRecords
  
  default_scope { where("deleted_at" => nil) }

  def self.deleted
    self.unscoped.where('deleted_at IS NOT NULL')
  end
    
  aasm :column => :status do
    state :active, :initial => true
    state :inactive
    state :discontinued
    state :disabled

    event :activate do
      transitions :to => :active, :from => [:inactive, :discontinued, :disabled ]
    end

    event :deactivate do
      transitions :to => :inactive, :from => [:active, :discontinued, :disabled ]
    end

    event :discontinue do
      transitions :to => :discontinued, :from => [:active, :inactive, :disabled ]
    end

    event :disable do
      transitions :to => :disabled, :from => [:inactive, :discontinued, :active ]
    end
  end

  belongs_to :application
  has_one :application_key_detail
  accepts_nested_attributes_for :application_key_detail

  
end
