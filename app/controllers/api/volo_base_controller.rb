class Api::VoloBaseController < ApplicationController
  include Api::ApplicationsHelper

  before_action :authorize_api_app!
  before_action :authorize_api_user!, :except => [:connect]

  def authorize_api_user!
    p current_user
    p user_signed_in?
    unless user_signed_in?
      return render :status => 401,
      :json => {status: false, notice: t(:unauthorized_api)}
    else
      if current_user.active == false
        return render :status => 401,
                    :json => {status: false, notice: "You are an inactive user"}
      end
    end
  end

  def authorize_api_user_from_request_header!
    params[:auth_token] = request.headers["auth_token"] if params["auth_token"].blank?
    p "----------------authorize_api_user_from_request_header! VOLO!--- #{params[:auth_token]}"
  end

end