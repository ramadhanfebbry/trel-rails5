class Api::V2::SessionsController < Api::V1::SessionsController
  def new
    super
  end

  def create
    super
  end

  def destroy
    super
  end

  def failure
    super
  end

  def logged_in
    super
  end

  def confirm_reactivation
    super
  end

  def validate_token
    super
  end

  def deactivate_account
   super
  end
end