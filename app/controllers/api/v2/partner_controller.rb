class Api::V2::PartnerController < Api::V1::PartnerController

  def category
    super
  end

  def partner_reward
    super
  end

  def get_partner_reward
    super
  end

  def partner_reward_code
    super
  end

end
