class Api::V2::TestsController < Api::V1::TestsController

  def test_locate
    super
  end

  def test_timeout
    super
  end

  def test_birthday_reward
    super
  end

  def post_birthday_reward
    super
  end

  def reward_list
    super
  end

  def  olo_export
    super
  end

  def test_claim
    super
  end

  def test_claim_reward
    super
  end

  def test_claim_reward_get_info
    super
  end

  def geo_data_testing
    super
  end

  def load_rewards
    super
  end

  def load_restaurants_and_users
    super
  end

  def olo_list_summaries
    super
  end

  def olo_orderexports
    super
  end

  def get_olo_order_batch
    super
  end

  def get_olo_orderexports
    super
  end

  def get_olo_list_summaries
    super
  end

  def referral_email
    super
  end

  def referral
    super
  end

  def claim_message
    super
  end

  def send_referer_email
    super
  end

  def test_answer_survey
    super
  end

  def test_submit_receipt
    super
  end

  def getOfferRestaurants
    super
  end

  def test_promocode
    super
  end

  def form_chain_image
    super
  end

  def test_answer_deal_survey
    super
  end

  def test_post_answer_deal_survey
    super
  end

  def test_post_deal_survey
    super
  end

  def forgot_password
    super
  end

  def submit_receipt
    super
  end

  def update_password
    super
  end

  def user_signin
    super
  end

  def user_signup
    super
  end

  def test_list_deals
    super
  end

  def test_detail_deals
    super
  end

  def test_deal_location
    super
  end

  #goal_stuff
  
  def test_get_categories
    super
  end

  def test_submit_categories
    super
  end

  def test_weekly_user_goal
    super
  end

  def test_reward_activity
    super
  end

  def load_categories
    super
  end

  def load_user_goals
    super
  end

  def test_deadline
    super
  end

  def test_deactivate_account
    super
  end

  def test_fb_reactivate_account
    super
  end

  def test_reactivate_account
    super
  end

  def test_deactivate_account
    super
  end

  def test_fb_reactivate_account
    super
  end

  def test_reactivate_account
    super
  end

  def sign_up_with_referral
    super
  end

  def load_users
    super
  end

  def load_giftable_rewards
    super
  end

  def test_add_gift_to_user
    super
  end

  def test_launch_text
    super
  end

  #POS STUFF start- -------
  def test_pos_reward
    super
  end

  def test_submit_barcode
    super
  end

  #END POS STUFF ------


  def test_submit_survey_step2
    super
  end

  def test_post_submit_survey
    super
  end

  def load_survey
    super
  end

  def load_raffle_additional_info
    super
  end

  def test_olo_aoo_settings
    super
  end

end
