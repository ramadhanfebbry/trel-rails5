  class Api::V2::RegistrationsController < Api::V1::RegistrationsController

  def new
    super
  end

  def create
    super
  end

  def update
    super
  end

  def confirm_reactivation(user)
    super
  end

  def migrate
    super
  end

end


