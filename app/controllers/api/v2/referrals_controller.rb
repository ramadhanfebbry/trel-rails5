class Api::V2::ReferralsController < Api::V1::ReferralsController

  def index
    super
  end

  def email
    super
  end

  def get_incentive_title(incentive)
    super
  end

  def mail_to_referer
    super
  end

end