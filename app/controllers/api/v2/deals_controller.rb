class Api::V2::DealsController < Api::V1::DealsController

  def get_survey
    super
  end

  def answer_survey
    super
  end

  def list_deals
    super
  end

  def deal_detail
    super
  end

  def deal_locations
    super
  end

  def checkin_submit
    super
  end
  
end