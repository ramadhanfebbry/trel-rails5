class Api::V2::OffersController < Api::V1::OffersController

  def nearby
    super
  end

  def index
    super
  end

  def show
    super
  end

  def restaurants
    super
  end
end
