class Api::V2::ApplicationKeysController < Api::V1::ApplicationKeysController
  
  def latest
    super
  end

  def launch_text
    super
  end

  def gallery_stream
    super
  end

  def last
    super
  end
end
