class Api::V2::OlolehosController < Api::V1::OlolehosController

  def registration
    super
  end

  def settings
    super
  end

  def forgot_password
    super
  end

  def login_update
    super
  end

  def update_profile
    super
  end

  def get_profile
    super
  end

  def get_phone_number
    super
  end

  def get_oauth_token
    super
  end

end
