class Api::V2::UserController < Api::V1::UserController

  def update_password
    super
  end

  def show
    super
  end

  def profile
    super
  end

  def activity
    super
  end

  def forgot_password
    super
  end

  def generate_keychain
    super
  end

  def get_user_id
    super
  end

  def pay_code
    super
  end

  def gift_code
    super
  end

  def giftcard_skin
    super
  end

  def module_accessed
    super
  end

  def facebook_data
    super
  end

  def geodata
    super
  end

  def braintree_client_token
    super
  end

  def update_profile
    super
  end

  def get_oauth_token
    super
  end

  def get_olo_mobile_uri
    super
  end

  def delete_old_bt_credit_card
    super
  end

  def notifications
    super
  end

end
