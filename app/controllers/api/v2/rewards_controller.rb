class Api::V2::RewardsController < Api::V1::RewardsController

  def get
    super
  end

  def index
    super
  end

  def destroy
    super
  end

  def claim
    super
  end

  def locate
    super
  end

  def activity
    super
  end

  def test_locate
    super
  end

  def test_claim
    super
  end

  def share
    super
  end

  def tap_to_gift
    super
  end

  def active
    super
  end

end