class Api::V2::ReceiptsController < Api::V1::ReceiptsController

  def new
    super
  end

  def create
    super
  end

  def upload
    super
  end

  def check_detail
    super
  end

  def pay_table_service
    super
  end

end
