class Api::V2::GiftCardsController < Api::V1::GiftCardsController

  def activate
    super
  end

  def deposit
    super
  end

  def balance
    super
  end

  def gift
    super
  end

  def unload
    super
  end

  def test_call
    super
  end

end
