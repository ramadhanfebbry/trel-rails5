class Api::V2::SocialSharesController < Api::V1::SocialSharesController

  def index
    super
  end

  def user_interaction
    super
  end

end