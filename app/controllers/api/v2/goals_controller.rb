class Api::V2::GoalsController < Api::V1::GoalsController

  def index
    super
  end

  def complete
    super
  end

  def deadline
    super
  end

  def history
    super
  end

end