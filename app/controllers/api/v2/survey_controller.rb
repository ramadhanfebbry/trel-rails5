class Api::V2::SurveyController < Api::V1::SurveyController

  def index
    super
  end

  def show
    super
  end

  def getSurvey
    super
  end

  def answer
    super
  end

  def pull
    super
  end

  def skip
    super
  end

end
