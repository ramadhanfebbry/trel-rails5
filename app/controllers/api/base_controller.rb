class Api::BaseController < ApplicationController
  include Api::ApplicationsHelper
  before_action :dump_headers
  before_action :set_locale

  before_action :authenticate, :only => [:category, :partner_reward, :get_partner_reward, :partner_reward_code]
  before_action :authorize_api_app_from_request_header!, :only => :notifications
  before_action :authorize_api_user_from_request_header!, :only => :notifications

  #FOR V2 PURPOSES
  before_action :authorize_api_app_from_request_header!, :if => :is_v2_request?
  before_action :authorize_api_user_from_request_header!, :if => :is_v2_request?
  before_action :check_query_string, :if => :is_v2_post_request?

  before_action :authorize_api_app!, :except => [:latest, :test_promocode, :create_test_promocode, :last]
  before_action :authorize_api_user!, :except => [:nearby, :answer, :forgot_password,
    :test_answer_survey, :test_submit_receipt, :latest, :test_promocode, :create_test_promocode,
    :list_deals, :deal_detail, :get_image_info, :migrate, :launch_text, :generate_keychain, :module_accessed,
    :geodata, :gallery_stream, :get_olo_mobile_uri, :giftcard_skin, :category, :partner_reward, :get_partner_reward, :partner_reward_code, :last]
  
  def authorize_api_user!
    unless user_signed_in?
      return render :status => 401,
      :json => {status: false, notice: t(:unauthorized_api)}
    else
      if current_user.active == false
        return render :status => 401,
                    :json => {status: false, notice: "You are an inactive user"}
      end
    end

    #    render :json => { status: false, notice: 'Unauthorized API request'}, :status => :unauthorized unless user_signed_in?
  end

  def is_v2_request?
    return true if params["controller"] == "api/v1/partner" && params["action"] == "category"
    return true if params["controller"] == "api/v1/partner" && params["action"] == "get_partner_reward"
    request.fullpath.include?("/api/v2/") && request.get?
  end

  def is_v2_post_request?
    request.fullpath.include?("/api/v2/") && request.post?
  end

  def check_and_build_params
    unless params["_json"].blank?
      new_params = params["_json"]
      if new_params.class.to_s == Hash
        params.merge!(new_params)
      else
        params.merge!(JSON.load(new_params))
      end
    end
  end

  def authorize_api_user_from_request_header!
    p "----------------authorize_api_user_from_request_header!----- #{params} -- #{request.headers["auth_token"]}"
    params["auth_token"] = request.headers["auth_token"]
  end

  def check_query_string
    if request.query_string.present?
      p "----------------check_query_string!----- #{request.query_string} "
      return render :status => 404, :json => {status: false, notice: "Invalid API request."}
    end if request.fullpath.include?("/api/v2/") && request.post?
  end

  def authenticate
    p "GO authenticate . . ."
    authenticate_or_request_with_http_basic do |user_name, password|
      RelevantAuth.authenticate(user_name, password)
    end
  end

end