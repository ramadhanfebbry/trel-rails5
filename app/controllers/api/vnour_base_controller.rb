class Api::VnourBaseController < ApplicationController
  include Api::ApplicationsHelper

  before_action :authorize_api_app_from_request_header!, :except => [:customer_connect, :forgot_password, :update_password, :profile]
  before_action :authorize_api_user_from_request_header!, :except => [:connect_setting, :customer_connect, :search_profile,
                                                                     :password_hint, :forgot_password, :update_password, :profile]

  before_action :authorize_api_app!
  before_action :authorize_api_user!, :except => [:connect_setting, :customer_connect, :search_profile,
                                                   :password_hint, :forgot_password]

  def authorize_api_user!
    p current_user
    p user_signed_in?
    unless user_signed_in?
      return render :status => 401,
      :json => {status: false, notice: t(:unauthorized_api)}
    else
      if current_user.active == false
        return render :status => 401,
                    :json => {status: false, notice: "You are an inactive user"}
      end
    end
  end

  def authorize_api_user_from_request_header!
    params[:auth_token] = request.headers["auth_token"] if params["auth_token"].blank?
    p "----------------authorize_api_user_from_request_header! VNOUR!--- #{params[:auth_token]}"
  end

end

