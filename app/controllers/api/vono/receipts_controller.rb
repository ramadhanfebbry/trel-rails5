class Api::Vono::ReceiptsController < Api::VonoBaseController

  skip_before_action :authorize_api_app_from_request_header!
  skip_before_action :authorize_api_user_from_request_header!
  skip_before_action :authorize_api_app!
  skip_before_action :authorize_api_user!

  before_action :authorize_api_app_from_header!

  def online_order
    p params
    email = params["email"]
    render :json => {:status => 1, :resultDescription => "Email can't be blank."} and return if email.blank?
    order_detail = params["order"]
    render :json => {:status => 1, :resultDescription => "Order detail can't be blank."} and return if order_detail.blank?
    oo_id = order_detail["id"]
    render :json => {:status => 1, :resultDescription => "Invalid Order."} and return if oo_id.blank?
    render :json => {:status => 1, :resultDescription => "Duplicate Order Submitted."} and return if Onosys::OnlineOrder.select("id").where(:online_order_identifier => oo_id).count > 0
    onosys_restaurant_id = order_detail["restaurant_id"] rescue nil
    if onosys_restaurant_id
      restaurant_detail = RestaurantDetail.where(external_location_store_id: params[:location_id]).first rescue nil
      restaurant= restaurant_detail.restaurant rescue nil
      render :json => {:status => 1, :resultDescription => "Invalid Location."} and return if restaurant.blank?
    end
    offer = restaurant.first_active_offer
    render :json => {:status => 1, :resultDescription => "Invalid Offer."} and return if offer.blank?

    ooo = Onosys::OnlineOrder.new(
        :chain_id => @chain.id, :restaurant_id => restaurant.try(:id),
        :online_order_identifier => oo_id, :loyalty_status => Onosys::OnlineOrder::STATUS[:NEW],
        :platform => @app.device_type, :email => email
    )

    if User.is_valid_email_address?(email)
      check_user = @chain.users.where(:email => email).first
      if check_user
        user = check_user
        ooo.user_id = check_user.id
      else
       user = @chain.users.new
       user.email = email
       user.password = ('a'..'z').to_a.shuffle.first(10).join
       user.register_type = 1
       user.locale_id = Locale.find_by_key('en').try(:id)
       user.chain_app_key = request.headers["appkey"]
       user.register_device_type = DEVICE_TYPE_SELECT.invert[@app.device_type].parameterize("_")
       user.sign_in_device_type = DEVICE_TYPE_SELECT.invert[@app.device_type].parameterize("_")
       user.signup_device_status = 3
       user.selected_device_info = nil
       user.save
       ooo.user_id = user.id
      end
      ooo.save
      ooo.save_detail(order_detail)

      #create receipt process
      receipt_id = ooo.as_received_receipt(@chain.id, user.id, order_detail)
      Onosys::ReceiptOnlineOrder.create(:receipt_id => receipt_id, :onosys_online_order_id => ooo.id)
      Delayed::Job.enqueue(Onosys::OnlineOrderProcessJob.new(receipt_id, ooo.id, restaurant.id, offer.id), :run_at => (Time.zone.now + @chain.pos_receipt_processing_delay))
      render :json => {:status => 1, :resultDescription => "success!", :loyaltyOrderID => receipt_id}
    else
      ooo.save
      ooo.save_detail(order_detail)
      render :json => {:status => 1, :resultDescription => "Invalid Email format!"}
    end

  end


end