class Api::Vono::UserController < Api::VonoBaseController
  skip_before_action :authorize_api_user!, :only => [:connect_setting, :connect]

  def connect_setting
    onosys_connect_setting = OnosysConnectSetting.find_by_chain_id(@chain.id) rescue nil

    if onosys_connect_setting
      render :json => {:status => true, :vono_connect_setting => {
          "api_root" => onosys_connect_setting.api_root,
          "api_key" => onosys_connect_setting.api_key,
          "iphone_api_key" => onosys_connect_setting.iphone_api_key,
          "android_api_key" => onosys_connect_setting.android_api_key,
          "client_id" => onosys_connect_setting.client_id,
          "client_secret" => onosys_connect_setting.client_secret,
          "provider" => onosys_connect_setting.provider
      } }
    else
      render :json => {:status => false, :message => "Chain does not have Onosys connect setting" }
    end
  end

  def connect
    p params
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    params[:password] = URI.decode(params[:password]) rescue ""
    params[:security_question] = URI.decode(params[:security_question]) rescue ""
    params[:security_answer] = URI.decode(params[:security_answer]) rescue ""
    params[:email] = params[:email].try(:downcase)

    if params[:connect_type].eql?("1")
      render :json => {:status => false, :notice => "first name must more than 1 characters"} and return if params[:first_name].length < 2
      render :json => {:status => false, :notice => "last name must more than 1 characters"} and return if params[:last_name].length < 2
    end

    params[:user] = {:first_name=>params[:first_name],
                     :last_name=>params[:last_name],
                     :username=>params[:username],
                     :email=>params[:email],
                     :password => params[:password],
                     :latitude=>params[:latitude],
                     :longitude=>params[:longitude],
                     :chain_id=>@chain.id,
                     :register_type=>params[:register_type],
                     :register_device_type=>((params[:sign_in_device_type] || params[:register_device_type]).downcase.tr(' ', '_') rescue nil),
                     :device_id => params[:device_id],
                     :ref_code => params[:referral_code],
                     :phone_number => params[:phone_number],
                     :dob_day => params[:dob_day],
                     :dob_month => params[:dob_month],
                     :dob_year => params[:dob_year],
                     :special_occassion => params[:special_occassion],
                     :zipcode => params[:zipcode],
                     :name => [params[:first_name], params[:last_name]].join(" "),
                     :marketing_optin => User.marketing_optin_value(params[:marketing_optin]),
                     :favorite_location => params[:favorite_location],
                     :special_occassion => params[:special_occassion],
                     :address1 => params[:address1],
                     :address2 => params[:address2],
                     :city => params[:city],
                     :state => params[:state],
                     :marketing_optin_texting => params[:marketing_optin_texting],
                     :device_token => params[:device_token]
    }
    params[:user_type] = "onosys"
    params[:user][:chain_id] = @chain.id
    params[:user][:locale_id] = Locale.find_by_key(params[:locale] ||= 'en').try(:id)
    params[:user][:olo_registration] = true #add this to activate validation for firstname and lastname
    vext_connect_setting = OnosysConnectSetting.get_onosys_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if vext_connect_setting.blank?
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly", :dev_message => "Account ID is blank"} and return if params[:account_id].blank?
    case params[:connect_type]
      when "1"
        users = User.unscoped.where("email = '#{params[:email]}' AND active IS TRUE and deleted_at IS NULL AND chain_id = #{@chain.id}").order("current_sign_in_at DESC")
        user_check = users.first
        if user_check.blank?
          user_check = User.new(params[:user])
        else
          user_check.attributes = params[:user].reject { |key,value| value.nil? }
        end
        user_check.password = ('a'..'z').to_a.shuffle.first(10).join
        user_check.required_domain_email_validation = @chain.chain_setting.domain_email_validation
        if !params[:favorite_location].blank?
          rest = @chain.restaurants.where("id = ? ", params[:favorite_location]) rescue []
          render :json => {:status => false, :notice => "Favorite Location not valid"} and return if rest.blank?
        end
        valid = user_check.valid?
        if !valid && !params[:phone_number].blank? && !users.select{|a| a.phone_number == params[:phone_number]}.blank?
          user_check.errors.messages.delete(:phone_number)
        end
        unless user_check.errors.messages.blank?
          p "user is not valid ------"
          p  user_check.errors
          custom_error = []
          if !user_check.errors.messages[:email].blank? && user_check.errors.messages[:email].first == "Please insert a valid email address"
            custom_error <<  "Please enter a valid email address."
          end
          if !user_check.errors.messages[:phone_number].blank? && user_check.errors.messages[:phone_number].first == "Please enter a valid 10 digit phone number"
            custom_error << "Please enter a valid phone number."
          end
          if !user_check.errors.messages[:phone_number].blank? && user_check.errors.messages[:phone_number].first == "This phone number is already taken. Please use the Login button to continue, or use a secondary phone number to Sign Up"
            custom_error << "The phone number you entered is already in use. Please enter a new phone number."
          end
          render :json => {:status => false, :notice => custom_error.blank? ? "Please complete all required fields with valid information" : custom_error.join(", "), :dev_message => user_check.errors.messages} and return
        end

        user = user_check
        if !user.id.blank?
          user.save
          if user.onosys_profile
            user.onosys_profile.update_attributes(:account_id => params["account_id"])
          else
            OnosysProfile.create(user_id: user.id, account_id: params["account_id"])
          end
          user.reload
          user.reset_authentication_token!
          notice_text = params[:referral_code].blank? ? (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 4)) : (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 3))
          return render :json => {:status => true, :auth_token => user.authentication_token,
                                  :notice => notice_text, :account_id => user.onosys_profile.try(:account_id)}
        else
          user = User.new(params[:user])
          user.password = ('a'..'z').to_a.shuffle.first(10).join
          if user.save
            if user.onosys_profile
              user.onosys_profile.update_attributes(:account_id => params["account_id"])
            else
              OnosysProfile.create(user_id: user.id, :account_id => params["account_id"])
            end
            user.reload
            user.reset_authentication_token!
            notice_text = params[:referral_code].blank? ? (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 4)) : (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 3))
            #MOVED TO DELAYED JOB
            Delayed::Job.enqueue(UserSignupJob.new(@chain, user, params))
            return render :json => {:status => true, :auth_token => user.authentication_token,
                                    :notice => notice_text, :account_id => user.onosys_profile.try(:account_id)}
          else
            p user.errors
            render :json => {:status => false, :notice => "Please complete all required fields with valid information", :dev_message => user.errors.messages} and return
          end
        end
      when "2"
        user_check = User.unscoped.where("email = '#{params[:email]}' AND active IS TRUE and deleted_at IS NULL AND chain_id = #{@chain.id}").order("current_sign_in_at DESC").first
        p user_check
        params[:user][:olo_registration] = false #add this to skip validation for firstname and lastname
        if user_check.blank?
          user_check = User.new(params[:user])
        else
          params[:user].reject! {|key, value| key == "marketing_optin"} if params[:marketing_optin].blank?
          params[:user].reject! {|key, value| key == "marketing_optin_texting"} if params[:marketing_optin_texting].blank?
          user_check.attributes = params[:user].reject { |key,value| value.nil? }
        end
        user_check.password = ('a'..'z').to_a.shuffle.first(10).join
        user = user_check
        if !user.id.blank?
          user.save
          if user.onosys_profile
            user.onosys_profile.update_attributes(:account_id => params["account_id"])
          else
            OnosysProfile.create(user_id: user.id, :account_id => params["account_id"])
          end
          user.reload
          user.reset_authentication_token!
        else
          if user.save
            if user.onosys_profile
              user.onosys_profile.update_attributes(:account_id => params["account_id"])
            else
              OnosysProfile.create(user_id: user.id, :account_id => params["account_id"])
            end
            user.reload
            user.reset_authentication_token!
          else
            p "error when saving users"
            p user.errors
            render :json => {:status => false, :notice => "Something went wrong with your request. Try to email or call app customer support."} and return
          end
        end
        UserDeviceLog.add_log(user, params[:android_id], params[:keychain], (params[:sign_in_device_type] || params[:register_device_type]).downcase.tr(' ', '_'), "Onosys Login", Time.current)
        return render :json => {:status => true, :auth_token => user.authentication_token,
                                :notice => t(:welcome_back), :account_id => user.onosys_profile.try(:account_id)}
    end
  end

  def get_profile
    user = current_user
    if user.chain_id == @chain.id
      account_id = user.try(:onosys_profile).try(:account_id) rescue nil
      access_token = user.try(:onosys_profile).try(:access_token) rescue nil
      render :json => {:status => false, :notice => "We were unable to access your information. Please try again."} and return if account_id.blank? || access_token.blank?

      onosys_connect_setting = OnosysConnectSetting.get_onosys_connect_setting(@chain.id) rescue nil
      render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if onosys_connect_setting.blank?

      onosys_api_user = Onosys::Api::User.new(onosys_connect_setting)
      success, message = onosys_api_user.get_profile(account_id, access_token)
      if success
        render :json => {status: true, user: user.as_json(:methods => [:profile_pic, :favorite_location_name]).merge(
            :milestone_points => user.user_milestone_points,
            :weekly_receipt_approved => user.approved_receipt_count("weekly"),
            :monthly_receipt_approved => user.approved_receipt_count("monthly"),
            :yearly_receipt_approved => user.approved_receipt_count("yearly"),
        ), onosys_profile: message} and return
      else
        render :json => {:status => false, :notice => message } and return
      end
    else
      render :json => {status: false, notice: t(:user_details_not_found)}
    end
  end

  def update_profile
    user = current_user
    user.email = params[:email] unless params[:email].blank?
    user.first_name = params[:first_name] unless params[:first_name].blank?
    user.last_name = params[:last_name] unless params[:last_name].blank?
    user.phone_number = params[:phone_number] unless params[:phone_number].blank?
    user.favorite_location = params[:favorite_location] unless params[:favorite_location].blank?
    user.dob_day = params[:dob_day] unless params[:dob_day].blank?
    user.dob_month = params[:dob_month] unless params[:dob_month].blank?
    user.dob_year = params[:dob_year] unless params[:dob_year].blank?

    if !params[:favorite_location].blank?
      rest = @chain.restaurants.where("id = ? ", params[:favorite_location]) rescue []
      render :json => {:status => false, :notice => "Favorite Location not valid"} and return if rest.blank?
    end

    if params[:email].present?
      exist_email = @chain.users.find_by_email(params[:email])
      render :json => {:status => false, :notice => "We are Sorry! The email is already associated with an account."} and return if exist_email.present? && exist_email.id != user.id
    end
    if params[:phone_number].present?
      exist_phone = @chain.users.find_by_phone_number(params[:phone_number])
      render :json => {:status => false, :notice => "We are Sorry! The phone number is already associated with an account."} and return if exist_phone.present? && exist_phone.id != user.id
    end

    render :json => {:status => false, :notice => "first name must more than 1 characters"} and return if params[:fname].present? && params[:fname].length < 2
    render :json => {:status => false, :notice => "last name must more than 1 characters"} and return if params[:lname].present? && params[:lname].length < 2

    account_id = user.try(:onosys_profile).try(:account_id) rescue nil
    access_token = user.try(:onosys_profile).try(:access_token) rescue nil
    render :json => {:status => false, :notice => "We were unable to update your information. Please try again."} and return if account_id.blank? || access_token.blank?

    onosys_connect_setting = OnosysConnectSetting.get_onosys_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if onosys_connect_setting.blank?

    onosys_api_user = Onosys::Api::User.new(onosys_connect_setting)
    success, message = onosys_api_user.update_profile(account_id, access_token, params)
    if success
      user.save
      render :json => {:status => true, :notice => message } and return
    else
      render :json => {:status => false, :notice => message } and return
    end
  end

  def update_password
    p "before---"
    p params
    params[:old_password] = URI.decode(params[:old_password]) rescue ""
    params[:new_password] = URI.decode(params[:new_password]) rescue ""
    params[:confirm_password] = URI.decode(params[:confirm_password]) rescue ""
    p "after---"
    p params

    new_password = params[:new_password]
    confirm_password = params[:confirm_password]
    render :json => {:status => false, :notice => "Please complete all required fields with valid information"} and return if new_password.blank? || confirm_password.blank? || !new_password.eql?(confirm_password)
    render :json => {:status => false, :notice => "Password must be 6 characters or longer"} and return unless new_password.length >= 6

    onosys_connect_setting = OnosysConnectSetting.get_onosys_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if onosys_connect_setting.blank?
    render :json => {:status => false, :notice => "We were unable to update your information. Please try again."} and return if !current_user.valid_password?(params[:old_password])

    account_id = current_user.try(:onosys_profile).try(:account_id) rescue nil
    access_token = current_user.try(:onosys_profile).try(:access_token) rescue nil
    render :json => {:status => false, :notice => "We were unable to update your information. Please try again."} and return if account_id.blank? || access_token.blank?
    params[:email] = current_user.email
    onosys_api_user = Onosys::Api::User.new(onosys_connect_setting)

    success, message = onosys_api_user.update_password(params, access_token)
    if success
      if current_user.reset_password!(new_password, confirm_password)
        render :json => {:status => true, :notice => message } and return
      else
        render :json =>{:status=>false, notice: t(:error_updating_password)} and return
      end
    else
      render :json => {:status => false, :notice => message } and return
    end
  end

  def forgot_password
    render :json => {:status => false, :notice => "Please insert a valid email address."} and return if params[:email].blank?
    p "------before--"
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    p "------after--"
    p params
    onosys_connect_setting = OnosysConnectSetting.get_onosys_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if onosys_connect_setting.blank?

    user = @chain.users.where(:email => params[:email]).first rescue nil
    account_id = user.try(:onosys_profile).try(:account_id) rescue nil
    render :json => {:status => false, :notice => "We were unable to access your information. Please check entered information and try again."} and return if account_id.blank?

    onosys_api_user = Onosys::Api::User.new(onosys_connect_setting)
    success, message = onosys_api_user.forgot_password(params)
    if success
      render :json => {:status => true, :notice => message } and return
    else
      render :json => {:status => false, :notice => "We were unable to access your information. Please check entered information and try again." } and return
    end
  end

  def get_refresh_token
    user = current_user
    if user.chain_id == @chain.id
      account_id = user.try(:onosys_profile).try(:account_id) rescue nil
      access_token = user.try(:onosys_profile).try(:access_token) rescue nil
      refresh_token = user.try(:onosys_profile).try(:refresh_token) rescue nil
      render :json => {:status => false, :notice => "We were unable to access your information. Please try again."} and return if account_id.blank? || access_token.blank? || refresh_token.blank?

      onosys_connect_setting = OnosysConnectSetting.get_onosys_connect_setting(@chain.id) rescue nil
      render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if onosys_connect_setting.blank?

      onosys_api_user = Onosys::Api::User.new(onosys_connect_setting)
      success, message = onosys_api_user.get_refresh_token(user.email, refresh_token)
      if success
        user
        render :json => {status: true, user: user.as_json(:methods => [:profile_pic, :favorite_location_name]).merge(
            :milestone_points => user.user_milestone_points,
            :weekly_receipt_approved => user.approved_receipt_count("weekly"),
            :monthly_receipt_approved => user.approved_receipt_count("monthly"),
            :yearly_receipt_approved => user.approved_receipt_count("yearly"),
        ), onosys_profile: message} and return
      else
        render :json => {:status => false, :notice => message } and return
      end
    else
      render :json => {status: false, notice: t(:user_details_not_found)}
    end
  end

  def get_rewards
    render :json => {:status => false, :notice => "Please insert a valid email address."} and return if params[:email].blank?
    p "------before--"
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    p "------after--"
    p params

    user = @chain.users.where(:email => params[:email]).first rescue nil
    if user
      rewards = collect_rewards(params[:appkey], @chain, user)
      render :json => { :status => true, :balance => {:points => (user.points/(@chain.user_points || 1)), :milestone_points => user.point_threshold}, rewards_image: "", rewards: rewards } and return
    else
      pwd = (0...8).map { (65 + rand(26)).chr }.join
      phone_number = rand.to_s[2..11]
      new_user = User.new(email: params[:email], register_type: 1, password: pwd, chain_id: @chain.id, phone_number: phone_number)
      if new_user.save
        rewards = collect_rewards(params[:appkey], @chain, new_user)
        render :json => { :status => true, :balance => {:points => (new_user.points/(@chain.user_points || 1)), :milestone_points => new_user.point_threshold}, rewards_image: "", rewards: rewards } and return
      else
        render :json => {:status => false, :notice => "User Not Found On System" } and return
      end
    end
  end

  def reward_claim
    latitude= params[:lat]
    longitude= params[:lng]
    render :json => {:status => false, :notice => "Please complete all required fields with valid information"} and return if params[:email].blank? || params[:reward_id].blank? || params[:location_id].blank?
    user = @chain.users.where(:email => params[:email]).first rescue nil
    render :json => {:status => false, :notice => "We were unable to access your information. Please check entered information and try again."} and return if user.blank?

    restaurant_detail = RestaurantDetail.where(external_location_store_id: params[:location_id]).first  rescue nil
    restaurant= restaurant_detail.restaurant rescue nil
    render :json => {:status => false, :notice => "Invalid Location."} and return if restaurant.blank?
    render :json => {status: false, notice: "Restaurant is not active"} and return unless restaurant.status
    if restaurant.status && [Restaurant::TYPES["COMING_SOON"]].include?(restaurant.restaurant_detail.status)
      render :json => {status: false, notice: "Restaurant is not active"} and return
    end if restaurant.restaurant_detail

    reward= Reward.find(params[:reward_id])
    case reward.reward_type
      when Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"], Reward::TYPES["BIRTHDAY"]
        user_reward_wallets = RewardWallet.where("user_id = ? AND reward_id = ? AND date(expiry_date) >= ?", user.id, reward.id, Date.current).order("created_at asc")
        render :json => {status: false, notice: "Reward expired"} and return if user_reward_wallets.blank?
        user_reward_claimed = user_reward_wallets.select{|r| r.status == RewardWallet::STATUS[:CLAIMED]}
        user_reward_active = user_reward_wallets.select{|r| r.status == RewardWallet::STATUS[:ACTIVE] || r.status == RewardWallet::STATUS[:REDEEMING]}
        render :json => {status: false, notice: "Reward already used"} and return if (!user_reward_claimed.blank? &&  user_reward_active.blank?)
    end

    if user.points >= reward.points
      render :json => {status: false, notice: t(:dont_have_enough_pints_reward)} and return if @chain.chain_setting.hybrid_milestone_regular_rewards_enabled && (user.point_threshold || 0) < reward.points
      p "----ONOSYS CLAIMED--"*10
      staffcode, length = staffcode_generate(reward, restaurant, @chain, user) #generate staffcode
      puts "1 " * 33
      puts staffcode
      staffcode = staffcode.add_zero_padding(length.to_i)
      puts "2 " * 33
      p staffcode
      puts "3 " * 33

      onosy_reward_claim = user.onosys_reward_claim
      if onosy_reward_claim.present?
        onosy_reward_claim.update_attributes(user_id: user.id, reward_id: reward.id, reward_code: staffcode, location_id: params[:location_id])
      else
        onosy_reward_claim = OnosysRewardClaim.create(user_id: user.id, reward_id: reward.id, reward_code: staffcode, location_id: params[:location_id])
      end
      if @chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM APP"])
        user.claim_with_staffcode_and_timer(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
      elsif @chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
        staffcode = onosy_reward_claim.reward_code
        user.claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, "ONOSYS")
      elsif @chain.on_hybrid_setting_reward_redeem_flow?
        if @chain.on_hybrid_setting_pos_based_rule?(restaurant)
          user.claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
        elsif @chain.on_hybrid_setting_app_based_rule?(restaurant)
          user.claim_with_staffcode_and_timer(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
        end
      end
      render :json => {:status => true, :notice => "Claim updated. Enjoy your reward.", :reward_code => staffcode, :reward_timer => reward_timer_generate}
      p " -------- rendered"
    else
      p "----ONOSYS NOT CLAIMED--"*10
      render :json => {status: false, notice: t(:dont_have_enough_pints_reward)}
      p "------ rendered"
    end
  end

  def reward_redeem
    render :json => {:status => false, :notice => "Please complete all required fields with valid information"} and return if params[:email].blank? || params[:reward_code].blank? || params[:location_id].blank? || params[:request_type].blank?
    user = @chain.users.where(:email => params[:email]).first rescue nil
    render :json => {:status => false, :notice => "We were unable to access your information. Please check entered information and try again.", :response_code => 1} and return if user.blank?

    reward_claim = OnosysRewardClaim.where(user_id: user.id, reward_code: params[:reward_code], location_id: params[:location_id]).first
    render :json => {:status => false, :notice => "Reward code invalid", :response_code => 1} and return if reward_claim.blank?

    restaurant_detail = RestaurantDetail.where(external_location_store_id: params[:location_id]).first  rescue nil
    restaurant= restaurant_detail.restaurant rescue nil
    render :json => {:status => false, :notice => "Invalid Location."} and return if restaurant.blank?
    render :json => {status: false, notice: "Restaurant is not active"} and return unless restaurant.status
    if restaurant.status && [Restaurant::TYPES["COMING_SOON"]].include?(restaurant.restaurant_detail.status)
      render :json => {status: false, notice: "Restaurant is not active", :response_code => 1} and return
    end if restaurant.restaurant_detail

    reward= Reward.find(reward_claim.reward_id)
    case reward.reward_type
      when Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"], Reward::TYPES["BIRTHDAY"]
        user_reward_wallets = RewardWallet.where("user_id = ? AND reward_id = ? AND date(expiry_date) >= ?", user.id, reward.id, Date.current).order("created_at asc")
        render :json => {status: false, notice: "Reward expired"} and return if user_reward_wallets.blank?
    end

    if params[:request_type].to_i == 0
      if params[:warn] == "true"
        return show_confirmation_claim
      end

      if user.points >= reward.points
        render :json => {status: false, notice: t(:dont_have_enough_pints_reward), :response_code => 1} and return if @chain.chain_setting.hybrid_milestone_regular_rewards_enabled && (user.point_threshold || 0) < reward.points
        p "----ONOSYS CLAIMED--"*30
        reward_transaction = RewardTransaction.find(reward_claim.reward_transaction_id)
        if @chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (@chain.on_hybrid_setting_reward_redeem_flow?)
          Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
        end
        render :json => {:status => true, :notice => t(:claim_updated), :response_code => 0}
      else
        p "----ONOSYS NOT CLAIMED--"*10
        render :json => {status: false, notice: t(:dont_have_enough_pints_reward), :response_code => 1}
      end
    elsif params[:request_type].to_i == 1
      p "----ONOSYS VOIDS CLAIMED--"*10
      t_id = reward_claim.reward_transaction_id
      render :json => {:status => false, :response_code => 0 , :notice => "Could not find the transaction !!"} and return if t_id.blank?
      reward_transaction = RewardTransaction.find(t_id)
      render :json => {:status => false, :response_code => 0 , :notice => "Could not find the transaction !!"} and return if reward_transaction.blank?

      user = User.find reward_transaction.user_id
      rt = reward_transaction
      reward = rt.reward
      unless rt.blank?
        reward_wallet = RewardWallet.where(:user_id => user.id, :reward_id => reward.id).first
        p "reward_wallet #{reward_wallet}"
        render :json => {status: false, notice: "Can't Void reward, status not meet."} and return if reward_wallet.status != 3

        user.earn(reward.points) rescue nil
        PointHistory.add_to_history(user.id, "Void Redeem for ##{reward.name}", (reward.points * 1)) rescue nil
        reward_wallet.update_attribute(:status, RewardWallet::STATUS[:ACTIVE]) rescue nil
        rt.update_column(:pos_used, false) rescue nil
        return render :json => {:status => true,  :response_code => 1, :notice => "Void SUCCESS"}
      else
        return render :json => {:status => false, :response_code => 0 , :notice => "User Not Found On System"}
      end
    end

  end

  private

  def show_confirmation_claim
    question = "Are You sure?"
    body = "Please do NOT click 'Confirm' until you are in front of our staff. Voucher valid for 2 minutes only."
    ## get the warning based on user locale
    begin
      key = current_user.locale.key
      content = REDIS.hget "chain_#{@chain.id}_warning_claim", key+"_warning_claim"
      content = JSON.parse(content)
      question = content["question"]
      body = content["body"]
    rescue => e
      puts "API:RewardController::show_confirmation_claim = #{e.inspect}"
    end
    render :json => {:warn_tile => question,:warn_body =>  body}
  end

  def staffcode_generate(reward, restaurant, chain, user)
    if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
      return Reward.pos_claim_staffcode_generate(chain, user)
    elsif chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM APP"])
      if reward.static_pos_code_setup?
        return reward.POSCode
      elsif chain.reward_code_uploaded_mode?
        return chain.get_reward_code_from_uploaded_code(user, reward, restaurant)
      elsif chain.reward_code_fishbowl_promotion_mode?
        return reward.get_fishbowl_promotion_code(user, restaurant)
      elsif chain.reward_code_from_relevant_mode?
        return Barcode.generate_reward_code(chain)
      end
    elsif chain.on_hybrid_setting_reward_redeem_flow?
      if chain.on_hybrid_setting_pos_based_rule?(restaurant)
        return Reward.pos_claim_staffcode_generate(chain, user)
      elsif chain.on_hybrid_setting_app_based_rule?(restaurant)
        if reward.static_pos_code_setup?
          return reward.POSCode
        elsif chain.reward_code_uploaded_mode?
          return chain.get_reward_code_from_uploaded_code(user, reward, restaurant)
        elsif chain.reward_code_fishbowl_promotion_mode?
          return reward.get_fishbowl_promotion_code(user, restaurant)
        elsif chain.reward_code_from_relevant_mode?
          return Barcode.generate_reward_code(chain)
        end
      end
    end
    return "-NOCODE-"
  end

  def reward_timer_generate
    c = Chain.find(@chain.id)
    c.timer
  end

  def without_timezone(date)
    date.strftime('%Y-%m-%dT23:59:59 +0000') if date
  end

  def collect_rewards(appkey, chain, user)
    chain_rewards= Reward.of_chain(chain.id, only_active=true, only_in_effect=true, only_unexpired=true)
    user_rewards= Reward.of_user(user.id, include_removed=false, include_claimed=false, include_gifted=false, twenty_day_exp = true)

    rewards = (chain_rewards + user_rewards).compact  #union both
    show_gifter = @app.application_keys.where(:appkey => appkey).first.show_gifter rescue false
    show_expired_reward = @app.application_keys.where(:appkey => appkey).first.show_expired_reward rescue true
    rewards = rewards.select{|a| !a.gifter} unless show_gifter
    rewards.sort! do |a, b|
      if a.gifter
        -1
      elsif b.gifter
        1
      elsif(a.expired.eql? b.expired)
        unless a.priority_number.eql?(b.priority_number)
          (b.priority_number <=> a.priority_number)
        else
          if a.points == 0 && b.points == 0
            (a.expiryDate <=> b.expiryDate)
          else
            (a.points <=> b.points)
          end
        end
      else
        a.expired ? 1 : -1
      end
    end

    rewards.each_with_index do |r, i|
      r.sort_by_id= i
      r.points = r.points/(chain.user_points || 1)
      r.device_expired = r.expired
    end

    reward_json = []
    rewards.map do |r|
      if r.reward_type == Reward::TYPES["REGULAR"]
        a = {:id => r.id, :name => r.name, :points => r.points, :fineprint => r.fineprint,
             :effectiveDate => without_timezone(r.effectiveDate), :expiryDate => without_timezone(r.expiryDate), :chain_id => r.chain_id,
             :survey_id => r.survey_id, :reward_type => r.reward_type, :POSCode => r.POSCode,
             :expired => r.expired, :sort_by_id => r.sort_by_id, :gifter => r.gifter ,
             :image_url => r.attachment.url.include?("missing") ? "" : r.reload.attachment.url,
             :image_thumbnail_url => r.thumbnail.url.include?("missing") ? "" : r.reload.thumbnail.url,
             :additional_fields => r.additional_informations, :device_reward_expired =>  r.device_expired
        }
      else
        a =
            {:id => r.id, :name => r.name, :points => r.points, :fineprint => r.fineprint,
             :effectiveDate => without_timezone(r.effectiveDate), :expiryDate => without_timezone(r.expiryDate), :chain_id => r.chain_id,
             :survey_id => r.survey_id, :reward_type => r.reward_type, :POSCode => r.POSCode,
             :expired => r.expired, :sort_by_id => r.sort_by_id, :gifter => r.gifter ,
             :image_url => r.attachment.url.include?("missing") ? "" : r.reload.attachment.url,
             :image_thumbnail_url => r.thumbnail.url.include?("missing") ? "" : r.reload.thumbnail.url,
             :new_reward => r.new_reward,
             :additional_fields => r.additional_informations, :device_reward_expired =>  r.device_expired
            }
      end

      unless show_expired_reward
        reward_json << a unless r.expired
      else
        reward_json << a
      end
    end
    return reward_json
  end

end