class Api::Vono::TestsController < ApplicationController
  include Api::ApplicationsHelper
  require 'uri'
  require 'net/http'

  def request_header_for_get_onosys_settings
    url = URI("http://trelevant.herokuapp.com/api/vono/connect/setting")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url.request_uri)
    request["appkey"] = params[:appkey]
    request["content-type"] = 'application/json'
    response = http.request(request)
    render :json => response.read_body and return
  end

end
