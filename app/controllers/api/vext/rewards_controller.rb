class Api::Vext::RewardsController  < ApplicationController
  # Returns rewards pushed into user’s wallet, rewards created for all users belonging to a chain
  #
  #GET /api/vext/rewards/get
  #QUERY appkey

  # http_basic_authenticate_with name: "user", password: "secret", only: [:redeem, :validate_reward]
  before_action :select_chain
  before_action :switch_params,:authenticate_new_nour_user!, except: [:redeem, :validate_reward]#, :authorize_apikey!


  def check_auth
    # authenticate_or_request_with_http_basic do |username,password|
    #   application = ApplicationKey.where(:appkey => username).first
    #
    #   p username
    #   p password
    #   p "check auth -----------------"
    #   @basic_username = username
    #   @basic_password = password
    #   olo_reward_validation =  olo_reward_setting(username,password)
    #   return true unless olo_reward_validation.blank?
    #   p application
    #   !application.blank? && password ==  Setting.olo_reward_api["nekter_secret_key"]
    # end
    authenticate_or_request_with_http_basic do |username, password|
      p username
      p password
      p "check auth Frel ENV-----------------"
      @basic_username = username
      @basic_password = password
      olo_reward_validation = olo_reward_setting(username, password)
      return true unless olo_reward_validation.blank?
      application = ApplicationKey.where(:appkey => username).first
      unless application.blank?
        @chain_id = application.application.chain_id
      end
      !application.blank? && password == Setting.olo_reward_api["secret_key_#{application.application.chain_id}"]
    end
  end

  # def authorize_apikey!
  #   p "params----before"
  #   p params
  #   check_and_build_params
  #   p "params ----after"
  #   p params
  #   unless @pos_location = RestaurantDetail.where(:olo_order_id => params["location_id"]).first#@pos_location = PosLocation.where(:apikey => params[:apikey]).first
  #     render :status => 404,
  #            :json => {status: false, notice: "Something wrong with your OLO ID"} and return
  #   end
  # end

  def authenticate_new_nour_user!
    # @user = authenticate_oauth_user!(params)
    #
    # if @user.blank?
    #   return render :json => {"status" => false, "notice" => "USER NOT FOUND ON SYSTEM"}
    # end
    # if application.nil?
    #   return render :json => {:error => "Could not find application"}
    # else
    p "user id === #{params['user_id']}"
    @user = User.find params['user_id'] rescue nil
    @user = @chain.users.joins(:user_profile).where("user_profiles.external_relevant_user_id" => params[:external_relevant_user_id]).first rescue nil
    if @user.blank?
      if params['user_id'].blank?
        return render :json => {"status" => false, "notice" => "Could not accept header !"}
      else
        return render :json => {"status" => false, "notice" => "USER NOT FOUND ON SYSTEM"}
      end
    end
    #end
  end

  # Returns rewards pushed into user’s wallet and rewards created for all users belonging to a chain
  #
  #GET /api/vext/rewards/
  #QUERY appkey
  #
  def index
    user = @chain.users.joins(:user_profile).where("user_profiles.external_relevant_user_id" => params[:external_relevant_user_id]).first rescue nil
    @user = user
    render :json => {:status => false, :notice => "We were unable to access your information. Please check entered information and try again.", :response_code => 1} and return if user.blank?

    chain_rewards= Reward.user_qualified(user.points).of_chain(@chain.id, only_active=true, only_in_effect=true, only_unexpired=true).user_qualified(user.points)
    user_rewards= Reward.of_user(user.id, include_removed=false, include_claimed=false, include_gifted=false, twenty_day_exp = true)

    rewards = (chain_rewards + user_rewards).compact  #union both
    show_gifter = @app.application_keys.where(:appkey => params[:appkey]).first.show_gifter rescue false
    show_expired_reward = @app.application_keys.where(:appkey => params[:appkey]).first.show_expired_reward rescue true
    rewards = rewards.select{|a| !a.gifter} unless show_gifter
    rewards.sort! do |a, b|
      if a.gifter
        -1
      elsif b.gifter
        1
      elsif(a.expired.eql? b.expired)
        unless a.priority_number.eql?(b.priority_number)
          (b.priority_number <=> a.priority_number)
        else
          if a.points == 0 && b.points == 0
            (a.expiryDate <=> b.expiryDate)
          else
            (a.points <=> b.points)
          end
        end
      else
        a.expired ? 1 : -1
      end
    end

    ## parameter from phone, local timestamp
    device_timestamp = params['device_timestamp']

    rewards.each_with_index do |r, i|
      r.sort_by_id= i
      r.points = r.points/(@chain.user_points || 1)
      ## reward expired based on comparing the local phone timestamp with the current reward expired
      unless device_timestamp.blank?

        ## check it if the reward has expired or no
        unless r.expiryDate.blank?
          # parse the device time stamp and do not convert to any time zone
          # example : "1/14/2015 22:00:00"  this is without timezone
          puts device_timestamp
          puts "**" * 100
          #device_date_expired = DateTime.strptime(device_timestamp)
          device_date_expired = DateTime.strptime(device_timestamp, '%m/%d/%Y %H:%M:%S')


          ## get from reward wallet expiry
          ## convert the date without timezone
          reward_date_expired = r.expiryDate
          reward_expiry_date = reward_date_expired.to_date
          reward_expiry_hour = reward_date_expired.hour
          reward_expiry_min = reward_date_expired.min
          result_reward_expiry = Time.parse(reward_date_expired.strftime('%Y-%m-%d %I:%M:%S UTC')).to_s.
          to_datetime.change(:hour => 23, :min => 59, :sec => 59)

          # compare it ----

          puts "DEVICE DATE EXPIRED  = #{device_date_expired}"
          puts "result_reward_expiry = #{result_reward_expiry}"
          #comparation = ((device_date_expired.to_datetime - result_reward_expiry.to_datetime) / 1.hour).to_i
          comparation = TimeDifference.between(device_date_expired.to_time,
           result_reward_expiry.to_time).
          in_hours

          puts comparation
          puts "-------------------------"

          if device_date_expired < result_reward_expiry
            r.device_expired = false
          elsif device_date_expired > result_reward_expiry
            r.device_expired = true
          end
          #puts comparation
          #puts "comparation"
          #puts "asdfasdfasdfadsfa"
          #puts comparation.to_i
          system_time_expired = result_reward_expiry.to_datetime.to_date + 24.hour < Date.today
          if comparation.to_i > 23 or system_time_expired#or comparation.to_i == 0
            #puts "enter wind " * 100
            ## if comparation is bigger than this. there would be a case that there user play with the time zone
            r.device_expired = r.expired
          end
        end
      else
        # using system time comparation
        r.device_expired = r.expired
      end
    end


    new_push_reward = {}
    user = @user
    locale = user.locale.key rescue "en"
    ch = user.chain
    if ch.new_reward_notif?(locale) and user.new_reward_notification == true and rewards.size > 0
      new_push_reward =  {:new_reward_notification => ch.get_reward_notif(locale)}
      ### set the notification to nil again
      user.update_column(:new_reward_notification,false)
    end

    reward_json = []

    rewards.map do |r|
      if r.reward_type == Reward::TYPES["REGULAR"]
        a = {
          :id => r.id, :name => r.name, :points => r.points, :fineprint => r.fineprint,
          :effectiveDate => without_timezone(r.effectiveDate), :expiryDate => without_timezone(r.expiryDate), :chain_id => r.chain_id,
          :survey_id => r.survey_id, :reward_type => r.reward_type,
          :expired => r.expired, :sort_by_id => r.sort_by_id, :gifter => r.gifter ,
          :image_url => r.attachment.url.include?("missing") ? "" : r.reload.attachment.url,
          :image_thumbnail_url => r.thumbnail.url.include?("missing") ? "" : r.reload.thumbnail.url,
          :additional_fields => r.additional_informations, :device_reward_expired =>  r.device_expired
        }
      else
        a = {
          :id => r.id, :name => r.name, :points => r.points, :fineprint => r.fineprint,
          :effectiveDate => without_timezone(r.effectiveDate), :expiryDate => without_timezone(r.expiryDate), :chain_id => r.chain_id,
          :survey_id => r.survey_id, :reward_type => r.reward_type,
          :expired => r.expired, :sort_by_id => r.sort_by_id, :gifter => r.gifter ,
          :image_url => r.attachment.url.include?("missing") ? "" : r.reload.attachment.url,
          :image_thumbnail_url => r.thumbnail.url.include?("missing") ? "" : r.reload.thumbnail.url,
          :new_reward => r.new_reward,
          :additional_fields => r.additional_informations, :device_reward_expired =>  r.device_expired
        }
      end

      unless show_expired_reward
        reward_json << a unless r.expired
      else
        reward_json << a
      end
    end

    render :json => {
      :status => true, balance: {:points => (user.points/(@chain.user_points || 1)),
        :milestone_points => (user.point_threshold.to_f/(@chain.user_points || 1))},
        rewards_image: @chain.rewards_image.exists? ? @chain.rewards_image.url : "",
        rewards: reward_json
        }.merge!(new_push_reward).to_json
    #
  end

  # A user claims a reward
  def olo_claim
    #begin
    confirm = params[:warn] || "false"

    user = @chain.users.joins(:user_profile).where("user_profiles.external_relevant_user_id" => params[:external_relevant_user_id]).first rescue nil
    @user = user
    render :json => {:status => false, :notice => "We were unable to access your information. Please check entered information and try again.", :response_code => 1} and return if user.blank?

    latitude= nil #params[:lat]
    longitude= nil #params[:lng]
    order_partner = params["order_partner"].upcase || "NUORDER"
    # restaurant= Restaurant.find(params[:location]) rescue nil
    # if restaurant.blank?
    #   return render :json => {:status => false, :notice => "Location Not found !"}
    # end
    # @chain = restaurant.chain
    # puts "nuorder claim #{@chain.name}"

    reward= Reward.find(params[:reward_id])

    # pos_locations = RestaurantDetail.where(:olo_order_id => params["location_id"])
    # @pos_location = nil

    # pos_locations.each do |pl|
    #   @pos_location = pl if pl.restaurant.chain_id == @chain.id
    # end

    # if @pos_location.blank?
    #   render :status => 404,
    #          :json => {status: false, notice: "Something wrong with your Location ID"} and return
    # end

    # restaurant = pos_locations.first.restaurant rescue nil
    restaurant = RestaurantDetail.find_by_id(params[:location_id]).try(:restaurant)
    render :status => 404,
    :json => {status: false, notice: "Something wrong with your Location ID"} and return if restaurant.blank?
    # restaurant  = nil
    ## if its only show the message
    if confirm == "true"
      return show_confirmation_claim
    end

    case reward.reward_type
    when Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"], Reward::TYPES["BIRTHDAY"]
      user_reward_wallets = RewardWallet.where("user_id = ? AND reward_id = ? AND date(expiry_date) >= ?", user.id, reward.id, Date.current).order("created_at asc")
      render :json => {status: false, notice: "Reward expired"} and return if user_reward_wallets.blank?
      user_reward_claimed = user_reward_wallets.select{|r| r.status == RewardWallet::STATUS[:CLAIMED]}
      user_reward_active = user_reward_wallets.select{|r| r.status == RewardWallet::STATUS[:ACTIVE] || r.status == RewardWallet::STATUS[:REDEEMING]}
      render :json => {status: false, notice: "Reward already used"} and return if (!user_reward_claimed.blank? &&  user_reward_active.blank?)
    end

    ## else claim the reward, and return the staff code and timer also
    #    begin
    if user.points >= reward.points
      p "----CLAIMED--"*30
      staffcode, length = staffcode_generate(reward, restaurant,@chain, user) #generate staffcode
      onosy_reward_claim = VextRewardClaim.create(chain_id: @chain.id, user_id: user.id, reward_id: reward.id, reward_code: staffcode, location_id: params[:location_id])

      if @chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM APP"])
        #user.claim_with_staffcode_and_timer(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
        user.claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, order_partner)
      elsif @chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
        user.claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, order_partner)
      elsif @chain.on_hybrid_setting_reward_redeem_flow?
        # if @chain.on_hybrid_setting_pos_based_rule?(restaurant)
        #   user.claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
        # elsif @chain.on_hybrid_setting_app_based_rule?(restaurant)
        #   user.claim_with_staffcode_and_timer(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
        # end
        user.claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, order_partner)
      end
      puts "aaaaa"
      p t(:claim_updated)
      p staffcode
      p reward_timer_generate(@chain)
      puts "bbbb"
      render :json => {:status => true, :notice => t(:claim_updated),
       :reward_code => staffcode.add_zero_padding(length.to_i), :reward_timer => reward_timer_generate(@chain)}
       p " -------- rendered"
     else
      p "----NOT CLAIMED--"*30
      render :json => {status: false, notice: t(:dont_have_enough_pints_reward)}
      p "------ rendered"
    end
    #rescue => e
    #  puts "error ------------ #{e.inspect}"
    #  render :json => {status: false, notice: t(:reward_claim_error)}
    #end

    #    rescue Exceptions::InsufficientUserPointsError
    #      render :json => {status: false, notice: t(:dont_have_enough_pints_reward)}
    #    rescue
    #      render :json => {status: false, notice: t(:cant_submit_request)}
    #    ensure
    #this_code_will_execute_always()
    #    end
  end

  def test_locate
  end

  def test_claim
  end

  def validate_reward
    user = @chain.users.joins(:user_profile).where("user_profiles.external_relevant_user_id" => params[:external_relevant_user_id]).first rescue nil
    render :json => {:status => false, :notice => "We were unable to access your information. Please check entered information and try again.", :response_code => 1} and return if user.blank?
    unless @pos_location = RestaurantDetail.where(:olo_order_id => params["location_id"]).first
      render :status => 404,
      :json => {status: false, notice: "Something wrong with your OLO ID"} and return
    end

    chain = @chain rescue nil
    location = @pos_location.restaurant
    chain = location.chain
    reward_ids = chain.rewards.map(&:id)
    reward_code = params["reward_code"].to_s.upcase

    reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"

    reward_code_setting = JSON.parse(reward_code_setting) rescue nil
    reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
    if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
      if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
        reward_code = reward_code.to_ean_13_format
      end
    end
    reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
    pos_reward_check = PosRewardCheck.new(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params["check"], :reward_code => reward_code)
    reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => reward_code, :active => true).first
    if reward_code_user.blank?
      render :json => {:message => "failure"} and return
    end
    reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", reward_code, reward_ids)
    if reward_transactions.blank?
      render :json => {:message => "failure"} and return
    end

    reward_code_expire = reward_code_setting["timer"].to_i
    if reward_code_expire != 0
      reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
    else
      reward_transaction = reward_transactions.order("created_at DESC").first
    end
    if reward_transaction.blank?
      render :json => {:message => "failure"} and return
    end

    #render :json => {:message => "success"} and return
    #xml_in_nokogiri_format = Nokogiri::XML(params[:check])
    p "request.content_type"
    p request.content_type
    p "request.content_type #{request.content_type}"
    p "----------------"
    xml_receipt = params["check"]

    # check params first.

    if params["check"].class == String
      check_to_hash =  YAML.load params["check"].gsub(/=>/, ': ')
    else params["check"].class == Hash
      check_to_hash = params["check"]
    end

    puts params["check"].class
    puts "aaaaaaaaaaaaaaaaaaaaaaa #{params["check"]}"

    olo_check = OloWebhook::Parser.new(check_to_hash)
    p "here is check id and seq number -----"
    puts check_to_hash
    check_id = check_to_hash['orderId']
    if check_id.blank?
      check_id = "#{Time.now.strftime('%H%m')}#{@pos_location.id}"
    end
    check_id = check_id.to_s
    seq_number = "olo"

    items_discount = []
    reward_transaction.update_column(:check_number, [check_id, seq_number, @pos_location.apikey].join("-"))
    reward = reward_transaction.reward
    user = reward_transaction.user
    reward_general_menu_item_ids = reward.general_menu_items.map(&:id)
    reward_menu_items = reward.reward_menu_items
    required_menu_items = reward_menu_items.select{|a| a.required }
    optional_menu_items = reward_menu_items.select{|a| !a.required }
    items_check_buy = []
    xml_menu_items = olo_check.menu_items_list
    parent_qualified = true
    xml_menu_items.each do |menu_item|
      item = {}
      item[:menu_item_id] = menu_item[:product_id] rescue 0

      item[:name] = menu_item[:description] rescue nil

      item[:quantity] = menu_item[:quantity].to_i rescue 0

      item[:total_amount] = menu_item[:total_amount].to_f rescue 0


      item[:price_per_item] = menu_item[:price].to_f rescue 0

      # item[:level_size_id] = menu_item.search("size-id").text rescue nil
      # item[:level_size_id] = menu_item["SizeId"].to_i  if item[:level_size_id].blank? &&  !menu_item["SizeId"].blank?
      # item[:level_size_id] = nil if item[:level_size_id].blank?

      p "---- item number  #{item[:menu_item_id]}"
      p "------- chain = #{chain.id}--"
      p "=------ pos location = #{@pos_location.id}"
      #arr_general_menu_item_id = GeneralMenuItemRangeValue.where("start_value <= #{item[:menu_item_id]} and end_value >= #{item[:menu_item_id]} and chain_id = #{chain.id}").map(&:general_menu_item_id)
      arr_general_menu_item_id = []
      pos_menu_items = PosMenuItem.select("id").where(:item_number_olo => item[:menu_item_id], :chain_id => chain.id, :level_size_id => item[:level_size_id])
      p "--- pos menu items = #{pos_menu_items.count}"
      pos_menu_items.each do |pos_menu_item|
        arr_general_menu_item_id << (pos_menu_item.general_menu_items.map(&:id) rescue [])
        break unless arr_general_menu_item_id.blank?
      end
      arr_general_menu_item_id = arr_general_menu_item_id.flatten
      item[:general_menu_item_id] = arr_general_menu_item_id.flatten

      item[:required_menu_item] = !(item[:general_menu_item_id] & required_menu_items.map(&:general_menu_item_id)).blank?
      if item[:general_menu_item_id].blank? && !item[:required_menu_item]
        parent_qualified = false
      elsif !item[:general_menu_item_id].blank? && item[:required_menu_item]
        parent_qualified = true
      end
      items_check_buy << item if parent_qualified
    end

    grouped_items_check_buy = []
    items_check_buy.each_with_index do |item_check, i|
      if item_check[:required_menu_item]
        item_check[:all_price_with_optional_item] = item_check[:price_per_item]
        menu_item_combo_sum = [item_check[:menu_item_id]]
        p '----'
        p item_check[:menu_item_combo_sum]
        (i+1).upto(items_check_buy.length-1) do |idx|
          optional_item = items_check_buy[idx]
          unless optional_item[:required_menu_item]
            item_check[:all_price_with_optional_item] += optional_item[:price_per_item]
            menu_item_combo_sum << optional_item[:menu_item_id]
          else
            break
          end
        end
      end
      item_check[:menu_item_combo_sum] = menu_item_combo_sum.join("-") if menu_item_combo_sum
      grouped_items_check_buy << item_check
    end

    if chain.pos_discount_setting.eql?(Chain::POS_DISCOUNT_TYPE["Discount Least priced qualifying item"])
      ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| a[:all_price_with_optional_item] <=> b[:all_price_with_optional_item]}
    else
      ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| b[:all_price_with_optional_item] <=> a[:all_price_with_optional_item]}
    end

    pos_discount_type = reward.pos_discount_type
    #non menu item discount based
    if pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?
      # if request.content_type == "application/json" && xml_receipt.class.to_s != "String"
      #   pos_check = params["check"]
      #   check_subtotal = pos_check["Subtotal"]
      # else
      #   pos_xml = PosReceiptXml.new(params["check"])
      #   check_subtotal = pos_xml.sub_total
      # end

      check_subtotal = olo_check.get_subtotal
      puts "Check subtotal olo === #{check_subtotal}"

      total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", check_id, seq_number, @pos_location.restaurant_id])
      subtotal_after_discount = check_subtotal - total_discount_registered

      puts "Subtotal after discount olo ===  #{subtotal_after_discount}"
      if subtotal_after_discount > 0
        discount_applied = subtotal_after_discount > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : subtotal_after_discount
        # reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
        # RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
        # pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
        # pos_reward_check.user_id = (user.id rescue nil)
        # pos_reward_check.save

        #Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction)) if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
        if chain.auto_loyalty && due_total == 0
          #xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, discount_applied)
          #Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
        end
        render :json => {:status => true, :discount => (discount_applied*-1), :message => "success"} and return
      else
        # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
        # pos_reward_check.user_id = (user.id rescue nil)
        # pos_reward_check.error_description = "Reward requirement is not met. subtotal after discount"
        # pos_reward_check.save
        # #reward_transaction.update_column(:pos_used, true)
        render :json => {:status => false,  :message => "Reward requirement is not met."} and return
      end
    end

    #what discount that reward offered
    required_menu_items.each do |reward_general_menu_item|
      #reward_general_menu_item_ids.each do |reward_general_menu_item_id|
      ordered_item_check_buy.each do |filtered_item|
        if filtered_item[:general_menu_item_id].include?(reward_general_menu_item.general_menu_item_id)
          reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]).count
          qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
          if reward_discount_count < qty_selected
            items_discount << {:menu_item_id => filtered_item[:menu_item_id], :price => filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]}
            #CODE FOR OPTIONAL MENU ITEM
            optional_menu_discount_items = []
            optional_menu_items.each do |opt_reward_general_menu_item|
              (grouped_items_check_buy.index(filtered_item)+1).upto(grouped_items_check_buy.length-1) do |idx|
                opt_filtered_item = grouped_items_check_buy[idx] rescue nil
                break if opt_filtered_item.blank?
                break if opt_filtered_item[:required_menu_item]
                if opt_filtered_item[:general_menu_item_id].include?(opt_reward_general_menu_item.general_menu_item_id)
                  reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => opt_filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]).count
                  qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
                  if reward_discount_count < qty_selected
                    #optional_menu_discount_items <<  opt_filtered_item
                    items_discount << {:menu_item_id => opt_filtered_item[:menu_item_id], :price => opt_filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]}
                  end
                end
              end
            end
            break
          end
        end
      end
    end

    #preset item discount if there not suitable combo
    items_discount = [] if !items_discount.blank? && !(required_menu_items.count <= items_discount.count)

    unless items_discount.blank?
      #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
      #reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
      #reward_code_user.destroy if reward_code_user
      p "here are the items that got the discount"
      p items_discount
      all_discount_item = items_discount.sum{ |a| a[:price]}
      if pos_discount_type.blank?
        calculated_discount_from_reward = all_discount_item
      elsif pos_discount_type.is_discount_percentage?
        calculated_discount_from_reward = ((pos_discount_type.discount_amount.to_f/100) * all_discount_item).round(2)
      elsif pos_discount_type.is_discount_price?
        calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? (all_discount_item - pos_discount_type.discount_amount) : opt_filtered_item[:price_per_item]).round(2)
      elsif pos_discount_type.is_discount_fixed_price?
        calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : all_discount_item).round(2)
      end
      reward_discount_tmp = []
      items_discount.each do |item|
        #RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => item[:price], :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum])
        reward_discount_tmp << {:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => item[:price],
          :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum]
        }
      end
      if chain.auto_loyalty && due_total == 0
        #xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, calculated_discount_from_reward)
        #Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
      end
      render :json => {:status => true, :discount => (calculated_discount_from_reward*-1), :message => "success"} and return
    else
      render :json => {:status => false, :discount => nil, :message => "Reward requirement is not met."} and return
    end
  end

  def redeem
    render :json => {:status => false, :notice => "Please complete all required fields with valid information"} and return if params[:external_relevant_user_id].blank? || params[:reward_code].blank? || params[:location_id].blank? || params[:redemption_type].blank?
    user = @chain.users.joins(:user_profile).where(:external_relevant_user_id => params[:external_relevant_user_id]).first rescue nil
    render :json => {:status => false, :notice => "We were unable to access your information. Please check entered information and try again.", :response_code => 1} and return if user.blank?

    reward_claim = VextRewardClaim.where(chain_id: @chain.id, user_id: user.id, reward_code: params[:reward_code], location_id: params[:location_id]).last
    render :json => {:status => false, :notice => "Reward code invalid", :response_code => 1} and return if reward_claim.blank?

    restaurant_detail = RestaurantDetail.where(external_location_store_id: params[:location_id]).first  rescue nil
    restaurant= restaurant_detail.restaurant rescue nil
    render :json => {:status => false, :notice => "Invalid Location."} and return if restaurant.blank?
    render :json => {status: false, notice: "Restaurant is not active"} and return unless restaurant.status
    if restaurant.status && [Restaurant::TYPES["COMING_SOON"]].include?(restaurant.restaurant_detail.status)
      render :json => {status: false, notice: "Restaurant is not active", :response_code => 1} and return
    end if restaurant.restaurant_detail

    reward= Reward.find(reward_claim.reward_id)
    case reward.reward_type
    when Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"], Reward::TYPES["BIRTHDAY"]
      user_reward_wallets = RewardWallet.where("user_id = ? AND reward_id = ? AND date(expiry_date) >= ?", user.id, reward.id, Date.current).order("created_at asc")
      render :json => {status: false, notice: "Reward expired"} and return if user_reward_wallets.blank?
    end

    if params[:redemption_type].to_i == 0
      if params[:warn] == "true"
        return show_confirmation_claim
      end

      if user.points >= reward.points
        render :json => {status: false, notice: t(:dont_have_enough_pints_reward), :response_code => 1} and return if @chain.chain_setting.hybrid_milestone_regular_rewards_enabled && (user.point_threshold || 0) < reward.points
        p "----ONOSYS CLAIMED--"*30
        reward_transaction = RewardTransaction.find(reward_claim.reward_transaction_id)
        if @chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (@chain.on_hybrid_setting_reward_redeem_flow?)
          Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
        end
        render :json => {:status => true, :notice => t(:claim_updated), :response_code => 0}
      else
        p "----ONOSYS NOT CLAIMED--"*10
        render :json => {status: false, notice: t(:dont_have_enough_pints_reward), :response_code => 1}
      end
    elsif params[:redemption_type].to_i == 1
      p "----ONOSYS VOIDS CLAIMED--"*10
      t_id = reward_claim.reward_transaction_id
      render :json => {:status => false, :response_code => 0 , :notice => "Could not find the transaction !!"} and return if t_id.blank?
      reward_transaction = RewardTransaction.find(t_id)
      render :json => {:status => false, :response_code => 0 , :notice => "Could not find the transaction !!"} and return if reward_transaction.blank?

      user = User.find reward_transaction.user_id
      rt = reward_transaction
      reward = rt.reward
      unless rt.blank?
        reward_wallet = RewardWallet.where(:user_id => user.id, :reward_id => reward.id).first
        p "reward_wallet #{reward_wallet}"
        render :json => {status: false, notice: "Can't Void reward, status not meet."} and return if reward_wallet.status != 3

        user.earn(reward.points) rescue nil
        PointHistory.add_to_history(user.id, "Void Redeem for ##{reward.name}", (reward.points * 1)) rescue nil
        reward_wallet.update_attribute(:status, RewardWallet::STATUS[:ACTIVE]) rescue nil
        rt.update_column(:pos_used, false) rescue nil
        return render :json => {:status => true,  :response_code => 1, :notice => "Void SUCCESS"}
      else
        return render :json => {:status => false, :response_code => 0 , :notice => "User Not Found On System"}
      end
    end

  end

  private

  def select_chain
    application_key = ApplicationKey.where(:appkey => params[:appkey]).first
    @chain = application_key.try(:application).try(:chain)
    render :json => {:status => false, :notice => "Chain Not Found !"} and return false if @chain.nil?

    if ChainSetting::EXTERNAL_PARTNERS["#{params[:order_partner].camelize}"] !=  @chain.chain_setting.external_partner
      render :json => {:status => false, :notice => "Order Partner Not Found !"} and return false
    end

  end

  def show_confirmation_claim
    question = "Are You sure?"
    body = "Please do NOT click 'Confirm' until you are in front of our staff. Voucher valid for 2 minutes only."
    ## get the warning based on user locale
    begin
      key = current_user.locale.key
      content = REDIS.hget "chain_#{@chain.id}_warning_claim", key+"_warning_claim"
      content = JSON.parse(content)
      question = content["question"]
      body = content["body"]
    rescue => e
      puts "API:RewardController::show_confirmation_claim = #{e.inspect}"
    end
    render :json => {:warn_tile => question,:warn_body =>  body}
  end

  def staffcode_generate(reward, restaurant,chain, user)
    if !chain.use_generated_code && reward.reward_pos_codes.count > 0
      #selected = reward.reward_pos_codes.where("user_id IS NULL").order("RANDOM()").first rescue nil
      selected = reward.reward_pos_codes.where("user_id IS NULL").limit(100).sample(1).first rescue nil
      unless selected.blank?
        selected.update_attributes(:user_id => current_user.id, :used_at => Time.current, :restaurant_id => (restaurant.id rescue nil))
        selected.code
      else
        "NOCODE"
      end
    elsif !chain.use_generated_code && reward.reward_pos_codes.count == 0
      "NOCODE"
    else
      if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
        Reward.pos_claim_staffcode_generate(chain, user)
      elsif chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM APP"])
        #Barcode.generate_reward_code(chain)
        Reward.pos_claim_staffcode_generate(chain, user)
      elsif @chain.on_hybrid_setting_reward_redeem_flow?
        if chain.on_hybrid_setting_pos_based_rule?(restaurant)
          Reward.pos_claim_staffcode_generate(chain, user)
        elsif chain.on_hybrid_setting_app_based_rule?(restaurant)
          #Barcode.generate_reward_code(chain)
          Reward.pos_claim_staffcode_generate(chain, user)
        end
      end
    end
  end

  def reward_timer_generate(chain)
    c = Chain.find(chain.id)
    c.timer
  end

  def without_timezone(date)
    date.strftime('%Y-%m-%dT23:59:59 +0000') if date
  end

  def olo_reward_setting(username,pwd)
    olo_reward_setting = OlorewardsSetting.where(:basic_auth => username, :basic_pwd => pwd).first
    @chain_id = olo_reward_setting.chain_id unless olo_reward_setting.blank?
  end

  def switch_params
    params['user_id'] = params['external_relevant_user_id']
  end
end
