class Api::Vext::UsersController  < Api::VextBaseController

  # Check what the partner value is
  # If the partner is nuorder, set the user to be a NuOrder user.
  # Check if Relevant account exists with the email for the chain and if the user is same as external partner value.
  # If yes
  # Save /update the nuOrder patron ID under Relevant account
  # Update the Relevant user meta data by fetching the user information from patron id( using GET Patron API call )
  # Create/ Update Relevant   profile and echo  external session data

  # If no
  # Get user details like phone number, dob, address etc by posting the get  nuORder patron details
  # Create Relevant account using the email and temp  password and other input fields.
  # Save /update the nuORder patron ID under Relevant account
  # Sign up the user with Relevant.
  # Generate an external Relevant User ID and save under Relevant user profile.

  def external_customer_connect
    if ChainSetting::EXTERNAL_PARTNERS["#{params[:order_partner].camelize}"] !=  @chain.chain_setting.external_partner
      return render :json => {:status => false, :notice => "Order Partner Not Found !"}
    end

    user                    = @chain.users.where(email: params[:email]).first
    nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
    nuorder_user            = Nuorder::Api::User.new(nuorder_connect_setting)
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if nuorder_connect_setting.blank?
    success, search_json = nuorder_user.get_profile(params[:patron_id])
    if success
      if user && !params[:patron_id].nil?
        if user.user_profile
          user.user_profile.update_column("patron_id", search_json["patron"]["id"])
          user.user_profile.update_column("external_relevant_user_id", SecureRandom.uuid) if user.user_profile.try(:external_relevant_user_id).blank?
        else
          UserProfile.create(user_id: user.id, patron_id: search_json["patron"]["id"], external_relevant_user_id: SecureRandom.uuid, password_hint: params[:password_hint])
        end
      else
        device_type = @application.device_type.to_i - 1
        type = DEVICE_TYPE_SELECT.keys.map{|a| a.parameterize("_")}[device_type] rescue '-'
        params_user = {
          user: {
            :first_name =>  search_json["patron"]["first_name"],
            :last_name  =>  search_json["patron"]["last_name"],
            :username   =>  search_json["patron"]["first_name"]+search_json["patron"]["last_name"],
            :email      =>  search_json["patron"]["patron_login"],
            :chain_id   =>  @chain.id,
            :phone_number => search_json["patron"]["patron_phone1"],
            :register_type => 1,
            :register_device_type => type,
            :locale_id => Locale.find_by_key(params[:locale] ||= 'en').try(:id)

            # :dob_day => params[:dob_day
            # :dob_month => params[:dob_month],
            # :dob_year => params[:dob_year]
          }
        }

        user            = User.new(params_user[:user])
        password        = user.random_password
        user.password   = password
        user.password_confirmation  = password
        user.build_user_profile(:password_hint => search_json["patron"]["password_hint"], :patron_id => search_json["patron"]["id"], external_relevant_user_id: SecureRandom.uuid)
        user.skip_validations       = true
        if user.save
          user.reload
          user.reset_authentication_token!
          #MOVED TO DELAYED JOB
          Delayed::Job.enqueue(UserSignupJob.new(@chain, user, params_user[:user]))
        else
          render :json => {:status => false, :notice => user.errors.messages } and return
        end
      end
    else
      render :json => {:status => false, :message => "We were unable to access your information. Please try again." } and return
    end

    if !user.blank?
      render :json => {
        :status => true,
        :message  => "Welcome back!",
        :external_relevant_user_id => user.user_profile.external_relevant_user_id,
        :auth_token => user.authentication_token
      }
    else
      render :json => {:status => false, :message => "__FAILED_MESSAGE__"}
    end
  end

  def onlineorder
    relevant_external_id = params['relevant_external_user_id']
    location_id = params['location_id']
    order_id = params['order_id']
    order = params['order']
    order_partner = params['order_partner']

    if relevant_external_id.blank? || location_id.blank? || order_id.blank? || order.blank? || order_partner.blank?
      render :json => {:status => false, :description => "__FAILED_MESSAGE__"}
    end
    order = nil
    # find restaurant first
    restaurant = Restaurant.first
    render :json => {:status => false, :description => "Invalid Location."} and return if restaurant.blank?
    # get the offer
    offer = restaurant.first_active_offer
    render :json => {:status => 1, :resultDescription => "Invalid Offer."} and return if offer.blank?

    if !order.blank?
      render :json => {:status => 0, :description  => "__SUCCESS_MESSAGE__", :loyalty_order_id => "__LOYALTY_ID__"}
    else
      render :json => {:status => false, :description => "__FAILED_MESSAGE__"}
    end
  end
end
