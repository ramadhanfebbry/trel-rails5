class Api::Vnuor::UserController < Api::VnourBaseController

  def connect_setting
    @nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Chain does not have NuOrder connect setting"} and return if @nuorder_connect_setting.blank?

    if @nuorder_connect_setting
      render :json => {:status => true, :nuorder_connect_setting => @nuorder_connect_setting}
    else
      render :json => {:status => false, :message => "Chain does not have NuOrder connect setting" }
    end
  end

  def customer_connect
    p params
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    params[:password] = URI.decode(params[:password]) rescue ""
    params[:security_question] = URI.decode(params[:security_question]) rescue ""
    params[:security_answer] = URI.decode(params[:security_answer]) rescue ""
    params[:email] = params[:email].try(:downcase)

    if params[:connect_type].eql?("1")
      render :json => {:status => false, :message => "first name must more than 1 characters"} and return if params[:first_name].length < 2
      render :json => {:status => false, :message => "last name must more than 1 characters"} and return if params[:last_name].length < 2
    end

    params[:user] = {:first_name=>params[:first_name],
                     :last_name=>params[:last_name],
                     :username=>params[:username],
                     :email=>params[:email],
                     :password => params[:password],
                     :latitude=>params[:latitude],
                     :longitude=>params[:longitude],
                     :chain_id=>@chain.id,
                     :register_type=>params[:register_type],
                     :register_device_type=>((params[:sign_in_device_type] || params[:register_device_type]).downcase.tr(' ', '_') rescue nil),
                     :device_id => params[:device_id],
                     :ref_code => params[:referral_code],
                     :phone_number => params[:phone_number],
                     :dob_day => params[:dob_day],
                     :dob_month => params[:dob_month],
                     :dob_year => params[:dob_year],
                     :special_occassion => params[:special_occassion],
                     :zipcode => params[:zipcode],
                     :name => [params[:first_name], params[:last_name]].join(" "),
                     :marketing_optin => User.marketing_optin_value(params[:marketing_optin]),
                     :security_question => params[:security_question],
                     :security_answer => params[:security_answer],
                     :favorite_location => params[:favorite_location],
                     :special_occassion => params[:special_occassion],
                     :address1 => params[:address1],
                     :address2 => params[:address2],
                     :city => params[:city],
                     :state => params[:state],
                     :marketing_optin_texting => params[:marketing_optin_texting],
                     :device_token => params[:device_token]

    }
    params[:user][:chain_id] = @chain.id
    params[:user][:locale_id] = Locale.find_by_key(params[:locale] ||= 'en').try(:id)
    nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if nuorder_connect_setting.blank?

    case params[:connect_type]
      when "1"
        users = User.unscoped.where("email = '#{params[:email]}' AND active IS TRUE and deleted_at IS NULL AND chain_id = #{@chain.id}").order("current_sign_in_at DESC")
        user_check = users.first
        if user_check.blank?
          user_check = User.new(params[:user])
        else
          user_check.attributes = params[:user].reject { |key,value| value.nil? }
        end
        user_check.required_domain_email_validation = @chain.chain_setting.domain_email_validation
        user_check.skip_validations = true
        render :json => {:status => false, :notice => "Password must be 6 characters or longer"} and return unless params[:password].length >= 6
        if !params[:favorite_location].blank?
          rest = @chain.restaurants.where("id = ? ", params[:favorite_location]) rescue []
          render :json => {:status => false, :notice => "Favorite Location not valid"} and return if rest.blank?
        end

        valid = user_check.valid?
        if !valid && !params[:phone_number].blank? && !users.select{|a| a.phone_number == params[:phone_number]}.blank?
          user_check.errors.messages.delete(:phone_number)
        end
        unless user_check.errors.messages.blank?
          p "user is not valid ------"
          p  user_check.errors
          custom_error = []
          if !user_check.errors.messages[:email].blank? && user_check.errors.messages[:email].first == "Please insert a valid email address"
            custom_error <<  "Please enter a valid email address."
          end
          if !user_check.errors.messages[:phone_number].blank? && user_check.errors.messages[:phone_number].first == "Please enter a valid 10 digit phone number"
            custom_error << "Please enter a valid phone number."
          end
          if !user_check.errors.messages[:phone_number].blank? && user_check.errors.messages[:phone_number].first == "This phone number is already taken. Please use the Login button to continue, or use a secondary phone number to Sign Up"
            custom_error << "The phone number you entered is already in use. Please enter a new phone number."
          end
          render :json => {:status => false, :notice => custom_error.blank? ? "Please complete all required fields with valid information" : custom_error.join(", "), :dev_message => user_check.errors.messages} and return
        end

        nuorder_user = Nuorder::Api::User.new(nuorder_connect_setting)
        success, message, json = nuorder_user.create(params)
        if success
          user = user_check
          if !user.id.blank?
            user.save
            if user.user_profile
              user.user_profile.update_column("patron_id", json["patron"]["id"]) if user.user_profile.try(:patron_id).blank?
              user.user_profile.update_column("external_relevant_user_id", SecureRandom.uuid) if user.user_profile.try(:external_relevant_user_id).blank?
              user.user_profile.update_column("password_hint", params[:password_hint]) if params[:password_hint].present? && user.user_profile.try(:password_hint).blank?
            else
              UserProfile.create(user_id: user.id, patron_id: json["patron"]["id"], external_relevant_user_id: SecureRandom.uuid, password_hint: params[:password_hint])
            end
            user.reload
            user.reset_authentication_token!
            return render :json => {:status => true, :auth_token => user.authentication_token,
                                    :message => message, :relevant_user_id => user.user_profile.try(:external_relevant_user_id),
                                    :first_name => user.try(:first_name), :last_name => user.try(:last_name),
                                    :phone_number => user.try(:phone_number)
            }
          else
            user = User.new(params[:user])
            user.skip_validations = true
            if user.save
              if user.user_profile
                user.user_profile.update_column("patron_id", json["patron"]["id"]) if user.user_profile.try(:patron_id).blank?
                user.user_profile.update_column("external_relevant_user_id", SecureRandom.uuid) if user.user_profile.try(:external_relevant_user_id).blank?
                user.user_profile.update_column("password_hint", params[:password_hint]) if params[:password_hint].present? && user.user_profile.try(:password_hint).blank?
              else
                UserProfile.create(user_id: user.id, patron_id: json["patron"]["id"], external_relevant_user_id: SecureRandom.uuid, password_hint: params[:password_hint])
              end
              user.reload
              user.reset_authentication_token!
              #MOVED TO DELAYED JOB
              Delayed::Job.enqueue(UserSignupJob.new(@chain, user, params))
              return render :json => {:status => true, :auth_token => user.authentication_token,
                                      :message => message, :relevant_user_id => user.user_profile.try(:external_relevant_user_id),
                                      :first_name => user.try(:first_name), :last_name => user.try(:last_name),
                                      :phone_number => user.try(:phone_number)
              }
            else
              render :json => {:status => false, :notice => user.errors.messages.join(", ")} and return
            end
          end
        else
          render :json => {:status => false, :notice => message} and return
        end
      when "2"
        user_check = User.unscoped.where("email = '#{params[:email]}' AND active IS TRUE and deleted_at IS NULL AND chain_id = #{@chain.id}").order("current_sign_in_at DESC").first
        p user_check
        if user_check.blank?
          user_check = User.new(params[:user])
        else
          params[:user].reject! {|key, value| key == "marketing_optin"} if params[:marketing_optin].blank?
          params[:user].reject! {|key, value| key == "marketing_optin_texting"} if params[:marketing_optin_texting].blank?
          user_check.attributes = params[:user].reject { |key,value| value.nil? }
        end

        nuorder_user = Nuorder::Api::User.new(nuorder_connect_setting)
        success, message, json = nuorder_user.authenticate(params)
        if success
          search_success, search_json = nuorder_user.get_profile(json["service_response"]["patron_id"])
          user = user_check
          if !user.id.blank?
            user.save
            if user.user_profile
              user.user_profile.update_column("patron_id", search_json["patron"]["id"]) if user.user_profile.try(:patron_id).blank?
              user.user_profile.update_column("external_relevant_user_id", SecureRandom.uuid) if user.user_profile.try(:external_relevant_user_id).blank?
              user.user_profile.update_column("password_hint", search_json["patron"]["password_hint"]) if user.user_profile.try(:password_hint).blank?
            else
              UserProfile.create(user_id: user.id, patron_id: search_json["patron"]["id"], external_relevant_user_id: SecureRandom.uuid, password_hint: search_json["patron"]["password_hint"])
            end
            user.reload
            user.reset_authentication_token!
          else
            user.first_name = search_json["patron"]["first_name"]
            user.last_name = search_json["patron"]["last_name"]
            user.phone_number = search_json["patron"]["patron_phone1"] || search_json["patron"]["patron_phone2"]
            user.save
            if user.user_profile
              user.user_profile.update_column("patron_id", search_json["patron"]["id"]) if user.user_profile.try(:patron_id).blank?
              user.user_profile.update_column("external_relevant_user_id", SecureRandom.uuid) if user.user_profile.try(:external_relevant_user_id).blank?
              user.user_profile.update_column("password_hint", search_json["patron"]["password_hint"]) if user.user_profile.try(:password_hint).blank?
            else
              UserProfile.create(user_id: user.id, patron_id: search_json["patron"]["id"], external_relevant_user_id: SecureRandom.uuid, password_hint: search_json["patron"]["password_hint"])
            end
            user.reload
            user.reset_authentication_token!
          end
          return render :json => {:status => true, :auth_token => user.authentication_token,
                                  :message => message, :relevant_user_id => user.user_profile.try(:external_relevant_user_id),
                                  :first_name => user.try(:first_name), :last_name => user.try(:last_name),
                                  :phone_number => user.try(:phone_number)
          }
        else
          render :json => {:status => false, :notice => message} and return
        end
    end
  end

  def get_profile
    user = current_user
    if user.chain_id == @chain.id
      patron_id = user.user_profile.try(:patron_id)
      render :json => {:status => false, :message => "We were unable to access your information. Please try again."} and return if patron_id.blank?
      nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
      render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if nuorder_connect_setting.blank?

      nuorder_user = Nuorder::Api::User.new(nuorder_connect_setting)
      success, message = nuorder_user.get_profile(patron_id)
      if success
        render :json => {status: true, user: user.as_json(:methods => [:profile_pic, :favorite_location_name]).merge(
            :milestone_points => user.user_milestone_points,
            :weekly_receipt_approved => user.approved_receipt_count("weekly"),
            :monthly_receipt_approved => user.approved_receipt_count("monthly"),
            :yearly_receipt_approved => user.approved_receipt_count("yearly"),
        ), patron: message["patron"]} and return
      else
        render :json => {:status => false, :message => message } and return
      end
    else
      render :json => {status: false, notice: t(:user_details_not_found)}
    end
  end

  def search_profile
    p params
    render :json => {:status => false, :notice => "Please insert a valid one or more criteria."} and return if params[:patron_id].blank? && params[:phone_number].blank? && params[:email].blank? && params[:loyalty_number_id].blank?
    nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if nuorder_connect_setting.blank?

    nuorder_user = Nuorder::Api::User.new(nuorder_connect_setting)
    success, message = nuorder_user.search_profile(params)
    if success
      render :json => {:status => true, :patrons_info => message } and return
    else
      render :json => {:status => false, :patrons_info => message } and return
    end
  end

  def forgot_password
    render :json => {:status => false, :notice => "Please insert a valid email address."} and return if params[:email].blank?
    p "------before--"
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    p "------after--"
    p params
    nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if nuorder_connect_setting.blank?

    user = @chain.users.where(:email => params[:email]).first rescue nil
    patron_id = user.try(:user_profile).try(:patron_id)
    render :json => {:status => false, :message => "We were unable to access your information. Please check entered information and try again."} and return if patron_id.blank?

    nuorder_user = Nuorder::Api::User.new(nuorder_connect_setting)
    success, message = nuorder_user.forgot_password(params)
    if success
      render :json => {:status => true, :message => message } and return
    else
      render :json => {:status => false, :message => "We were unable to access your information. Please check entered information and try again." } and return
    end
  end

  def update_password
    p "before---"
    p params
    params[:old_password] = URI.decode(params[:old_password]) rescue ""
    params[:new_password] = URI.decode(params[:new_password]) rescue ""
    params[:confirm_password] = URI.decode(params[:confirm_password]) rescue ""
    p "after---"
    p params

    new_password = params[:new_password]
    confirm_password = params[:confirm_password]
    render :json => {:status => false, :message => "Please complete all required fields with valid information"} and return if new_password.blank? || confirm_password.blank? || !new_password.eql?(confirm_password)
    render :json => {:status => false, :message => "Password must be 6 characters or longer"} and return unless new_password.length >= 6

    nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if nuorder_connect_setting.blank?

    patron_id = current_user.try(:user_profile).try(:patron_id) rescue nil
    render :json => {:status => false, :message => "We were unable to update your information. Please try again."} and return if patron_id.blank?
    render :json => {:status => false, :message => "We were unable to update your information. Please try again."} and return if !current_user.valid_password?(params[:old_password])

    nuorder_user = Nuorder::Api::User.new(nuorder_connect_setting)
    success, message = nuorder_user.update_password(patron_id, params)
    if success
      if current_user.reset_password!(new_password, confirm_password)
        render :json => {:status => true, :message => message } and return
      else
        render :json =>{:status=>false, notice: t(:error_updating_password)} and return
      end
    else
      render :json => {:status => false, :message => message } and return
    end
  end

  def profile
    user = current_user
    user.email = params[:email] unless params[:email].blank?
    user.first_name = params[:first_name] unless params[:first_name].blank?
    user.last_name = params[:last_name] unless params[:last_name].blank?
    user.phone_number = params[:phone_number]
    user.favorite_location = params[:favorite_location]
    user.dob_day = params[:dob_day]
    user.dob_month = params[:dob_month]
    user.dob_year = params[:dob_year]

    if !params[:favorite_location].blank?
      rest = @chain.restaurants.where("id = ? ", params[:favorite_location]) rescue []
      render :json => {:status => false, :notice => "Favorite Location not valid"} and return if rest.blank?
    end

    if params[:email].present?
      exist_email = @chain.users.find_by_email(params[:email])
      render :json => {:status => false, :message => "We are Sorry! The email is already associated with an account."} and return if exist_email.present? && exist_email.id != user.id
    end
    if params[:phone_number].present?
      exist_phone = @chain.users.find_by_phone_number(params[:phone_number])
      render :json => {:status => false, :message => "We are Sorry! The phone number is already associated with an account."} and return if exist_phone.present? && exist_phone.id != user.id
    end

    render :json => {:status => false, :message => "first name must more than 1 characters"} and return if params[:fname].present? && params[:fname].length < 2
    render :json => {:status => false, :message => "last name must more than 1 characters"} and return if params[:lname].present? && params[:lname].length < 2

    patron_id = user.user_profile.try(:patron_id)
    render :json => {:status => false, :message => "We were unable to access your information. Please try again."} and return if patron_id.blank?

    nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if nuorder_connect_setting.blank?

    nuorder_user = Nuorder::Api::User.new(nuorder_connect_setting)
    success, message = nuorder_user.update_profile(patron_id, params)
    if success && user.save
      user.user_profile.update_column("password_hint", params[:password_hint]) if params[:password_hint].present?
      render :json => {:status => true, :message => message } and return
    else
      render :json => {:status => false, :message => message } and return
    end
  end

  def password_hint
    render :json => {:status => false, :notice => "Please insert a valid email address."} and return if params[:email].blank?
    p "------before--"
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    p "------after--"
    p params
    user = @chain.users.find_by_email(params[:email])
    render :json => {:status => false, :message => "We were unable to access your information. Please try again."} and return if user.blank?

    patron_id = user.user_profile.try(:patron_id)
    render :json => {:status => false, :message => "We were unable to access your information. Please try again."} and return if patron_id.blank?

    nuorder_connect_setting = NuorderConnectSetting.get_nuorder_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if nuorder_connect_setting.blank?

    nuorder_user = Nuorder::Api::User.new(nuorder_connect_setting)
    success, search_json = nuorder_user.get_profile(patron_id)
    if success
      render :json => {:status => true, :hint => search_json["patron"]["password_hint"], :message => "success" } and return
    else
      render :json => {:status => false, :hint => nil, :message => "We were unable to access your information. Please try again." } and return
    end
  end

end