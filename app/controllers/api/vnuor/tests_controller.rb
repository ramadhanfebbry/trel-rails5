class Api::Vnuor::TestsController < ApplicationController
  include Api::ApplicationsHelper
  require 'uri'
  require 'net/http'

  def request_header_for_signup_login
    url = URI("http://trelevant.herokuapp.com/api/vnuor/user/onlineorder/customer_connect")
    data = {
        "appkey" => params[:appkey],
        "first_name" => params[:first_name],
        "last_name" =>params[:last_name],
        "username" =>params[:username],
        "email" =>params[:email],
        "password" => params[:password],
        "latitude" =>params[:latitude],
        "longitude" =>params[:longitude],
        "register_type" =>params[:register_type],
        "register_device_type" =>(params[:sign_in_device_type] || params[:register_device_type]).downcase.tr(' ', '_'),
        "device_id"  => params[:device_id],
        "ref_code" => params[:referral_code],
        "phone_number" => params[:phone_number],
        "dob_day" => params[:dob_day],
        "dob_month" => params[:dob_month],
        "dob_year" => params[:dob_year],
        "special_occassion" => params[:special_occassion],
        "zipcode" => params[:zipcode],
        "name" => [params[:first_name], params[:last_name]].join(" "),
        "marketing_optin" => User.marketing_optin_value(params[:marketing_optin]),
        "security_question" => params[:security_question],
        "security_answer" => params[:security_answer],
        "favorite_location" => params[:favorite_location],
        "special_occassion" => params[:special_occassion],
        "address1" => params[:address1],
        "address2" => params[:address2],
        "city" => params[:city],
        "state" => params[:state],
        "marketing_optin_texting" => params[:marketing_optin_texting],
        "connect_type" => params[:connect_type],
        "password_hint" => params[:password_hint],
        "loyalty_card" => params[:loyalty_card],
    }

    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Post.new(url.request_uri)
    request["content-type"] = 'application/json'
    request.body = data.to_json
    response = http.request(request)
    render :json => response.read_body and return
  end

  def request_header_for_get_nuorder_settings
    url = URI("http://trelevant.herokuapp.com/api/vnuor/connect/setting")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url.request_uri)
    request["appkey"] = params[:appkey]
    request["content-type"] = 'application/json'
    response = http.request(request)
    render :json => response.read_body and return
  end

  def request_header_for_get_user_profile
    url = URI("http://trelevant.herokuapp.com/api/vnuor/user/profile")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url.request_uri)
    request["appkey"] = params[:appkey]
    request["auth_token"] = params[:auth_token]
    request["content-type"] = 'application/json'
    response = http.request(request)
    render :json => response.read_body and return
  end

  def request_header_for_search_profile
    data = {
        "patron_id" => params[:patron_id],
        "email" => params[:email],
        "phone_number" => params[:phone_number],
        "loyalty_number_id" => params[:loyalty_number_id]
    }
    url = URI("http://trelevant.herokuapp.com/api/vnuor/user/onlineorder/search_profile")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url.request_uri)
    request["appkey"] = params[:appkey]
    request["content-type"] = 'application/json'
    request.body = data.to_json
    response = http.request(request)
    render :json => response.read_body and return
  end

  def request_header_for_forgot_password
    data = {
        "email" => params[:email],
        "appkey" => params[:appkey]
    }
    url = URI("http://trelevant.herokuapp.com/api/vnuor/user/forgot_password")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Post.new(url.request_uri)
    request["content-type"] = 'application/json'
    request.body = data.to_json
    response = http.request(request)
    render :json => response.read_body and return
  end

  def request_header_for_update_password
    data = {
        "appkey" => params[:appkey],
        "auth_token" => params[:auth_token],
        "old_password" => params[:old_password],
        "new_password" => params[:new_password],
        "confirm_password" => params[:confirm_password]
    }
    url = URI("http://trelevant.herokuapp.com/api/vnuor/user/update_password")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Post.new(url.request_uri)
    request["content-type"] = 'application/json'
    request.body = data.to_json
    response = http.request(request)
    render :json => response.read_body and return
  end

  def request_header_for_update_profile
    data = {
        "appkey" => params[:appkey],
        "auth_token" => params[:auth_token],
        "email" => params[:email],
        "first_name" => params[:first_name],
        "last_name" => params[:last_name],
        "phone_number" => params[:phone_number],
        "password_hint" => params[:password_hint],
        "favorite_location" => params[:favorite_location],
        "dob_year" => params[:dob_year],
        "dob_month" => params[:dob_month],
        "dob_day" => params[:dob_day]
    }
    url = URI("http://trelevant.herokuapp.com/api/vnuor/user/profile")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Post.new(url.request_uri)
    request["content-type"] = 'application/json'
    request.body = data.to_json
    response = http.request(request)
    render :json => response.read_body and return
  end

  def request_header_for_password_hint
    data = {
        "email" => params[:email]
    }
    url = URI("http://trelevant.herokuapp.com/api/vnuor/user/onlineorder/password_hint")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url.request_uri)
    request["appkey"] = params[:appkey]
    request["content-type"] = 'application/json'
    request.body = data.to_json
    response = http.request(request)
    render :json => response.read_body and return
  end

end
