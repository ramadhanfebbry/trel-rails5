require "pp"
class Api::V1::SurveyController < Api::BaseController

  before_action :check_deal, :only => [:show, :getSurvey,:answer]

  #GET /api/v1/surveys/show
  #QUERY title
  #      questions
  #      question_choices

  def index
    @user = current_user
    @survey = @chain.surveys.where("status = ? AND valid_to_date >= ?", Survey::STATUS["Active"], Date.current).order("created_at DESC").first
    render :json => {:status => true, :survey_id => @survey.try(:id)}
  end

  def show
    #surveyId = params[:id]
    @user = current_user
    @survey = Survey.where('id = ? and chain_id = ? and status = ?', @survey_id, @chain.id, Survey::STATUS["Active"]).first
    render :json => {:status => false, :notice => "Invalid Survey"} and return if @survey.blank?
    application_key = @app.application_keys.where(:appkey => params[:appkey]).first
    ordered_questions =  @survey.all_questions_collection(@user, application_key)
    p "----------------ordered questions --------------------------"
    p ordered_questions
    #render :json => {status: true, survey: @survey.as_json(:only => [:id, :title, :text, :question_type, :label, :value],
    #    :include => {questions: {:include=>[:question_choices]}})}
    render :json => {
        :status => true, :survey => [
            {
                :id => @survey.id, :title => @survey.title,
                :questions => ordered_questions.map { |q|
                  {:id => q.id, :question_type => q.question_type, :text => q.text, :question_order_id => ordered_questions.index(q) + 1,
                   :question_choices => q.question_choices.map { |qc|
                     {:id => qc.id, :label => qc.label, :value => qc.value}
                   }
                  }
                }
            }
        ]
    }
  end

  def getSurvey
    #surveyId = params[:id]
    @user = current_user
    @survey = Survey.where('id = ? and chain_id = ? and status = ?', @survey_id, @chain.id, Survey::STATUS["Active"]).first
    show_new_question_types = @app.application_keys.where(:appkey => params[:appkey]).first.show_new_question_types rescue false
    ordered_questions =  @survey.all_questions_collection(@user, show_new_question_types)
    p "----------------ordered questions --------------------------"
    p ordered_questions
    #render :json => {status: true, survey: @survey.as_json(:only => [:id, :title, :text, :question_type, :label, :value],
    #    :include => {questions: {:include=>[:question_choices]}})}
    render :json => {
        :status => true, :survey => [
            {
                :id => @survey.id, :title => @survey.title,
                :questions => ordered_questions.map { |q|
                  {:id => q.id, :question_type => q.question_type, :text => q.text, :question_order_id => ordered_questions.index(q) + 1,
                   :question_choices => q.question_choices.map { |qc|
                     {:id => qc.id, :label => qc.label, :value => qc.value}
                   }
                  }
                }
            }
        ]
    }
  end

  def answer
    p params
    @answers = params[:answers]
    #survey_id = params[:id]
    questions = Question.where("id in(?) and survey_id = ?",@answers.keys, @survey_id)
    @survey = Survey.find @survey_id
    ### if question is not the same group in survey
    #unless @survey.rotating_questions.blank?
    #  if questions.size < @answers.keys.size - @survey.max_rotating_questions
    #    return render :json => {status: false, notice: t(:question_not_on_the_survey)}
    #  end
    #else
    #  if questions.size != @answers.keys.size
    #    return render :json => {status: false, notice: t(:question_not_on_the_survey)}
    #  end
    #end



    @results = []
    begin
      if SurveysUser.where(:user_id => current_user.id, :receipt_id => params[:receiptId]).blank?
        survey_user = SurveysUser.create(:user_id => current_user.id, :survey_id => @survey_id,
                                         :receipt_id => params[:receiptId], :reward_id => params[:rewardId], :offer_id => params[:offerId])
      end
    rescue ActiveRecord::RecordNotUnique
      #if user_id and receipt_id have stored on the database. it should be uniq
      render :json => {status: true, message: "submit success"} and return
    end

    @survey_user = survey_user
    @answers.each do |question, answer|
      @results << Answer.saveAnswer(Question.find(question), answer, survey_user.id)
    end
    p "---(***"*100
    ## update average scores on survey users
    survey_user.update_scores rescue ""
    survey_user.show_survey_or_no rescue ""

    #send survey incentive base on setting threshold
    @survey.send_incentive_to(current_user, survey_user)

    ##Add job to send email if there is negative feedback or contain an comment
    Delayed::Job.enqueue(SurveyResponseAlertJob.new(@chain, survey_user, Time.current))

    ## if its from deal, insert to user_deal_survey
    unless @deal.blank?
      deal = @deal
      #restaurant = Restaurant.find(params[:restaurant_id])

      #check deal whether checkin or not
      if deal.activity_type.eql?("Checkin")
        render :json => {status: false, message: "Deal is not allowed for Submit survey"} and return
      end

      #check if deal has an incentive or not
      incentive = deal.incentive
      if incentive.blank?
        render :json => {status: false, message: "This deal doesn't have incentive"} and return
      end

      #restaurant_ids = deal.restaurants.map(&:id)
      #if !restaurant_ids.include?(restaurant.id)
      #  render :json => {status: false, message: "Restaurant is not included as participants in this deal"} and return
      #end

      #check deal still active or no
      today = Date.today
      if deal.start_date > today or today > deal.end_date
        render :json => {status: false, message: "You can't checkin, Deal is expired or not actived yet"} and return
      end

      #check max user
      if deal.max_user > 0 and UserDealSurvey.where(deal_id: deal.id).count >= deal.max_user.to_i
      render :json => {status: false, message: "Our Apologies. Unfortunately this contest or survey has ended and participation is no longer possible. Our apologies for the inconvenience. Please try again next time!"} and return
      end

      #check limit user per day
      if deal.limit_user_per_day > 0 and UserDealSurvey.where("user_id = ? AND deal_id = ? and date(created_at) = ?",current_user.id, deal.id, today).count >= deal.limit_user_per_day.to_i
        render :json => {status: false, message: "Per day limit crossed"} and return
      end

      user_activity = UserDealSurvey.create(:deal_id => deal.id,
                                            #:restaurant_id => params[:restaurant_id],
                                            :user_id => current_user.id,
                                            #:latitude => params[:userlat],
                                            #:longitude	 => params[:userlong],
                                            :survey_user_id => @survey_user.id,
                                            :survey_id => @survey_id,
      )

      #earn points or reward if deal is guarantee
      if deal.deal_type.eql?(1)
        #earn points if incentive is points
        if deal.incentive_type.eql?("DealOffer") and incentive.kind.eql?(1) and !incentive.points.blank?
          current_user.earn(incentive.points)
          PointHistory.add_to_history(current_user.id, "Survey Deal Incentive", incentive.points)
          #add to user_incentive
          UserIncentive.add_incentive_to_user(incentive, current_user, user_activity, deal)
        end

        if deal.incentive_type.eql?("DealOffer") and incentive.kind.eql?(2)
          #add to user_incentive
          UserIncentive.add_incentive_to_user(incentive, current_user, user_activity, deal)
        end

        if deal.incentive_type.eql?("Reward")
          RewardWallet.create_with_delay(:reward_id => incentive.id, :user_id => current_user.id)
          UserIncentive.add_incentive_to_user(incentive, current_user, user_activity, deal)
        end

      end
      pp "end of submit survey"
      render :json => {status: true, message: "submit success"} and return
    end

    unless @results.include?(false)
      render :json => {status: true, message: (ApiResponseText.get_message_chain_by_locale(@chain.id, current_user.locale_id, 5))}
    else
      render :json => {status: false, notice: (ApiResponseText.get_message_chain_by_locale(@chain.id, current_user.locale_id, 6))}
    end
  end

  def pull
    user = current_user
    render :json => {status: false, notice: "Chain is not associated with chain."} and return if @chain.id != user.chain_id
    last_receipt = user.receipts.where(:is_online_order => false,  :is_olo => false ).order("created_at DESC").first
    render :json => {:status => false, :notice => "User does not have any receipts.", :receipt_id => nil, :restaurant_name => nil, :receipt_date => nil, :survey_id => nil} and return if last_receipt.blank?
    last_transaction = last_receipt.last_transaction
    offer = last_transaction.offer
    render :json => {:status => false, :notice => "Receipt does not have offer.", :receipt_id => nil, :restaurant_name => nil, :receipt_date => nil, :survey_id => nil} and return if offer.blank?
    survey = offer.survey
    render :json => {:status => false, :notice => "Receipt offer does not link to an offer.", :receipt_id => nil, :restaurant_name => nil, :receipt_date => nil, :survey_id => nil} and return if survey.blank?
    survey_user = SurveysUser.where(:survey_id => survey.id, :receipt_id => last_receipt.id, :user_id => user.id).first
    render :json => {:status => false, :notice => "User has answered the survey for receipt.", :receipt_id => nil, :restaurant_name => nil, :receipt_date => nil, :survey_id => nil} and return unless survey_user.blank?
    skipped_survey = PullSurvey.where(:user_id => user.id, :receipt_id => last_receipt.id, :survey_id => survey.id, :skipped => true).first
    render :json => {:status => false, :notice => "User has skipped the survey for receipt.", :receipt_id => nil, :restaurant_name => nil, :receipt_date => nil, :survey_id => nil} and return unless skipped_survey.blank?
    restaurant = last_transaction.restaurant
    render :json => {:status => true, :receipt_id => last_receipt.id, :restaurant_name => restaurant.name, :receipt_date => last_receipt.created_at, :survey_id => survey.id}
  end

  def skip
    user = current_user
    receipt_id =  params[:receipt_id]
    receipt = Receipt.try(:find, receipt_id) rescue nil
    render :json => {:status => false, :notice => "Invalid Receipt"} and return if receipt.blank?
    survey_id = params[:survey_id]
    survey = Survey.try(:find, survey_id) rescue nil
    render :json => {:status => false, :notice => "Invalid Survey"} and return if survey.blank?
    skipped_survey = PullSurvey.where(:user_id => user.id, :receipt_id => receipt.id, :survey_id => survey.id, :skipped => true).first
    render :json => {:status => false, :notice => "Already Skipped."} and return unless skipped_survey.blank?
    PullSurvey.create(:user_id => user.id, :chain_id => @chain.id, :survey_id => survey.id, :receipt_id => receipt.id, :skipped => true)
    render :json => {:status => true}
  end

  private

  def check_deal
    unless params[:dealid].blank?

      @deal = Deal.find params[:dealid]

      return render :json => {status: false, notice: t(:deal_not_found)} if @deal.blank?

      @survey_check = Survey.find(@deal.activity.survey_id) rescue nil

      if @survey_check
        @survey_id = @survey_check.id
      else
        return render :json => {status: false, notice: t(:survey_is_not_on_deals)}
      end
    else
      @survey_id = params[:id] || params[:surveyid]
    end
  end

end
