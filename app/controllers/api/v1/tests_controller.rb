class Api::V1::TestsController < ApplicationController
  include Api::ApplicationsHelper

  def test_locate
    authorize! :test_locate, :location
    chain = authenticateAppKey(params[:appkey]).try(:chain)
    render :template => "test_api/test_locate"
  end

  def test_timeout
      sleep(20)
  end

  def test_birthday_reward
    
  end

  def post_birthday_reward
    type = params["test"]

    if type == "10min"
      job_type = 3
    else
      job_type = 2
    end

    Chain.active.select('id').each do |ch_id|
      a = BirthdayAnnualyReward.new(ch_id,job_type)
      a.execute_annualy_reward
    end

    redirect_to :back , :notice => "Testing Complete"
  end

  def reward_list

  end

  def  olo_export
    render :layout => "admin"
  end

  def test_claim
    authorize! :test_claim, :claim
    chain = authenticateAppKey(params[:appkey]).try(:chain)
    user= current_user
    chain_rewards= Reward.of_chain(chain.id, only_active=true, only_in_effect=true, only_unexpired=true)
    user_rewards= Reward.of_user(user.id, include_removed=false, include_claimed=false)

    @rewards = chain_rewards | user_rewards  #union both

    render :template => "test_api/test_claim"
  end


  def test_claim_reward

  end

  def test_claim_reward_get_info

  end

  def geo_data_testing

  end


  def load_rewards
    user = User.where(:authentication_token => params[:user]).first
    unless user.blank?
      begin
        chain = user.chain
        chain_rewards= Reward.of_chain(chain.id, only_active=true, only_in_effect=true, only_unexpired=true)
        user_rewards= Reward.of_user(user.id, include_removed=false, include_claimed=false, include_gifted=false)
        @rewards = chain_rewards | user_rewards #union both
      rescue
        @rewards = []
      end
    else
      @rewards = []
    end
  end

  def load_restaurants_and_users
    @application_key = ApplicationKey.where(:appkey => params[:appkey]).first
    application = @application_key.application rescue nil
    chain = application.chain rescue nil
    @restaurants = chain.restaurants rescue []
    @users = chain.users rescue []
  end

  def olo_list_summaries
    render :layout => 'admin'
  end

  def olo_orderexports
    render :layout => 'admin'
  end

  def get_olo_order_batch
    @x = Olo::Order.new
    @x.get_batch(params["batch_id"])
    @batch_id = params["batch_id"]

    render :layout => 'admin'
  end

  def get_olo_orderexports
      @olo =  Olo::Order.new
      @result = @olo.orders

    render :layout => 'admin'
  end

  def get_olo_list_summaries
    begin
     @olo =  Olo::Order.new
     @result = @olo.list_order_summaries(params['start_date'].to_date, params['end_date'].to_date)
    rescue
      @result = "FORMAT date is wrong"
      end
    render :layout => 'admin'
  end

  def referral_email
    
  end

  def referral
    
  end

  def claim_message
    
  end

  def send_referer_email
     redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_answer_survey
    authorize! :test_answer_survey, :answer_survey
    @survey_id = params[:survey_id]
    @survey= Survey.includes(:chain).find(@survey_id)
    @chain= @survey.chain
    @application= Application.find_by_chain_id(@chain.id)
    @appkey= @application.application_keys.first
    @users= @chain.users
    render :template => "test_api/test_answer_survey"
  end

  def test_submit_receipt
    authorize! :test_submit_receipt, :submit_receipt
    if params[:appkey]
      @application=ApplicationKey.find_by_appkey(params[:appkey])
      @chain=@application.application.chain
      @offers=@chain.offers
      @users=@chain.users
    else
      @applications=Application.includes(:chain).all
    end
    user = current_user
    #@restaurants= chain.restaurants
    #@offers= chain.offers
    render :template => "test_api/test_submit_receipt"
  end

  def getOfferRestaurants
    authorize! :getOfferRestaurants, :offer_restaurant
    @id = params[:id]
    @opt = RestaurantOffer.select('restaurants.id, restaurants.name').joins("inner join restaurants On restaurant_offers.restaurant_id=restaurants.id").where("restaurant_offers.offer_id = ?", @id) #find_all_by_chain_id(@id)

    render :json => @opt
  end

  def test_promocode
    render :template => "test_api/test_promocode" , :layout => 'admin'
  end

  #  def test_get_deal_survey
  #  end

  def form_chain_image;end

  def test_answer_deal_survey    
  end

  def test_post_answer_deal_survey
    begin
    
      answer_hash = {}
      answer = params[:answers]
      answer = answer.split(',')
      survey = Survey.find(params[:id])
      question_ids = survey.questions.map(&:id)

      question_ids.each_with_index do |x, index|
        answer_hash.merge!({x.to_s => answer[index]})
      end

      redirect_to answer_api_v1_survey_path(survey,
        :dealid => params[:dealid],
        :appkey => params[:appkey],
        :auth_token => params[:auth_token],
        :locale => params[:locale],
        :answers => answer_hash,
        :restaurant_id => params[:restaurant_id],
        :userlat => params[:userlat],
        :userlong => params[:userlong]
      )
    rescue
      render :json => {:status => false, :message => "You have entered wrong input, please check"}
    end
  end

  def test_post_deal_survey
    begin
      redirect_to api_v1_survey_path(Survey.find(params[:id]),
        :deal_id => params[:deal_id],
        :appkey => params[:appkey],
        :auth_token => params[:auth_token],
        :locale => params[:locale])
    rescue
      render :json => {:status => false, :message => "You have entered wrong input, please check"}
    end
  end

  def forgot_password
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def submit_receipt
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def update_password
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def user_signin
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def user_signup
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_list_deals
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester")) 
  end

  def test_detail_deals
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester")) 
  end

  def test_deal_location
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  #goal_stuff
  
  def test_get_categories
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_submit_categories
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_weekly_user_goal
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_reward_activity
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def load_categories
    @application_key = ApplicationKey.where(:appkey => params[:appkey]).first
    application = @application_key.application rescue nil
    @chain = application.chain rescue nil
    @goal_categories = @chain.goal_categories rescue []
  end

  def load_user_goals
    user = User.where(:authentication_token => params[:auth_token]).first
    @user_goals = if user.blank?
      []
    else
      today = Time.now
      start_date = today.send("custom_beginning_of_#{Setting.goals.unit_time}")
      end_date = today.send("custom_end_of_#{Setting.goals.unit_time}")
      
      week = Week.find_or_create_by_start_date_and_end_date(start_date,end_date)
      user.user_goals.where(:week_id => week.id)
    end
  end

  def test_deadline
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end
  def test_deactivate_account
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_fb_reactivate_account
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_reactivate_account
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_deactivate_account
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_fb_reactivate_account
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def test_reactivate_account
    redirect_to root_path and return if !current_admin or !(current_admin.role.eql?("admin") or current_admin.role.eql?("tester"))
  end

  def sign_up_with_referral

  end

  def load_users
    @application_key = ApplicationKey.where(:appkey => params[:appkey]).first
    application = @application_key.application rescue nil
    @chain = application.chain rescue nil
    @users = @chain.users rescue []
  end

  def load_giftable_rewards
    @application_key = ApplicationKey.where(:appkey => params[:appkey]).first
    application = @application_key.application rescue nil
    @chain = application.chain rescue nil
    @rewards = @chain.rewards.where(:reward_type => Reward::TYPES["GIFTABLE"]) rescue []
  end

  def test_add_gift_to_user

  end

  def test_launch_text

  end

  #POS STUFF start- -------
  def test_pos_reward

  end

  def test_submit_barcode
    
  end

  #END POS STUFF ------


  def test_submit_survey_step2
    @messages = []
    @appkey = params[:appkey]
    application_key = ApplicationKey.where(:appkey => params[:appkey]).first
    p application_key
    @app = application_key.application rescue nil
    @chain = @app.chain rescue nil
    p @app
    p "-----"
    @auth_token = params[:auth_token]
    @user = User.where(:authentication_token => @auth_token).first
    @messsages << "User is invalid" if @user.blank?
    @survey_id = params[:id]
    @receiptId = params[:receiptId]
    @survey = Survey.where('id = ? and chain_id = ? and status = ?', @survey_id, @chain.id, Survey::STATUS["Active"]).first rescue nil
    @messages << "Could not find active survey" if @survey.blank?
    application_key = @app.application_keys.where(:appkey => params[:appkey]).first rescue nil
    @messages << "Invalid app key" if application_key.blank?
    @ordered_questions =  @survey.all_questions_collection(@user, application_key) rescue []
    unless @survey
      redirect_to test_submit_survey_path, :notice => "You submitted wrong data, please check again. #{@messages.join(' and ')}"
    end  
  end

  def test_post_submit_survey
    begin
      @messages = []
      @appkey = params[:appkey]
      application_key = ApplicationKey.where(:appkey => params[:appkey]).first
      p application_key
      @app = application_key.application rescue nil
      @chain = @app.chain rescue nil
      @user = User.where(:authentication_token => params[:auth_token]).first
      @messsages << "User is invalid" if @user.blank?
      answer_hash = {}
      answer = params[:answers]
      answer = answer.split(',')
      survey = Survey.find(params[:id])
      ordered_questions =  survey.all_questions_collection(@user, application_key) rescue []
      question_ids = ordered_questions.map(&:id)

      question_ids.each_with_index do |x, index|
        answer_hash.merge!({x.to_s => answer[index]})
      end

      redirect_to answer_api_v1_survey_path(survey,
        :receiptId => params[:receiptId],
        :appkey => params[:appkey],
        :auth_token => params[:auth_token],
        :answers => answer_hash
      )
    rescue
      render :json => {:status => false, :message => "#{@messages.join(' and ')}"}
    end
  end

  def load_survey

  end

  def load_raffle_additional_info
    begin
      @messages = []
      @appkey = params[:appkey]
      application_key = ApplicationKey.where(:appkey => @appkey).first
      @app = application_key.application rescue nil
      @messsages << "Appkey is invalid" if @app.blank?
      @chain = @app.chain rescue nil
      @messsages << "Chain is invalid" if @chain.blank?
      @user = @chain.users.where(:authentication_token => params[:auth_token]).first rescue nil
      @messsages << "User is invalid" if @user.blank?

      raffle = @chain.raffles.where(:id => params[:raffle_id]).first rescue nil
      @messsages << "Raffle is invalid" if raffle.blank?

      @additional_info = raffle.raffle_additional_informations rescue nil
      @messsages << "Raffle additional info is invalid" if @additional_info.blank?      

    rescue
      render :json => {:status => false, :message => "#{@messages.join(' and ')}"}
    end
  end

  def test_olo_aoo_settings; end

end
