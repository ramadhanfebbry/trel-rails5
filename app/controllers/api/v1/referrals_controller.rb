class Api::V1::ReferralsController < Api::BaseController
  #before_action :find_user_and_chain

  def index
    ref = ReferralProgram.by_chain(@chain).active.first

    unless ref.blank?
      render :json => ref
    else
      render :json => {:status => false, :message => "There are no active referral for #{@chain.name}"}
    end
  end

  def email
    user = current_user    
    chain = user.chain    
    locale = user.locale    
    ref_locale = chain.referral_locales.where("locale_id = ?",locale.id).first    
    active_referral = ReferralProgram.where("chain_id = ?", chain.id).active.first    
        
    unless active_referral.blank? ## if there are active referral
      ref_code = ReferralCode.user_ref_code(active_referral,user)
      
      if ref_locale and ref_code
        email_content = ref_locale.email_content % {
            :referral_code => "#{ref_code}",
            :itunes_link => "#{chain.itunes_link}",
            :play_store_link => "#{chain.play_store_link}"
        }
        email_content.gsub!(/\r\n?/, "\n")
        email_content.gsub!(/\n/, "\n")

        facebook_text = ref_locale.facebook_text % {
            :referral_code => "#{ref_code}",
            :itunes_link => "#{chain.itunes_link}",
            :play_store_link => "#{chain.play_store_link}"
        } rescue nil
        facebook_text.gsub!(/\r\n?/, "\n") unless facebook_text.blank?
        facebook_text.gsub!(/\n/, "\n") unless facebook_text.blank?

        twitter_text = ref_locale.twitter_text % {
            :referral_code => "#{ref_code}",
            :itunes_link => "#{chain.itunes_link}",
            :play_store_link => "#{chain.play_store_link}"
        } rescue nil
        twitter_text.gsub!(/\r\n?/, "\n") unless twitter_text.blank?
        twitter_text.gsub!(/\n/, "\n")  unless twitter_text.blank?

        other_media_text = ref_locale.other_media_text % {
            :referral_code => "#{ref_code}",
            :itunes_link => "#{chain.itunes_link}",
            :play_store_link => "#{chain.play_store_link}"
        } rescue nil
        other_media_text.gsub!(/\r\n?/, "\n") unless other_media_text.blank?
        other_media_text.gsub!(/\n/, "\n") unless other_media_text.blank?

        render :json => {
          :status => true,
          :email_title => ref_locale.email_subject,
          :email_body => email_content,
          :facebook_text => facebook_text,
          :twitter_text => twitter_text,
          :other_media_text => other_media_text,
          :referral_code => ref_code,
          :start_date => active_referral.start_date,
          :end_date => active_referral.expiry_date,
          :referral_program_title => active_referral.name,
          :incentive_title => get_incentive_title(active_referral.incentive)
          }
      else
        render :json => {:notice => "Admin Has not Set the email template.. "}
      end
    else
      render :json => {:status  => false,:notice => " No active referral program "}
    end
  end

  def get_incentive_title(incentive)
    title = if incentive.blank?
      "There are no incentive yet for this referral"
    elsif incentive.class == Reward
      incentive.name
    else
      incentive.title
    end
    return title
  end

  def mail_to_referer
    user = current_user
    UserMailer.mail_to_referer(user).deliver

    render :json => {:status => true, :notice => "Email to referer has been sent"}
  end

  private

  def find_user_and_chain
    @user = current_user
    @chain = @user.chain
  end
end