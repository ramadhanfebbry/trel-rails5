class Api::V1::ApplicationKeysController < Api::BaseController

  def latest
    status,  notice_text, known_key =  Application.get_the_latest_key(params[:appkey])
    
    render :json => {status: status, notice: notice_text.blank? ? '' : t(notice_text), known_key: known_key}
  end

  def launch_text
    locale = params[:locale]
    locale_id = Locale.find_by_key(locale ||= 'en').try(:id)
    launch_texts = @chain.launch_texts.where(:locale_id => locale_id).select("title, subtitle")
    launch_texts_with_order = []
    launch_texts.each_with_index do |lt, i|
      launch_texts_with_order << {:title => lt.title, :subtitle => lt.subtitle, :order => i+1}
    end
    render :json => {:status => true, :group => launch_texts_with_order }.to_json
  end

  def gallery_stream
    gallery_stream = @chain.stream_gallery
    gallery_images = gallery_stream.stream_gallery_images rescue []
    gallery_images = gallery_images.delete_if{|c| c.input_source.eql?(StreamGalleryImage::EMBED_SOURCE["XML FEED LINK"]) && c.image_feed_lists.blank?}
    list = []
    gallery_images.each do |gi|
      if gi.input_source.eql?(StreamGalleryImage::EMBED_SOURCE["XML FEED LINK"])
        gi.image_feed_lists.each do |image_feed|
          list << {:image_url => image_feed[:image_url], :image_hyeperlink => image_feed[:hyperlink], :image_title => "", :image_description => ""}
        end
      elsif gi.input_source.eql?(StreamGalleryImage::EMBED_SOURCE["IMAGES AND METADATA UPLOAD"])
        list << { :image_url => (gi.embed.url).to_s, :image_hyperlink => gi.try(:hyperlink), :image_title => gi.try(:title), :image_description => gi.try(:description)}
      end
    end
    render :json => {:status => true, :stream_title => gallery_stream.try(:title), :stream_description => gallery_stream.try(:description),
                     :animation_style => gallery_stream.try(:animation_style), :transition_duration => gallery_stream.try(:transition_duration),
                     :display_duration => gallery_stream.try(:display_duration), :images => list
    }.to_json
  end

  def last
    status, app_version, is_required, notice_text =  Application.get_the_latest_version(params[:appkey])
    
    render :json => {status: status, notice: notice_text.blank? ? '' : t(notice_text), is_required: is_required, app_version: app_version}
  end
end
