class Api::V1::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  layout 'user'

  def facebook
    auth_hash = request.env['omniauth.auth']
    data = auth_hash.extra.raw_info
    provider = auth_hash["provider"].capitalize

    @user = User.find_by_email_and_register_type(data.email, User::USERS_TYPES[provider])
    if @user
      @user.update_attributes(:first_name=>data.first_name, :last_name=>data.last_name, :email => data.email, :facebookID => data.id, :register_type => User::USERS_TYPES[provider])
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => provider
      sign_in_and_redirect @user, :event => :authentication
    else
      redirect_to new_user_registration_url
    end
  end

end
