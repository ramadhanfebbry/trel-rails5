class Api::V1::CategoriesController < Api::BaseController

  #categories?appkey=asdad&auth_token
  skip_before_action :authorize_api_user!, :only => :index
  
  def index
    user = current_user
    @categories = @chain.goal_categories.where(:status => GoalCategory::STATUS[:ACTIVE])
    locale = params[:locale].blank? ? "en" : params[:locale]
    @list_categories = []
    if user
      @list_categories = @categories.map do |c|
        { :id => c.id, :text => c.get_locale(locale), :chain_id => c.chain_id, :selected => user.categories.include?(c) ? 1 : 0 }
      end
      @list_categories = @list_categories.sort_by{|h| h[:selected] <=> h[:selected]}
    else
      @list_categories = @categories.map do |c|
        { :id => c.id, :text => c.get_locale(locale), :chain_id => c.chain_id }
      end
    end

    if @list_categories.blank?
      render :json => {status: false, notice: "Welcome! Please pick 3 areas to focus on this week. Completing your goals might even lead to a surprise!"}.to_json
    else
      render :json => {status: true, categories: @list_categories}.to_json
    end
  end

  def select
    user = current_user
    params[:category_ids] = params[:category_ids].split(",") unless params[:category_ids].blank?

    today = Time.current
    start_date = today.send("custom_beginning_of_#{Setting.goals.unit_time}")
    end_date = today.send("custom_end_of_#{Setting.goals.unit_time}")

    week = Week.where(:start_date => start_date, :end_date => end_date).first

    if user.categories.blank? or week.blank? or (week and user.user_goals.where(:week_id => week.id).blank?) or user.allow_to_update_goals
      user.category_part = true
      user.category_ids = params[:category_ids]
      if user.save
        render :json => {status: true, notice: "Your goals have been updated." }
      else
        render :json => {status: false, notice: (user.errors.messages[:category].first rescue nil) }
      end
    else
      temp_category = TmpCategory.where(:user_id => user.id).first
      if temp_category.blank?
        temp_category = TmpCategory.create(:user_id => user.id, :category_ids => (params[:category_ids].join(",") rescue nil))
      else
        temp_category.update_attributes(:category_ids => (params[:category_ids].join(",") rescue nil))
      end
      if temp_category.errors.blank?
        render :json => {status: true, notice: "Your updated goals will appear next week." }
      else
        render :json => {status: false, notice: (temp_category.errors.messages[:category].first rescue nil) }
      end
    end


  end
  
end
