class Api::V1::DealsController < Api::BaseController
  #http://prelevant.herokuapp.com/api/v1/survey/486439760?appkey=ABCDEFG&auth_token=PSK9Ap4nH9x6Dn9QzYzS
  
  def get_survey
    deal_id = params[:deal_id]
    surveyId = params[:surveyid]
    if params[:deal_id].blank?
      @survey = Survey.where('id = ? and chain_id = ? and status = ?', surveyId, @chain.id, Survey::STATUS["Active"])
    else
      @deal = Deal.find(deal_id)
      @survey = Survey.find(@deal.activity.survey_id)
    end

    render :json => {status: true, survey: @survey.as_json(:only =>
          [:id, :title, :text, :question_type, :label, :value],
        :include => {questions: {:include=>[:question_choices]}})}
  end

  def answer_survey
    @answers = params[:answers]
    deal_id = params[:deal_id]
    
    @deal = Deal.find(deal_id)
    @survey = Survey.find(@deal.activity.survey_id)

    survey_id = @survey.id
    questions = Question.where("id in(?) and survey_id = ?",@answers.keys, survey_id)

    ### if question is not the same group in survey
    if questions.size < @answers.keys.size
      return render :json => {status: false, notice: t(:question_not_on_the_survey)}
    end

    @results = []

    if SurveysUser.where(:user_id => current_user.id, :receipt_id => params[:receiptId]).blank?
      survey_user = SurveysUser.create(:user_id => current_user.id, :survey_id => survey_id,
          :receipt_id => params[:receiptId], :reward_id => params[:rewardId], :offer_id => params[:offerId])
    end
    @answers.each do |question, answer|
      @results << Answer.saveAnswer(Question.find(question),@user, answer, survey_user.id)
    end
    ## update average scores on survey users
    survey_user.update_scores rescue ""
    survey_user.show_survey_or_no rescue ""

    unless @results.include?(false)
      render :json => {status: true, message: t(:thanks_feedback)}
    else
      render :json => {status: false, notice: t(:unknown_question)}
    end
  end

  def list_deals
    deals = @chain.deals.where("activity_type IN ('Checkin','SubmitSurvey')")
    render :json  => {deals: deals.as_json(
        :only => [:id, :title, :chain_id, :activity_type, :activity_id],
        :methods =>  [:deal_type_name, :fine_print,  :deal_image_listing_url_iphone,  :deal_image_listing_url_android]
      )}
  end

  def deal_detail
    deal = Deal.where("activity_type IN ('Checkin','SubmitSurvey')").find(params[:dealid])
    available_methods = [:deal_type_name, :fine_print, :deal_image_listing_url_iphone, :deal_image_listing_url_android]
    available_methods << :associated_survey if deal.activity_type.eql?('SubmitSurvey')
    render :json  => deal.as_json(
      :only => [:id, :title, :chain_id, :activity_type, :activity_id],
      :methods =>  available_methods
    )
  end

  def deal_locations
    deal = Deal.where("activity_type IN ('Checkin','SubmitSurvey')").find(params[:dealid])
    activity = deal.activity
    deal_restaurant_ids = deal.restaurants.map(&:id)
    user_lat = params[:userlat]
    user_lng = params[:userlong]
   
    @restaurants = Restaurant.geo_scope(:origin => [user_lat, user_lng]).
      where('restaurants.id IN (?)', deal_restaurant_ids).group(Restaurant.col_list).order('distance asc').limit(10)
    
    @restaurants.map{|r| r.user_coordinate = [user_lat, user_lng]; r.min_preset_distance = activity.minimum_distance} if user_lat && user_lng
    render :json => {status: true, restaurants: @restaurants.as_json(
        :only => [:id, :name, :app_display_text, :address, :app_display_text, :zipcode, :phone_number, :latitude, :longitude],
        :methods =>  [:today_open_hour, :restaurant_distance, :preset_distance_status] )}
  end

  def checkin_submit
    deal = Deal.find(params[:dealid])
    restaurant = Restaurant.find(params[:restaurant_id])
    #check deal whether checkin or not
    if !deal.activity_type.eql?("Checkin")
      render :json => {status: false, message: "Deal is not allowed for checkin"}
      return
    end

    #check if deal has an incentive or not
    incentive = deal.incentive
    if incentive.blank?
      render :json => {status: false, message: "This deal doesn't have incentive"}
      return
    end

    restaurant_ids = deal.restaurants.map(&:id)
    if !restaurant_ids.include?(restaurant.id)
      render :json => {status: false, message: "Restaurant is not included as participants in this deal"}
      return
    end

    #check deal still active or no
    today = Date.today
    if deal.start_date > today or today > deal.end_date
      render :json => {status: false, message: "You can't checkin, Deal is expired or not actived yet"}
      return
    end

    #check max user
    if deal.max_user > 0 and UserCheckin.where(deal_id: deal.id).count >= deal.max_user.to_i
      render :json => {status: false, message: "Our Apologies. Unfortunately this contest or survey has ended and participation is no longer possible. Our apologies for the inconvenience. Please try again next time!"}
      return
    end

    #check limit user per day
    if deal.limit_user_per_day > 0 and UserCheckin.where("user_id = ? AND deal_id = ? AND DATE(created_at) = ?",current_user.id, deal.id, today).count >= deal.limit_user_per_day.to_i
      render :json => {status: false, message: "Sorry, you've reached the maximum number of Check-ins for today."}
      return
    end

    user_activity = UserCheckin.create(:deal_id => deal.id,
      :restaurant_id => params[:restaurant_id],
      :user_id => current_user.id,
      :user_latitude => params[:userlat],
      :user_longitude	 => params[:userlong]
    )

    #earn points or reward if deal is guarantee
    if deal.deal_type.eql?(1)
      #earn points if incentive is points
      if deal.incentive_type.eql?("DealOffer") and incentive.kind.eql?(1) and !incentive.points.blank?
        current_user.earn(incentive.points)
        PointHistory.add_to_history(current_user.id, "Checkin Deal Incentive", incentive.points)
        #add to user_incentive
        UserIncentive.add_incentive_to_user(incentive, current_user, user_activity, deal)
      end

      if deal.incentive_type.eql?("DealOffer") and incentive.kind.eql?(2)
        #add to user_incentive
        UserIncentive.add_incentive_to_user(incentive, current_user, user_activity, deal)
      end

      if deal.incentive_type.eql?("Reward")
        RewardWallet.create_with_delay(:reward_id => incentive.id, :user_id => current_user.id)
        UserIncentive.add_incentive_to_user(incentive, current_user, user_activity, deal)
      end
      
    end
    render :json => {status: true, message: "Thank you. You have successfully checked in to this location."}
  end
  
end