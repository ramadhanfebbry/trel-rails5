class Api::V1::GamesController < Api::BaseController

  def points
    user = current_user
    game = @chain.game
    render :json => {:status => false, :message => "Game not setup yet."} and return if game.blank?

    level_number = params[:level_number]
    level_selected = game.levels.where(:number => level_number).first
    render :json => {:status => false, :message => "Level #{level_number} not setup yet."} and return if level_selected.blank?

    game_points = params[:game_points]
    game_point_converted = game_points.to_f/level_selected.ratio.to_f

    if game.daily_cap > 0
      today = Time.current.utc.to_date
      daily_points = LevelsPoint.sum(:points,
                                     :conditions => ['user_id = ? AND game_id = ? AND date(created_at) = ?',
                                                     user.id, game.id, today])
      if (daily_points + game_point_converted) <= game.daily_cap
        game_point_converted =  game_point_converted.floor
        if game_point_converted > 0
          user.earn(game_point_converted, nil,  "game_point")
          LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => game_point_converted, :game_point_submitted => game_points )
          PointHistory.add_to_history(user.id, "Game Point", game_point_converted)
          Delayed::Job.enqueue(GameJob.new(user, game_point_converted))
        end
        render :json => {:status => true, :points_earned => game_point_converted, :message => GameNotification.get_message(user) % {:user_points_earned => game_point_converted, :user_posted_gamepoints => game_points },  :cap_reached => false}
      elsif daily_points < game.daily_cap
        points_earned = game.daily_cap - daily_points
        points_earned = points_earned.floor
        if points_earned > 0
          user.earn(points_earned, nil, "game_point")
          LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => points_earned, :game_point_submitted => game_points)
          PointHistory.add_to_history(user.id, "Game Point", points_earned)
          Delayed::Job.enqueue(GameJob.new(user, points_earned))
        end
        render :json => {:status => true, :points_earned => points_earned, :message => GameNotification.get_message(user) % {:user_points_earned => points_earned, :user_posted_gamepoints => game_points},  :cap_reached => true, :cap => "daily"}
      else
        LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => 0, :game_point_submitted => game_points)
        render :json => {:status => true, :points_earned => 0, :message => GameNotification.get_message(user) % {:user_points_earned => 0, :user_posted_gamepoints => game_points},  :cap_reached => true, :cap => "daily"}
      end
    elsif game.weekly_cap > 0
      today =  Time.current.utc
      week_start = today.beginning_of_week
      week_end =   today.end_of_week
      weekly_points = LevelsPoint.sum(:points,
                                      :conditions => ['user_id = ? AND game_id = ? AND  created_at >= ? AND created_at <= ?',
                                                      user.id, game.id, week_start, week_end])
      if (weekly_points + game_point_converted) <= game.weekly_cap
        game_point_converted =  game_point_converted.floor
        if game_point_converted > 0
          user.earn(game_point_converted, nil, "game_point")
          LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => game_point_converted, :game_point_submitted => game_points)
          PointHistory.add_to_history(user.id, "Game Point", game_point_converted)
          Delayed::Job.enqueue(GameJob.new(user, game_point_converted))
        end
        render :json => {:status => true, :points_earned => game_point_converted, :message => GameNotification.get_message(user) % {:user_points_earned => game_point_converted, :user_posted_gamepoints => game_points},  :cap_reached => false}
      elsif weekly_points < game.weekly_cap
        points_earned = game.weekly_cap - weekly_points
        points_earned = points_earned.floor
        if points_earned > 0
          user.earn(points_earned, nil, "game_point")
          LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => points_earned, :game_point_submitted => game_points)
          PointHistory.add_to_history(user.id, "Game Point", points_earned)
          Delayed::Job.enqueue(GameJob.new(user, points_earned))
        end
        render :json => {:status => true, :points_earned => points_earned, :message => GameNotification.get_message(user) % {:user_points_earned => points_earned, :user_posted_gamepoints => game_points},  :cap_reached => true, :cap => "weekly"}
      else
        LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => 0, :game_point_submitted => game_points)
        render :json => {:status => true, :points_earned => 0, :message => GameNotification.get_message(user) % {:user_points_earned => 0, :user_posted_gamepoints => game_points},  :cap_reached => true, :cap => "weekly"}
      end


    elsif game.monthly_cap > 0
      today =  Time.current.utc
      month_start = today.beginning_of_month
      month_end =   today.end_of_month
      monthly_points = LevelsPoint.sum(:points,
                                       :conditions => ['user_id = ? AND game_id = ? AND  created_at >= ? AND created_at <= ?',
                                                       user.id, game.id, month_start, month_end])
      if (monthly_points + game_point_converted) <= game.monthly_cap
        game_point_converted = game_point_converted.floor
        if game_point_converted > 0
          user.earn(game_point_converted, nil, "game_point")
          LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => game_point_converted, :game_point_submitted => game_points)
          PointHistory.add_to_history(user.id, "Game Point", game_point_converted)
          Delayed::Job.enqueue(GameJob.new(user, game_point_converted))
        end
        render :json => {:status => true, :points_earned => game_point_converted, :message => GameNotification.get_message(user) % {:user_points_earned => game_point_converted, :user_posted_gamepoints => game_points},  :cap_reached => false}
      elsif monthly_points < game.monthly_cap
        points_earned = game.monthly_cap - monthly_points
        points_earned = points_earned.floor
        if points_earned > 0
          user.earn(points_earned, nil, "game_point")
          LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => points_earned, :game_point_submitted => game_points)
          PointHistory.add_to_history(user.id, "Game Point", points_earned)
          Delayed::Job.enqueue(GameJob.new(user, points_earned))
        end
        render :json => {:status => true, :points_earned => points_earned, :message => GameNotification.get_message(user) % {:user_points_earned => points_earned, :user_posted_gamepoints => game_points},  :cap_reached => true, :cap => "monthly"}
      else
        LevelsPoint.create(:user_id => user.id, :game_id => game.id, :level_number => level_number, :points => 0, :game_point_submitted => game_points)
        render :json => {:status => true, :points_earned => 0, :message => GameNotification.get_message(user) % {:user_points_earned => 0, :user_posted_gamepoints => game_points}, :cap_reached => true, :cap => "monthly"}
      end
    end
  end

  def activity
    user = current_user
    activities = LevelsPoint.where(:user_id => user.id).order("created_at DESC")
    render :json => {:status => true, :activities => activities.map do |act|
      {:datetime => act.created_at.in_time_zone, :level_number => act.level_number,
       :game_id => act.game.id, :game_point_submitted => act.game_point_submitted, :user_point_earned => act.points }
    end
    }
  end

end