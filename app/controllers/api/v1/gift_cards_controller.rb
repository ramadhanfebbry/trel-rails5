class Api::V1::GiftCardsController < Api::BaseController

  skip_before_action :authorize_api_app!, :only => :test_call
  skip_before_action :authorize_api_user!, :only => :test_call

  def activate
    vantiv_credential = @chain.vantiv_credential
    render :json => {:status => false, :notice => "Something went wrong, please try again later or please reach out to our technical team via Contact Us.", :dev_message => "Chain does not have VANTIV credential"} and return if vantiv_credential.blank?
    user = current_user
    render :json => {:status => false, :notice => "User is not belongs to chain"} and return if user.chain_id != @chain.id

    amount = params[:amount]
    rmp = RelevantMobilePayment.new
    success, code, message, authorization = rmp.create_payment(params[:appkey], params[:auth_token], amount, params[:cvv])
    if success
      unless user.has_vantiv_gift_card?
        VantivGiftCard.transaction do
          available_gift_card = nil
          if params[:card_number].blank?
            available_gift_card = @chain.vantiv_gift_cards.available.first
            if available_gift_card.blank?
              Vantiv::TransactionLog.add(1, user, nil, nil, nil, nil, "No Available Giftcard at this moment, please try again later.", nil, nil, false)
              render :json => {:status => false, :notice => "No Available Giftcard at this moment, please try again later."} and return
            end
            available_gift_card.update_column(:used, true)
            gift_card_profile = Vantiv::GiftCardProfile.create(:user_id => user.id,
                                                               :gift_card_number => available_gift_card.card_number,
                                                               :activate_at => Time.zone.now,
                                                               :expiration_month => available_gift_card.expiration_month,
                                                               :expiration_year => available_gift_card.expiration_year,
                                                               :cvv => available_gift_card.cvv,
                                                               :cvv2 => available_gift_card.cvv2,
                                                               :security_code => available_gift_card.security_code,
                                                               :vantiv_gift_card_id => available_gift_card.id
            )
          else
            existing_card_profile = Vantiv::GiftCardProfile.where(:gift_card_number => params[:card_number]).first
            unless existing_card_profile.blank?
              Vantiv::TransactionLog.add(1, user, params[:card_number], nil, nil, nil, "Code already used by other user", nil, nil, false)
              p "VOIDED CC TRANSACTION BECAUSE PROVIDE GIFT CARD IS INVALID."
              void_success, void_code, void_message = rmp.void_payment(params[:appkey], params[:auth_token], amount, authorization)
              render :json => {:status => false, :notice => "Activate card failed, Please try again later.", :dev_message => "Code already used by other user"} and return
            end
            gift_card_profile = Vantiv::GiftCardProfile.create(:user_id => user.id,
                                                               :gift_card_number => params[:card_number],
                                                               :activate_at => Time.zone.now,
                                                               :expiration_month => 12,
                                                               :expiration_year => 49
            )
          end

          p "------ GIFT CARD PROFILE ----"
          p gift_card_profile
          vantiv_gc = Vantiv::GiftCard.new(ENV["VANTIV_ROOT_URL"], vantiv_credential.account_id, vantiv_credential.account_token, vantiv_credential.application_id, vantiv_credential.acceptor_id)
          v_status, v_code, v_message, ref_num, trans_id = vantiv_gc.activate_card(gift_card_profile.gift_card_number, gift_card_profile.expiration_month, gift_card_profile.expiration_year, amount)
          if v_status
            Vantiv::TransactionLog.add(1, user, gift_card_profile.gift_card_number, ref_num, trans_id, amount, nil, nil, nil, true)
            render :json => {:status => true, :notice => "Activate card success, Your credit card has been successfully charged.",
                             :gift_card_number => gift_card_profile.gift_card_number,
                             :expiration_month => available_gift_card.expiration_month,
                             :expiration_year => available_gift_card.expiration_year,
                             :cvv => available_gift_card.cvv,
                             :cvv2 => available_gift_card.cvv2,
                             :security_code => available_gift_card.security_code, :ref_num => ref_num, :transaction_id => trans_id}
          else
            p "VOIDED CC TRANSACTION BECAUSE ACTIVATE CARD PROCESS FAILED."
            void_success, void_code, void_message = rmp.void_payment(params[:appkey], params[:auth_token], amount, authorization)
            Vantiv::TransactionLog.add(1, user, gift_card_profile.gift_card_number, ref_num, trans_id, amount, nil, "#{v_code}--#{v_message}", nil, false)
            gift_card_profile.destroy
            available_gift_card.update_column(:used, false) if available_gift_card
            render :json => {:status => false, :notice => "Activate card failed, Please try again later.", :ref_num => ref_num, :transaction_id => trans_id, :dev_message => "#{v_code}--#{v_message}"}
          end
        end
      else
        gift_card_profile = user.vantiv_gift_card_profile
        vantiv_gc = Vantiv::GiftCard.new(ENV["VANTIV_ROOT_URL"], vantiv_credential.account_id, vantiv_credential.account_token, vantiv_credential.application_id, vantiv_credential.acceptor_id)
        v_status, v_code, v_message, ref_num, trans_id = vantiv_gc.card_reload(gift_card_profile.gift_card_number, gift_card_profile.expiration_month, gift_card_profile.expiration_year, amount)
        if v_status
          Vantiv::TransactionLog.add(1, user, gift_card_profile.gift_card_number, ref_num, trans_id, amount, nil, nil, nil, true)
          render :json => {:status => true, :notice => "Reload card success, Your credit card has been successfully charged and Your giftcard balance is updated.", :ref_num => ref_num, :transaction_id => trans_id}
        else
          p "VOIDED CC TRANSACTION BECAUSE RELOAD CARD PROCESS FAILED."
          void_success, void_code, void_message = rmp.void_payment(params[:appkey], params[:auth_token], amount, authorization)
          Vantiv::TransactionLog.add(1, user, gift_card_profile.gift_card_number, ref_num, trans_id, amount, nil, "#{v_code}--#{v_message}", nil, false)
          render :json => {:status => false, :notice => "Reload card failed, Please try again later.", :ref_num => ref_num, :transaction_id => trans_id, :dev_message => "#{v_code}--#{v_message}"}
        end
      end
    else
      Vantiv::TransactionLog.add(1, user, nil, nil, nil, nil, nil, nil, "#{code} -- #{message}", false)
      message = "We're sorry but your purchase could not go through. Please try again in a bit. If the problem persists please reach out to our technical team via Contact Us."
      message = "We're sorry but your purchase could not go through. Please update your payment method information or reach out to our technical team via Contact Us." if [59, 60].include?(code.to_i)
      render :json => {:status => false, :message => message, :dev_message => "#{code} -- #{message}"} and return
    end
  end

  def deposit
    vantiv_credential = @chain.vantiv_credential
    render :json => {:status => false, :notice => "Unfortunately we are not able to increase your balance at this time. Please try again. If the problem persists please reach out to us via Support", :dev_message => "Chain does not have VANTIV credential"} and return if vantiv_credential.blank?
    user = current_user
    render :json => {:status => false, :notice => "Unfortunately we are not able to increase your balance at this time. Please try again. If the problem persists please reach out to us via Support", :dev_message => "User is not belongs to chain"} and return if user.chain_id != @chain.id

    amount = params[:amount]
    render :json => {:status => false, :notice => "Unfortunately we are not able to increase your balance at this time. Please try again. If the problem persists please reach out to us via Support", :dev_message => "Amount should be greater than 0"} and return if amount.to_f <= 0

    rmp = RelevantMobilePayment.new
    success, code, message, authorization = rmp.create_payment(params[:appkey], params[:auth_token], amount, params[:cvv])
    if success
      gift_card_profile = user.vantiv_gift_card_profile
      if gift_card_profile.blank?
        p "VOIDED CC TRANSACTION BECAUSE RELOAD CARD PROCESS FAILED."
        void_success, void_code, void_message = rmp.void_payment(params[:appkey], params[:auth_token], amount, authorization)
        render :json => {:status => false, :notice => "Unfortunately we are not able to increase your balance at this time. Please try again. If the problem persists please reach out to us via Support", :ref_num => ref_num, :transaction_id => trans_id, :dev_message => "User does not have Gift Card connected. User has to do Activate/Register card first!!"} and return
      end
      vantiv_gc = Vantiv::GiftCard.new(ENV["VANTIV_ROOT_URL"], vantiv_credential.account_id, vantiv_credential.account_token, vantiv_credential.application_id, vantiv_credential.acceptor_id)
      v_status, v_code, v_message, ref_num, trans_id = vantiv_gc.card_reload(gift_card_profile.gift_card_number, gift_card_profile.expiration_month, gift_card_profile.expiration_year, amount)
      if v_status
        Vantiv::TransactionLog.add(2, user, gift_card_profile.gift_card_number, ref_num, trans_id, amount, nil, nil, nil, true)
        render :json => {:status => true, :notice => "Success! Your card balance has been increased successfully!", :ref_num => ref_num, :transaction_id => trans_id}
      else
        p "VOIDED CC TRANSACTION BECAUSE RELOAD CARD PROCESS FAILED."
        void_success, void_code, void_message = rmp.void_payment(params[:appkey], params[:auth_token], amount, authorization)
        Vantiv::TransactionLog.add(2, user, gift_card_profile.gift_card_number, ref_num, trans_id, amount, nil, "#{v_code}--#{v_message}", nil, false)
        render :json => {:status => false, :notice => "Unfortunately we are not able to increase your balance at this time. Please try again. If the problem persists please reach out to us via Support", :ref_num => ref_num, :transaction_id => trans_id, :dev_message => "#{v_code}--#{v_message}"}
      end
    else
      Vantiv::TransactionLog.add(2, user, nil, nil, nil, nil, nil, nil, "#{code} -- #{message}", false)
      message = "We're sorry but your purchase could not go through. Please try again in a bit. If the problem persists please reach out to our technical team via Contact Us."
      message = "We're sorry but your purchase could not go through. Please update your payment method information or reach out to our technical team via Contact Us." if [59, 60].include?(code.to_i)
      render :json => {:status => false, :message => message, :dev_message => "#{code} -- #{message}"} and return
    end
  end

  def balance
    vantiv_credential = @chain.vantiv_credential
    render :json => {:status => false, :notice => "Something went wrong, please try again later or please reach out to our technical team via Contact Us.", :dev_message => "Chain does not have VANTIV credential"} and return if vantiv_credential.blank?
    user = current_user
    render :json => {:status => false, :notice => "User is not belongs to chain"} and return if user.chain_id != @chain.id
    gift_card_profile = user.vantiv_gift_card_profile
    render :json => {:status => false, :notice => "User doesn't have gift card profile"} and return if gift_card_profile.blank?

    vantiv_gc = Vantiv::GiftCard.new(ENV["VANTIV_ROOT_URL"], vantiv_credential.account_id, vantiv_credential.account_token, vantiv_credential.application_id, vantiv_credential.acceptor_id)
    v_status, v_code, v_message, balance, ref_num, trans_id = vantiv_gc.get_card_balance(gift_card_profile.gift_card_number, gift_card_profile.expiration_month, gift_card_profile.expiration_year)
    if v_status
      Vantiv::TransactionLog.add(3, user, gift_card_profile.gift_card_number, ref_num, trans_id, 0, nil, nil, nil, true)
      render :json => {:status => true, :notice => "Get balance card success", :balance => balance, :expiration_month => gift_card_profile.expiration_month, :expiration_year => gift_card_profile.expiration_year, :ref_num => ref_num, :transaction_id => trans_id }
    else
      Vantiv::TransactionLog.add(3, user, gift_card_profile.gift_card_number, ref_num, trans_id, 0, nil, "#{v_code}--#{v_message}", nil, false)
      render :json => {:status => false, :notice => "Get Balance card failed, Please try again later.", :ref_num => ref_num, :transaction_id => trans_id, :dev_message => "#{v_code}--#{v_message}"}
    end
  end

  def gift
    vantiv_credential = @chain.vantiv_credential
    render :json => {:status => false, :notice => "Something went wrong, please try again later or please reach out to our technical team via Contact Us.", :dev_message => "Chain does not have VANTIV credential"} and return if vantiv_credential.blank?
    user = current_user
    render :json => {:status => false, :notice => "User is not belongs to chain"} and return if user.chain_id != @chain.id

    giftcard_amount = params[:giftcard_amount]
    giftee_email = params[:giftee_email]
    giftee_name = params[:giftee_name]
    custom_message = params[:custom_message]
    gifter_name = params[:gifter_name]
    image_header_url = params[:image_header_url].present? ? params[:image_header_url] : (@chain.giftcard_skins.where(is_active: true).first.email_header_image.try(:url) rescue nil)

    render :json => {:status => false, :message => "Please enter a valid email address."} and return unless User.is_valid_email_address?(giftee_email)
    render :json => {:status => false, :message => "Unfortunately we are not able to process your request. Please double check the information you have entered and try again. If the problem persists please reach out to us via Support.", :dev_message => "Invalid Amount."} and return if giftcard_amount.to_f <= 0

    rmp = RelevantMobilePayment.new
    success, code, message, authorization = rmp.create_payment(params[:appkey], params[:auth_token], giftcard_amount, params[:cvv])
    if success
      available_gift_card = @chain.vantiv_gift_cards.available.first
      if available_gift_card.blank?
        void_success, void_code, void_message = rmp.void_payment(params[:appkey], params[:auth_token], giftcard_amount, authorization)
        p "VOIDED because NO AVAILABLE GIFTCARD ON BATCH: VOIDED STATUS -- #{void_success} -- #{void_code} -- #{void_message} ----"
        render :json => {:status => false, :notice => "Unfortunately we are not able to process your request. Please double check the information you have entered and try again. If the problem persists please reach out to us via Support.", :dev_message => "No Available Giftcard at this moment, please try again later."} and return
      end
      available_gift_card.update_column(:used, true)
      vantiv_gc = Vantiv::GiftCard.new(ENV["VANTIV_ROOT_URL"], vantiv_credential.account_id, vantiv_credential.account_token, vantiv_credential.application_id, vantiv_credential.acceptor_id)
      v_status, v_code, v_message, ref_num, trans_id = vantiv_gc.activate_card(available_gift_card.card_number, available_gift_card.expiration_month, available_gift_card.expiration_year, giftcard_amount)
      if v_status
        giftcard_notification_email = @chain.giftcard_notification_emails.where(:locale_id => user.locale_id).first
        Vantiv::TransactionLog.add(4, user, available_gift_card.card_number, ref_num, trans_id, giftcard_amount, nil, nil, nil, true)
        VantivGiftCardEmailSentLog.create(chain_id: @chain.id, giftcard_notification_email: nil, card_number: available_gift_card.card_number, giftcard_amount: giftcard_amount, giftee_email: giftee_email, giftee_name: giftee_name, custom_message: custom_message, gifter_name: gifter_name, image_header_url: image_header_url, user_id: user.id)
        unless giftcard_notification_email.blank?
          card_number = available_gift_card.card_number
          Delayed::Job.enqueue(GiftcardNotificationEmailJob.new(@chain, giftcard_notification_email, card_number, giftcard_amount, giftee_email, giftee_name, custom_message, gifter_name, image_header_url))
        end
        render :json => {:status => true, :message => "Success! Your gift card has been sent to #{giftee_email}", :ref_num => ref_num, :transaction_id => trans_id } and return
      else
        void_success, void_code, void_message = rmp.void_payment(params[:appkey], params[:auth_token], giftcard_amount, authorization)
        Vantiv::TransactionLog.add(4, user, available_gift_card.card_number, ref_num, trans_id, giftcard_amount, nil, "#{v_code}--#{v_message}", nil, false)
        p "VOIDED because ACTIVATE CARD IS FAILED: VOIDED STATUS -- #{void_success} -- #{void_code} -- #{void_message} ----"
        available_gift_card.update_column(:used, false)
        render :json => {:status => false, :notice => "Unfortunately we are not able to process your request. Please double check the information you have entered and try again. If the problem persists please reach out to us via Support.", :ref_num => ref_num, :transaction_id => trans_id, :dev_message => "#{v_code}--#{v_message}"} and return
      end
    else
      Vantiv::TransactionLog.add(4, user, nil, nil, nil, nil, nil, nil, "#{code} -- #{message}", false)
      message = "We're sorry but your purchase could not go through. Please try again in a bit. If the problem persists please reach out to our technical team via Contact Us."
      message = "We're sorry but your purchase could not go through. Please update your payment method information or reach out to our technical team via Contact Us." if [59, 60].include?(code.to_i)
      render :json => {:status => false, :message => message, :dev_message => "#{code}--#{message}"} and return
    end
  end

  def unload
    vantiv_credential = @chain.vantiv_credential
    render :json => {:status => false, :notice => "Something went wrong, please try again later or please reach out to our technical team via Contact Us.", :dev_message => "Chain does not have VANTIV credential"} and return if vantiv_credential.blank?
    user = current_user
    render :json => {:status => false, :notice => "User is not belongs to chain"} and return if user.chain_id != @chain.id

    if params[:card_number].blank?
      gift_card_profile = user.vantiv_gift_card_profile
      card_number = gift_card_profile.gift_card_number
      exp_month = gift_card_profile.expiration_month
      exp_year = gift_card_profile.expiration_year
    else
      card_number = params[:card_number]
      exp_month = 12
      exp_year = 49
    end

    amount = params[:amount].blank? ? "0" : params[:amount]
    vantiv_gc = Vantiv::GiftCard.new(ENV["VANTIV_ROOT_URL"], vantiv_credential.account_id, vantiv_credential.account_token, vantiv_credential.application_id, vantiv_credential.acceptor_id)
    v_status, v_code, v_message, balance, ref_num, trans_id = vantiv_gc.card_unload(card_number, exp_month, exp_year, amount)
    if v_status
      Vantiv::TransactionLog.add(5, user, card_number, ref_num, trans_id, 0, nil, nil, nil, true)
      render :json => {:status => true, :notice => "Card Unload success", :balance => balance, :expiration_month => gift_card_profile.expiration_month, :expiration_year => gift_card_profile.expiration_year, :ref_num => ref_num, :transaction_id => trans_id }
    else
      Vantiv::TransactionLog.add(5, user, card_number, ref_num, trans_id, 0, nil, "#{v_code}--#{v_message}", nil, false)
      render :json => {:status => false, :notice => "Card Unload failed, Please try again later.", :ref_num => ref_num, :transaction_id => trans_id, :dev_message => "#{v_code}--#{v_message}"}
    end
  end

  def test_call
    render :json => {:status => false, :notice => "Please select chain"} and return if params[:chain_id].blank?
    render :json => {:status => false, :notice => "Please select Api Call"} and return if params[:api_call].blank?

    chain = Chain.find(params[:chain_id])
    vantiv_credential = chain.vantiv_credential
    render :json => {:status => false, :notice => "Something went wrong, please try again later or please reach out to our technical team via Contact Us.", :dev_message => "Chain does not have VANTIV credential"} and return if vantiv_credential.blank?

    card_number = params[:card_number]
    amount = params[:amount]
    render :json => {:status => false, :notice => "Please Insert Valid Card Number"} and return if card_number.blank?
    exp_month = 12
    exp_year = 49
    vantiv_gc = Vantiv::GiftCard.new(ENV["VANTIV_ROOT_URL"] || "https://certtransaction.elementexpress.com/express.asmx", vantiv_credential.account_id, vantiv_credential.account_token, vantiv_credential.application_id, vantiv_credential.acceptor_id)
    case params[:api_call]
      when "activate_card"
        v_status, v_code, v_message, ref_num, trans_id = vantiv_gc.activate_card(card_number, exp_month, exp_year, amount)
        Vantiv::TransactionLog.add(1, chain, card_number, ref_num, trans_id, amount, nil, "#{v_code}--#{v_message}", nil, v_status)
        render :json => {:status => v_status, :code => v_code, :notice => v_message, :ref_num => ref_num, :transaction_id => trans_id}
      when "card_reload"
        v_status, v_code, v_message, ref_num, trans_id = vantiv_gc.card_reload(card_number, exp_month, exp_year, amount)
        Vantiv::TransactionLog.add(2, chain, card_number, ref_num, trans_id, amount, nil, "#{v_code}--#{v_message}", nil, v_status)
        render :json => {:status => v_status, :code => v_code, :notice => v_message, :ref_num => ref_num, :transaction_id => trans_id}
      when "get_card_balance"
        v_status, v_code, v_message, balance, ref_num, trans_id = vantiv_gc.get_card_balance(card_number, exp_month, exp_year)
        Vantiv::TransactionLog.add(3, chain, card_number, ref_num, trans_id, 0, nil, "#{v_code}--#{v_message}", nil, v_status)
        render :json => {:status => v_status, :code => v_code, :notice => v_message, :ref_num => ref_num, :transaction_id => trans_id, :balance => balance}
      when "card_unload"
        v_status, v_code, v_message, balance, ref_num, trans_id = vantiv_gc.card_unload(card_number, exp_month, exp_year, amount)
        Vantiv::TransactionLog.add(5, chain, card_number, ref_num, trans_id, 0, nil, "#{v_code}--#{v_message}", nil, v_status)
        render :json => {:status => v_status, :code => v_code, :notice => v_message, :ref_num => ref_num, :transaction_id => trans_id, :balance => balance}
      when "card_reversal"
        v_status, v_code, v_message, balance, ref_num, trans_id = vantiv_gc.reversal(card_number, exp_month, exp_year, amount)
        Vantiv::TransactionLog.add(6, chain, card_number, ref_num, trans_id, 0, nil, "#{v_code}--#{v_message}", nil, v_status)
        render :json => {:status => v_status, :code => v_code, :notice => v_message, :ref_num => ref_num, :transaction_id => trans_id, :balance => balance}
    end
  end

end
