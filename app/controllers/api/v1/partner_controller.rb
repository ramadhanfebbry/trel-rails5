class Api::V1::PartnerController < Api::BaseController

  def category
    categories = @chain.partner_categories
    if categories.blank?
      render :json => {status: false, message: "Chain does not have Partner categories"}
    else
      render :json => {status: true, categories: categories.as_json(
          :only => [:id, :description], :methods => [:title])}
    end
  end

  def partner_reward
    params[:reward] = {:reward_type => Reward::TYPES["PARTNER"],
                       :effectiveDate => params[:start_date],
                       :expiryDate => params[:end_date],
                       :name => params[:title],
                       :description => params[:description]
    }
    valid_all_locations = (params[:valid_all_locations] || false rescue false)
    restaurant_ids = params[:valid_locations] rescue []
    render :json => {status: false, message: "Invalid Locations"} and return if valid_all_locations == false && restaurant_ids.blank?

    valid_restaurant_ids = @chain.restaurants.map(&:id) rescue []
    restaurant_ids.each do |restaurant_id|
      render :json => {status: false, message: "Invalid Locations"} and return if valid_restaurant_ids.exclude?(restaurant_id)
    end

    if valid_all_locations
      valid_category_ids = PartnerCategory.where("chain_id = #{@chain.id}").map(&:id)
    else
      valid_category_ids = []
      restaurant_ids.each do |rest_id|
        partner_categories = PartnerSubCategory.includes(:restaurant_categories).where("partner_sub_categories.chain_id = #{@chain.id}  and restaurant_categories.restaurant_id = #{rest_id}")
        partner_categories.each do |category|
          valid_category_ids << category.partner_category.id.to_i if category.partner_category
        end if partner_categories
      end
    end
    p "valid_category_ids #{valid_category_ids}"
    order_from_category = params[:discount_formula][:order_from_category] rescue []
    order_from_category.each do |order|
      p "order"
      p order["categoryId"].to_i
      render :json => {status: false, message: "Invalid Category"} and return if valid_category_ids.exclude?(order["categoryId"].to_i)
    end
    discount_from_category = params[:discount_formula][:discount_from_category] rescue []
    discount_from_category.each do |discount|
      p "discount"
      p discount["categoryId"].to_i
      render :json => {status: false, message: "Invalid Category"} and return if valid_category_ids.exclude?(discount["categoryId"].to_i)
    end

    @reward = Reward.new(params[:reward])
    @reward.points = 0
    @reward.chain_id = @chain.id
    if @reward.save && @reward.activate!
      Delayed::Job.enqueue(PartnerRewardJob.new(@chain, @reward, params))
      render :json => {status: true, :reward_id  => @reward.id} and return
    else
      render :json => {status: false, message: @reward.errors.full_messages} and return
    end
  end

  def get_partner_reward
    if params[:reward_id].present?
      @reward = Reward.find(params[:reward_id]) rescue nil
      render :json => {status: false, :message  => "Couldn't find Reward with id #{params[:reward_id]}"} and return if @reward.blank?
      if @reward
        render :json => {status: true, rewards: @reward.as_json(
            :only => [:id, :description],
            :methods =>  [:title, :start_date, :end_date, :valid_days_of_week_and_time, :valid_all_locations, :valid_locations, :discount_formula, :order_from_category, :discount_from_category])} and return
      else
        render :json => {status: false, :message  => "Chain does not have Partner Rewards"} and return
      end
    else
      if params[:all_rewards] == "true"
        @rewards = @chain.rewards.not_expired.where(reward_type: Reward::TYPES["PARTNER"])
        render :json => {status: true, rewards: @rewards.as_json(
            :only => [:id, :description],
            :methods =>  [:title, :start_date, :end_date, :valid_days_of_week_and_time, :valid_all_locations, :valid_locations, :discount_formula, :order_from_category, :discount_from_category])} and return
      else
        @rewards = @chain.rewards.where(reward_type: Reward::TYPES["PARTNER"])
        render :json => {status: true, rewards: @rewards.as_json(
            :only => [:id, :description],
            :methods =>  [:title, :start_date, :end_date, :valid_days_of_week_and_time, :valid_all_locations, :valid_locations, :discount_formula, :order_from_category, :discount_from_category])} and return
      end
    end
  end

  def partner_reward_code
    reward= Reward.find(params[:reward_id]) rescue nil
    if reward
      user_id = params[:user_id]
      champain_id = params[:champain_id]
      number_of_valid_days = params[:number_of_valid_days].to_i
      staffcode, length = Barcode.generate_partner_reward_code(@chain, reward.id, user_id, champain_id, number_of_valid_days)
      code_exp = Time.now + number_of_valid_days.days
      code_exp = reward.expiryDate if reward.expiryDate < code_exp
      render :json => {:status => true,
                       :reward_code => staffcode.add_zero_padding(length.to_i),
                       :expiration_date => code_exp
      }
    else
      render :json => {status: false, message: "Couldn't find Reward with id #{params[:reward_id]}"}
    end
  end

  def webhook
    chain = @chain
    reward_id = params[:reward_id]
    reward_code = params[:reward_code]
    check = params[:check]
    location_id = params[:location_id]
    discount = params[:discount]
    user_id = params[:user_id]
    campaign_id = params[:campaign_id]
    status = params[:status]
    status_description = params[:status_description]

    render :json => {:status => false, :message => "Something went wrong, please comeback shortly"} and return if reward_id.blank? && reward_code.blank? && check.blank? && location_id.blank? && discount.blank?

    reward = Reward.find reward_id rescue nil
    render :json => {:status => false, :code => 101, :message => "Reward code is invalid"} and return if reward.blank?

    location = Restaurant.find(location_id) rescue nil
    render :json => {:status => false, :message => "Invalid Location"} and return if location.blank?

    webhook = BridgWebhook.create(
          chain_id: @chain.id,
          check: check,
          reward_id: reward_id,
          restaurant_id: location_id,
          reward_code: reward_code,
          discount: discount,
          user_id: user_id,
          campaign_id: campaign_id,
          status: status,
          status_description: status_description
        )

        reward_code = reward_code.to_s.upcase
        reward_code_setting = PartnerCodeSetting.where("chain_id = #{chain.id}").first rescue nil
        reward_code_setting = JSON.parse(reward_code_setting.to_json) rescue nil
        reward_code_setting ||= PartnerCodeSetting::DEFAULT_SETTING_REWARDCODE
        if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
          if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
            reward_code = reward_code.to_ean_13_format
          end
        end
        reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)

        reward_code_user = PartnerRewardCode.where(chain_id: chain.id, code: reward_code, reward_id: reward_id, used: false).first rescue nil
        render :json => {:status => false, :code => 101, :message => "Reward code is invalid"} and return if reward_code_user.blank?

        code_exp = reward_code_user.created_at + reward_code_user.number_of_valid_days.days
        render :json => {:status => false, :code => 101, :message => "Reward code is invalid"} and return if code_exp < Time.zone.now


        #xml_in_nokogiri_format = Nokogiri::XML(params[:check])
        p "request.content_type"
        p request.content_type
        p "request.content_type #{request.content_type}"
        p "----------------"
        check = Nokogiri::XML(xml_receipt)
        render :json => {:confirm => 0, :response_code => 1} and return if check.children.blank?
        check_id = check.root["id"] rescue nil
        seq_number = check.root["seq_num"] rescue nil
        revenue_center = check.search("rvc-num").text.to_s rescue nil
        employee_num =  check.search("emp-num").text.to_s rescue nil
        tbl_num =  check.search("tbl-num").text.to_s rescue nil
        payment_included = check.search("tender//type").map(&:text).include?("MobileTender") rescue false
        menu_items_list = check.search('menu-item')
        due_total = 0
        pos_used_type = "micros"

        p "here is check id and seq number -----"
        check_id = check_id.strip rescue nil
        seq_number = seq_number.strip rescue nil
        p "check id is #{check_id}"
        p "seq num is #{seq_number}"

        items_discount = []
        reward_general_menu_item_ids = reward.general_menu_items.map(&:id)
        reward_menu_items = reward.reward_menu_items
        required_menu_items = reward_menu_items.select{|a| a.required }
        optional_menu_items = reward_menu_items.select{|a| !a.required }
        items_check_buy = []
        xml_menu_items = menu_items_list
        parent_qualified = true
        xml_menu_items.each do |menu_item|
          item = {}
          item[:menu_item_id] = menu_item.attr("id").strip rescue nil
          item[:menu_item_id] = menu_item["ItemId"].to_s.strip if item[:menu_item_id].blank?

          item[:name] = menu_item.search("name").text.strip rescue nil
          item[:name] = menu_item["Name"].to_s.strip if item[:name].blank?

          item[:quantity] = menu_item.search("quantity").text.to_i rescue 0
          item[:quantity] = menu_item["Quantity"].to_i if item[:quantity] == 0

          item[:total_amount] = menu_item.search("total-amount").text.to_f rescue 0
          item[:total_amount] = menu_item["TotalPrice"].to_f if item[:total_amount] == 0

          item[:price_per_item] = ((item[:total_amount]/item[:quantity]) rescue 0)
          item[:price_per_item] = menu_item["UnitPrice"].to_f if item[:price_per_item] == 0

          item[:level_size_id] = menu_item.search("size-id").text rescue nil
          item[:level_size_id] = menu_item["SizeId"].to_i  if item[:level_size_id].blank? &&  !menu_item["SizeId"].blank?
          item[:level_size_id] = nil if item[:level_size_id].blank?

          p "---- item number  #{item[:menu_item_id]}"
          p "------- chain = #{chain.id}--"
          p "=------ pos location = #{@pos_location.id}"
          arr_general_menu_item_id = GeneralMenuItemRangeValue.where("start_value <= #{item[:menu_item_id]} and end_value >= #{item[:menu_item_id]} and chain_id = #{chain.id}").map(&:general_menu_item_id)
          pos_menu_items = PosMenuItem.select("id").where(:item_number => item[:menu_item_id], :chain_id => chain.id, :level_size_id => item[:level_size_id])
          pos_menu_items.each do |pos_menu_item|
            arr_general_menu_item_id << (pos_menu_item.general_menu_items.map(&:id) rescue [])
            break unless arr_general_menu_item_id.blank?
          end
          item[:general_menu_item_id] = arr_general_menu_item_id.flatten
          item[:required_menu_item] = !(item[:general_menu_item_id] & required_menu_items.map(&:general_menu_item_id)).blank?
          if item[:general_menu_item_id].blank? && !item[:required_menu_item]
            parent_qualified = false
          elsif !item[:general_menu_item_id].blank? && item[:required_menu_item]
            parent_qualified = true
          end
          items_check_buy << item if parent_qualified
        end

        #ordered_item_check_buy = items_check_buy.sort{|a,b| a[:price_per_item] <=> b[:price_per_item]}
        #filtered_items_check = items_check_buy.select{|a| !(reward_general_menu_item_ids && a[:general_menu_item_id]).blank?}

        grouped_items_check_buy = []
        items_check_buy.each_with_index do |item_check, i|
          if item_check[:required_menu_item]
            item_check[:all_price_with_optional_item] = item_check[:price_per_item]
            menu_item_combo_sum = [item_check[:menu_item_id]]
            p '----'
            p item_check[:menu_item_combo_sum]
            (i+1).upto(items_check_buy.length-1) do |idx|
              optional_item = items_check_buy[idx]
              unless optional_item[:required_menu_item]
                item_check[:all_price_with_optional_item] += optional_item[:price_per_item]
                menu_item_combo_sum << optional_item[:menu_item_id]
              else
                break
              end
            end
          end
          item_check[:menu_item_combo_sum] = menu_item_combo_sum.join("-") if menu_item_combo_sum
          grouped_items_check_buy << item_check
        end

        if chain.pos_discount_setting.eql?(Chain::POS_DISCOUNT_TYPE["Discount Least priced qualifying item"])
          ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| a[:all_price_with_optional_item] <=> b[:all_price_with_optional_item]}
        else
          ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| b[:all_price_with_optional_item] <=> a[:all_price_with_optional_item]}
        end

        pos_discount_type = reward.pos_discount_type

        #non menu item discount based
        if pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?
          if request.content_type == "application/json" && xml_receipt.class.to_s != "String"
            pos_check = params["check"]
            check_subtotal = pos_check["Subtotal"]
          else
            pos_xml = PosReceiptXml.new(params["check"])
            check_subtotal = pos_xml.sub_total
          end
          total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", check_id, seq_number, @pos_location.restaurant_id])
          subtotal_after_discount = check_subtotal - total_discount_registered
          if subtotal_after_discount > 0
            discount_applied = subtotal_after_discount > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : subtotal_after_discount
            reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
            RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
            pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
            pos_reward_check.user_id = (user.id rescue nil)
            pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
            pos_reward_check.save
            if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
              reward.claimed_by(user)
              Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
            end
            if chain.auto_loyalty && due_total == 0
              xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, discount_applied)
              Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
            end
            render :json => {:status => true, :discount => (discount_applied*-1), :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
          else
            pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
            pos_reward_check.user_id = (user.id rescue nil)
            pos_reward_check.error_description = "Reward requirement is not met."
            pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
            pos_reward_check.save
            #reward_transaction.update_column(:pos_used, true)
            render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
          end
        end

        #check min no items ordered
        qualified_items_ordered_validation = ordered_item_check_buy.select{|a| !(a[:general_menu_item_id] & reward_menu_items.map(&:general_menu_item_id)).blank?}
        return [items_discount, "Discount is not activated, because minimum number of items ordered validation."] if pos_discount_type.min_no_items_ordered < 1
        return [items_discount, "Discount is not activated, because number of items to be discounted validation."] if pos_discount_type.min_no_items_discounted < 1

        return [items_discount, "The Items that you bought is not met with the requirement minimum number of items ordered."] if qualified_items_ordered_validation.count < pos_discount_type.min_no_items_ordered && pos_discount_type.min_no_items_ordered > 1
        item_discounted = 0
        #what discount that reward offered
        required_menu_items.each do |reward_general_menu_item|
          #reward_general_menu_item_ids.each do |reward_general_menu_item_id|
          ordered_item_check_buy.each do |filtered_item|
            next if filtered_item[:price_per_item] <= 0 && pos_used_type == "northkey"
            if filtered_item[:general_menu_item_id].include?(reward_general_menu_item.general_menu_item_id)
              reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]).count
              qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
              if reward_discount_count < qty_selected
                items_discount << {:menu_item_id => filtered_item[:menu_item_id], :price => filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]}
                item_discounted += 1
                break if item_discounted >= pos_discount_type.min_no_items_discounted
                #CODE FOR OPTIONAL MENU ITEM
                optional_menu_discount_items = []
                optional_menu_items.each do |opt_reward_general_menu_item|
                  (grouped_items_check_buy.index(filtered_item)+1).upto(grouped_items_check_buy.length-1) do |idx|
                    opt_filtered_item = grouped_items_check_buy[idx] rescue nil
                    break if opt_filtered_item.blank?
                    break if opt_filtered_item[:required_menu_item]
                    if opt_filtered_item[:general_menu_item_id].include?(opt_reward_general_menu_item.general_menu_item_id)
                      reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => opt_filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]).count
                      qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
                      if reward_discount_count < qty_selected
                        #optional_menu_discount_items <<  opt_filtered_item
                        items_discount << {:menu_item_id => opt_filtered_item[:menu_item_id], :price => opt_filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => opt_filtered_item[:level_size_id]}
                        item_discounted += 1
                        break if item_discounted >= pos_discount_type.min_no_items_discounted
                      end
                    end
                  end
                end
                # break
              end
            end
          end
        end

        #preset item discount if there not suitable combo
        items_discount = [] if !items_discount.blank? && !(required_menu_items.count <= items_discount.count)

        unless items_discount.blank?
          #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
          reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
          reward_code_user.destroy if reward_code_user
          p "here are the items that got the discount"
          p items_discount
          all_discount_item = items_discount.sum{ |a| a[:price]}
          if pos_discount_type.blank?
            calculated_discount_from_reward = all_discount_item
          elsif pos_discount_type.is_discount_percentage?
            calculated_discount_from_reward = ((pos_discount_type.discount_amount.to_f/100) * all_discount_item).round(2)
          elsif pos_discount_type.is_discount_price?
            calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? (all_discount_item - pos_discount_type.discount_amount) : all_discount_item).round(2)
          elsif pos_discount_type.is_discount_fixed_price?
            calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : all_discount_item).round(2)
          end
          items_discount.each do |item|
            RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => calculated_discount_from_reward, :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum] )
          end
          pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
          pos_reward_check.user_id = (user.id rescue nil)
          pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
          pos_reward_check.save
          if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
            reward.claimed_by(user)
            Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
          end
          if chain.auto_loyalty && due_total == 0
            xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, calculated_discount_from_reward)
            Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
          end
          render :json => {:status => true, :discount => (calculated_discount_from_reward*-1), :discount_items => items_discount, :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
        else
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.user_id = (user.id rescue nil)
          pos_reward_check.error_description = "Reward requirement is not met."
          pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
          pos_reward_check.save
          #reward_transaction.update_column(:pos_used, true)
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
        end


  end

end