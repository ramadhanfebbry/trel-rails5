class Api::V1::OffersController < Api::BaseController

  # Returns closest 10 restaurants to the provided location, each restaurant should contain a list of participating offers
  #
  #GET /api/v1/offers/nearby
  #QUERY lat
  #      lng
  #      appkey
  #
  def nearby
    today = Date.today
    base_conditions   = "restaurants.chain_id = #{@chain.id} AND \"offers\".\"effectiveDate\" <= '#{today}' AND \"offers\".\"expiryDate\" >= '#{today}'"

    lat = params[:lat]
    lng = params[:lng]
    psearch = params[:search].to_s.strip
    webordering = params[:webordering].blank? ? "" : "online_order_support_status desc nulls last, "

    first_conditions  = []
    second_conditions   = []
    @restaurants = []
    radius = nil

    unless psearch.blank?
      search_criteria = CGI.unescape(psearch)
      if /\A\d+\z/.match(search_criteria)
        restaurant = @chain.restaurants.where(:zipcode => search_criteria).first
        if restaurant
          search_criteria = "#{restaurant.try(:city).try(:name)},#{restaurant.try(:region).try(:abbreviation)}"
          lat = restaurant.latitude
          lng = restaurant.longitude
        else
          address = search_criteria.to_region rescue ""
          lat = search_criteria.to_lat rescue 0
          lng = search_criteria.to_lon rescue 0
          search_criteria = address
          search_criteria = "" if search_criteria.nil?
          lat = 0 if lat.nil?
          lng = 0 if lng.nil?

          if search_criteria.blank?
            @restaurants = []
            render :json => {status: true, restaurants: @restaurants.as_json(
                :only => [:id, :name, :app_display_text, :address, :app_display_text, :zipcode, :phone_number, :latitude, :longitude, :online_order_link, :beacon_serial_number, :beacon_uuid, :location_qrcode_identifier, :social_link, :agm_support, :external_partner_id.to_s, :online_order_support_status],
                :methods =>  [:today_open_hour, :restaurant_distance, :cloud_connect_site_id, :ncr_aloha_loyalty_store_id, :is_open, :city_label, :state_label, :country_label, :available_offers, :online_partner] )}
            return
          end
        end
        radius = 50
        @restaurants = Restaurant.offer_active.geo_scope(:origin => [lat, lng], :within => radius).joins({:city => :region}, :offers).
            where(base_conditions).
            group(Restaurant.col_list).order("#{webordering} distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
        @restaurants.map{|r| r.user_coordinate = [lat, lng]} if lat && lng
        @restaurants.map{|r| r.nil_to_blank_string }
        render :json => {status: true, restaurants: @restaurants.as_json(
            :only => [:id, :name, :app_display_text, :address, :app_display_text, :zipcode, :phone_number, :latitude, :longitude, :online_order_link, :online_order_support_status, :aloha_online_id, :delivery_radius_miles, :social_link, :agm_support, :external_partner_id.to_s, :online_order_support_status],
            :methods =>  [:today_open_hour, :restaurant_distance, :cloud_connect_site_id, :ncr_aloha_loyalty_store_id, :is_open, :city_label, :state_label, :country_label, :available_offers, :online_partner] )}
        return
      end
      search_criterias = search_criteria.split(",")

      city = search_criterias.first.strip.downcase
      state = search_criterias.last.strip.downcase || city

      if search_criterias.size == 1
        conditions = []
        conditions << base_conditions
        conditions << "lower(regions.abbreviation) = '#{state}'"
        first_conditions << conditions.join(" AND ")

        conditions = []
        conditions << base_conditions
        conditions << "lower(regions.name) similar to '%#{state}%'"
        first_conditions << conditions.join(" AND ")

        conditions = []
        conditions << base_conditions
        conditions << "lower(cities.name) similar to '%(#{city})%'"
        first_conditions << conditions.join(" AND ")

        count = 0
        first_conditions.each do |con|
          count = count + 1
          conditions = []
          conditions << base_conditions
          conditions << con unless con.blank?
          conditions = conditions.join(" AND ")
          if radius.blank?
            @restaurants = Restaurant.offer_active.geo_scope(:origin => [lat, lng]).joins({:city => :region}, :offers).
                where(conditions).
                group(Restaurant.col_list).order("#{webordering} distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
          else
            @restaurants = Restaurant.offer_active.geo_scope(:origin => [lat, lng], :within => radius).joins({:city => :region}, :offers).
                where(conditions).
                group(Restaurant.col_list).order("#{webordering} distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)

          end
          break unless @restaurants.to_a.blank?
        end

        if count == 1
          if @restaurants.to_a.blank? == false && @restaurants.to_a.size < Setting.API.restaurants.nearby_limit
            reg = @restaurants.to_a.first.region.id
            conditions = []
            conditions << base_conditions
            conditions << "regions.id = #{reg}"
            conditions = conditions.join(" AND ")
            if radius.blank?
              res = Restaurant.offer_active.geo_scope(:origin => [lat, lng]).joins({:city => :region}, :offers).where(conditions).group(Restaurant.col_list).order("#{webordering} distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
            else
              res = Restaurant.offer_active.geo_scope(:origin => [lat, lng], :within => radius).joins({:city => :region}, :offers).where(conditions).group(Restaurant.col_list).order("#{webordering} distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
            end

            @restaurants = @restaurants + res
            @restaurants = @restaurants.uniq.first(@chain.max_number_of_locations)

          end
        end

      elsif search_criterias.size > 1
        conditions = []
        conditions << base_conditions
        conditions << "lower(regions.abbreviation) = '#{state}'"
        conditions << "lower(cities.name) similar to '%(#{city})%'"
        first_conditions << conditions.join(" AND ")

        conditions = []
        conditions << base_conditions
        conditions << "lower(regions.name) similar to '%(#{state})%'"
        conditions << "lower(cities.name) similar to '%(#{city})%'"
        first_conditions << conditions.join(" AND ")

        conditions = []
        conditions << base_conditions
        conditions << "(lower(regions.name) similar to '%(#{state})%' OR lower(regions.abbreviation) similar to '%(#{state})%')"
        first_conditions << conditions.join(" AND ")

        first_conditions.each do |con|
          if radius.blank?
            @res = Restaurant.offer_active.geo_scope(:origin => [lat, lng]).joins({:city => :region}, :offers).
                where(con).
                group(Restaurant.col_list).order("#{webordering} distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
          else
            @res = Restaurant.offer_active.geo_scope(:origin => [lat, lng], :within => radius).joins({:city => :region}, :offers).
                where(con).
                group(Restaurant.col_list).order("#{webordering} distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
          end

          @restaurants = @restaurants + @res
        end
        @restaurants = @restaurants.uniq.first(@chain.max_number_of_locations) unless @restaurants.to_a.blank?
      end

    else
      @restaurants = Restaurant.offer_active.geo_scope(:origin => [lat, lng]).joins({:city => :region}, :offers).
          where(base_conditions).
          group(Restaurant.col_list).order("#{webordering} distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
    end

    # first_conditions = "(#{first_conditions.join(" OR ")})" unless first_conditions.blank?
    # second_conditions = "(#{second_conditions.join(" OR ")})" unless second_conditions.blank?

    @restaurants.map{|r| r.user_coordinate = [lat, lng]} if lat && lng
    @restaurants.map{|r| r.nil_to_blank_string }
    render :json => {status: true, restaurants: @restaurants.as_json(
        :only => [:id, :name, :app_display_text, :address, :app_display_text, :zipcode, :phone_number, :latitude, :longitude, :online_order_link, :online_order_support_status, :aloha_online_id, :delivery_radius_miles, :social_link, :agm_support, :external_partner_id.to_s, :online_order_support_status],
        :methods =>  [:today_open_hour, :restaurant_distance, :cloud_connect_site_id, :ncr_aloha_loyalty_store_id, :is_open, :city_label, :state_label, :country_label, :available_offers, :online_partner] )}

  end

  def index
    render :json=>{status: true, receipt: {offers: @chain.offers.as_json(:only=>[:id, :name])}}
  end

  # GET /api/v1/offers/1
  def show
    offerId = params[:id]
    render :json => Offer.where('id = ? and chain_id = ?', offerId, @chain.id)
  end

  def restaurants
    @id = params[:offer_id]
    @opt = RestaurantOffer.select('restaurants.id, restaurants.name').joins(:restaurant).
      where("restaurant_offers.offer_id = ? and restaurants.chain_id = ? and restaurants.active = ?", @id, @chain.id,true)

    render :json => @opt
  end
end
