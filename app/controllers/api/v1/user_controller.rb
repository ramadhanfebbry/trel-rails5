class Api::V1::UserController < Api::BaseController

  # update user password
  #
  #GET /api/v1/user/update_password
  #QUERY id
  #      password
  #      password_confirmation
  #      current_password
  #      appkey
  #      auth_token
  #
  def update_password
    p "params----before"
    p params
    check_and_build_params
    p "params ----after"
    p params

    if current_user.chain_id != @chain.id
      render :json => {status: false, notice: t(:user_not_found)} and return
    end

    if current_user.register_type.eql?(2)
      render :json => {:status => false, :notice => "Password cannot be reset for accounts created using Facebook Connect"} and return
    end

    current_password= CGI::escape(params[:current_password])
    current_password= URI.decode(params[:current_password])

    new_password= CGI::escape(params[:password])
    new_password= URI.decode(params[:password])

    new_password_confirmation= CGI::escape(params[:password_confirmation])
    new_password_confirmation= URI.decode(params[:password_confirmation])

    p "current password #{current_password}"
    p "new_password #{new_password}"
    p "new_password_confirmation #{new_password_confirmation}"

    if !current_user.valid_password?(current_password)
      render :json =>{:status=>false, notice: t(:incorrect_current_password)} and return
    end

    if new_password.nil? || new_password.length < Setting.user.password_size
      render :json =>{:status=>false, notice: t(:password_too_long_error, :password_size => Setting.user.password_size)} and return
    end

    if new_password != new_password_confirmation
      render :json =>{:status=>false, notice: t(:password_dont_match)} and return
    end

    if new_password !~ eval(Setting.user.password_regex)
      render :json =>{:status=>false, notice: t(:password_is_invalid)} and return
    end

    if current_user.reset_password!(new_password, new_password_confirmation)
      render :json =>{:status=>true, notice: t(:updated_account)}
    else
      render :json =>{:status=>false, notice: t(:error_updating_password)}
    end
  end

  def show
    userId = params[:id]
    user = User.where('id = ? and chain_id = ?', userId, @chain.id)
    if user.blank?
      render :json => {status: false, notice: t(:user_details_not_found)}
    else
      render :json => {status: true, user: user}
    end
  end


  def profile
    puts "=== IP"
    puts location = request.env["REMOTE_ADDR"]
    puts "=== IP"
    if current_user.chain_id == @chain.id
      render :json => {status: true, user: current_user.as_json(:methods => [:profile_pic, :favorite_location_name]).merge(
                 :milestone_points => current_user.user_milestone_points,
                 :weekly_receipt_approved => current_user.approved_receipt_count("weekly"),
                 :monthly_receipt_approved => current_user.approved_receipt_count("monthly"),
                 :yearly_receipt_approved => current_user.approved_receipt_count("yearly"),
                 :mall_employee    => current_user.try(:user_profile).try(:mall_employee),
                 :retailer         => current_user.try(:user_profile).try(:retailer)
             )}
    else
      render :json => {status: false, notice: t(:user_details_not_found)}
    end
  end

  def activity
    all_activities = []
    payments = []
    if @chain.id == current_user.chain_id
      all_activities << Receipt.where(:user_id => current_user.id, :chain_id => @chain.id, :is_receipt_barcode => false)
      #all_activities << PointHistory.where(:user_id => current_user.id, :description => ["Admin Push Point",
      #"Referer Incentive", "Referee Incentive", "Game Point", "Dashboard Push Point", "Signup"])
      all_activities << PointHistory.where( "user_id = ? AND group_no IN (1,2,3,4,5,6,7,8,9,10,11,12,18,20)", current_user.id )
      all_activities << PointHistory.where( "user_id = ? AND group_no = ? AND point > 0", current_user.id, 19) #query for guest DNA migration points
      all_activities << PaymentHistory.where(:user_id => current_user.id, :status => 0, :action => "payment")
      #"Dashboard Guest Relation",
      #"Dashboard Individual Job"
      #"Dashboard Push Point"

      all_activities = all_activities.flatten
      unless all_activities.blank?
        all_activities = all_activities.sort{|a,b| b.created_at <=> a.created_at}
        render :json => {
            :status=>true,
            receipts: all_activities.map{ |c|
              if c.class.to_s.eql?("Receipt")
                {:id => c.id, :activity_type => "points", :image_url => c.image_url, :last_transaction => c.last_transaction_for_user_activity_call}
              elsif c.class.to_s.eql?("PaymentHistory")
                {:id => c.id, :activity_type => "payments", :created_at => c.created_at, :amount => c.amount, :restaurant => c.try(:restaurant).try(:app_display_text)}
              else
                {:id => c.id, :activity_type => "points", :image_url => nil, :last_transaction => {:id => c.id, :subtotal => (c.point.to_i.to_s rescue ""), :total_points_earned => (c.point.to_i.to_s rescue ""), :base_points_earned => (c.point.to_i.to_s rescue ""), :admin_id => (c.user_id.to_s rescue ""), :status => 3 , :created_at => c.created_at}}
              end
            }
         }
      else
        render :json => {:status => false, :notice => t(:no_activities)}
      end
    else
      render :json => {status: false, notice: t(:user_details_not_found)}
    end
  end

  def forgot_password
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    params[:user]= {:email => params[:email].downcase,
      :chain_id => @chain.id}

    user = User.find_for_database_authentication(params[:user])
    if (user)
      if user.register_type == 2
        render :json => {:status => false, :notice => "Password cannot be reset for accounts created using Facebook Connect."} and return
      end
      pass = user.random_password
      chk = user.update_attributes(:password => pass, :forgot_password => true)
      if (chk)
        render :json => {:status => true, notice: t(:password_emailed)}
      else
        render :json => {:status => false, notice: t(:something_went_wrong)}
      end
    else
      if params[:migrate].to_i == 1
        migrate_result = @chain.forgot_password_migrate_w_ritas(params)
        return render :json => migrate_result if migrate_result != "CONT"
      elsif params[:migrate].to_i == 2
        migrate_result = @chain.forgot_password_migrate_w_guest_dna(params)
        return render :json => migrate_result if migrate_result != "CONT"
      end
      render :json => {:status => false, notice: ApiResponseText.get_message_chain_by_locale(@chain, (@chain.locales.try(:first).try(:id) || Locale.first.id), 8)}
    end
  end

  def generate_keychain
    begin
      begin
        keychain_value = Digest::SHA1.hexdigest([Time.current, rand, @chain.id].join)
        user_device = UserDevice.where(:keychain_value => keychain_value).first
      end until user_device.blank?
      saved = UserDevice.create(:keychain_value => keychain_value) rescue nil
    end until !saved.nil?
    render :json => {:status => true, :keychain_value => keychain_value}
  end

  def get_user_id
    lat = params['lat']
    long = params['long']

    if @chain == current_user.chain
      user_code_setting = @chain.user_code_setting
      user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
      user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
      if user_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"])
        if user_code_setting["user_metadata"].eql?(1)
          user_code, barcode = [current_user.id, user_code_setting["prefix"].to_s + current_user.id.to_s.add_zero_padding(user_code_setting["zero_padding"].to_i) + user_code_setting["postfix"].to_s]
          render :json => {:status => true, :usercode => barcode}
        else
          user_code, barcode = [current_user.id, user_code_setting["prefix"].to_s + current_user.phone_number.to_s.add_zero_padding(user_code_setting["zero_padding"].to_i) + user_code_setting["postfix"].to_s]
          render :json => {:status => true, :usercode => barcode}
        end
      else
        valid = false
        user_code, barcode = Barcode.generate_user_code_v2(current_user, @chain, user_code_setting, @app.id,lat , long)
        time_start = Time.zone.now
        while valid == false
          if UserCodeUser.where(:chain_id => @chain.id, :user_code => user_code).count > 0
            user_code, barcode = Barcode.generate_user_code_v2(current_user, @chain, user_code_setting, @app.id,lat , long)
          else
            begin
              expired_at = user_code_setting["timer"].to_i == 0 ? Time.current + 30.minutes : Time.current + user_code_setting["timer"].to_i.seconds
              UserCodeUser.create!(:chain_id => @chain.id, :user_code => user_code, :user_id => current_user.id, :expired_at => expired_at)
              valid = true
            rescue ActiveRecord::RecordNotUnique => e
              valid = false
              user_code, barcode = Barcode.generate_user_code_v2(current_user, @chain, user_code_setting, @app.id,lat , long)
            end
          end
          time_end = Time.zone.now
          if time_end - time_start > 25
            barcode = "NOCODE"
            length = 0
            valid = true
          end
        end
        render :json => {:status => true, :usercode => barcode}
      end
    elsif @chain == current_user.chain && @chain.user_code_setting.blank?
      render :json => {:status => false, :notice => "User code setting is not set !"}
    else
      render :json => {:status => false, :usercode => nil, :notice => "User and chain are not associated."}
    end

  end

  def pay_code
    lat = params['lat']
    long = params['long']

    if @chain == current_user.chain
      if current_user.braintree_user_created
        pay_code = REDIS.get "paycode_chain_#{@chain.id}"
        pay_code_setting = JSON.parse(pay_code) rescue nil
        pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
        #code = Barcode.generate_pay_code(current_user, @chain, @app.id,lat , long)
        Barcode.save_btree_customer_id_as_paycode(current_user, @chain, @app.id, lat, long)
        render :json => {:status => true, :code => pay_code_setting["prefix"].to_s + current_user.braintree_identifier.add_zero_padding(pay_code_setting["zero_padding"].to_i) + pay_code_setting["postfix"].to_s }
      else
        render :json => {:status => true, :code => nil }
      end
    else
      render :json => {:status => false, :code => nil, :notice => "User and chain are not associated or Pay code setting is not set."}
    end
  end

  def gift_code
    lat = params['lat']
    long = params['long']

    #if @chain == current_user.chain
    #  code = Barcode.generate_gift_code(current_user, @chain, @app.id,lat , long)
    #  render :json => {:status => true, :giftcard_code => code }
    #else
    #  render :json => {:status => false, :code => nil, :notice => "User and chain are not associated or gift code setting is not set."}
    #end
    if @chain == current_user.chain
      gift_code = @chain.gift_code_setting
      gift_code_setting = JSON.parse(gift_code.to_json) rescue nil
      gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE

      if gift_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["STATIC"])
        if gift_code_setting["default_code"]
          barcode = current_user.vantiv_gift_card_profile || "NOCODE"
        else
          gift_code, barcode = Barcode.generate_gift_code_v2(current_user, @chain, @app.id,lat , long)
        end
        render :json => {:status => true, :giftcard_code => barcode}
      else
        valid = false
        gift_code, barcode = Barcode.generate_gift_code_v2(current_user, @chain, @app.id,lat , long)
        time_start = Time.zone.now
        while valid == false
          if GiftCodeUser.where(:chain_id => @chain.id, :gift_code => gift_code).count > 0
            gift_code, barcode = Barcode.generate_gift_code_v2(current_user, @chain, @app.id,lat , long)
          else
            begin
              expired_at = gift_code_setting["timer"].to_i == 0 ? Time.current + 30.minutes : Time.current + gift_code_setting["timer"].to_i.seconds
              GiftCodeUser.create!(:chain_id => @chain.id, :gift_code => gift_code, :user_id => current_user.id, :expired_at => expired_at)
              valid = true
            rescue ActiveRecord::RecordNotUnique => e
              valid = false
              gift_code, barcode = Barcode.generate_gift_code_v2(current_user, @chain, @app.id,lat , long)
            end
          end
          time_end = Time.zone.now
          if time_end - time_start > 25
            barcode = "NOCODE"
            length = 0
            valid = true
          end
        end
        render :json => {:status => true, :giftcard_code => barcode}
      end
    elsif @chain == current_user.chain && @chain.gift_code_setting.blank?
      render :json => {:status => false, :notice => "Gift code setting is not set !"}
    else
      render :json => {:status => false, :giftcard_code => nil, :notice => "Gift and chain are not associated."}
    end
  end

  def giftcard_skin
    giftcard_skins = @chain.giftcard_skins

    unless giftcard_skins.blank?
      list = []
      giftcard_skins.each do |gs|
        list << { :id => gs.id, :title => gs.title, :description => gs.description,
                  :app_image_url => gs.app_image.try(:url), :email_header_image_url => gs.email_header_image.try(:url)}
      end
      render :json => {:status => true, :images => list}.to_json
    else
      render :json => {:status => false}
    end
  end

  def module_accessed
    mh = ModuleHistory.new(
     :user_id => (@chain.users.where(:authentication_token => params[:auth_token]).first.id rescue nil),
     :keychain => params[:keychain],
     :android_id => params[:android_id],
     :lat => params[:lat],
     :long => params[:long],
     :module_name => params[:module_name])
    if mh.save
      render :json => {:status => true}
    else
      render :json => {:status => false}
    end

  end

  def facebook_data
    user = current_user
    facebook_info = FacebookInfo.where(:user_id => user.id).first
    if facebook_info
      facebook_info.update_attributes(
        :fb_id => params[:fb_id],
        :fb_name => params[:fb_name],
        :fb_fname => params[:fb_fname],
        :fb_lname => params[:fb_lname],
        :fb_link => params[:fb_link],
        :fb_gender => params[:fb_gender],
        :fb_locale => params[:fb_locale],
        :fb_age_range => params[:fb_age_range]
      )
    else
      FacebookInfo.create(
          :chain_id => user.chain_id,
          :user_id => user.id,
          :fb_id => params[:fb_id],
          :fb_name => params[:fb_name],
          :fb_fname => params[:fb_fname],
          :fb_lname => params[:fb_lname],
          :fb_link => params[:fb_link],
          :fb_gender => params[:fb_gender],
          :fb_locale => params[:fb_locale],
          :fb_age_range => params[:fb_age_range]
      )
    end
    render :json => {:status => true }
  end

  # parameters
  # auth_token if available
  # android_id
  # keystore
  # latitude
  # longitude

  def geodata
    geo_data = AnalyticsGeodata.new(
        :user_id => (@chain.users.where(:authentication_token => params[:auth_token]).first.id rescue nil),
        :lat => params['lat'],
        :long => params['long'],
        :keychain => params['keychain'],
        :android_id => params['android_id'],
        :device_time => params['device_time']
    )

    setting = @chain.geo_data_setting

    unless setting.blank?
      ## inser to user geo data setting based on user_id if exist
      ## else based on android_id / keystore
      #UserGeoSetting.create(
      #    :user_id => (user.id rescue nil),
      #    :geo_data_setting_id => setting.id ,
      #    :keystore => params['keystore'],
      #    :android_id => params['android_id'],
      #    :device_type => (params['keystore'].blank? ? 2 : 1 ),
      #    :next_date => (Time.now + setting.n_days_to_post_from_today.days),
      #    :last_run_at => Time.now
      #)
      geo_data.save
      render :json => {
          :collect_start_time => setting.collecting_start_time,
          :collect_end_time => setting.collecting_end_time,
          :post_data_start_time => setting.collecting_post_start_time,
          :post_data_end_time => setting.collecting_post_end_time,
          :next_date => (Time.now + setting.n_days_to_post_from_today.days).to_date,
          :date_to_retry => (Time.now + setting.num_days_retry.days).to_date
      }
    else
      render :json => {:status => false, :notice => "Geo Data Seting is not set for this chain !"}
    end

  end

  def braintree_client_token
    begin
      btree_setting = @chain.btree_setting
      render :json => {:status => false, :notice => "Please add braintree setting for this chain."} and return if btree_setting.blank?
      passed = true
      btree_error = ""
      user = current_user
      btree_setting.set_env_setting
      unless user.braintree_user_created
        pos_location = @chain.pos_locations.first
        render :json => {:status => false, :notice => "Invalid APIKEY."} and return if pos_location.blank?
        paycode = Barcode.generate_pay_code_for_btree(user, @chain)
        status, message = BtreeSetting.create_new_customer_from_mobile_pay(pos_location.apikey, user, paycode)
        #status, message = btree_setting.create_new_braintree_customer(user)
        if status
          user.update_columns(:braintree_user_created => true, :braintree_identifier => paycode )
          passed = true
        else
          passed = false
          p "ERROR ====="
          p message
          btree_error = "We are sorry. Something went wrong. Please try again . If problem persists, log out and log back in or contact customer support from Info."
        end
      else
        paycode = user.braintree_identifier
      end

      render :json => {:status => false, :notice => btree_error} and return if passed == false
      #generate client_token
      client_token = Braintree::ClientToken.generate(
          :customer_id => paycode,
          :options => { :make_default => true }
      )
      render :json => {:status => true, :client_token => client_token}
    rescue => e
      p "rescue ======>"
      p e.message
      render :json => {:status => false, :notice => "We are sorry. Something went wrong. Please try again . If problem persists, log out and log back in or contact customer support from Info.", :code => nil}
    end

  end

  def update_profile
    begin
      user = current_user
      user_profile = user.user_profile || UserProfile.new(:user_id => user.id)
      user_profile.mall_employee = UserProfile.mall_employee_value(params[:mall_employee]) unless params[:mall_employee].blank?
      user_profile.retailer = params[:retailer] unless params[:retailer].blank?
      user_profile.avatar = params[:profilepic] unless params[:profilepic].blank?
      if user.register_type.eql?(User::USERS_TYPES['Facebook']) && user.email != params[:email]
        render :json => {:status => false, :message => "Email FB can't change, Profile update failed."} and return
      else
        user.email = params[:email]
      end unless params[:email].blank?
      user.favorite_location = params[:favorite_location]
      user.favorite_menu_item = params[:favorite_menu_item] unless params[:favorite_menu_item].blank?
      user.email = params[:email] unless params[:email].blank?
      user.first_name = params[:first_name] unless params[:first_name].blank?
      user.last_name = params[:last_name] unless params[:last_name].blank?
      user.last_name = params[:last_name] unless params[:last_name].blank?
      user.phone_number = params[:phone_number] unless params[:phone_number].blank?
      user.dob_day = params[:dob_day] unless params[:dob_day].blank?
      user.dob_month = params[:dob_month] unless params[:dob_month].blank?
      user.dob_year = params[:dob_year] unless params[:dob_year].blank?
      user.marketing_optin = User.marketing_optin_value(params[:marketing_optin])
      user.marketing_optin_texting = User.marketing_optin_texting_value(params[:marketing_optin_texting])
      user.app_usage_purpose = params[:app_usage_purpose] unless params[:app_usage_purpose].blank?
      user.gender = params[:gender] unless params[:gender].blank?
      user.user_profile = user_profile
      if !params[:favorite_location].blank?
        rest = @chain.restaurants.where("id = ? ",params[:favorite_location].to_i) rescue []
        render :json => {:status => false, :message => "Favorite Location not valid"} and return if rest.blank?
      end
      if !params[:email].blank? && user.register_type == User::USERS_TYPES['Facebook']
        render :json => {:status => false, :message => "Update Email are not allowed for Facebook type account."} and return
      end
      if user.save
        Delayed::Job.enqueue(UserMarketingUpdateJob.new(user))
        render :json => {
                   status: true,
                   notice: "Your profile information has been updated.",
                   user: user.as_json(
                       :methods => [:profile_pic, :favorite_location_name]
                   ).merge(
                       :milestone_points => user.user_milestone_points,
                       :mall_employee    => user.try(:user_profile).try(:mall_employee),
                       :retailer         => user.try(:user_profile).try(:retailer)
                   )}
      else
        p "---error exist---" * 10
        p user.errors.messages
        render :json => {status: false, notice: "We're sorry, this number or email  is already associated with an account. Please contact app support to update."}
      end
    rescue ActiveRecord::StatementInvalid => e
      render :json => {:status => false, :message => "Favorite Location not valid"} and return if rest.blank?
    rescue => e
      p e.message
      render :json => {status: false, notice: "We're sorry, this number or email  is already associated with an account. Please contact app support to update.", :dev_message => e.message}
    end

  end

  def get_oauth_token
    user = User.find_by_authentication_token(params[:auth_token]) rescue nil
    render :json =>  {status: false, :message => "Could not find the user"} and return if user.blank?

    access_grant = AccessGrant.where(client_id: params[:appkey], user_id: user.id).first rescue nil
    if access_grant.blank?
      access_grant = user.access_grants.create({:client_id => params[:appkey]}, :without_protection => true)
    end
    render :json => {status: true,
                     :access_token => access_grant.access_token,
                     :refresh_token => access_grant.refresh_token
    }
  end

  def get_olo_mobile_uri
    render :json =>  {status: false, :message => "Could not find the app key"} and return if params[:appkey].blank?
    application_key = ApplicationKey.where(:appkey => params[:appkey]).first
    render :json =>  {status: false, :message => "Could not find the app key"} and return if application_key.blank?
    application = application_key.application
    chain = application.chain
    state = "eyJQcm92aWRlciI6InJlbGV2YW50IiwiVXJsIjoiLyIsIkhhc2giOiJhV2l6Q1d2R0NoVDdzT0I2bURKS2pHU0FoY3pUdDduTXFVUU51cyt1QzlFPSIsIkJhc2tldElkIjpudWxsLCJWZW5kb3JJZCI6bnVsbCwiU2NoZW1lSWQiOm51bGx90"

    if chain.id == 1
      redirect_uri = "https%3a%2f%2fnekter.ololitesandbox.com%2faccount%2foauthcallback"
    elsif chain.id == 36
      redirect_uri = "http%3a%2f%2flepainquotidien.ololitesandbox.com%2faccount%2foauthcallback"
    end
    render :json =>  {status: true, :message => "true", :url => "http://trelevant.herokuapp.com/auth/olo/authorize_mobile_app?redirect_uri=#{redirect_uri}&state=#{state}&client_id=#{params[:appkey]}"}
  end

  def delete_old_bt_credit_card
    btree_setting = @chain.btree_setting
    render :json => {:status => false, :notice => "Please add braintree setting for this chain."} and return if btree_setting.blank?
    user = current_user
    btree_setting.set_env_setting
    render :json => BtreeSetting.delete_old_credit_card(user)
  end

  def notifications
    p params
    user = current_user
    @user_notification = UserActivityNotification.where(:chain_id =>  @chain.id, :unique_identifier => params[:notification_identifier]).first
    if @user_notification
      field_keys = params.keys - ["appkey", "auth_token", "notification_identifier", "controller", "action"]
      field_values = {}
      field_keys.each do |key|
        field = @user_notification.user_activity_notification_fields.where(:identifier => key).first
        field_values[key.to_sym] = params[key] if field
      end
      notification = ((@user_notification.notification % field_values) rescue @user_notification.notification)
      if user.push_to_device? && user.sign_in_device_type.to_s.downcase.eql?("android") && !user.device_token.blank?
        Delayed::Job.enqueue(UserActivityNotificationJob.new(@chain.id,user.device_token, notification, "android"), :run_at => Time.zone.now + @user_notification.delay_time.seconds)
      elsif user.push_to_device? &&  user.sign_in_device_type.to_s.downcase.eql?("iphone") && !user.device_token.blank?
        Delayed::Job.enqueue(UserActivityNotificationJob.new(@chain.id,user.device_token, notification, "iphone"), :run_at => Time.zone.now + @user_notification.delay_time.seconds)
      end
      render :json => {:status => true, :notification => notification}
    else
      render :json => {:status => false, :notice => "Could not find notification based on identifier", :notification => nil}
    end
  end

end
