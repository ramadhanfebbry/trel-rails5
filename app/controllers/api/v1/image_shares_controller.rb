class Api::V1::ImageSharesController < Api::BaseController

  def get_image_info    
    locale = params[:locale]
    dt = Date.today
    locale_id = Locale.find_by_key(locale ||= 'en').try(:id)      
    image = ChainImageShare.by_chain_and_locale(@chain.id, locale_id).active_image(dt).first

    unless image.blank?
      render :json => {
        :image_url => image.image_url,
        :title => image.chain_image_share_locales.first.title,
        :description => image.chain_image_share_locales.first.description,
        :fb_title => image.fb_title,
        :fb_description => image.fb_description,
        :twitter_title => image.twitter_title,
        :twitter_description => image.twitter_description
        }
    else
      render :json => {:status => false, :message => "There are no active image for this chain"}
    end
  end
end