class Api::V1::RestaurantsController < Api::BaseController

  skip_before_action :authorize_api_user!, :only => :index

  def index
    if params[:restaurant_id].present? || params[:aoid].present? || params[:external_partner_id].present?
      if params[:restaurant_id].present?
        restaurant = @chain.restaurants.find(params[:restaurant_id]) rescue nil
      elsif params[:aoid].present? && params[:external_partner_id].present?
        restaurant = @chain.restaurants.where(:aloha_online_id => params[:aoid], :external_partner_id => params[:external_partner_id]).first rescue nil
      elsif params[:aoid].present?
        restaurant = @chain.restaurants.where(:aloha_online_id => params[:aoid]).first rescue nil
      elsif params[:external_partner_id].present?
        restaurant = @chain.restaurants.where(:external_partner_id => params[:external_partner_id]).first rescue nil
      end

      if restaurant.blank?
        render :json => {:status => false, :restaurant => nil}
      else
        restaurant.nil_to_blank_string
        render :json => {
            :status => true,
            :restaurant => restaurant.as_json(:methods =>  [:today_open_hour, :restaurant_distance, :cloud_connect_site_id, :ncr_aloha_loyalty_store_id, :is_open, :city_label, :state_label, :country_label, :available_offers, :ts_pay_support, :online_partner, :operating_hours, :location_id])
        }
      end
    else
      render :json => {:status => false, :restaurant => nil}
    end
  end

  def nearby
    base_conditions   = "restaurants.chain_id = #{@chain.id}"

    lat = params[:lat]
    lng = params[:lng]
    psearch = params[:search].to_s.strip

    first_conditions  = []
    second_conditions   = []
    @restaurants = []
    results_from_zipcode = []
    radius = nil

    unless psearch.blank?
      search_criteria = CGI.unescape(psearch)
      if /\A\d+\z/.match(search_criteria)
        results_from_zipcode = @chain.restaurants.where(:zipcode => search_criteria, :status => true)
        restaurant = results_from_zipcode.first rescue nil
        if restaurant
          search_criteria = "#{restaurant.try(:city).try(:name)},#{restaurant.try(:region).try(:abbreviation)}"
          lat = restaurant.latitude
          lng = restaurant.longitude
        else
          address = search_criteria.to_region rescue ""
          lat = search_criteria.to_lat rescue 0
          lng = search_criteria.to_lon rescue 0
          search_criteria = address
          search_criteria = "" if search_criteria.nil?
          lat = 0 if lat.nil?
          lng = 0 if lng.nil?

          if search_criteria.blank?
            @restaurants = []
            render :json => {status: true, restaurants: @restaurants.as_json(
                       :only => [:id, :name, :app_display_text, :address, :app_display_text, :zipcode, :phone_number, :latitude, :longitude, :online_order_link, :beacon_serial_number, :beacon_uuid, :location_qrcode_identifier, :social_link, :external_partner_id.to_s, :online_order_support_status],
                       :methods =>  [:today_open_hour, :restaurant_distance, :is_open, :city_label, :state_label, :country_label, :available_offers, :operating_hours, :online_partner, :location_id])}
            return
          end
        end
        radius = 50
      end
      search_criterias = search_criteria.split(",")

      city = search_criterias.first.strip.downcase
      state = search_criterias.last.strip.downcase || city

      if search_criterias.size == 1
        conditions = []
        conditions << base_conditions
        conditions << "lower(regions.abbreviation) = '#{state}'"
        first_conditions << conditions.join(" AND ")

        conditions = []
        conditions << base_conditions
        conditions << "lower(regions.name) similar to '%#{state}%'"
        first_conditions << conditions.join(" AND ")

        conditions = []
        conditions << base_conditions
        conditions << "lower(cities.name) similar to '%(#{city})%'"
        first_conditions << conditions.join(" AND ")

        count = 0
        first_conditions.each do |con|
          count = count + 1
          conditions = []
          conditions << base_conditions
          conditions << con unless con.blank?
          conditions = conditions.join(" AND ")
          if radius.blank?
            @restaurants = Restaurant.active.geo_scope(:origin => [lat, lng]).joins(:city => :region).
                where(conditions).
                group(Restaurant.col_list).order("distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
          else
            @restaurants = Restaurant.active.geo_scope(:origin => [lat, lng], :within => radius).joins(:city => :region).
                where(conditions).
                group(Restaurant.col_list).order("distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)

          end
          break unless @restaurants.to_a.blank?
        end

        if count == 1
          if @restaurants.to_a.blank? == false && @restaurants.to_a.size < @chain.max_number_of_locations
            reg = @restaurants.to_a.first.region.id
            conditions = []
            conditions << base_conditions
            conditions << "regions.id = #{reg}"
            conditions = conditions.join(" AND ")
            if radius.blank?
              res = Restaurant.active.geo_scope(:origin => [lat, lng]).joins(:city => :region).where(conditions).group(Restaurant.col_list).order("distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
            else
              res = Restaurant.active.geo_scope(:origin => [lat, lng], :within => radius).joins(:city => :region).where(conditions).group(Restaurant.col_list).order("distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
            end

            @restaurants = @restaurants + res
            # @restaurants = @restaurants.uniq.first(@chain.max_number_of_locations)
            @restaurants = @restaurants.uniq

          end
        end

      elsif search_criterias.size > 1
        conditions = []
        conditions << base_conditions
        conditions << "lower(regions.abbreviation) = '#{state}'"
        conditions << "lower(cities.name) similar to '%(#{city})%'"
        first_conditions << conditions.join(" AND ")

        conditions = []
        conditions << base_conditions
        conditions << "lower(regions.name) similar to '%(#{state})%'"
        conditions << "lower(cities.name) similar to '%(#{city})%'"
        first_conditions << conditions.join(" AND ")

        conditions = []
        conditions << base_conditions
        conditions << "(lower(regions.name) similar to '%(#{state})%' OR lower(regions.abbreviation) similar to '%(#{state})%')"
        first_conditions << conditions.join(" AND ")

        first_conditions.each do |con|
          if radius.blank?
            @res = Restaurant.active.geo_scope(:origin => [lat, lng]).joins(:city => :region).
                where(con).
                group(Restaurant.col_list).order("distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
          else
            @res = Restaurant.active.geo_scope(:origin => [lat, lng], :within => radius).joins(:city => :region).
                where(con).
                group(Restaurant.col_list).order("distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
          end

          @restaurants = @restaurants + @res
        end
        #@restaurants = @restaurants.uniq.first(@chain.max_number_of_locations) unless @restaurants.to_a.blank?
        @restaurants = @restaurants.uniq unless @restaurants.to_a.blank?
      end

      if results_from_zipcode.present?
        @restaurants = results_from_zipcode + @restaurants
      end

      @restaurants = @restaurants.uniq.first(@chain.max_number_of_locations)
    else
      @restaurants = Restaurant.active.geo_scope(:origin => [lat, lng]).joins(:city => :region).
        where(base_conditions).
        group(Restaurant.col_list).order("distance asc, restaurants.name asc").limit(@chain.max_number_of_locations)
    end

    # first_conditions = "(#{first_conditions.join(" OR ")})" unless first_conditions.blank?
    # second_conditions = "(#{second_conditions.join(" OR ")})" unless second_conditions.blank?

    @restaurants.map{|r| r.user_coordinate = [lat, lng]} if lat && lng
    @restaurants.map{|r| r.nil_to_blank_string }
    render :json => {status: true, restaurants: @restaurants.as_json(
                       :only => [:id, :name, :app_display_text, :address, :app_display_text, :zipcode, :phone_number, :latitude, :longitude, :online_order_link, :beacon_serial_number, :beacon_uuid, :location_qrcode_identifier, :social_link, :external_partner_id.to_s, :online_order_support_status],
    :methods => [:today_open_hour, :restaurant_distance, :is_open, :city_label, :state_label, :country_label, :available_offers, :operating_hours, :online_partner, :location_id] )}
  end
end
