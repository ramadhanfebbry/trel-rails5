class Api::V1::RafflesController < Api::BaseController
  before_action :find_user

  def show
    raffle = @chain.raffles.where("start_date <= ? AND end_date >= ? AND active IS TRUE", Time.zone.now, Time.zone.now).select("id, title, description").first rescue []
    raffle_informations = raffle.raffle_additional_informations.select("id,field_name, app_display_text, description") rescue []
    raffle_steps = raffle.raffle_steps.where(:active => true).order("id asc") rescue []
    qualify = false

    participation_ids = []
    raffle_step_participation = []
    program = raffle.raffle_user_informations.where("user_id = ?", @user.id).first rescue []
    if !program.blank?
      participation = program.raffle_participation_histories
      participation_ids = participation.map{|b| b.raffle_step_id}

      available_steps = raffle_steps.reject{|c| participation_ids.include? c.id}
      qualify = available_steps.blank? ? true : false

      if !program.date_of_submission.blank?
        qualify = false
      end
    end

    raffle_steps_array = []
    raffle_steps.each do |raffle_step|
      raffle_participation = participation.select{|p| p.raffle_step_id == raffle_step.id}.first rescue []
      show_custom_api_response = false
      show_custom_api_response = true if !raffle_participation.blank? && raffle_participation.custom_api_executed == false && !raffle_step.custom_api_response.blank?
      raffle_participation.update_column(:custom_api_executed, true) unless raffle_participation.blank?
      raffle_steps_array << {step_id: raffle_step.id, display_title: raffle_step.display_title, display_subtitle: raffle_step.display_subtitle, display_description: raffle_step.display_description, code: raffle_step.code, :custom_api_response => raffle_step.custom_api_response, :show_custom_api_response => show_custom_api_response, status: (participation_ids.include? raffle_step.id) ? 1 : 0 }
    end

    render :json => {:raffle => raffle,
     :additional_information_fields => raffle_informations,
     :raffle_steps => raffle_steps_array,
     :qualify_for_raffle => qualify }.to_json
  end

  def submit_code
    raffle = Raffle.where("id = ? AND start_date <= ? AND end_date >= ? AND active IS TRUE", params[:raffle_id], Time.zone.now, Time.zone.now).first rescue nil
    render :json => {:status => false, :message => "We are sorry! Looks like something went wrong. Please try again later."} and return if raffle.blank?

    raffle_step = raffle.raffle_steps.find(params[:raffle_step_id]) rescue nil
    render :json => {:status => false, :message => "We are sorry! Looks like something went wrong. Please try again later."} and return if raffle_step.blank? || raffle_step.active == false

    render :json => {:status => false, :message => "Code submitted invalid. Please try again."} and return if raffle_step.code != params[:code]

    program = raffle.raffle_user_informations.where("user_id = ?", @user.id).first rescue nil
    if program.blank?
       program = RaffleUserInformation.create(:raffle_id => raffle.id, :user_id => @user.id, :user_email => @user.email)
    end

    participation = program.raffle_participation_histories.where("raffle_step_id = ?", raffle_step.id) rescue nil
    render :json => {:status => false, :message => "Looks like you have already completed this step."} and return if !participation.blank?

    participation = program.raffle_participation_histories.build(:raffle_step_id => raffle_step.id)

    if participation.save
      custom_api_response = raffle_step.custom_api_response.blank? ? "Congratulations! You are one step closer to your goal!" : raffle_step.custom_api_response
      render :json => {:status => true, :message => custom_api_response }
    else
      render :json => {:status => false, :message => "We are sorry! Looks like something went wrong. Please try again later."}  
    end
  
  end

  def submit_info
    render :json => {:status => false, :message => "We are sorry! Looks like something went wrong. Please try again later."} and return if params[:additional_info].blank?
    raffle = Raffle.where("id = ? AND start_date <= ? AND end_date >= ? AND active IS TRUE", params[:raffle_id], Time.zone.now, Time.zone.now).first rescue nil
    render :json => {:status => false, :message => "We are sorry! Looks like something went wrong. Please try again later."} and return if raffle.blank?
    program = raffle.raffle_user_informations.where("user_id = ?", @user.id).first rescue nil
    render :json => {:status => false, :message => "We are sorry! Looks like something went wrong. Please try again later."} and return if program.blank?
    flag = false

    if program.date_of_submission.blank?
      raffle_steps = raffle.raffle_steps.where(:active => true).order("id asc") rescue []

      participation_ids = []
      participation = program.raffle_participation_histories
      participation_ids = participation.map{|b| b.raffle_step_id}

      available_steps = raffle_steps.reject{|c| participation_ids.include? c.id}
      qualify = available_steps.blank? ? true : false

      information_submited = params[:additional_info].to_a.map{|x| x[0] + "," + x[1].gsub(",","") }.join(",")

      if qualify
        program.update_attributes(:date_of_submission => Time.zone.now, :information_submited => information_submited )
        flag = true
      end
    end

    if flag
      render :json => {:status => true, :message => "Thank you! Your information has been saved successfully. Hope you win!"}
    else
      render :json => {:status => false, :message => "We are sorry! Looks like something went wrong. Please try again later."}
    end
  end

  private

  def find_user
    @user = @chain.users.where("authentication_token = ?", params[:auth_token]).first
    render :json => {:status => false, :message => "We are sorry! Looks like something went wrong. Please try again later."} and return if @user.blank?
  end
  
end
  
