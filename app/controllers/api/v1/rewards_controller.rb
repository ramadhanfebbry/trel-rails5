require 'timeout'
class Api::V1::RewardsController < Api::BaseController
  # Returns rewards pushed into user’s wallet, rewards created for all users belonging to a chain
  #
  #GET /api/v1/rewards/get
  #QUERY appkey
  #
  def get
    user = current_user
    new_push_reward = {}
    unless @chain.new_push_reward.blank?
      new_push_reward =  {:new_reward_notification => @chain.new_push_reward}
      ### set the notification to nil again
      user.update_column(:new_reward_notification => false)
    end
    rewards = Reward.where(:chain_id => @user.chain.id, :status => "active")
    rewards_in_wallet = RewardWallet.includes(:reward).where("rewards.status = 'active' AND reward_wallets.user_id = ?", user.id)

    render :json => {status: true, rewards: rewards, user_rewards: rewards_in_wallet}
  end

  # Returns rewards pushed into user’s wallet and rewards created for all users belonging to a chain
  #
  #GET /api/v1/rewards/
  #QUERY appkey
  #
  def index
    start_time = Time.now
    p "rewards api # index start in =#{start_time}"
    user= current_user

    chain_rewards= Reward.of_chain(@chain.id, only_active=true, only_in_effect=true, only_unexpired=true)
    user_rewards= Reward.of_user(user.id, include_removed=false, include_claimed=false, include_gifted=false, twenty_day_exp = true)

    rewards = (chain_rewards + user_rewards).compact  #union both
    show_gifter = @app.application_keys.where(:appkey => params[:appkey]).first.show_gifter rescue false
    show_expired_reward = @app.application_keys.where(:appkey => params[:appkey]).first.show_expired_reward rescue true
    rewards = rewards.select{|a| !a.gifter} unless show_gifter
    rewards.sort! do |a, b|
      if a.gifter
        -1
      elsif b.gifter
        1
      elsif(a.expired.eql? b.expired)
        unless a.priority_number.eql?(b.priority_number)
          (b.priority_number <=> a.priority_number)
        else
          if a.points == 0 && b.points == 0
            (a.expiryDate <=> b.expiryDate)
          else
            (a.points <=> b.points)
          end
        end
      else
        a.expired ? 1 : -1
      end
    end

    ## parameter from phone, local timestamp
    device_timestamp = params['device_timestamp']

    rewards.each_with_index do |r, i|
      r.sort_by_id= i
      r.points = r.points/(@chain.user_points || 1)
      ## reward expired based on comparing the local phone timestamp with the current reward expired
      unless device_timestamp.blank?

        ## check it if the reward has expired or no
        unless r.expiryDate.blank?
          # parse the device time stamp and do not convert to any time zone
          # example : "1/14/2015 22:00:00"  this is without timezone
         # puts device_timestamp
         # puts "**" * 100
          #device_date_expired = DateTime.strptime(device_timestamp)
          device_date_expired = DateTime.strptime(device_timestamp, '%m/%d/%Y %H:%M:%S')


          ## get from reward wallet expiry
          ## convert the date without timezone
          reward_date_expired = r.expiryDate
          reward_expiry_date = reward_date_expired.to_date
          reward_expiry_hour = reward_date_expired.hour
          reward_expiry_min = reward_date_expired.min
          result_reward_expiry = Time.parse(reward_date_expired.strftime('%Y-%m-%d %I:%M:%S UTC')).to_s.
              to_datetime.change(:hour => 23, :min => 59, :sec => 59)

          # compare it ----

          #puts "DEVICE DATE EXPIRED  = #{device_date_expired}"
          #puts "result_reward_expiry = #{result_reward_expiry}"
          #comparation = ((device_date_expired.to_datetime - result_reward_expiry.to_datetime) / 1.hour).to_i
          comparation = TimeDifference.between(device_date_expired.to_time,
                                               result_reward_expiry.to_time).
              in_hours

          #puts comparation
          #puts "-------------------------"

          if device_date_expired < result_reward_expiry
            r.device_expired = false
          elsif device_date_expired > result_reward_expiry
            r.device_expired = true
          end
          #puts comparation
          #puts "comparation"
          #puts "asdfasdfasdfadsfa"
          #puts comparation.to_i
          system_time_expired = result_reward_expiry.to_datetime.to_date + 24.hour < Date.today
          if comparation.to_i > 23 or system_time_expired#or comparation.to_i == 0
            #puts "enter wind " * 100
            ## if comparation is bigger than this. there would be a case that there user play with the time zone
            r.device_expired = r.expired
          end
        end
      else
        # using system time comparation
        r.device_expired = r.expired
      end
    end


    new_push_reward = {}
    user = current_user
    locale = user.locale.key rescue "en"
    ch = user.chain
    if ch.new_reward_notif?(locale) and user.new_reward_notification == true and rewards.size > 0
      new_push_reward =  {:new_reward_notification => ch.get_reward_notif(locale)}
      ### set the notification to nil again
      user.update_column(:new_reward_notification,false)
    end

    reward_json = []

    rewards.map do |r|
      if r.reward_type == Reward::TYPES["REGULAR"]
        a = {:id => r.id, :name => r.name, :points => r.points, :fineprint => r.fineprint,
         :effectiveDate => without_timezone(r.effectiveDate), :expiryDate => without_timezone(r.expiryDate), :chain_id => r.chain_id,
         :survey_id => r.survey_id, :reward_type => r.reward_type, :POSCode => r.POSCode,
         :expired => r.expired, :sort_by_id => r.sort_by_id, :gifter => r.gifter ,
         :image_url => r.attachment.url.include?("missing") ? "" : r.reload.attachment.url,
         :image_thumbnail_url => r.thumbnail.url.include?("missing") ? "" : r.reload.thumbnail.url,
         :additional_fields => r.additional_informations, :device_reward_expired =>  r.device_expired
        }
      else
        a =
        {:id => r.id, :name => r.name, :points => r.points, :fineprint => r.fineprint,
         :effectiveDate => without_timezone(r.effectiveDate), :expiryDate => without_timezone(r.expiryDate), :chain_id => r.chain_id,
         :survey_id => r.survey_id, :reward_type => r.reward_type, :POSCode => r.POSCode,
         :expired => r.expired, :sort_by_id => r.sort_by_id, :gifter => r.gifter ,
         :image_url => r.attachment.url.include?("missing") ? "" : r.reload.attachment.url,
         :image_thumbnail_url => r.thumbnail.url.include?("missing") ? "" : r.reload.thumbnail.url,
         :new_reward => r.new_reward,
         :additional_fields => r.additional_informations, :device_reward_expired =>  r.device_expired
        }
      end
      
      unless show_expired_reward
        reward_json << a unless r.expired
      else
        reward_json << a
      end
    end
    p "rewards api # index END in =#{Time.now - start_time}"
    render :json => {status: true, balance: {:points => (user.points/(@chain.user_points || 1)),
                                             :milestone_points => (user.point_threshold.to_f/(@chain.user_points || 1))},
        rewards_image: @chain.rewards_image.exists? ? @chain.rewards_image.url : "", 
        rewards: reward_json
    }.merge!(new_push_reward).to_json
  end

  # Remove a reward from user's reward wallet
  #
  #DELETE /api/v1/rewards/:reward_id
  def destroy
    user= current_user
    reward = Reward.find(params[:id])
    reward_in_wallets = RewardWallet.where("user_id = ? AND reward_id = ? AND status != ?", user.id, reward.id, RewardWallet::STATUS[:REMOVED])
    unless reward_in_wallets.blank?
      p "in----------"
      reward_in_wallets.each do |rw|
        reward= rw.reward.dup
        reward.id = rw.reward.id
        reward.isExpired ||= rw.expiry_date.to_date < Time.zone.now.to_date if rw.expiry_date # expired in wallet
        reward.expiryDate = rw.expiry_date  if rw.expiry_date
        if reward.is_expired?
          p "is expired......."
          rw.update_attribute(:status, RewardWallet::STATUS[:REMOVED])
          render :json => {status: true, notice: t(:reward_removed)} and return
        end
      end
    end
    render :json => {status: false, notice: t(:reward_not_expired_not_allowed_to_delete)}
  end

  # A user claims a reward
  #
  #POST /api/v1/rewards/:reward_id
  #  def claim
  #    user= current_user
  #    restaurant= Restaurant.find(params[:location])
  #    latitude= params[:lat]
  #    longitude= params[:lng]
  #
  #    reward= Reward.find(params[:reward_id])
  #    begin
  #      user.claim(reward, restaurant, latitude, longitude)
  #      render :json => {status: true, notice: t(:claim_updated)}
  #    rescue Exceptions::InsufficientUserPointsError
  #      render :json => {status: false, notice: t(:dont_have_enough_pints_reward)}
  #    rescue
  #      render :json => {status: false, notice: t(:cant_submit_request)}
  #    ensure
  #      #this_code_will_execute_always()
  #    end
  #  end
  def claim
    # begin
      confirm = params[:warn] || "false"

      user= current_user
      latitude= params[:lat]
      longitude= params[:lng]

      restaurant= Restaurant.find(params[:location])
      render :json => {status: false, notice: "Restaurant is not active"} and return unless restaurant.status
      reward= Reward.find(params[:reward_id])
      
      ## if its only show the message
      if confirm == "true"
        return show_confirmation_claim
      end

      case reward.reward_type
        when Reward::TYPES["ONE_TIME"], Reward::TYPES["MILESTONE"], Reward::TYPES["PUSH_REWARD"], Reward::TYPES["PROMOTION"], Reward::TYPES["INCENTIVE"], Reward::TYPES["GIFTABLE"], Reward::TYPES["BIRTHDAY"]
          user_reward_wallets = RewardWallet.where("user_id = ? AND reward_id = ? AND date(expiry_date) >= ?", user.id, reward.id, Date.current).order("created_at asc")
          render :json => {status: false, notice: "Reward expired"} and return if user_reward_wallets.blank?
          user_reward_claimed = user_reward_wallets.select{|r| r.status == RewardWallet::STATUS[:CLAIMED]}
          user_reward_active = user_reward_wallets.select{|r| r.status == RewardWallet::STATUS[:ACTIVE] || r.status == RewardWallet::STATUS[:REDEEMING]}
          render :json => {status: false, notice: "Reward already used"} and return if (!user_reward_claimed.blank? &&  user_reward_active.blank?)
      end

      ## else claim the reward, and return the staff code and timer also
      #    begin
      if user.points >= reward.points
        render :json => {status: false, notice: t(:dont_have_enough_pints_reward)} and return if @chain.chain_setting.hybrid_milestone_regular_rewards_enabled && (user.point_threshold || 0) < reward.points
        p "----CLAIMED--"*30
        if @chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM APP"])
          staffcode, length = staffcode_generate(reward, restaurant, @chain, user) #generate staffcode
          user.claim_with_staffcode_and_timer(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
        elsif @chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
          fake_code, length = user.check_fake_reward_transactions(@chain, reward) rescue "CONT"
          if fake_code == "CONT"
            staffcode, length = staffcode_generate(reward, restaurant, @chain, user) #generate staffcode
            user.claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
          else
            render :json => {:status => true, :notice => t(:claim_updated),
                             :reward_code => fake_code.add_zero_padding(length.to_i), :reward_timer => reward_timer_generate} and return
          end
        elsif @chain.on_hybrid_setting_reward_redeem_flow?
          if @chain.on_hybrid_setting_pos_based_rule?(restaurant)
            fake_code, length = user.check_fake_reward_transactions(@chain, reward) rescue "CONT"
            if fake_code == "CONT"
              staffcode, length = staffcode_generate(reward, restaurant, @chain, user) #generate staffcode
              user.claim_for_pos_process(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
            else
              render :json => {:status => true, :notice => t(:claim_updated),
                               :reward_code => fake_code.add_zero_padding(length.to_i), :reward_timer => reward_timer_generate} and return
            end
          elsif @chain.on_hybrid_setting_app_based_rule?(restaurant)
            staffcode, length = staffcode_generate(reward, restaurant, @chain, user) #generate staffcode
            user.claim_with_staffcode_and_timer(reward, restaurant, latitude, longitude, staffcode, params['additional_info'])
          end
        end
        p t(:claim_updated)
        p staffcode
        p reward_timer_generate
        render :json => {:status => true, :notice => t(:claim_updated),
                         :reward_code => staffcode.add_zero_padding(length.to_i), :reward_timer => reward_timer_generate}
        p " -------- rendered"
      else
        p "----NOT CLAIMED--"*30
        render :json => {status: false, notice: t(:dont_have_enough_pints_reward)}
        p "------ rendered"
      end
    # rescue => e
    #   puts "error ------------ #{e.inspect}"
    #   render :json => {status: false, notice: t(:reward_claim_error)}
    # end

    #    rescue Exceptions::InsufficientUserPointsError
    #      render :json => {status: false, notice: t(:dont_have_enough_pints_reward)}
    #    rescue
    #      render :json => {status: false, notice: t(:cant_submit_request)}
    #    ensure
    #this_code_will_execute_always()
    #    end
  end

  def locate
    @restaurants = Restaurant.active.geo_scope(:origin => [params[:lat], params[:lng]])
    .where('restaurants.chain_id = ?', @chain.id).group(Restaurant.col_list)
    .order('distance asc').limit(Setting.API.rewards.nearby_limit)

    render :json => {status: true, restaurants: @restaurants.as_json(
        :only => [:id, :name, :app_display_text, :address, :app_display_text, :zipcode, :phone_number, :latitude, :longitude])}
  end

  def activity
    user = current_user
    show_expired_reward_listing = @app.application_keys.where(:appkey => params[:appkey]).first.show_expired_reward_activity_listing rescue true
    if @chain.users.include?(user)
      reward_transactions = RewardTransaction.select("restaurant_id, reward_id, user_id, created_at").where(:user_id => user.id, :redeeming => false).order("created_at desc")
      #reward_transactions = reward_transactions.group_by(&:reward_id).sort_by {|k,v| v}.reverse
      #reward_transactions = reward_transactions.map{|x| x[1]}.flatten

      reward_activity_listing = []
      reward_transactions.each do |trans|
        reward = trans.reward
        reward_activity_listing << { reward: (reward rescue nil), claim_date: trans.created_at }
      end

      if show_expired_reward_listing
        user_rewards= Reward.expired_reward_and_not_redeemed_of_user(user.id)
        rewards = (user_rewards).compact  #union both
        rewards.each do |reward|
          reward_activity_listing << { reward: (reward rescue nil), claim_date: nil }
        end

      end
      render :json => {status: true, activities: reward_activity_listing
      }.to_json
    else
      render :json => { status: false, notice: "Sorry something went wrong. Please try again."}
    end

  end

  def test_locate
  end

  def test_claim
  end

  def share
    reward = Reward.find params[:id]
    locale = params[:locale] || 'en'
    app_locale = Locale.where(:key => locale).first
    reward_share = @chain.reward_share_texts(:locale_id => app_locale.id).first rescue nil

    render :json => {:status => true,:share_text => (reward_share.send("share_text_#{params[:platform].eql?("1") ? "facebook" : "twitter"}")  % { :reward_name => reward.name} rescue nil)}
  end

  def tap_to_gift
    user = current_user
    reward = Reward.find params[:reward_id]
    #return if reward selected is not giftable reward
    render :json => {:status => false, :message => "Please select giftable reward"} and return unless reward.giftable?
    promotion = Promotion.where(:promotable_type => "Reward", :promotable_id => reward.id, :chain_id => reward.chain_id).first
    #return if reward doesn't have promotion
    render :json => {:status => false, :message => "Please add this reward to promotion"} and return if promotion.blank?

    reward_wallet = RewardWallet.where("user_id = ? AND reward_id = ? AND gifter IS TRUE AND status = ? AND expiry_date > ?", user.id, reward.id, RewardWallet::STATUS[:ACTIVE], Time.current).first
    #return if reward wallet nil
    render :json => {:status => false, :message => "There is no giftable reward on user reward wallet"} and return if reward_wallet.blank?

    notification = reward.giftable_notification_template(user)
    generated_code = PromoCode.generate_sample_codes(1).first
    #with warn params
    render :json => {:status => true, :warning => notification[:warn_message] % {
        :chain_name => user.chain.name,
        :reward_fine_print => reward.fineprint,
        :number_of_time => reward.number_of_times_gifted,
        :reward_title => reward.name,
        :code => generated_code,
        :promo_expiration_date => (promotion.expiry_date.strftime("%b %d, %Y") rescue nil)
    }}  and return if params["warn"].eql?("true")

    #if already gifted
    render :json => {:status => false, :message => "Already gifted"}  and return if reward_wallet.status.eql?(RewardWallet::STATUS[:GIFTED])

    if reward_wallet and reward_wallet.gifter
      reward_wallet.update_attribute(:status, RewardWallet::STATUS[:GIFTED])
      promocode = PromoCode.new(:code => generated_code, :shared => true, :promotion_id => promotion.id, :chain_id => user.chain_id, :from_user_id => user.id)
      promocode.save
      render :json => {:status => true,
                       :email_subject => notification[:subject] % {
                           :chain_name => user.chain.name,
                           :reward_fine_print => reward.fineprint,
                           :number_of_time => reward.number_of_times_gifted,
                           :reward_title => reward.name,
                           :code => generated_code,
                           :promo_expiration_date => (promotion.expiry_date.strftime("%b %d, %Y") rescue nil)
                       },
                       :email_body => notification[:content] % {
                           :chain_name => user.chain.name,
                           :reward_fine_print => reward.fineprint,
                           :number_of_time => reward.number_of_times_gifted,
                           :reward_title => reward.name,
                           :code => generated_code,
                           :promo_expiration_date => (promotion.expiry_date.in_time_zone.strftime("%b %d, %Y") rescue nil)
                       }}
    else
      render :json => {:status => false, :message => "Sorry something went wrong. Please try again."}
    end

  end

  def active
    t_zone = Time.current
    user = current_user
    chain_regular_rewards= Reward.of_chain(@chain.id, only_active=true, only_in_effect=true, only_unexpired=true).user_qualified(user.points).count
    active_reward_wallet = RewardWallet.includes(:reward).where("reward_wallets.status = ? AND date(TIMEZONE('UTC', reward_wallets.expiry_date) AT TIME ZONE '#{t_zone.strftime('%Z')}') >= ? AND rewards.points <= ? AND reward_wallets.user_id = ?", 1, t_zone.strftime("%Y-%m-%d"), user.points, user.id).count
    if (active_reward_wallet + chain_regular_rewards) > 0
      notification = @chain.active_reward_notifications.where(:locale_id => user.locale_id).first
      render :json => {:status => true, :message => notification.try(:notification), :cancel_field_label => notification.try(:cancel_button_text), :continue_field_label => notification.try(:continue_button_text)}
    else
      render :json => {:status => false, :message => nil, :cancel_field_label => nil, :continue_field_label => nil}
    end
  end

  private

  def show_confirmation_claim
    question = "Are You sure?"
    body = "Please do NOT click 'Confirm' until you are in front of our staff. Voucher valid for 2 minutes only."
    ## get the warning based on user locale
    begin
      key = current_user.locale.key
      content = REDIS.hget "chain_#{@chain.id}_warning_claim", key+"_warning_claim"
      content = JSON.parse(content)
      question = content["question"]
      body = content["body"]
    rescue => e
      puts "API:RewardController::show_confirmation_claim = #{e.inspect}"
    end
    render :json => {:warn_tile => question,:warn_body =>  body}
  end

  def staffcode_generate(reward, restaurant, chain, user)
    if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
       p "REWARD REDEEM POS generation"
       staffcode, length = Reward.pos_claim_staffcode_generate(chain, user)
       p staffcode
       return [staffcode, length]
      return Reward.pos_claim_staffcode_generate(chain, user)
    elsif chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM APP"])
      if reward.static_pos_code_setup?
        return reward.POSCode
      elsif chain.reward_code_uploaded_mode?
        return chain.get_reward_code_from_uploaded_code(user, reward, restaurant)
      elsif chain.reward_code_fishbowl_promotion_mode?
        return reward.get_fishbowl_promotion_code(user, restaurant)
      elsif chain.reward_code_from_relevant_mode?
        return Barcode.generate_reward_code(chain)
      end
    elsif chain.on_hybrid_setting_reward_redeem_flow?
      if chain.on_hybrid_setting_pos_based_rule?(restaurant)
        return Reward.pos_claim_staffcode_generate(chain, user)
      elsif chain.on_hybrid_setting_app_based_rule?(restaurant)
        if reward.static_pos_code_setup?
          return reward.POSCode
        elsif chain.reward_code_uploaded_mode?
          return chain.get_reward_code_from_uploaded_code(user, reward, restaurant)
        elsif chain.reward_code_fishbowl_promotion_mode?
          return reward.get_fishbowl_promotion_code(user, restaurant)
        elsif chain.reward_code_from_relevant_mode?
          return Barcode.generate_reward_code(chain)
        end
      end
    else
      "-NOCODE-"
    end
  end

  def reward_timer_generate
    c = Chain.find(@chain.id)
    c.timer
  end

  def without_timezone(date)
    date.strftime('%Y-%m-%dT23:59:59 +0000') if date
  end

end