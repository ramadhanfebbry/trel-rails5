class Api::V1::OloWebhooksController < ApplicationController

  def create
    event_type = ''
    message_id = ''
    olo_timestamp = ''
    olo_signature = ''
    OloWebhook.create(chain_id: (chain.id rescue nil), content: params)
    return render :json => {:status => true}
  end

  def index
    return render :json => {
                      :status => false,
                      :message => "Wrong request !"
                  }
  end

  def create_olo

    chain = Chain.find(params['chain_name']) rescue nil
    signature = chain.olo_signature unless chain.blank?

    puts "OLO::WEBHOOK::SIGNATURE == #{signature}"
    olo_test = ENV['OLO_WEBHOOK_TEST'] || false

    if (chain and signature.present?) or (olo_test.to_s == "true")
      webhook = OloWebhook.create(chain_id: (chain.id rescue nil), content: params)
      webhook.delay_receipt
      render json: {status: true}
    else
      render json:{ status:false}
    end
  end
end