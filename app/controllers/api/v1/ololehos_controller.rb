class Api::V1::OlolehosController < Api::BaseController

  skip_before_action :authorize_api_user!, :only => [:registration, :settings, :forgot_password, :login_update]

  def registration
    p params
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    params[:password] = URI.decode(params[:password]) rescue ""
    params[:security_question] = URI.decode(params[:security_question]) rescue ""
    params[:security_answer] = URI.decode(params[:security_answer]) rescue ""
    params[:email] = params[:email].try(:downcase)

    if params[:connect_type].eql?("1") || params[:aoo_detail].eql?("1")
      unless params[:aoo_detail].eql?("1")
        render :json => {:status => false, :message => "first name must more than 1 characters"} and return if params[:first_name].length < 2
        render :json => {:status => false, :message => "last name must more than 1 characters"} and return if params[:last_name].length < 2
      end
      render :json => {:status => false, :message => "security question must more than 1 characters"} and return if params[:security_question].length < 2
      render :json => {:status => false, :message => "security answer must more than 1 characters"} and return if params[:security_answer].length < 2
    end

    params[:user] = {:first_name=>params[:first_name],
                     :last_name=>params[:last_name],
                     :username=>params[:username],
                     :email=>params[:email],
                     :password => params[:password],
                     :latitude=>params[:latitude],
                     :longitude=>params[:longitude],
                     :chain_id=>@chain.id,
                     :register_type=>params[:register_type],
                     :register_device_type=>(params[:sign_in_device_type] || params[:register_device_type]),
                     :device_id => params[:device_id],
                     :ref_code => params[:referral_code],
                     :phone_number => params[:phone_number],
                     :dob_day => params[:dob_day],
                     :dob_month => params[:dob_month],
                     :dob_year => params[:dob_year],
                     :special_occassion => params[:special_occassion],
                     :zipcode => params[:zipcode],
                     :name => [params[:first_name], params[:last_name]].join(" "),
                     :marketing_optin => User.marketing_optin_value(params[:marketing_optin]),
                     :security_question => params[:security_question],
                     :security_answer => params[:security_answer],
                     :favorite_location => params[:favorite_location],
                     :special_occassion => params[:special_occassion],
                     :address1 => params[:address1],
                     :address2 => params[:address2],
                     :city => params[:city],
                     :state => params[:state],
                     :marketing_optin_texting => params[:marketing_optin_texting]
    }
    params[:user][:chain_id] = @chain.id
    params[:user][:locale_id] = Locale.find_by_key(params[:locale] ||= 'en').try(:id)
    olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    device_used = (params[:sign_in_device_type] || params[:register_device_type])
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if olo_connect_setting.blank?

    case params[:connect_type]
      when "1"
        users = User.unscoped.where("email = '#{params[:email]}' AND active IS TRUE and deleted_at IS NULL AND chain_id = #{@chain.id}").order("current_sign_in_at DESC")
        user_check = users.first
        if user_check.blank?
          user_check = User.new(params[:user])
        else
          user_check.attributes = params[:user]
        end
        user_check.aoo_phone_required = true
        user_check.aoo_signup_process = true
        user_check.required_domain_email_validation = @chain.chain_setting.domain_email_validation
        render :json => {:status => false, :notice => I18n.t("api.cloud_connect_aloha_aoo.failed.password_length")} and return unless params[:password].length >= 7
        if !params[:favorite_location].blank?
          rest = @chain.restaurants.where("id = ? ",params[:favorite_location]) rescue []
          render :json => {:status => false, :notice => I18n.t("api.cloud_connect_aloha_aoo.failed.invalid_location")} and return if rest.blank?
        end

        valid = user_check.valid?
        if !valid && !params[:phone_number].blank? && !users.select{|a| a.phone_number == params[:phone_number]}.blank?
          user_check.errors.messages.delete(:phone_number)
        end
        unless user_check.errors.messages.blank?
          p "user is not valid ------"
          p  user_check.errors
          custom_error = []
          if !user_check.errors.messages[:email].blank? && user_check.errors.messages[:email].first == "Please insert a valid email address"
            custom_error <<  I18n.t("api.cloud_connect_aloha_aoo.failed.invalid_email")
          end
          if !user_check.errors.messages[:phone_number].blank? && user_check.errors.messages[:phone_number].first == "Please enter a valid 10-digit phone number"
            custom_error << I18n.t("api.cloud_connect_aloha_aoo.failed.invalid_phone_number")
          end
          if !user_check.errors.messages[:phone_number].blank? && user_check.errors.messages[:phone_number].first == "This phone number is already taken. Please use the Login button to continue, or use a secondary phone number to Sign Up"
            custom_error << I18n.t("api.cloud_connect_aloha_aoo.failed.phone_number_used")
          end
          render :json => {:status => false, :notice => custom_error.blank? ? I18n.t("api.cloud_connect_aloha_aoo.failed.required_fields") : custom_error.join(", "), :dev_message => user_check.errors.messages} and return
        end

        # olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["#{device_used}_api_key"])
        olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["api_key"])
        success, message = olo_user.create(params)
        if success
          user = user_check
          if !user.id.blank?
            if user.aoo_customer
              user.password = params[:password]
              user.save
              user.reload
              Olo::RelevantUser.create_new_aoo_customer_and_linked_it_to_olo_and_relevant(@chain, user, message, params, false)
              Olo::RelevantUser.connect_with_aloha_loyalty(@chain, @app, user) if @chain.connect_to_aloha_program
              user.reset_authentication_token!
              user.aoo_new_user = false
              params[:aoo_new_user] = false
              params[:olo_process] = true
              UserAooSignupWorker.perform_in(5.seconds, @chain.id, user.id, params)
              notice_text =  I18n.t("api.cloud_connect_aloha_aoo.success.account_saved")
              return render :json =>  {:status => true, :auth_token => user.authentication_token, notice: notice_text , :card_number => user.try(:ncr_profile).try(:card_number), :relevant_user_id => user.id, :customer_id => user.try(:aoo_customer).try(:customer_id), :phone_number => user.try(:aoo_customer).try(:phone_number), :olo_auth_token => user.try(:aoo_customer).try(:olo_auth_token) }
            else
              user.save
              user.reload
              user.reset_authentication_token!
              user.aoo_new_user = false
              Olo::RelevantUser.create_new_aoo_customer_and_linked_it_to_olo_and_relevant(@chain, user, message, params)
              Olo::RelevantUser.connect_with_aloha_loyalty(@chain, @app, user) if @chain.connect_to_aloha_program
              params[:aoo_new_user] = false
              params[:olo_process] = true
              UserAooSignupWorker.perform_in(5.seconds, @chain.id, user.id, params)
              notice_text =  I18n.t("api.cloud_connect_aloha_aoo.success.account_saved")
              return render :json => {:status => true, :auth_token => user.authentication_token, notice: notice_text , :card_number => user.try(:ncr_profile).try(:card_number), :relevant_user_id => user.id, :customer_id => user.try(:aoo_customer).try(:customer_id), :phone_number => user.try(:aoo_customer).try(:phone_number), :olo_auth_token => user.try(:aoo_customer).try(:olo_auth_token)}
            end
          else
            user = User.new(params[:user])
            if user.save
              Olo::RelevantUser.create_new_aoo_customer_and_linked_it_to_olo_and_relevant(@chain, user, message, params)
              Olo::RelevantUser.connect_with_aloha_loyalty(@chain, @app, user) if @chain.connect_to_aloha_program
              user.reload
              user.reset_authentication_token!
              user.aoo_new_user = true
              params[:aoo_new_user] = true
              params[:olo_process] = true
              UserAooSignupWorker.perform_in(5.seconds, @chain.id, user.id, params)
              notice_text =  I18n.t("api.cloud_connect_aloha_aoo.success.account_saved")
              return render :json => {:status => true, :auth_token => user.authentication_token, notice: notice_text , :card_number => user.try(:ncr_profile).try(:card_number), :relevant_user_id => user.id, :customer_id => user.try(:aoo_customer).try(:customer_id), :phone_number => user.try(:aoo_customer).try(:phone_number), :olo_auth_token => user.try(:aoo_customer).try(:olo_auth_token)}
            else
              render :json => {:status => false, :notice => user.errors.messages.join(", ")} and return
            end
          end
        else
          render :json => {:status => false, :notice => message} and return
        end
      when "2"
        user_check = User.unscoped.where("email = '#{params[:email]}' AND active IS TRUE and deleted_at IS NULL AND chain_id = #{@chain.id}").order("current_sign_in_at DESC").first
        p user_check
        if user_check.blank?
          user_check = User.new(params[:user])
        else
          user_check.attributes = params[:user]
        end
        user_check.aoo_phone_required = false
        user_check.aoo_signup_process = false

        aoo_check = {:aoo_exist => false}
        # olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["#{device_used}_api_key"])
        olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["api_key"])
        success, message, first_name, last_name = olo_user.authenticate(params)
        if success
          user = user_check
          params[:first_name] = first_name unless first_name.blank?
          params[:last_name] = last_name unless last_name.blank?
          if !user.id.blank?
            if user.aoo_customer
              user.reload
              user.first_name = first_name unless first_name.blank?
              user.last_name = last_name unless last_name.blank?
              user.password = params[:password]
              user.save
              Olo::RelevantUser.create_new_aoo_customer_and_linked_it_to_olo_and_relevant(@chain, user, message, params, false)
              Olo::RelevantUser.connect_with_aloha_loyalty(@chain, @app, user) if @chain.connect_to_aloha_program
              aoo_check = {}
              user.reset_authentication_token!
              notice_text = I18n.t("api.cloud_connect_aloha_aoo.success.logged_in")
              user.aoo_new_user = false
              params[:aoo_new_user] = false
              params[:olo_process] = true
              UserAooSignupWorker.perform_in(5.seconds, @chain.id, user.id, params)
              return render :json => {:status => true, :auth_token => user.authentication_token, notice: notice_text , :card_number => user.try(:ncr_profile).try(:card_number), :relevant_user_id => user.id, :customer_id => user.try(:aoo_customer).try(:customer_id), :phone_number => user.try(:aoo_customer).try(:phone_number), :olo_auth_token => user.try(:aoo_customer).try(:olo_auth_token)}.merge(aoo_check)
            else
              if params[:aoo_detail].eql?("1")
                user_check.aoo_phone_required = true
                user_check.aoo_signup_process = true
                user_check.first_name = first_name unless first_name.blank?
                user_check.last_name = last_name unless last_name.blank?
                valid = user_check.valid?
                render :json => {:status => false, :notice => I18n.t("api.cloud_connect_aloha_aoo.failed.required_fields"), :dev_message => user_check.errors.messages} and return unless valid
                user = user_check
                user.password = params[:password]
                user.save
                Olo::RelevantUser.create_new_aoo_customer_and_linked_it_to_olo_and_relevant(@chain, user, message, params)
                Olo::RelevantUser.connect_with_aloha_loyalty(@chain, @app, user) if @chain.connect_to_aloha_program
                aoo_check = {}
                user.reset_authentication_token!
                user.aoo_new_user = true
                params[:aoo_new_user] = true
                params[:olo_process] = true
                notice_text = I18n.t("api.cloud_connect_aloha_aoo.success.logged_in")
                UserAooSignupWorker.perform_in(5.seconds, @chain.id, user.id, params) if user.aoo_customer
                return render :json => {:status => true, :auth_token => user.authentication_token, notice: notice_text , :card_number => user.try(:ncr_profile).try(:card_number), :relevant_user_id => user.id, :customer_id => user.try(:aoo_customer).try(:customer_id), :phone_number => user.try(:aoo_customer).try(:phone_number), :olo_auth_token => user.try(:aoo_customer).try(:olo_auth_token)}.merge(aoo_check)
              else
                user.reload
                user.first_name = first_name unless first_name.blank?
                user.last_name = last_name unless last_name.blank?
                user.password = params[:password]
                user.save
                user.reset_authentication_token!
                user.aoo_new_user = false
                params[:aoo_new_user] = false
                params[:olo_process] = true
                UserAooSignupWorker.perform_in(5.seconds, @chain.id, user.id, params)
                notice_text = I18n.t("api.cloud_connect_aloha_aoo.success.logged_in")
                return render :json => {:status => true, :auth_token => user.authentication_token, notice: notice_text , :card_number => user.try(:ncr_profile).try(:card_number), :relevant_user_id => user.id, :customer_id => user.try(:aoo_customer).try(:customer_id), :phone_number => user.try(:aoo_customer).try(:phone_number), :olo_auth_token => user.try(:aoo_customer).try(:olo_auth_token)}.merge(aoo_check)
              end
            end
          else
            if params[:aoo_detail].eql?("1")
              user = user_check
              user_check.aoo_phone_required = true
              user_check.aoo_signup_process = true
              user_check.first_name = first_name unless first_name.blank?
              user_check.last_name = last_name unless last_name.blank?
              valid = user_check.valid?
              render :json => {:status => false, :notice => I18n.t("api.cloud_connect_aloha_aoo.failed.required_fields"), :dev_message => user_check.errors.messages} and return unless valid
              user.save
              Olo::RelevantUser.create_new_aoo_customer_and_linked_it_to_olo_and_relevant(@chain, user, message, params)
              Olo::RelevantUser.connect_with_aloha_loyalty(@chain, @app, user) if @chain.connect_to_aloha_program
              aoo_check = {}
            else
              user_check.first_name = first_name unless first_name.blank?
              user_check.last_name = last_name unless last_name.blank?
              user = user_check
              user.save
            end
            user.reload
            user.reset_authentication_token!
            user.aoo_new_user = true
            params[:aoo_new_user] = true
            return render :json => {:status => true}.merge(aoo_check)
          end
        else
          render :json => {:status => false, :notice => message} and return
        end
    end
  end

  def settings
    @olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    if @olo_connect_setting
      render :json => {:status => true, :olo_connect_setting => @olo_connect_setting}
    else
      render :json => {:status => false, :message => "Chain does not have OLO connect setting" }
    end
  end

  def forgot_password
    p "------before--"
    p params
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    p "------after--"
    p params
    email = params[:email]
    user = @chain.users.where(:email => email).first rescue nil
    render :json => {:status => false, :message => "We were unable to access your information. Please try again."} and return if user.blank?

    olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if olo_connect_setting.blank?

    olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["api_key"])
    success, message = olo_user.forgot_password(email)
    if success
      render :json => {:status => true, :message => message} and return
    else
      render :json => {:status => false, :message => message } and return
    end
  end

  def login_update
    p "before---"
    p params
    params[:email] = CGI::escape(params[:email]) rescue ""
    params[:email] = URI.decode(params[:email]) rescue ""
    params[:old_password] = URI.decode(params[:old_password]) rescue ""
    params[:new_password] = URI.decode(params[:new_password]) rescue ""
    params[:confirm_password] = URI.decode(params[:confirm_password]) rescue ""
    p "after---"
    p params

    new_password = params[:new_password]
    confirm_password = params[:confirm_password]
    user = @chain.users.where(:email => params[:email]).first rescue nil

    render :json => {:status => false, :message => I18n.t("api.aoo_update_login.failed.required_fields")} and return  if user.blank?
    render :json => {:status => false, :message => I18n.t("api.aoo_update_login.failed.required_fields")} and return  if new_password.blank?
    render :json => {:status => false, :message => I18n.t("api.aoo_update_login.failed.required_fields")} and return  if confirm_password.blank?
    render :json => {:status => false, :message => I18n.t("api.aoo_update_login.failed.required_fields")} and return  if !new_password.eql?(confirm_password)

    authtoken = user.aoo_customer.try(:olo_auth_token)
    render :json => {:status => false, :message => "user olo auth token is blank!"} and return if authtoken.blank?

    olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if olo_connect_setting.blank?

    olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["api_key"])
    success, message = olo_user.login_update_password(params, authtoken)
    if success
      user.password = new_password
      user.password_confirmation = confirm_password
      user.save
      render :json => {:status => true, :message => message} and return
    else
      render :json => {:status => false, :message => message } and return
    end
  end

  def update_profile
    p params
    user = current_user
    default_bonus_plan = @app.default_bonus_plan
    secondary_bonus_plan = @app.secondary_bonus_plan
    aoo_customer = user.aoo_customer
    customer_id = aoo_customer.customer_id
    client = @chain.aloha_client_setup rescue nil
    user.first_name = params[:fname] unless params[:fname].blank?
    user.last_name = params[:lname] unless params[:lname].blank?
    user.favorite_location = params[:fav_location] unless params[:fav_location].blank?
    user.marketing_optin = User.marketing_optin_value(params[:marketing_optin]) unless params[:marketing_optin].blank?

    render :json => {:status => false, :message => "first name must more than 1 characters"} and return if params[:fname].present? && params[:fname].length < 2
    render :json => {:status => false, :message => "last name must more than 1 characters"} and return if params[:lname].present? && params[:lname].length < 2
    render :json => {:status => false, :message => "Please provide a valid First Name"} and return if params[:fname].blank? && params[:lname].present?
    render :json => {:status => false, :message => "Please provide a valid Last Name"} and return if params[:lname].blank? && params[:fname].present?

    authtoken = user.aoo_customer.try(:olo_auth_token)
    render :json => {:status => false, :message => "user olo auth token is blank!"} and return if authtoken.blank?

    olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if olo_connect_setting.blank?
    olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["api_key"])

    unless params[:phone_number].blank?
      user.phone_number = params[:phone_number]
      exist_user = @chain.users.find_by_phone_number(params[:phone_number]) if @chain.unique_phone_number_required
      render :json => {:status => false, :message => "phone number is already taken."} and return if exist_user.present? && exist_user.id != user.id

      success_phone_number, message_olo_profile = olo_user.update_phone_number(params, authtoken)
      render :json => {:status => false, :message => message_olo_profile} and return unless success_phone_number
    end

    if (params[:fname].present? && params[:lname].present?) || params[:phone_number].present?
      success_olo_profile, message_olo_profile = olo_user.update_profile(params, user) if (params[:fname].present? && params[:lname].present?)
      if success_olo_profile || success_phone_number
        user.save
        if @chain.fishbowl_setting && @chain.fishbowl_setting.sync_flow == "v1"
          user.create_or_get_profile_with_update_attributes({:activity_notification_optin => nil })
        end
        unless aoo_customer.blank?
          aoo_customer.first_name = params[:fname] unless params[:fname].blank?
          aoo_customer.last_name = params[:lname] unless params[:lname].blank?
          aoo_customer.phone_number = params[:phone_number] unless params[:phone_number].blank?
          if aoo_customer.save
            AooCustomer.update_aoo_info_customer(@chain, aoo_customer, customer_id)
            ncr_profile = user.ncr_profile
            unless ncr_profile.blank?
              ncr_profile.update_attributes(
                  :first_name => aoo_customer.first_name,
                  :last_name => aoo_customer.last_name,
                  :phone_number => aoo_customer.phone_number,
                  :date_of_birth => "#{aoo_customer.dob_day}/#{aoo_customer.dob_month}/#{aoo_customer.dob_year}",
                  :locale => "en_US",
                  :postal_code => aoo_customer.zipcode,
                  :zipcode => aoo_customer.zipcode,
                  :special_occassion => aoo_customer.special_occassion,
                  :address1 => aoo_customer.address1,
                  :address2 => aoo_customer.address2,
                  :city => aoo_customer.city,
                  :state => aoo_customer.state
              )
              Olo::RelevantUser.update_aoo_aloha_member_profile(@chain, client, @app, default_bonus_plan, secondary_bonus_plan, aoo_customer)
            end
          else
            render :json => {:status => true, :message => I18n.t("api.aoo_update_user_profile_metadata.failed.cant_update_profile")} and return
          end
        end
        render :json => {:status => true, :message => message_olo_profile} and return
      else
        render :json => {:status => false, :message => message_olo_profile} and return
      end
    else
      if user.save
        render :json => {:status => true, :message => "Your account information has been updated."} and return
      else
        render :json => {:status => false, :message => "We were unable to update your information. Please try again."} and return
      end
    end
  end

  def get_profile
    p params
    user = current_user
    authtoken = user.aoo_customer.try(:olo_auth_token)
    render :json => {:status => false, :message => "user olo auth token is blank!"} and return if authtoken.blank?

    olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if olo_connect_setting.blank?

    olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["api_key"])
    success, message = olo_user.get_profile(user, authtoken)
    if success
      render :json => {:status => true, :message => message} and return
    else
      render :json => {:status => false, :message => message } and return
    end
  end

  def get_phone_number
    p params
    user = current_user
    authtoken = user.aoo_customer.try(:olo_auth_token)
    render :json => {:status => false, :message => "user olo auth token is blank!"} and return if authtoken.blank?

    olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly"} and return if olo_connect_setting.blank?

    olo_user = Olo::Api::User.new(olo_connect_setting["api_root"], olo_connect_setting["api_key"])
    success, message = olo_user.get_phone_number(user, authtoken)
    if success
      render :json => {:status => true, :message => message} and return
    else
      render :json => {:status => false, :message => message } and return
    end
  end

  def get_oauth_token
    p params
    user = @chain.users.find_by_authentication_token(params[:auth_token]) rescue nil
    render :json =>  {status: false, :message => "Could not find the user"} and return if user.blank?

    access_grant = AccessGrant.where(client_id: params[:appkey], user_id: user.id).first rescue nil
    if access_grant.blank?
      access_grant = user.access_grants.create({:client_id => params[:appkey]}, :without_protection => true)
    end
    render :json => {status: true, :access_token => access_grant.access_token, :refresh_token => access_grant.refresh_token} and return
  end

end
