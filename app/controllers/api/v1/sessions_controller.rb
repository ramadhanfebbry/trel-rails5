class Api::V1::SessionsController < Devise::SessionsController
  include Api::ApplicationsHelper

  #FOR V2 PURPOSES
  before_action :check_api_and_authtoken_from_header
  before_action :check_query_string

  skip_before_action :require_no_authentication, only: [:new, :create]
  before_action :set_locale
  before_action :authorize_api_app!

  def new
    p "------------------------"
    p " SESSION new call"
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])

    params[:password] = CGI::escape(params[:password])
    params[:password] = URI.decode(params[:password])
    #need to uri decode twice to handle different params iphone and android
    params[:password] = URI.decode(params[:password])

    if @chain.php_chain_id && @chain.check_php_migration
      continue_process = migrate_by_email(params[:user][:email], params[:user][:register_type])
    else
      continue_process = true
    end
    return unless continue_process

    user = User.find_for_database_authentication(params[:user])

    if (user && params[:user][:register_type] == 2)
      web_and_mobile_signup_status = User.web_and_mobile_signup_check(@app, user, params[:sign_in_device_type])
      unless web_and_mobile_signup_status.eql?("CONT")
        render :json => web_and_mobile_signup_status
        warden.logout
        return
      end
      if @chain.chain_setting.user_email_confirmation == true
        return render :json => {:status => false , :notice => "Your Account is not activated , Pls check your email for activation link"} if resource.is_activated_email?
      end
      sign_in(user)
      user.update_column(:marketing_optin, User.marketing_optin_value(params[:marketing_optin])) unless params[:marketing_optin].blank?
      user.reset_authentication_token!
      auth_token= user.authentication_token
      #clear device token that already have by another user
      set_device_token_as_unique_for_user(user, params[:sign_in_device_type], params[:device_token], params[:device_id])
      unless current_user.active
        confirm_reactivation and return
      else
        user.update_phone_info(params[:phone_model], params[:os], params[:appkey])
        #user device stuff
        UserDeviceLog.add_log(user, params[:android_id], params[:keychain], (params[:sign_in_device_type] || params[:register_device_type]), "Login", Time.current)

        render :json => {:status => true, :auth_token => current_user.authentication_token, notice: t(:welcome_back), :client_set_status_value => user.client_set_status_value, :client_set_status_label => user.client_set_status_label}
      end
    elsif (user)
      render :json => {:status => false, notice: t(:wrong_email_or_password)}
    else
      if params[:migrate].to_i == 1
        result = @chain.migrate_old_ritas_user_to_relevant(params)
        return render :json => result if result != "CONT"
      elsif params[:migrate].to_i == 2
        result = @chain.migrate_old_guest_dna_user_to_relevant(params)
        return render :json => result if result != "CONT"
      elsif params[:migrate].to_i == 3
        result = @chain.migrate_tech_user_as_relevant(params)
        return render :json => result if result != "CONT"
      end
      render :json => {:status => false, notice: t(:user_doesnt_exist)}
    end
  end

  def create
    p "password nya : #{params[:password]}"
    params[:email] = CGI::escape(params[:email].to_s)
    params[:email] = URI.decode(params[:email].to_s)

    params[:password] = CGI::escape(params[:password].to_s)
    params[:password] = URI.decode(params[:password].to_s)
    #need to uri decode twice to handle different params iphone and android
    params[:password] = URI.decode(params[:password].to_s)
    p "password nya after converted: #{params[:password]}"

    if @chain.php_chain_id && @chain.check_php_migration
      continue_process = migrate_by_email(params[:email], params[:register_type])
    else
      continue_process = true
    end
    return unless continue_process
    params[:user]={:authenticity_token => params[:authenticity_token],
                   :email => params[:email].downcase,
                   :password => params[:password],
                   :remember_me => params[:remember_me],
                   :chain_id => @chain.id,
                   :register_type => params[:register_type].to_i,
                   :first_name => params[:first_name],
                   :last_name => params[:last_name],
                   :name => [params[:first_name], params[:last_name]].join(" "),
                   :marketing_optin => User.marketing_optin_value(params[:marketing_optin])
    }

    resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#new")
    if resource.nil?
      if params[:migrate].to_i == 1
        result = @chain.migrate_old_ritas_user_to_relevant(params)
        return render :json => result if result != "CONT"
      elsif params[:migrate].to_i == 2
        result = @chain.migrate_old_guest_dna_user_to_relevant(params)
        return render :json => result if result != "CONT"
      end
      render :json => {:status => false, notice: t(:user_not_found)}
    else
      web_and_mobile_signup_status = User.web_and_mobile_signup_check(@app, resource, params[:sign_in_device_type])
      p "web_and_mobile_signup_status"
      p "-------#{web_and_mobile_signup_status}----"

      unless web_and_mobile_signup_status.eql?("CONT")
        render :json => web_and_mobile_signup_status
        warden.logout
        return
      end
      if @chain.chain_setting.user_email_confirmation == true
        p "user email confirmation === #{resource}"
        return render :json => {:status => false , :notice => "Your Account is not activated , Pls check your email for activation link"} if resource.is_activated_email? == false
      end
      sign_in(resource_name, resource)
      resource.reset_authentication_token!
      auth_token= current_user.authentication_token
      #clear device token that already have by another user
      #update device_token and sign_in device type when user successfully login
      set_device_token_as_unique_for_user(resource, params[:sign_in_device_type], params[:device_token], params[:device_id])
      warden.logout

      unless current_user.active
        confirm_reactivation and return
      else
        resource.update_phone_info(params[:phone_model], params[:os], params[:appkey])
        #user device stuff
        UserDeviceLog.add_log(resource, params[:android_id], params[:keychain], (params[:sign_in_device_type] || params[:register_device_type]), "Login", Time.current)

        render :json => {:status => true, :auth_token => auth_token, notice: t(:welcome_back), :client_set_status_value => resource.client_set_status_value, :client_set_status_label => resource.client_set_status_label}
      end
    end
  end

  def destroy
    current_user.reset_authentication_token! unless current_user.nil?
    sign_out(current_user)
    #warden.logout
    #set_flash_message :notice, :signed_out if signed_out
    render :json => {:status => true, :notice => t(:logged_out)}
  end

  def failure
    render :json => {:status => false, :errors => t(:login_failed)}
  end

  def logged_in
    render :json => {:status => true, :auth_token => current_user.authentication_token, :notice => ["Already logged in."]}
  end

#  def reactivate_account
#    render :status => 401,
#      :json => {status: false, notice: t(:unauthorized_api)} and return unless user_signed_in?
#    user = current_user
#    if params[:confirmed] and params[:confirmed].eql?("true")
#      user.update_attribute(:active, true)
#      render :json => {:status => true, :auth_token => user.authentication_token, notice: t(:welcome_back_active)}
#    else
#      user.reset_authentication_token! unless user.nil?
#      warden.logout
#      render :json => {:status => true, :notice => t(:logged_out_inactive)}
#    end
#  end

  def confirm_reactivation
    render :status => 401,
           :json => {status: false, notice: t(:unauthorized_api)} and return unless user_signed_in?
    user = current_user
    chain = user.chain

    if params["account_activate"] and params["account_activate"] == "true"
      user.update_attribute(:active, true)
      user.reset_authentication_token!
      user.update_phone_info(params[:phone_model], params[:os], params[:appkey])
      render :json => {:status => true, :auth_token => user.authentication_token, notice: t(:welcome_back_active)}
    elsif params["account_activate"] and params["account_activate"] == "false"
      user.reset_authentication_token! unless user.nil?
      warden.logout
      render :json => {:status => true, :notice => t(:logged_out)}
    else
      notification = chain.account_notifications.where(:locale_id => user.locale_id).first
      render :json => {:status => false,
                       :confirmation_text => notification.nil? ? "confirmation text not set, contact admin" : notification.reactivation_confirmation_text,
                       :confirm_button_text => notification.nil? ? "confirmation confirm not set, contact admin" : notification.confirm_button_text ,
                       :cancel_button_text => notification.nil? ? "confirmation cancel button not set, contact admin" : notification.cancel_button_text ,
      }
    end
  end

  def deactivate_account
    render :status => 401,
           :json => {status: false, notice: t(:unauthorized_api)} and return unless user_signed_in?
    user = current_user
    if params["warn"] and params["warn"].eql?("false")
      user.update_attribute(:active, false)
      user.reset_authentication_token! unless user.nil?
      warden.logout
      render :json => {:status => true, :notice => t(:logged_out)}
    elsif params["warn"] and params["warn"].eql?("true")
      chain = user.chain
      notification = chain.account_notifications.where(:locale_id => user.locale_id).first
      render :json => {:status => true,
                       :confirmation_text => notification.nil? ? "confirmation text not set, contact admin" :notification.deactivation_confirmation_text,
                       :confirm_button_text => notification.nil? ? "confirmation button text not set, contact admin" :notification.confirm_button_text,
                       :cancel_button_text => notification.nil? ? "confirmation button text not set, contact admin" :notification.cancel_button_text,
      }
    end
  end

  def validate_token
    begin
      user =  @chain.users.find_by_authentication_token(params[:auth_token])
      if user
        user.update_column(:device_token, params[:device_token])
        render :json => {:status => true, :message =>  "User session is active and Device Token has been updated.", :user_activity_status => true}
      else
        render :json => {status: true, message: "Your session has expired, please login again.", :user_activity_status => false}
      end
    rescue
      render :json => {status: false, message: "We are sorry! We could not process your request. Please try again. If problem persists, contact support.", :user_activity_status => false}
    end
  end

  #  def authorize_api_app!
  #    render :status => 404,
  #           :json => {status: false, notice: "Application not found."} and return unless authenticateAppKey(params[:appkey])
  #  end
end