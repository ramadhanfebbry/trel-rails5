class Api::V1::GoalsController < Api::BaseController

  def index
    p "------------goal index calling-------------"
    user = current_user
    today = Time.current
    start_date = today.send("custom_beginning_of_#{Setting.goals.unit_time}")
    end_date = today.send("custom_end_of_#{Setting.goals.unit_time}")

    p "start date = #{start_date}"
    p "end date = #{end_date}"
    locale = params[:locale].blank? ? "en" : params[:locale]

    week = Week.where(:start_date => start_date, :end_date => end_date).first
    p "week"
    p week
    user_goals = week.blank? ? [] : user.user_goals.where(:week_id => week.id)
    completed_goals = user_goals.select{|ug| ug.completed == true}
    uncompleted_goals = user_goals.select{|ug| ug.completed == false}

    if completed_goals.blank? and uncompleted_goals.blank?
      render :json => {status: false, notice: "Please tap on the 'Edit Goals' button to update your goals for this week"}
    else
      render :json => {status: true, completed_goals: completed_goals.map do |c|
        { :id => c.id, :goal => c.goal.get_text_by_locale(locale), :week_id => c.week_id }
      end, uncompleted_goals: uncompleted_goals.map do |c|
        { :id => c.id, :goal => c.goal.get_text_by_locale(locale), :week_id => c.week_id }
      end }.to_json
    end

  end

  def complete
    user = current_user
    locale = params[:locale].blank? ? "en" : params[:locale]
    today = Time.current
    start_date = today.send("custom_beginning_of_#{Setting.goals.unit_time}")
    end_date = today.send("custom_end_of_#{Setting.goals.unit_time}")

    week = Week.where(:start_date => start_date, :end_date => end_date).first
    if week
      user_goal = UserGoal.find params[:goal_id]
      if user_goal
        if user_goal.completed
          render :json => {status: false, notice: 'This goal already completed'}
        else
          user_goal.update_attribute(:completed, true)
          if user_goal.goal.status.eql?(Goal::STATUS[:INACTIVE]) or user_goal.goal.category.status.eql?(GoalCategory::STATUS[:INACTIVE])
            render :json => {status: true, notice: 'Keep up the good work tomorrow by completing another one!'}
          else
            message_associated = ""
            incentive = user_goal.goal.incentive
            incentive_title = ""
            if incentive
              incentive_title = incentive.class.to_s.eql?("DealOffer") ? incentive.title : incentive.name
              if incentive.class.to_s.eql?("DealOffer") and !incentive.points.blank?
                current_user.earn(incentive.points)
                GoalUserIncentive.add_incentive_to_user(incentive, user, user_goal.goal, week)
              elsif incentive.class.to_s.eql?("Reward")
                RewardWallet.create_with_delay(:reward_id => incentive.id, :user_id => current_user.id)
                GoalUserIncentive.add_incentive_to_user(incentive, user, user_goal.goal, week)
              end
              message_associated = user_goal.goal.get_message_incentive(locale)
            end
            render :json => {status: true, notice: 'Keep up the good work tomorrow by completing another one!', message_associated: message_associated, incentive_title: incentive_title }
          end
        end
      else
        render :json => {status: false, notice: 'This goal not setup yet'}
      end
    else
      render :json => {status: false, notice: 'Week is not available'}
    end
  end

  def deadline
    today = Time.current
    start_date = today.send("custom_beginning_of_#{Setting.goals.unit_time}")
    end_date = today.send("custom_end_of_#{Setting.goals.unit_time}")

    week = Week.where(:start_date => start_date, :end_date => end_date).first

    if week
      render :json => {status: true, deadline: end_date}
    else
      render :json => {status: false, notice: 'Week is not available'}
    end


  end

  def history
    user = current_user
    locale = params[:locale].blank? ? "en" : params[:locale]
    today = Time.current + 1.weeks
    prev_week = today.prev_week
    end_date = prev_week.end_of_week
    start_date = (prev_week - 3.weeks).beginning_of_week
    user_goals = user.user_goals.where("DATE(user_goals.created_at) >= ? AND DATE(user_goals.created_at) <= ? AND completed IS TRUE", start_date, end_date)
    render :json => {status: true, history_completed_goals: user_goals.map do |ug|
      {:id => ug.id, :goal => ug.goal.get_text_by_locale(locale), :completed_at => ug.updated_at }
    end}.to_json

  end

end