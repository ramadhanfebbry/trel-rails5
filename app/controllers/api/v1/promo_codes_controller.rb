class Api::V1::PromoCodesController < Api::BaseController

  def create
    promocode = params[:code]
    #promocode =  PromoCode.find_by_code(params[:promocode])
    force = params[:force]

    status, notice, stackable = PromoCode.code_process(promocode, current_user, force)
       render :json => {status: status, notice: t(notice), stackable: stackable }
  end


end