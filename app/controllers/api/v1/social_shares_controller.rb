class Api::V1::SocialSharesController < Api::BaseController
  skip_before_action :authorize_api_user! , :only => :index

  def index
    puts "Social Shares Controller API"
    #chain = current_user.chain
    @social = @chain.social_shares.active.first
    unless @social.blank?
      render :json => {
          :status => @social.status,
          :social_share_program_id => @social.id,
          :facebook_share_text => @social.fb_text,
          :instagram_share_text => @social.instagram_text,
          :twitter_share_text => @social.twitter_text,
          :program_description => @social.description
      }
    else
      render :json => {
          :status => false,
          :message => "Your chain doesnt have social shares activated"
          }
    end
  end

  #user_interaction_api_v1_social_shares
  # /api/v1/social_shares/user_interaction
  #i/p parameters
  #-appkey
  #-user auth_token
  #-medium_id( -Facebook medium _id =1, twitter medium_id=2, instagram medium id=3)
  #-social_program_id

  def user_interaction
    #puts "user interaction"
    medium = params['medium_id']
    user = current_user
    social_share = user.chain.social_shares.active.first
    #puts social_share
    #puts medium

    ### Inserting Into #####
    unless social_share.blank?
      p "come"
      success = SocialShareParticipant.get_reward_or_point(user,social_share,medium)
      return render :json => {:status => success == true ? "success" : "fail" }
    else
      return  render :json => {:status => "fail", :message => "Your chain does not have any active social share program"}
    end
  end


end