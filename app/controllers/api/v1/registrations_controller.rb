  class Api::V1::RegistrationsController < Devise::RegistrationsController
  include Api::ApplicationsHelper

  skip_before_action :require_no_authentication, only: [:new, :create, :cancel]

  before_action :set_locale
  before_action :authorize_api_app!
  before_action :check_query_string

  def new
    super
  end

  def create
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    if @chain.php_chain_id && @chain.check_php_migration
      continue_process = migrate_by_email(params[:email], params[:register_type])
    else
      continue_process = true
    end
    return unless continue_process
    params[:user] = {:first_name=>params[:first_name],
                     :last_name=>params[:last_name],
                     :username=>params[:username],
                     :email=>params[:email].downcase,
                     :password => params[:password],
                     :latitude=>params[:latitude],
                     :longitude=>params[:longitude],
                     :chain_id=>@chain.id,
                     :register_type=>params[:register_type],
                     :register_device_type=>(params[:sign_in_device_type] || params[:register_device_type]).downcase.tr(' ', '_'),
                     :device_id => params[:device_id],
                     :ref_code => params[:referral_code],
                     :phone_number => params[:phone_number],
                     :dob_day => params[:dob_day],
                     :dob_month => params[:dob_month],
                     :dob_year => params[:dob_year],
                     :special_occassion => params[:special_occassion],
                     :zipcode => params[:zipcode],
                     :name => [params[:first_name], params[:last_name]].join(" "),
                     :marketing_optin => User.marketing_optin_value(params[:marketing_optin]),
                     :marketing_optin_texting => params[:marketing_optin_texting],
                     :favorite_location => params[:favorite_location],
                     :favorite_menu_item => params[:favorite_menu_item],
                     :app_usage_purpose => params[:app_usage_purpose],
                     :gender => params[:gender].blank? ? nil : params[:gender]
    }
    # set user locale (default to en)
    params[:user][:locale_id] = Locale.find_by_key(params[:locale] ||= 'en').try(:id)

    if !params[:favorite_location].blank?
      rest = @chain.restaurants.where("id = ? ",params[:favorite_location].to_i) rescue []
      render :json => {:status => false, :message => "Favorite Location not valid"} and return if rest.blank?
    end

    user = User.find_for_database_authentication(params[:user])
    p "-----asdasd"
    p user
    if params[:migrate].to_i == 1
      return render :json => {:status => false, :notice => "Email address has been registered. Please login to continue."} unless user.blank?
      result = @chain.migrate_old_ritas_user_to_relevant(params)
      return render :json => result if result != "CONT"
    elsif params[:migrate].to_i == 2
      return render :json => {:status => false, :notice => "Email address has been registered. Please login to continue."} unless user.blank?
      result = @chain.migrate_old_guest_dna_user_to_relevant(params)
      return render :json => result if result != "CONT"
    elsif params[:migrate].to_i == 3
      return render :json => {:status => false, :notice => "Email address has been registered. Please login to continue."} unless user.blank?
      result = @chain.migrate_tech_user_as_relevant(params)
      return render :json => result if result != "CONT"
    end

    if (user)
      if params[:register_type].eql?("2")
        p "----------facebook callllll----------"
        p params
        user.update_column(:marketing_optin, User.marketing_optin_value(params[:marketing_optin])) unless params[:marketing_optin].blank?
        unless user.active
          confirm_reactivation(user) and return
        else
          sign_in(user)
          user.reset_authentication_token!
          auth_token = user.authentication_token
          #clear device token that already have by another user
          #update device_token and sign_in device type when user successfully login
          p "-------------a-asdasdasd-"
          p params[:sign_in_device_type]
          p params[:device_token]
          set_device_token_as_unique_for_user(user, (params[:sign_in_device_type] || params[:register_device_type]), params[:device_token], params[:device_id])
          user.update_phone_info(params[:phone_model], params[:os], params[:appkey])

          #user device stuff
          UserDeviceLog.add_log(user, params[:android_id], params[:keychain], (params[:sign_in_device_type] || params[:register_device_type]), "Login", Time.current)

          warden.logout
          render :json => {:status => true, :auth_token => auth_token, notice: t(:welcome_back), :client_set_status_value => user.client_set_status_value, :client_set_status_label => user.client_set_status_label}
        end
      else
        render :json=>{:status=>false, notice: t(:email_already_exist)}
      end

    else
      build_resource
      resource.required_domain_email_validation = @chain.chain_setting.domain_email_validation

      if !params[:mall_employee].blank? || !params[:retailer].blank?
        resource.build_user_profile
        resource.user_profile.mall_employee = UserProfile.mall_employee_value(params[:mall_employee]) unless params[:mall_employee].blank?
        resource.user_profile.retailer      = params[:retailer] unless params[:retailer].blank?
      end

      if resource.save
        if resource.active_for_authentication?
          set_flash_message :notice, :signed_up if is_navigational_format?

          resource.reset_authentication_token!
          #MOVED TO DELAYED JOB
          Delayed::Job.enqueue(UserSignupJob.new(@chain, resource, params))
          p "goes here---------"
          warden.logout
          notice_text = params[:referral_code].blank? ? (ApiResponseText.get_message_chain_by_locale(@chain.id, resource.locale_id, 4)) : (ApiResponseText.get_message_chain_by_locale(@chain.id, resource.locale_id, 3))
          p "here is the notice ---- #{notice_text}"

          if @chain.chain_setting.user_email_confirmation == true
            p "confirmation email setting is true"
            p "sending email"
            p "creating token --- #{resource.set_confirmation_token}"
            p "creating token --- #{resource.set_confirmation_token}"
            UserMailer.registration_confirmation(resource).deliver
            render :json => {:status => true, :notice => notice_text, :client_set_status_value => resource.client_set_status_value, :client_set_status_label => resource.client_set_status_label, :activation_url => confirm_email_url(@user.user_confirmation.reload.confirm_token)}
          else
            render :json => {:status => true, :notice => notice_text, :auth_token => resource.authentication_token, :client_set_status_value => resource.client_set_status_value, :client_set_status_label => resource.client_set_status_label}
          end
        else
          set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
          expire_session_data_after_sign_in!
          render :json=>{:status=>false, :notice=> t(:please_try_again)}
        end
      else
        p resource
        #check user from another register type
        user = User.where("chain_id = ? AND email = ? AND register_type != ?", resource.chain_id, resource.email, resource.register_type).first
        resource.errors.delete(:password) if user && user.register_type == 2
        clean_up_passwords resource
        notice_text = []
        resource.errors.messages.each do |key, value|
          notice_text << "#{value.first}"
        end
        render :json=>{:status=>false, :errors=>resource.errors, :notice => notice_text.join(" #{t(:and)} ") } #:user=>resource.errors
      end
    end
  end

  def update
    super
  end

  def confirm_reactivation(user)
    chain = user.chain

    if params["account_activate"] and params["account_activate"] == "true"
      user.update_attribute(:active,true)
      user.reset_authentication_token!
      render :json => {:status => true, :auth_token => user.authentication_token, notice: t(:welcome_back_active)}
    elsif params["account_activate"] and params["account_activate"] == "false"
      user.reset_authentication_token! unless user.nil?
      warden.logout
      render :json => {:status => true, :notice => t(:logged_out)}
    else
      notification = chain.account_notifications.where(:locale_id => user.locale_id).first
      render :json => {:status => false,
                       :confirmation_text => notification.nil? ? "confirmation text not set, contact admin" : notification.reactivation_confirmation_text,
                       :confirm_button_text => notification.nil? ? "confirmation confirm not set, contact admin" : notification.confirm_button_text ,
                       :cancel_button_text => notification.nil? ? "confirmation cancel button not set, contact admin" : notification.cancel_button_text ,
      }
    end
  end

  def migrate
    render :json => {:success => false, :message => "Chain is not configured with php chain"} and return if @chain.php_chain_id.blank?
    @php_chain_id = @chain.php_chain_id
    if params[:user_id]
      migrate_by_user_id(params[:user_id])
    elsif params[:email]
      migrate_by_email(params[:email])
    else
      render :json => {:success => false, :message => "Please specify your params"}
    end
    p "migration completed"
  end


  #  def authorize_api_app!
  #    render :status => 404,
  #           :json => {status: false, notice: "Application not found."} and return unless authenticateAppKey(params[:appkey])
  #  end
end


