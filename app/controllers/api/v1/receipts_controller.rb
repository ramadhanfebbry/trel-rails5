class Api::V1::ReceiptsController < Api::BaseController

  def new
    render :json=>{status: true, receipt: {offers: chain.offers.as_json(:only=>[:id, :name])}}
  end

  def create
    puts "RECEIPT IMAGE PROCESSING"
    application_key = ApplicationKey.where(:appkey => params[:appkey]).first rescue nil
    params[:receipt]={status: application_key && application_key.ocr_send_message ? 5 : 1, image: params[:image], user_id: current_user.id, chain_id: @chain.id, img_from_mobile: true}
    @receipt = Receipt.new(params[:receipt])
    @receipt_transaction = ReceiptTransaction.new()
    
    restaurant_offer = RestaurantOffer.find_by_offer_id_and_restaurant_id(params[:offer], params[:restaurant])

    if (restaurant_offer.blank?)
      render :json=>{status: false, notice: t(:offer_not_found)}
    else
      @receipt.application_id= app.id
      @receipt_transaction.restaurant_offer = restaurant_offer
      @receipt_transaction.restaurant_id = params[:restaurant]
      @receipt_transaction.status = params[:receipt][:status]
      @receipt_transaction.receipt = @receipt

      if params[:date].present?
        full_date = params[:date]
        time = params[:time]
        date = full_date.split("/")[1]
        month = full_date.split("/")[0]
        year = full_date.split("/")[2]
        @receipt_transaction.issue_date = Time.parse("#{date}/#{month}/#{year} #{time} UTC") rescue nil
      end
      @receipt_transaction.subtotal =  params[:subtotal] if params[:subtotal].present?
      @receipt_transaction.receipt_number =  params[:receipt_number] if params[:receipt_number].present?

      Receipt.transaction do
        puts "RECEIPT IMAGE PROCESS ------------------------------"
        puts @receipt.valid? and @receipt_transaction.valid?
        puts @receipt.valid?
        puts @receipt_transaction.valid?
        puts "RECEIPT IMAGE PROCESS ------------------------------ END"
        if @receipt.save and @receipt_transaction.save
          receipt_transaction_detail = @receipt_transaction.receipt_transaction_detail
          if receipt_transaction_detail.blank?
            receipt_transaction_detail = ReceiptTransactionDetail.new(:receipt_transaction_id => @receipt_transaction.id)
            receipt_transaction_detail.clientfeedback1 = params[:clientfeedback1] rescue nil
            receipt_transaction_detail.clientfeedback2 = params[:clientfeedback2] rescue nil
            receipt_transaction_detail.save
          else
            # if nil don't change the value
            receipt_transaction_det_attr = {}
            receipt_transaction_det_attr['clientfeedback1'] = params[:clientfeedback1] rescue nil
            receipt_transaction_det_attr['clientfeedback2'] = params[:clientfeedback2] rescue nil
            receipt_transaction_detail.update_attributes(receipt_transaction_det_attr)
          end
          Delayed::Job.enqueue(
              ReceiptProcess::ReceiptImageProcess.new(@receipt, @receipt_transaction, params, current_user),
              :priority => 1
          )
          # move to delayed job
          # if @receipt.save and @receipt_transaction.save
          # unless params[:restaurant].blank?
          #   ru = RestaurantUser.new(:user_id => current_user.id, :restaurant_id => params[:restaurant])
          #   ru.save
          #   EmailMarketing.update_email_marketing_location(current_user, params[:restaurant])
          #   Chain.update_user_fishbowl_location(current_user, params[:restaurant])
          # end
          # @receipt.sqs_send_message(params[:appkey])
          id_survey = restaurant_offer.offer.survey.try(:id)
          render :json=>{
            status: true,
            test_token: current_user.authentication_token, old_token: params[:auth_token],
            notice: (ApiResponseText.get_message_chain_by_locale(@chain.id, current_user.locale_id, 1)),
            receipt_id: @receipt.id,
            survey_id: id_survey
          }
        else
          render :json=>{status: false, notice: (ApiResponseText.get_message_chain_by_locale(@chain.id, current_user.locale_id, 2))}
        end
      end
    end
  end

  def upload
    user = current_user
    barcode = params[:barcode].to_s.upcase
    latitude = params[:latitude]
    longitude = params[:longitude]
    beacons = params[:beacons]
    p "user scanned receipt is #{user.id} - #{user.email}"
    render :json => {status: false, notice: "User not accosiated to chain"} and return if @chain.id != user.chain_id

    if params[:code_type].to_i == 2
      return render :json => user.process_loyalty_with_beacon_information(barcode, beacons, latitude, longitude)
    end

    pos_location_ids = @chain.pos_locations.select("id").map(&:id)
    render :json => {:status => false, :notice => "We are sorry. The code scanned is either invalid or expired. Please contact customer support from the info section of the app for further assistance."} and return if pos_location_ids.blank?

    if @chain.pos_used_type == Chain::POS_USED_TYPE["NETPOS"]
      return render :json => @chain.net_pos_process_loyalty(user, params[:barcode])
    end

    barcode = params[:barcode].to_s.upcase
    receipt_code_setting = REDIS.get "receiptcode_chain_#{@chain.id}"
    receipt_code_setting = JSON.parse(receipt_code_setting) rescue nil
    receipt_code_setting ||= AppcodeSetting::DEFAULT_SETTING_RECEIPTCODE
    if receipt_code_setting && receipt_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && receipt_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["RECEIPTCODE"])
      if receipt_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && receipt_code_setting["barcode_format"].to_i == 1
        barcode = barcode.to_ean_13_format
      end
    end
    p "barcode after converted with selected Setting = #{barcode}"
    pos_check_upload = PosCheckUpload.where("chain_id = ? AND barcode = ? AND status <> ?", @chain.id, barcode, PosCheckUpload::STATUS[:PROCESSED]).first rescue nil
    if pos_check_upload.blank?
      pos_check_upload = PosCheckUpload.where("chain_id = ? AND old_barcode = ? AND status <> ?", @chain.id, barcode, PosCheckUpload::STATUS[:PROCESSED]).first rescue nil
    end
    p "selected pos check upload"
    p pos_check_upload
    if pos_check_upload
      pos_xml = PosReceiptXml.new(pos_check_upload.xml_data)
      receipt_date = DateTime.strptime(pos_xml.receipt_date, "%m/%d/%y %H:%M:%S")
      if @chain.is_expired_micros_receipt?(receipt_date)
        render :json => {:status => false, :notice => "Receipt code expired. Please contact customer support from the info section of the app for further assistance."} and return
      end
      pos_location = pos_check_upload.pos_location
      restaurant = pos_location.restaurant
      offer = restaurant.first_active_offer
      render :json => {:status => false, :notice => "Invalid Offer"} and return if offer.blank?
      survey_id = offer.try(:survey).try(:id)
      receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true, :pos_check_upload_id => pos_check_upload.id)
      receipt_transaction = ReceiptTransaction.new
      receipt_transaction.status = Receipt::STATUS[:RECEIVED]
      receipt.receipt_transactions << receipt_transaction
      receipt.save
      render :json => {
          status: true,
          message: (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 7)),
          survey_id: survey_id,
          receipt_id: receipt.id
      }
      pos_check_upload.update_column(:service,PosCheckUpload::SERVICE[:TABLE])
      Delayed::Job.enqueue(ReceiptCodeJob.new(pos_check_upload, user, restaurant.id, offer.id, receipt.id), :run_at => Time.zone.now + @chain.pos_receipt_processing_delay.minutes)
    else
      render :json=>{status: false, notice: ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 9)}
    end
  end

  def check_detail
    user = current_user
    p "user scanned receipt is #{user.id} - #{user.email}"
    render :json => {status: false, notice: "User not accosiated to chain"} and return if @chain.id != user.chain_id
    response = Receipt.get_detail_micros_receipt(@chain, params[:code])

    if params[:code].eql?("SM1986")
      receipt = user.receipts.first
      dummy_check = "<check xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" code=\"PRINTER_CODE\" code_type=\"\" obj_num=\"302\" seq_num=\"1566\" id=\"302\" check-id=\"\" xmlns=\"ltt:ABC\"><meal><open-time>09/03/15 12:19:56</open-time><close-time /><date>09/03/15 12:20:05</date><meal-period>0</meal-period><cover-count>1</cover-count><emp-num>101</emp-num><rvc-num>1</rvc-num><rvc-name>Restaurant</rvc-name><order-type-num>1</order-type-num><order-type-name>Eat In</order-type-name><payment-total>0.00</payment-total><tax-total>3.05</tax-total><tip-total>0.00</tip-total><discount-total>0.00</discount-total><sales-total>35.00</sales-total><due-total>38.05</due-total></meal><payment><total>0</total><direct-tips>0</direct-tips><tax>0</tax><item-sales>0</item-sales></payment><menu-items><menu-item id=\"8014\"><quantity>1</quantity><name>Dinner For Two</name><total-amount>35.00</total-amount></menu-item></menu-items><discounts /><tenders /><service-charges /></check>"
      hash_check = Hash.from_xml(dummy_check) rescue nil
      menu_items = hash_check["check"]["menu_items"]["menu_item"] rescue nil
      hash_check["check"]["menu_items"]["menu_item"] = [hash_check["check"]["menu_items"]["menu_item"]] if hash_check && hash_check["check"]["menu_items"] && menu_items.class.to_s != "Array"
      render :json=> {status: true, notice: "check" , :check => hash_check } and return
    end

    render :json=> {status: response[:status], notice: response[:notice], :check => response[:check]}
  end

  def pay_table_service
    p params
    user = current_user
    chain = user.chain
    tip_amount = params[:tip].to_f
    code = params[:code]

    if !params[:signature].blank? && !['image/jpeg', 'image/png', 'image/gif'].include?(params[:signature].content_type)
      render :json => {:status => false, :message => "Signature should be an image"} and return
    end

    if code.eql?("SM1986")
      receipt = user.try(:receipts).try(:first)
      offer = receipt.try(:last_transaction).try(:offer)
      survey_id = offer.try(:survey).try(:id)
      render :json => {:status => true, :message => "Payment Confirmed.", :amount_billed => 10, :survey_id => survey_id, :receipt_id => receipt.try(:id) } and return
    end

    response = Receipt.get_detail_micros_receipt(chain, code)
    if response[:status]
       pos_check_upload = PosCheckUpload.find(response[:pos_check_upload])
       #if pos_check_upload.status == PosCheckUpload::STATUS[:CONVERTED]
       #  p "---------pos check upload already converted/processed detected ----------"
       #  render :json => {:status => false, :message => "The check was alredy used before"} and return
       #end
       pos_xml = PosReceiptXml.new(pos_check_upload.xml_data)
       if pos_xml.due_total == 0
         p "---------pos check upload already paid for detected ----------"
         render :json => {:status => false, :message => "The check was already paid for"} and return
       end
       apikey = pos_check_upload.try(:pos_location).try(:apikey)

       pay_code = REDIS.get "paycode_chain_#{chain.id}"
       pay_code_setting = JSON.parse(pay_code) rescue nil
       pay_code_setting ||= AppcodeSetting::DEFAULT_SETTING_PAYCODE
       if chain.payment_processor == Chain::PAYMENT_PROCESSOR_TYPE["BRAINTREE"]
         Barcode.save_btree_customer_id_as_paycode(user, @chain, @app.id)
         paycode = pay_code_setting["prefix"].to_s + user.braintree_identifier.add_zero_padding(pay_code_setting["zero_padding"].to_i) + pay_code_setting["postfix"].to_s
         barcode, user_selected, user_session = AppcodeSetting.paycode_convert(chain, paycode)
       elsif chain.payment_processor == Chain::PAYMENT_PROCESSOR_TYPE["MERCURYPAY"]
         Barcode.save_mercury_paycode_as_paycode(user, chain, @app.id)
         paycode = pay_code_setting["prefix"].to_s + "MER#{user.id}PAY".add_zero_padding(pay_code_setting["zero_padding"].to_i) + pay_code_setting["postfix"].to_s
       end

       #PAYMENT PROCESS START!!
       authorization_code = nil
       payment_response, error_code, error_message, authorization_code =  Receipt.do_pay_ts(apikey, paycode, pos_xml.due_total, tip_amount, "micros", pos_xml.check_id, pos_xml.seq_num, pos_xml.revenue_center, pos_xml.employee_num, pos_xml.check_open_time )

       #FOR TESTING PURPOSE ONLY WHEN PAYMENT ERROR WE NEED TO SKIP IT, DO NOT PUSH IT TO LIVE!!!
       payment_response, error_code, error_message, authorization_code = true, nil, nil, Time.zone.now.to_i.to_s if params[:skip_payment].to_s.eql?("1")
       unless payment_response
         UserPaymentHistory.create(:user_id => user.id, :qrcode => code,
                                   :success => false, :error_description => error_message,
                                   :error_code => error_code, :payment_type => 2, :signature => params[:signature])
         render :json => {:status => false, :message => "We are sorry! We were unable to process your payment. Please try again. If problem persists contact your server or cashier for alternate payment options." } and return
       end
       pos_location = pos_check_upload.pos_location
       restaurant = pos_location.restaurant
       offer = restaurant.first_active_offer
       render :json => {:status => false, :message => "Invalid Offer"} and return if offer.blank?
       survey_id = offer.try(:survey).try(:id)
       receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true, :pos_check_upload_id => pos_check_upload.id)
       receipt_transaction = ReceiptTransaction.new
       receipt_transaction.status = Receipt::STATUS[:RECEIVED]
       receipt.receipt_transactions << receipt_transaction
       receipt.save
       pos_check_upload.update_attributes(
           :user_code => barcode,
           :user_id => user.id,
           :code_type => "paycode",
           :service => PosCheckUpload::SERVICE[:TABLE],
           :payment_included => true
       )
       UserPaymentHistory.create(:user_id => user.id, :pos_check_upload_id => pos_check_upload.id, :receipt_id => receipt.try(:id), :qrcode => code, :amount => pos_xml.due_total, :tip => tip_amount,
                                 :success => true, :payment_type => 2, :signature => params[:signature], :authorization_code => authorization_code)
       Delayed::Job.enqueue(ReceiptCodeJob.new(pos_check_upload, user, restaurant.id, offer.id, receipt.id), :run_at => Time.zone.now + @chain.pos_receipt_processing_delay.minutes)
       user_session.update_attributes(:executed => true, :receipt_id => receipt.id, :active => false) if user_session
       render :json => {:status => true, :message => "Payment Confirmed.", :amount_billed => pos_xml.due_total + tip_amount, :survey_id => survey_id, :receipt_id => receipt.id }
    else
      render :json => {:status => response[:status], :notice => response[:notice]}
    end

  end

end
