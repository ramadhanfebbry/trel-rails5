class Api::Volo::UserController < Api::VonoBaseController

  skip_before_action :authorize_api_user!, :only => [:connect]

   def connect
    p params
    params[:email] = CGI::escape(params[:email])
    params[:email] = URI.decode(params[:email])
    params[:password] = URI.decode(params[:password]) rescue ""
    params[:security_question] = URI.decode(params[:security_question]) rescue ""
    params[:security_answer] = URI.decode(params[:security_answer]) rescue ""
    params[:email] = params[:email].try(:downcase)

    if params[:connect_type].eql?("1")
      render :json => {:status => false, :notice => "first name must more than 1 characters"} and return if params[:first_name].length < 2
      render :json => {:status => false, :notice => "last name must more than 1 characters"} and return if params[:last_name].length < 2
    end

    params[:user] = {:first_name=>params[:first_name],
                     :last_name=>params[:last_name],
                     :username=>params[:username],
                     :email=>params[:email],
                     :password => params[:password],
                     :latitude=>params[:latitude],
                     :longitude=>params[:longitude],
                     :chain_id=>@chain.id,
                     :register_type=>params[:register_type],
                     :register_device_type=>((params[:sign_in_device_type] || params[:register_device_type]).downcase.tr(' ', '_') rescue nil),
                     :device_id => params[:device_id],
                     :device_token => params[:device_token],
                     :ref_code => params[:referral_code],
                     :phone_number => params[:phone_number],
                     :dob_day => params[:dob_day],
                     :dob_month => params[:dob_month],
                     :dob_year => params[:dob_year],
                     :special_occassion => params[:special_occassion],
                     :zipcode => params[:zipcode],
                     :name => [params[:first_name], params[:last_name]].join(" "),
                     :marketing_optin => User.marketing_optin_value(params[:marketing_optin]),
                     :favorite_location => params[:favorite_location],
                     :special_occassion => params[:special_occassion],
                     :address1 => params[:address1],
                     :address2 => params[:address2],
                     :city => params[:city],
                     :state => params[:state],
                     :marketing_optin_texting => params[:marketing_optin_texting],
                     :device_token => params[:device_token]
    }
    params[:user_type] = "olo"
    params[:user][:chain_id] = @chain.id
    params[:user][:locale_id] = Locale.find_by_key(params[:locale] ||= 'en').try(:id)
    params[:user][:olo_registration] = true #add this to activate validation for firstname and lastname
    render :json => {:status => false, :notice => "Something went wrong, please comeback shortly", :dev_message => "Account Token is blank"} and return if params[:account_token].blank?
    case params[:connect_type]
      when "1"
        users = User.unscoped.where("email = '#{params[:email]}' AND active IS TRUE and deleted_at IS NULL AND chain_id = #{@chain.id}").order("current_sign_in_at DESC")
        user_check = users.first
        if user_check.blank?
          user_check = User.new(params[:user])
        else
          user_check.attributes = params[:user].reject { |key,value| value.nil? }
        end
        user_check.password = ('a'..'z').to_a.shuffle.first(10).join
        user_check.required_domain_email_validation = @chain.chain_setting.domain_email_validation
        if !params[:favorite_location].blank?
          rest = @chain.restaurants.where("id = ? ", params[:favorite_location]) rescue []
          render :json => {:status => false, :notice => "Favorite Location not valid"} and return if rest.blank?
        end
        valid = user_check.valid?
        if !valid && !params[:phone_number].blank? && !users.select{|a| a.phone_number == params[:phone_number]}.blank?
          user_check.errors.messages.delete(:phone_number)
        end
        unless user_check.errors.messages.blank?
          p "user is not valid ------"
          p  user_check.errors
          custom_error = []
          if !user_check.errors.messages[:email].blank? && user_check.errors.messages[:email].first == "Please insert a valid email address"
            custom_error <<  "Please enter a valid email address."
          end
          if !user_check.errors.messages[:phone_number].blank? && user_check.errors.messages[:phone_number].first == "Please enter a valid 10 digit phone number"
            custom_error << "Please enter a valid phone number."
          end
          if !user_check.errors.messages[:phone_number].blank? && user_check.errors.messages[:phone_number].first == "This phone number is already taken. Please use the Login button to continue, or use a secondary phone number to Sign Up"
            custom_error << "The phone number you entered is already in use. Please enter a new phone number."
          end
          render :json => {:status => false, :notice => custom_error.blank? ? "Please complete all required fields with valid information" : custom_error.join(", "), :dev_message => user_check.errors.messages} and return
        end

        user = user_check
        if !user.id.blank?
          user.save
          if user.olo_profile
            user.olo_profile.update_attributes(:account_token => params["account_token"])
          else
            OloProfile.create(user_id: user.id, account_token: params["account_token"])
          end
          user.reload
          user.reset_authentication_token!
          notice_text = params[:referral_code].blank? ? (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 4)) : (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 3))
          return render :json => {:status => true, :auth_token => user.authentication_token,
                                  :notice => notice_text, :account_token => user.olo_profile.try(:account_token)}
        else
          user = User.new(params[:user])
          user.password = ('a'..'z').to_a.shuffle.first(10).join
          if user.save
            if user.olo_profile
              user.olo_profile.update_attributes(:account_token => params["account_token"])
            else
              OloProfile.create(user_id: user.id, account_token: params["account_token"])
            end
            user.reload
            user.reset_authentication_token!
            notice_text = params[:referral_code].blank? ? (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 4)) : (ApiResponseText.get_message_chain_by_locale(@chain.id, user.locale_id, 3))
            #MOVED TO DELAYED JOB
            Delayed::Job.enqueue(UserSignupJob.new(@chain, user, params))
            return render :json => {:status => true, :auth_token => user.authentication_token,
                                    :notice => notice_text, :account_token => user.olo_profile.try(:account_token)}
          else
            p user.errors
            render :json => {:status => false, :notice => "Please complete all required fields with valid information", :dev_message => user.errors.messages} and return
          end
        end
      when "2"
        user_check = User.unscoped.where("email = '#{params[:email]}' AND active IS TRUE and deleted_at IS NULL AND chain_id = #{@chain.id}").order("current_sign_in_at DESC").first
        p user_check
        params[:user][:olo_registration] = false #add this to skip validation for firstname and lastname
        if user_check.blank?
          user_check = User.new(params[:user])
        else
          params[:user].reject! {|key, value| key == "marketing_optin"} if params[:marketing_optin].blank?
          params[:user].reject! {|key, value| key == "marketing_optin_texting"} if params[:marketing_optin_texting].blank?
          user_check.attributes = params[:user].reject { |key,value| value.nil? }
        end
        user_check.password = ('a'..'z').to_a.shuffle.first(10).join
        user = user_check
        if !user.id.blank?
          user.save
          if user.olo_profile
            user.olo_profile.update_attributes(:account_token => params["account_token"])
          else
            OloProfile.create(user_id: user.id, :account_token => params["account_token"])
          end
          user.reload
          user.reset_authentication_token!
        else
          if user.save
            if user.olo_profile
              user.olo_profile.update_attributes(:account_token => params["account_token"])
            else
              OloProfile.create(user_id: user.id, :account_token => params["account_token"])
            end
            user.reload
            user.reset_authentication_token!
          else
            p "error when saving users"
            p user.errors
            render :json => {:status => false, :notice => "Something went wrong with your request. Try to email or call app customer support."} and return
          end
        end
        UserDeviceLog.add_log(user, params[:android_id], params[:keychain], (params[:sign_in_device_type] || params[:register_device_type]).downcase.tr(' ', '_'), "Olo Login", Time.current)
        return render :json => {:status => true, :auth_token => user.authentication_token,
                                :notice => t(:welcome_back), :account_token => user.olo_profile.try(:account_token)}
    end
   end

  def update_profile
    user = current_user
    user.email = params[:email] unless params[:email].blank?
    user.first_name = params[:first_name] unless params[:first_name].blank?
    user.last_name = params[:last_name] unless params[:last_name].blank?
    user.phone_number = params[:phone_number] unless params[:phone_number].blank?
    user.favorite_location = params[:favorite_location] unless params[:favorite_location].blank?
    user.dob_day = params[:dob_day] unless params[:dob_day  ].blank?
    user.dob_month = params[:dob_month] unless params[:dob_month].blank?
    user.dob_year = params[:dob_year] unless params[:dob_year].blank?
    user.marketing_optin = User.marketing_optin_value(params[:marketing_optin]) unless params[:marketing_optin].blank?
    user.favorite_menu_item = params[:favorite_menu_item] unless params[:favorite_menu_item].blank?
    user.zipcode = params[:zipcode] unless params[:zipcode].blank?
    user.app_usage_purpose = params[:app_usage_purpose] unless params[:app_usage_purpose].blank?
    user.marketing_optin_texting = params[:marketing_optin_texting] unless params[:marketing_optin_texting].blank?
    user.gender = params[:gender] unless params[:gender].blank?
    update_contact_detail = false

    if !params[:favorite_location].blank?
      rest = @chain.restaurants.where("id = ? ", params[:favorite_location]) rescue []
      render :json => {:status => false, :notice => "Favorite Location not valid"} and return if rest.blank?
    end

    if params[:email].present?
      exist_email = @chain.users.find_by_email(params[:email])
      render :json => {:status => false, :notice => "We are Sorry! The email is already associated with an account."} and return if exist_email.present? && exist_email.id != user.id
    end
    if params[:phone_number].present?
      if @chain.unique_phone_number_required
        exist_phone = @chain.users.find_by_phone_number(params[:phone_number])
        render :json => {:status => false, :notice => "We are Sorry! The phone number is already associated with an account."} and return if exist_phone.present? && exist_phone.id != user.id
      end
      update_contact_detail = true
    end

    render :json => {:status => false, :notice => "first name must more than 1 characters"} and return if params[:fname].present? && params[:fname].length < 2
    render :json => {:status => false, :notice => "last name must more than 1 characters"} and return if params[:lname].present? && params[:lname].length < 2

    account_id = user.try(:olo_profile).try(:account_token) rescue nil
    render :json => {:status => false, :notice => "We were unable to update your information. Please try again."} and return if account_id.blank?

    success2 = true
    olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    olo_api_user = Olo::Api::User.new(olo_connect_setting)
    success1, message1 = olo_api_user.update_profile(user, params)
    success2, message2 = olo_api_user.update_profile_detail(user, params) if update_contact_detail

    if user.save
      if success1 && success2
        render :json => {:status => true, :notice => "Your profile information has been updated."} and return
      else
        render :json => {:status => false, :notice =>  "We were unable to update your information. Please try again.", :dev_message => {:update_profile => success1, :error_msg => message1, :update_profile_detail => success2, :error_message_detail => message2} } and return
      end
    else
      render :json => {:status => false, :notice =>  "We were unable to update your information. Please try again.", :dev_message => user.errors } and return
    end
  end

  def merge_profile
    p "masuk merge_profile"
    user = current_user

    account_id = user.try(:olo_profile).try(:account_token) rescue nil
    render :json => {:status => false, :notice => "We were unable to get your information. Please try again."} and return if account_id.blank?

    olo_connect_setting = OloConnectSetting.get_olo_connect_setting(@chain.id) rescue nil
    olo_api_user = Olo::Api::User.new(olo_connect_setting)
    profile, profile_data = olo_api_user.get_olo_profile(user)
    phone_number, phone_number_data = olo_api_user.get_olo_phone_number(user)

    if user.save
      if profile && phone_number
        render :json => {:status => true,
                :user => user.as_json.except("email,first_name,last_name,phone_number").merge(
                    :email => (profile_data["emailaddress"] rescue nil),
                    :first_name => (profile_data["firstname"] rescue nil),
                    :last_name => (profile_data["lastname"] rescue nil),
                    :phone_number => (phone_number_data["contactdetails"] rescue nil),
                    :favorite_location_name => (user.try(:favorite_restaurant).try(:app_display_text) rescue nil),
                    :mall_employee => user.try(:user_profile).try(:mall_employee),
                    :retailer => user.try(:user_profile).try(:retailer),
                    :milestone_points => user.user_milestone_points,
                    :weekly_receipt_approved => user.approved_receipt_count("weekly"),
                    :monthly_receipt_approved => user.approved_receipt_count("monthly"),
                    :yearly_receipt_approved => user.approved_receipt_count("yearly"),
                    :profile_pic => (user.try(:user_profile).try(:avatar).try(:url) rescue nil)
                )

        } and return
      else
        render :json => {:status => false, :notice =>  "We were unable to get your information. Please try again.", :dev_message => {:update_profile => profile, :error_msg => profile_data, :update_profile_detail => phone_number, :error_message_detail => phone_number_data} } and return
      end
    else
      render :json => {:status => false, :notice =>  "We were unable to get your information. Please try again.", :dev_message => user.errors } and return
    end
  end

end