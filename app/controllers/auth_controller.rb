class AuthController < ApplicationController
  before_action :set_appkey
  before_action :clear_session, :only => :authorize
  before_action :authenticate_user!, :except => [:access_token, :user]
  skip_before_action :verify_authenticity_token, :only => [:access_token]

  def welcome
    render :text => "Hiya! #{current_user.first_name} #{current_user.last_name}"
  end

  def authorize_mobile_app
    AccessGrant.prune!
    authentication_token = params[:auth_token]

    if authentication_token.nil?
      render :json => {:error => "Could not authenticate authentication token"}
      return
    else
      user_login = User.find_by_authentication_token(params[:auth_token])
      sign_in(user_login) if user_login
      session[:redirect_uri] = params[:redirect_uri] if params[:redirect_uri].present?
      session[:state] = params[:state] if params[:state].present?

      if user_login.first_name.blank? || user_login.last_name.blank?
        redirect_to edit_user_user_path(user_login)
      else
        access_grant = user_login.access_grants.create({:client_id => application.appkey, :state => session[:state]}, :without_protection => true)
        redirect_to access_grant.redirect_uri_for(params[:redirect_uri])
      end

    end
  end

  def authorize
    AccessGrant.prune!
    access_grant = current_user.access_grants.create({:client_id => application.appkey, :state => params[:state]}, :without_protection => true)
    session[:redirect_uri] = params[:redirect_uri] if params[:redirect_uri].present?
    redirect_to access_grant.redirect_uri_for(params[:redirect_uri])
  end

  def access_token
    application = ApplicationKey.where(:appkey => params[:client_id]).first

    if application.nil?
      render :json => {:error => "Could not find application"}
      return
    end

    access_grant = AccessGrant.authenticate(params[:code], application.appkey)
    if access_grant.nil?
      render :json => {:error => "Could not authenticate access code"}
      return
    end

    access_grant.start_expiry_period!
    render :json => {:access_token => access_grant.access_token, :refresh_token => access_grant.refresh_token, :expires_in => Devise.timeout_in.to_i}
  end

  def failure
    render :text => "ERROR: #{params[:message]}"
  end

  def user
    user = authenticate_oauth_user!(params)
    if user
      hash = {
          "provider" => 'olo',
          "id" => user.id,
          "user_profile_url" => user_users_url,
          "first_name" => user.first_name || "Guest",
          "last_name" => user.last_name  || "User",
          "email" => user.email,
          "contact_phone" => user.phone_number
      }
    else
      hash = {}
    end

    p "-----------------------"
    p hash
    render :json => hash.to_json
  end

  # Incase, we need to check timeout of the session from a different application!
  # This will be called ONLY if the user is authenticated and token is valid
  # Extend the UserManager session
  def isalive
    warden.set_user(current_user, :scope => :user)
    response = { 'status' => 'ok' }

    respond_to do |format|
      format.any { render :json => response.to_json }
    end
  end

  def session_destroy
    access_grant = AccessGrant.where(:access_token => params[:oauth_token]).first
    user = access_grant.try(:user)
    sign_out(user) if user
    access_grant.destroy if access_grant
    render :json => {:status => "Logged Out"}
  end

  def clear_session
    if (request.url.to_s.include?("auth/olo/authorize?") and !flash[:original_referer].to_s.include?("signin"))
      puts "Current_user is #{current_user.id} and url = #{request.url.to_s}"

      if !flash[:original_referer].to_s.include?("signup") and !flash[:original_referer].to_s.include?("facebook")
        # delete first
        AccessGrant.where(:user_id => current_user.id).delete_all
        sign_out(current_user)
      end

    end if !current_user.blank?
  end

  protected

  def application
    @application ||= ApplicationKey.find_by_appkey(params[:client_id])
  end

end