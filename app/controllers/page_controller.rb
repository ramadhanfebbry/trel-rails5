require 'net/http'
require 'uri'

class PageController < ApplicationController
  layout 'tokenex'
  before_action :set_page_chain, only: [:heartland, :tokenex_htp, :contact, :contact_submit]
  def tokenex_htp
    if @chain_page_name.present? && template_exists?(@chain_page_name, "page")
      url = "#{TOKENEX_API_HOST}/mobilepay/api/v2/tokenex/session"
      body = {"originURL" => TOKENEX_API_ORIGIN_URL}
      uri   = URI(url)
      @headers = {
        "auth_token" => params[:auth_token],
        "appkey" => params[:appkey]
      }

      @response = HTTParty.post(url, body: body,  headers: @headers )
      @response  = JSON.parse(@response.body)
            @card_types = [["Visa", "Visa"], ["MasterCard", "MasterCard"], ["AMEX", "amex"], ["Diners Club", "diners"], ["Discover", "discover"], ["JCB", "jcb"]]
      render @chain_page_name
    else
      render "maintenance"
    end
  end

  def submit_tokenex_htp
    @headers = {
      "auth_token" => params[:auth_token],
      "appkey" => params[:appkey]
    }
    begin
      url = "#{TOKENEX_API_HOST}/mobilepay/api/v2/tokenex/session/success"
      expiration = params[:expiration].split("/")

      card_type = ["VISA", "MASTERCARD", "AMEX", "DINERS_CLUB", "DISCOVER", "JCB"]
      params[:cardType] = "diners_club"if params[:cardType] == "diners"
      if card_type.include?(params[:cardType].upcase)
        body = {
          expiryYear: expiration[1],
          expiryMonth: expiration[0],
          cardType: params[:cardType].upcase,
          sessionID: params[:sessionID],
          token: params[:token],
          cardLastDigits: params[:cardLastDigits],
          referenceNumber: params[:referenceNumber],
          zipCode: params[:zip_code]
        }
        body[:address] = params[:address] if params[:address]
        Rails.logger.info "Tokenex: #{body.inspect}"


        @response = HTTParty.post(url, body: body,  headers: @headers )
        Rails.logger.info @response
        response  = JSON.parse(@response.body)
        if response["status"] == true
          render text: response["message"]
        else

          redirect_to "/page/#{params[:chain_id]}/tokenex_htp?#{@headers.to_query}",  :alert => "System error, please try again"
        end

      else
        redirect_to "/page/#{params[:chain_id]}/tokenex_htp?#{@headers.to_query}",  :alert => "System error, please try again"
      end
    rescue
      redirect_to "/page/#{params[:chain_id]}/tokenex_htp?#{@headers.to_query}",  :alert => "System error, please try again"
    end
  end

  def contact
    begin
      @parameter_return = prepare_set_url_parameter(params)
    # -    @user         = @chain.users.find_by_authentication_token(params[:auth_token]) if params[:auth_token].present? rescue nil
    user             = get_account_info
    @user = {}
    if user["User"]
      @user[:first_name] = user["User"]["FirstName"]
      @user[:last_name] = user["User"]["LastName"]
      @user[:email] = user["User"]["EMail"]
      @user[:phone_number] = user["User"]["PhoneNumber"]
    end
    @topics       = get_topic_peiwei
    @restaurants  = []
    get_location_peiwei.each do |data|
      restaurant      = data["value"]
      @restaurants << {name: restaurant["Shop"],
        address: restaurant["Address1"],
        city: "#{restaurant["City"]}, #{restaurant["State"]} #{restaurant["Zip"]} ",
        value: "#{restaurant["Shop"]}, #{restaurant["City"]}, #{restaurant["State"]}",
        StoreID: restaurant["StoreID"],
        label: "#{restaurant["Address1"]} #{restaurant["City"]} #{restaurant["State"]} #{restaurant["Zip"]}  #{restaurant["Shop"]}"
      }
    end
      # @chain = Chain.first  if Rails.env == "development"
      # restaurants = @chain.restaurants.active.select("restaurants.name, app_display_text, zipcode, address, cities.name as city_name, regions.abbreviation").joins(city: [region: :country])
      # @restaurants = []
      # restaurants.each do |restaurant|
      #   @restaurants << {name: restaurant.app_display_text,
      #     address: restaurant.address,
      #     city: "#{restaurant.city_name}, #{restaurant.abbreviation} #{restaurant.zipcode} ",
      #     label: "#{restaurant.app_display_text}, #{restaurant.city_name}, #{restaurant.abbreviation}"
      #   }
      # end


      render @chain_page_name+"_contact", layout: false
    rescue
      render text: "System error, please try again"
    end
  end

  def contact_submit
    parameter_return = prepare_set_url_parameter(params)
    # begin

    header = {"X-XSRF-TOKEN" => Base64.encode64(cookies["XSRF-TOKEN"])}
    url         = "https://www.peiwei.com/api/feedback/contact"
    body        = params[:contact]
    body[:VisitDate] = body[:VisitDate].present? ? "/Date(#{body[:VisitDate].to_datetime.to_i}+0000)/" : "/Date(#{Time.now.to_i}+0000)/"
    headers          = header.merge(set_header)
    puts headers
    @response        = HTTParty.post(url,  headers: headers, body: body.to_json)
    @response_body   = JSON.parse(@response.body)

    if @response_body["Errors"].empty?
      redirect_to "/page/contact/#{params[:appkey]}?#{parameter_return}",  :notice => "Thank you for sharing your feedback with us. We will be in touch with you soon."
    else
      redirect_to "/page/contact/#{params[:appkey]}?#{parameter_return}",  :alert => "Please check and try again."
    end
    # rescue
    #   redirect_to "/page/contact/#{params[:appkey]}?#{parameter_return}",  :alert => "Please check and try again."
    # end
  end

  def heartland
    if @chain_page_name.present? && File.exists?(Rails.root.join("app", "views", params[:controller], "#{@chain_page_name}.html.erb")) #template_exists?(@chain_page_name, "page")
      url = "#{TOKENEX_API_HOST}/mobilepay/api/v2/heartland/method/initialize"
      body = {"originURL" => TOKENEX_API_ORIGIN_URL}
      uri   = URI(url)
      @headers = {
        "auth_token" => params[:auth_token],
        "appkey" => params[:appkey]
      }

      # @response = HTTParty.post(url, body: body,  headers: @headers )
      # @response  = JSON.parse(@response.body)
      @response = {
        status: true,
        code: 0,
        merchantPublicKey: "pkapi_cert_JTFQdMngm8hRwAKlBZ",
        sessionID: "HbZkPxsEX24662881U352747IDFVQ16hhj",
        completeURL: "http://fundy.relevantmobile.com:8090/mobilepay/api/v2/heartland/method/complete"
      }
      render 'heartland', layout: false
    else
      render "maintenance"
    end
  end

  private

  def set_page_chain
    application_key = ApplicationKey.where(:appkey => params[:appkey]).first
    app = application_key.application rescue nil
    @chain = app.chain rescue nil
    @chain_page_name = if @chain.present? && ["contact", "contact_submit"].include?(params[:action])
      @chain.name.gsub(/[^\w]/, "_").downcase
    elsif @chain.present? && @chain.id == params[:chain_id].to_i
      @chain.name.gsub(/[^\w]/, "_").downcase
    else
      nil
      "tijuana_flats" if Rails.env == "development"
    end
  end

  def get_topic_peiwei
    headers = set_header
    url = "https://www.peiwei.com/api/feedback/topics"
    response       = HTTParty.get(url,  headers: headers)
    response_body  = JSON.parse(response.body)
    return response_body["Topics"] rescue []
  end

  def get_location_peiwei
    headers = set_header
    url = "https://www.peiwei.com/api/location/autocomplete"
    response       = HTTParty.get(url,  headers: headers)
    response_body  = JSON.parse(response.body)
    return response_body["Data"] rescue []
  end

  def prepare_set_url_parameter(parameters)
    param = {}
    param["Authorization"] = parameters["Authorization"] if parameters["Authorization"].present?
    param["Recognition"] = parameters["Recognition"] if parameters["Recognition"].present?
    param["auth_token"] = parameters["auth_token"] if parameters["auth_token"].present?
    return param.to_query
  end

  def set_header
    headers         = { "Content-Type"  => "application/json" }
    headers["Authorization"] = params["Authorization"] if params["Authorization"]
    headers["Recognition"] = params["Recognition"] if params["Recognition"]
    return headers
  end

  def get_account_info
    headers = set_header
    url = "https://www.peiwei.com/api/account/authenticatedProfile"
    response       = HTTParty.get(url,  headers: headers)
    return_s = if response.code == 200
      response_body  = JSON.parse(response.body)
    else
      {}
    end
    return return_s
  end
end
