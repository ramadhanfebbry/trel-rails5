class MenusController < ApplicationController

  #def terms_of_use
  #  appkey = params[:appkey]
  #  application_key = ApplicationKey.where(:appkey => appkey).first rescue nil
  #  chain = application_key.application.chain rescue nil
  #  locale_key = params[:locale] || "en"
  #  @content = Term.content_by_chain_and_locale(chain, locale_key)  rescue ""
  #end
  #
  #def privacy_policy
  #  appkey = params[:appkey]
  #  application_key = ApplicationKey.where(:appkey => appkey).first rescue nil
  #  chain = application_key.application.chain rescue nil
  #  locale_key = params[:locale] || "en"
  #  @content = PrivacyPolicy.content_by_chain_and_locale(chain, locale_key)  rescue ""
  #end

  def confirm_email
    user = UserConfirmation.find_by_confirm_token(params[:id])
    if user
      user = User.find user.user_id
      user.email_activate
      return render :json => {:status => true, :message => "Your accout has activate, you should be able to login"}
    else
      return render :json => {:status => false, :message => "Something wrong with your activation key"}
    end
  end


  def terms_of_use
    appkey = params[:appkey]
    application_key = ApplicationKey.where(:appkey => appkey).first rescue nil
    chain = application_key.application.chain rescue nil
    locale_key = params[:locale] || "en"
    if chain.blank?
      @content = Term.content_by_locale(locale_key)
    else
      @content = TextTileContent.content_by_chain_and_locale(chain, locale_key, "term")  rescue ""
    end
  end

  def privacy_policy
    appkey = params[:appkey]
    application_key = ApplicationKey.where(:appkey => appkey).first rescue nil
    chain = application_key.application.chain rescue nil
    locale_key = params[:locale] || "en"
    if chain.blank?
      @content = PrivacyPolicy.content_by_locale(locale_key)
    else
      @content = TextTileContent.content_by_chain_and_locale(chain, locale_key, "privacy_policy")  rescue ""
    end
  end

  def faq
    appkey = params[:appkey]
    application_key = ApplicationKey.where(:appkey => appkey).first rescue nil
    chain = application_key.application.chain rescue nil
    locale_key = params[:locale] || "en"
    @content = TextTileContent.content_by_chain_and_locale(chain, locale_key, "faq")  rescue ""
    render :template => "menus/terms_of_use"
  end

  def about_us
    appkey = params[:appkey]
    application_key = ApplicationKey.where(:appkey => appkey).first rescue nil
    chain = application_key.application.chain rescue nil
    locale_key = params[:locale] || "en"
    @content = TextTileContent.content_by_chain_and_locale(chain, locale_key, "about_us")  rescue ""
    render :template => "menus/terms_of_use"
  end

  def contact_us_faq
    appkey = params[:appkey]
    application_key = ApplicationKey.where(:appkey => appkey).first rescue nil
    chain = application_key.application.chain rescue nil
    locale_key = params[:locale] || "en"
    @content = TextTileContent.content_by_chain_and_locale(chain, locale_key, "contact_us_faq")  rescue ""
    render :template => "menus/terms_of_use"
  end

  def game_faq
    appkey = params[:appkey]
    application_key = ApplicationKey.where(:appkey => appkey).first rescue nil
    chain = application_key.application.chain rescue nil
    locale_key = params[:locale] || "en"
    @content = TextTileContent.content_by_chain_and_locale(chain, locale_key, "game_faq")  rescue ""
    render :template => "menus/terms_of_use"
  end

  def url
    begin
      chain = Chain.find(params[:chain_id])
      chain_url = chain.chain_urls.find_by_slug(params[:url_id])
      redirect_to chain_url.url
    rescue
      redirect_to "http://relevantmobile.com/"
    end
  end

  def download_reward_transactions
    data = []
    RewardTransaction.all.each_with_index do |rt, i|
      data << rt.restaurant_and_reward_by_lat_lng(rt.latitude, rt.longitude)
    end
    respond_to do |format|
      format.html
      format.js
      format.csv {export_rt_csv data }

    end
  end

  def status_url_skrill_payment
    p "status_url_skrill_payment ---"
    p params
    render :text => "status_url_skrill_payment ---"
  end


  def export_rt_csv(rts)

    rt_csv = CSV.generate do |csv|
      # header row
      csv << [
        "Reward Transction ID",
        "User Email",
        "Reward",
        "Chain",
        "GET RESTAURANT FROM",
        "Lat and lng",
        "Before Calculate",
        "AFTER cal(Closest Restaurant)",
        "distance before",
        "distance after",
        "different",
        "REWARD WALLET ID"
      ]

      # data rows
      rts.each do |rt|
        csv << [
          rt[0], rt[1], rt[2], rt[3], rt[4], rt[5], rt[6], rt[7], rt[8], rt[9], rt[10], rt[11]
        ]
      end
    end

    send_data(rt_csv, :type => 'text/csv', :filename => 'reward_transction.csv')
  end

  def generated_faq
    p "---------"
    p faq_data = params[:faq_data]
    arr_faq_data = faq_data.split("-")
    if arr_faq_data.count == 3
      chain_id = arr_faq_data[1]
      faq_id = arr_faq_data[2]
      if chain = Chain.find(chain_id)
        if faq = chain.faqs.find(faq_id)
          @content = faq.generated_html_content
        else
          @content = "Request Failed"
        end
      else
        @content = "Request Failed"
      end
    else
      @content = "Request Failed"
    end
  end

  def show_receipt
    id = Base64.decode64(params[:url_id])
    @receipt = Receipt.find(id)
    if @receipt.is_receipt_barcode
      @transaction = @receipt.last_transaction rescue nil
      @pos_check_upload = @receipt.pos_check_upload rescue nil
      @pos_xml = PosReceiptXml.new(@pos_check_upload.xml_data) rescue nil
      @payment_total = @pos_xml.total rescue 0
      @menu_items = @pos_xml.xml_in_nokogiri_format.search("menu-item") rescue []
      @menu_items_hash = {}
      @menu_items.map{|a| @menu_items_hash[a.attr("id").to_s] = (a.search("name").text rescue nil)}
      check_id = @pos_xml.check_id rescue nil
      seq_num = @pos_xml.seq_num rescue nil
      @reward_dicounts = RewardDiscount.where(:check_id => check_id, :seq_num =>  seq_num, :restaurant_id => @pos_check_upload.pos_location.restaurant_id) rescue nil
      @discount_total = @pos_xml.discount_total rescue nil
      @direct_discounts = @pos_xml.xml_in_nokogiri_format.search("discount") rescue []
    elsif @receipt.is_ncr_receipt
      @transaction = @receipt.last_transaction rescue nil
      @ncr_data_receipt = Dashboard::NcrDataReceipt.find(@receipt.ncr_data_receipt_id) rescue nil
      json_encode = @ncr_data_receipt.json_data.gsub("=>",":").gsub("nil", "0") rescue nil
      @receipt_data_in_json = ActiveSupport::JSON.decode(json_encode)
    end
    render :layout => false
  end
  
end
