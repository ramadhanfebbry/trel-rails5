class ChartsController < ApplicationController
  layout false

  before_action :find_chain_for_chart
  before_action :find_survey, :only => [:avg_surveys,:survey_question1, :survey_question2,:survey_question3,:survey_question4,:survey_question5]
  before_action :left_filter, :only => [:active_members, :members, :average_frequency,:average_check_frequency,
    :avg_surveys,:survey_question1, :survey_question2,:survey_question3,:survey_question4,:survey_question5
  ]

  def active_members
    filter = sort_by(params)
    @chart_data = [] ; @restaurants = []
    restaurant_id_filter = params[:check_values].split('_').compact || nil if params[:check_values]
    @restaurants = @restaurants_cache
    unless restaurant_id_filter.blank?
      @restaurants = []
      @restaurants_cache.each do |res|
        @restaurants << res if restaurant_id_filter.include?(res.id.to_s)
      end
      @restaurants = @restaurants.compact.flatten
    end
    #@restaurants.to_a.delete_if{|x| ! restaurant_id_filter.include?(x.id.to_s)} unless restaurant_id_filter.blank?
    data = nil
    all_data = []
    @restaurants = [@restaurants[0]] if @restaurants.size <= 10 and restaurant_id_filter.blank? and @filter_location.blank?
    @restaurants = @restaurants[0..2] if @restaurants.size > 10 and restaurant_id_filter.blank? and @filter_location.blank?
    if params[:date].blank? and !@filter.blank?
      params[:date] = @filter
    end
    ## if its an average

    return @chart_data = [DashboardQuery::ActiveMember.average_active_members_generator(@restaurants,filter, params[:date], @chain)] if params["avg"]  && params["avg"].eql?('true') && params[:date] != "day" or (!@filter_average.blank? and params["avg"].blank?)
    #xxx
    ### if its a day filter
    puts "Chart::ActiveMembers ======="
    puts "restaurant_list ======= #{@restaurants}"
    puts "restaurant_list ======= #{@restaurants.size}"
    return @chart_data = DashboardQuery::ActiveMember.active_members_generator_day(@restaurants,filter, params[:date], @chain) if params[:date] == "day" and params["avg"].blank?

    return @chart_data = DashboardQuery::ActiveMemberV2.average_populate(@restaurants,filter, params[:date], @chain) if params[:date] == "day" and params["avg"] && params["avg"].eql?('true') and @restaurants.size > 5

    # usual chart
    return @chart_data = DashboardQuery::ActiveMemberV2.populate_daily_users(@restaurants,filter, params[:date], @chain, current_owner)
    #@restaurants.each_with_index do |restaurant|
    #  ch = restaurant.chain rescue @chain
    #  if restaurant_id_filter.blank?
    #    data = restaurant.active_members_chart(filter, params[:date], ch)
    #    @chart_data << {:name => restaurant.dashboard_display_text, :data => data}
    #    all_data << data
    #  else
    #    if restaurant_id_filter.include?(restaurant.id.to_s)
    #      data = restaurant.active_members_chart(filter, params[:date], ch)
    #      @chart_data << {:name => restaurant.dashboard_display_text, :data => data}
    #      all_data << data
    #    end
    #  end
    #end

    if params["avg"] == "true" #and params[:date] == "day"
      res_count = []
      current_owner.chains.each{|x| res_count << x.restaurants.size}
      result = []
      size = all_data.size
      all_data = all_data.sort{|x| -x.size}
      dates = all_data.flatten.delete_if{|x| x.to_i < 10000000}

      unless all_data[0].blank?
        dates.sort.each do |dt|
          total = 0 #
          date = dt
          res_count_tmp = 0
          0.upto(size + 1) do |cnt|
            begin
              all_data[cnt].each do |data|
                begin
                  if date == data[0]
                    total += data[1]
                    res_count_tmp += 1
                  end
                rescue
                  next
                end
              end
            rescue
              next
            end
          end
          result << [date, (total/res_count_tmp).round]  if total > 0
        end
      end

      @chart_data = [{:name => "Avg", :data => result}]
    else
      @chart_data = @chart_data.uniq
    end
  end

  def members
    filter = sort_by(params)
    count = []
    all_data = []
    restaurant_id_filter = params[:check_values].split('_').compact || nil  if params[:check_values]
    colors = {}

    @restaurants = @restaurants_cache
    unless restaurant_id_filter.blank?
      @restaurants = []
      @restaurants_cache.each do |res|
        @restaurants << res if restaurant_id_filter.include?(res.id.to_s)
      end
      @restaurants = @restaurants.compact.flatten
    end

    #@restaurants.delete_if{|x| ! restaurant_id_filter.include?(x.id.to_s)} unless restaurant_id_filter.blank?

    @restaurants = [@restaurants[0]] if @restaurants.size <= 10  and restaurant_id_filter.blank? and @filter_location.blank?
    @restaurants = @restaurants[0..2] if @restaurants.size > 10 and restaurant_id_filter.blank?  and @filter_location.blank?
    @restaurants.each_with_index do |res, index|
      colors.merge!({"#{res.dashboard_display_text}" => CHART_COLOR[index]})
    end
    if params[:date].blank? and !@filter.blank?
      params[:date] = @filter
    end
    if (params["avg"] && params["avg"].eql?("true") && params[:date] != "day") or (!@filter_average.blank? and params["avg"].blank?)
      @chart_data =  DashboardQuery::Member.average_member_chart_restaurant_user(nil, @chains, @restaurants, params[:date], @chain, user_ids = nil)
      return @chart_data
    elsif params["avg"] && params["avg"].eql?("true") && params[:date] == "day"
      @chart_data =  DashboardQuery::Member.average_day_member_chart_restaurant_user(nil, @chains, @restaurants, params[:date], @chain, user_ids = nil)
    end
    @restaurants.each_with_index do |res, index|
      ch = res.chain rescue @chain
      user_ids = nil
      #user_ids =  ch.users.select("distinct users.id,users.created_at").where("users.active = ?", true)
      #count << Restaurant.member_chart(filter,@chains, [res], params[:date],ch,user_ids)# if index == 0
      data = Restaurant.member_chart_restaurant_user(filter,@chains, [res], params[:date],ch,user_ids)
      count << {:name => res.dashboard_display_text,
                :data => data,
                :color => colors["#{res.dashboard_display_text}"] }# if index == 0
      all_data << data
    end
    @chart_data = count

    if (params["avg"] && params["avg"].eql?("true") && params[:date] != "day") or (!@filter_average.blank? and params["avg"].blank?)
      res_count = []
      current_owner.chains.each{|x| res_count << x.restaurants.size}
      result = []
      size = all_data.size
      all_data = all_data.sort{|x| -x.size}

      dates = all_data.flatten.delete_if{|x| x.to_i < 10000000}

      tmp_date = nil
      unless all_data[0].blank?
        dates.sort.each do |dt|
          total = 0 #
          date = dt
          res_count_tmp = 0
          0.upto(size + 1) do |cnt|
            begin
              all_data[cnt].each_with_index do |data,index|
                begin

                  puts index
                  #if params[:date] != "day"
                  if date == data[0]
                    total += data[1]
                    res_count_tmp += 1
                  end
                    #else
                    #  if date.hour == data[0].hour
                    #    total += data[1]
                    #    res_count_tmp += 1
                    #  end
                    #end
                rescue
                  next
                end
              end
            rescue
              next
            end
          end
          total_avg = (total.to_f/@restaurants.size.to_f).round(2)
          total_avg = nil if total_avg.nan?
          if params[:date] != "day"
            result << [date,total_avg ]
          else
            result << [date, total_avg] if tmp_date != date or total_avg.to_f > 0.0
          end
          tmp_date = date
        end
      end
      #puts "result = #{result.uniq!}"
      #result.uniq!.sort! {|a,b| a[0] <=> b[0] } if params[:date] == "day" && result.uniq!
      @chart_data = [{:name => "Avg", :data => result}]
    else
      @chart_data = count
    end
  end

  #def members
  #  filter = sort_by(params)
  #  count = []
  #  restaurant_id_filter = params[:check_values].split('_').compact || nil  if params[:check_values]
  #
  #  #puts   params[:check_values].split('_').compact   if params[:check_values]
  #  @restaurants.delete_if{|x| ! restaurant_id_filter.include?(x.id.to_s)} unless restaurant_id_filter.blank?
  #
  #  @restaurants = [@restaurants[0]] if @restaurants.size <= 10  and restaurant_id_filter.blank?
  #  @restaurants = @restaurants[0..2] if @restaurants.size > 10 and restaurant_id_filter.blank?
  #  #@restaurants.each_with_index do |res, index|
  #  #  ch = res.chain rescue @chain
  #  #  user_ids =  ch.users.select("distinct users.id,users.created_at").where("users.active = ?", true)
  #  #  count << Restaurant.member_chart(filter,@chains, [res], params[:date],ch,user_ids)# if index == 0
  #  #end
  #  user_ids =  @chain.users.select("distinct users.id,users.created_at").where("users.active = ?", true)
  #  count = Restaurant.member_chart(filter,@chains, @restaurants, params[:date],@chain,user_ids)# if index == 0
  #  @chart_data = count
  #end

  def average_frequency
    filter = sort_by(params)
    count = []
    restaurant_id_filter = params[:check_values].split('_').compact || nil   if params[:check_values]
    data = nil
    all_data = []
    colors = {}
    @restaurants = @restaurants_cache
    @restaurants = [@restaurants[0]] if @restaurants.size <= 10  and restaurant_id_filter.blank? and @filter_location.blank?
    @restaurants = @restaurants[0..2] if @restaurants.size > 10  and restaurant_id_filter.blank? and @filter_location.blank?
    @restaurants.each_with_index do |res, index|
      colors.merge!({"#{res.dashboard_display_text}" => CHART_COLOR[index]})
    end
    if params[:date].blank? and !@filter.blank?
      params[:date] = @filter
    end
    ## if its an average
    return @chart_data = [ReceiptTransaction.calculate_average_receipt(@restaurants, filter, params[:date])] if params["avg"]  && params["avg"].eql?('true') && params[:date] != "day"  && @restaurants.size > 5 || (!@filter_average.blank? and params["avg"].blank?)
    return @chart_data = [ReceiptTransaction.calculate_average_receipt_day(@restaurants, filter, params[:date])] if params["avg"]  && params["avg"].eql?('true') && params[:date] == "day" && @restaurants.size > 5

    @restaurants.each_with_index do |restaurant|
      ch = restaurant.chain rescue @chain
      if restaurant_id_filter.blank?
        if AverageChartTable.table_exists?
          data = ReceiptTransaction.average_receipt(restaurant.id, filter, params[:date])
        else
          data = ReceiptTransaction.new_average_receipt(restaurant.id, filter, params[:date], ch)
        end
        count << {:name => restaurant.dashboard_display_text,:colors => colors["#{restaurant.dashboard_display_text}"],
          :data => data}
        all_data << data
      else
        if AverageChartTable.table_exists?
          data = ReceiptTransaction.average_receipt(restaurant.id, filter, params[:date])
        else
          data = ReceiptTransaction.new_average_receipt(restaurant.id, filter, params[:date], ch)
        end
        
        if restaurant_id_filter.include?(restaurant.id.to_s)
          count << {:name => restaurant.dashboard_display_text,
            :data => data,:colors => colors["#{restaurant.dashboard_display_text}"]}
        end
        all_data << data
      end

    end

    if params["avg"] == "true"
      res_count = []
      current_owner.chains.each{|x| res_count << x.restaurants.size}
      result = []
      size = all_data.size
      all_data = all_data.sort{|x| -x.size}

      dates = all_data.flatten.delete_if{|x| x.to_i < 10000000}

      tmp_date = nil
      unless all_data[0].blank?
        dates.sort.each do |dt|
          total = 0 #
          date = dt
          res_count_tmp = 0
          0.upto(size + 1) do |cnt|
            begin
            all_data[cnt].each_with_index do |data,index|
              begin

                puts index
                #if params[:date] != "day"
                  if date == data[0]
                    total += data[1]
                    res_count_tmp += 1
                  end
                #else
                #  if date.hour == data[0].hour
                #    total += data[1]
                #    res_count_tmp += 1
                #  end
                #end
              rescue
                next
              end
            end
            rescue
              next
            end
          end
          total_avg = (total.to_f/res_count_tmp.to_f).round(2)
          total_avg = nil if total_avg.nan?
          if params[:date] != "day"
            result << [date,total_avg ]
          else
            result << [date, total_avg] if tmp_date != date or total_avg.to_f > 0.0
          end
          tmp_date = date
        end
      end
      #puts "result = #{result.uniq!}"
      #result.uniq!.sort! {|a,b| a[0] <=> b[0] } if params[:date] == "day" && result.uniq!
      @chart_data = [{:name => "Avg", :data => result}]
    else
      @chart_data = count
    end
  end

  def average_check_frequency
    chart = []
    filter = sort_by(params)
    restaurant_id_filter = params[:check_values].split('_').compact || nil   if params[:check_values]
    @restaurants.each_with_index do |restaurant|
      if restaurant_id_filter.blank?
        chart << {:name => restaurant.dashboard_display_text, :data => generate_chart_avg(restaurant,filter), :stack => "1x"}
      else
        if restaurant_id_filter.include?(restaurant.id.to_s)
          chart << {:name => restaurant.dashboard_display_text, :data => generate_chart_avg(restaurant,filter), :stack => "1x"}
        end
      end
    end

    @chart_data = chart
  end


  def top_one_chart

  end

  def frequency
    ReceiptTransaction.frequency_ratio(@restaurants.map(&:id))
  end

  def avg_surveys
    @chart_data = []
    filter = sort_by(params)
    restaurant_id_filter = params[:check_values].split('_').compact || nil   if params[:check_values]
    @restaurants = [@restaurants[0]] if @restaurants.size <= 10  and restaurant_id_filter.blank?
    @restaurants = @restaurants[0..2] if @restaurants.size > 10  and restaurant_id_filter.blank?
    #restaurant_id_filter = params[:check_values] || nil
    @restaurants.each do |restaurant|
      if restaurant_id_filter.blank?
        @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_avg_chart(filter,@survey, params[:date])}
      else
        if restaurant_id_filter.include?(restaurant.id.to_s)
          @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_avg_chart(filter,@survey, params[:date])}
        end
      end
    end
    render :template => "charts/surveys/avg_surveys"
  end

  def survey_question1
    filter = sort_by(params)
    @chart_data = []

    restaurant_id_filter = params[:check_values].split('_').compact || nil   if params[:check_values]
    @restaurants = [@restaurants[0]] if @restaurants.size <= 10  and restaurant_id_filter.blank?
    @restaurants = @restaurants[0..2] if @restaurants.size > 10  and restaurant_id_filter.blank?
    @restaurants.each_with_index do |restaurant|
      if restaurant_id_filter.blank?
        @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,1, params[:date])}
      else
        if restaurant_id_filter.include?(restaurant.id.to_s)
          @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,1, params[:date])}
        end
      end
    end
    render :template => "charts/surveys/survey_question1"
  end

  def survey_question2
    filter = sort_by(params)
    @chart_data = []

    restaurant_id_filter = params[:check_values].split('_').compact || nil   if params[:check_values]
    @restaurants = [@restaurants[0]] if @restaurants.size <= 10  and restaurant_id_filter.blank?
    @restaurants = @restaurants[0..2] if @restaurants.size > 10  and restaurant_id_filter.blank?

    @restaurants.each_with_index do |restaurant|
      if restaurant_id_filter.blank?
        @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,2 ,params[:date])}
      else
        if restaurant_id_filter.include?(restaurant.id.to_s)
          @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,2, params[:date])}
        end
      end
    end
    render :template => "charts/surveys/survey_question2"
  end

  def survey_question3
    filter = sort_by(params)
    @chart_data = []
    #@survey = Survey.find params[:id]
    restaurant_id_filter = params[:check_values].split('_').compact || nil   if params[:check_values]
    @restaurants = [@restaurants[0]] if @restaurants.size <= 10  and restaurant_id_filter.blank?
    @restaurants = @restaurants[0..2] if @restaurants.size > 10  and restaurant_id_filter.blank?

    @restaurants.each_with_index do |restaurant|
      if restaurant_id_filter.blank?
        @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,3, params[:date])}
      else
        if restaurant_id_filter.include?(restaurant.id.to_s)
          @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,3, params[:date])}
        end
      end
    end
    render :template => "charts/surveys/survey_question3"
  end

  def survey_question4
    filter = sort_by(params)
    @chart_data = []

    restaurant_id_filter = params[:check_values].split('_').compact || nil   if params[:check_values]
    @restaurants = [@restaurants[0]] if @restaurants.size <= 10  and restaurant_id_filter.blank?
    @restaurants = @restaurants[0..2] if @restaurants.size > 10  and restaurant_id_filter.blank?

    @restaurants.each_with_index do |restaurant|
      if restaurant_id_filter.blank?
        @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,4, params[:date])}
      else
        if restaurant_id_filter.include?(restaurant.id.to_s)
          @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,4, params[:date])}
        end
      end
    end
    render :template => "charts/surveys/survey_question4"
  end

  def survey_question5
    filter = sort_by(params)
    @chart_data = []
    #@survey = Survey.find params[:id]
    restaurant_id_filter = params[:check_values] || nil
    @restaurants.each_with_index do |restaurant|
      if restaurant_id_filter.blank?
        @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,5)}
      else
        if restaurant_id_filter.include?(restaurant.id.to_s)
          @chart_data << {:name => restaurant.dashboard_display_text, :data => restaurant.survey_question_chart(filter,@survey,5)}
        end
      end
    end
    render :template => "charts/surveys/survey_question5"
  end

  private

  def find_chain_for_chart
    #@restaurants = current_owner.restaurant_list
    @restaurants_cache = Rails.cache.fetch("find_restaurant_for_chart_#{current_owner.id}",
                                     :expires_in => 6.hours) do
      current_owner.restaurant_list.to_a
    end
    @chain = current_owner.chain
    @chains = [@chain]

    ## this is commented out since the feature is hold
    if Rails.env == "production"
      #@filter_location = REDIS.get "owner_dashboard_setting_#{@chain.id}_location"
      #@filter_average = REDIS.get "owner_dashboard_setting_#{@chain.id}_average"
      #@filter = REDIS.get "owner_dashboard_setting_#{@chain.id}_filter"
    else
      @filter_location = nil
      @filter_average = nil
      @filter = "month"
    end
  end

  def sort_by(params)
    case_issue_date = "(case receipt_transactions.issue_date when
    null then receipts.created_at
    else receipt_transactions.issue_date
    end)"
    sql = "users.created_at"
    if params[:action] == "active_members"
      #sql = "TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}'"
      sql =  "current_date + sd.a"
    elsif  params[:action] == "average_check_frequency"
      #sql = "TIMEZONE('UTC',#{case_issue_date}) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}'"
      sql = case_issue_date
    elsif params[:action] == "avg_surveys"
      sql = "surveys_users.updated_at"
    elsif params[:action] == "members"
      #sql = "TIMEZONE('UTC',#{case_issue_date}) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}'"
      #sql = "date(date)"
      time_zone = Time.zone.now.strftime("%Z")
      date_zone = "date(TIMEZONE('UTC',restaurant_users.created_at AT TIME ZONE '#{time_zone}'))"

      sql = "#{date_zone}"
    elsif params[:action].include?("survey_question") or params[:action] == "avg_surveys"
      sql = "TIMEZONE('UTC', surveys_users.updated_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}'"
    elsif params[:action] == "average_frequency"
      if AverageChartTable.table_exists?
        sql = "receipt_date"
      else
        #sql = "TIMEZONE('UTC', #{case_issue_date}) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}'"
        sql = case_issue_date
      end
    end
    unless params[:date].nil?
      case params[:date]
      when ""
        "all"
      when "all"
        "all"
      when "day"
        "date(#{sql}) = '#{(Time.zone.now).to_date}'"
      when "week"
        "date(#{sql}) between '#{(Time.zone.now - 6.days).to_date }' and '#{(Time.zone.now + 1.days).to_date}'"
      when "month"
        "date(#{sql}) between '#{(Time.zone.now - 30.days).to_date}' and '#{(Time.zone.now + 1.days).to_date }'"
      when "3month"
        "date(#{sql}) between '#{(Time.zone.now - 90.days).to_date}' and '#{ (Time.zone.now + 1.days).to_date}'"
      end
    end
  end

  def generate_chart_avg(restaurant,filter)
    data = ReceiptTransaction.average_frequency_check_chart(restaurant, filter)

    ar1 = data[0]
    ar2 = data[1]
    ar3 = data[2]

    tmp_filter = filter
    if tmp_filter == "all"
      tmp_filter = nil
    elsif tmp_filter.blank?
      tmp_filter = "date(TIMEZONE('UTC', receipts.updated_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') between '#{(Time.zone.now - 30.days).to_date}' and '#{Time.zone.now.to_date}'"
    end

    receipts1 = Receipt.by_restaurant_ids([restaurant.id]).where("user_id in(?)", ar1).where("receipts.status = 3").where(tmp_filter)
    receipts2 = Receipt.by_restaurant_ids([restaurant.id]).where("user_id in(?)", ar2).where("receipts.status = 3").where(tmp_filter)
    receipts3 = Receipt.by_restaurant_ids([restaurant.id]).where("user_id in(?)", ar3).where("receipts.status = 3").where(tmp_filter)

    ## avg 1
    count1 = 0
    total1 = 0
    avg1 = 0
    receipts1.each_with_index do |r, index|
      total1 += r.last_transaction.subtotal.to_f
      count1 = count1 + 1
    end
    if total1 > 0 and count1 > 0
      avg1 = total1 / count1
    else
      avg1 = total1
    end

    ## avg 2
    count2 = 0
    total2 = 0
    avg2 = 0
    receipts2.each_with_index do |r, index|
      total2 += r.last_transaction.subtotal.to_f
      count2 = count2 + 1
    end
    if total2 > 0 and count2 > 0
      avg2 = total2 / count2
    else
      avg2 = total2
    end

    ## avg 3
    count3 = 0
    total3 = 0
    avg3 = 0
    receipts3.each_with_index do |r, index|
      total3 += r.last_transaction.subtotal.to_f
      count3 = count3 + 1
    end

    #debugger
    if total3 > 0 and count3 > 0
      avg3 = total3 / count3
    else
      avg3 = total3
    end

    return avg1.round(2),avg2.round(2),avg3.round(2)
  end

  def generate_chart_question
    filter = sort_by(params)
    @chart_data_avg = []
    @chart_question1 = []
    @chart_question2 = []
    @chart_question3 = []
    @chart_question4 = []

    @survey = Survey.find params[:id]
    restaurant_id_filter = params[:check_values] || nil
    @restaurants.each_with_index do |restaurant|
      if @restaurants.map(&:id).include?(restaurant_id_filter) or restaurant_id_filter.blank?
        display_text = restaurant.dashboard_display_text
        result = restaurant.survey_avg_chart(filter,@survey)
        @chart_data_avg << {:name => display_text , :data => result[:all]}
        @chart_question1 << {:name => display_text , :data => result[:q1]}
        @chart_question2 << {:name => display_text , :data => result[:q2]}
        @chart_question3 << {:name => display_text , :data => result[:q3]}
        @chart_question4 << {:name => display_text , :data => result[:q4]}
      end
    end
  end

  def find_survey
    @survey = Survey.find(params[:id],:include => :questions)
    session[:survey_id] = params[:id]
    session[:survey_id] = "&id=#{params[:id]}"
  end

  def left_filter
    unless params[:check_values].blank?
      session[:left_filter] = "&check_values="+params[:check_values].to_s  if params[:check_values] and params[:check_values].class == String
    else      
      session[:left_filter] = ""
    end
  end
end