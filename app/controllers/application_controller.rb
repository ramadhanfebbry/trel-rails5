class ApplicationController < ActionController::Base
  include ApplicationHelper
  
  #before_action :authenticate_admin!
  # before_action :locale
  #rescue_from Rack::Timeout::RequestTimeoutError, :backtrace => true do |e|
  #  TimeoutTrace.create(
  #      :notice_error => e,
  #      :referer => request.referer,
  #      :request_params => request.params,
  #      :rack_response => "{
  #                          :type => 'timeout_error,'
  #                          :message => 'The request timed out.'
  #                      }.to_json, 503"
  #  )
  #  NewRelic::Agent.instance.error_collector.notice_error e,
  #      :uri => request.path,
  #      :referer => request.referer,
  #      :request_params => request.params
  #      #rack_response({
  #      #              :type => "timeout_error",
  #      #              :message => "The request timed out."
  #      #          }.to_json, 503)
  #     render :json => {:status => 404, :alert => "Timeout Error"}
  #end


  #rescue_from Rack::Timeout::RequestTimeoutError, :backtrace => true do |e|
  #
  #  NewRelic::Agent.instance.error_collector.notice_error e,
  #                                                        uri: request.path,
  #      referer: request.referer,
  #      request_params: request.params
  #
  #
  #  TimeoutTrace.create(
  #      :notice_error => e,
  #      :referer => request.referer,
  #      :request_params => request.params,
  #      :rack_response => "{
  #                    :type => 'timeout_error,'
  #                    :message => 'The request timed out.'
  #                }.to_json, 503"
  #  )
  #
  #  render :json => { :status => 404 , :alert => "Request Too long"}
  #end

  def locale
    cookies[:locale] ||= "en"
    locale ? params[:locale] : cookies[:locale]
  	I18n.locale = locale || I18n.default_locale
    cookies[:locale] ||= locale
  end

  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = "Access denied."
    redirect_to root_url
  end

  def set_appkey
    session[:client_id] = params[:client_id]
  end

  def check_is_logged_out_oauth
    p "check_is_logged_out_oauth"
    p  session[:state]
    p params[:state]
    p "------aaaa"
    if !session[:state].blank? && !session[:state].eql?(params[:state])
      session[:state] = nil
      sign_out(current_user) if current_user
    end
  end

  def authenticate_oauth_user!(params)
    p "authenticate_oauth_user!"*10
    p params
    User.where(["access_grants.access_token = ? AND (access_grants.access_token_expires_at IS NULL OR access_grants.access_token_expires_at > ?)", params[:oauth_token], Time.now]).joins(:access_grants).select("users.*").first
  end

  def get_olo_chain
    unless session[:client_id].blank?
      application_key = ApplicationKey.where(:appkey => session[:client_id]).first
      @application = application_key.application
      @chain = @application.chain
    end
    p "get_olo_chain! ======================================="
    if @chain.present?
      p "chain name:        = #{@chain.try(:name)}"
    else
      p "Chain is blank..."
      render :json => { :status => 404 , :message => "Chain is blank..."}
    end
  end

  def authenticate_olo_user!
    p "authenticate_olo_user! ======================================="
    p "current_user        = #{current_user.email}" if current_user
    if current_user.chain_id == 1
      redirect_to "https://nekter.ololitesandbox.com/"
    elsif current_user.chain_id == 36
      redirect_to "http://lepainquotidien.olosandbox.com/"
    end if current_user
  end
end
