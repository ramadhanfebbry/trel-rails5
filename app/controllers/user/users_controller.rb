class User::UsersController < ApplicationController

  before_action :get_olo_chain
  layout :set_layout
  include Api::ApplicationsHelper
  before_action :authenticate_user!

  def index
    @current_user = current_user
    render :layout => "application"
  end

  def edit
    @current_user = current_user
  end

  def update
    @current_user = current_user
    params[:user][:olo_registration] = true
    if @current_user.update_attributes(params[:user])
      if @chain
        flash[:original_referer] = "signin"
        unless session[:state].blank?
          access_grant = current_user.access_grants.create({:client_id => session[:client_id], :state => session[:state]}, :without_protection => true)
          redirect_to access_grant.redirect_uri_for(session[:redirect_uri])
        else
          sign_in_and_redirect(@current_user)
        end
      else
        render "edit"
      end
    else
      render "edit"
    end
  end

  def show
    @current_user = current_user
  end

  private

  def set_layout
    return "api_sign/lpq" if session[:client_id].blank?
    if @chain.id == 1
      "api_sign/nekter"
    else
      "api_sign/lpq"
    end
  end

end
