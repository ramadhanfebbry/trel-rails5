class User::SessionsController < Devise::SessionsController

  before_action :get_olo_chain
  prepend_before_action :authenticate_olo_user!, only: [:new]
  layout :set_layout
  include Api::ApplicationsHelper

  def create
    if @application && @chain
      device_type = @application.device_type.to_i - 1
      type = DEVICE_TYPE_SELECT.keys.map{|a| a.parameterize("_")}[device_type] rescue '-'
      p '=' * 55
      p params
      p "device_type = #{device_type}"
      p "type = #{type}"
      p "------------------------"
      params[:user][:email] = CGI::escape(params[:user][:email])
      params[:user][:email] = URI.decode(params[:user][:email])

      params[:user] = {
          :email => params[:user][:email].downcase,
          :password => params[:user][:password],
          :remember_me => params[:user][:remember_me],
          :chain_id => @chain.id,
          :register_type => 1,
          :sign_in_device_type => type
      }
    end

    resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#new")
    flash[:original_referer] = "signin"
    set_flash_message(:notice, :signed_in) if is_navigational_format?
    sign_in(resource_name, resource)
    if resource.first_name.blank? || resource.last_name.blank?
      redirect_to edit_user_user_path(resource)
    else
      respond_with resource, :location => after_sign_in_path_for(resource)
    end
    #super
  end

  private

  def set_layout
    return "api_sign/lpq" if session[:client_id].blank?
    if @chain.id == 1
      "api_sign/nekter"
    else
      "api_sign/lpq"
    end
  end
end
