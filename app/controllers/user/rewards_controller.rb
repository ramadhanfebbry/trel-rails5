class User::RewardsController < User::BaseController
  before_action :authenticate_user!

  def index
    @current_user = current_user
    @activeRewards = Reward.of_user(current_user.id)

    @privateRewards = Array.new
    @publicRewards = Array.new

    @activeRewards.each do |reward|
      if reward.reward_type == Reward::TYPES["REGULAR"]
        @publicRewards << reward
      else
        @privateRewards << reward
      end
    end

  end

end
