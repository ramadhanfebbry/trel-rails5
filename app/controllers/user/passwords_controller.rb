class User::PasswordsController < Devise::PasswordsController

  before_action :get_olo_chain
  prepend_before_action :authenticate_olo_user!, only: [:new, :edit]
  layout :set_layout
  include Api::ApplicationsHelper

  def create
    user = User.find_by_email_and_chain_id(resource_params[:email], @chain.id)
    if user
      pass = user.random_password
      user.locale_id = @chain.try(:locales).try(:first).try(:id) || Locale.find_by_key('en').try(:id) if user.locale_id.blank?
      user.update_attributes(:password => pass, :forgot_password => true)
      #flash.now[:notice] = t(:password_emailed)
      redirect_to user_session_path, notice: t(:password_emailed)
    else
      flash.now[:notice] = t(:forgot_password_user_does_not_exist)
      render "new"
    end
  end

  private

  def set_layout
    return "api_sign/lpq" if session[:client_id].blank?
    if @chain.id == 1
      "api_sign/nekter"
    else
      "api_sign/lpq"
    end
  end
end
