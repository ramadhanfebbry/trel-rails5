class User::RegistrationsController < Devise::RegistrationsController

  before_action :get_olo_chain
  prepend_before_action :authenticate_olo_user!, only: [:new]
  layout :set_layout
  include Api::ApplicationsHelper

  def new
    super
  end

  def create
    if @application && @chain
      device_type = @application.device_type.to_i - 1
      type = DEVICE_TYPE_SELECT.keys.map{|a| a.parameterize("_")}[device_type] rescue '-'
      p 'olo ---- ' * 55
      p "chain name  = #{@chain.name}"

      params[:user][:chain_id] = @chain.id
      params[:user][:register_type] = 1
      params[:user][:sign_in_device_type] = type
      params[:user][:olo_registration] = true
      params[:user][:locale_id] = @chain.locales.first.try(:id) || Locale.find_by_key('en').try(:id)
    end
    flash[:original_referer] = "signup"
    super
  end

  def update
    super
  end

  private

  def set_layout
    return "api_sign/lpq" if session[:client_id].blank?
    if @chain.id == 1
      "api_sign/nekter"
    else
      "api_sign/lpq"
    end
  end
end


