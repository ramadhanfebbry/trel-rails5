class User::ReceiptsController < User::BaseController

  before_action :authenticate_user!

  # GET /receipts
  def index
    @receipts = current_user.receipts.includes(:chain).order("id desc")
  end

  # GET /receipts/1
  def show
    @receipt = current_user.receipts.includes(:chain).find(params[:id])
    @receipt_transaction = @receipt.last_transaction
  end

  # GET /receipts/new
  def new
    @receipt = Receipt.new
  end

  # POST /receipts
  def create
    @receipt = Receipt.new(params[:receipt])
    @receipt.user = current_user
    @receipt.image_url = @receipt.user.hash.to_s
    @receipt.thumbnail_url = 'tn' + @receipt.image_url + '.jpg'
    @receipt.status = ReceiptStatus::RECEIVED

    respond_to do |format|
      if @receipt.save
        format.html { redirect_to user_receipt_url(@receipt), notice: 'Receipt was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /receipts/1
  def update
    @receipt = current_user.receipts.find(params[:id])

    respond_to do |format|
      if @receipt.update_attributes(params[:receipt])
        format.html { redirect_to user_receipt_url(@receipt), notice: 'Receipt was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /receipts/1
  def destroy
    @receipt = current_user.receipts.find(params[:id])
    @receipt.destroy

    respond_to do |format|
      format.html { redirect_to user_receipts_url }
    end
  end
end
