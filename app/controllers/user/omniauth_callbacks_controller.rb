class User::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  before_action :get_olo_chain
  layout 'user'
  include Api::ApplicationsHelper

  def facebook
    facebook_auth
  end

  def facebook_nekter
    facebook_auth
  end

  def facebook_aroma
    facebook_auth
  end

  def facebook_lpq
    facebook_auth
  end

  private

  def facebook_auth
    if @application && @chain
      device_type = @application.device_type.to_i - 1
      type = DEVICE_TYPE_SELECT.keys.map{|a| a.parameterize("_")}[device_type] rescue '-'
    end

    auth_hash = request.env['omniauth.auth']
    data = auth_hash.extra.raw_info
    provider = auth_hash["provider"].capitalize

    p "facebook ======================================================== "
    p data
    @user = User.find_by_email_and_chain_id_and_register_type(data.email, @chain.id, 2)
    if @user.present?
      web_and_mobile_signup_status = User.web_and_mobile_signup_check(@application, @user, type)
      unless web_and_mobile_signup_status.eql?("CONT")
        render :json => web_and_mobile_signup_status
        warden.logout
        return
      end
      @user.update_attributes(:first_name=>data.first_name, :last_name=>data.last_name, :email => data.email,
                              :facebookID => data.id, :register_type => 2, :sign_in_device_type => type)
      if @user.locale_id.blank?
        locale_id = @chain.locales.first.try(:id) || Locale.find_by_key('en').try(:id)
        @user.update_column("locale_id", locale_id)
      end
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => provider
      #sign_in_and_redirect @user, :event => :authentication
    else
      @user = User.new(:first_name=>data.first_name, :last_name=>data.last_name, :email => data.email,
                       :facebookID => data.id, :register_type => 2, :sign_in_device_type => type)
      @user.chain_id = @chain.id
      @user.locale_id = @chain.locales.first.try(:id) || Locale.find_by_key('en').try(:id)
      @user.save
      flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => provider
    end
    sign_in_and_redirect @user
  end


end
