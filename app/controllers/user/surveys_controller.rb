class User::SurveysController < ApplicationController
  #before_action :authenticate_user!

  def index
    @current_user = User.where('email = ?', 'u@dg.com').first
    @chain = @current_user.chain
    #@surveys = Survey.where('chain_id = ? and status = ?', @chain.id, Survey::STATUS["Active"])
    @surveys = Survey.where('chain_id = ?', @chain.id)
  end

  def show
    @survey = Survey.find(params[:id])
  end

  def answer
  end

end
