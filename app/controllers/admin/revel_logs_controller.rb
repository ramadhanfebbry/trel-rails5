class Admin::RevelLogsController < Admin::BaseController
  def index
    @revel_logs = RevelLog.order("created_at DESC").paginate(:per_page => 20, :page => params[:page])
  end

  def success_users
    @revel_log = RevelLog.find params[:id]
    @users = @revel_log.success_users.paginate(:per_page => 30, :page => params[:page]) rescue []
  end

  def failed_users
    @revel_log = RevelLog.find params[:id]
    @users = @revel_log.failed_users.paginate(:per_page => 30, :page => params[:page]) rescue []
  end

  def filter_log
    conditions = []
    conditions << "chain_id = #{params[:chain_id]}" unless params[:chain_id].blank?
    conditions << "status = '#{params[:status]}'" unless params[:status].blank?
    conditions = conditions.join(" AND ")
    @revel_logs = RevelLog.where(conditions).order("created_at DESC").paginate(:per_page => 20, :page => params[:page])
    render :template => "admin/revel_logs/index.html"
  end

  def manual_sync
  end

  def do_manual_sync
    return redirect_to "/admin/revel_logs/manual_sync", :alert => "Please select chain" if params[:chain_id].blank?
    chain = Chain.find(params[:chain_id])

    uploaded_io = params[:file] rescue ""
    if uploaded_io.blank?
      return redirect_to "/admin/revel_logs/manual_sync", :alert => "Pls upload the xls/csv again"
    else
      revel_log = RevelLog.where(chain_id: chain.id, status: "completed", attachment_file_name: uploaded_io.original_filename).first rescue nil
      return redirect_to "/admin/revel_logs/manual_sync", :alert => "Duplicate file name, please make sure upload correct file." if revel_log.present?

      next_path = Rails.root.join('tmp', uploaded_io.original_filename)
      File.open(next_path, 'wb') do |file|
        file.write(uploaded_io.read)
      end

      if File.exist?(next_path)
        aw,bucket = Reward.initialize_aws
        aw.buckets[bucket].objects["#{chain.id}_#{uploaded_io.original_filename.split('.').first}_temporary"].write(:file => next_path.to_s)

        rl = RevelLog.create(:chain_id => chain.id, :log_type => "revel to relevant manual sync",
                             :attachment_file_name => uploaded_io.original_filename, :status => "in queue")
        Delayed::Job.enqueue(DailySyncRevelToRelevantJob.new(chain, uploaded_io.original_filename, rl, "manual_sync"), :delayable_type => "Sync revel")

        return redirect_to "/admin/revel_logs", :notice => "Processing File"
      else
        return redirect_to "/admin/revel_logs/manual_sync", :notice => "Pls upload the xls/csv again"
      end
    end
  end
end
