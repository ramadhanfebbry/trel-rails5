class Admin::FishbowlLogsController < Admin::BaseController
  def index
    @fishbowl_logs = FishbowlLog.order("created_at DESC").paginate(:per_page => 20, :page => params[:page])
  end

  def success_users
    @fishbowl_logs = FishbowlLog.find params[:id]
    @users = @fishbowl_logs.success_users.paginate(:per_page => 30, :page => params[:page])
  end

  def failed_users
    @fishbowl_logs = FishbowlLog.find params[:id]
    @users = @fishbowl_logs.failed_users.paginate(:per_page => 30, :page => params[:page])
  end

  def filter_log
    conditions = []
    conditions << "chain_id = #{params[:chain_id]}" unless params[:chain_id].blank?
    conditions << "status = '#{params[:status]}'" unless params[:status].blank?
    conditions = conditions.join(" AND ")
    @fishbowl_logs = FishbowlLog.where(conditions).order("created_at DESC").paginate(:per_page => 20, :page => params[:page])
    render :template => "admin/fishbowl_logs/index.html"
  end
end
