class Admin::HomeController <  Admin::BaseController
  skip_before_action :authenticate_admin! , :only => [:index]
  before_action :check_request, :only => [:index]

  def index

  end

  def test_api

  end

  def test_vext
    
  end

  def test_nuorder
  end

  def test_onosys
  end

  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : Restaurant.where("chain_id IN (?)", params[:chains]).order("name asc") rescue []
  end

  def load_offers
    unless params[:online_order].blank?
      @offers = params[:chains].blank? ? [] : Offer.unscoped.where("chain_id IN (?) AND is_online_order = ? ",params[:chains], true).order("name asc") rescue []
    else
      @offers = params[:chains].blank? ? [] : Offer.where("chain_id IN (?)", params[:chains]).order("name asc") rescue []
    end
  end

  def load_rewards
    @rewards = params[:chains].blank? ? [] : Reward.where("chain_id IN (?)", params[:chains]).order("name asc") rescue []
  end

  private

  def check_request
    if request.format.html?
      authenticate_admin!
    else
      render :nothing => true
    end
  end
end
