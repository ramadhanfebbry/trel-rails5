class Admin::Onosys::OnlineOrdersController < Admin::BaseController

  def index
    @online_orders = [] #::Onosys::OnlineOrder.order("id DESC").paginate(:page => params[:page], per_page: Setting.pagination.per_page)
  end

  def show
    @online_order = ::Onosys::OnlineOrder.find params[:id]
    @online_order_detail = ::Onosys::OnlineOrderDetail.where(:onosys_online_order_id => @online_order.id).first
    respond_to do |format|
      format.html
      format.js
    end
  end

  def search
    q = params[:search][:q]
    q = q.gsub("'","''")
    params[:search][:q] = q

    ar_field = [
        "onosys_online_orders.id",
        "restaurants.name",
        "users.email",
        "onosys_online_orders.loyalty_status",
        "onosys_online_orders.created_at"
    ]

    order = params[:qry_order]
    online_orders = ::Onosys::OnlineOrder.select("onosys_online_orders.id, onosys_online_orders.online_order_identifier, onosys_online_orders.platform, onosys_online_orders.created_at, onosys_online_orders.loyalty_status, onosys_online_orders.user_id, onosys_online_orders.restaurant_id, onosys_online_orders.chain_id")
    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      p "qry_order : #{ qry_order}"
      @online_orders = online_orders.custom_search(params[:search]).order(qry_order).reverse_order
      qry_order = qry_order + " asc"
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @online_orders = online_orders.custom_search(params[:search]).order(qry_order)
    else
      qry_order = 'ncr_online_orders.id desc'
      @online_orders = online_orders.custom_search(params[:search]).order(qry_order)
    end
    @online_orders = @online_orders.paginate(page: params[:page] , per_page: Setting.pagination.per_page)
    @action = '-search'

    respond_to do |format|
      format.js { render :index }
    end
  end

end
