class Admin::RewardPosCodesController < Admin::BaseController

  before_action :find_reward

  def index
    conditions = []
    conditions << "users.email LIKE '%#{params[:search][:email]}%'" if params[:search] && !params[:search][:email].blank?
    conditions << "code LIKE '%#{params[:search][:code]}%'" if params[:search] && params[:search][:code]
    conditions = conditions.join(" AND ")
    @codes = @reward.reward_pos_codes.joins("LEFT JOIN users ON reward_pos_codes.user_id = users.id")
    .where(conditions).paginate(:per_page => 50, :page => params[:page])
  end

  def new
    @code = @reward.reward_pos_codes.new
  end

  def create
    begin
      csv_text = File.read("#{params[:reward_pos_code][:csv].path}")
      csv = CSV.parse(csv_text, :headers => false)
      Delayed::Job.enqueue(PosRewardCodeJob.new(@reward.id, csv))
      redirect_to admin_reward_reward_pos_codes_path(@reward), :notice => "Your CSV has successfully uploaded. Please wait in a few mins to see the result."
    rescue => e
      redirect_to new_admin_reward_reward_pos_code_path(@reward), :alert => "Please upload your CSV file correctly"
    end

  end

  protected

  def find_reward
    @reward = Reward.find(params[:reward_id])
  end

end