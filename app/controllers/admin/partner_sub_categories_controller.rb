class Admin::PartnerSubCategoriesController < Admin::BaseController

  def index
    if params["search"] && params["search"]["chain_id"].present?
      @chain = Chain.find( params["search"]["chain_id"])
      @categories = @chain.partner_categories.select("id,name").order("name ASC")
      @category_id = params["search"]["category_id"] rescue nil
      p "@category_id #{@category_id}"
      if @category_id
        @partner_sub_categories = @chain.partner_sub_categories.where("partner_category_id = #{@category_id}")
        .order("chain_id").paginate(:page => params[:page], :per_page => 10)
      else
        @partner_sub_categories = @chain.partner_sub_categories.order("chain_id").paginate(:page => params[:page], :per_page => 10)
      end
    else
      @partner_sub_categories = PartnerSubCategory.order("chain_id").paginate(:page => params[:page], :per_page => 10)
      @chain = nil
    end
  end

  def new
    @partner_sub_category = PartnerSubCategory.new
  end

  def create
    @partner_sub_category = PartnerSubCategory.new(params[:partner_sub_category])
    render :new, :alert => "Restaurant Can't be blank!" if params[:restaurants].blank?
    if @partner_sub_category.save
      @chain = @partner_sub_category.chain
      Delayed::Job.enqueue(PartnerRewardParticipatingJob.new(@chain, @partner_sub_category, "participate", false, params[:restaurants])) if params[:restaurants].present?
      redirect_to admin_partner_sub_categories_path, :notice => "Partner Category successfully created."
    else
      render :new, :alert => "Partner Category failed."
    end
  end

  def edit
    @partner_sub_category = PartnerSubCategory.find(params[:id])
    @chain = @partner_sub_category.chain
    @categories = @chain.partner_categories
    @participants = @partner_sub_category.restaurants.select("restaurants.id,name,address").order("name ASC")
    ids = @participants.collect(&:id)
    @non_participants = Restaurant.where('chain_id = ? AND NOT id in (?)', @partner_sub_category.chain.id, ids.empty? ? '-111' : ids).order("name ASC")
    respond_to do |format|
      format.js
      format.html
    end
  end

  def update
    @partner_sub_category = PartnerSubCategory.find params[:id]
    if @partner_sub_category.update_attributes(params[:partner_sub_category])
      redirect_to admin_partner_sub_categories_path, :notice => "Partner Category successfully updated."
    else
      render :edit, :alert => "Partner Category failed."
    end
  end

  def show
    @partner_sub_category = PartnerSubCategory.find params[:id]
    @participants = @partner_sub_category.restaurants.select("restaurants.id,name,address").order("name ASC")
    ids = @participants.collect(&:id)
    @non_participants = Restaurant.where('chain_id = ? AND NOT id in (?)', @partner_sub_category.chain.id, ids.empty? ? '-111' : ids).order("name ASC")
  end

  def destroy
    @partner_sub_category = PartnerSubCategory.find params[:id]
    @partner_sub_category.destroy
    redirect_to admin_partner_sub_categories_path, :notice => "Partner Category successfully destroyed."
  end

  def load_restaurants
    chain = Chain.find(params[:chain_id])
    @restaurants = chain.restaurants.select("id,name,address").order("name ASC")
    @categories = chain.partner_categories.select("id,name").order("name ASC")
    @page = params[:page]
  end

  def update_participation
    @partner_sub_category = PartnerSubCategory.find params[:id]
    @chain = @partner_sub_category.chain
    submit= params[:commit]

    case submit
      when "Pull Out"
        Delayed::Job.enqueue(PartnerRewardParticipatingJob.new(@chain, @partner_sub_category, "pullout", false, params[:pullout])) if params[:pullout].present?
      when "Participate"
        Delayed::Job.enqueue(PartnerRewardParticipatingJob.new(@chain, @partner_sub_category, "participate", false, params[:participate])) if params[:participate].present?
      else
    end
    redirect_to edit_admin_partner_sub_category_path(@partner_sub_category)
  end

  def load_menu_items
    @chain = Chain.find(params[:chain_id])
    @partner_sub_category = PartnerSubCategory.find params[:partner_sub_category]
    @restaurants = @partner_sub_category.restaurants.select("restaurants.id,name")
  end

  def search_menu_items
    @partner_sub_category = PartnerSubCategory.find params[:id]
    @restaurant_id = params[:restaurant_id] rescue nil
    @menu_items = PartnerMenuItemsRestaurant.where("restaurant_id = #{@restaurant_id} ").paginate(:page => params[:page], :per_page => 10) if @restaurant_id.present?
  end

  def add_item
    @partner_sub_category = PartnerSubCategory.find params[:id]
    partner_menu_item_id = params[:partner_menu_item_id]
    restaurant_id = params[:restaurant_id]
    psc = PartnerSubCategoryMenuItem.find_or_initialize_by_partner_sub_category_id_and_partner_menu_item_id_and_restaurant_id(@partner_sub_category.id, partner_menu_item_id, restaurant_id)
    psc.save
  end

  def remove_item
    @partner_sub_category = PartnerSubCategory.find params[:id]
    partner_menu_item_id = params[:partner_menu_item_id]
    restaurant_id = params[:restaurant_id]
    psc = PartnerSubCategoryMenuItem.find_or_initialize_by_partner_sub_category_id_and_partner_menu_item_id_and_restaurant_id(@partner_sub_category.id, partner_menu_item_id, restaurant_id)
    psc.delete
  end

end