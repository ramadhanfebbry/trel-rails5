class Admin::VantivGiftCardEmailSentLogsController < Admin::BaseController
  def index;end

  def resend_mail
    vantiv_gift_card_email_sent_log = VantivGiftCardEmailSentLog.find(params[:id])
    @giftee_email = vantiv_gift_card_email_sent_log.giftee_email
    chain = vantiv_gift_card_email_sent_log.chain
    user = vantiv_gift_card_email_sent_log.user
    giftcard_notification_email = chain.giftcard_notification_emails.where(:locale_id => user.locale_id).first
    if !giftcard_notification_email.blank?
      Delayed::Job.enqueue(GiftcardNotificationEmailJob.new(chain, giftcard_notification_email,
        vantiv_gift_card_email_sent_log.card_number,
        vantiv_gift_card_email_sent_log.giftcard_amount,
        vantiv_gift_card_email_sent_log.giftee_email,
        vantiv_gift_card_email_sent_log.giftee_name,
        vantiv_gift_card_email_sent_log.custom_message,
        vantiv_gift_card_email_sent_log.gifter_name,
        vantiv_gift_card_email_sent_log.image_header_url))
      @noftif = "Success! Your gift card has been sent to #{@giftee_email}"
    else
      @noftif = "Your gift card failed to sent! "
    end
  end

  def search
    query_sql = "vantiv_gift_card_email_sent_logs.chain_id = :chain_id"
    @query = {chain_id: params[:chain_id]}
    query_var = {chain_id: @query[:chain_id]}
    if !params[:gifter_email].blank?
      @query[:gifter_email] = params[:gifter_email]
      query_var[:giftee_email] = "#{@query[:gifter_email]}%"
      query_sql  = "users.email LIKE :giftee_email"
    end

    if !params[:from].blank? && !params[:to].blank?
      @query[:from] = params[:from]
      @query[:to] = params[:to]
      query_var[:from] = @query[:from].to_date.beginning_of_day
      query_var[:to] = @query[:to].to_date.end_of_day
      query_sql += " AND vantiv_gift_card_email_sent_logs.created_at BETWEEN :from AND :to"
    end

    @vantiv_gift_card_email_sent_logs = VantivGiftCardEmailSentLog.joins(:user).
    where(query_sql, query_var).
    paginate(page: params[:page], per_page: 10)
  end
end
