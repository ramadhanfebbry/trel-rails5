class Admin::PromoCodesController < Admin::BaseController 
  before_action :find_promo_code
  
  def destroy
    if @promo_code.delete
      flash[:notice] = "Promo code deleted"
      redirect_to :back
    end
  end

  private

  def find_promo_code
    @promo_code = PromoCode.find params[:id]
  end
end