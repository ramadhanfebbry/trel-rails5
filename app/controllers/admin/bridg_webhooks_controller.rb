class Admin::BridgWebhooksController < Admin::BaseController

  def index
    @bridg_webhooks = BridgWebhook.where(params[:chains]). order('updated_at desc').paginate(per_page: 10, page: params[:page])
  end

  def edit
    @bridg_webhook = BridgWebhook.find params[:id]
  end

  def update
    @bridg_webhook = BridgWebhook.find params[:id]
    @bridg_webhook.content["customer"]["email"]  = params["olo_webhook"]["email"]
    @bridg_webhook.content["totals"]["subTotal"] = params["olo_webhook"]["subtotal"]
    @bridg_webhook.content["orderId"]            = params["olo_webhook"]["order_id"]

    if @bridg_webhook.save
      redirect_to admin_webhooks_path
    end
  end

  def process_receipt_olo
    @bridg_webhook = BridgWebhook.find params[:id]
    @bridg_webhook.delay_receipt

    redirect_to admin_webhooks_path
  end
end
