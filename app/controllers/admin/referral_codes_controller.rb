class Admin::ReferralCodesController < Admin::BaseController
  before_action :find_ref,:find_chain
  # GET /admin/referral_programs
  def index    
    #@codes = ReferralUser.where("referral_program_id = ?",@referral_program.id).includes(:referral_code => :user).
    #    paginate(page: params[:page], per_page: 10)
   # @codes = ReferralCode.where("users.chain_id = ?",@chain.id).includes(:user, :referral_users).
   #             paginate(page: params[:page], per_page: 25)
    #@codes = ReferralCode.joins("LEFT JOIN REFERRAL_USERS ON REFERRAL_CODES.id = REFERRAL_USERS.code_id").
    #    joins("left join referral_programs on referral_programs.id = referral_users.referral_program_id and referral_programs.chain_id = #{@chain.id}
    #           and referral_programs.id = #{@referral_program.id}").
    #    select('referral_codes.*, count(referral_users.id) as ref_count').
    #    group('referral_codes.id').order("ref_count desc").
    #            paginate(page: params[:page], per_page: 25)

    #select * from referral_codes inner join users on users.id = referral_codes.user_id
    #left join referral_users on referral_codes.id = referral_users.code_id
    #where referral_codes.user_id in( select  users.id from users where chain_id = 7)

    @codes = ReferralCode.joins("inner join users on users.id = referral_codes.user_id").
        joins("left join referral_users on referral_codes.id = referral_users.code_id").
        where("referral_codes.user_id in( select  users.id from users where chain_id = ?)",@chain.id).
                paginate(page: params[:page], per_page: 25)
    @referer_reward = @referral_program.incentive_referer
    @referee_reward = @referral_program.incentive
  end

  def generate_codes    
    @referral_program.generate_referral_codes(@chain)
  end


  def search
    #@codes= ReferralCode.where("users.chain_id = ? and (users.email like '%#{params[:search]}%' or code like '%#{params[:search]}%')",
    #                   @chain.id).includes(:user).
    #    paginate(page: params[:page], per_page: 25)
    #sr = ""
    #user_ids = ReferralUser.where("referral_program_id = ? and (users.email like '%#{params[:search]}%')", @referral_program.id).includes(:user).select('referral_users.user_id')
    #unless user_ids.blank?
    #  sr = " OR referral_users.user_id in (#{user_ids.map(&:user_id).join(',')})"
    #end
    #@referee_reward = params['referee_reward']
    #@referer_reward = params['referer_reward']
    #@codes=  ReferralUser.where("referral_program_id = ? and (users.email like '%#{params[:search]}%' or code like '%#{params[:search]}%') #{sr}",@referral_program.id).
    #    includes(:referral_code => :user).
    #    paginate(page: params[:page], per_page: 10)

    # find users
    unless params['search'].blank?
    user_ids = User.where("chain_id = ? and email like ?", @chain.id,"%#{params[:search]}%").select('id')
 #   @codes = ReferralCode.joins("LEFT JOIN REFERRAL_USERS ON REFERRAL_CODES.id = REFERRAL_USERS.code_id").
 #       joins("left join referral_programs on referral_programs.id = referral_users.referral_program_id and referral_programs.chain_id = #{@chain.id}
 #and referral_programs.id = #{@referral_program.id}").
 #       select('referral_codes.*, count(referral_users.id) as ref_count').
 #       where("referral_users.user_id in(?) or referral_codes.user_id in(?)",user_ids,user_ids).
 #       group('referral_codes.id').order("ref_count desc").
    @codes = ReferralCode.joins("inner join users on users.id = referral_codes.user_id").
        joins("left join referral_users on referral_codes.id = referral_users.code_id").
        where("referral_codes.user_id in( select  users.id from users where chain_id = ?) AND (referral_codes.user_id in(?) or referral_users.user_id in(?))",@chain.id,user_ids,user_ids).
        paginate(page: params[:page], per_page: 25)

    else
    @codes = ReferralCode.joins("inner join users on users.id = referral_codes.user_id").
        joins("left join referral_users on referral_codes.id = referral_users.code_id").
        where("referral_codes.user_id in( select  users.id from users where chain_id = ?)",@chain.id).
        paginate(page: params[:page], per_page: 25)
    @referer_reward = @referral_program.incentive_referer
    @referee_reward = @referral_program.incentive
    end
  end

  private

  def find_ref
    @referral_program = ReferralProgram.find(params[:referral_program_id])
  end

  def find_chain
    @chain = @referral_program.chain
  end
end