class Admin::VantivGiftCardsController < Admin::BaseController

  def index
  end

  def search
    @chain = Chain.find(params["chain_id"])
    @vantiv_gift_cards = VantivGiftCard.where(chain_id: @chain.id)
  end

  def new
    @chain = Chain.find params['chain_id']
    @vantiv_gift_card = VantivGiftCard.new
  end

  def edit
    @vantiv_gift_card = VantivGiftCard.find params[:id]
    @chain = Chain.find @vantiv_gift_card.chain_id
    render :layout => false
  end

  def create
    @chain = Chain.find(params["chain_id"])
    test_file = params[:csv]
    unless test_file.blank?
      sheet1 = CSV.read(test_file.path, {:headers => true, :encoding => 'iso-8859-1:utf-8'})
      sheet1.each do |row|
        vantiv_gc = VantivGiftCard.where(chain_id: @chain.id, card_number: row[0].last(-2)).first rescue nil
        if vantiv_gc.present?
          @vantiv_gift_card = vantiv_gc
        else
          @vantiv_gift_card = VantivGiftCard.new
        end
        @vantiv_gift_card.card_number = row[0].last(-2)
        @vantiv_gift_card.expiration_month = row[1].split("/")[0]
        @vantiv_gift_card.expiration_year = row[1].split("/")[1]
        @vantiv_gift_card.cvv = row[3].last(6).first(3)
        @vantiv_gift_card.cvv2 = row[3].last(3)
        @vantiv_gift_card.security_code = row[6]
        @vantiv_gift_card.chain_id =  @chain.id
        @vantiv_gift_card.save!
      end
    else
      error = true
    end

    if error
      redirect_to admin_vantiv_gift_cards_path, alert: "Please Upload csv files !"
    else
      redirect_to admin_vantiv_gift_cards_path, notice: "Vantiv gift card processed."
    end
  end

  def update
    @vantiv_gift_card = VantivGiftCard.find params[:id]
    if @vantiv_gift_card.update_attributes(params[:vantiv_gift_card])
      @update = true
    else
      @update = false
    end
  end

  def destroy
    @vantiv_gift_card = VantivGiftCard.find params[:id]
    @vantiv_gift_card.destroy unless @vantiv_gift_card.blank?

    redirect_to admin_vantiv_gift_cards_path , :notice => "Vantiv Gift Card Deleted"
  end

  def unlink_data
    @chain = Chain.find(params[:chain_id])
    @chain.vantiv_gift_cards.update_all(:used => false)
    Vantiv::GiftCardProfile.joins("LEFT JOIN users ON users.id = gift_card_profiles.user_id").where("users.chain_id = #{@chain.id}").destroy_all
    redirect_to admin_vantiv_gift_cards_path , :notice => "Vantiv Gift Card Unlinked for #{@chain.name}"
  end

end
