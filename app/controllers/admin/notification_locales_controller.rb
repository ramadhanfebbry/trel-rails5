class Admin::NotificationLocalesController < Admin::BaseController

  def index
    rescue_from_cancan(:index, NotificationLocale)
    @notification_locales = NotificationLocale.paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end
  
end
