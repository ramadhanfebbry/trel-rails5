class Admin::HistoryNotificationsController < Admin::BaseController
  before_action :find_plain

  def index
    type = params['type'].blank? ? "success" : params['type']
    kind = params['kind'].blank? ? "device" : params['kind']
    if type != "pending"
        @histories = @plain.history_notifications.send(type).send(kind).
            paginate(page: params[:page], per_page: Setting.pagination.per_page)
    else
      @histories = HistoryNotification.pending(@plain).
          paginate(page: params[:page], per_page: Setting.pagination.per_page)
    end
  end

  private

  def find_plain
    @plain = PlainPushNotification.find params['plain_push_notification_id']
  end
end