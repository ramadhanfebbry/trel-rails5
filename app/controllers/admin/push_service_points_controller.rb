class Admin::PushServicePointsController < Admin::BaseController
 
  def index
    @push_service_points = PushServicePoint.select("id, chain_id, point, date_text_ttd, recipient_count, push_service_points.created_at, sent, push_service_points.status, admin_id").includes(:chain).order("created_at DESC").paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @push_service_points }
      format.js {
        render "index.js"
      }
    end
  end

  def sync
    @push_service_points = PushServicePoint.select("id, chain_id, point, date_text_ttd, recipient_count, push_service_points.created_at, sent, push_service_points.status, push_service_points.admin_id").includes(:chain).order("created_at DESC").paginate(:page => params[:page], :per_page => 10)

    respond_to do |format|
      format.html {render "index.html.erb"}
      format.json { render json: @push_service_points }
      format.js {
        render "index.js"
      }
    end
  end

  def show
    @push_service_point = PushServicePoint.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push_service_point }
    end
  end

  def new
    @push_service_point = PushServicePoint.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @push_service_point }
    end
  end

  def edit
    @push_service_point = PushServicePoint.find(params[:id])
  end

  def create
    begin
      @push_service_point = PushServicePoint.new
      user_ids = []
      @push_service_point.errors.add(:point, "Point can't be blank") if params["push_service_point"]["point"].blank?
      chain = Chain.find params["push_service_point"]["chain_id"] rescue nil
      @push_service_point.errors.add(chain, "Chain can't be blank") if chain.blank?

      case params["select_type"]
        when "manual_enter"
          begin
            @push_service_point.temp_receiver_user_ids = params["push_service_point"]["temp_receiver_user_ids"]
            user_ids = params["push_service_point"]["temp_receiver_user_ids"]
            user_ids = user_ids.squish.gsub("\\r", '').gsub('\\n', '').gsub(' ', '').split(',')
            @push_service_point.errors.add(:recepient, "Recepient can't be blank") if user_ids.blank?
          rescue => e
            @push_service_point.errors.add(:recepient, e.message)
          end
        when "csv"
          test_file = params[:xls]
          unless test_file.blank?
            csv = CSV.read(test_file.path, {:headers => false, :encoding => 'iso-8859-1:utf-8'})
            csv.each do |row|
              user_ids << row.first.to_i
            end
          else
            @push_service_point.errors.add(:xls, "Please Upload csv files !")
          end
        when "all_user"
          if @push_service_point.errors.blank?
            user_ids = chain.users.select("id").where("device_token IS NOT NULL").pluck("id")
          end
      end

      date_text = %w(1 2 3).map { |e| params["push_service_point"]["sent_at(#{e}i)"]}.map{|x| x.rjust(2, '0')}.join("-")
      time_text = %w(4 5).map { |e| params["push_service_point"]["sent_at(#{e}i)"] }.map{|x| x.rjust(2, '0')}.join(":")
      time_zone = params["push_service_point"]['time_zone_default'].to_s
      full_date_time_text = "#{date_text} #{time_text} #{time_zone}"
      time_now_in_utc = full_date_time_text.to_time
      @push_service_point.select_type = params["select_type"]
      @push_service_point.point = params["push_service_point"]["point"]
      @push_service_point.chain_id = chain.id
      @push_service_point.date_text_ttd = full_date_time_text
      @push_service_point.sent_at = time_now_in_utc
      @push_service_point.admin_id = current_admin.id
      @push_service_point.recipient_count = user_ids.size
      @push_service_point.receiver_user_ids = "{#{user_ids.join(',')}}"
      respond_to do |format|
        if @push_service_point.errors.blank? && @push_service_point.save
          format.html { redirect_to admin_push_service_points_url, notice: 'Plain push service point was successfully created.' }
          format.json { render json: @push_service_point, status: :created, location: @push_service_point }
        else
          format.html { render action: "new" }
          format.json { render json: @push_service_point.errors, status: :unprocessable_entity }
        end
      end
    rescue => e
      @push_service_point.errors.add(:exeception, e.message)
      render action: "new"
    end
  end

  def update
    @push_service_point = PushServicePoint.find(params[:id])

    respond_to do |format|
      if @push_service_point.update_attributes(params[:push_service_point])
        format.html { redirect_to @push_service_point, notice: 'Push service point was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @push_service_point.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @push_service_point = PushServicePoint.find(params[:id])
    @push_service_point.destroy

    respond_to do |format|
      format.html { redirect_to push_service_points_url }
      format.json { head :no_content }
    end
  end

  def progress
    @psn = PushServicePoint.find(params[:id])
    @androids = PsnPushNotificationJob.select("id").where(:job_summary_id => @psn.id, :provider => 2)
    @ios = PsnPushNotificationJob.find_by_sql("SELECT recipient_count, success_count, failure_count from job_summary where job_id = #{@psn.id} AND provider = 1")
  end

  def cancel
    @psn = PushServicePoint.find(params[:id])
    if !@psn.finished?
      @psn.cancel
      redirect_to admin_push_service_points_path, :notice => "Job canceled successfully."
    elsif !@psn.parsed?
      @psn.update_attributes(:sent_at => Time.zone.now - 10.minutes, :status => 3)
      redirect_to admin_push_service_points_path, :notice => "Job canceled successfully."
    else
      redirect_to admin_push_service_points_path, :notice => "Job canceled failed."
    end

  end

  def pause
    @psn = PushServicePoint.find(params[:id])
    if !@psn.finished?
      @psn.pause
      redirect_to admin_push_service_points_path, :notice => "Job paused successfully."
    else
      redirect_to admin_push_service_points_path, :notice => "Job paused failed."
    end

  end

  def resume
    @psn = PushServicePoint.find(params[:id])
    if @psn.paused?
      @psn.resume
      redirect_to admin_push_service_points_path, :notice => "Job resumed successfully."
    else
      redirect_to admin_push_service_points_path, :notice => "Job resumed failed."
    end
  end

end
