class Admin::PosLocationsController < Admin::BaseController

  def index
    conditions = []
    conditions  << "pos_locations.chain_id IN (#{params[:chains].join(',')})" unless params[:chains].blank?
    conditions  << "(LOWER(apikey) LIKE '%#{(params[:q] || "").downcase}%' OR LOWER(restaurants.name) LIKE '%#{(params[:q] || "").downcase}%')"
    @pos_locations = PosLocation.includes(:restaurant).where(conditions.join(" AND ")).paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @pos_location = PosLocation.new(:chain => Chain.first)
    @pos_location.set_apikey
  end

  def create
    @pos_location = PosLocation.new(params[:pos_location])
    if @pos_location.save
      redirect_to admin_pos_locations_path, :notice => "POS Location successfully created."
    else
      render :new
    end
  end

  def edit
    @pos_location = PosLocation.find params[:id]
  end

  def update
    @pos_location = PosLocation.find params[:id]
    if @pos_location.update_attributes(params[:pos_location])
      redirect_to admin_pos_locations_path, :notice => "POS Location successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @pos_location = PosLocation.find params[:id]
    @pos_location.destroy
    redirect_to admin_pos_locations_path, :notice => "POS Location successfully destroyed."
  end

  def chain_restaurants
    chain = Chain.find params[:id]
    restaurants = chain.restaurants.order("app_display_text asc") rescue []
    render :json => restaurants.map { |x| {:id => x.id, :name => x.app_display_text} }
  end

  def new_group_of_pos

  end

  def create_group
    redirect_to new_group_of_pos_admin_pos_locations_path, :alert => "Please pick at least one restaurant" and return if params[:restaurants].blank?
    Delayed::Job.enqueue(PosLocationCreateGroupJob.new(params[:restaurants], params[:menu_upload_status], params[:check_status]))
    redirect_to admin_pos_locations_path, :notice => "Group POS Locations successfully posted, it will be processed soon."
  end


  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : Restaurant.where("chain_id IN (?)", params[:chains]).order('name asc') rescue []
  end


end