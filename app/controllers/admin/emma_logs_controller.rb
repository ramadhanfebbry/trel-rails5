class Admin::EmmaLogsController < Admin::BaseController
  def index
    @emma_logs = EmmaLog.order("created_at DESC").paginate(:per_page => 20, :page => params[:page])
  end

  def success_users
    @emma_logs = EmmaLog.find params[:id]
    @users = @emma_logs.success_users.paginate(:per_page => 30, :page => params[:page])
  end

  def failed_users
    @emma_logs = EmmaLog.find params[:id]
    @users = @emma_logs.failed_users.paginate(:per_page => 30, :page => params[:page])
  end

  def filter_log
    conditions = []
    conditions << "chain_id = #{params[:chain_id]}" unless params[:chain_id].blank?
    conditions << "status = '#{params[:status]}'" unless params[:status].blank?
    conditions = conditions.join(" AND ")
    @emma_logs = EmmaLog.where(conditions).order("created_at DESC").paginate(:per_page => 20, :page => params[:page])
    render :template => "admin/emma_logs/index.html"
  end
end
