class Admin::RemovePointsController < Admin::BaseController
  # GET /admin/remove_points
  # GET /admin/remove_points.json
  def index
    @remove_points = RemovePoint.order("created_at desc").paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @remove_points }
    end
  end

  # GET /admin/remove_points/1
  # GET /admin/remove_points/1.json
  def show
    @remove_point = RemovePoint.find(params[:id], :include => :user_remove_points)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @remove_point }
    end
  end

  # GET /admin/remove_points/new
  # GET /admin/remove_points/new.json
  def new
    @remove_point = RemovePoint.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @remove_point }
    end
  end

  # GET /admin/remove_points/1/edit
  def edit
    @remove_point = RemovePoint.find(params[:id])
  end

  # POST /admin/remove_points
  # POST /admin/remove_points.json
  def create
    @remove_point = RemovePoint.new(params[:remove_point])
    @remove_point.pusher_id = current_admin.id
    user_ids = []
    user_ids = params[:remove_point][:user_temp_ids]
    user_ids = user_ids.squish.gsub("\\r",'').gsub('\\n','').gsub(' ','').split(',')
    @remove_point.user_ids = {:user_ids => user_ids}

    ## custom validations
    count_user_ids = User.where("id in (?)", user_ids).count
    loop = (count_user_ids.to_f / 100.to_f).ceil
    tmp_user_ids = []
    success = true
    arr_str = []
    1.upto(loop) do |page|
      tmp_user_ids += User.where("id in (?) and points < ?", user_ids,params[:remove_point][:points]).select('id,points').paginate(page: page, per_page: 100)
      #tmp_user_ids.select!{|x| x < params[:remove_point][:points].to_i}

      unless tmp_user_ids.blank?
        puts tmp_user_ids
        tmp_user_ids = tmp_user_ids.each{|x| arr_str << x.id }
        arr_str = arr_str.join(',')
        success = false
        break
      end
    end

    return redirect_to new_admin_remove_point_path, :alert => "the value entered by admin is greater than users account points,  Please check id :  #{arr_str}" if success == false

    if @remove_point.valid?
      @remove_point.save
      @remove_point.send_remove_point_job
      redirect_to admin_remove_points_path, notice: 'Remove point was successfully created.'
    else
      render :new
    end
  end

  def summary_success
    @remove_point = RemovePoint.find params[:id]
    @logs = @remove_point.success_logs.select{|a| a[(params[:search_type] || "user_id" ).to_sym].to_s =~ /#{params[:key]}/}.paginate(:page => params[:page], :per_page => 30)
  end

  def summary_failed
    @remove_point = RemovePoint.find params[:id]
    @logs = @remove_point.error_logs.select{|a| a[(params[:search_type] || "user_id" ).to_sym].to_s =~ /#{params[:key]}/}.paginate(:page => params[:page], :per_page => 30)
  end


  # PUT /admin/remove_points/1
  # PUT /admin/remove_points/1.json
  def update
    @remove_point = RemovePoint.find(params[:id])

    data_array = []

    @user_failed = []
    @remove_point.attributes = params[:remove_point]

    params[:user_ids].uniq.each do |user_id|
      begin
        user = User.find(user_id.to_i)
        if user.chain_id.eql?(@remove_point.chain_id)
          data_array << {:email => user.email, :user_id => user.id}
        else
          @user_failed << {:user_id => user_id, :message => "User not included on chain that selected. User chain is #{user.chain.name}, but selected chain is  #{@remove_point.chain.name}"}
        end
      rescue => e
        @user_failed << {:user_id => user_id, :message => e.inspect.to_s}
      end
    end if !params[:user_ids].blank? and @remove_point.valid?

    @remove_point.user_remove_points.new(data_array)

    ## delete user list email and delayed jobs
    @remove_point.user_remove_points.delete_all
    @remove_point.delayed_jobs.delete_all

    @remove_point.user_remove_points.new(data_array)


    respond_to do |format|
      if @remove_point.save && @remove_point.send_remove_point_job
        @is_save = true
        format.html { redirect_to admin_remove_points_path, notice: 'Remove point was successfully updated.' }
        format.json { head :ok }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @remove_point.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /admin/remove_points/1
  # DELETE /admin/remove_points/1.json
  def destroy
    @remove_point = RemovePoint.find(params[:id])
    @remove_point.destroy

    respond_to do |format|
      format.html { redirect_to admin_remove_points_path }
      format.json { head :ok }
    end
  end
end
