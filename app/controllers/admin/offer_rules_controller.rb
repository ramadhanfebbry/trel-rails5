class Admin::OfferRulesController < Admin::BaseController

  # GET /offers
  def index
    rescue_from_cancan(:index, Offer)
    @offers = Offer.active_chain.paginate(page: params[:page], per_page: 10) #Offer.all

    respond_to do |format|
      format.html
      format.js
    end
  end

  def edit
    @offer = Offer.find params[:offer_id]
    @offer_rule = OfferRule.find params[:id]
  end

  def set_type
    @offer_rule = OfferRule.find params[:id]
    @general_item = GeneralMenuItem.find params[:menu_item_id]


    @offer = Offer.find params[:offer_id]

    OfferRuleMenuItem.find_or_create_by_offer_rule_id_and_general_menu_item_id_and_chain_id(@offer_rule.id, @general_item.id, @offer.chain_id)
    #offer_menu_item = @general_item.offer_menu_items.first
    #@general_item.required = reward_menu_item.required
    @general_item_ids = @offer_rule.offer_rule_menu_items.map(&:general_menu_item_id)
    rmi = OfferRuleMenuItem.where(:offer_rule_id => @offer_rule.id, :general_menu_item_id => @general_item.id).first
    puts rmi
    type = params[:type].eql?("Unset") ? false : true
    rmi.update_column(:zero_subtotal, type)
    if type == false
      @general_item = GeneralMenuItem.find params[:menu_item_id]
      rmi = OfferRuleMenuItem.where(:offer_rule_id => @offer_rule.id, :general_menu_item_id => @general_item.id).first
      rmi.destroy if rmi
    end
    #@general_item.zero_subtotal = type
    @general_item_ids = @offer_rule.offer_rule_menu_items.map(&:general_menu_item_id)
    @rmi = rmi

    redirect_to :back, :notice => "Menu Item Updated"
  end


  def update
    @offer = Offer.find(params[:offer_id])
    @offer_rule = OfferRule.find(params[:id])
    respond_to do |format|
    if @offer_rule.update_attributes(params[:offer_rule])
      @is_save = true
      format.js { }
      format.html{ redirect_to edit_admin_offer_path(@offer), notice: 'Offer Rule was successfully updated.'}
    else
      @is_save = false
      @error_messages = @offer_rule.errors.messages
      format.js {  }
      format.html {render action: "edit"}
    end
    end
  end

  def update_rule_status
    @offer = Offer.find(params[:offer_id])
    @offer_rule = OfferRule.find(params[:id])

    @offer_rule.update_column(:status , !@offer_rule.status)

    redirect_to :back
  end

  def new
    @offer = Offer.find params[:offer_id]
    @offer_rule = @offer.offer_rules.new
    #render :layout => false
  end

  def create
    @offer = Offer.find(params[:offer_id])
    @offer_rule = OfferRule.new(params[:offer_rule])
    @offer_rule.offer_id = @offer.id

    respond_to do |format|
      if @offer_rule.save
        @is_save = true
          format.html { redirect_to edit_admin_offer_path(@offer), notice: 'Offer Rule was successfully created.' }
          format.js {  }
      else
        @is_save = false
        @error_messages = @offer_rule.errors.messages
        format.html { render action: "new" }
        format.js {}
      end
    end
  end

  def destroy
    @offer_rule = OfferRule.find params[:id]
    @offer = Offer.find params[:offer_id]

    @offer_rule.destroy

    respond_to do |format|
      format.html { redirect_to :back }
    end
  end

  def general_rule_items
    @offer_rule = OfferRule.find params[:id]
    @offer = @offer_rule.offer
    chain = @offer.chain
    chain_id = chain.id
    @general_items = OfferRuleMenuItem.select("general_menu_items.*").
        joins("
      RIGHT JOIN general_menu_items ON general_menu_items.id = offer_rule_menu_items.general_menu_item_id AND offer_rule_menu_items.offer_rule_id = #{@offer_rule.id}
                                                                        ").
        order("offer_rule_menu_items.updated_at DESC NULLS LAST")
    .where("general_menu_items.chain_id = #{chain_id} AND (LOWER(item_name) LIKE '%#{(params[:key] || "").downcase}%' OR LOWER(description) LIKE '%#{(params[:key] || "").downcase}%' OR LOWER(item_keywords) LIKE '%#{(params[:key] || "").downcase}%')")
    .paginate(:page => params[:page], :per_page => Setting.pagination.per_page)
    @general_item_ids = @offer_rule.offer_rule_menu_items.map(&:general_menu_item_id)
  end

  def add_general_rule_item
    @offer = Offer.find params[:offer_id]
    @offer_rule = OfferRule.find params[:id]
    @general_item = GeneralMenuItem.find params[:menu_item_id]
    OfferRuleMenuItem.create(:offer_rule_id => @offer_rule.id, :general_menu_item_id => @general_item.id,:chain_id => @offer.chain_id)
    #offer_menu_item = @general_item.offer_menu_items.first
    #@general_item.required = reward_menu_item.required
    @general_item_ids = @offer_rule.offer_rule_menu_items.map(&:general_menu_item_id)
  end

  def remove_general_rule_item
    @offer = Offer.find params[:offer_id]
    @offer_rule = OfferRule.find params[:id]

    @general_item = GeneralMenuItem.find params[:menu_item_id]
    rmi = OfferRuleMenuItem.where(:offer_rule_id => @offer_rule.id, :general_menu_item_id => @general_item.id).first
    rmi.destroy if rmi
    @general_item_ids = @offer_rule.offer_rule_menu_items.map(&:general_menu_item_id)
  end

end
