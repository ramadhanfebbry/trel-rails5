class Admin::PartnerCodeSettingsController < Admin::BaseController

  def index
  end

  def search
    @chain = Chain.find(params["chain_id"])
    @partner_code_setting = PartnerCodeSetting.find_by_chain_id(@chain.id)
  end

  def new
    @chain = Chain.find params['chain_id']
    @partner_code_setting = PartnerCodeSetting.new(PartnerCodeSetting::DEFAULT_SETTING_REWARDCODE)
    render :layout => false
  end

  def edit
    @partner_code_setting = PartnerCodeSetting.find params[:id]
    @chain = Chain.find @partner_code_setting.chain_id
    render :layout => false
  end

  def create
    @partner_code_setting = PartnerCodeSetting.new(params[:partner_code_setting])
    if @partner_code_setting.save
      @save = true
    else
      @save = false
    end
  end

  def update
    @partner_code_setting = PartnerCodeSetting.find params[:id]
    if @partner_code_setting.update_attributes(params[:partner_code_setting])
      @update = true
    else
      @update = false
    end
  end

  def destroy
    @partner_code_setting = PartnerCodeSetting.find params[:id]
    @partner_code_setting.destroy unless @partner_code_setting.blank?

    redirect_to admin_partner_code_settings_path , :notice => "Partner Code Setting Deleted"
  end

end
