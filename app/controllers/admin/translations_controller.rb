class Admin::TranslationsController < Admin::BaseController

  def index
  	@translations = I18n.backend.store
  end
  
  def create
    I18n.backend.store_translations(params[:language], {params[:key] => params[:value]}, :escape => false)
    redirect_to admin_translations_url, :notice => "Added translations"
  end

end
