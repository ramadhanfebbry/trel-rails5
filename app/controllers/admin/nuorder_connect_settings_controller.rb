class Admin::NuorderConnectSettingsController < Admin::BaseController

  def index

  end

  def search
    @chain = Chain.find(params["chain_id"])
    @nuorder_connect_setting = NuorderConnectSetting.find_by_chain_id(@chain.id)
  end

  def new
    @chain = Chain.find params['chain_id']
    @nuorder_connect_setting = NuorderConnectSetting.new
    render :layout => false
  end

  def edit
    @nuorder_connect_setting = NuorderConnectSetting.find params[:id]
    @chain = Chain.find @nuorder_connect_setting.chain_id
    render :layout => false
  end

  def create
    @nuorder_connect_setting = NuorderConnectSetting.new(params[:nuorder_connect_setting])

    if @nuorder_connect_setting.save
      @save = true
    else
      @save = false
    end
  end

  def update
    @nuorder_connect_setting = NuorderConnectSetting.find params[:id]
    if @nuorder_connect_setting.update_attributes(params[:nuorder_connect_setting])
      @update = true
    else
      @update = false
    end
  end

  def destroy
    @nuorder_connect_setting = NuorderConnectSetting.find params[:id]
    @nuorder_connect_setting.destroy unless @nuorder_connect_setting.blank?

    redirect_to admin_nuorder_connect_settings_path , :notice => "OLO Connect Setting Deleted"
  end

end
