class Admin::RegionsController < Admin::BaseController
  before_action :find_region, :only => [:update_status, :edit, :show,:update]

  def index
    rescue_from_cancan(:index, Region)
    @countries = Country.active_countries
    @region = Region.new
    @regions = Region.includes(:country).paginate(page: params[:page], per_page: 10)
  end

  def update_status
    respond_to do |format|

      if @region.update_attribute(:active, params[:region_status])
        flash[:notice] = "region has been updated"
        format.html { redirect_to admin_regions_path }
        format.json { render json: @region }
      else
        flash[:error] = "region failed to update"
        format.html { redirect_to admin_regions_path }
        format.json { render json: @region }
      end
    end
  end
  
  def show    
  end

  def edit   
  end

  def update
    respond_to do |format|
      if @region.update_attributes(params[:region])
        flash[:notice] = "region has been updated"
        format.html { redirect_to admin_regions_path }
      else
        flash[:notice] = "region failed to create #{@region.errors.full_messages.each{|msg|}}"
        format.html { render :action => "edit" }
        format.json { render json: "failed" }
      end
    end
  end

  def create
    @region = Region.new(params[:region])

    respond_to do |format|
      if @region.save
        flash[:notice] = "region has been created"
        format.html { redirect_to admin_regions_path }
      else
        flash[:notice] = "region failed to create #{@region.errors.full_messages.each{|msg|}}"
        format.html { redirect_to admin_regions_path }
        format.json { render json: @region }
      end
    end
  end

  def search_region
    sth = params[:search_text_hidden]
    ar_field = [
      "regions.id",
      "regions.name",
      "countries.name",
      "regions.abbreviation",
      "regions.id",
      "regions.active"
    ]

    order = params[:qry_order]
    country_id = params[:country_id]

    if country_id != ""
      condition = "regions.country_id = '#{country_id}' AND lower(regions.name) like '%#{sth.downcase}%'"
    else
      condition = "lower(regions.name) like '%#{sth.downcase}%'"
    end

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @regions = Region.includes(:country).where(condition).order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @regions = Region.includes(:country).where(condition).order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    end

  end

  private

  def find_region
    @region = Region.find(params[:id])
  end
end
