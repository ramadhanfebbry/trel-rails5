class Admin::OloOrdersController < Admin::BaseController

  def index
    @orders = OloOrder.order('created_at desc').paginate(:per_page => 10, :page => params[:page])
  end

  def show
    @order = OloOrder.find params[:id]
  end

  def order_details
    @order = OloOrder.find params[:id]
    @details = @order.olo_order_details.order('created_at desc').paginate(:per_page => 10, :page => params[:page])
  end

  def olo_order_show
    @order = OloOrder.find params[:id]
    @detail = OloOrderDetail.find params[:detail_id]
  end

  def olo_receipt_detail
    @detail = OloWebhook.where(:receipt_id => params['receipt_id']).first
    if @detail.blank?
       @detail = OloOrderDetail.where(:receipt_id => params['receipt_id']).first rescue nil
    end

    @order = OloOrder.find @detail.olo_order_id rescue nil
  end

  def batch_process
    @order = OloOrder.find params[:id]
    @order.olo_order_details.not_processed.each do |x|
      begin
      x.receipt_process
      rescue
        next
        end
    end
    redirect_to order_details_admin_olo_order_path(@order) , :notice => "Order Are processed"
  end

  def process_olo_detail
    @od = OloOrderDetail.find params[:id]
    @od.receipt_process
    redirect_to order_details_admin_olo_order_path(@od.olo_order), :notice => "Order being procees"
  end

  def search
    sth = params[:search_text_hidden]

    ar_field = [
      "id",
      "batch_id"
    ]

    order = params[:qry_order]

    qry_condition = []
    qry_condition << "chain_id = '#{params[:chn_id]}'" unless params[:chn_id].blank?
    qry_condition << "batch_id = '#{sth.to_i}'" unless params[:search_text_hidden].blank?
    qry_condition = qry_condition.join(" AND ")


    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @orders = OloOrder.where(qry_condition).order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @orders = OloOrder.where(qry_condition).order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    else
      @orders = OloOrder.where(qry_condition).paginate(page: params[:page], per_page: 10)
    end
  end

end