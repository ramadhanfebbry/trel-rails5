class Admin::MilestonesController < Admin::BaseController

  # GET /milestones
  def index
  end

  def milestones_by_chain
    if params[:id].eql?("0")
      @chain = nil
      @milestones = []
    else
      @chain = Chain.find(params[:id])
      @milestones = @chain.milestones
    end
    respond_to do |format|
      format.js
      format.html
    end
  end

  def new
    @chain = Chain.find(params[:id])
    @milestone = @chain.milestones.new
  end

  def create
    @milestone = Milestone.new(params[:milestone])
    @chain = @milestone.chain
    if @milestone.save
      redirect_to milestones_by_chain_admin_milestone_path(@milestone.chain), :notice => "Milestone has successfully created."
    else
      render :new
    end
  end

  def add_reward
    @milestone = Milestone.find(params[:id])
    @reward = Reward.new
  end

  def create_milestone_reward
    @milestone = Milestone.find(params[:id])
    @reward = Reward.new(params[:reward])
    @reward.chain = @milestone.chain
    if @reward.save
      @milestone.update_attribute(:reward_id, @reward.id)
      @is_save = true
    else
      @is_save = false
    end
  end

  def edit_reward
    @milestone = Milestone.find(params[:id])
    @reward = Reward.find(params[:reward_id])
  end

  def update_milestone
    @milestone = Milestone.find(params[:id])
    @reward = Reward.find(params[:reward_id])
    if @reward.update_attributes(params[:reward])
      @is_save = true
    else
      @is_save = false
    end
  end

  def edit
    @milestone = Milestone.find(params[:id])
  end

  def update
    @milestone = Milestone.find(params[:id])
    if @milestone.update_attributes(params[:milestone])
      redirect_to milestones_by_chain_admin_milestone_path(@milestone.chain), :notice => "Milestone has successfully created."
    else
      render :edit
    end
  end

end
