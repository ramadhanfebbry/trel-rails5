class Admin::SurveysController < Admin::BaseController
  # GET /admin/surveys
  # GET /admin/surveys.json

  def index
    rescue_from_cancan(:index, Survey)
    @surveys = Survey.paginate(page: params[:page], per_page: 10)

    respond_to do |format|
      format.html # index.html.erb
      format.js
      format.json { render json: @surveys }
    end
  end

  # GET /admin/surveys/1
  # GET /admin/surveys/1.json
  def show
    rescue_from_cancan(:show, Survey)
    @survey = Survey.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @survey }
    end
  end

  # GET /admin/surveys/new
  # GET /admin/surveys/new.json
  def new
    rescue_from_cancan(:new, Survey)
    @survey = Survey.new
    #3.times do
    #  question = @survey.questions.build
    #  4.times { question.question_choices.build }
    #end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @survey }
    end
  end

  # GET /admin/surveys/1/edit
  def edit
    rescue_from_cancan(:edit, Survey)
    @survey = Survey.find(params[:id])
    @question = Question.new
  end

  # POST /admin/surveys
  # POST /admin/surveys.json
  def create
    rescue_from_cancan(:create, Survey)
    params[:survey][:valid_from_date] = format_date(params[:survey][:valid_from_date]) if !params[:survey][:valid_from_date].blank?
    params[:survey][:valid_to_date] = format_date(params[:survey][:valid_to_date]) if !params[:survey][:valid_to_date].blank?
    @survey = Survey.new(params[:survey])

    respond_to do |format|
      if @survey.save
        format.html { redirect_to admin_surveys_url, notice: 'Survey was successfully created.' }
        format.json { render json: @survey, status: :created, location: @survey }
      else
        format.html { render action: "new" }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/surveys/1
  # PUT /admin/surveys/1.json
  def update
    rescue_from_cancan(:update, Survey)
    @survey = Survey.find(params[:id])


    respond_to do |format|
      params[:survey][:valid_from_date] = format_date(params[:survey][:valid_from_date]) if !params[:survey][:valid_from_date].blank?
      params[:survey][:valid_to_date] = format_date(params[:survey][:valid_to_date]) if !params[:survey][:valid_to_date].blank?

      if @survey.update_attributes(params[:survey])
        format.html { redirect_to admin_surveys_url, notice: 'Survey was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/surveys/1
  # DELETE /admin/surveys/1.json
  def destroy
    rescue_from_cancan(:destroy, Survey)
    @survey = Survey.find(params[:id])
    @survey.destroy

    respond_to do |format|
      format.html { redirect_to admin_surveys_url }
      format.json { head :ok }
    end
  end

  def group_chain_list
    sth = params[:search_text_hidden]

    ar_field = [
      "surveys.id",
      "surveys.title",
      "chains.name",
      "surveys.valid_from_date",
      "surveys.valid_to_date",
      "offers.name",
      "rewards.name",
      "surveys.status",
      "surveys.repeatable"
    ]

    order = params[:qry_order]

    if params[:chn_id] != ""
      qry_condition = "chains.status = 'active' AND chains.id = '#{params[:chn_id]}' AND lower(surveys.title) like '%#{sth.downcase}%'"
    else
      qry_condition = "chains.status = 'active' AND lower(surveys.title) like '%#{sth.downcase}%'"
    end

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @surveys = Survey.includes(:chain, :offers, :rewards).where(qry_condition).order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @surveys = Survey.includes(:chain, :offers, :rewards).where(qry_condition).order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    else
      @surveys = Survey.includes(:chain, :offers, :rewards).where(qry_condition).paginate(page: params[:page], per_page: 10)
    end
  end

  def edit_max_question
    @survey = Survey.find params[:id]
  end

  def update_max_question
    @survey = Survey.find params[:id]
    @survey.update_attributes(:rotating_locations => nil, :max_rotating_questions => params[:survey][:max_rotating_questions])
    redirect_to admin_surveys_url, notice: 'Max rotating questions was successfully updated.'
  end

  def set_rotating_position
    @survey = Survey.find params[:id]
    @rotating_location =  ActiveSupport::JSON.decode(@survey.rotating_locations)  rescue nil
  end

  def submit_position_rotating
    @survey = Survey.find params[:id]
    @survey.update_attribute(:rotating_locations, ActiveSupport::JSON.encode(params[:position]))
  end

  def update_order_number
    params[:order].each_with_index do |q, i|
      q = Question.find q
      q.update_attribute(:order_number, i + 1)
    end unless params[:order].blank?
    @survey = Survey.find params[:id]
  end

  def set_choice_for_negative_response
    @choice = QuestionChoice.find params[:id]
    @question = @choice.question
    value = params[:value] == "1" ? true : false
    @choice.update_column(:is_considered_for_negative_response, value)
  end

  def pick_incentive
    @survey = Survey.find(params[:id])
    @chain = @survey.chain
  end

  def load_onetime_reward
    @survey = Survey.find(params[:id])
    @chain = @survey.chain
    @rewards = @chain.rewards.active.where(:reward_type => Reward::TYPES["INCENTIVE"]).
        paginate(page: params[:page], per_page: Setting.pagination.per_page) rescue []
    respond_to do |format|
      format.js { render "admin/surveys/incentive/load_one_time_reward"}
    end
  end

  def load_points
    @survey = Survey.find(params[:id])
    @chain = @survey.chain

    respond_to do |format|
      format.js { render "admin/surveys/incentive/load_points"}
    end
  end

  def add_point
    @survey = Survey.find(params[:id])
    @chain = @survey.chain
    @is_save_point = @survey.update_attributes(:point => params[:survey][:point], :reward_id => nil, :point_validation => true)
    @from_edit = params[:edit]
  end

  def assign_incentive
    @survey = Survey.find(params[:id])
  end

  def assign_with_existing_reward
    @survey = Survey.find(params[:id])
    @reward = Reward.find(params[:reward_id])
    @survey.update_attributes(:point => nil, :reward_id => @reward.id)
  end

  def remove_incentive
    @survey = Survey.find(params[:id])
    @chain = @survey.chain
    if params[:type] == "reward"
      @survey.update_attributes(:reward_id => nil)
    elsif params[:type] == "point"
      @survey.update_attributes(:point => nil)
    end
  end

  def edit_point
    @survey = Survey.find(params[:id])
    @chain = @survey.chain
  end

  def assign_incentive
    @survey = Survey.find(params[:id])
    @chain = @survey.chain
    @reward = Reward.new
  end

  def create_reward_incentive
    @survey = Survey.find(params[:id])
    @chain = @survey.chain
    @is_save = false
    @reward = Reward.new(params[:reward])
    @reward.status = "active"
    @reward.chain_id = @chain.id

    respond_to do |format|
      if @reward.save
        restaurant_ids = @chain.restaurants.map(&:id)

        unless restaurant_ids.blank?
          @reward.participate(restaurant_ids)
        end
        @survey.update_attributes(:point => nil, :reward_id => @reward.id)

        @is_save = true
        format.js { render "admin/surveys/incentive/create_incentive" }
      else
        format.js { render "admin/surveys/incentive/create_incentive"  }
      end
    end

  end

end