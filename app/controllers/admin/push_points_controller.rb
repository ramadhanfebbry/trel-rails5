class Admin::PushPointsController < Admin::BaseController
  # GET /admin/push_points
  # GET /admin/push_points.json
  def index
    @push_points = PushPoint.order("created_at desc").paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @push_points }
      format.js
    end
  end

  # GET /admin/push_points/1
  # GET /admin/push_points/1.json
  def show
    @push_point = PushPoint.find(params[:id])
    @user_ids = @push_point.user_ids[:user_ids].paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push_point }
    end
  end

  # GET /admin/push_points/new
  # GET /admin/push_points/new.json
  def new
    @push_point = PushPoint.new
    @push_point.all_user = false

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @push_point }
    end
  end

  # GET /admin/push_points/1/edit
  def edit
    @push_point = PushPoint.find(params[:id])
  end

  def all_user
    @push_point = PushPoint.new
    @push_point.all_user = true
  end


  # POST /admin/push_points
  # POST /admin/push_points.json
  def create
    #data_array = []
    #data_array = User.where("chain_id = ?", params["push_point"]["chain_id"]).map{|x| {:email => x.email, :user_id => x.id} } unless params['users_chain'].blank?
    #
    #@user_failed = []
    #@push_point = PushPoint.new(params[:push_point])
    #params[:user_ids].uniq.each do |user_id|
    #  begin
    #    user = User.find(user_id)
    #    if user.chain_id.eql?(@push_point.chain_id)
    #      data_array << {:email => user.email, :user_id => user.id}
    #    else
    #      @user_failed << {:user_id => user_id, :message => "User not included on chain that selected. User chain is #{user.chain.name}, but selected chain is  #{@push_point.chain.name}"}
    #    end
    #  rescue => e
    #    @user_failed << {:user_id => user_id, :message => e.inspect.to_s}
    #  end
    #end if !params[:user_ids].blank? and @push_point.valid?
    #
    #@push_point.user_push_points.new(data_array)
    #
    #respond_to do |format|
    #  if @push_point.save && @push_point.send_push_point_job
    #    @is_save = true
    #    format.html { redirect_to admin_push_points_path, notice: 'Push point was successfully created.' }
    #    format.json { render json: @push_point, status: :created, location: @push_point }
    #    format.js
    #  else
    #    format.html { render action: "new" }
    #    format.json { render json: @push_point.errors, status: :unprocessable_entity }
    #    format.js
    #  end
    #end
    @push_point = PushPoint.new(params[:push_point])
    @push_point.pusher_id = current_admin.id
    confirm = false if params['confirm'].blank?
    confirm = true unless params['confirm'].blank?
    case params[:push_point][:push_type].to_i
      when 1
        unless params[:push_point][:xls].blank?
          begin
            user_ids = []
            book = Spreadsheet.open "#{params[:push_point][:xls].path}"
            sheet1 = book.worksheet 0
            sheet1.column(0).each do |row|
              user_ids << row.to_i if !row.to_s.include?(" ")
            end
            @duplicate_count = user_ids.length - user_ids.compact.uniq.length
            user_ids = user_ids.compact.uniq
            @uniq_count =  user_ids.length
            error = false
            err_msg = ""

            if user_ids.size == 0
              err_msg = "There are No Content on the xls files"
              error = true
            end

            if user_ids.size > 0
              user_ids.each do |uid|
                if uid.to_i == 0
                  error = true
                  err_msg = "Only User ids content are valid in the xls files"
                  break
                end
              end
            end

            return redirect_to new_admin_push_point_path, :alert => err_msg if error == true
            @push_point.user_ids = {:user_ids => user_ids}
            REDIS.set "push_point_#{current_admin.id}", user_ids.to_json
            puts "saving redis"
            @push_point.all_user = false
            is_valid = @push_point.valid?
          rescue => e
            puts "#{e.inspect}"
            @push_point.errors.add(:xls, "Please upload the xls file" )
          end
        else ## it means confirmations is == true
          puts "masuk sini boiiiiii"
          unless params['confirm'].blank?
            puts "masuk sini boiiiiii2"
            @push_point.xls = true
            is_valid = @push_point.valid?
          end
        end
      when 2
        user_ids = []
        user_ids = params[:push_point][:user_temp_ids]
        user_ids = user_ids.squish.gsub("\\r",'').gsub('\\n','').gsub(' ','').split(',')
        @duplicate_count = user_ids.length - user_ids.compact.uniq.length
        user_ids = user_ids.compact.uniq
        @uniq_count =  user_ids.length
        @push_point.user_ids = {:user_ids => user_ids}
        is_valid = @push_point.valid?
        @push_point.all_user = false
      when 3
        # count_email = User.where("chain_id = ?", params[:push_point][:chain_id]).count
        # loop = (count_email.to_f / 4000.to_f).ceil
        user_ids = []
        # 1.upto(loop) do |page|
        #   user_ids += User.where("chain_id = ?", params[:push_point][:chain_id]).
        #       select('id').
        #       paginate(page: page, per_page: 4000).map(&:id)
        # end

        ## no need confirmations
        params['confirm'] = "true"
        @push_point.is_all_user = true
        @push_point.user_ids = {:user_ids => user_ids}
        is_valid  =  verify_recaptcha(:private_key => ENV['RECAPTCHA_PRIVATE_KEY'],
                                      :model => @push_point, :message => "Oh! It's error with reCAPTCHA!"
        ) && @push_point.valid?
    end

    if is_valid  and params['confirm'] == "true"
      if @push_point.push_type.eql?(1)
        u_ids = JSON.parse(REDIS.get "push_point_#{current_admin.id}")
        @push_point.user_ids = {:user_ids => u_ids}
      end

      @push_point.executed_at = nil if params['scheduler'].to_s != "on"
      diff_hour = 5
      diff_hour = 4 if params['time_zone_default'].to_s == "EDT"
      @push_point.executed_at = @push_point.executed_at - diff_hour.hours if params['scheduler'] == "on"
      @push_point.save
      @push_point.delay(:delayable_type => @push_point.class.to_s, :delayable_id => @push_point.id).send_push_point_job
      redirect_to admin_push_points_path, notice: 'Push point was successfully created.'
    else
      if confirm == false and is_valid == true
        @confirmation_box = true
      end
      render :new
    end
  end

  def summary_success
    @push_point = PushPoint.find params[:id]
    @logs = @push_point.success_log_entries.select{|a| a[(params[:search_type] || "user_id" ).to_sym].to_s =~ /#{params[:key]}/}.paginate(:page => params[:page], :per_page => 30)

    respond_to do |format|
      format.html
      format.js
      format.csv {
        Delayed::Job.enqueue(
            ExportCsvJob::SummarySuccessPoint.new(
                params,
                current_admin),
            :delayable_type => "export_point_point_#{@push_point.id}")

        redirect_to summary_success_admin_push_point_path(@push_point), :notice => "Files will be sent to your email shortly.."
      }
    end
  end

  def summary_failed
    @push_point = PushPoint.find params[:id]
    @logs = @push_point.error_log_entries.select{|a| a[(params[:search_type] || "user_id" ).to_sym].to_s =~ /#{params[:key]}/}.paginate(:page => params[:page], :per_page => 30)
  end
  
  def filter_push_points
    sth = params[:search_text_hidden]
    if params[:chn_id] != ""
      @push_points = PushPoint.where("chain_id = '#{params[:chn_id]}' AND lower(push_points.notes) like ?",'%'+sth.downcase+'%').paginate(page: params[:page], per_page: Setting.pagination.per_page)
    else
      @push_points = PushPoint.where("lower(push_points.notes) like ?",'%'+sth.downcase+'%').paginate(page: params[:page], per_page: Setting.pagination.per_page)
    end
  end

  def preview_xls_data
    @email_from_xls = []
    test_file = params[:xls]
    manual_user_ids = params[:manual_user_ids].split(",")
    @notice = "Preview"
    unless test_file.blank?
      begin
        book = Spreadsheet.open "#{test_file.path}"
        sheet1 = book.worksheet 0
        sheet1.column(0).each { |row|     @email_from_xls << row.to_i }
        @email_from_xls = @email_from_xls.compact.uniq
      rescue
        @error = "Please upload the xls file"
      end
    end
    @email_from_xls << manual_user_ids
    @email_from_xls = @email_from_xls.flatten
    @email_from_xls = @email_from_xls.uniq
    respond_to do |format|
      format.html do
        render :layout => false
      end
      format.js
    end
  end

  # PUT /admin/push_points/1
  # PUT /admin/push_points/1.json
  def update
    @push_point = PushPoint.find(params[:id])

    data_array = []

    @user_failed = []
    @push_point.attributes = params[:push_point]
    params[:user_ids].uniq.each do |user_id|
      begin
        user = User.find(user_id.to_i)
        if user.chain_id.eql?(@push_point.chain_id)
          data_array << {:email => user.email, :user_id => user.id}
        else
          @user_failed << {:user_id => user_id, :message => "User not included on chain that selected. User chain is #{user.chain.name}, but selected chain is  #{@push_point.chain.name}"}
        end
      rescue => e
        @user_failed << {:user_id => user_id, :message => e.inspect.to_s}
      end
    end if !params[:user_ids].blank? and @push_point.valid?

    @push_point.user_push_points.new(data_array)

    ## delete user list email and delayed jobs
    @push_point.user_push_points.delete_all
    @push_point.delayed_jobs.delete_all

    @push_point.user_push_points.new(data_array)

    respond_to do |format|
      if @push_point.save && @push_point.send_push_point_job
        @is_save = true
        format.html { redirect_to admin_push_points_path, notice: 'Push point was successfully updated.' }
        format.json { head :ok }
        format.js
      else
        format.html { render action: "edit" }
        format.json { render json: @push_point.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  # DELETE /admin/push_points/1
  # DELETE /admin/push_points/1.json
  def destroy
    @push_point = PushPoint.find(params[:id])
    @push_point.destroy

    respond_to do |format|
      format.html { redirect_to admin_push_points_path }
      format.json { head :ok }
    end
  end
end