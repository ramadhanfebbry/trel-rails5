class Admin::AppcodeSettingsController < Admin::BaseController
  before_action :find_chain

  def new
    @appcode = AppcodeSetting.new
    @appcode.chain_id = @chain.id
  end


  def create
    @appcode = AppcodeSetting.new(params[:appcode_setting])
    @appcode.chain_id = @chain.id

    if @appcode.save
      redirect_to edit_admin_chain_appcode_setting_path(@chain,@appcode), :notice => "App loyalty created"
    else
      render 'new'
    end
  end

  def update
    @appcode = AppcodeSetting.find(params[:id])

    respond_to do |format|
      if @appcode.update_attributes(params[:appcode_setting])
        format.html { redirect_to edit_admin_chain_appcode_setting_path(@chain,@appcode), notice: 'App loyalty created updated' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @appcode.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @appcode = AppcodeSetting.find params[:id]
  end


  private

  def find_chain
    @chain = Chain.find params[:chain_id]
  end

end