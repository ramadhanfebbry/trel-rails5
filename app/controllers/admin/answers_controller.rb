class Admin::AnswersController < Admin::BaseController 
  def index
    rescue_from_cancan(:index, SurveysUser)
    #@answers = Answer.includes(:user,:question => {:survey => {:rewards => :chain}})
    #@answer_user_ids = Answer.all.map(&:user_id).uniq!
    @surveys = SurveysUser.includes(:user,:answers, :survey => :chain ).order("surveys_users.updated_at desc").paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.html
      format.js
      #format.csv {export_answers_csv SurveysUser.includes(:user,:answers, :survey => :chain ).order("surveys_users.updated_at desc") }
      format.csv {export_answer_csv }

    end
  end

  def search
    rescue_from_cancan(:search, SurveysUser)
    @search_params = params["search"]
#    @surveys = SurveysUser.includes(:user,:answers, :survey => :chain ).search(@search_params).order("surveys_users.updated_at desc").paginate(page: params[:page], per_page: 10)#[SurveysUser.first]

    ar_field = [
      "surveys_users.updated_at",
      "surveys_users.receipt_id",
      "users.email",
      "chains.name",
      "",
      "surveys.title",
      "",
      "restaurants.dashboard_display_text",
      "receipts.status",
      "surveys_users.receipt_id",
      "surveys_users.updated_at"

    ]

    order = params[:qry_order]

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @surveys = SurveysUser.includes(:user,:answers,:restaurant, :receipt, :survey => :chain ).search(@search_params).order(qry_order).reverse_order
      qry_order = qry_order + " desc"
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @surveys = SurveysUser.includes(:user,:answers,:restaurant, :receipt, :survey => :chain ).search(@search_params).order(qry_order)
    end

    @surveys = @surveys.paginate(page: params[:page], per_page: 10)

    respond_to do |format|
      format.html
      format.js
      #format.csv { export_answers_csv SurveysUser.includes(:user,:answers, :survey => :chain ).search(@search_params).order("surveys_users.updated_at desc") }
      format.csv { export_answer_csv }

    end
  end

  def export_answer_csv
    search = params['search']
    search = {} if search.blank?
    search.merge!('admin_id' => current_admin.id)
    Delayed::Job.enqueue(ReportJob::AnswerJob.new(search), :delayable_type => "export_answer_csv")
    
    redirect_to admin_answers_path, :flash => {:notice  => "Please wait, it will send to your email shortly....", :send_job => true}
  end

  def export_answers_csv(surveys)

    surveys_csv = CSV.generate do |csv|
      # header row
      csv << [
        "Survey Submit Id",
        "Email",
        "Chain",
        "Survey Name",        
        "Offer / Reward / Deal Names",
        "q1",
        "q2",
        "q3",
        "q4",
        "q5",
        "Restaurant Location (for offers)",
        "Receipt review status","Date time"
      ]

      # data rows
      surveys.each do |answer_survey|
        q1 = nil;q2=nil;q3=nil;q4=nil;q4=nil;q5=nil;
        (
          
          answer_survey.answers.each_with_index do |x,index|
            text_content = ""
            text_content += x.question.text rescue "No Question Selected "
            text_content += " = "
            text_content +=  x.value.to_s.to_s.gsub(',','').squish
            q1 = text_content if index == 0
            q2 = text_content if index == 1
            q3 = text_content if index == 2
            q4 = text_content if index == 3
            q5 = text_content if index == 4
          end
        )
        csv << [
          answer_survey.receipt_id,
          (answer_survey.user.email rescue nil),
          (answer_survey.survey.chain.name rescue nil),
          (answer_survey.survey.title rescue nil),
          (answer_survey.receipt.receipt_transactions.first.offer.name rescue nil),
          q1,
          q2,
          q3,
          q4,
          q5,          
          (answer_survey.receipt.last_transaction.restaurant.dashboard_display_text rescue nil),
          (Receipt::STATUS.index(answer_survey.receipt.status) rescue nil),
          (answer_survey.updated_at.strftime('%m-%d-%Y'))
        ]
      end
    end

    send_data(surveys_csv, :type => 'text/csv', :filename => 'answers.csv')
  end
end
