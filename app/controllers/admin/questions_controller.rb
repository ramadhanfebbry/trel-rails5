class Admin::QuestionsController < Admin::BaseController
  # GET /admin/questions
  # GET /admin/questions.json
  def index
    rescue_from_cancan(:index, Question)
    @questions = Question.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @questions }
    end
  end

  # GET /admin/questions/1
  # GET /admin/questions/1.json
  def show
    rescue_from_cancan(:show, Question)
    @question = Question.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @question }
    end
  end

  # GET /admin/questions/new
  # GET /admin/questions/new.json
  def new
    rescue_from_cancan(:new, Question)
    #@question = Question.new(:survey_id=>params[:survey])
    @survey_id =  params[:survey]
    respond_to do |format|
      format.js { render :layout => false } # new.html.erb
      format.json { render :json=>@question }
    end
  end

  # GET /admin/questions/1/edit
  def edit
    rescue_from_cancan(:edit, Question)
    @question = Question.find(params[:id])

    if @question.update_attributes(params[:question])
      render :json=> true
    else
      render :json=> {:error=>@question.errors}
    end
  end

  # POST /admin/questions
  # POST /admin/questions.json
  def create
    rescue_from_cancan(:create, Question)
    @question = Question.new(params[:question])
    @survey = @question.survey
    @is_save = @question.save
    respond_to do |format|
      if @is_save
        format.js { render :layout => false } # new.html.erb
        format.json {
          render :json=>{
              :head=>true,
              :question=>@question.as_json(:only => [:id, :text, :label, :value], :include => [:question_choices])
          }
        }
      else
        format.js
        format.json { render :json=> @question.errors }
      end
    end
  end

  # PUT /admin/questions/1
  # PUT /admin/questions/1.json
  def update
    rescue_from_cancan(:update, Question)
    @question = Question.find(params[:id])


    respond_to do |format|
      if @question.update_attributes(params[:Question])
        format.html { redirect_to admin_questions_url, notice: 'Question was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/questions/1
  # DELETE /admin/questions/1.json
  def destroy
    rescue_from_cancan(:destroy, Question)
    @question = Question.find(params[:id])
    @survey = @question.survey
    respond_to do |format|
      if (@question)
        choices = @question.question_choices
        @question.delete
        choices.destroy_all
        rotating_questions_before = @question.show_rotating_questions_on_position("before")
        rotating_questions_after = @question.show_rotating_questions_on_position("after")
        @survey.update_attribute(:rotating_locations, nil) if !rotating_questions_before.blank? or !rotating_questions_after.blank?
        format.js { render :layout => false }
        format.json { render :json=> true }
      else
        format.json { render :json=> {:error=>"System error: unable to delete."}}
      end
    end
  end

  def question_with_type
    @value_type = params[:id].to_i
    @question = Question.new(:survey_id => params[:survey_id], :question_type => @value_type)
    1.upto(2) do |i|
      @question.question_choices.build
    end
  end
end