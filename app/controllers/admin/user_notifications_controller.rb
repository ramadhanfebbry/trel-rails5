class Admin::UserNotificationsController < Admin::BaseController

  before_action :find_chain

  def  index
    @user_notifications = @chain.user_activity_notifications
  end

  def new
    @user_notification = @chain.user_activity_notifications.new(:unique_identifier => Time.zone.now.to_i)
    @user_notification.user_activity_notification_fields.build
  end

  def create
    begin
      @user_notification = @chain.user_activity_notifications.new(params[:user_activity_notification])
      if @user_notification.save
        redirect_to admin_chain_user_notifications_path(@chain), :notice => "User Activity Notification Based has been created."
      else
        render :new
      end
    rescue ActiveRecord::NestedAttributes::TooManyRecords
      params[:user_activity_notification].delete(:user_activity_notification_fields_attributes)
      @user_notification = @chain.user_activity_notifications.new(params[:user_activity_notification]) if @user_notification.blank?
      @user_notification.errors.add(:user_activity_notification_fields, "To Many records, Max 5 allowed.")
      render :new
    end
  end

  def edit
    @user_notification = @chain.user_activity_notifications.find(params[:id])
  end

  def update
    @user_notification = @chain.user_activity_notifications.find(params[:id])

    begin
      if @user_notification.update_attributes(params[:user_activity_notification])
        redirect_to admin_chain_user_notifications_path(@chain), :notice => "User Activity Notification Based has been updated."
      else
        render :edit
      end
    rescue ActiveRecord::NestedAttributes::TooManyRecords
      @user_notification.errors.add(:user_activity_notification_fields, "To Many records, Max 5 allowed.")
      render :edit
    end
  end

  def destroy
    @user_notification = @chain.user_activity_notifications.find(params[:id])
    @user_notification.destroy
    redirect_to admin_chain_user_notifications_path(@chain), :notice => "User Activity Notification Based has been deleted."
  end

  protected

  def find_chain
    @chain = Chain.find(params[:chain_id])
  end

end