class Admin::BucketPathsController < Admin::BaseController

  before_action :find_chain
  # GET /admin/bucket_paths
  # GET /admin/bucket_paths.json
  def index
    @bucket_paths = @chain.bucket_paths

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @bucket_paths }
    end
  end

  # GET /admin/bucket_paths/1
  # GET /admin/bucket_paths/1.json
  def show
    @bucket_path = @chain.bucket_paths.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @bucket_path }
    end
  end

  # GET /admin/bucket_paths/new
  # GET /admin/bucket_paths/new.json
  def new
    p BucketPath.tag_choices
    @bucket_path = @chain.bucket_paths.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @bucket_path }
    end
  end

  # GET /admin/bucket_paths/1/edit
  def edit
    @bucket_path = @chain.bucket_paths.find(params[:id])
  end

  # POST /admin/bucket_paths
  # POST /admin/bucket_paths.json
  def create
    @bucket_path = @chain.bucket_paths.new(params[:bucket_path])

    respond_to do |format|
      if @bucket_path.save
        format.html { redirect_to admin_chain_bucket_paths_path(@chain), notice: 'Bucket path was successfully created.' }
        format.json { render json: @bucket_path, status: :created, location: @bucket_path }
      else
        format.html { render action: "new" }
        format.json { render json: @bucket_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/bucket_paths/1
  # PUT /admin/bucket_paths/1.json
  def update
    @bucket_path = @chain.bucket_paths.find(params[:id])

    respond_to do |format|
      if @bucket_path.update_attributes(params[:bucket_path])
        format.html { redirect_to admin_chain_bucket_paths_path(@chain), notice: 'Bucket path was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @bucket_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/bucket_paths/1
  # DELETE /admin/bucket_paths/1.json
  def destroy
    @bucket_path = @chain.bucket_paths.find(params[:id])
    @bucket_path.destroy

    respond_to do |format|
      format.html { redirect_to admin_chain_bucket_paths_url(@chain) }
      format.json { head :ok }
    end
  end

  private
  def find_chain
    @chain = Chain.find(params[:chain_id])
  end
end