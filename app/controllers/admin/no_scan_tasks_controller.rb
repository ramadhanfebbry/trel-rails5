class Admin::NoScanTasksController < Admin::BaseController
  before_action :find_job, :except => [:index, :search]

  def index
    #job_type = params['type'].blank? ? "queue" : params['type']
    #@jobs = OwnerJob.includes(:owner,:chain).send(job_type).order('updated_at desc').paginate(page: params[:page], per_page: 10)
    #respond_to do |format|
    # # format.html{render :template => '/admin/miss_you_tasks/new_index'}
    #  format.html {}
    #  format.js { render 'search' }
    #end
    params['type'] = ""

    search = {}
    @chain_id = params['chain_id']
    @chain = Chain.find(@chain_id) rescue nil

    type = params['type'].titleize rescue "Queue"
    search.merge!({:chain_id => @chain_id, :type => OwnerJob::STATUS[type], :owner_id => nil})

    @jobs = NoScanJob.search(search).admin_only.includes(:chain).order('updated_at desc').paginate(page: params[:page], per_page: 10)
    @testing = REDIS.get "testing_owner_job"
    respond_to do |format|
      format.html {render 'index'}
    end
  end

  def show
    respond_to do |format|
      format.html
      format.js
      if params["error"].blank?
        format.csv {
          render :layout => false
        }
      else
        format.csv {
          render :template => "admin/no_scan_tasks/show_error.csv",:layout => false
        }
      end
    end
  end

  def testing_activation
    testing = REDIS.get "testing_owner_job"
    if testing.blank?
      REDIS.set "testing_owner_job", true
    else
      REDIS.set "testing_owner_job", nil
    end
    redirect_to admin_miss_you_tasks_path
  end

  def edit
    if @job.status != 1
      return render :text => "You cannot Edit this job This already been approved"
    end
  end

  def update_step
    @chain = Chain.find @job.chain_id
    @owner = Owner.find @job.owner_id
    @question1 = @chain.dashboard_questions.where('order_number = 1').first
    @question2 = @chain.dashboard_questions.where('order_number = 2').first
    @users_count = @chain.users.count

    @rewards = @owner.chain.rewards.active if params[:step] == "2"
    respond_to do |format|
      #format.html{render :template => '/admin/miss_you_tasks/new_index'}
      format.html {}
      format.js { render "step1" }
    end
  end

  def validation_ajax

  end

  def update_step_ajax
    if params[:step] == "3"
      @date = params["date"]
      min = params["time_stamp"]["(5i)"].to_i
      hour = params["time_stamp"]["(4i)"].to_i
      puts "hour = #{hour}"
      hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
      plus_day = 0

      if hour > 24
        hour = hour - 24
        plus_day = 1
      end
      @date = @date.to_time.change(:hour => hour, :min => min)
      @date = @date + plus_day.days
      @job.schedule_type = params['owner_job']['schedule_type']
      @job.owner_scheduler = OwnerScheduler.set_scheduler(params)
      @job.save

      if @job.schedule_type == 1 # if one time
        @job.update_column(:executed_at, @date)
      end
    elsif params[:step] == "1"
      case params[:owner_job][:job_type].to_i
        when 1
          params[:owner_job][:activity_days] = nil
        when 2
          params[:owner_job][:percentage] = nil
        when 3
          params[:owner_job][:activity_days] = nil
          params[:owner_job][:percentage] = nil
      end
    elsif params[:step] == "2"

      case params[:owner_job][:reward_type].to_i
        when 1
          params[:owner_job][:points] = nil
          params[:owner_job][:subject_email] = nil
          params[:owner_job][:content_email] = nil
          params[:owner_job][:content_phone] = nil
        when 2
          params[:owner_job][:subject_email] = nil
          params[:owner_job][:content_email] = nil
          params[:owner_job][:content_phone] = nil
          params[:owner_job][:reward_id] = nil
        when 3
          params[:owner_job][:points] = nil
          params[:owner_job][:reward_id] = nil
      end
    end
    #if @job.update_attributes(params[:owner_job]) == false
    #  render :text => "Error"
    #end
  end

  def update_from_admin
    if @job.update_attributes(params["owner_job"])
      exec =@job.executed_at
      exec = exec.change(:hour => params["time_stamp"]["(4i)"].to_i, :min => params["time_stamp"]["(5i)"].to_i)
      puts "exec ====== #{exec}"
      @job.update_column(:executed_at, exec)
      redirect_to admin_no_scan_tasks_path, :notice => "Job Successfully updated"
    else
      render :action => :edit
    end
  end

  def update
    if params['type'] == 'cancel'
      ## delete the job first
      DelayedJob.where(:delayable_id => @job.id, :delayable_type => 'OwnerJob').first.destroy rescue nil
      @job.update_attribute(:status, 5) ## update as a rejected
      redirect_to admin_miss_you_tasks_path(:type => 'rejected', :chain_id => params['chain_id']), :notice => "The job is moved to rejected.."
    elsif params['type'] == "move_to_queue"
      ## from rejected -> queue
      if @job.update_attribute(:status, 1)
        @job.execute_jobs  if @job.schedule_type == 1
        redirect_to admin_no_scan_tasks_path(:type => 'queue', :chain_id => params['chain_id']), :notice => "The job is approved.."
      end
    else
      if @job.update_attribute(:status, 2)
        if @job.schedule_type == 1
          jobs = DelayedJob.where('delayable_type = ? and delayable_id = ?','OwnerJob', @job.id)
          unless jobs.blank?
            puts "deleting duplicate one time jobs"
            ## if there were delayedjob for this job , delete it first
            jobs.delete_all
          end
          @job.execute_jobs
        else
          @job.update_column(:status, 0)
        end
        redirect_to admin_no_scan_tasks_path(:type => 'approved', :chain_id => params['chain_id']), :notice => "The job is approved.."
      end
    end
  end

  def search
    search = {}
    @chain_id = params['chain_id']
    @chain = Chain.find(@chain_id) rescue nil
    type = params['type'].titleize
    search.merge!({:chain_id => @chain_id, :type => OwnerJob::STATUS[type]})
    @testing = REDIS.get "testing_owner_job"
    @jobs = NoScanJob.search(search).admin_only.includes(:noscanjob_logs,:chain).order('updated_at desc').paginate(page: params[:page], per_page: 10)

    respond_to do |format|
      format.html {render 'index'}
    end
  end

  def destroy
    if @job.update_attribute(:status, 5)
      redirect_to admin_miss_you_tasks_path(:type => 'rejected', :chain_id => params['chain_id']), :notice => "The miss you task is stopped.."
    end
  end

  def completed_job
    @history = @job.owner_job_histories.first
  end

  private

  def find_job
    @job = NoScanJob.find params[:id]  rescue nil
  end
end