class Admin::AdminJobsController < Admin::BaseController

  def index
    if params["type"] == "running" || params['type'].blank?
     @jobs = DelayedJob.running.paginate(page: params[:page], per_page: 10)
    elsif params["type"] == "queue"
      @jobs = DelayedJob.running.order('run_at desc').paginate(page: params[:page], per_page: 10)
    else # error jobs
      @jobs = DelayedJob.errors.order('run_at desc').paginate(page: params[:page], per_page: 10)
    end
  end
end
