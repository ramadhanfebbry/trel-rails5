class Admin::CommentsController < Admin::BaseController

  def index
    @comments = Comment.order('created_at desc').paginate(:per_page => 10, :page => params[:page])
  end

  def update_email_process
    @comment = Comment.find(params[:id])
    @comment.generate_parse_email

    redirect_to :back, :notice => "Parse updated"
  end

  def process_receipt
    @comment = Comment.find(params[:id])
    @comment.receipt_process

    redirect_to :back, :notice => "Order updated"
  end

  def new
    @comment = Comment.new
  end

  def create
    @comment = Comment.new(params[:comment])

    unless @comment.parameter.blank?
      ## updaload an eml files
      puts @comment.parameter
      uploaded_io = params[:comment][:parameter]
      File.open(Rails.root + '/tmp/' + uploaded_io.original_filename, 'wb') do |file|
        file.write(uploaded_io.read)
      end

      m = Mail.read(Rails.root + '/tmp/' + uploaded_io.original_filename)
      @comment.body = m.html_part.body.to_s
      File.delete(Rails.root + '/tmp/' + uploaded_io.original_filename)
      #uploaded_io = params[:person][:picture]

    end

    if @comment.save
      redirect_to admin_comments_path, :notice => 'email created'
    else
        render :new
    end
  end

  def update
    @comment = Comment.find(params[:id])

    unless params[:comment][:parameter].blank?
      ## updaload an eml files
      puts @comment.parameter
      uploaded_io = params[:comment][:parameter]
      File.open(Rails.root + '/tmp/' + uploaded_io.original_filename, 'wb') do |file|
        file.write(uploaded_io.read)
      end

      m = Mail.read(Rails.root + '/tmp/' + uploaded_io.original_filename)
      @comment.body = m.html_part.body.to_s
      File.delete(Rails.root + '/tmp/' + uploaded_io.original_filename)
      #uploaded_io = params[:person][:picture]

    end

    if @comment.update_attributes(params[:comment])
      redirect_to admin_comments_path, :notice => 'email updated'
    else
      render :edit
    end
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def show
    @comment = Comment.find(params[:id])
    render :layout => false
  end

  def upload_eml_files
    Comment.find(2).update_column(:body,m.html_part.body.to_s)
  end

  def nokogiri_parse_email
    p "=" * 100
    p parse_email(params[:id])
    p "=" * 100
  end
end