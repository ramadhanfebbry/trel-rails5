class Admin::DashboardQuestionsController < Admin::BaseController
  before_action :find_chain

  def index

  end

  def destroy
    question = DashboardQuestion.find(params[:id])
    @valid = question.destroy

    respond_to do |format|
      format.js { render :template => "admin/dashboard_questions/destroy_choice.js" }
    end
  end

  def new
    if @chain.dashboard_questions.size > 0
      q1 = @chain.dashboard_questions.where('order_number = 2').first
      q2 = @chain.dashboard_questions.where('order_number = 1').first

      if q1.blank?
        @question1 = DashboardQuestion.new
        1.upto(2) do |i|
          @question1.dashboard_choices.build
        end
      else
        @question1 = q1
      end
      if q2.blank?
        @question2 = DashboardQuestion.new
        1.upto(2) do |i|
          @question2.dashboard_choices.build
        end
      else
        @question2 = q2
      end

      @question = DashboardQuestion.new
      @valid = true
      #@error_message = "Limit Question Are 2, 1 question for each step"
    else

      @question1 = DashboardQuestion.new
      1.upto(2) do |i|
        @question1.dashboard_choices.build
      end
      @question2 = DashboardQuestion.new
      1.upto(2) do |i|
        @question2.dashboard_choices.build
      end
      @valid = true
    end
  end

  def update
    chain = Chain.find params['chain_id']
    @question = DashboardQuestion.find(params['id'])
    #@question = DashboardQuestion.new()
    #@survey = @question.survey

    @step = @question.order_number
    #begin

    @is_save = @question.update_attributes(params[:dashboard_question].merge!(:chain_id => chain.id))

    respond_to do |format|
      if @is_save
        #format.js { render :layout => false } # new.html.erb
        format.js { render 'create' }
        format.json {
          render :json => {
              :head => true,
              :chain => chain.as_json(:only => [:id]),
              :question => @question.as_json(:only => [:id, :text, :value], :include => [:question_choices])
          }
        }
      else
        format.js { render 'create' }
        format.json { render :json => @question.errors }
      end
    end
  end

  def create
    @question = DashboardQuestion.new(params[:dashboard_question].merge!(:chain_id => @chain.id))
    #@survey = @question.survey
    @is_save = @question.save
    @step = params["dashboard_question"]["order_number"]
    @question.dashboard_choices_attributes = params[:dashboard_question]["dashboard_choices_attributes"] rescue []
    respond_to do |format|
      if @is_save
        format.js { render :layout => false } # new.html.erb
        format.json {
          render :json => {
              :head => true,
              :chain => @chain.as_json(:only => [:id]),
              :question => @question.as_json(:only => [:id, :text, :value], :include => [:question_choices])
          }
        }
      else
        format.js
        format.json { render :json => @question.errors }
      end
    end
  end

  private

  def find_chain
    @chain = Chain.find params[:chain_id]
  end
end
