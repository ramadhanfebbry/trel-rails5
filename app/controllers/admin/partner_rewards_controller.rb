class Admin::PartnerRewardsController < Admin::BaseController

  def index
    rescue_from_cancan(:index, Reward)
    unless params["chain_id"].blank?
      @chain = Chain.find(params["chain_id"])
      @rewards = @chain.rewards.where("reward_type = #{Reward::TYPES["PARTNER"]}").includes(:chain).order("updated_at DESC").paginate(page: params[:page], per_page: Setting.pagination.per_page)
      @reward_type = 1
      @reward = Reward.new
    else
      @rewards = []
      @reward = Reward.new
      @chain = nil
    end
  end

  def new
    rescue_from_cancan(:new, Reward)
    @reward = Reward.new
    @chain = Chain.find(params["chain_id"])
  end

  def create
    rescue_from_cancan(:create, Reward)
    @reward = Reward.new(params[:reward])
    @reward.points = 0
    @chain = Chain.find(@reward.chain_id)
    respond_to do |format|
      if @reward.save and @reward.activate!
        all_location = (params[:partner_reward][:valid_all_locations] || false rescue false)
        partner = PartnerReward.new(params[:partner_reward])
        partner.reward_id = @reward.id
        partner.valid_all_locations = all_location
        partner.valid_locations = "{#{params[:partner_reward][:valid_locations].join(",") rescue nil}}"
        partner.save
        params[:partner_hours].each do |partner_hour|
          hour = partner_hour[1]
          PartnerHour.create(partner_reward_id: partner.id, day_of_week: hour["day_of_week"], start: hour["start"],  end: hour["end"] ) if hour["start"].present? && hour["end"].present?
        end if params[:partner_hours].present?
        params[:partner_reward_category].each do |category|
          mandatory = category[1]["mandatory"] == "true" ? true : false rescue false
          PartnerRewardCategory.create(partner_reward_id: partner.id, category_id: category[1]["category_id"], mandatory: mandatory)
        end if params[:partner_reward_category]
        params[:partner_reward_discount].each do |discount|
          mandatory = discount[1]["mandatory"] == "true" ? true : false rescue false
          PartnerRewardDiscount.create(partner_reward_id: partner.id, category_id: discount[1]["category_id"], mandatory: mandatory)
        end if params[:partner_reward_discount]
        format.js {}
        format.html { redirect_to admin_partner_reward_path(@reward), notice: 'Reward was successfully created.' }
      else
        p "#{@reward.errors.full_messages}"
        format.html { render action: "new", :alert => @reward.errors.full_messages }
      end
    end
  end

  def edit
    @reward = Reward.find(params[:id])
    @chain = @reward.chain
    @partner_hours = @reward.partner_reward.partner_hours rescue []
    @partner_reward_categories = @reward.partner_reward.partner_reward_categories rescue []
    @partner_reward_discounts = @reward.partner_reward.partner_reward_discounts rescue []
    @restaurants = @chain.restaurants.order("name ASC") if @reward.partner_reward && @reward.partner_reward.try(:valid_all_locations) == false
  end

  def update
    rescue_from_cancan(:update, Reward)
    @reward = Reward.find(params[:id])
    @chain = @reward.chain
    partner = @reward.partner_reward
    if partner.valid_all_locations
      before_save_locations = @chain.restaurants.map(&:id) rescue []
    else
      before_save_locations = @reward.partner_reward.valid_locations.parameterize.split("-") rescue []
    end

    params[:reward][:reward_type] = @reward.reward_type
    respond_to do |format|
      if @reward.update_attributes(params[:reward])
        res_ids = params[:partner_reward][:valid_locations]
        params[:partner_reward].delete("valid_locations")
        all_location = (params[:partner_reward][:valid_all_locations] || false rescue false)
        partner.valid_locations = "{#{res_ids.join(",") rescue nil}}"
        partner.valid_all_locations = all_location
        partner.update_attributes(params[:partner_reward])
        partner.save

        params[:partner_hours].each do |partner_hour|
          hour = partner_hour[1]
          if hour["start"].present? && hour["end"].present?
            ph = PartnerHour.find_or_initialize_by_partner_reward_id_and_day_of_week(partner.id, hour["day_of_week"])
            ph.day_of_week =  hour["day_of_week"]
            ph.start =  hour["start"]
            ph.end =  hour["end"]
            ph.save
          else
            partner.partner_hours.where(day_of_week: hour["day_of_week"]).first.delete rescue nil
          end
        end
        params_category_ids = []
        params[:partner_reward_category].each do |category|
          mandatory = category[1]["mandatory"] == "true" ? true : false rescue false
          pc = PartnerRewardCategory.find_or_initialize_by_partner_reward_id_and_category_id(partner.id, category[1]["category_id"])
          pc.mandatory = mandatory
          params_category_ids << (category[1]["category_id"].to_i rescue nil) if pc.save
        end if params[:partner_reward_category]

        partner_reward_category_ids = partner.partner_reward_categories.collect(&:category_id)
        partner_reward_category_ids - params_category_ids

        category_delete = partner_reward_category_ids - params_category_ids rescue nil
        category_delete.each do |pc|
          PartnerRewardCategory.where("partner_reward_id = #{partner.id} AND category_id =#{pc}").first.delete rescue nil
        end if category_delete.present?

        params_discount_ids = []
        params[:partner_reward_discount].each do |disc|
          mandatory = disc[1]["mandatory"] == "true" ? true : false rescue false
          pc = PartnerRewardDiscount.find_or_initialize_by_partner_reward_id_and_category_id(partner.id, disc[1]["category_id"])
          pc.mandatory = mandatory
          params_discount_ids << (disc[1]["category_id"].to_i rescue nil) if pc.save
        end if params[:partner_reward_discount]

        partner_reward_discount_ids = partner.partner_reward_discounts.collect(&:category_id)
        discount_delete = partner_reward_discount_ids - params_discount_ids
        discount_delete.each do |pc|
          PartnerRewardDiscount.where("partner_reward_id = #{partner.id} AND category_id =#{pc}").first.delete rescue nil
        end if discount_delete.present?

        format.html { redirect_to admin_partner_reward_path(@reward), notice: 'Reward was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def show
    rescue_from_cancan(:show, Reward)
    @reward = Reward.find(params[:id])
    @chain = @reward.chain
    @participants = @reward.restaurants.order("name ASC")
    ids = @participants.collect(&:id)
    @non_participants = Restaurant.where('chain_id = ? AND NOT id in (?)', @chain.id, ids.empty? ? '-111' : ids).order("name ASC")
    respond_to do |format|
      format.js
      format.html
    end
  end

  def destroy
    rescue_from_cancan(:destroy, Reward)
    @reward= Reward.find(params[:id])
    if @reward.status == "active" #or @reward.restaurant_rewards.count == 0
      @reward.update_attribute(:status, "inactive")
      message = "Reward successfuly deactived"
    else
      @reward.update_attribute(:status, "active")
      message = "Reward successfuly actived"
    end
    redirect_to admin_partner_rewards_path, notice: message
  end

  def search
    p params
    @rewards = []
    @reward = Reward.new
    unless params["chain_id"].blank?
      @chain = Chain.find(params["chain_id"])
      psearch_is_blank = params["search"].map{|x,v| v}.join.gsub(" ","").blank? rescue true
      @page = params[:page].blank? ? 1 : params[:page]
      if psearch_is_blank && params[:qry_order].blank?
        @rewards = @chain.rewards.where("reward_type = #{Reward::TYPES["PARTNER"]}").includes(:chain).order("updated_at DESC").paginate(page: @page, per_page: Setting.pagination.per_page)
      else
        if params["search"]["reward_type"].kind_of?(String)
          params["search"]["reward_type"] = params["search"]["reward_type"].split(",").map{|x| x.to_i}
        end
        @effective_date = params["search"]["from"]
        @expiry_date = params["search"]["to"]
        @search_query = params["search"]["q"]
        @reward_select_type = params["search"]["reward_type"]

        ar_field = [
            "rewards.updated_at",
            "rewards.id",
            "chains.name",
            "rewards.name",
            '"effectiveDate"',
            '"expiryDate"'
        ]

        order = params[:qry_order]
        @order = order.to_i

        if order.to_i < 1000
          qry_order = "#{ar_field[order.to_i]}"
          @rewards = @chain.rewards.where("reward_type = #{Reward::TYPES["PARTNER"]}").includes(:chain).order(qry_order).reverse_order.search(params[:search]).paginate(page: @page, per_page: Setting.pagination.per_page)
        elsif order.to_i > 999
          order = order.to_i - 1000
          qry_order = "#{ar_field[order]}"
          @rewards = @chain.rewards.where("reward_type = #{Reward::TYPES["PARTNER"]}").includes(:chain).order(qry_order).search(params[:search]).paginate(page: @page, per_page: Setting.pagination.per_page)
        end
      end
    end
  end

  def load_restaurants
    chain = Chain.find(params["chain_id"])
    @restaurants = chain.restaurants.order("name ASC")
    @reward = Reward.find(params[:reward_id]) rescue nil
  end

  def load_categories
    @chain = Chain.find(params["chain_id"])
    @type = params[:type]
    if params[:restaurant_id].present?
      partner_categories = []
      params[:restaurant_id].each do |res_id|
        rest_categories = PartnerSubCategory.includes(:restaurant_categories).where("partner_sub_categories.chain_id = #{@chain.id}  and restaurant_categories.restaurant_id = #{res_id}")
        rest_categories.each do |category|
          partner_categories << category.partner_category
        end
      end

      @partner_categories = partner_categories.uniq.flatten.paginate(page: params[:page], per_page: Setting.pagination.per_page)
    else
      @partner_categories = @chain.partner_categories.paginate(page: params[:page], per_page: Setting.pagination.per_page)
    end
  end

end
