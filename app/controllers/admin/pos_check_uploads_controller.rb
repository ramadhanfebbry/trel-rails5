class Admin::PosCheckUploadsController <  Admin::BaseController

  def index
    conditions = []
    conditions << "pos_check_uploads.chain_id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "pos_locations.restaurant_id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    if conditions.blank?
      #@pos_check_uploads = PosCheckUpload.order("id DESC").paginate(:page => params[:page], :per_page => 10)
      @pos_check_uploads = ActiveRecordExtensions::PosCheckUpload.order('id desc').select('id,pos_location_id,chain_id,created_at,
                           updated_at,barcode, check_id,status,sequence_number, user_code, user_id, error_code,
                           service, payment_included, code_type, old_barcode,
                           is_xpient_check, pos_used_type, check_state, check_creator_type, pos_check_uploads.restaurant_id').
          paginate(:page => 1, :per_page => 10,
                   :total_entries => -1
      )

    else
      @pos_check_uploads = ActiveRecordExtensions::PosCheckUpload.joins("LEFT JOIN pos_locations ON pos_check_uploads.pos_location_id = pos_locations.id").
          where(conditions.join(" AND ")).order("pos_check_uploads.id desc").select('pos_check_uploads.id,pos_location_id,pos_check_uploads.chain_id,pos_check_uploads.created_at,
 pos_check_uploads.updated_at,barcode, check_id,status,sequence_number, user_code, user_id, error_code, service, payment_included, code_type, old_barcode,
is_xpient_check, pos_used_type, check_state, check_creator_type, pos_check_uploads.restaurant_id').
          paginate(:page => params[:page], :per_page => 10, :total_entries => -1)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @pos_check_upload = ActiveRecordExtensions::PosCheckUpload.find params[:id]
    respond_to do |format|
      format.html
      format.js
    end
  end

  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : (Restaurant.where("chain_id IN (?)", params[:chains]).order("name asc") rescue [])
  end

  def search
    conditions = []
    conditions << "pos_check_uploads.chain_id = (#{params[:search][:chains]})" if params[:search].present? && params[:search][:chains].present?
    conditions << "pos_locations.restaurant_id = (#{params[:search][:restaurant_id]})" if params[:search].present? && params[:search][:restaurant_id].present?
    conditions << "pos_check_uploads.code_type = '#{params[:code_type]}'" unless params[:code_type].blank?
    conditions << "pos_check_uploads.barcode = '#{params[:barcode]}'" unless params[:barcode].blank?
    conditions << "users.email = '#{params[:user_email]}'" unless params[:user_email].blank?
    conditions << "pos_check_uploads.check_id = '#{params[:check_id]}'" unless params[:check_id].blank?
    conditions << "pos_check_uploads.sequence_number = '#{params[:sequence_number]}'" unless params[:sequence_number].blank?
    conditions << "pos_check_uploads.status = (#{params[:status]})" unless params[:status].blank?
    conditions << "pos_check_uploads.service = (#{params[:service]})" unless params[:service].blank?
    conditions << "pos_check_uploads.payment_included = (#{params[:payment_included]})" unless params[:payment_included].blank?
    conditions << "pos_check_uploads.pos_used_type = (#{params[:pos_used_type]})" unless params[:pos_used_type].blank?
    conditions << "pos_check_uploads.created_at >= '#{date_string_from_time_zone_to_utc(params[:date_from])}'" unless params[:date_from].blank?
    conditions << "pos_check_uploads.created_at <= '#{date_string_from_time_zone_to_utc("#{params[:date_to]} 23:59:59")}'" unless params[:date_to].blank?
    if conditions.blank?
      @pos_check_uploads = PosCheckUpload.order('id desc').select('id,pos_location_id,chain_id,created_at,
                           updated_at,barcode, check_id,status,sequence_number, user_code, user_id, error_code,
                           service, payment_included, code_type, old_barcode,
                           is_xpient_check, pos_used_type, check_state, check_creator_type, pos_check_uploads.restaurant_id').
          paginate(:page => params[:page], :per_page => 10,
                   :total_entries => -1
      )

    else
      @pos_check_uploads = PosCheckUpload.joins("LEFT JOIN pos_locations ON pos_check_uploads.pos_location_id = pos_locations.id
                                                 LEFT JOIN users ON pos_check_uploads.user_id = users.id").
          where(conditions.join(" AND ")).order("pos_check_uploads.id desc").select('pos_check_uploads.id,pos_location_id,pos_check_uploads.chain_id,pos_check_uploads.created_at,
 pos_check_uploads.updated_at,barcode, check_id,status,sequence_number, user_code, user_id, users.email as user_email, error_code, service, payment_included, code_type, old_barcode,
is_xpient_check, pos_used_type, check_state, check_creator_type, pos_check_uploads.restaurant_id').
          paginate(:page => params[:page], :per_page => 10, :total_entries => -1)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end


end
