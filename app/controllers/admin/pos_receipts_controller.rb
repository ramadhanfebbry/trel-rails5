class Admin::PosReceiptsController < Admin::BaseController

  def index
    @pos_receipts = PosReceipt.paginate(:per_page => 20, :page => params[:page])
  end

  def show
    @pos_receipt = PosReceipt.find(params[:id])
  end

  def destroy
    @pos_receipt = PosReceipt.find(params[:id])
    @pos_receipt.destroy
    redirect_to admin_pos_receipts_path, :notice => "Pos Receipt has been deleted."
  end
end
