class Admin::RotatingGroupsController < Admin::BaseController

  def index
    @rotating_groups = []
  end

  def search_by_chain
    @rotating_groups = RotatingGroup.where(:chain_id => params[:id]).paginate :page => params[:page], :per_page => 10
    respond_to do |format|
      format.js
      format.html { render :template => "admin/rotating_groups/index"}
    end

  end

  def new
    @chain = Chain.find(params[:chain_id])
    @rotating_group = RotatingGroup.new
  end

  def create
    @chain = Chain.find(params[:rotating_group][:chain_id])
    @rotating_group = RotatingGroup.new(params[:rotating_group])
    if @rotating_group.save
      redirect_to search_by_chain_admin_rotating_group_path(@chain), :notice => "New rotating group has created."
    else
      render :new
    end
  end

  def edit
    @rotating_group = RotatingGroup.find params[:id]
  end

  def update
    @rotating_group = RotatingGroup.find params[:id]
    if @rotating_group.update_attributes(params[:rotating_group])
      redirect_to search_by_chain_admin_rotating_group_path(@rotating_group.chain), :notice => "Rotating group has updated."
    else
      render :edit
    end
  end

  def destroy
    @rotating_group = RotatingGroup.find params[:id]
    @rotating_group.destroy
    redirect_to admin_rotating_groups_path, :notice => "Rotating group has destroyed."
  end


end