class Admin::PushRewardsController < Admin::BaseController

  def index
    @push_rewards = PushReward.order("created_at desc").paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @push_rewards }
      format.js
    end
  end

  def show
    @push_reward = PushReward.find(params[:id])
    @user_ids = @push_reward.user_ids[:user_ids].paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push_reward }
    end
  end

  def new
    @push_reward = PushReward.new
    @push_reward.all_user = false
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @push_reward }
    end
  end

  def edit
    @push_reward = PushReward.find(params[:id])
  end

  def all_user
    @push_reward = PushReward.new
    @push_reward.all_user = true
  end

  def create
    @push_reward = PushReward.new(params[:push_reward])
    @push_reward.pusher_id = current_admin.id
    confirm = false if params['confirm'].blank?
    confirm = true unless params['confirm'].blank?
    case params[:push_reward][:push_type].to_i
      when 1
        unless params[:push_reward][:xls].blank?
          begin
            user_ids = []
            book = Spreadsheet.open "#{params[:push_reward][:xls].path}"
            sheet1 = book.worksheet 0
            sheet1.column(0).each do |row|
              user_ids << row.to_s.strip.to_i if !row.to_s.include?(" ")
            end
            @duplicate_count = user_ids.length - user_ids.compact.uniq.length
            user_ids = user_ids.compact.uniq
            @uniq_count =  user_ids.length
                error = false
            err_msg = ""


            if user_ids.size == 0
              err_msg = "There are No Content on the xls files"
              error = true
            end

            if user_ids.size > 0
              user_ids.each do |uid|
                if uid != 0 and uid.to_i == 0
                  error = true
                  err_msg = "Only User ids content are valid in the xls files"
                  break
                end
              end
            end

            return redirect_to new_admin_push_reward_path, :alert => err_msg if error == true
            ### save it on redis first for confirmation purpose.
            REDIS.set "push_reward_#{current_admin.id}", user_ids.to_json
            puts "saving redis"
            @push_reward.user_ids = {:user_ids => user_ids}
            @push_reward.all_user = false
            #@push_reward.save!
            is_valid = @push_reward.valid?
          rescue => e
            puts "PushRewardsController:create #{e.inspect}"
            @push_reward.errors.add(:xls, "Please upload the xls file" )
          end
        else ## it means confirmations is == true
          puts "masuk sini boiiiiii"
            unless params['confirm'].blank?
              puts "masuk sini boiiiiii2"
              @push_reward.xls = true
              is_valid = @push_reward.valid?
            end
        end
      when 2
        user_ids = []
        user_ids = params[:push_reward][:user_temp_ids]
        user_ids = user_ids.squish.gsub("\\r",'').gsub('\\n','').gsub(' ','').split(',')

        @duplicate_count = user_ids.length - user_ids.compact.uniq.length
        user_ids = user_ids.compact.uniq
        @uniq_count =  user_ids.length
        @push_reward.user_ids = {:user_ids => user_ids}
        @push_reward.all_user = false
        is_valid = @push_reward.valid?
      when 3
        #user_ids = User.where("chain_id = ?", params[:push_reward][:chain_id]).select('id').map(&:id)
        # count_email = User.where("chain_id = ?", params[:push_reward][:chain_id]).count
        # loop = (count_email.to_f / 4000.to_f).ceil
        user_ids = []
        # 1.upto(loop) do |page|
        #   user_ids += User.where("chain_id = ?", params[:push_reward][:chain_id]).select('id').paginate(page: page, per_page: 4000).map(&:id)
        # end
        @push_reward.user_ids = {:user_ids => user_ids}
        @push_reward.is_all_user = true
        ### save it on redis first for confirmation purpose.
        REDIS.set "push_reward_#{current_admin.id}", user_ids.to_json
        params['confirm'] = "true"
        is_valid  =  verify_recaptcha(:private_key => ENV['RECAPTCHA_PRIVATE_KEY'],
                                      :model => @push_reward, :message => "Oh! It's error with reCAPTCHA!") && @push_reward.valid?
    end
    if is_valid and params['confirm'] == "true"
      if @push_reward.push_type.eql?(1)
        u_ids = JSON.parse(REDIS.get "push_reward_#{current_admin.id}")
        @push_reward.user_ids = {:user_ids => u_ids}
      end

      diff_hour = 5
      diff_hour = 4 if params['time_zone_default'].to_s == "EDT"
      @push_reward.executed_at = nil if params['scheduler'].to_s != "on"
      @push_reward.executed_at = @push_reward.executed_at - diff_hour.hours if params['scheduler'] == "on"
      @push_reward.save
      @push_reward.delay(:delayable_type => @push_reward.class.to_s, :delayable_id => @push_reward.id).send_push_reward_job(current_admin)
      #x.update_column(:run_at,nil)
      redirect_to admin_push_rewards_path, notice: 'Push reward was successfully created.'
    else
      if confirm == false and is_valid == true
        @confirmation_box = true
      end
      render :new
    end
  end

  def push_reward_new
    @params_reward_id = params['reward_id']
    if params['reward_id'].blank?
      @reward = Reward.new
    else
      @reward = Reward.find params['reward_id']   rescue "not found"
    end
    render :layout =>  false
  end

  def update
    @push_reward = PushReward.find(params[:id])
    @push_reward.push_type = params[:push_reward][:push_type]
    @push_reward.pusher_id = current_admin.id

    case params[:push_reward][:push_type].to_i
      when 1
        unless params[:push_reward][:xls].blank?
         # begin
            user_ids = []
            book = Spreadsheet.open "#{params[:push_reward][:xls].path}"
            sheet1 = book.worksheet 0
            sheet1.column(0).each do |row|
              user_ids << row.to_i
            end
            user_ids = user_ids.compact.uniq

            error = false
            err_msg = ""

            if user_ids.size == 0
              err_msg = "There are No Content on the xls files"
              error = true
            end

            if user_ids.size > 0
              user_ids.each do |uid|
                if uid.to_i == 0
                  error = true
                  err_msg = "Only User ids content are valid in the xls files"
                  break
                end
              end
            end

            return redirect_to new_admin_push_reward_path, :alert => err_msg if error == true
          #rescue => e
           # puts "PushRewardsController:create #{e.inspect}"
           # @push_reward.errors.add(:xls, "Please upload the xls file" )
         # end
        end
      when 2
        user_ids = []
        user_ids = params[:push_reward][:user_temp_ids]
        user_ids = user_ids.squish.gsub("\\r",'').gsub('\\n','').gsub(' ','').split(',')
      when 3
        yyyy
        #user_ids = User.where("chain_id = ?", params[:push_reward][:chain_id]).select('id').map(&:id)
        count_email = User.where("chain_id = ?", params[:push_reward][:chain_id]).count
        loop = (count_email.to_f / 4000.to_f).ceil
        user_ids = []
        1.upto(loop) do |page|
          user_ids += User.where("chain_id = ?", params[:push_reward][:chain_id]).select('id').paginate(page: page, per_page: 4000).map(&:id)
        end
    end
    @push_reward.user_ids = user_ids
    @push_reward.user_temp_ids = user_ids
    puts "bainurr budiyana #{@push_reward.valid?}"
    if @push_reward.save! #@push_reward.valid?

      #@push_reward.delay.send_push_reward_job(current_admin)
      redirect_to admin_push_rewards_path, notice: 'Push reward was successfully updated.'
    else
      render :new
    end
  end

  def summary_success
    @push_reward = PushReward.find params[:id]
    @logs = @push_reward.success_log_entries.select{|a| a[(params[:search_type] || "user_id" ).to_sym].to_s =~ /#{params[:key]}/}.paginate(:page => params[:page], :per_page => 30)
    respond_to do |format|
      format.html
      format.js
      format.csv {Delayed::Job.enqueue(
          ExportCsvJob::SummarySuccessReward.new(
              params,
              current_admin),
          :delayable_type => "export_point_point_#{@push_reward.id}")
      redirect_to summary_success_admin_push_reward_path(@push_reward), :notice => "Files will be sent to your email shortly.."
      }
    end
  end

  def summary_failed
    @push_reward = PushReward.find params[:id]
    @logs = @push_reward.error_log_entries.select{|a| a[(params[:search_type] || "user_id" ).to_sym].to_s =~ /#{params[:key]}/}.paginate(:page => params[:page], :per_page => 30)
  end

  def filter_push_rewards
    query_conditions = []
    query_conditions << "lower(rewards.name) like '%#{params[:reward_name].downcase}%'" if params[:reward_name].present?
    query_conditions << "push_rewards.chain_id = '#{params[:chn_id]}'" if params[:chn_id].present?
    query_conditions << "push_rewards.reward_id = '#{params[:reward_id]}'" if params[:reward_id].present?
    params[:page] = 1 if params[:page].blank?

    @push_rewards = PushReward.includes(:reward).where(query_conditions.join(" AND ")).paginate(page: params[:page], per_page: 10)

  end

  def execute_again
    @push_reward = PushReward.find params[:id]
    @push_reward.delay(delayable_type: 'PushReward', delayable_id: @push_reward.id).send_push_reward_job(current_admin)

    redirect_to admin_push_rewards_path, notice: 'Executing Push reward.'
  end


  def destroy
    @push_reward = PushReward.find(params[:id])
    @push_reward.destroy

    respond_to do |format|
      format.html { redirect_to admin_push_rewards_path }
      format.json { head :ok }
    end
  end
end
