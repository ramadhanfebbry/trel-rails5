class Admin::ApplicationKeysController < Admin::BaseController

  cache_sweeper :application_key_sweeper, :only => [ :create, :update, :destroy ]

  before_action :get_application

  # GET /admin/application_keys
  # GET /admin/application_keys.json
  def index
    @application_keys = @application.application_keys

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @application_keys }
    end
  end

  # GET /admin/application_keys/1
  # GET /admin/application_keys/1.json
  def show
    @application_key = @application.application_keys.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @application_key }
    end
  end

  # GET /admin/application_keys/new
  # GET /admin/application_keys/new.json
  def new
    @application_key = @application.application_keys.new
    @application_key.build_application_key_detail

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @application_key }
    end
  end

  # GET /admin/application_keys/1/edit
  def edit
    @application_key = @application.application_keys.find(params[:id])
    @application_key.build_application_key_detail unless @application_key.application_key_detail
  end

  # POST /admin/application_keys
  # POST /admin/application_keys.json
  def create
    @application_key = @application.application_keys.new(applicatin_key_params)

    respond_to do |format|
      if @application_key.save
        format.html { redirect_to admin_application_application_keys_path(@application), notice: 'Application key was successfully created.' }
        format.json { render json: @application_key, status: :created, location: @application_key }
      else
        format.html { render action: "new" }
        format.json { render json: @application_key.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/application_keys/1
  # PUT /admin/application_keys/1.json
  def update
    @application_key = @application.application_keys.find(params[:id])

    respond_to do |format|
      if @application_key.update_attributes(params[:application_key])
        format.html { redirect_to admin_application_application_keys_path(@application), notice: 'Application key was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @application_key.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/application_keys/1
  # DELETE /admin/application_keys/1.json
  def destroy
    @application_key = @application.application_keys.find(params[:id])
    @application_key.destroy

    respond_to do |format|
      format.html { redirect_to admin_application_application_keys_url(@application) }
      format.json { head :ok }
    end
  end

  def update_status
    rescue_from_cancan(:destroy, ApplicationKey)
    @application_key = ApplicationKey.find(params[:id])
    case params[:status]
    when APPLICATION_KEY_STATUS[:ACTIVE]
      @application_key.send("activate")
    when APPLICATION_KEY_STATUS[:INACTIVE]
      @application_key.send("deactivate")
    when APPLICATION_KEY_STATUS[:DISCONTINUED]
      @application_key.send("discontinue")
    when APPLICATION_KEY_STATUS[:DISABLED]
      @application_key.send("disable")
    end
    @application_key.save
    respond_to do |format|
      format.html { redirect_to admin_application_application_keys_url(@application_key.application), notice: 'Application key status was successfully updated.' }
    end
  end

  def set_newest_version
    rescue_from_cancan(:destroy, ApplicationKey)
    @application = Application.find(params[:application_id])
    @application_key = ApplicationKey.find(params[:id])
    @application.application_keys.update_all(is_newest: false)
    @application_key.update_attribute(:is_newest, params[:newest] )
    respond_to do |format|
      format.html { redirect_to admin_application_application_keys_url(@application), notice: 'Application key newest was successfully updated.' }
    end
  end

  protected

  def get_application
    @application = Application.find(params[:application_id])
  end

  def applicatin_key_params
    params.require(:application_key).permit(:appkey, :application_id, :version, :ocr_send_message, :show_new_question_types, :show_new_question_slider, :show_new_question_multiple_dropdown, :show_gifter, :show_expired_reward, :show_expired_reward_activity_listing,
      application_key_detail_attributes: [:is_required])
  end
end
