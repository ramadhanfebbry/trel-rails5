class Admin::PerksController < Admin::BaseController
  before_action :find_perk, :only => [:destroy, :edit, :show,:update]

  def index
    rescue_from_cancan(:index, Perk)
    @perks = Perk.paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  def new
    rescue_from_cancan(:index, Perk)
    @perk = Perk.new
    @chains = Chain.all
  end

  def create    
    @perk = Perk.new(params[:perk])
    unless params[:restaurant_ids].blank?
      params[:restaurant_ids].each do |res|
        @perk.perk_restaurants << PerkRestaurant.new(:restaurant_id => res)
      end
    end
    
    respond_to do |format|
      if @perk.save
        format.html { redirect_to admin_perks_path, notice: 'Perk was successfully created.' }
        format.json { render json: @perk, status: :created }
      else        
        format.html { render action: "new" }
        format.json { render json: @perk.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    unless @perk.perk_restaurants.blank?
      @restaurants = Restaurant.by_ids(@perk.perk_restaurants.map(&:restaurant_id))
      #@chain = @restaurants.first.chain.id
    end
  end

  def show
  end

  def update
    unless params[:restaurant_ids].blank?
      params[:restaurant_ids].each do |res|
        @perk.perk_restaurants << PerkRestaurant.new(:restaurant_id => res)
      end
    end
    respond_to do |format|
      if @perk.update_attributes(params[:perk])
        format.html { redirect_to admin_perks_path, notice: 'Perk was successfully updated.' }
        format.json { render json: @perk, status: :destroy }
      else
        format.html { render action: "edit" }
        format.json { render json: @perk.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @perk.destroy
        format.html { redirect_to admin_perks_path, notice: 'Perk was successfully destroyed.' }
        format.json { render json: @perk, status: :destroy }
      else
        format.html { render action: "new" }
        format.json { render json: @perk.errors, status: :unprocessable_entity }
      end
    end
  end

  protected

  def find_perk
    @perk = Perk.find params[:id]
  end
end
