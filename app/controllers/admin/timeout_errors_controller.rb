class Admin::TimeoutErrorsController <  Admin::BaseController
# GET /admin/timeout_errors
# GET /admin/timeout_errors.json
  layout 'admin'
  def index
    @admin_timeout_errors = TimeoutTrace.order('created_at desc').paginate(:page => params[:page], :per_page => 10)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_timeout_errors }
    end
  end
# GET /admin/timeout_errors/1
# GET /admin/timeout_errors/1.json
  def show
    @admin_timeout_error = TimeoutTrace.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @admin_timeout_error }
    end
  end
# GET /admin/timeout_errors/new
# GET /admin/timeout_errors/new.json
  def new
    @admin_timeout_error = TimeoutTrace.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @admin_timeout_error }
    end
  end
# GET /admin/timeout_errors/1/edit
  def edit
    @admin_timeout_error = TimeoutTrace.find(params[:id])
  end
# POST /admin/timeout_errors
# POST /admin/timeout_errors.json
  def create
    @admin_timeout_error = TimeoutTrace.new(params[:admin_timeout_error])
    respond_to do |format|
      if @admin_timeout_error.save
        format.html { redirect_to @admin_timeout_error, notice: 'Timeout error was successfully created.' }
        format.json { render json: @admin_timeout_error, status: :created, location: @admin_timeout_error }
      else
        format.html { render action: "new" }
        format.json { render json: @admin_timeout_error.errors, status: :unprocessable_entity }
      end
    end
  end
# PUT /admin/timeout_errors/1
# PUT /admin/timeout_errors/1.json
  def update
    @admin_timeout_error = TimeoutTrace.find(params[:id])
    respond_to do |format|
      if @admin_timeout_error.update_attributes(params[:admin_timeout_error])
        format.html { redirect_to @admin_timeout_error, notice: 'Timeout error was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @admin_timeout_error.errors, status: :unprocessable_entity }
      end
    end
  end
# DELETE /admin/timeout_errors/1
# DELETE /admin/timeout_errors/1.json
  def destroy
    @admin_timeout_error = TimeoutTrace.find(params[:id])
    @admin_timeout_error.destroy
    respond_to do |format|
      format.html { redirect_to admin_timeout_errors_url }
      format.json { head :ok }
    end
  end
end