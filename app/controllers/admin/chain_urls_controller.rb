class Admin::ChainUrlsController < Admin::BaseController

  before_action :find_chain

  def index
    @chain_urls = @chain.chain_urls
  end

  def new
    @chain_url = @chain.chain_urls.new

  end

  def create
    @chain_url = @chain.chain_urls.new(chain_url_params)
    if @chain_url.save
      redirect_to admin_chain_chain_urls_path(@chain), :notice => "Chain URL has created"
    else
      render :new
    end
  end

  def edit
    @chain_url = @chain.chain_urls.find(params[:id])
  end

  def update
    @chain_url = @chain.chain_urls.find(params[:id])
    if @chain_url.update(chain_url_params)
      redirect_to admin_chain_chain_urls_path(@chain), :notice => "Chain URL has updated"
    else
      render :edit
    end
  end

  def destroy
    @chain_url = @chain.chain_urls.find(params[:id])
    @chain_url.destroy
    redirect_to admin_chain_chain_urls_path(@chain), :notice => "Chain URL has destroyed"
  end

  protected
  def find_chain
    @chain = Chain.find(params[:chain_id])
  end

  def chains_params
    params.require(:chain).permit(:name, :url)
  end

  def chain_url_params
    params.require(:chain_url).permit(:name, :url, :chain_id)
  end
end
