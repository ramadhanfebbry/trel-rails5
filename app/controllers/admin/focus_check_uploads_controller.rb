class Admin::FocusCheckUploadsController < Admin::BaseController

  def index
    conditions = []
    conditions << "focus_check_uploads.chain_id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "pos_locations.restaurant_id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    if conditions.blank?
      @pos_check_uploads = FocusPos::CheckUpload.includes(:pos_location, :user).order('id desc').select('id,pos_location_id,chain_id,created_at,
                           updated_at, check_id,status, user_code, user_id, code_type').
          paginate(:page => params[:page], :per_page => 10,
                   :total_entries => -1
          )

    else
      @pos_check_uploads = FocusPos::CheckUpload.includes(:pos_location, :user).
          where(conditions.join(" AND ")).order("focus_check_uploads.id desc").select('id,pos_location_id,chain_id,created_at,
                           updated_at, check_id,status, user_code, user_id, code_type').
          paginate(:page => params[:page], :per_page => 10, :total_entries => -1)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @pos_check_upload = FocusPos::CheckUpload.find params[:id]
    respond_to do |format|
      format.html
      format.js
    end
  end

  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : Restaurant.where("chain_id IN (?)", params[:chains]).order('name asc') rescue []
  end

end
