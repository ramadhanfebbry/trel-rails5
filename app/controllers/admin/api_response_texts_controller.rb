class Admin::ApiResponseTextsController < Admin::BaseController

	before_action :find_chain

	def index
		@api_response_texts = @chain.api_response_texts
	end

	def new
		@api_response_text = @chain.api_response_texts.build
	end

	def edit
		@api_response_text = @chain.api_response_texts.find(params[:id])
	end

	def create
		@api_response_text = @chain.api_response_texts.build(api_response_text_params)
		if @api_response_text.save
			redirect_to admin_chain_api_response_texts_path, :notice => "Api Response Text has been created."
		else
			render :new
		end
	end

	def update_content
		@api_response_text = @chain.api_response_texts.find(params[:id])
		@is_save = @api_response_text.update(api_response_text_params)
		p @api_response_text.errors
	end

	def destroy
		@api_response_text = @chain.api_response_texts.find(params[:id])
		@api_response_text.destroy
		redirect_to admin_chain_api_response_texts_path, :notice => "Api Response Text has been deleted."
	end

	def show_form
		@type = params[:type]
		if !@chain.blank? && !@type.blank?
			@parameterize = @type.parameterize("_")+"_texts"
			p @chain.locales
			p @chain.send(@parameterize).map{|gl| gl.locale}
			(@chain.locales - (@chain.send(@parameterize).map{|gl| gl.locale}).uniq).each do |locale|
        @chain.send(@parameterize).build(:locale => locale, :message_for => ApiResponseText::MESSAGE_FOR[params[:type]])
      end
		end	
	end

	def set_to_default_message
		@api_response_text = @chain.api_response_texts.find(params[:id])
		locale = Locale.find(params[:locale])
		message_for = ApiResponseText::MESSAGE_FOR.invert[params[:message_for].to_i]
		@text = ApiResponseText::DEFAULT_MESSAGE[locale.key][message_for]
	end


	private

	def find_chain
		@chain = Chain.find params[:chain_id]		
	end

	def api_response_text_params
		params.required(:api_response_text).permit(
			:message_for, :locale_id, :message)
	end

end
