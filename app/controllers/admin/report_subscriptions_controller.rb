class Admin::ReportSubscriptionsController <  Admin::BaseController

  def index
    if params['chain_id'].blank?
      @report_subscriptions = ReportSubscription.includes(:report_definition).where("report_definitions.active = true").order("report_subscriptions.id desc").
          paginate(page: params[:page], per_page: 10)
    else
      @report_subscriptions = ReportSubscription.includes(:report_definition).where("report_definitions.active = true and report_subscriptions.chain_id = ?", params['chain_id']).order("report_subscriptions.id desc").
          paginate(page: params[:page], per_page: 10)
    end

  end

  def subscription_filename
    @subscription = ReportSubscription.find(params["id"])
    @subscription_name = @subscription.subscription_format

    if @subscription_name.blank?
      @subscription_name = SubscriptionFormat.new(:subscription_id => params[:id])
    end
  end

  def get_filename_subscription
    @subscription = ReportSubscription.find(params["id"])
    @subscription_name = @subscription.subscription_format
    render :layout => false
  end

  def post_subscrition_filename

    @subscription = ReportSubscription.find(params["id"])
    @subscription.subscription_format.delete rescue nil
    @subscription_name = SubscriptionFormat.new(params["subscription_format"])
    @subscription_name.subscription_id = @subscription.id

    p @subscription_name
    if @subscription_name.save
      redirect_to :back, :notice => "Filename for this Report are updated"
    else
      #xxx
      redirect_to :back, :alert => "Filename for this Report are not updated"
    end
  end

    def new
    @report_subscription = ReportSubscription.new
    chain_ids = ReportSubscription.all.map(&:chain_id).compact
    chain_ids = []

    chain_ids = 0 if chain_ids.blank?

    @chains = Chain.where("id not in(?)",chain_ids).map{|x| [x.name, x.id]}
    @report_definitions = ReportDefinition.all.map{|x| [x.name, x.id]}
  end

  def edit
    @report_subscription = ReportSubscription.find(params[:id])
    chain_ids = ReportSubscription.all.map(&:chain_id).compact

    chain_ids = 0 if chain_ids.blank?

    @chains = Chain.active#where("id not in(?)",chain_ids).map{|x| [x.name, x.id]}
    @report_definitions = ReportDefinition.all.map{|x| [x.name, x.id]}

  end


  def search_chain
    @report_subscription = ReportSubscription.find(params[:id]) rescue []
    chain = Chain.find params[:chain_id]
    @chain = chain
    @restaurants = chain.restaurants
    @owners = chain.owners
    @selected_owner = @report_subscription.owners.map(&:id) rescue []
  end

  def search_report_description
    @report_subscription = ReportSubscription.find(params[:id]) rescue ReportSubscription.new
    @report_description = ReportDefinition.find(params[:definition_id])
    @custom_params = @report_subscription.custom_params   rescue []
  end

  def report_settings
    @form = ReportSubscription::TYPES.key(params[:kind].to_i)
    @report_subscription = ReportSubscription.find(params[:id]) rescue []
  end

  def destroy
    @report_subscription = ReportSubscription.find(params[:id])

    if @report_subscription.destroy
      redirect_to admin_report_subscriptions_path, :notice => "subscription deleted"
    end
  end

  def test_report
    @report_subscription = ReportSubscription.find(params[:id])
  end

  def post_test_report
    @report_subscription = ReportSubscription.find(params["subscription_id"])
    emails = params[:emails].gsub(' ','').gsub(/\r/,"").gsub(/\n/,"").split(',')

    ReportJob::ReportGenerator.generate_report(
        @report_subscription.get_chain_id,
        @report_subscription.report_definition, @report_subscription, emails
    )

    redirect_to admin_report_subscriptions_path, :notice => "test email will be sent shortly., please wait"
  end

  def show
    @report_subscription = ReportSubscription.find(params[:id])
  end

  def create
    @report_definitions = ReportDefinition.all.map{|x| [x.name, x.id]}
    @report_subscription = ReportSubscription.new(params[:report_subscription])
    params[:query][:restaurant_ids] = params[:query][:restaurant_ids].join(',') if params[:query] and params[:query][:restaurant_ids]

    @report_subscription.custom_params = params[:query]
    @report_subscription.report_settings = params[:report_settings]
    @report_subscription.owners << Owner.where("id in (?)", params[:owner_select_id])

    if @report_subscription.save
       redirect_to admin_report_subscriptions_path, :notice => "report subscription has been saved"
    else
      @report_subscription = ReportSubscription.new
      chain_ids = ReportSubscription.all.map(&:chain_id).compact
      chain_ids = []

      chain_ids = 0 if chain_ids.blank?

      @chains = Chain.where("id not in(?)",chain_ids).map{|x| [x.name, x.id]}
      @report_definitions = ReportDefinition.all.map{|x| [x.name, x.id]}
       render :action => :new
    end
  end

  def update
    @report_subscription = ReportSubscription.find(params[:id])

    @report_subscription.custom_params = params[:query]
    @report_subscription.report_settings = params[:report_settings]
    @report_subscription.owners = Owner.where("id in (?)", params[:owner_select_id])

    if @report_subscription.update_attributes(params[:report_subscription]) and @report_subscription.save
      redirect_to admin_report_subscriptions_path, :notice => "report subscription has been saved"
    else
      render :action => :edit
    end
  end
end
