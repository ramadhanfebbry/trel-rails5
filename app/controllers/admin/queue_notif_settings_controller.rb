class Admin::QueueNotifSettingsController < Admin::BaseController

  def index
    @timer =  (REDIS.get('queue_notif_time') || 10).to_i
  end

  def create
    REDIS.set('queue_notif_time',params['queue_timer'].to_i)

    redirect_to :back, :notice => "Timer already updated"
  end

end
