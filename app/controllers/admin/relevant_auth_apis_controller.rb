class Admin::RelevantAuthApisController < Admin::BaseController

  def index
    @relevant_auths = RelevantAuth.order("id asc")
  end

  def new
    @relevant_auth = RelevantAuth.new
  end

  def create
    @relevant_auth = RelevantAuth.new(params[:relevant_auth])
    if @relevant_auth.save
      redirect_to admin_relevant_auth_apis_path, :notice => "Relevant Connect authentication has been successfully created."
    else
      render :new
    end
  end

  def edit
    @relevant_auth = RelevantAuth.find(params[:id])
  end

  def update
    @relevant_auth = RelevantAuth.find(params[:id])
    if @relevant_auth.update_attributes(params[:relevant_auth])
      redirect_to admin_relevant_auth_apis_path, :notice => "Relevant Connect authentication has been successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @relevant_auth = RelevantAuth.find(params[:id])
    @relevant_auth.destroy
    redirect_to admin_relevant_auth_apis_path, :notice => "Relevant Connect authentication has been successfully deleted."
  end

  def inactive
    @relevant_auth = RelevantAuth.find(params[:id])
    @relevant_auth.update_column(:status, RelevantAuth::STATE[:INACTIVE])
    redirect_to admin_relevant_auth_apis_path, :notice => "Relevant Connect authentication has been successfully inactived."
  end

  def active
    @relevant_auth = RelevantAuth.find(params[:id])
    @relevant_auth.update_column(:status, RelevantAuth::STATE[:ACTIVE])
    redirect_to admin_relevant_auth_apis_path, :notice => "Relevant Connect authentication has been successfully actived."
  end
end
