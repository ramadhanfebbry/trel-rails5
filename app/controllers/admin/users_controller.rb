class Admin::UsersController < Admin::BaseController
  # GET /users
  def index
    @users = []
    unless @ability.can? :index, User
      redirect_to "/", :alert => "You don't have access that page." and return
    else
      #@users = User.active_chain

      if !request.format.csv?
        # @users = @users.paginate(page: params[:page], per_page: Setting.pagination.per_page) #user.all
        @user = []
      end

      respond_to do |format|
        format.html
        format.js
        format.csv { render :layout => false }
      end
    end
  end

  def miss_you_date
    @user = User.find params[:id]
    @rests = RestaurantUser.where(:user_id => @user.id)
    unless @rests.blank?
      @last_active = @rests.map(&:last_active).compact.sort.last

    end
    @miss_date = @user.miss_you_date
  end

  def update_signup
    @user = User.find params[:id]
    @user.update_column(:created_at, params["signup_date"])
    @user.update_column(:no_scan_date, params["no_scan_date"].blank? ? nil : params["no_scan_date"])

    @rests = RestaurantUser.where(:user_id => @user.id)
    unless @rests.blank?
      @last_active = @rests.map(&:last_active).compact.sort.last

    end
    @miss_date = @user.miss_you_date

    redirect_to :back, :notice => "User signup,  no scan Reward / point date date Has been updated"
  end

  def update_miss_you
    @user = User.find params[:id]
    miss_date = params[:miss_you_date]
    miss_date = nil  if params[:miss_you_date].blank?

    last_act = params[:last_active]
    last_act = nil if params[:last_active].blank?

    res_users = RestaurantUser.where(:user_id => @user.id)
    res_users.each do |ru|
      ru.update_column(:last_active, last_act)
      ru.update_column(:last_redeem, last_act)
      ru.update_column(:miss_you_date, miss_date)
    end
    @user.update_column(:miss_you_date, miss_date)
    unless res_users.blank?
    redirect_to :back, :notice => "User #{@user.email} last active/ miis you date Has been updated"
    else
      redirect_to :back, :alert => "THIS USER HAS NO ACTIVITY YET! ( havent submit receipt )"
      end
  end

  def search
    if (params["search"].values - [""]).size > 0 or !params['flagqry'].blank?
      @users = User.select("id, email, chain_id, chain_app_key, client_set_status_label, client_set_status_value,
          favorite_menu_item, authentication_token, register_type, users.created_at, latitude, longitude, ref_code,
          sign_in_device_type, device_id, device_token, os_version, phone_model, phone_number, points, locale_id,
          total_points_earned, zipcode, first_name, last_name, point_threshold, signup_device_status, favorite_location,
          dob_year, dob_month, dob_day, active, push_email, push_device, marketing_optin, app_usage_purpose, gender, marketing_optin_texting").search(params[:search]).paginate(page: params[:page], per_page: Setting.pagination.per_page, :total_entries => -1)
    end

    @action = '-search'
    respond_to do |format|
      format.js { render :index }
      format.csv {
        @users = User.active_chain.search(params[:search])
        render :layout => false, :action => :index
      }
    end
  end

  def deal_participation
    @user = User.find params[:id]
    @user_incentives = @user.deal_participants
  end

  def show
    rescue_from_cancan(:show, User)
    @user = User.find(params[:id])
  end

  def new
    rescue_from_cancan(:new, User)
    @user = User.new
  end

  def earned_deal_points
    rescue_from_cancan(:earned_deal_points, User)
    @user = User.find params[:id]
    @user_incentives = UserIncentive.incentive_points_for_user(@user)
  end

  def update_notification_email
    @user = User.find params[:id]
    push_email = @user.push_email
    @user.update_attribute(:push_email, !push_email)
    redirect_to admin_users_path(:page => params[:page])
  end

  def update_notification_device
    @user = User.find params[:id]
    push_device = @user.push_device
    @user.update_attribute(:push_device, !push_device)
    redirect_to admin_users_path(:page => params[:page])
  end

  def update_marketing_optin
    @user   = User.find params[:id]
    marketing_optin   = @user.marketing_optin
    @user.update_attribute(:marketing_optin, !marketing_optin)
    redirect_to admin_users_path(:page => params[:page])
  end

  def offline_deal_reward
    rescue_from_cancan(:offline_deal_reward, User)
    @user = User.find params[:id]
    @user_incentives = UserIncentive.incentive_offline_reward_for_user(@user)
  end

  def show_perks
    rescue_from_cancan(:show_perks, User)
    @user = User.find(params[:id])
    @perks = []
    perk_wallets = PerkWallet.by_user_id(@user.id)
    unless perk_wallets.blank?
      @perks = Perk.by_ids(perk_wallets.map(&:perk_id))
    end
    render :layout => false
  end

  def show_rewards
    if request.format.js?
      @user = User.find params[:id]
      rewards = Reward.where(:chain_id => @user.chain.id, :status => "active")
      rewards_in_wallet = RewardWallet.includes(:reward).order("reward_wallets.created_at DESC").find_all_by_user_id(params[:id])
      @rewards = rewards
      @rewards_wallet = rewards_in_wallet - rewards_in_wallet.select { |x| x.reward.eql?(nil) }
      render :layout => false
    else
      render :nothing => true
    end
  end

  def update_wallet_status
    @wallet = RewardWallet.find(params["reward_wallet_id"])

    @wallet.update_attributes(params["reward_wallet"])
    render :nothing => true
  end

  def update_reward_status
    @reward = Reward.find(params["reward_id"])

    if params['activated'] && params['activated'] == "true"
      @reward.set_active
    elsif params['activated'] && params['activated'] == "false"
      @reward.set_expired
    end
    render :nothing => true
  end

  def available_rewards
    if request.format.js?
      @user = User.find params[:id]
      @chain = @user.chain
      chain_rewards= Reward.of_chain(@chain.id, only_active=true, only_in_effect=true, only_unexpired=false)
      user_rewards= Reward.of_user(@user.id, include_removed=false, include_claimed=false)

      rewards= chain_rewards | user_rewards #union both
      rewards.sort! do |a, b|
        begin
          if (a.expired.eql? b.expired)
            (a.points <=> b.points)
          else
            a.expired ? 1 : -1
          end
        rescue
          next
        end
      end

      rewards.each_with_index do |r, i|
        r.sort_by_id= i
      end
      @rewards = rewards

      @current_balance= @user.points
      render :layout => false
    else
      render :nothing => true
    end
  end

  def create
    rescue_from_cancan(:create, User)
    #render :text => params and return
    @user = User.new(params[:user])
    respond_to do |format|
      if @user.save
        #UserMailer.welcome_email(@user).deliver
        format.html { redirect_to admin_users_url, notice: 'user was successfully updated.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def edit
    rescue_from_cancan(:edit, User)
    @user = User.find(params[:id])
  end

  def update
    rescue_from_cancan(:update, User)
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update_attributes(params[:user])
        uf = UserFishbowl.select("id").where(:chain_id =>  @user.chain_id, :user_id => @user.id).count
        Delayed::Job.enqueue(UserMarketingUpdateJob.new(@user)) if uf > 0
        format.html { redirect_to admin_users_url, notice: 'user was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def activate_deactivate
    @user = User.find(params[:id])
    if @user.change_status
      redirect_to admin_users_path(:page => params[:page])
    end
  end

  def destroy
    rescue_from_cancan(:destroy, User)
    @user = User.find(params[:id])

    if (@user)
      @user.destroy
    end

    redirect_to admin_users_url
  end

  def deleted
    rescue_from_cancan(:deleted, User)
    @users = User.includes(:chain).unscoped.deleted.paginate(page: params[:page], per_page: 10)
  end

  def revive
    rescue_from_cancan(:revive, User)
    user = User.deleted.find(params[:id])
    user.revive
    respond_to do |format|
      format.html { redirect_to deleted_admin_users_url, notice: 'user was successfully revived.' }
    end

  end

  def chain_users
    chain = Chain.find params[:id]
    @users = chain.users.order("email asc") rescue []
    render :json => @users.map { |x| {:id => x.id, :full_name => x.full_name, :email => x.email, :blank_name => x.full_name.blank?} }
  end

  def chain_appkeys
    chain = Chain.find params[:id]
    application_ids = chain.applications.map(&:id)
    @application_keys = ApplicationKey.where("application_id IN (?)", application_ids).order("appkey asc") rescue []
    render :json => @application_keys.map { |x| {:appkey => x.appkey} }
  end

  def receipts
    @user = User.find(params[:id])
    @type = params[:type]
    conditions = []
    conditions << "receipts.user_id = #{@user.id}"
    if @type.eql?("barcode")
      conditions << "receipts.is_receipt_barcode IS TRUE"
    else
      conditions << "receipts.is_receipt_barcode IS FALSE AND receipts.is_online_order IS FALSE AND receipts.is_olo IS FALSE"
    end

    conditions = conditions.join(" AND ")
    @receipts = Receipt.where(conditions)
  end

  def receipts_online
    @user = User.find(params[:id])
    @type = params[:type]
    conditions = []
    conditions << "receipts.user_id = #{@user.id}"
    conditions << "(receipts.is_online_order IS TRUE OR receipts.is_olo IS TRUE)"

    conditions = conditions.join(" AND ")
    @receipts = Receipt.where(conditions)
  end

  def giftcards
    @user = User.find(params[:id])
    @chain = @user.chain
    @giftcard_transactions = Vantiv::TransactionLog.where(chain_id: @chain.id, user_id: @user.id).order("created_at DESC").paginate(page: params[:page], per_page: 10)
  end

  def point_histories
    @user = User.find(params[:id])
    @histories = PointHistory.where(:user_id => @user.id).order("created_at DESC")
  end

  def gift_card_redeem_histories
    @user = User.find(params[:id])
    @histories = GiftCardRedemHistory.where(:user_id => @user.id).order("created_at DESC")
  end

  def game_activity
    @user = User.find(params[:id])
    @activities = LevelsPoint.where(:user_id => @user.id).order("created_at DESC").paginate(page: params[:page], per_page: 10)
  end

  def show_accosiated_devices
    @user = User.find(params[:id])
    @devices = @user.top_5_logs
    render :layout => false
  end

  def payments
    @user = User.find(params[:id])
    @payments = PaymentHistory.includes(:user_session => :receipt).where(:user_id => @user.id, :status => 0, :action => "payment").order("created_at DESC").paginate(page: params[:page], per_page: 20)
  end

  def online_orders
    @user = User.find(params[:id])
    @online_orders = ::Onosys::OnlineOrder.where(:user_id => @user.id).order("id DESC").paginate(page: params[:page], per_page: 10)
  end

  def nuorder
    @user = User.find(params[:id])
    p @online_orders = ::Vext::OnlineOrder.where(:user_id => @user.id).order("id DESC").paginate(page: params[:page], per_page: 10)
     @online_orders.size
  end

  def confirm_email
    user = UserConfirmation.find_by_confirm_token(params[:id])
    if user
      user = User.find user.user_id
      user.email_activate
      return render :json => {:status => true, :message => "Your accout has activate, you should be able to login"}
    else
      return render :json => {:status => false, :message => "Something wrong with your activation key"}
    end
  end
end
