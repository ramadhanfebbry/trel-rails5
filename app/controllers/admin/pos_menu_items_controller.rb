class Admin::PosMenuItemsController < Admin::BaseController

  def index
    conditions = []
    pos_location_ids = []
    unless params[:chains].blank?
      conditions << "chains.id IN (#{params[:chains].join(",")})"
      params[:chains].each do |chain_id|
        pl = PosLocation.where(:chain_id => chain_id).first
        pos_location_ids << pl.id if pl
      end
    else
      Chain.select("id").each do |chain|
        pl = PosLocation.where(:chain_id => chain.id).first
        pos_location_ids << pl.id if pl
      end
    end
    conditions << "pos_locations.id IN (#{pos_location_ids.join(",")})" unless pos_location_ids.blank?
    if conditions.blank?
      @pos_menu_items = PosMenuItem.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    else
      @pos_menu_items = PosMenuItem.includes(:pos_location => {:restaurant => :chain}).where(conditions.join(" AND ")).order("pos_menu_items.created_at DESC").paginate(:page => params[:page], :per_page => 10)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : PosLocation.where("chain_id IN (?)", params[:chains])
  end

  def new

  end

  def create
    chain = Chain.find(params[:chains])
    redirect_to new_admin_pos_menu_item_path, :alert => "Invalid Chain" and return if params[:chains].blank?
    xml_in_nokogiri_format = Nokogiri::XML(params[:menu_data])
    redirect_to new_admin_pos_menu_item_path, :alert => "Bad XML Menu Data" and return if xml_in_nokogiri_format.children.blank?
    Delayed::Job.enqueue(PosMenuItemJob.new(chain, params[:menu_data], params["is_olo"]))
    redirect_to admin_pos_menu_items_path, :notice => "Upload Success"
  end

end
