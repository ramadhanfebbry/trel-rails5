class Admin::ReceiptBarcodesController < Admin::BaseController

	
  # GET /receipts
  def index
    rescue_from_cancan(:index, Receipt)
    #page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
    @receipts = []#Receipt.active_chain.receipt_barcode.desc_list.paginate(page: page, per_page: Setting.pagination.per_page, :total_entries => -1) #Receipt.all

    respond_to do |format|
      format.html
      format.js
      format.csv {
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsJob.new(
                params,
                current_admin,
                true, "barcode"),
            :delayable_type => "receipt_export_csv")
        redirect_to admin_receipt_barcodes_path, :notice => "Receipt CSV will sent to your email shotrly"
      }
    end
  end

  def search
    q = params[:search][:q]
    q = q.gsub("'","''")
    params[:search][:q] = q

    ar_field = [
      "receipts.id",
      "offers.name",
      "users.email",
      "chains.name",
      "restaurants.name",
      "receipt_transactions.receipt_number",
      "receipts.created_at",
      "receipts.status"
    ]

    order = params[:qry_order]
    restaurants_ids = params["search"]["restaurants"]
    @receipts = []
    receipt_barcodes = Receipt.select("id, status, created_at, user_id, chain_id, is_ncr_receipt, is_direct_ncr_receipt").receipt_barcode
    is_paginate = true
    page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']

    if order.to_i < 1000
      p "1 " * 33
      qry_order = "#{ar_field[order.to_i]}"
      qry_order = qry_order + " desc"
      if restaurants_ids
        restaurants_ids.each do |restaurant_id|
          @receipts += receipt_barcodes.custom_search_with_barcode(params[:search], restaurant_id).order(qry_order)
        end
      else
        @receipts = receipt_barcodes.custom_search_with_barcode(params[:search]).order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, total_entries: -1)
        is_paginate = false
      end
    elsif order.to_i > 999
      p "2 " * 33
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      if restaurants_ids
        restaurants_ids.each do |restaurant_id|
          @receipts += receipt_barcodes.custom_search_with_barcode(params[:search], restaurant_id).order(qry_order)
        end
      else
        @receipts = receipt_barcodes.custom_search_with_barcode(params[:search]).order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, total_entries: -1)
        is_paginate = false
      end
    else
      p "2 " * 33
      qry_order = 'receipt_transactions.receipt_id desc'
      if restaurants_ids
        restaurants_ids.each do |restaurant_id|
          @receipts += receipt_barcodes.custom_search_with_barcode(params[:search], restaurant_id).joins(:chain).order(qry_order)
        end
      else
        @receipts = receipt_barcodes.custom_search_with_barcode(params[:search]).joins(:chain).order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, total_entries: -1)
        is_paginate = false
      end
    end

    #count = Receipt.custom_search(params[:search]).joins(:chain).count(distinct: true)
    #@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page, total_entries: count)
    @receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page, total_entries: -1) if is_paginate
    @action = '-search'
    
    respond_to do |format|
      format.html {render :index}
      format.js { render :index }
      format.csv {
      #  @receipts = Receipt.custom_search(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order);render :layout => false, :action => :index
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsJob.new(
                params,
                current_admin,
                nil, "barcode"),
            :delayable_type => "receipt_export_csv")
        redirect_to admin_receipt_barcodes_path, :notice => "Receipt CSV will sent to your email shotrly"
      }
    end
  end

  # GET /receipts/1
  def show
    rescue_from_cancan(:show, Receipt)
    @receipt = Receipt.receipt_barcode.active_chain.find(params[:id])
    @receipt_transactions = @receipt.receipt_transactions.order('id desc')
  end

  # GET /receipts/1/edit
  def edit
    rescue_from_cancan(:edit, Receipt)
    @receipt = Receipt.receipt_image.active_chain.find(params[:id])
    @receipt_transaction = @receipt.last_transaction
  end

  # PUT /receipts/1
  def update
    rescue_from_cancan(:update, Receipt)
    @receipt = Receipt.active_chain.find(params[:id])
    @receipt.attributes = params[:receipt]
    @old_transaction = @receipt.last_transaction

    @receipt_transaction = ReceiptTransaction.new(@old_transaction.attributes.merge(params[:receipt_transaction]))
    @receipt_transaction.reviewer=current_admin
    @receipt_transaction.status = params[:receipt][:status]

    @receipt_transaction.receipt = @receipt
    @receipt_transaction.restaurant_offer = @old_transaction.restaurant_offer
    @receipt_transaction.reject_reason_id = 0 if @receipt.status!=4
    @receipt_transaction.instructions = {:rejected => "Reject Receipt"} if @receipt.status.to_i.eql?(4)
    @receipt_transaction.created_at = nil
    @receipt_transaction.updated_at = nil
    @receipt_transaction.admin_id = 1
    #@receipt_transaction.tax = params[:tax]
    #@receipt_transaction.cashier = params[:cashier]
    #@receipt_transaction.guests= params[:guests]
    #@receipt_transaction.total_payment = params[:total_payment]
    #@receipt_transaction.payment_method = params[:payment_method]
    #@receipt_transaction.check_number= params[:check_number]
    #@receipt_transaction.issue_date = params[:issue_date]
    @receipt_transaction.reviewer_validation = true
    @receipt_transaction.updated_manually = true
    @uniq_receipt = true
    @today_maxed = false
    @valid_max_subtotal = true
    if @receipt.status.to_i.eql?(Receipt::STATUS[:APPROVED])
      if @receipt.chain.max_subtotal > 0 && @receipt.chain.max_subtotal < @receipt_transaction.subtotal.to_f && !params[:ignore_subtotal].eql?("true")
        @valid_max_subtotal = false
      end
      if @receipt.chain.id.eql?(Setting.chain_id.zoes)
        receipt_number = @receipt_transaction.receipt_number.gsub(/\D/, '').to_i
        receipt_transactions = ReceiptTransaction.where("receipt_id != ? AND DATE(issue_date) = ? AND restaurant_id = ? AND LOWER(receipt_number) LIKE ? AND status = ?", @receipt.id, @receipt_transaction.issue_date.to_date, @receipt_transaction.restaurant_id, "%#{receipt_number}", Receipt::STATUS[:APPROVED]) rescue []
      else
        receipt_number = @receipt_transaction.receipt_number
        receipt_transactions = ReceiptTransaction.where("receipt_id != ? AND DATE(issue_date) = ? AND restaurant_id = ? AND receipt_number = ? AND status = ?", @receipt.id, @receipt_transaction.issue_date.to_date, @receipt_transaction.restaurant_id, receipt_number, Receipt::STATUS[:APPROVED]) rescue []
      end
      @uniq_receipt = false unless receipt_transactions.blank?
      @selected_receipts = receipt_transactions.map{|rt| rt.receipt}
    end
    if @receipt.status.to_i.eql?(Receipt::STATUS[:APPROVED]) && @uniq_receipt && @receipt.chain.today_max_user_limit?(@receipt, params[:receipt_transaction][:issue_date])
      @today_maxed = true
    end
    respond_to do |format|
      if @uniq_receipt && !@today_maxed && @valid_max_subtotal
        if @receipt_transaction.save and @receipt.update_attributes(params[:receipt])
          @is_save = true
          RestaurantUser.create(:user_id => @receipt.user_id, :restaurant_id => @receipt_transaction.restaurant_id) unless @receipt_transaction.restaurant_id.blank?
          if @receipt.status == Receipt::STATUS[:APPROVED]
            @receipt.push_one_time_reward(@receipt.user)
            @receipt.push_referral_code_reward ## reward from referral code
            #begin
            #  AverageChartTable.update_average(@receipt.last_transaction)
            #rescue => e
            #  puts "average chart table update #{e.inspect}"
            #end
          end
          #format.html { redirect_to admin_receipt_url(@receipt), notice: 'Receipt was successfully updated.' }
        else
          @is_save = false
          #format.html { render action: "edit" }
        end
      end
      format.js
    end
  end

  def reject_reasons
    @id = params[:id]
    @opt = RejectReason.select("reject_reasons.id, reject_reasons.description") #find_all_by_chain_id(@id)

    render :json => @opt
  end

  # DELETE /receipts/1
  def destroy
    rescue_from_cancan(:destroy, Receipt)
    @receipt = Receipt.active_chain.receipt_image.find(params[:id])
    @receipt.destroy

    respond_to do |format|
      format.html { redirect_to admin_receipts_url }
    end
  end

  # def check_custom_image_uploader
  #   require 'rubygems'
  #   require 'aws/s3'
  #   image_url = "http://3.bp.blogspot.com/_3I6eIowAe7I/TH8IfL8BHNI/AAAAAAAAAz0/ueyBcoDyU_0/s1600/krishna-christ.jpg"
  #   image_dimention = ["1250","200"]
  #   process_flag = 1
  #   @returning_data = image_processing image_url, image_dimention, process_flag
  #   render :text => @returning_data and return     
  #   #render :text => "Looking ahead to see what can be done" and return
  # end

  def url
    @receipt = Receipt.find(params[:id])
    redirect_to @receipt.image.url
  end

  def blocked
    @receipt = Receipt.find(params[:id])
  end

  def goto
    @receipt = Receipt.find(params[:id]) rescue nil
  end

  def menu_receipt
    @xpient_template = false
    @receipt = Receipt.find(params[:id])
    if @receipt.is_receipt_barcode
      @transaction = @receipt.last_transaction rescue nil
      @pos_check_upload = @receipt.pos_check_upload rescue nil
      if @pos_check_upload && @pos_check_upload.is_xpient_check
        @xpient_template = true
        xpient_check = XpientCheck.new(YAML.load(@pos_check_upload.xml_data))
        @payment_total = xpient_check.total rescue 0
        @short_code = xpient_check.short_code
        @check_id = xpient_check.pos_order_id
        @menu_items = xpient_check.line_items rescue []
      elsif @pos_check_upload
        json_check = YAML.load(@pos_check_upload.xml_data) rescue nil

        if json_check.class == ActiveSupport::HashWithIndifferentAccess
          if @receipt.chain.pos_used_type == Chain::POS_USED_TYPE["FOCUS"]
            @pos_xml = FocusPos::Check.new(@pos_check_upload.xml_data)
          else
            @pos_xml = PosReceiptJson.new(@pos_check_upload.xml_data)
          end
        elsif json_check.class == Hash && @receipt.chain.pos_used_type == Chain::POS_USED_TYPE["NETPOS"]
          @pos_xml = NetPosReceipt.new(@pos_check_upload.xml_data)
        else
          @pos_xml = PosReceiptXml.new(@pos_check_upload.xml_data)
        end

        @payment_total = @pos_xml.total rescue 0
        @menu_items = @pos_xml.list_item rescue []
        check_id = @pos_xml.check_id
        seq_num = @pos_xml.seq_num
        @reward_discounts = RewardDiscount.where(:check_id => check_id, :seq_num =>  seq_num, :restaurant_id => @pos_check_upload.pos_location.restaurant_id).sum(:discount).to_f rescue 0
        @discount_total = @pos_xml.discount_total
        @direct_discounts = @pos_xml.xml_in_nokogiri_format.search("discount") rescue []
      else
        if @receipt.chain.pos_used_type == Chain::POS_USED_TYPE["FOCUS"]
          pos_check_upload_id = FocusPos::ReceiptCheckUpload.where(:receipt_id => @receipt.id).first.try(:focus_check_upload_id)
          @pos_check_upload = FocusPos::CheckUpload.find(pos_check_upload_id) rescue nil
          @pos_xml = FocusPos::Check.new(@pos_check_upload.check_data)
          @payment_total = @pos_xml.total rescue 0
          @menu_items = @pos_xml.list_item rescue []
          check_id = @pos_xml.check_id
          seq_num = @pos_xml.seq_num
          @reward_discounts = RewardDiscount.where(:check_id => check_id, :seq_num =>  seq_num, :restaurant_id => @pos_check_upload.pos_location.restaurant_id).sum(:discount).to_f rescue 0
          @discount_total = @pos_xml.discount_total
          @direct_discounts = @pos_xml.xml_in_nokogiri_format.search("discount") rescue []
        end
      end
    elsif @receipt.is_ncr_receipt
      @transaction = @receipt.last_transaction rescue nil
      @ncr_data_receipt = @receipt.ncr_data_receipt
      json_encode = @ncr_data_receipt.json_data.gsub("=>",":").gsub("nil", "0") rescue nil
      @receipt_data_in_json = ActiveSupport::JSON.decode(json_encode)
    end
  end

  def export_status
    #@receipts = Receipt.all
    respond_to do |format|
      format.csv {
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsStatusJob.new(
                params,
                current_admin,
                false, "barcode"),
            :delayable_type => "receipt_export_status_csv")
        redirect_to admin_receipt_barcodes_path
      }
    end
  end


  def list
  end

  def new_search
    receipt_barcodes = Receipt.select('receipts.id, receipts.status, receipts.created_at, receipts.user_id, receipts.chain_id,
    receipts.is_ncr_receipt, receipts.is_direct_ncr_receipt, t1.id as receipt_transaction_id, t1.restaurant_id, t1.payment_method').joins("JOIN receipt_transactions
    t1 ON (receipts.id = t1.receipt_id) LEFT OUTER JOIN receipt_transactions t2 ON (receipts.id = t2.receipt_id AND
    (t1.id < t2.id))").order("receipts.created_at desc").where("t2.id is null and receipts.is_receipt_barcode IS TRUE and receipts.chain_id = #{params[:search][:chains]} and t1.restaurant_id = #{params[:search][:restaurant_id]}")

    @receipts = receipt_barcodes
    @receipts = @receipts.where("DATE(receipts.created_at) >= '#{params[:search][:from]}' AND DATE(receipts.created_at) <= '#{params[:search][:to]}'") if params[:search][:from].present? && params[:search][:to].present?
    @receipts = @receipts.where("receipts.status = #{params[:search][:review_status]}") unless params[:search][:review_status].blank?
    @receipts = @receipts.where("t1.payment_method = #{params[:search][:payment_method]}")unless params[:search][:payment_method].blank?

    q = params[:search][:q] rescue nil
    if params[:search][:qry_column] == "Email"
      chain = Chain.find(params[:search][:chains])
      user = chain.users.where("lower(email) ='#{q.downcase}' ").first rescue nil
      user.present? ? @receipts = @receipts.where("receipts.user_id ='#{user.id}' ") : @receipts = []
    elsif params[:search][:qry_column] == "Server name"
      @receipts = @receipts.where("lower(t1.server_name) ='#{q.downcase}' ")
    elsif params[:search][:qry_column] == "Cashier"
      @receipts = @receipts.where("lower(t1.cashier) ='#{q.downcase}' ")
    elsif params[:search][:qry_column] == "Receipt number"
      @receipts = @receipts.where("lower(t1.receipt_number) LIKE '%#{q.downcase}%' ")
    elsif params[:search][:qry_column] == "Barcode"
      query_receipts = @receipts
      @receipts = []
      pos_check_upload_ids = PosCheckUpload.where("chain_id = #{params[:search][:chains]} and lower(barcode) = '#{params[:search][:q].downcase}' ")
      pos_check_upload_ids.each do |pos|
        @receipts += query_receipts.where("receipts.pos_check_upload_id = #{pos.id}")
      end
    end if params[:search][:qry_column].present? && q.present?

    @receipts = @receipts.paginate(page: params[:page], per_page: Setting.pagination.per_page)
    respond_to do |format|
      format.html { render :new_search }
      format.js { render :new_search }
    end
  end
end