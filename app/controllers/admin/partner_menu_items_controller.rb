class Admin::PartnerMenuItemsController < Admin::BaseController

  def index
    if params["search"] && params["search"]["chain_id"].present?
      @chain = Chain.find( params["search"]["chain_id"])
      @restaurants = @chain.restaurants.select("id,name,address").order("name ASC")
      @restaurant_id = params["search"]["restaurant_id"] rescue nil
      if @restaurant_id
        @partner_menu_items = @chain.partner_menu_items.includes("partner_menu_items_restaurants").where("partner_menu_items_restaurants.restaurant_id = #{@restaurant_id}")
        .order("partner_menu_items.updated_at ASC").paginate(:page => params[:page], :per_page => 10)
      else
        @partner_menu_items = @chain.partner_menu_items.order("updated_at ASC").paginate(:page => params[:page], :per_page => 10)
      end
    else
      @partner_menu_items = PartnerMenuItem.order("updated_at ASC").paginate(:page => params[:page], :per_page => 10)
      @chain = nil
    end
  end

  def new
    @partner_menu_item = PartnerMenuItem.new
  end

  def create
    chain = Chain.find(params[:partner_menu_item][:chain_id]) rescue nil
    redirect_to new_admin_partner_menu_item_path, :alert => "Invalid Chain" and return if chain.blank?
    locations = params[:restaurants] rescue nil
    redirect_to new_admin_partner_menu_item_path, :alert => "Locations can't be blank." and return if locations.blank?
    xml_in_nokogiri_format = Nokogiri::XML(params[:menu_data])
    redirect_to new_admin_partner_menu_item_path, :alert => "Bad XML Menu Data" and return if xml_in_nokogiri_format.children.blank?

    Delayed::Job.enqueue(PartnerMenuItemParticipatingJob.new(chain, params[:menu_data], params[:restaurants])) if params[:restaurants].present?
    redirect_to admin_partner_menu_items_path, :notice => "Upload Success"
  end

  def edit
    @partner_menu_item = PartnerMenuItem.find params[:id]
    @chain = @partner_menu_item.chain
    @categories = @chain.partner_sub_categories.select("id,name")
  end

  def destroy
    @partner_menu_item = PartnerMenuItem.find params[:id]
    @partner_menu_item.destroy
    redirect_to admin_partner_menu_items_path, :notice => "Partner Category successfully destroyed."
  end

  def load_restaurants
    chain = Chain.find(params[:chain_id])
    @restaurants = chain.restaurants.select("id,name,address").order("name ASC")
    p "@restaurants #{@restaurants.count}"
    @page = params[:page]
  end

end
