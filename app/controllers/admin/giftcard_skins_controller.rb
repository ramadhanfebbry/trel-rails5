class Admin::GiftcardSkinsController < Admin::BaseController

  before_action :find_chain

  def index
    @giftcard_skins = @chain.giftcard_skins.paginate(:page => params[:page], :per_page => 20)
  end

  def new
    @giftcard_skin = GiftcardSkin.new
  end

  def create
    @giftcard_skin = GiftcardSkin.new(params_giftcard_skin)
    @giftcard_skin.chain_id = @chain.id
    if @chain.giftcard_skins.count >= 20
      redirect_to admin_chain_giftcard_skins_path(@chain), alert: "Max limit giftcard skins is 20."
    else
      if @giftcard_skin.save
        redirect_to admin_chain_giftcard_skins_path(@chain), notice: "giftcard skin created."
      else
        render "edit"
      end
    end
  end

  def show
    @giftcard_skin = GiftcardSkin.find(params[:id])
  end

  def edit
    @giftcard_skin = GiftcardSkin.find(params[:id])
  end

  def update
    @giftcard_skin = GiftcardSkin.find(params[:id])
    if @giftcard_skin.update_attributes(params_giftcard_skin)
      redirect_to admin_chain_giftcard_skins_path(@chain), notice: "giftcard skin updated."
    else
      render "edit"
    end
  end

  def destroy
    @giftcard_skin = GiftcardSkin.find(params[:id])
    @giftcard_skin.destroy
    redirect_to admin_chain_giftcard_skins_path(@chain), notice: "giftcard skin deleted."
  end

  private

  def find_chain
    @chain = Chain.find params[:chain_id] rescue nil
  end

  def params_giftcard_skin
    params.required(:giftcard_skin).permit(:title, :is_active, :description, :app_image, :email_header_image)
  end
end
