class Admin::FaqsController < Admin::BaseController

	before_action :find_chain
	layout 'admin', :except => [:preview]

	def index
		@faqs = @chain.faqs
	end

	def show
		@faqs = @chain.faqs
	end

	def new
		@faq = @chain.faqs.build
	end

	def edit
		@faq = @chain.faqs.find(params[:id])
	end

	def create
		@faq = @chain.faqs.build(params_faq)
		if @faq.save
			redirect_to admin_chain_faqs_path, :notice => "FAQ Pages has been created."
		else
			render :new
		end
	end

  def update
    p params
    @faq = @chain.faqs.find(params[:id])
    if @faq.update(params_faq)
      if @faq.url
        content = '<!DOCTYPE html><html><head><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="viewport" content="initial-scale=1.0, maximum-scale=1, user-scalable=0"/><meta name="apple-mobile-web-app-status-bar-style" content="black" /><title>' + @faq.title + '</title>'
        content += '<link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" media="screen" rel="stylesheet" type="text/css" /><link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css" media="screen" rel="stylesheet" type="text/css" />'
        content += '<link href="/assets/fonts.css" media="screen" rel="stylesheet" type="text/css" />'
        content += '<style>'+ @faq.css_content.html_safe + '</style>'
        content += '</head><body class="m-body"><div class="container m-container">'+ @faq.html_content.html_safe + '</div></body></html>'

        url = "http://" + request.host + "/faqs/faq-#{@chain.id}-#{@faq.id}.html"
        if @faq.update_attributes(:url => url, :generated_html_content => content)
          redirect_to admin_chain_faqs_path, :notice => "FAQ Pages has been update."
        else
          redirect_to admin_chain_faqs_path, :notice => "URL failed to update."
        end
      else
        redirect_to admin_chain_faqs_path, :notice => "FAQ Pages has been update."
      end
    else
      render :edit
    end
  end

	def destroy
		@faq = @chain.faqs.find(params[:id])
		file_path = "#{Rails.root}/public/faqs/faq-#{@chain.id}-#{@faq.id}.html"
		if File.exist?(file_path) 
			File.delete(file_path)
		end
		@faq.destroy
		redirect_to admin_chain_faqs_path, :notice => "FAQ Pages has been deleted."
	end

	def preview
		@faq = @chain.faqs.find(params[:id])
	end

  def generate_url
    @faq = @chain.faqs.find(params[:id])

    content = '<!DOCTYPE html><html><head><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="viewport" content="initial-scale=1.0, maximum-scale=1, user-scalable=0"/><meta name="apple-mobile-web-app-status-bar-style" content="black" /><title>' + @faq.title + '</title>'
    content += '<link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap.css" media="screen" rel="stylesheet" type="text/css" /><link href="http://getbootstrap.com/2.3.2/assets/css/bootstrap-responsive.css" media="screen" rel="stylesheet" type="text/css" />'
    content += '<link href="/assets/fonts.css" media="screen" rel="stylesheet" type="text/css" />'
    content += '<style>'+ @faq.css_content.html_safe + '</style>'
    content += '</head><body class="m-body"><div class="container m-container">'+ @faq.html_content.html_safe + '</div></body></html>'
    url = "http://" + request.host + "/faqs/faq-#{@chain.id}-#{@faq.id}.html"
    if @faq.update_attributes(:url => url, :generated_html_content => content)
      redirect_to admin_chain_faqs_path, :notice => "URL has been update."
    else
      redirect_to admin_chain_faqs_path, :notice => "URL failed to update."
    end

  end



	private

	def find_chain
		@chain = Chain.find params[:chain_id]
	end

	def params_faq
		params.required(:faq).permit!
	end
end
