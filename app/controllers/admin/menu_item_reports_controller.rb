class Admin::MenuItemReportsController < Admin::BaseController

  def index
    @menu_item_report = MenuItemReport.new
  end

  def load_user_list
    @chain = Chain.find params[:id] rescue nil
    @rewards = @chain.rewards.order("name asc") rescue []
    respond_to do |format|
      format.js
      format.json { render :json => @rewards}
    end
  end

  def create
    @menu_item_report = MenuItemReport.new(params[:menu_item_report])
    if @menu_item_report.valid?
      p @menu_item_report
      p "over here--------"
      Delayed::Job.enqueue(MenuItemReportJobv2.new(@menu_item_report.reward_id, @menu_item_report.email, @menu_item_report.start_date, @menu_item_report.end_date))
      p "delayed job added--------"
      redirect_to admin_menu_item_reports_path, :notice => "The results will send to the email that is filled on the form."
    else
      chain = Chain.find params[:menu_item_report][:chain_id] rescue nil
      @rewards = chain.rewards
      render :index
    end
  end

end
