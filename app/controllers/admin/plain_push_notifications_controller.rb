class Admin::PlainPushNotificationsController < Admin::BaseController
  # GET /admin/plain_push_notifications
  # GET /admin/plain_push_notifications.json
  def index
    @plain_push_notifications = PlainPushNotification.order("created_at desc").paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @plain_push_notifications }
      format.js
    end
  end

  def email_list
    plain = PlainPushNotification.find(params[:id])
    @plain = plain
    @emails = []
    @emails = plain.email[:emails] if plain.email
    @all_user = User.where("id in(?)", @emails).count
    @device_success = plain.history_notifications.where("plain_kind = 2 and status = 1").count
    @email_success = plain.history_notifications.where("plain_kind = 1 and status = 1").count
    @device_pending = @all_user - @device_success
    @email_pending = @all_user - @email_success
    @failed_device = plain.history_notifications.where("plain_kind = 2 and status = 2").count
    @failed_email = plain.history_notifications.where("plain_kind = 1 and status = 2").count

    if @device_success == 0 and @email_success == 0 ## if still using old way
      @emails = User.where("id in(?)", @emails).paginate(:page => params[:page], :per_page => 10) #@emails.paginate(:page => params[:page], :per_page => 100)
    else
      @emails = User.joins(:history_notifications).where("plain_id = ? and status = 1", @plain.id).select('distinct(users.id), users.email').
          paginate(:page => params[:page], :per_page => 10)
    end
    #@emails = plain.history_notifications.
  end

  def get_email_for_chains
    @emails = []
    @locales = []
    @chains = Chain.where("id in(?)", params["chain_ids"]) unless params["chain_ids"].blank? || params["chain_ids"] == "null"
    unless @chains.blank?
      @chains.each do |ch|
        @emails << User.where("chain_id = ?", ch).map(&:id)
        @locales << ch.locales.map(&:key)
      end
    end
    @locales = @locales.flatten.uniq
    @emails = @emails.flatten
  end

  def preview_xls_data
    @plain_push_notification = PlainPushNotification.new
    test_file = params[:xls]
    notice = "Preview"
    unless test_file.blank?
      book = Spreadsheet.open "#{test_file.path}"
      sheet1 = book.worksheet 0
      debugger
      @email_from_xls = []
      sheet1.column(0).each { |row| @email_from_xls << row }
      begin
        @email_from_xls = @emails_from_xls.compact
      rescue
      end
    else
      notice = "Please upload the xls file"
    end
    render :layout => false
  end

  # GET /admin/plain_push_notifications/1
  # GET /admin/plain_push_notifications/1.json
  def show
    @plain_push_notification = PlainPushNotification.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @plain_push_notification }
    end
  end

  def show_email_list
    @plain_push_notification = PlainPushNotification.find(params[:id])
    mails = @plain_push_notification.email[:emails]
    conditions = []
    if mails.first.to_i > 0
      conditions << "id in (#{mails.map(&:to_i).join(",")})"
    else
      conditions << "email in (#{mails.join(",")})"
    end

    if params[:search_by].eql?("user_id") && !params[:key].blank?
      conditions << "id = #{params[:key].to_i}"
    elsif params[:search_by].eql?("email") && !params[:key].blank?
      conditions << "LOWER(email) LIKE '%#{params[:key].downcase}%'"
    end

    emails = User.where(conditions.join(" AND ")).paginate(:page => params[:page], :per_page => 10)
    @emails = emails#.paginate(:page => params[:page], :per_page => 100)
  end

  def show_log_notifications
    @plain_push_notification = PlainPushNotification.find(params[:id])
    conditions = []
    conditions << "history_notifications.status = 1"
    conditions << "history_notifications.plain_kind = 2"
    conditions << "LOWER(users.email) LIKE '%#{params[:email].downcase}%'" unless params[:email].blank?
    conditions << "LOWER(users.register_device_type) = '#{params[:device_type].downcase}'" unless params[:device_type].blank?
    @notifications = @plain_push_notification.history_notifications.joins(:user).where(conditions.join(" AND ")).paginate(:page => params[:page], :per_page => 10)
  end


  # GET /admin/plain_push_notifications/new
  # GET /admin/plain_push_notifications/new.json
  def new
    @plain_push_notification = PlainPushNotification.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @plain_push_notification }
    end
  end

  # GET /admin/plain_push_notifications/1/edit
  def edit
    @plain_push_notification = PlainPushNotification.find(params[:id])
  end

  def create
    ## emails is change with user ids
    emails = []
    chain_ids = params["search"]["chains"] rescue []
    case params["select_type"]
      when "manual_enter"
        emails = params["plain_push_notification"]["email"]
        emails = emails.squish.gsub("\\r", '').gsub('\\n', '').gsub(' ', '').split(',')
      when "csv"
        @plain_push_notification = PlainPushNotification.new
        test_file = params[:xls]
        notice = "Preview"
        unless test_file.blank?
          begin
            book = Spreadsheet.open "#{test_file.path}"
            sheet1 = book.worksheet 0
            debugger

            sheet1.column(0).each { |row| emails << row if !row.to_s.include?(" ")}

            emails = emails.compact.uniq
            error = false
            err_msg = ""

            if emails.size == 0
              err_msg = "There are No Content on the xls files"
              error = true
            end

            if emails.size > 0
              emails.each do |uid|
                if uid.to_i == 0
                  error = true
                  err_msg = "Only User ids content are valid in the xls files no email address, or anything"
                  break
                end
              end
            end

            return redirect_to new_admin_plain_push_notification_path, :alert => err_msg if error == true
          rescue => e
            puts "PUSHPOINTSCONTROLLER :: CREATE #{e.inspect}"
            return redirect_to new_admin_plain_push_notification_path, :alert => "Please Upload xls files !"
          end
        else
          notice = "Please upload the xls file"
        end
      when "all_user"
        count_email = User.where("chain_id in (?)", chain_ids).count
        loop = (count_email.to_f / 4000.to_f).ceil
        emails = []
        1.upto(loop) do |page|
         emails += User.where("chain_id in (?)", chain_ids).select('id').paginate(page: page, per_page: 4000).map(&:id)
        end
    end

    content = params["notification"] #[:content]#"content"=>{"en"=>"test", "es"=>"asdfasfasdf"}
                                     #debugger
    if content.class != ActiveSupport::HashWithIndifferentAccess
      content = {"en" => content}
    end

    chain = Chain.find chain_ids
    chain.locales.each do |locale|
      content["content"]["subject_#{locale.key}"] = content["content"]["subject_en"]
      content["content"]["#{locale.key}"] = content["content"]["en"]
      content["content"]["device_text_#{locale.key}"] = content["content"]["device_text_en"]
    end

    p "--------content"
    p content
    p "---------"
    date_text = params[:date_execute] rescue ""
    time_text = "#{params[:date][:hour]}:#{params[:date][:minute]}" rescue ""
    p "#{date_text} #{time_text} EDT"
    run_at = Time.zone.parse("#{date_text} #{time_text} EDT") rescue nil
    @plain_push_notification = PlainPushNotification.new(params["plain_push_notification"])
    diff_hour = 5
    diff_hour = 4 if params['time_zone_default'].to_s == "EDT"
    @plain_push_notification.executed_at
    @plain_push_notification.executed_at = @plain_push_notification.executed_at - diff_hour.hours unless @plain_push_notification.executed_at.blank?
    @plain_push_notification.executed_at
    p run_at
    if run_at && run_at >= Time.zone.now
      @plain_push_notification.executed_at = run_at.utc
    end
    @plain_push_notification.executed_at = nil if params['scheduler'].to_s != "on"
    @plain_push_notification.email = {:emails => emails}
    @plain_push_notification.admin_id = current_admin.id
    @plain_push_notification.content = {:content => content}
    puts "chain_id ================= #{chain_ids}"
    @plain_push_notification.chains = {:chain_ids => chain_ids}
    @plain_push_notification.is_push_email = false if params["plain_push_notification"]["is_push_email"].blank?
    @plain_push_notification.is_push_phone = false if params["plain_push_notification"]["is_push_phone"].blank?
                                     #UserMailer.push_chain_plain_notification()

    respond_to do |format|
      if @plain_push_notification.save #&&
        @plain_push_notification.delay(:delayable_type => @plain_push_notification.class.to_s, :delayable_id => @plain_push_notification.id).send_plain_email
        #x.update_column(:run_at, nil)
        format.html { redirect_to admin_plain_push_notifications_url, notice: 'Plain push notification was successfully created.' }
        format.json { render json: @plain_push_notification, status: :created, location: @plain_push_notification }
      else
        format.html { render action: "new" }
        format.json { render json: @plain_push_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /admin/plain_push_notifications
  # POST /admin/plain_push_notifications.json
  #def create
  #  ## emails is change with user ids
  #  emails = params[:plain_push_notification][:email]
  #  emails = emails.squish.gsub("\\r",'').gsub('\\n','').gsub(' ','').split(',')
  #
  #  unless emails.blank?
  #    emails = emails + (params[:email].blank?? [] : params[:email])
  #  else
  #    emails = params[:email]
  #  end
  #  content = params["notification"]#[:content]#"content"=>{"en"=>"test", "es"=>"asdfasfasdf"}
  #  #debugger
  #  if content.class != ActiveSupport::HashWithIndifferentAccess
  #    content = {"en" => content}
  #  end
  #  chain_ids = params["search"]["chains"] rescue []
  #
  #  @plain_push_notification = PlainPushNotification.new(params[:plain_push_notification])
  #  @plain_push_notification.email = {:emails => emails}
  #  @plain_push_notification.content = {:content => content}
  #  @plain_push_notification.chains = {:chain_ids => chain_ids }
  #  @plain_push_notification.is_push_email = false if params["plain_push_notification"]["is_push_email"].blank?
  #  @plain_push_notification.is_push_phone = false if params["plain_push_notification"]["is_push_phone"].blank?
  #  #UserMailer.push_chain_plain_notification()
  #
  #  respond_to do |format|
  #    if @plain_push_notification.save && @plain_push_notification.send_plain_email
  #      format.html { redirect_to admin_plain_push_notifications_url, notice: 'Plain push notification was successfully created.' }
  #      format.json { render json: @plain_push_notification, status: :created, location: @plain_push_notification }
  #    else
  #      format.html { render action: "new" }
  #      format.json { render json: @plain_push_notification.errors, status: :unprocessable_entity }
  #    end
  #  end
  #end

  # PUT /admin/plain_push_notifications/1
  # PUT /admin/plain_push_notifications/1.json
  def update
    @plain_push_notification = PlainPushNotification.find(params[:id])
    @plain_push_notification.admin_id = current_admin.id

    respond_to do |format|
      if @plain_push_notification.update_attributes(params[:plain_push_notification])
        format.html { redirect_to admin_plain_push_notifications_url, notice: 'Plain push notification was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @plain_push_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/plain_push_notifications/1
  # DELETE /admin/plain_push_notifications/1.json
  def destroy
    @plain_push_notification = PlainPushNotification.find(params[:id])
    @plain_push_notification.destroy

    respond_to do |format|
      format.html { redirect_to admin_plain_push_notifications_url }
      format.json { head :ok }
    end
  end

  def filter_plain_push_notifications
    sth = params[:search_text_hidden]
    if params[:chn_id] != ""
      @plain_push_notifications = PlainPushNotification.where("chains ILIKE ?",  "%#{params[:chn_id]}%").paginate(page: params[:page], per_page: Setting.pagination.per_page)
    else
      @plain_push_notifications = PlainPushNotification.all.paginate(page: params[:page], per_page: Setting.pagination.per_page)
    end
  end
end
