class Admin::OloConnectSettingsController < Admin::BaseController

  def index

  end

  def search
    @chain = Chain.find(params["chain_id"])
    @olo_connect_setting = OloConnectSetting.find_by_chain_id(@chain.id)
  end

  def new
    @chain = Chain.find params['chain_id']
    @olo_connect_setting = OloConnectSetting.new
    render :layout => false
  end

  def edit
    @olo_connect_setting = OloConnectSetting.find params[:id]
    @chain = Chain.find @olo_connect_setting.chain_id
    render :layout => false
  end

  def create
    @olo_connect_setting = OloConnectSetting.new(params[:olo_connect_setting])

    if @olo_connect_setting.save
      @save = true
    else
      @save = false
    end
  end

  def update
    @olo_connect_setting = OloConnectSetting.find params[:id]
    if @olo_connect_setting.update_attributes(params[:olo_connect_setting])
      @update = true
    else
      @update = false
    end
  end

  def destroy
    @olo_connect_setting = OloConnectSetting.find params[:id]
    @olo_connect_setting.destroy unless @olo_connect_setting.blank?

    redirect_to admin_olo_connect_settings_path , :notice => "OLO Connect Setting Deleted"
  end

end
