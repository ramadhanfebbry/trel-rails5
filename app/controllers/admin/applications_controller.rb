class Admin::ApplicationsController < Admin::BaseController

  cache_sweeper :application_sweeper, :only => [ :create, :update, :destroy ]

  # GET /applications
  def index
    rescue_from_cancan(:index, Application)
    @applications = Application.active_chain
  end

  # GET /applications/1
  def show
    rescue_from_cancan(:show, Application)
    @application = Application.enabled.find(params[:id])

  end

  # GET /applications/new
  def new
    rescue_from_cancan(:new, Application)
    @application = Application.new
  end

  # GET /applications/1/edit
  def edit
    rescue_from_cancan(:edit, Application)
    @application = Application.find(params[:id])
  end

  # POST /applications
  def create
    rescue_from_cancan(:create, Application)
    @application = Application.new(application_params)

    respond_to do |format|
      if @application.save
        format.html { redirect_to admin_application_url(@application), notice: 'Application was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /applications/1
  def update
    rescue_from_cancan(:update, Application)
    @application = Application.find(params[:id])

    respond_to do |format|
      if @application.update(application_params)
        format.html { redirect_to admin_application_url(@application), notice: 'Application was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /applications/1
  def destroy
    rescue_from_cancan(:destroy, Application)
    @application = Application.find(params[:id])
    @application.destroy

    respond_to do |format|
      format.html { redirect_to admin_applications_url }
    end
  end

  def set_key
    rescue_from_cancan(:set_key, Application)
    @application = Application.find(params[:id])
  end

  def change_status
    application = Application.find(params[:id])
    application.send(params[:status])
    application.save
    redirect_to admin_applications_url, notice: 'Application has been already updated.'
  end

  def test_send_apn
    rescue_from_cancan(:test_send_apn, Application)
    @application = Application.find(params[:id])
    @certificate = @application.rapns_app
    @rapns = Rpush::Apns::Notification.new
  end

  def send_apn
    rescue_from_cancan(:send_apn, Application)
    @application = Application.find(params[:id])
    @certificate = @application.rapns_app
    @rapns = Rpush::Apns::Notification.new(params[:rpush_apns_notification])
    @rapns.app = @certificate
    @rapns.uri = "https://www.google.com"
    @rapns.sound = "1.aiff"
    if @rapns.save
      redirect_to admin_applications_path, notice: "APN has been successfully sent."
    else
      render :test_send_apn
    end
  end

  private

  def application_params
    params.require(:application).permit(:name, :chain_id, :device_type)
  end
end
