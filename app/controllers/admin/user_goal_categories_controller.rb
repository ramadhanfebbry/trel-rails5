class Admin::UserGoalCategoriesController < Admin::BaseController

  def index

  end

  def goal_categories_by_chain
    @chain = Chain.find(params[:id]) rescue nil
    @users = @chain.users.includes(:categories).where("email LIKE ?", "%#{params[:query]}%").paginate(:page => params[:page], :per_page => 10) rescue []
    respond_to do |format|
      format.html {render "goal_categories_by_chain.html"}
      format.js {render "goal_categories_by_chain.js"}
    end
  end

  def edit
    @user = User.find(params[:id])
    @chain = @user.chain
  end

  def update
    @user = User.find(params[:id])
    @chain = @user.chain

    today = Time.current
    start_date = today.send("custom_beginning_of_#{Setting.goals.unit_time}")
    end_date = today.send("custom_end_of_#{Setting.goals.unit_time}")

    week = Week.where(:start_date => start_date, :end_date => end_date).first

    if @user.categories.blank? or week.blank? or (week and @user.user_goals.where(:week_id => week.id).blank?) or @user.allow_to_update_goals
      @user.category_part = true
      @user.category_ids = params[:category_ids]
      if @user.save
        redirect_to goal_categories_by_chain_admin_user_goal_category_path(@chain), :notice => "Your goals have been updated."
      else
        render :edit
      end
    else
      temp_category = TmpCategory.where(:user_id => @user.id).first
      if temp_category.blank?
        temp_category = TmpCategory.create(:user_id => @user.id, :category_ids => (params[:category_ids].join(",") rescue nil))
      else
        temp_category.update_attributes(:category_ids => (params[:category_ids].join(",") rescue nil))
      end
      if temp_category.errors.blank?
        redirect_to goal_categories_by_chain_admin_user_goal_category_path(@chain), :notice => "Your updated goals will appear next week."
      else
        render :edit
      end
    end
  end

  def show_goals
    @user = User.find(params[:id])
    @weeks = Week.order("updated_at DESC").paginate(:page => params[:page], :per_page => 10)
  end
  
end
