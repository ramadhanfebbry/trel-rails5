class Admin::ChoicesController < Admin::BaseController
  # GET /admin/choices
  # GET /admin/choices.json
  def index
    rescue_from_cancan(:index, QuestionChoice)
    @choices = QuestionChoice.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json :@choices }
    end
  end

  # GET /admin/choice/1
  # GET /admin/choice/1.json
  def show
    rescue_from_cancan(:show, QuestionChoice)
    @choice = QuestionChoice.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json :@choice }
    end
  end

  # GET /admin/choice/new
  # GET /admin/choice/new.json
  def new
    rescue_from_cancan(:new, QuestionChoice)
    @choice = QuestionChoice.new(params[:choice])

    if(@choice.question.question_choices.length>=10)
      render :json=> {:error=>"You cannot add more then 10 choices."}
    elsif @choice.save
      render :json=> {:head=>true, :url=>edit_admin_choice_url(@choice), :delete_url=>admin_choice_path(@choice) }
    else
      render :json=> {:error=>"System error: insertion failed."}
    end
  end

  # GET /admin/choice/1/edit
  def edit
    rescue_from_cancan(:edit, QuestionChoice)
    @choice = QuestionChoice.find(params[:id])

    if @choice.update_attributes(params[:choice])
      render :json=> true
    else
      render :json=> {:error=>@choice.errors}
    end
  end

  # POST /admin/choices
  # POST /admin/choices.json
  def create
    rescue_from_cancan(:create, QuestionChoice)
    @choice = QuestionChoice.new(params[:choice])

    respond_to do |format|
      if @choice.save
        format.html { redirect_to admin_questions_url, notice: 'Question was successfully created.' }
        format.json { render json :@choice, status: :created }
      else
        format.html { render action: "new" }
        format.json { render json :@choice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/choice/1
  # PUT /admin/choice/1.json
  def update
    rescue_from_cancan(:update, QuestionChoice)
    @choice = QuestionChoice.find(params[:id])


    respond_to do |format|
      if @choice.update_attributes(params[:choice])
        format.html { redirect_to admin_questions_url, notice: 'Question was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json :@choice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/choice/1
  # DELETE /admin/choice/1.json
  def destroy
    rescue_from_cancan(:destroy, QuestionChoice)
    @choice = QuestionChoice.find(params[:id])
    @survey = @choice.question.survey
    respond_to do |format|
      if (@choice.question.question_choices.length>2)
        @choice.destroy
        format.js { render :template => "admin/questions/destroy.js" }
        format.json {  render :json=> true }
      elsif(@choice.question.question_choices.length<=2)
        format.json { render :json=> {:error=>"question must have at least 2 choices"} }
      else
        format.json { render :json=> {:error=>"System error: unable to delete."} }
      end
    end
    

  end
end
