class Admin::StreamGalleriesController < Admin::BaseController

  def index
    @stream_galleries = StreamGallery.paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  def search
    @chain = Chain.find(params[:chain_id]) rescue nil
    @stream_gallery = @chain.stream_gallery rescue nil
  end

  def new
    @chain = Chain.find(params[:chain_id]) rescue nil
    @stream_gallery = StreamGallery.new(:chain_id => @chain.id)
  end

  def edit
    @stream_gallery = StreamGallery.find(params[:id])
  end


  def create
    @stream_gallery = StreamGallery.new(params[:stream_gallery])
    @chain = @stream_gallery.chain
    @is_saved = @stream_gallery.save
  end

  def update
    @stream_gallery = StreamGallery.find(params[:id])
    @chain = @stream_gallery.chain
    @is_saved = @stream_gallery.update_attributes(params[:stream_gallery])
  end

  def destroy
    @stream_gallery = StreamGallery.find(params[:id])
    @chain = @stream_gallery.chain
    @stream_gallery.destroy if @stream_gallery
    @stream_gallery = nil
  end

  def add_xml_feed_link
    @stream_gallery = StreamGallery.find(params[:id])
    @chain = @stream_gallery.chain
    @stream_gallery_image = @stream_gallery.xml_embeds.new(:chain_id => @chain.id, :input_source => StreamGalleryImage::EMBED_SOURCE["XML FEED LINK"])
  end

  def create_xml_feed_link
    @stream_gallery = StreamGallery.find(params[:id])
    @chain = @stream_gallery.chain
    @stream_gallery_image = @stream_gallery.xml_embeds.new(params[:stream_gallery_image])
    @stream_gallery_image.chain_id = @chain.id
    @is_saved = @stream_gallery_image.save
    Delayed::Job.enqueue(StreamGalleriesUrlParserJob.new(@stream_gallery_image.id)) if @is_saved
  end

  def remove_xml_embed
    @stream_gallery_image = StreamGalleryImage.find(params[:id])
    @stream_gallery = @stream_gallery_image.stream_gallery
    @stream_gallery_image.destroy
    @chain = @stream_gallery.chain
  end

  def edit_xml_embed
    @stream_gallery_image = StreamGalleryImage.find(params[:id])
    @stream_gallery = @stream_gallery_image.stream_gallery
    @chain = @stream_gallery.chain
  end

  def update_xml_embed
    @stream_gallery_image = StreamGalleryImage.find(params[:id])
    @stream_gallery = @stream_gallery_image.stream_gallery
    @is_saved = @stream_gallery_image.update_attributes(params[:stream_gallery_image])
    @chain = @stream_gallery.chain
    Delayed::Job.enqueue(StreamGalleriesUrlParserJob.new(@stream_gallery_image.id)) if @is_saved
  end

  def add_image_upload
    @stream_gallery = StreamGallery.find(params[:id])
    @chain = @stream_gallery.chain
    @stream_gallery_image = @stream_gallery.image_embeds.new(:chain_id => @chain.id, :input_source => StreamGalleryImage::EMBED_SOURCE["IMAGES AND METADATA UPLOAD"])
  end

  def create_image_upload
    @stream_gallery = StreamGallery.find(params[:id])
    @chain = @stream_gallery.chain
    @stream_gallery_image = @stream_gallery.xml_embeds.new(params[:stream_gallery_image])
    @stream_gallery_image.chain_id = @chain.id
    @is_saved = @stream_gallery_image.save
    if @is_saved
      render :text => "success--#{@stream_gallery_image.id}"
    else
      render :text => @stream_gallery_image.errors.to_json
    end
  end

  def remove_image_upload
    @stream_gallery_image = StreamGalleryImage.find(params[:id])
    @stream_gallery = @stream_gallery_image.stream_gallery
    @stream_gallery_image.destroy
    @chain = @stream_gallery.chain
  end

  def edit_image_upload
    @stream_gallery_image = StreamGalleryImage.find(params[:id])
    @stream_gallery = @stream_gallery_image.stream_gallery
    @chain = @stream_gallery.chain
  end

  def update_image_upload
    @stream_gallery_image = StreamGalleryImage.find(params[:id])
    @stream_gallery = @stream_gallery_image.stream_gallery
    @is_saved = @stream_gallery_image.update_attributes(params[:stream_gallery_image])
    @chain = @stream_gallery.chain
    p params[:stream_gallery_image][:embed]
    p params[:stream_gallery_image][:embed].blank?
    p "----"
    unless params[:stream_gallery_image][:embed].blank?
      if @is_saved
        render :text => "success--#{@stream_gallery_image.id}"
      else
        render :text => @stream_gallery_image.errors.to_json
      end
    end
  end

  def reload_image_upload
    @stream_gallery_image = StreamGalleryImage.find(params[:id])
    @stream_gallery = @stream_gallery_image.stream_gallery
    @chain = @stream_gallery.chain
  end

end
