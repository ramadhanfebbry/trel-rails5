class Admin::ChainsController < Admin::BaseController

  cache_sweeper :chain_sweeper, :only => [ :create, :update, :destroy ]
  skip_before_action :authenticate_admin!, :only => [:report_notifications]

  # GET /chains
  def index
    rescue_from_cancan(:index, Chain)
    sth = params[:search_text_hidden]
    sah = params[:search_appkey_hidden]
    ar_field = [
      "chains.id",
      "chains.name",
      "owners.first_name",
      "locales.name",
      "chains.points",
      "chains.user_points",
      "chains.email",
      "chains.survey_email",
      "chains.hours_delay_reward",
      "chains.user_limit_receipt_approved_per_day"
    ]
    order = params[:qry_order]
    chains = Chain.includes(:owners, :locales, :applications => :application_keys).joins(:applications => :application_keys)
    
    if sth.present? || order.present? || sah.present?
      qry_condition = "chains.status = 'active'"
      sth_size = sth.split(" ").size rescue 2
      if sth_size.eql?(1) && /\D/.match(sth).blank?
        qry_condition += " AND chains.id = '#{sth.gsub(" ","")}'"
      else
        qry_condition += " AND lower(chains.name) like '%#{sth.downcase}%'"
      end
      qry_condition += " AND lower(application_keys.appkey) like '%#{sah.downcase}%'" if sah.present?

      if order.to_i < 1000
        qry_order = "#{ar_field[order.to_i]}"
        @chains = chains.where(qry_condition).order(qry_order).paginate(page: params[:page], per_page: 10)
      elsif order.to_i > 999
        order = order.to_i - 1000
        qry_order = "#{ar_field[order]}"
        @chains = chains.where(qry_condition).order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
      else
        @chains = chains.where(qry_condition).paginate(page: params[:page], per_page: 10)
      end
    else
      @chains = chains.active.paginate(page: params[:page], per_page: 10) #Chain.all
    end

    respond_to do |format|
      format.js
      format.html
    end
  end

  def dashboard_setting
    @chain = Chain.find(params['id'])
  end

  def new_avg_survey_setting
    @chain = Chain.find(params['id'])
    @status = REDIS.get "avg_survey_setting_#{@chain.id}"
    @status = @status.blank? ? "on" : "off"
    render :layout => false
  end

  def avg_survey_setting
   @chain = Chain.find(params['id'])
   status = params["status"]
   if status.downcase == "off"
     REDIS.set "avg_survey_setting_#{@chain.id}", "true"
   else
     REDIS.set "avg_survey_setting_#{@chain.id}", nil
   end
   redirect_to dashboard_setting_admin_chain_path(@chain) , :notice => "Update Setting avg Survey for dashboard"
 end

 def new_email_parse
  @chain = Chain.find params[:id]
  @subject = REDIS.get "subject_email_parse_user_reject_#{@chain.id}"
  @content = REDIS.get "content_email_parse_user_reject_#{@chain.id}"
  @on_off = REDIS.get "content_email_parse_user_on_off_#{@chain.id}"

end

def create_email_parse
  @chain = Chain.find params[:id]
  REDIS.set "subject_email_parse_user_reject_#{@chain.id}", params['subject']
  REDIS.set "content_email_parse_user_reject_#{@chain.id}", params['content']
  REDIS.set "content_email_parse_user_on_off_#{@chain.id}", params['send_email']
  @subject = REDIS.get "subject_email_parse_user_reject_#{@chain.id}"
  @content = REDIS.get "content_email_parse_user_reject_#{@chain.id}"
  @chain.update_column(:email_url, params['email_parse_url'])
  flash[:notice] = "Email Parse updated"
  redirect_back(fallback_location: root_path) 
end


def update_hours
  @chain = Chain.find(params["chain_id"])
  @hour = params["chain_delay"]
  @save = @chain.update_attributes(:hours_delay_reward => @hour)
end

def chain_share_images
  rescue_from_cancan(:chain_share_images, Chain)
    @chains = Chain.includes(:owners, :locales).active.paginate(page: params[:page], per_page: 10) #Chain.all
  end

  # GET /chains/1
  def show
    rescue_from_cancan(:show, Chain)
    @chain = Chain.find(params[:id])
    @restaurants = @chain.restaurants.paginate(page: params[:page], per_page: 10)
    @offers = @chain.offers.all
    @keys = REDIS.hkeys "chain_#{@chain.id}"
    @warning_keys = REDIS.hkeys "chain_#{@chain.id}_warning_claim"
    @reward_notifications = REDIS.hkeys "chain_#{@chain.id}_reward_notif"
    respond_to do |format|
      format.js
      format.html
    end
  end

  # GET /chains/new
  def new
    rescue_from_cancan(:new, Chain)
    @chain = Chain.new
    @chain.build_chain_setting
  end

  # GET /chains/1/edit
  def edit
    rescue_from_cancan(:edit, Chain)
    @chain = Chain.find(params[:id])
  end

  def generate_warning_template
    @chain = Chain.find(params[:id])
    chain_locales = @chain.chain_locales
    unless chain_locales.blank?
      chain_locales.each do |c_locale|
        c_locale.save_warning_claim_template_to_redis
      end
    end
    flash[:notice] = 'Warning Template was successfully created.'
    redirect_to admin_chain_url(@chain)
  end

  # POST /chains
  def create
    rescue_from_cancan(:create, Chain)
    @chain = Chain.new(chain_params)
    @chain.email_validation = true
    respond_to do |format|
      if @chain.save
        ## generating default data for guest relation default settings
        REDIS.set "guest_relation_setting_reward_#{@chain.id}", "true"
        REDIS.set "guest_relation_setting_points_#{@chain.id}", "true"
        #        ChainMailer.welcome_email(@chain).deliver
        format.html { redirect_to admin_chain_url(@chain), notice: 'Chain was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /chains/1
  def update
    rescue_from_cancan(:update, Chain)
    @chain = Chain.find(params[:id])
    @chain.email_validation = true
    respond_to do |format|
      if @chain.update(chain_params)
        format.html { redirect_to admin_chain_url(@chain), notice: 'Chain was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def search_chain
    sth = params[:search_text_hidden]
    sah = params[:search_appkey_hidden]
    ar_field = [
      "chains.id",
      "chains.name",
      "owners.first_name",
      "locales.name",
      "chains.points",
      "chains.user_points",
      "chains.email",
      "chains.survey_email",
      "chains.hours_delay_reward",
      "chains.user_limit_receipt_approved_per_day"
    ]

    order = params[:qry_order]

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @chains = Chain.includes(:owners, :locales, :applications => :application_keys).where("chains.status = 'active' AND lower(chains.name) like '%#{sth.downcase}%' AND lower(application_keys.appkey) like '%#{sah.downcase}%'").order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @chains = Chain.includes(:owners, :locales, :applications => :application_keys).where("chains.status = 'active' AND lower(chains.name) like '%#{sth.downcase}%' AND lower(application_keys.appkey) like '%#{sah.downcase}%'").order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    else
      @chains = Chain.includes(:owners, :locales, :applications => :application_keys).where("chains.status = 'active' AND lower(chains.name) like '%#{sth.downcase}%' AND lower(application_keys.appkey) like '%#{sah.downcase}%'").paginate(page: params[:page], per_page: 10)
    end

  end

  # DELETE /chains/1
  #  def destroy
  #    rescue_from_cancan(:destroy, Chain)
  #    @chain = Chain.find(params[:id])
  #
  #    if @chain.restaurants.count == 0
  #      @chain.chains_owners.destroy_all
  #      @chain.destroy
  #    end
  #
  #    redirect_to admin_chains_url
  #  end
  def surveys
    @opt = Survey.find_all_by_chain_id(params[:id]).sort{|a,b| a[:title].downcase <=> b[:title].downcase}
    response = @opt.as_json(:only=>[:id, :title])

    response  = [:id => nil, :title => "No survey"] + response  if params['online'] == "true"
    #arr1 = arr1 + arr2 unless arr2.blank?
    render :json => response.to_json
  end

  def restaurants
    @opt = Restaurant.select('restaurants.id, restaurants.name').where("restaurants.chain_id = ?", params[:id]).order("name asc") rescue []
    @chain = Chain.find params[:id]
    render :json => @opt
  end

  def delete_email_template
    chain = Chain.find(params[:id])
    REDIS.hdel "chain_#{chain.id}", params[:key]
    redirect_to admin_chain_url(chain), notice: 'Email template was successfully deleted.'
  end

  def delete_warning_template
    chain = Chain.find(params[:id])
    REDIS.hdel "chain_#{chain.id}_warning_claim", params[:key]
    redirect_to admin_chain_url(chain), notice: 'Warning Chain template was successfully deleted.'
  end

  def delete_reward_notif
    chain = Chain.find(params[:id])
    REDIS.hdel "chain_#{chain.id}_reward_notif", params[:key]
    redirect_to admin_chain_url(chain), notice: 'New Reward Notification for chain was successfully deleted.'
  end

  def edit_email_template
    @chain = Chain.find(params[:id])
    @key = params[:key]
    @content = REDIS.hget "chain_#{@chain.id}", @key
  end

  def edit_warning_template
    @chain = Chain.find(params[:id])
    @key = params[:key]
    @content = REDIS.hget "chain_#{@chain.id}_warning_claim", @key
    @content = JSON.parse(@content) rescue {:question => nil, :body => nil}
  end

  def edit_reward_notif
    @chain = Chain.find(params[:id])
    @key = params[:key]
    @content = REDIS.hget "chain_#{@chain.id}_reward_notif", @key
    @content = JSON.parse(@content) rescue {:body => nil}
  end

  def new_email_template
    @chain = Chain.find(params[:id])
  end

  def new_warning_template
    @chain = Chain.find(params[:id])
  end

  def new_reward_notif
    @chain = Chain.find(params[:id])
  end

  def add_warning_template
    chain = Chain.find(params[:id])
    key = params["warning_template"]["key"]

    if key.blank?
      locale = Locale.find(params[:warning_template][:locale])
      key = locale.key+"_warning_claim"
    end

    question = params[:warning_template][:question]
    content = params[:warning_template][:body]
    stored = {:question => question , :body => content}
    REDIS.hdel "chain_#{chain.id}_warning_claim", key
    REDIS.hset "chain_#{chain.id}_warning_claim", key, stored.to_json
    redirect_to admin_chain_url(chain), notice: 'Warning Claim template was successfully saved.'
  end

  def add_reward_notif
    chain = Chain.find(params[:id])
    key = params["reward_notif"]["key"]

    if key.blank?
      locale = Locale.find(params[:reward_notif][:locale])
      key = locale.key+"_reward_notif"
    end
    puts "aaaaaaaaaaaaaaaaaaaaaaa"
    content = params[:reward_notif][:body]
    puts content
    stored = {:body => content}
    puts stored
    REDIS.hdel "chain_#{chain.id}_reward_notif", key
    REDIS.hset "chain_#{chain.id}_reward_notif", key, stored.to_json
    puts key
    redirect_to admin_chain_url(chain), notice: 'Reward Notification was successfully saved.'
  end

  def add_email_template
    chain = Chain.find(params[:id])
    locale = Locale.find(params[:email_template][:locale])
    code = params[:email_template][:code]
    content = params[:email_template][:content]
    REDIS.hdel "chain_#{chain.id}", "#{locale.key}_#{code}"
    REDIS.hset "chain_#{chain.id}", "#{locale.key}_#{code}", content
    redirect_to admin_chain_url(chain), notice: 'Email template was successfully saved.'
  end

  def reward_timer_setting
    @chain = Chain.find(params[:id])
  end

  def new_dashboard_signature
    @chain = Chain.find(params[:id])
    @signature = REDIS.get "chain_#{@chain.id}_signature"
  end

  def create_dashboard_signature
    chain = Chain.find(params[:id])

    content = params['signature']
    #REDIS.hdel "chain_#{chain.id}_signature"
    REDIS.set "chain_#{chain.id}_signature",content
    redirect_to new_dashboard_signature_admin_chain_path(chain), notice: "Signature template #{chain.name} was successfully saved."
  end

  def create_email_template
    chain = Chain.find(params[:id])
    key = params[:email_template][:key]
    content = params[:email_template][:content]
    REDIS.hdel "chain_#{chain.id}",key
    REDIS.hset "chain_#{chain.id}", key, content
    redirect_to admin_chain_url(chain), notice: 'Email template was successfully saved.'
  end

  def change_status
    chain = Chain.find(params[:id])
    chain.send(params[:status])
    chain.save
    if params[:status] == "activate"
      chain.add_ldap_chain
    else
      chain.remove_ldap_chain
    end
    redirect_to admin_chains_url, notice: 'Chain has been already updated.'
  end

  def deactivated
    rescue_from_cancan(:index, Chain)
    @chains = Chain.includes(:owners, :locales).inactive.paginate(page: params[:page], per_page: 10) #Chain.all
  end

  def new_reward_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.notification_locales.map{|gl| gl.locale}).each do |locale|
      @chain.notification_locales.build(:locale => locale)
    end
  end

  def new_birthday_reward_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.birthday_reward_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.birthday_reward_notifications.build(:locale => locale)
    end
  end

  def new_referral_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.referral_locales.map{|gl| gl.locale}).each do |locale|
      @chain.referral_locales.build(:locale => locale)
    end
  end

  def new_gift_notification
    @chain = Chain.find params[:id]
    p "----"*40
    p @chain.locales - @chain.gift_notifications.map{|gl| gl.locale}
    (@chain.locales - @chain.gift_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.gift_notifications.build(:locale => locale)
    end
  end

  def new_milestone_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.milestone_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.milestone_notifications.build(:locale => locale)
    end
  end

  def new_game_notification
    @chain = Chain.find params[:id]
    p "----"*40
    p @chain.locales - @chain.game_notifications.map{|gl| gl.locale}
    (@chain.locales - @chain.game_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.game_notifications.build(:locale => locale)
    end
  end

  def new_reset_email_notification
    @chain = Chain.find params[:id]
    p "----"*40
    p @chain.locales - @chain.reset_email_notifications.map{|gl| gl.locale}
    (@chain.locales - @chain.reset_email_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.reset_email_notifications.build(:locale => locale)
    end
  end

  def new_steppingstone_notification
    @chain = Chain.find params[:id]
    p "----"*40
    p @chain.locales - @chain.steppingstone_notifications.map{|gl| gl.locale}
    (@chain.locales - @chain.steppingstone_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.steppingstone_notifications.build(:locale => locale)
    end
  end

  def create_reward_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Reward notification setting has been already updated.'
    else
      render "new_reward_notification"
    end
  end

  def create_birthday_reward_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Birthday Reward notification setting has been already updated.'
    else
      render "new_birthday_reward_notification"
    end
  end

  def create_referral_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Email referral notification setting has been already updated.'
    else
      render "new_referral_notification"
    end
  end

  def create_gift_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Email gift notification setting has been already updated.'
    else
      render "new_gift_notification"
    end
  end

  def create_game_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Game notification setting has been already updated.'
    else
      render "new_game_notification"
    end
  end

  def create_reset_email_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Reset Email notification setting has been already updated.'
    else
      render "new_reset_email_notification"
    end
  end

  def create_steppingstone_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Steppingstone notification setting has been already updated.'
    else
      render "new_steppingstone_notification"
    end
  end

  def daily_report
    @chain = Chain.find params[:id]
    @report_value = @chain.generate_daily_report_for_chain_owner
  end

  def weekly_report

  end

  def new_point_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.point_push_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.point_push_notifications.build(:locale => locale)
    end
  end

  def create_point_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Point notification setting has been already updated.'
    else
      render "new_point_notification"
    end
  end

  #account notification
  def new_account_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.account_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.account_notifications.build(:locale => locale)
    end
  end

  def create_account_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Account notification setting has been already updated.'
    else
      render "new_account_notification"
    end
  end

  def create_milestone_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Milestone notification setting has been already updated.'
    else
      render "new_milestone_notification"
    end
  end

  #reward share text
  def new_reward_share_text
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.reward_share_texts.map{|gl| gl.locale}).each do |locale|
      @chain.reward_share_texts.build(:locale => locale)
    end
  end

  def create_reward_share_text
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Reward Share Text has been already updated.'
    else
      render "new_reward_share_text"
    end
  end

  #surveys response alert
  def new_survey_response_alert
    @chain = Chain.find params[:id]
    @survey_response_alert = @chain.survey_response_alert.blank? ? @chain.build_survey_response_alert : @chain.survey_response_alert
    (@chain.locales - @survey_response_alert.notification_locales.map{|gl| gl.locale}).each do |locale|
      @survey_response_alert.notification_locales.build(:locale => locale)
    end
  end

  def create_survey_response_alert
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'new survey response alert updated.'
    else
      render "new_survey_response_alert"
    end
  end

  #game point notification settings
  def new_game_point_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.game_point_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.game_point_notifications.build(:locale => locale)
    end
  end

  def create_game_point_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Game Point Notification setting has been already updated.'
    else
      render "new_game_point_notification"
    end
  end

  #admin push - reward notification setting
  def new_admin_push_reward_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.admin_push_reward_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.admin_push_reward_notifications.build(:locale => locale, :send_type => 1)
    end
  end

  def create_admin_push_reward_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Admin Push Reward Notification setting has been already updated.'
    else
      render "new_admin_push_reward_notification"
    end
  end

  #admin push - point notification setting
  def new_admin_push_point_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.admin_push_point_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.admin_push_point_notifications.build(:locale => locale, :send_type => 2)
    end
  end

  def create_admin_push_point_notification
    @chain = Chain.find params[:id]
    if @chain.update_attributes(chain_params)
      redirect_to admin_chains_url, notice: 'Admin Push Reward Notification setting has been already updated.'
    else
      render "new_admin_push_reward_notification"
    end
  end

  def new_admin_rounding_setting
    @chain = Chain.find params[:id]
  end

  def create_admin_rounding_setting
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Point Rounding Setting has been already updated.'
    else
      render "new_admin_rounding_setting"
    end
  end

  #mailchimp setting
  def new_mailchimp_setting
    @chain = Chain.find params[:id]
  end

  def create_mailchimp_setting
    @chain = Chain.find params[:id]
    @chain.user_sync_required = true
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Mailchimp setting has been updated.'
    else
      render "new_mailchimp_setting"
    end
  end

  def new_emma_setting
    @chain = Chain.find params[:id]
    if @chain.emma_setting.nil?
      @emma_setting = EmmaSetting.new
    else
      @emma_setting = @chain.emma_setting
    end
  end

  def create_emma_setting
    @chain = Chain.find params[:id]
    @chain.user_sync_required = true
    @chain.emma_ignore_marketing_optin = params[:emma_ignore_marketing_optin]
    @chain.save
    if @chain.emma_setting.nil?
      @emma_setting = EmmaSetting.new(emma_settings_params)
      @emma_setting.chain_id = @chain.id
      if @emma_setting.save
        redirect_to admin_chains_url, notice: 'Emma setting has been updated.'
      end
    else
      if @chain.emma_setting.update_attributes(params[:emma_setting])
        redirect_to admin_chains_url, notice: 'Emma setting has been updated.'
      end
    end
  end

  def sync_now_emma
    @chain = Chain.find params[:id]
    if @chain.emma_setting.present?
      ml = EmmaLog.create(:chain_id => @chain.id, :log_type => "Sync Now Emma", :status => "in queue")
      dl = Delayed::Job.enqueue(EmmaJob.new(@chain, ml), :queue => 'synch_marketing_job')
      @chain.emma_setting.update_column(:emma_log_id, ml.id)
    end
  end

   #momentfeed setting
   def new_momentfeed_setting
    @chain = Chain.find params[:id]
    if @chain.momentfeed_setting.nil?
      @momentfeed_setting = MomentfeedSetting.new
    else
      @momentfeed_setting = @chain.momentfeed_setting
    end
  end

  def create_momentfeed_setting
    @chain = Chain.find params[:id]
    if @chain.momentfeed_setting.nil?
      @momentfeed_setting = MomentfeedSetting.new(params[:momentfeed_setting])
      @momentfeed_setting.chain_id = @chain.id
      if !params[:momentfeed_setting][:api_key].blank? && !params[:momentfeed_setting][:base_url].blank? && @momentfeed_setting.save
        redirect_to admin_chains_url, notice: 'Momentfeed setting has been updated.'
      else
        render 'new_momentfeed_setting'
      end
    else
      @momentfeed_setting = @chain.momentfeed_setting
      if params[:momentfeed_setting][:api_key].blank? && params[:momentfeed_setting][:base_url].blank?
        @momentfeed_setting.destroy
        redirect_to admin_chains_url, notice: 'Momentfeed setting has been removed.'
      else
        if @momentfeed_setting.update_attributes(params[:momentfeed_setting])
          redirect_to admin_chains_url, notice: 'Momentfeed setting has been updated.'
        else
          render 'new_momentfeed_setting'
        end
      end
    end
  end

  def manual_sync_momentfeed_settings
    chain = Chain.find(params[:id])
    unless chain.momentfeed_setting.blank?
      json_url = "#{chain.momentfeed_setting.base_url}?api_token=#{chain.momentfeed_setting.api_key}"
      Delayed::Job.enqueue(MomentfeedImportRestaurantsJob.new(chain)) if chain.momentfeed_setting.base_url
      redirect_to admin_chains_path, notice: "Manual sync restaurant json executed"
    else
      flash[:error] = "Momentfeed settings cannot be blank"
      render 'new_momentfeed_setting'
    end
  end

  def new_launch_text
    @chain = Chain.find params[:id]
    if @chain.launch_texts.blank?
      @chain.launch_texts.build
    end
  end

  def create_new_launch_text
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Launch text has been updated.'
    else
      render "new_launch_text"
    end
  end

  def create_api_response_text
    @chain = Chain.find params[:id]
    @is_save = @chain.update(chain_params)
    @type = params[:type]
    @parameterize = @type.parameterize("_")+"_texts" rescue nil
  end

  def new_survey_incentive_threshold
    @chain = Chain.find params[:id]
  end

  def create_survey_incentive_threshold
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Survey Incentive Threshold has been updated.'
    else
      render "new_survey_incentive_threshold"
    end

  end

  def new_reward_expiration_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.reward_exp_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.reward_exp_notifications.build(:locale => locale)
    end
  end

  def create_reward_expiration_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Reward expiration notification has been updated.'
    else
      render "new_reward_expiration_notification"
    end
  end

  def new_active_reward_notification
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.active_reward_notifications.map{|gl| gl.locale}).each do |locale|
      @chain.active_reward_notifications.build(:locale => locale)
    end
  end

  def create_active_reward_notification
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Active reward notifications has been updated.'
    else
      render "new_active_reward_notification"
    end
  end

  def new_giftcard_notification_email
    @chain = Chain.find params[:id]
    (@chain.locales - @chain.giftcard_notification_emails.map{|gl| gl.locale}).each do |locale|
      @chain.giftcard_notification_emails.build(:locale => locale)
    end
  end

  def create_giftcard_notification_email
    @chain = Chain.find params[:id]
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Giftcard Notification Email has been already updated.'
    else
      render "new_giftcard_notification_email"
    end
  end

  def import_chain

  end

  def import_preview
    test_file = params[:xls]
    book = Spreadsheet.open "#{test_file.path}"
    puts book
    sheet1 = book.worksheet 0

    @chains = []
    index = 0
    sheet1.each 1 do |row|
      puts row
      res = Chain.new
      res.id = index

      res.name = row[0]
      res.points = row[1]
      res.email = row[2]
      res.timer = row[3]
      index = index + 1
      @chains << res
    end
    Rails.cache.write('chain_import', @chains, :time_to_idle => 60.seconds, :timeToLive => 600.seconds)

    respond_to do |format|
      format.html {
        render :template => "admin/chains/import_chain"
      }
      format.js
    end
  end

  def create_import
    chains = Rails.cache.read('chain_import')
    chains = chains.uniq.select{|x|  params[:chains].include?(x.id.to_s)} unless params[:chains].blank?

    chains.each do |chain|
      chain.id = nil
      chain.save
    end
    notice = params[:chains].blank? ? "Please select at least one chain" : "Successfully import #{chains.size} chains"
    redirect_to import_chain_admin_chains_path  , notice: notice
  end

  def daily_report_test

  end

  def get_data_jasper
    start_date = params[:start_date].to_date rescue nil
    end_date =   params[:end_date].to_date   rescue nil
    report = Report.where("chain_id = ? and start_date = ? and end_date = ?", params[:chain_id],start_date,
      end_date).first
    chain = Chain.find params[:chain_id]
    unless report.blank?
      report.destroy
    end
    owners = Owner.where("role_id = 1 and chain_id = ?",chain.id).joins(:chains_owners)
    owners.each do |owner|
      Delayed::Job.enqueue(ReportJob::DailyJob.new(chain.id,"daily",start_date,end_date, owner,"PDF"))
    end

    redirect_to "/admin/daily_report_test", :notice => "Your daily report #{start_date} - #{end_date} is processing"
  end

  def rewards_pos_codes
    @chain = Chain.find params[:id]
    @rewards = @chain.rewards.paginate(:page => params[:page], :per_page => 10)
  end

  def sync_now
    @chain = Chain.find params[:id]
    if @chain.mailchimp_sync
      ml = MailchimpLog.create(:chain_id => @chain.id, :log_type => "Sync Now", :status => "in queue")
      dl = Delayed::Job.enqueue(EmailMarketingJob.new(@chain, ml), :queue => 'synch_marketing_job')
      @chain.update_column(:mailchimp_log_id, ml.id)
    end
  end

  ## dashboard 2.3
  def new_guest_relation_setting
    @chain = Chain.find params[:id]
    @cr = REDIS.get "guest_relation_setting_reward_#{@chain.id}"
    @cr = !@cr.blank?
    @pr = REDIS.get "guest_relation_setting_points_#{@chain.id}"
    @pr = !@pr.blank?
  end

  def update_guest_relation_setting
    @chain= Chain.find params[:id]
    if params[:val] == "ON"
      REDIS.set "guest_relation_setting_#{params[:mode]}_#{@chain.id}", "true"
    else
      REDIS.set "guest_relation_setting_#{params[:mode]}_#{@chain.id}", nil
    end
    redirect_to new_guest_relation_setting_admin_chain_path(@chain)
  end

  def set_take_action_setting_chain
    @chain= Chain.find params[:id]

    REDIS.set "db_take_action_setting_chain_#{@chain.id}" , nil if params['val'] == "ON"
    REDIS.set "db_take_action_setting_chain_#{@chain.id}" , "true" if params['val'] == "OFF"
  end

  def itunes_play_store_config
    @chain= Chain.find params[:id]
    @itunes_link = REDIS.get "itunes_link_setting_chain_#{@chain.id}"
    @itunes_link_text = REDIS.get "itunes_link_text_setting_chain_#{@chain.id}"
    @play_store_link = REDIS.get "play_store_link_setting_chain_#{@chain.id}"
    @play_store_link_text = REDIS.get "play_store_link_text_setting_chain_#{@chain.id}"
  end

  def post_itunes_play_store_config
    @chain= Chain.find params[:id]
    error = ""
    if params['itunes_link'] && !params['itunes_link']['itunes_link_text'].blank?
      @itunes_link_text =  REDIS.set "itunes_link_text_setting_chain_#{@chain.id}", params['itunes_link']['itunes_link_text']
    else
      error += "Itunes link text could not be blank "
    end

    if is_url?(params['itunes_link']['itunes'])
      @itunes_link = REDIS.set "itunes_link_setting_chain_#{@chain.id}" , params['itunes_link']['itunes']
    else
      error += "Itunes url is not an url "
    end

    if params['itunes_link'] && !params['itunes_link']['play_store_link_text'].blank?
      @play_store_link_text =  REDIS.set "play_store_link_text_setting_chain_#{@chain.id}", params['itunes_link']['play_store_link_text']
    else
      error += "Play Store link text could not be blank "
    end

    if is_url?(params['itunes_link']['play_store'])
      @play_store_link = REDIS.set "play_store_link_setting_chain_#{@chain.id}", params['itunes_link']['play_store']
    else
      error += "| Playstore url is not an url"
    end

    note = error
    note = "Link Updated" if error.blank?

    flash[:notice] = "#{note}"
    redirect_back(fallback_location: root_path) 
  end

  def load_survey_question
    @survey = Survey.find params[:id] rescue nil
  end

  def set_considered_question
    if params[:type] == "question"
      question = Question.find params[:id]
      value = params[:value] == "1" ? true : false
      question.update_column(:is_considered_for_negative_response, value)
      @survey = question.survey
    elsif params[:type] == "choice"
      choice = QuestionChoice.find params[:id]
      value = params[:value] == "1" ? true : false
      choice.update_column(:is_considered_for_negative_response, value)
      question = choice.question
      @survey = question.survey
    end
  end

  def set_considered_rotating_question
    if params[:type] == "question"
      question = Question.find params[:id]
      value = params[:value] == "1" ? true : false
      question.update_column(:is_considered_for_negative_response, value)
      @rotating_group = question.rotating_group
    elsif params[:type] == "choice"
      choice = QuestionChoice.find params[:id]
      value = params[:value] == "1" ? true : false
      choice.update_column(:is_considered_for_negative_response, value)
      question = choice.question
      @rotating_group = question.rotating_group
    end
  end

  def load_survey_rotating_question
    @rotating_group = RotatingGroup.find params[:id] rescue nil
  end

  def post_region_report
    chain = Chain.find(params['chain_id'])
    restaurant_ids = chain.restaurants.map(&:id)
    tag_ids = Tag.joins(:restaurants).where("taggable_type = 'Restaurant' and taggable_id in(?)",restaurant_ids).map(&:id)
    tag_ids = tag_ids.uniq
    @tags_ids = []
    params_string = "&this_period_start=#{params['this_period_start']}"
    params_string += "&this_period_end=#{params['this_period_end']}"
    params_string += "&last_period_start=#{params['last_period_start']}"
    params_string += "&last_period_end=#{params['last_period_end']}"
   # puts params['tag_id'].size
   # puts tag_ids.size
   if tag_ids.size == 0 or params['tag_id'].size == tag_ids.size
      ## all report
      puts "in to this......"
      @jasper_path = "/reports/trelevant/new_region_report"
      @jasper_path ="/reports/trelevant/region_report" if ENV["DEFAULT_HOST_EMAIL"].include?('trelevant.herokuapp.com')
    else
      @jasper_path = "/reports/trelevant/new_region_report_1"
      params_string += "&tags=#{URI.encode(params['tag_id'].join(',').gsub(' ','_'))}"
    end
    @file_type =  "csv"
    puts "file type = #{@file_type}"


    params_string += "&chain_id=#{params['chain_id']}"
    url = "#{Setting.jasper.server}/rest_v2/reports#{@jasper_path}.#{@file_type.downcase}?#{params_string}"

    x = ReportSubscription.first
    cookies = x.authenticate_jasper
    #url = URI.parse(url)
    puts "file_path #{url}"
    puts cookies
    response_get_report = RestClient.get url, cookies
    puts response_get_report
    file_for_name = "tag_report This Period: #{params['this_period_start']}_#{params['this_period_end']}  Last Perod: #{params['last_period_start']} - #{params['last_period_end']}.csv"
    path = "#{Rails.root}/tmp/test_jasper_file.#{@file_type}"
    File.open(path, "w+") { |f| f << response_get_report.force_encoding("utf-8") }
    OwnerMailer.send_reward_redeemed(nil,current_admin.email,
     "#{Rails.root}/tmp/test_jasper_file.csv",
     file_for_name , "Tag Report for chain").deliver!
    redirect_to region_report_admin_chains_path , :notice => "It will send to your email shortly"
  end

  def region_report
    respond_to do |format|
      format.csv {
      #  send_file "#{Rails.root}/tmp/test_jasper_file.csv"

    }
    format.html {}
  end
end

def post_grouping_region_report
  chain = Chain.find(params['chain_id'])
  restaurant_ids = chain.restaurants.map(&:id)
  tag_ids = Tag.joins(:restaurants).where("taggable_type = 'Restaurant' and taggable_id in(?)",restaurant_ids).map(&:id)
  tag_ids = tag_ids.uniq
  @tags_ids = []
    #all_location = params['tag_id'] unless params['tag_id'].blank?#tag_ids
    group2 = params['tag_id'] unless params['tag_id'].blank?#tag_ids
    group3 = params['tag_id2'] unless params['tag_id2'].blank?#tag_ids
    period1 = {:start_date => params['this_period_start'],:end_date => params['this_period_end']}
    period2 = {:start_date => params['last_period_start'],:end_date => params['last_period_end']}
    period2 =  nil if params['last_period_start'].blank? || params['last_period_end'].blank?

    Delayed::Job.enqueue(ReportJob::GroupingTagRewardJob.new(period1, period2, group2, group3, nil, chain, params['email']))
    redirect_to grouping_region_report_admin_chains_path , :notice => "It will send to your email shortly"
  end

  def reward_count_list
    respond_to do |format|
      format.csv {
        #  send_file "#{Rails.root}/tmp/test_jasper_file.csv"

      }
      format.html {}
    end
  end

  def post_reward_count_list
    chain = Chain.find(params['chain_id'])
    restaurant_ids = chain.restaurants.map(&:id)
    tag_ids = Tag.joins(:restaurants).where("taggable_type = 'Restaurant' and taggable_id in(?)",restaurant_ids).map(&:id)
    tag_ids = tag_ids.uniq
    @tags_ids = []
    #all_location = params['tag_id'] unless params['tag_id'].blank?#tag_ids
    group2 = params['tag_id'] unless params['tag_id'].blank?#tag_ids
    group3 = params['tag_id2'] unless params['tag_id2'].blank?#tag_ids
    period1 = {:start_date => params['this_period_start'],:end_date => params['this_period_end']}
    period2 = {:start_date => params['last_period_start'],:end_date => params['last_period_end']}
    period2 =  nil if params['last_period_start'].blank? || params['last_period_end'].blank?

    #if !params['email'].blank?
    Delayed::Job.enqueue(ReportJob::GroupingTagRedemptionJob.new(period1, period2, group2, group3, nil, chain, params['email'], params['reward_id']))
    redirect_to reward_count_list_admin_chains_path , :notice => "It will send to your email shortly"
  end

  def geodata_report
    respond_to do |format|
      format.csv {
        #  send_file "#{Rails.root}/tmp/test_jasper_file.csv"

      }
      format.html {}
    end
  end

  def post_geodata_report
    chain = Chain.find(params['chain_id'])

    Delayed::Job.enqueue(ReportJob::ZoesGeodata.new(chain, params['email'], params['this_period_start'],params['this_period_end']))
    redirect_to geodata_report_admin_chains_path , :notice => "It will send to your email shortly"
  end

  def lapsed_list
    respond_to do |format|
      format.csv {
        #  send_file "#{Rails.root}/tmp/test_jasper_file.csv"

      }
      format.html {}
    end
  end

  def post_lapsed_list
    chain = Chain.find(params['chain_id'])
    restaurant_ids = chain.restaurants.map(&:id)
    tag_ids = Tag.joins(:restaurants).where("taggable_type = 'Restaurant' and taggable_id in(?)",restaurant_ids).map(&:id)
    tag_ids = tag_ids.uniq
    @tags_ids = []
    #all_location = params['tag_id'] unless params['tag_id'].blank?#tag_ids
    group2 = params['tag_id'] unless params['tag_id'].blank?#tag_ids
    group3 = params['tag_id2'] unless params['tag_id2'].blank?#tag_ids
    period1 = nil
    period2 = nil

    Delayed::Job.enqueue(ReportJob::GroupingLapsedUserNew.new(period1, period2, group2, group3, nil, chain, params['email']))
    Delayed::Job.enqueue(ReportJob::GroupingLapsedUserDetail.new(period1, period2, group2, group3, nil, chain, params['email']))

    redirect_to lapsed_list_admin_chains_path , :notice => "It will send to your email shortly"
  end

  def user_email_list
    respond_to do |format|
      format.csv {
        #  send_file "#{Rails.root}/tmp/test_jasper_file.csv"

      }
      format.html {}
    end
  end

  def post_user_email_list

    chain = Chain.find(params['chain_id'])
    period1 = {:start_date => params['this_period_start'], :end_date => params['this_period_end']}
    # period1 = nil

    #if !params['email'].blank?
    Delayed::Job.enqueue(ReportJob::UserEmailLocationVisit.new(chain, params['email'], period1))
    redirect_to user_email_list_admin_chains_path, :notice => "It will send to your email shortly"
    #else
    #  redirect_to user_email_list_admin_chains_path, :alert => "Please Setup The email"
    #end
  end

  def grouping_region_report
    respond_to do |format|
      format.csv {
        #  send_file "#{Rails.root}/tmp/test_jasper_file.csv"

      }
      format.html {}
    end
  end

  def survey_check_report

  end

  def pos_survey_check_report
    start_date = params["start_date"]
    puts "params start_date = #{start_date}"

    end_date = params["end_date"]
    puts "params end_date = #{end_date}"
    Delayed::Job.enqueue(ReportJob::SurveyCheckDetailJob.new(params['survey_id'], params['email'], start_date, end_date))

    redirect_to :back , :notice => "Please wait, the report will sent to your email shortly."
  end

  def region_match
    unless params['reward'].blank?
      @chain = Chain.find params[:h_chain_id]
    end
    @restaurants = Restaurant.where(:chain_id => params[:h_chain_id]).order("name asc") rescue []
  end

  def grouping_region_match
    unless params['reward'].blank?
      @chain = Chain.find params[:h_chain_id]
    end
    @restaurants = Restaurant.where(:chain_id => params[:h_chain_id]).order("name asc") rescue []
  end

  def new_push_reward
    @chain = Chain.find(params[:id])
  end

  def post_push_reward
    @chain = Chain.find(params[:id])

    if @chain.update_column(:reward_timer, params[:chain][:reward_timer])
      redirect_to reward_timer_setting_admin_chain_path(@chain), notice: 'Reward Timer is updated.'
    else
      render "new_push_reward"
    end
  end

  def new_fishbowl_setting
    @chain = Chain.find(params[:id])
    @chain.user_sync_required = true
    @fishbowl_setting = @chain.fishbowl_setting
    if @fishbowl_setting.blank?
      @chain.build_fishbowl_setting
    end
  end

  def create_fishbowl_setting
    @chain = Chain.find(params[:id])
    @chain.user_sync_required = true
    if @chain.update(chain_params)
      redirect_to admin_chains_url, notice: 'Fishbowl setting has been updated.'
    else
      render "new_fishbowl_setting"
    end
  end

  def remove_fishbowl_setting
    @chain = Chain.find(params[:id])
    @chain.fishbowl_setting.destroy
    Delayed::Job.enqueue(ChainFishbowlUserJob.new(record, "remove"))
    redirect_to admin_chains_url, notice: 'Fishbowl setting has been deleted.'
  end

  def fishbowl_initial_sync
    @chain = Chain.find params[:id]
    if @chain.fishbowl_setting
      fl = FishbowlLog.create(:chain_id => @chain.id, :log_type => "Initial Sync", :status => "in queue")
      dl = Delayed::Job.enqueue(FishbowlJob.new(@chain, fl), :queue => 'synch_marketing_job')
      @chain.fishbowl_setting.update_column(:fishbowl_log_id, fl.id)
    end
    redirect_to admin_chains_path, :notice => "Initial Sync is processing for chain #{@chain.name}"
  end

  def fishbowl_sync_now
    @chain = Chain.find params[:id]
    if @chain.fishbowl_setting
      fl = FishbowlLog.create(:chain_id => @chain.id, :log_type => "Sync Now", :status => "in queue")
      dl = Delayed::Job.enqueue(FishbowlJob.new(@chain, fl))
      @chain.fishbowl_setting.update_column(:fishbowl_log_id, fl.id)
    end
  end


  def new_pay_code_setting
    @chain = Chain.find params[:id]
    if @chain.pay_code_setting.blank?
      @chain.build_pay_code_setting(AppcodeSetting::DEFAULT_SETTING_PAYCODE)
    end
  end


  def create_pay_code_setting
    @chain = Chain.find(params[:id])
    if @chain.update_attributes(chain_params)
      REDIS.set "paycode_chain_#{@chain.id}", @chain.pay_code_setting.to_json
      redirect_to admin_chains_url, notice: 'Pay code setting has been updated.'
    else
      render "new_pay_code_setting"
    end
  end

  def new_user_code_setting
    @chain = Chain.find params[:id]
    if @chain.user_code_setting.blank?
      @chain.build_user_code_setting(AppcodeSetting::DEFAULT_SETTING_USERCODE)
    end
  end

  def create_user_code_setting
    @chain = Chain.find(params[:id])
    if @chain.update_attributes(chain_params)
      user_code_setting = @chain.user_code_setting
      user_code_setting = JSON.parse(user_code_setting.to_json) rescue nil
      if user_code_setting && user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, @chain.id, AppcodeSetting::CODE_TYPES["USERCODE"]).count
      elsif user_code_setting && user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + user_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, @chain.id, AppcodeSetting::CODE_TYPES["USERCODE"]).count
      end
      @chain.update_column(:user_code_count, code_count)
      redirect_to admin_chains_url, notice: 'User code setting has been updated.'
    else
      render "new_user_code_setting"
    end
  end

  def new_receipt_code_setting
    @chain = Chain.find params[:id]
    if @chain.receipt_code_setting.blank?
      @chain.build_receipt_code_setting(AppcodeSetting::DEFAULT_SETTING_RECEIPTCODE)
    end
  end

  def create_receipt_code_setting
    @chain = Chain.find(params[:id])
    if @chain.update_attributes(chain_params)
      REDIS.set "receiptcode_chain_#{@chain.id}", @chain.receipt_code_setting.to_json
      redirect_to admin_chains_url, notice: 'Receipt code setting has been updated.'
    else
      render "new_receipt_code_setting"
    end
  end

  def new_reward_code_setting
    @chain = Chain.find params[:id]
    if @chain.reward_code_setting.blank?
      @chain.build_reward_code_setting(AppcodeSetting::DEFAULT_SETTING_REWARDCODE)
    end
    if @chain.fishbowl_promotion_setting.blank?
      @chain.build_fishbowl_promotion_setting
    end
  end

  def create_reward_code_setting
    @chain = Chain.find(params[:id])
    if @chain.update_attributes(chain_params)
      REDIS.set "rewardcode_chain_#{@chain.id}", @chain.reward_code_setting.to_json
      redirect_to admin_chains_url, notice: 'Reward redeemption code setting has been updated.'
    else
      render "new_reward_code_setting"
    end
  end

  def new_gift_code_setting
    @chain = Chain.find params[:id]
    if @chain.gift_code_setting.blank?
      @chain.build_gift_code_setting(AppcodeSetting::DEFAULT_SETTING_GIFTCODE)
    end
  end

  def create_gift_code_setting
    @chain = Chain.find(params[:id])
    if @chain.update_attributes(chain_params)
      gift_code_setting = @chain.gift_code_setting
      gift_code_setting = JSON.parse(gift_code_setting.to_json) rescue nil

      if gift_code_setting && gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, @chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).count
      elsif gift_code_setting && gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, @chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).count
      end
      @chain.update_column(:gift_code_count, code_count)
      redirect_to admin_chains_url, notice: 'Gift code setting has been updated.'
    else
      render "new_gift_code_setting"
    end
  end

  def new_btree_setting
    @chain = Chain.find params[:id]
    if @chain.btree_setting.blank?
      @chain.build_btree_setting
    end
  end

  def create_btree_setting
    @chain = Chain.find(params[:id])
    if @chain.update_attributes(chain_params)
      redirect_to admin_chains_url, notice: 'Btree setting has been updated.'
    else
      render "new_btree_setting"
    end
  end

  def new_netpos_setting
    @chain = Chain.find params[:id]
    if @chain.netpos_setting.blank?
      @chain.build_netpos_setting
    end
  end

  def create_netpos_setting
    @chain = Chain.find(params[:id])
    if @chain.update_attributes(chain_params)
      redirect_to admin_chains_url, notice: 'Netpos setting has been updated.'
    else
      render "new_netpos_setting"
    end
  end



  #def get_data_jasper
  #    report_server = "http://fc.relevantmobile.com:8080/jasperserver"
  #    report_user = "jasperadmin"
  #    report_password = "jasperadmin"
  #
  #    #login to JasperServer and acquire cookies for its session
  #    response_login =
  #      RestClient.post "http://fc.relevantmobile.com:8080/jasperserver/login.html",
  #        :j_username => report_user,
  #        :j_password => report_password
  #
  #    #get resourceDescriptor before generate the report
  #    response_resource =
  #      RestClient.get "#{report_server}/rest/resource/reports/samples/daily_report/daily",
  #        {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}
  #
  #    doc = Nokogiri::XML.parse(response_resource)
  #    chain_param = Nokogiri::XML::Node.new "parameter", doc
  #    chain_param['name'] = 'chain_id'
  #    chain_param.content = 994122837#params[:chain_id]
  #
  #    start_param = Nokogiri::XML::Node.new "parameter", doc
  #    start_param['name'] = 'start_date'
  #    start_param.content = params[:start_date].to_datetime.to_i * 1000
  #
  #
  #    end_param = Nokogiri::XML::Node.new "parameter", doc
  #    end_param['name'] = 'end_date'
  #    end_param.content = params[:end_date].to_datetime.to_i * 1000
  #
  #
  #    doc.children.first.add_child(chain_param)
  #    doc.children.first.add_child(start_param)
  #    doc.children.first.add_child(end_param)
  #
  #    response_put_report =
  #      RestClient.put "#{report_server}/rest/report/reports/samples/daily_report/daily?RUN_OUTPUT_FORMAT=HTML",
  #        doc.to_s,
  #        {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}
  #
  #    #get the UUID that generated from previous action
  #    uuid = Nokogiri::XML.parse(response_put_report).xpath('//report/uuid').text
  #
  #    #download the report
  #    response_get_report =
  #      RestClient.get "#{report_server}/rest/report/#{uuid}?file=report",
  #        {:cookies => {"JSESSIONID" => response_login.cookies["JSESSIONID"]}}
  #
  #    if RAILS_ENV != "production"
  #      secret_key = Setting.storage.s3_access_key_id
  #      access_key = Setting.storage.s3_secret_access_key
  #      bucket = Setting.storage.s3_bucket
  #    else
  #      secret_key = ENV["S3_KEY"]
  #      access_key = ENV["S3_SECRET"]
  #      bucket = ENV["S3_BUCKET"]
  #    end
  #
  #
  #    aw = AWS::S3.new(
  #      :access_key_id     =>secret_key ,
  #      :secret_access_key =>access_key
  #    )
  #
  #    File.open("#{Rails.root}/tmp/test.html","w+"){|f| f << response_get_report.force_encoding("utf-8")}
  #
  #    key = "/report/#{params[:chain_id]}/#{params[:start_date].to_date.to_s}_#{params[:end_date].to_date.to_s}.html"
  #    aw.buckets[bucket].objects[key].write(:file =>  "tmp/test.html")
  #    File.delete("tmp/test.html")
  #
  #    url = aw.buckets[bucket].objects[key].url_for(:read).to_s
  #
  #    Report.find_or_create_by_start_date_and_end_date_and_chain_id(
  #      :start_date => params[:start_date].to_date,
  #      :end_date => params[:end_date].to_date,
  #      :chain_id => params[:chain_id],
  #      :s3_path => key
  #
  #    )
  #
  #    response.headers["Content-Type"] = "application/html"
  #    send_data response_get_report, :filename => "report.html"
  #  end

  def olo_setting
    @chain = Chain.find(params[:id])
  end

  def olo_update
    @chain = Chain.find(params[:id])
    if @chain.update_column("olo_url", params[:chain][:olo_url]) && @chain.update_column("olo_signature", params[:chain][:olo_signature])
      redirect_to admin_chains_path, notice: "Update Olo Setting for Chain"
    else
      render 'olo_setting'
    end
  end

  def hybrid_reward_redemption
    @chain = Chain.find(params[:id])
    @participants = Restaurant.joins(:hybrid_reward_redemption_flow).where("flow = 'app_based' AND restaurants.chain_id = ?", @chain.id )
    @non_participants = Restaurant.joins(:hybrid_reward_redemption_flow).where("flow = 'pos_based' AND restaurants.chain_id = ?", @chain.id )
  end

  def update_hybrid_reward_redemption
    @chain = Chain.find(params[:id])
    submit= params[:commit]
    case submit
    when "Move to POS integrated rules"
      HybridRewardRedemptionFlow.where("chain_id = ? AND restaurant_id in (?)", @chain.id, params[:pullout]).update_all(flow: "pos_based")
    when "Move to APP based rules"
      HybridRewardRedemptionFlow.where("chain_id = ? AND restaurant_id in (?)", @chain.id, params[:participate]).update_all(flow: "app_based")
    else
    end
    redirect_to hybrid_reward_redemption_admin_chain_path(@chain)
  end

  def revel_setting
    rescue_from_cancan(:edit, Chain)
    @chain = Chain.find(params[:id])
  end

  def location_update_json
    @chain = Chain.find(params[:id])
    @json_url = REDIS.get("json_url#{@chain.id}")
    @json_frequency = REDIS.get("json_frequency#{@chain.id}")
    @json_report_address = REDIS.get("json_report_address#{@chain.id}")
  end

  def save_location_json
    @chain = Chain.find(params[:id])
    REDIS.set("json_url#{@chain.id}", params[:json_url])
    REDIS.set("json_frequency#{@chain.id}", params[:frequency])
    REDIS.set("json_report_address#{@chain.id}", params[:report_address])
    redirect_to admin_chains_path, notice: "location json Update"
  end

  def manual_sync_restaurant_json
    chain = Chain.find(params[:id])
    json_url = REDIS.get("json_url#{chain.id}")
    if json_url
      Delayed::Job.enqueue(SmoothieKingImportRestaurantsJob.new(chain)) if json_url
      redirect_to admin_chains_path, notice: "Manual sync restaurant json executed"
    else
      flash[:error] = "Json url can't be blank"
      render 'location_update_json'
    end
  end

  private

  def is_url?(string)
    begin
      uri = URI.parse(string)
      return %w( http https ).include?(uri.scheme)
    rescue URI::BadURIError
      return false
    rescue URI::InvalidURIError
      return false
    end
  end

  def emma_settings_params
    params.require(:emma_setting).permit
  end

  def chain_params
    params.require(:chain).permit(
      :name, :points, :user_points, :email, :survey_email, :timer, :hours_delay_reward,
      :user_limit_receipt_approved_per_day, :max_subtotal, :maximum_offer_points_earned_per_day,
      :reward_timer, :max_number_of_locations, :code_scan_range, :selected_locales, :selected_owners,
      :phone_number_is_required, :unique_phone_number_required, :birthday_required,
      :check_age_required, :check_age_value, :required_unique_device_info, :require_point_signup_incentive,
      :point_signup_incentive, :fb_signup_incentive, :fb_signup_incentive_point, :ftp_url,
      :ftp_port, :ftp_username, :ftp_password, :activate_milestone_receipt,
      :reward_code_generated_type, :php_chain_id, :check_php_migration, :user_reward_redeemption_flow,
      :pos_receipt_processing_delay, :pos_used_type, :no_of_days_micros_table_receipt_generated,
      :pos_discount_setting, :netpos_batch_check_request, :auto_loyalty, :payment_processor,
      :survey_incentive_daily_cap, :survey_incentive_weekly_cap, :survey_incentive_monthly_cap,
      chain_setting_attributes: [
        :hybrid_milestone_regular_rewards_enabled, :olo_restaurant_hour_sync,
        :domain_email_validation, :user_email_confirmation, :external_partner],
      launch_texts_attributes: [
        :locale_id, :title, :subtitle
        ],
      fishbowl_setting_attributes:
        [:api_username, :api_password, :list_ids, :site_id, :ignore_marketing_optin, :real_time_sync, :welcome_email_mailing_id, :browser_welcome_email_mailing_id, :points_notification_mailing_id, :payment_notification_mailing_id, :reward_redeem_mailing_id, :we_miss_you_reward_notification, :we_miss_you_point_notification, :no_scans_7_days_mailing_id]
      )
  end


end
