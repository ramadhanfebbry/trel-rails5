class Admin::OffersController < Admin::BaseController

  # GET /offers
  def index
    rescue_from_cancan(:index, Offer)
    @offers = Offer.active_chain.paginate(page: params[:page], per_page: 10) #Offer.all

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /offers/1
  def show
    rescue_from_cancan(:show, Offer)
    @offer = Offer.active_chain.find(params[:id])
    @chain = @offer.chain
    #@participants = @offer.restaurants
    @participants = Restaurant.find_by_sql("SELECT DISTINCT ON(offer_id, restaurant_id) restaurants.* FROM restaurants INNER JOIN restaurant_offers ON restaurants.id = restaurant_offers.restaurant_id WHERE restaurants.deleted_at IS NULL AND restaurant_offers.deleted_at IS NULL AND restaurant_offers.offer_id = #{@offer.id} AND restaurants.chain_id = #{@chain.id}")
    ids = @participants.collect(&:id)
    #@non_participants = Restaurant.where('chain_id = ? AND NOT id in (?) AND status = ?', @chain.id, ids.empty? ? '-1' : ids, true)
    @non_participants = Restaurant.where("restaurants.chain_id =? and restaurants.id NOT in (?) and restaurants.status = ? and restaurant_details.status in(?)", @chain.id, (ids.empty? ? '-1' : ids), true,[0,2,4]).joins("INNER JOIN restaurant_details on restaurant_details.restaurant_id = restaurants.id")
  end

  # GET /offers/new
  def new
    rescue_from_cancan(:new, Offer)
    @offer = Offer.new
  end

  # GET /offer/1/edit
  def edit
    rescue_from_cancan(:edit, Offer)
    @offer = Offer.active_chain.find(params[:id])
  end

  # POST /offers
  def create
    rescue_from_cancan(:create, Offer)
    @offer = Offer.new(params[:offer])
    params[:offer][:effectiveDate] = format_date(params[:offer][:effectiveDate]) if !params[:offer][:effectiveDate].blank?
    params[:offer][:expiryDate] = format_date(params[:offer][:expiryDate]) if !params[:offer][:expiryDate].blank?

    respond_to do |format|
      if @offer.save
        #@offer.update_participation(params[:restaurants], params[:ids])
        if @offer.is_online_order == false
          format.html { redirect_to admin_offer_path(@offer), notice: 'Offer was successfully created.' }
        else
          format.html { redirect_to admin_online_offer_path(@offer), notice: 'Online Offer was successfully created.' }
        end
      else
        format.html { render action: "new" }
      end
    end
  end


  # PUT /offers/1
  def update
    rescue_from_cancan(:update, Offer)
    if params[:offer][:is_online_order].to_s != "true"
      @offer = Offer.active_chain.find(params[:id])
    else
      @offer = Offer.unscoped.online_offers.active_chain.find(params[:id])
    end
    params[:offer][:effectiveDate] = format_date(params[:offer][:effectiveDate]) if !params[:offer][:effectiveDate].blank?
    params[:offer][:expiryDate] = format_date(params[:offer][:expiryDate]) if !params[:offer][:expiryDate].blank?

    respond_to do |format|
      if @offer.update_attributes(params[:offer])
        if @offer.is_online_order == false
        format.html { redirect_to admin_offer_path(@offer), notice: 'Offer was successfully updated.' }
        else
          format.html { redirect_to admin_online_offer_path(@offer), notice: 'Offer was successfully updated.' }
          end
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /offers/1
  def destroy
    rescue_from_cancan(:destroy, Offer)
    @offer= Offer.active_chain.find(params[:id])

    if @offer.restaurant_offers.count == 0
      @offer.destroy
    end

    redirect_to admin_offers_url
  end

  def update_participation
    rescue_from_cancan(:update_participation, Offer)
    @offer= Offer.active_chain.find(params[:offer_id])
    submit= params[:commit]

    case submit
    when "Pull Out"
      @offer.pullout(params[:pullout])
    when "Participate"
      @offer.participate(params[:participate])
    else
    end

    redirect_to admin_offer_path(@offer)
  end

  def chain_offers
    #    list_offer = [Offer.new(:name => "Please select")]
    list_offer = [:id => '', :name => "Please select"]
    chain = Chain.find params[:id]
    @offers = chain.offers.order("name asc") rescue []
    list_offer = list_offer  + @offers
    render :json => list_offer
  end

  def group_chain_list
    sth = params[:search_text_hidden]
    ar_field = [
      "offers.id",
      "chains.name",
      "offers.name",
      '"effectiveDate"',
      '"expiryDate"',
      '"timeStart"',
      "offers.multiplier",
      "offers.bonus_points",
      "offers.bonus_points_ftu",
      "surveys.title"
    ]

    order = params[:qry_order]

    if params[:chn_id] != ""
      qry_condition = "chains.status = 'active' AND chains.id = '#{params[:chn_id]}' AND lower(offers.name) like '%#{sth.downcase}%'"
    else
      qry_condition = "chains.status = 'active' AND lower(offers.name) like '%#{sth.downcase}%'"
    end

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @offers = Offer.includes(:chain, :survey).joins(:chain).where(qry_condition).order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @offers = Offer.includes(:chain, :survey).joins(:chain).where(qry_condition).order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    else
      @offers = Offer.includes(:chain, :survey).joins(:chain).where(qry_condition).paginate(page: params[:page], per_page: 10)
    end
  end

  def getOfferRestaurants
    @id = params[:id]
    if @id != "null"
      @opt = RestaurantOffer.select('restaurants.id, restaurants.name').joins("inner join restaurants On restaurant_offers.restaurant_id=restaurants.id").where("restaurant_offers.offer_id = ?", @id) #find_all_by_chain_id(@id)
      render :json => @opt
    else
      render :nothing => true
    end
  end
end
