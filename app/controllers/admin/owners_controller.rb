class Admin::OwnersController < Admin::BaseController
  # GET /owners
  def index
    rescue_from_cancan(:index, Owner)
    if params[:chn_id].present?
      @chains = Chain.where(id: params[:chn_id]).paginate(page: params[:page], per_page: 10) #Owner.all
    else
      @chains = Chain.paginate(page: params[:page], per_page: 10) #Owner.all
    end
    respond_to do |format|
      format.js
      format.html
    end
  end

  def show
    rescue_from_cancan(:show, Owner)
    @chain = Chain.find(params[:id])
    chain_owners = @chain.owners.where("owners.is_active = ? and role_id != 3",true)
    restaurants_owners = []
    @chain.restaurants.each do |restaurant|
      restaurants_owners << restaurant.owners.where("owners.is_active = ? and owners.role_id = 2",true)
    end

    @owners = chain_owners + restaurants_owners
    @owners = @owners.compact.flatten.uniq
  end


  ## dashboard 2.3 ######################################
  def set_user_owner_status
    ow = Owner.find params[:user_id]
    @owner = ow
    @chain = Chain.find params[:id]
    ow.set_role(params[:mode], params[:val])
  end

  def edit_permission
    @owner = Owner.find params[:id]
    @chain = @owner.chain.id
  end

  def set_take_action_setting
    REDIS.set "db_take_action_setting_#{params['chain_id']}" , nil if params['val'] == "ON"
    REDIS.set "db_take_action_setting_#{params['chain_id']}" , "true" if params['val'] == "OFF"
    @chain = Chain.find params['chain_id']
  end

  def set_take_action_setting_new
    @chain = Chain.find params['chain_id']
    @owner = Owner.find(params['owner_id'])

    if @owner.is_chain_owner?
      REDIS.set "db_take_action_setting_#{params['chain_id']}_#{params['owner_id']}" , nil if params['val'] == "ON"
      REDIS.set "db_take_action_setting_#{params['chain_id']}_#{params['owner_id']}" , "true" if params['val'] == "OFF"
    else
      REDIS.set "db_take_action_setting_#{params['chain_id']}_#{params['owner_id']}" , "true" if params['val'] == "ON"
      REDIS.set "db_take_action_setting_#{params['chain_id']}_#{params['owner_id']}" , nil if params['val'] == "OFF"
    end

  end


  ################## end ##############################

  def dashboard_setting
    @chain = Chain.find params[:id]
    @filter_location = REDIS.get "owner_dashboard_setting_#{@chain.id}_location"
    @filter_average = REDIS.get "owner_dashboard_setting_#{@chain.id}_average"
    @filter_month = REDIS.get "owner_dashboard_setting_#{@chain.id}_month"
    @filter = REDIS.get "owner_dashboard_setting_#{@chain.id}_filter"
  end

  def post_setting
   # val = {:all_location => , :average => params["average"], :month => params["1month"]}
   REDIS.set "owner_dashboard_setting_#{params[:chain_id]}_location", params["all_location"]
   REDIS.set "owner_dashboard_setting_#{params[:chain_id]}_average", params["average"]
   REDIS.set "owner_dashboard_setting_#{params[:chain_id]}_month", params["1month"]
   REDIS.set "owner_dashboard_setting_#{params[:chain_id]}_filter", params["filter"]
   redirect_to dashboard_setting_admin_owner_path(params[:chain_id]), :notice => "Dashboard setting is saved.."
 end

 def restaurant_selection
  if params[:chain_id] && params[:res_id].blank?
    @chain = Chain.find params[:chain_id]
    @restaurants = Restaurant.where(:chain_id => @chain.id)
  elsif !params[:res_id].blank?
    @restaurant = Restaurant.find(params[:res_id])
    @owners = Owner.where("restaurant_id = ?", params[:res_id]).joins(:restaurants_owners)
    @res = true
  else
    render :nothing => true
  end
end

def question_to
  @val = REDIS.get "owner_question_email"
    #REDIS.hset "chain_#{chain.id}", "#{locale.key}_#{code}", content
  end

  def select_chain
    @chain_id = params[:chain_id]
    @val = REDIS.get "owner_question_email_chain_#{@chain_id}"
  end

  def set_email_to
    #@keys = REDIS.hkeys "owner_question_email"
    REDIS.set "owner_question_email", params["email_to"]
    redirect_to question_to_admin_owners_path, :notice => "Email Owner has been set.."
  end

  def set_email_confirmation
    REDIS.set "owner_confirmation_email_subject", params["email_subject"]
    REDIS.set "owner_confirmation_email_content", params["email_content"]
    redirect_to owner_confirmation_email_admin_owners_path, :notice => "Email Setting Updated.."
  end

  def owner_confirmation_email
    @email_subject = REDIS.get "owner_confirmation_email_subject"
    @email_content = REDIS.get "owner_confirmation_email_content"
  end

  def detail
    @owner = Owner.find params[:id]
  end

  def new
    rescue_from_cancan(:new, Owner)
    @owner = Owner.new(:role_id => 1)
  end

  def create
    rescue_from_cancan(:create, Owner)
    @owner = Owner.new(params[:owner])
    #@owner.set_restaurant_owner
    @owner.generate_password
    respond_to do |format|
      if @owner.save
        @owner.set_chain(params[:owner][:chain_id])
        chain = Chain.find params[:owner][:chain_id]
        Delayed::Job.enqueue(OwnerJob::CreateOwnerJob.new(chain,@owner, @owner.password_generated))
        format.html { redirect_to detail_admin_owner_url(@owner), notice: 'Owner was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def edit
    rescue_from_cancan(:edit, Owner)
    @owner = Owner.find(params[:id])
  end

  def update
    rescue_from_cancan(:update, Owner)
    @owner = Owner.find(params[:id])
    respond_to do |format|
      if params[:owner][:password].blank?
        params[:owner].delete(:password)
        params[:owner].delete(:password_confirmation)
      end
      if @owner.update_attributes(params[:owner])
        ldap = Ldap.new
        ldap.create_or_update_owner(@owner, params[:owner][:password])
        format.html { redirect_to detail_admin_owner_url(@owner), notice: 'Owner was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    rescue_from_cancan(:destroy, Owner)
    @owner = Owner.find(params[:id])

    if(@owner)
      #      @owner.chains_owners.destroy_all
      #      @owner.destroy
      @owner.update_attribute(:is_active, !@owner.is_active)
      ldap = Ldap.new
      ldap.add_or_remove_chain_owner(@owner)
    end
    redirect_url = @owner.role_id.eql?(1) ? admin_owner_url(@owner.chains.first) : admin_owner_url(@owner.restaurants.first.chain)
    redirect_url = list_manager_admin_manager_path(@owner.chains.first) if @owner.role_id.eql?(3)
    redirect_to redirect_url, :notice => "Owner has successfully #{@owner.is_active ? "activate" : "deactivate"}"

  end

  def unknown_owner
    @owners = Owner.where("(restaurants_owners.owner_id is null) or is_active = ?", false).joins(:chains_owners, :restaurants_owners)
    @owners = @owners.uniq
  end

  ##### Managers

  def managers
    @chains = Chain.paginate(page: params[:page], per_page: 10) #Owner.all
  end

  def show_managers
    @chain = Chain.find(params[:id])
    @managers = @chain.owners.where(:role_id => 3)
  end

  def new_manager
    @owner = Owner.new
  end

  def restaurant_selection_manager
   @chain = Chain.find(params[:chain_id])
   @restaurants = @chain.restaurants
 end

 def create_manager
  rescue_from_cancan(:create, Owner)
  @owner = Owner.new(params[:owner])
    @owner.role_id = 3 ## manager
    #@owner.set_restaurant_owner
    @owner.restaurant_ids = params[:restaurant_select_id]
    @owner.generate_password

    respond_to do |format|
      if @owner.save
        @owner.set_chain(params[:owner][:chain_id])
        OwnerMailer.confirmation_email(@owner).deliver
        format.html { redirect_to detail_admin_manager_url(@owner), notice: 'Manager was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def edit_manager
    rescue_from_cancan(:create, Owner)
    @owner = Owner.find(params[:id])
  end

  def update_manager
    rescue_from_cancan(:update, Owner)
    @owner = Owner.find(params[:id])
    respond_to do |format|
      if params[:owner][:password].blank?
        params[:owner].delete(:password)
        params[:owner].delete(:password_confirmation)
      end

      @owner.role_id = 3 ## manager
      #  @owner.restaurant_ids = params[:restaurant_select_id]

      if @owner.update_attributes(params[:owner])

        format.html { redirect_to detail_admin_manager_url(@owner), notice: 'Manager was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def detail_manager
    @owner = Owner.find params[:id]
  end

  private

  def owner_params
    params.require(:owner).permit(:role_id, :first_name, :last_name, :email, :title, :address, :password, :password_confirmation, :city_id, :zipcode,:work_contact_number, :cell_contact_number, :owner_city_text)
  end
end
