class Admin::ReportsController < Admin::BaseController

  def index    
  end

  def search_reports
    @chain = Chain.find(params[:chain_id])
    unless params[:restaurant_id].blank?
      @reports = Report.where(:chain_id => @chain.id, :restaurant_id => params[:restaurant_id]).includes(:chain)
    else
      @reports = Report.where(:chain_id => @chain.id).includes(:chain)
    end
    
    @restaurants = @chain.restaurants unless @chain.blank?
    @restaurant = Restaurant.find(params[:restaurant_id]) unless params[:restaurant_id].blank?
    @reports_json =
      unless @reports.blank?
        @reports.map {|report| {
        :title => "#{report.kind.nil?? "daily" : report.kind} #{report.start_date}",
        :start => report.start_date.to_date,
        :end =>report.end_date.to_date,:url => report.get_report_url } }.to_json
      else
        {:title => "", :start => Date.today, :end => Date.today}.to_json
      end
    #@js = {:title => "test", :start => Date.today, :end => Date.today, :url => "http://www.google.com"}
  end
end