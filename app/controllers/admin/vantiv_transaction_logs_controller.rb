class Admin::VantivTransactionLogsController < Admin::BaseController

  def index

  end

  def search
    @chain = Chain.find(params["chain_id"])
    @vantiv_transaction_logs = Vantiv::TransactionLog.where(:chain_id => @chain.id).order("id DESC").paginate(page: params[:page], per_page: 10)
  end

  def search_query
   @chain = Chain.find(params["chain_id"])
   @search_query = params["search"] rescue nil
   @effective_date = params["search_date"]["from"] rescue nil
   @expiry_date = params["search_date"]["to"] rescue nil

   query = {chain_id: @chain.id}

   if params["search"].present?
     query[:card_number] = params["search"]["card_number"] if !params["search"]["card_number"].blank?
     query[:transaction_id] = params["search"]["transaction_id"] if !params["search"]["transaction_id"].blank?
     query[:ref_num] = params["search"]["ref_num"] if !params["search"]["ref_num"].blank?
     unless params["search"]["email"].blank?
      users = User.where( "email LIKE :q AND chain_id = :chain " ,
        q: "#{params["search"]["email"]}%", chain: @chain.id)
      query[:user_id] = users.map(&:id)
    end
  end

  if !@effective_date.blank? && !@expiry_date.blank?
    query[:created_at] = @effective_date.to_date.beginning_of_day .. @expiry_date.to_date.end_of_day
  end

  @vantiv_transaction_logs = Vantiv::TransactionLog.where(query).order("id DESC").paginate(page: params[:page], per_page: 10)


  end

end
