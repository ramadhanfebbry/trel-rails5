class Admin::OlorewardsSettingsController <  Admin::BaseController

  def index
    @admin_olorewards_settings = OlorewardsSetting.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_olorewards_settings }
    end
  end

  def search
    @chain = Chain.find(params["chain_id"])
    @admin_olorewards_setting = @chain.olorewards_setting
  end

  def show
    @admin_olorewards_setting = OlorewardsSetting.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @admin_olorewards_setting }
    end
  end

  def new
    @chain = Chain.find params['chain_id']
    @admin_olorewards_setting = OlorewardsSetting.new
    render :layout => false
  end

  def edit
    @admin_olorewards_setting = OlorewardsSetting.find(params[:id])
    @chain = @admin_olorewards_setting.chain
    render :layout => false
  end

  def create
    @chain = Chain.find params[:olorewards_setting][:chain_id]
    p params[:admin_olorewards_setting]
    @admin_olorewards_setting = OlorewardsSetting.new(params[:olorewards_setting])

    respond_to do |format|
      if @admin_olorewards_setting.save
        @save = true
        format.html { redirect_to admin_olorewards_settings_path, notice: 'Olorewards setting was successfully created.' }
        format.js {  }
      else
        @save = false
        format.html { render action: "new" }
        format.js {  }
      end
    end
  end

  def update
    @admin_olorewards_setting = OlorewardsSetting.where(:chain_id => params[:olorewards_setting][:chain_id], :id => params[:id]).first rescue nil 

    respond_to do |format|
      if @admin_olorewards_setting.present? && @admin_olorewards_setting.update_attributes(params[:olorewards_setting])
        format.html { redirect_to admin_olorewards_settings_path, notice: 'Olorewards setting was successfully updated.' }
        format.js { }
      else
        format.html { render action: "edit" }
        format.js { }
      end
    end
  end

  def destroy
    @admin_olorewards_setting = OlorewardsSetting.find(params[:id])
    @admin_olorewards_setting.destroy

    respond_to do |format|
      format.html { redirect_to admin_olorewards_settings_url }
      format.json { head :no_content }
    end
  end
end