class Admin::ExternalSettingsController <  Admin::BaseController

  def index
    @external_settings = ExternalSetting.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @external_settings }
    end
  end

  def search
    @chain = Chain.find(params["chain_id"])
    @external_setting = @chain.external_setting
  end

  def show
    @external_setting = ExternalSetting.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @external_setting }
    end
  end

  def new
    @chain = Chain.find params['chain_id']
    @external_setting = ExternalSetting.new
    render :layout => false
  end

  def edit
    @external_setting = ExternalSetting.find(params[:id])
    @chain = @external_setting.chain
    render :layout => false
  end

  def create
    @chain = Chain.find params[:external_setting][:chain_id]
    p params[:external_setting]
    p @external_setting = ExternalSetting.new(params[:external_setting])

    respond_to do |format|
      if @external_setting.save
        p @external_setting.errors
        @save = true
        format.html { redirect_to external_settings_path, notice: 'Olorewards setting was successfully created.' }
        format.js {  }
      else
        @save = false
        format.html { render action: "new" }
        format.js {  }
      end
    end
  end

  def update
    @external_setting = ExternalSetting.where(:chain_id => params[:external_setting][:chain_id], :id => params[:id]).first rescue nil

    respond_to do |format|
      if @external_setting.present? && @external_setting.update_attributes(params[:external_setting])
        format.html { redirect_to external_settings_path, notice: 'Olorewards setting was successfully updated.' }
        format.js { }
      else
        format.html { render action: "edit" }
        format.js { }
      end
    end
  end

  def destroy
    @external_setting = ExternalSetting.find(params[:id])
    @external_setting.destroy

    respond_to do |format|
      format.html { redirect_to admin_external_settings_url }
      format.json { head :no_content }
    end
  end
end
