class Admin::PromotionsController < Admin::BaseController
  
  before_action :find_promotion,:only => [:show, :edit,:update, :show_promo_codes, :destroy]
  
  def index
    @promotions = Promotion.active_chain.order("promotions.updated_at DESC").paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  def promo_offer
    @promo_offers = PromotionOffer.paginate(page: params[:page], per_page: Setting.pagination.per_page)
    render :layout => false
  end

  def new_offer
    @chain = Chain.find params[:chain_id]
    @offer = PromotionOffer.new
    render :layout => false
  end

  def create_offer
    @offer = PromotionOffer.new(params[:promotion_offer])

    if @offer.save
      @save = true
      @promo_offers = PromotionOffer.where(:chain_id => @offer.chain_id).
          paginate(page: params[:page], per_page: Setting.pagination.per_page)
      @offer = PromotionOffer.new
      @chain = Chain.find params[:promotion_offer][:chain_id]
    else
      @save = false
     # @offer = PromotionOffer.new
      @chain = Chain.find params[:promotion_offer][:chain_id]
    end
  end

  def search
    q = params[:search][:q]
    q = q.gsub("'","''")
    params[:search][:q] = q

    ar_field = [
      "promotions.updated_at",
      "promotions.id",
      "promotions.name",
      "promotions.promotable_type",
      "promotions.effective_date",
      "promotions.expiry_date",
      "promotions.limit"
    ]

    order = params[:qry_order]

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @promotions = Promotion.active_chain.includes(:chain).search(params[:search]).order(qry_order).reverse_order
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @promotions = Promotion.active_chain.includes(:chain).search(params[:search]).order(qry_order)
    end

    @promotions = @promotions.paginate(page: params[:page], per_page: Setting.pagination.per_page)


#    @promotions = Promotion.active_chain.order("promotions.updated_at DESC").includes(:chain).search(params[:search]).paginate(page: params[:page], per_page: Setting.pagination.per_page)
    respond_to do |format|
      format.js { render :index }      
    end
  end

  def new
    @promotion = Promotion.new
    @type = params[:type]
    if @type.eql?("repeated")
      @promotion.is_repeated = true
    end
    # @promotion_notification = @promotion.build_promotion_notification
  end

  def reward_list
    @type = params[:type]
    render :layout => false
  end

  def generate_promo_code
    @promo_codes =  PromoCode.generate_sample_codes(10)
    render :layout => false
  end

  def create    
    rescue_from_cancan(:new, Promotion)
    @promotion = Promotion.new(params[:promotion])
    @type = params[:type]

    unless params["selected_type"].blank?
      if params["selected_type"] == "Perk"
        @data = Perk.find params["selected_id"]
      elsif params["selected_type"] == "Reward"
        @data = Reward.find params["selected_id"]
        @data.executed_at = nil # skip validation for delayed jobs execution
      else
        @data = PromotionOffer.find params["selected_id"]
      end
      @promotion.chain_id = @data.chain_id
      puts @promotion.chain_id
      puts "--------------------------------------------------------------------"
      @data.promotions << @promotion
    else
      @data = @promotion
    end

    if @data.save && @promotion.set_promo_code(current_admin)
      if params[:promotion_notification].present?
        if push_notification(params, @promotion)
          p "Notification has been set"
          @notice = "and notification has been set. "
        else
          flash[:error] = "Promotion could not create"
          render :action => "new"
        end
      end

      flash[:notice] = "Promotion has created #{@notice}"
      redirect_to admin_promotions_path
    else
      flash[:error] = "Promotion could not create"
      render :action => "new"
    end
  end

  def update
    rescue_from_cancan(:new, Promotion)
    @type = params[:type]
    if @promotion.update_attributes(params[:promotion]) == false
      return render :action => "edit"
    end
    
    if params["selected_type"] == "Perk"
      @data = Perk.find params["selected_id"]
    elsif params["selected_type"] == "Reward"
      @data = Reward.find params["selected_id"]
    else
      @data = PromotionOffer.find params["selected_id"]
    end
    @promotion.chain_id = @data.chain_id
    #@promotion.chain_id = params["promotion"]["chain_id"]
    @data.promotions << @promotion
    if @data.save! && @promotion.set_promo_code(current_admin)
      if params[:promotion_notification].present?
        if push_notification(params, @promotion)
          flash[:notice] = "Promotions updated and notification has been set"
          redirect_to admin_promotions_path
        else
          flash[:notice] = "Promotions error"
          redirect_to :action => "edit"
        end
      else
        flash[:notice] = "Promotions updated"
        redirect_to admin_promotions_path
      end
    else
      flash[:notice] = "Promotions error"
      redirect_to :action => "edit"
    end
  end

  def search_rewards
    unless params["chain_id"].blank?
      @chain = Chain.find(params["chain_id"])
      condition = "reward_type = #{Reward::TYPES["PROMOTION"]}"
      condition = "reward_type = #{Reward::TYPES["GIFTABLE"]}" if params[:type].eql?('gift')
      @rewards = @chain.rewards.where(condition).includes(:chain).order("rewards.updated_at desc").paginate(page: params[:page], per_page: Setting.pagination.per_page)
      @reward_type = 1
      @reward = Reward.new
    else
      @rewards = []
      @reward = Reward.new      
    end
    render :layout => false
  end

  def search_perks
     unless params["chain_id"].blank?
      @chain = Chain.find(params["chain_id"])
      @perks = @chain.perks.where("chain_id = ?", @chain.id).includes(:chain).paginate(page: params[:page], per_page: Setting.pagination.per_page)
    else
      @rewards = []      
    end
    render :layout => false
  end

  def search_offers
    unless params["chain_id"].blank?
      @chain = Chain.find(params["chain_id"])
      @promo_offers = @chain.promotion_offers.where("chain_id = ?", @chain.id).includes(:chain).paginate(page: params[:page], per_page: Setting.pagination.per_page)
    else
      @rewards = []
    end
    @offer = PromotionOffer.new
    render :layout => false
  end

  def perk_list
    render :layout => false
  end

  def edit
    @notification = @promotion.promotion_notification
    @type = params[:type]
  end

  def show
    
  end

  def show_promo_codes
    params[:search] ||= ""
    @promotion = Promotion.find(params[:id])
    @codes= @promotion.promo_codes.paginate(page: params[:page], per_page: 10)#.joins(:owner).where("LOWER(users.email) like '%#{params[:search].downcase}%' or LOWER(code) like '%#{params[:search].downcase}%'").paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def search_code_by_email_and_code
    params[:search] ||= ""
    @promotion = Promotion.find(params[:id])

    @codes= @promotion.promo_codes.includes(:users_promo_codes => :user).
        where("lower(code) like '%#{params[:search].downcase}%' or users.email like '%#{params[:search].downcase}%'").
        paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def destroy
    if @promotion.delete
      flash[:notice] = "Promotions deleted"
      redirect_to admin_promotions_path
    end
  end

  def code_histories
    @promo_code = PromoCode.find params[:id]
    @histories = @promo_code.users_promo_codes
  end

  def promotion_notification
    @promotion = Promotion.find params[:id].to_i
    @promotion_notification = @promotion.promotion_notification
  end

  def preview_xls_data
    @plain_push_notification = PromotionNotification.new
    test_file = params[:xls]
    notice = "Preview"
    unless test_file.blank?
      book = Spreadsheet.open "#{test_file.path}"
      sheet1 = book.worksheet 0
      debugger
      @email_from_xls = []
      sheet1.column(0).each { |row| @email_from_xls << row }
      begin
        @email_from_xls = @emails_from_xls.compact
      rescue
      end
    else
      notice = "Please upload the xls file"
    end
    render :layout => false
  end

  private

  def find_promotion
    @promotion = Promotion.find params[:id]
  end

  def push_notification(params, promotion)
    ## emails is change with user ids
    chain_id = params["promotion"]["chain_id"]

    content = params["notification"]
    if content.class != ActiveSupport::HashWithIndifferentAccess
      content = {"en" => content}
    end

    date_text = params[:date_execute] rescue ""
    time_text = "#{params[:date][:hour]}:#{params[:date][:minute]}" rescue ""
    p "#{date_text} #{time_text} EDT"
    run_at = Time.zone.parse("#{date_text} #{time_text} EDT") rescue nil
    # @promotion_notification = PromotionNotification.new(params[:promotion_notification])
    @promotion_notification = promotion.build_promotion_notification(params[:promotion_notification])
    p "------- executed at ---------"
    p run_at
    if run_at && run_at >= Time.zone.now
      @promotion_notification.executed_at = run_at.utc
    end
    @promotion_notification.admin_id = current_admin.id
    @promotion_notification.content = {:content => content}
    puts "chain_id ================= #{chain_id}"
    @promotion_notification.chain_id = chain_id
    @promotion_notification.is_push_email = false if params["promotion_notification"]["is_push_email"].blank?
    @promotion_notification.is_push_phone = false if params["promotion_notification"]["is_push_phone"].blank?

    p "@promotion_notification"
    p @promotion_notification
    p "@promotion_notification"

    #if @promotion_notification.save && @promotion_notification.delay.send_plain_email
    if @promotion_notification.save
      return true
    else
      p "promotion_notification_error : #{@promotion_notification.errors.full_messages}"
      return false
    end
  end
end
