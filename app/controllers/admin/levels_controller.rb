class Admin::LevelsController < Admin::BaseController

  before_action :get_game

  # GET /admin/levels
  # GET /admin/levels.json
  def index
    @levels = @game.levels

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_levels }
    end
  end

  # GET /admin/levels/1
  # GET /admin/levels/1.json
  def show
    @level = @game.levels.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @level }
    end
  end

  # GET /admin/levels/new
  # GET /admin/levels/new.json
  def new
    @level = @game.levels.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @level }
    end
  end

  # GET /admin/levels/1/edit
  def edit
    @level = @game.levels.find(params[:id])
  end

  # POST /admin/levels
  # POST /admin/levels.json
  def create
    @level = @game.levels.new(params[:level])

    respond_to do |format|
      if @level.save
        format.html { redirect_to admin_game_levels_path(@game), notice: 'Level was successfully created.' }
        format.json { render json: @level, status: :created, location: @admin_level }
      else
        format.html { render action: "new" }
        format.json { render json: @level.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/levels/1
  # PUT /admin/levels/1.json
  def update
    @level = @game.levels.find(params[:id])

    respond_to do |format|
      if @level.update_attributes(params[:level])
        format.html { redirect_to admin_game_levels_path(@game), notice: 'Level was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @level.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/levels/1
  # DELETE /admin/levels/1.json
  def destroy
    @level = @game.levels.find(params[:id])
    @level.destroy

    respond_to do |format|
      format.html { redirect_to admin_game_levels_url(@game) }
      format.json { head :ok }
    end
  end

  protected

  def get_game
    @game = Game.find(params[:game_id])
  end
end