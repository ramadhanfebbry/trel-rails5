class Admin::PartnerCategoriesController < Admin::BaseController

  def index
    if params["search"] && params["search"]["chain_id"].present?
      @chain = Chain.find( params["search"]["chain_id"])
      @partner_categories = @chain.partner_categories.order("chain_id").paginate(:page => params[:page], :per_page => 10)
    else
      @partner_categories = PartnerCategory.order("chain_id").paginate(:page => params[:page], :per_page => 10)
      @chain = nil
    end
  end

  def new
    @partner_category = PartnerCategory.new
  end

  def create
    @partner_category = PartnerCategory.new(params[:partner_category])
    if @partner_category.save
      redirect_to admin_partner_categories_path, :notice => "Partner Category successfully created."
    else
      render :new, :alert => "Partner Category failed."
    end
  end

  def edit
    @partner_category = PartnerCategory.find(params[:id])
    respond_to do |format|
      format.js
      format.html
    end
  end

  def update
    @partner_category = PartnerCategory.find params[:id]
    if @partner_category.update_attributes(params[:partner_category])
      redirect_to admin_partner_categories_path, :notice => "Partner Category successfully updated."
    else
      render :edit, :alert => "Partner Category failed."
    end
  end

  def show
    @partner_category = PartnerCategory.find params[:id]
  end

  def destroy
    @partner_category = PartnerCategory.find params[:id]
    @partner_category.destroy
    redirect_to admin_partner_categories_path, :notice => "Partner Category successfully destroyed."
  end

end