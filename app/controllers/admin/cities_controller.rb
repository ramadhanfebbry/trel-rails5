class Admin::CitiesController < Admin::BaseController
  before_action :find_city, :only => [:update_status, :show, :edit, :update]

  def index
    rescue_from_cancan(:index, City)
    @cities = City.paginate(page: params[:page], per_page: 10).includes(:region => :country)
    @countries = Country.active_countries
    @states = Region.active_regions
    @city = City.new
    
    respond_to do |format|
      format.html { }
      format.json { render json: @cities }
    end
  end

  def load_states
    unless params[:id].blank?
      @states = Country.find(params[:id]).regions
    else
      @states = []
    end
  end

  def update_status
    if @city.update_attribute(:active, params["city_status"])
      flash[:notice] = "City successfully updated"
      redirect_to admin_cities_path
    else
      flash[:notice] = "City failed to update"
      redirect_to admin_cities_path
    end
  end

  def show    
  end

  def edit
    region = Region.find(@city.region_id)
    @region_id = @city.region_id
    @country_id = region.country_id
    @countries = Country.active_countries
    @states = Region.active_regions.find_all_by_country_id(@country_id)
  end

  def update
    respond_to do |format|
      if @city.update_attributes(params[:city])
        flash[:notice] = "city has been updated"
        format.html { redirect_to admin_cities_path }
      else
        flash[:notice] = "city failed to create"
        format.html { render :action => "edit" }
        format.json { render json: "failed" }
      end
    end
  end

  def create
    @city = City.new(params[:city])

    respond_to do |format|
      if @city.save
        flash[:notice] = "City successfully created"
        format.html { redirect_to admin_cities_path }
        format.json { render json: @city }
      else
        flash[:notice] = "City failed to create #{@city.errors.full_messages.each{|msg|}}"
        format.html { redirect_to admin_cities_path }
        format.json { render json: @city.errors }
      end
    end
  end
  # GET /cities/lookup
  def lookup
    @cities = City.by_input(params[:term]).limit(params[:maxRows])

    render json: @cities.as_json(:only => [:id], :methods => [:label, :value])
  end

  # GET /cities/country_regions
  def getCountryRegions
    @regions = Region.active_regions.select('id, name').find_all_by_country_id(params[:id])
    render :json => @regions
  end

  def search_cities
    sth = params[:search_text_hidden]
    ar_field = [
      "cities.id",
      "cities.name",
      "regions.name",
      "countries.name",
      "",
      "cities.active"
    ]

    order = params[:qry_order]
    country_id = params[:country_id]
    region_id = params[:region_id]

    condition = "lower(cities.name) like '%#{sth.downcase}%'"
    if country_id != ""
      condition = condition + " AND countries.id = '#{country_id}'"
      if region_id != ""
        condition = condition + " AND regions.id = '#{region_id}'"
      end
    end

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @cities = City.includes(:region => :country).where(condition).order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @cities = City.includes(:region => :country).where(condition).order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    end

  end

  private

  def find_city
    @city = City.find params[:id]
  end
end