class Admin::StepsController < Admin::BaseController

  before_action :get_chain, :get_steppingstone

  def index
    @steps = @step_parent.steppingstone_steps
    template = "/admin/steps/index"
    template = "/admin/steps/points_earned/index" if @step_parent.class.to_s.eql?("Steppingstone") && @step_parent.step_type == "points_earned"
    render :template =>  template
  end

  def new
    @step = @step_parent.steppingstone_steps.new
  end

  def create
    @step = @step_parent.steppingstone_steps.new(params[:steppingstone_step])
    if @step.save
      redirect_to params[:type].blank? ? admin_chain_steppingstone_steps_path(@chain, @step_parent) : admin_chain_bucket_path_steps_path(@chain,"bucket", @step_parent), :notice => "Step successfully created."
    else
      render "new"
    end
  end

  def edit
    @step = @step_parent.steppingstone_steps.find params[:id]
  end

  def edit_milestone_point
    @step = @step_parent.steppingstone_steps.find params[:id]
  end

  def update
    @step = @step_parent.steppingstone_steps.find params[:id]
    if @step.update_attributes(params[:steppingstone_step])
      redirect_to params[:type].blank? ? admin_chain_steppingstone_steps_path(@chain, @step_parent) : admin_chain_bucket_path_steps_path(@chain, "bucket", @step_parent), :notice => "Step successfully updated."
    else
      render "edit"
    end
  end

  def update_milestone_point
    @step = @step_parent.steppingstone_steps.find params[:id]
    if @step.update_attributes(params[:steppingstone_step])
      redirect_to  admin_chain_steppingstone_steps_path(@chain, @step_parent), :notice => "Step successfully updated."
    else
      render "edit_milestone_point"
    end
  end

  def destroy
    @step = @step_parent.steppingstone_steps.find params[:id]
    @step.destroy
    redirect_to params[:type].blank? ? admin_chain_steppingstone_steps_path(@chain, @step_parent) : admin_chain_bucket_path_steps_path(@chain, "bucket", @step_parent), :notice => "Step successfully destroy."
  end

  def add_reward
    steps = @step_parent.steppingstone_steps
    @step = steps.find params[:id]
    @reward_ids = steps.map(&:reward_id).compact
    @rewards = @chain.rewards.where(:reward_type => Reward::TYPES["MILESTONE"]).paginate(page: params[:page], per_page: 10)
  end

  def add_point_reward
    steps = @step_parent.steppingstone_steps
    @step = steps.find params[:id]
    @reward_ids = steps.map(&:reward_id).compact
    @rewards = @chain.rewards.where(:reward_type => Reward::TYPES["MILESTONE"]).paginate(page: params[:page], per_page: 10)
  end

  def add_one_time_reward
    steps = @step_parent.steppingstone_steps
    @step = steps.find params[:id]
    @reward_ids = steps.map(&:reward_id).compact
    @rewards = @chain.rewards.where(:reward_type => Reward::TYPES["MILESTONE"]).paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.js { render "/admin/steps/points_earned/add_reward"}
    end
  end

  def add_point
    @step = @step_parent.steppingstone_steps.find params[:id]
  end

  def set_point
    @step = @step_parent.steppingstone_steps.find params[:id]
    @step.point_as_incentive = true
    if @step.update_attributes(params[:steppingstone_step])
      @updated = true
    else
      @updated = false
    end  
  end

  def remove_point
    @step = @step_parent.steppingstone_steps.find params[:id]
    @step.update_attribute(:point, nil)
  end

  def remove_reward
    @step = @step_parent.steppingstone_steps.find params[:id]
    @step.update_attribute(:reward_id, nil)
  end

  def add_milestone_reward
    @step = @step_parent.steppingstone_steps.find params[:id]
    reward = Reward.find params[:reward_id]
    @step.update_attribute(:reward_id, reward.id)
  end

  def remove_one_time_reward
    @step = @step_parent.steppingstone_steps.find params[:id]
    @step.update_attribute(:reward_id, nil)
    respond_to do |format|
      format.js { render "admin/steps/points_earned/remove_reward"}
    end
  end

  def add_the_one_reward
    @step = @step_parent.steppingstone_steps.find params[:id]
    reward = Reward.find params[:reward_id]
    @step.update_attribute(:reward_id, reward.id)

    respond_to do |format|
      format.js { render "admin/steps/points_earned/add_the_one_reward"}
    end
  end

  def new_milestone_reward
    @step = @step_parent.steppingstone_steps.find params[:id]
    @reward = Reward.new
  end

  def create_milestone_reward
    @step = @step_parent.steppingstone_steps.find params[:id]
    @reward = Reward.new(params[:reward])
    if @reward.save
      @reward.participate(@chain.restaurants.map(&:id))
      @is_save = true
    else
      @is_save = false
    end
  end

  def create_one_time_reward
    @step = @step_parent.steppingstone_steps.find params[:id]
    @reward = Reward.new(params[:reward])
    if @reward.save
      @reward.participate(@chain.restaurants.map(&:id))
      @is_save = true
    else
      @is_save = false
    end
  end

  ## points earned stepping
  def new_points_earned_step
    @step = @step_parent.steppingstone_steps.new
    render :template =>   "/admin/steps/points_earned/new"
  end

  def new_one_time_reward
    @step = @step_parent.steppingstone_steps.find params[:id]
    @reward = Reward.new
  end

  def create_step_points_earned
    @step = @step_parent.steppingstone_steps.new(params[:steppingstone_step])
    if @step.save
      redirect_to  admin_chain_steppingstone_steps_path(@chain, @step_parent)
    else
      render :template =>   "/admin/steps/points_earned/new", :action => "new_points_earned_step"
    end
  end

  protected

  def get_chain
    @chain = Chain.find params[:chain_id]
  end

  def get_steppingstone
    if params[:type].blank?
      @step_parent = Steppingstone.find params[:steppingstone_id]
    else
      @step_parent = BucketPath.find params[:bucket_path_id]
    end

  end

end