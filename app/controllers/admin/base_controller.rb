class Admin::BaseController < ApplicationController
  layout 'admin'

  before_action :authenticate_admin!
  before_action :load_ability
  before_action :set_locale
 
  def set_locale
    I18n.locale = cookies[:locale] || I18n.default_locale
  end

  # def default_url_options(options={})
  # logger.debug "default_url_options is passed options: #{options.inspect}\n"
  # { :locale => I18n.locale }
  # end

  def image_processing image_url, image_dimention, process_flag, user, offer, key, restaurant
    require 'RMagick'
    cat = Magick::ImageList.new(image_url)
    new_image = cat.resize_to_fit(image_dimention[0])
    new_file_name = user+offer+key+restaurant+Time.now.to_i.to_s+".jpg"
    # UserID_OfferId_Appkey_restaurantID_
    new_image_uploaded_path = "#{Rails.root}/tmp/#{new_file_name}"
    new_image.write(new_image_uploaded_path)
    returning_date = nil
    bucket = Setting.storage.s3_bucket
    begin
      con = AWS::S3::Base.establish_connection!(:access_key_id => Setting.storage.s3_access_key_id, :secret_access_key => Setting.storage.s3_secret_access_key)
      snd = AWS::S3::S3Object.store(new_file_name,open(new_image_uploaded_path),bucket)
      returning_date = "https://s3.amazonaws.com/#{bucket}/#{new_file_name}"
    rescue => e
      returning_date = e.message
    end
    
    return returning_date
  end


  # This is getting called, when you visit.. Rewards#index. Use is as you wish. 
  def send_message_on_sqs   
 
  end

  def load_ability
    @ability = Ability.new(current_admin)
  end

  #  rescue_from CanCan::AccessDenied do |exception|
  #    redirect_to "/", :alert => exception.message
  #  end

  def rescue_from_cancan(action, model_name)
    unless @ability.can? action, model_name
      raise CanCan::AccessDenied.new("Not authorized!", action, model_name)
    end
  end

end
