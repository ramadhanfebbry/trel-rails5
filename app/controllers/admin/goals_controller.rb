class Admin::GoalsController < Admin::BaseController

  def index

  end

  def goals_by_chain
    @chain = Chain.find(params[:id]) rescue nil
    @goals = @chain.goals.order("updated_at DESC") rescue []
    @goals = @goals.group_by {|g| g.category_id}
    respond_to do |format|
      format.html {render "goals_by_chain.html"}
      format.js {render "goals_by_chain.js"}
    end
  end

  def new
    @chain = Chain.find(params[:chain_id])
    @goal = @chain.goals.new
    @chain.locales.each do |locale|
      @goal.goal_locales.build(:locale_id => locale.id)
    end
  end

  def create
    @goal = Goal.new(params[:goal])
    @chain = @goal.chain
    if @goal.save
      redirect_to goals_by_chain_admin_goal_path(@chain), :notice => "Goal Successfully created"
    else
      render :new
    end
  end

  def edit
    @goal = Goal.find(params[:id])
    @chain = @goal.chain
    (@chain.locales - @goal.goal_locales.map{|gl| gl.locale}).each do |locale|
      @goal.goal_locales.build(:locale_id => locale.id)
    end
  end

  def update
    @goal = Goal.find(params[:id])
    @chain = @goal.chain
    if @goal.update_attributes(params[:goal])
      redirect_to goals_by_chain_admin_goal_path(@chain), :notice => "Goal Successfully updated"
    else
      render :edit
    end
  end

  def pick_incentive
    @goal = Goal.find params[:id]
    @deal_offers = DealOffer.all
    @rewards = Reward.where(:reward_type => 6)
  end

  def load_ontime_reward
    @goal = Goal.find params[:id]
    @rewards = @goal.chain.rewards.where(:reward_type => Reward::TYPES["INCENTIVE"]).paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  def load_points
    @points = DealOffer.where(:kind => 1).paginate(page: params[:page], per_page: Setting.pagination.per_page)
    @goal = Goal.find params[:id]
  end

  def assign_with_existing_reward
    @reward = Reward.find(params[:reward_id])
    @goal = Goal.find(params[:id])
    @goal.update_attributes(:incentive_type => "Reward", :incentive_id => @reward.id)
  end

  def assign_with_existing_point
    @deal_offer = DealOffer.find(params[:point_id])
    @goal = Goal.find(params[:id])
    @goal.update_attributes(:incentive_type => "DealOffer", :incentive_id => @deal_offer.id)
  end

  def assign_incentive
    @goal = Goal.find params[:id]
    @type = params[:type]
    case @type
    when "reward"
      @reward = Reward.new
    when "offer"
      @offer = DealOffer.new
    end
  end

  def create_reward_incentive
    @goal = Goal.find params[:id]
    @is_save = false
    @reward = Reward.new(params[:reward])
    @reward.chain_id = @goal.chain.id
    reward_before = @goal.incentive
    @type = "reward"

    respond_to do |format|
      if @reward.save and @goal.update_attributes(:incentive_type => "Reward", :incentive_id => @reward.id)
        @is_save = true
        reward_before.destroy if reward_before
        format.js { render :create_incentive }
      else
        format.js { render :create_incentive }
      end
    end
  end

  def create_offer_incentive
    @goal = Goal.find params[:id]
    @is_save = false
    @type = "offer"
    @offer = DealOffer.new(params[:deal_offer])
    @offer.kind = DealOffer::TYPES["POINTS"]
    reward_before = @goal.incentive

    respond_to do |format|
      if @offer.save and @goal.update_attributes(:incentive_type => "DealOffer", :incentive_id => @offer.id)
        @is_save = true
        reward_before.destroy if reward_before
        format.js { render :create_incentive }
      else
        format.js { render :create_incentive }
      end
    end
  end

  def edit_incentive
    @offer = DealOffer.find(params[:id])
    @goal = @offer.deal
  end

  def update_incentive
    @offer = DealOffer.find(params[:id])
    @goal = Goal.where(:incentive_type => @offer.class.to_s, :incentive_id => @offer.id).first
    @is_save = false

    respond_to do |format|
      if @offer.update_attributes(params[:deal_offer])
        @is_save = true
        format.js { render :update_incentive }
      else
        format.js { render :update_incentive }
      end
    end
  end

  def deactivate
    @goal = Goal.find(params[:id])
    @chain = @goal.chain
    @goal.update_attribute(:status, Goal::STATUS[:INACTIVE])
    redirect_to goals_by_chain_admin_goal_path(@chain), :notice => "Goal Successfully deactived"
  end

  def activate
    @goal = Goal.find(params[:id])
    @chain = @goal.chain
    @goal.update_attribute(:status, Goal::STATUS[:ACTIVE])
    redirect_to goals_by_chain_admin_goal_path(@chain), :notice => "Goal Successfully actived"
  end

  def remove_incentive
    @goal = Goal.find(params[:id])
    @goal.update_attribute(:incentive, nil)
    @goal.message_incentive_locales.destroy_all
  end

  def add_message_incentive
    @goal = Goal.find(params[:id])
    (@goal.chain.locales - @goal.message_incentive_locales.map{|gl| gl.locale}).each do |locale|
      @goal.message_incentive_locales.build(:locale_id => locale.id)
    end
  end

  def create_message_incentive
    @goal = Goal.find(params[:id])
    @is_save = @goal.update_attributes(params[:goal]) ? true : false
  end

  
end
