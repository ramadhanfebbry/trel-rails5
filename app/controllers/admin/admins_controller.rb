class Admin::AdminsController < Admin::BaseController
#  load_and_authorize_resource

  # GET /admin/admins
  # GET /admin/admins.json
  def index
    rescue_from_cancan(:index, Admin)
    @admins = Admin.all

    unless current_admin.has_role?("admin")
      redirect_to admin_admin_path(current_admin)
    end
  end

  # GET /admin/admins/1
  # GET /admin/admins/1.json
  def show
    @admin = Admin.find(params[:id])
    rescue_from_cancan(:show, @admin)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @admin }
    end
  end

  # GET /admin/admins/new
  # GET /admin/admins/new.json
  def new
    rescue_from_cancan(:new, Admin)
    @admin = Admin.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @admin }
    end
  end

  # GET /admin/admins/1/edit
  def edit
    @admin = Admin.find(params[:id])
    rescue_from_cancan(:edit, @admin)
  end

  # POST /admin/admins
  # POST /admin/admins.json
  def create
    rescue_from_cancan(:create, Admin)
    @admin = Admin.new(params[:admin])

    respond_to do |format|
      if @admin.save
        format.html { redirect_to admin_admins_path, notice: 'Admin was successfully created.' }
        format.json { render json: @admin, status: :created, location: @admin }
      else
        format.html { render action: "new" }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/admins/1
  # PUT /admin/admins/1.json
  def update
    @admin = Admin.find(params[:id])
    rescue_from_cancan(:update, @admin)

    respond_to do |format|
      if @admin.update_attributes(params[:admin])
        if current_admin.has_role?("admin")
          format.html { redirect_to admin_admins_path, notice: 'Admin was successfully updated.' }
        else
          format.html { redirect_to admin_admin_path(@admin), notice: 'Profile was successfully updated'}
        end
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/admins/1
  # DELETE /admin/admins/1.json
  def destroy
    rescue_from_cancan(:destroy, Admin)
    @admin = Admin.find(params[:id])
    @admin.destroy

    respond_to do |format|
      format.html { redirect_to admin_admins_url }
      format.json { head :ok }
    end
  end
end
