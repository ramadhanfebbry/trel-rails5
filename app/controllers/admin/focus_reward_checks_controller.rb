class Admin::FocusRewardChecksController <  Admin::BaseController

  def index
    conditions = []
    conditions << "chains.id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "restaurants.id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    conditions << "users.email LIKE '%#{params[:email]}%'" unless params[:email].blank?
    if conditions.blank?
      @pos_reward_checks = FocusPos::RewardCheck.includes(:chain, :pos_location, :user).order("created_at DESC").paginate(:page => params[:page], :per_page => 10, :total_entries => -1)
    else
      @pos_reward_checks = FocusPos::RewardCheck.includes(:user, {:pos_location => :restaurant}, :chain).where(conditions.join(" AND ")).order("focus_reward_checks.created_at DESC").paginate(:page => params[:page], :per_page => 10, :total_entries => -1)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @pos_reward_check = PosRewardCheck.find params[:id]
    respond_to do |format|
      format.html
      format.js
    end
  end


  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : Restaurant.where("chain_id IN (?)", params[:chains]).order('name asc') rescue []
  end
end
