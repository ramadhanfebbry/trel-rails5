class Admin::OloReceiptsController < Admin::BaseController


  # GET /receipts
  def index
    rescue_from_cancan(:index, Receipt)
    #page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
    @receipts = []#Receipt.select("id, status, created_at, user_id, chain_id").active_chain.receipt_olo.desc_list.paginate(page: page, per_page: Setting.pagination.per_page) #Receipt.all

    respond_to do |format|
      format.html
      format.js
      format.csv {
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsOloJob.new(
                params,
                current_admin,
                true),
            :delayable_type => "receipt_olo_order_csv")
        redirect_to admin_online_orders_path, :notice => "Receipt CSV will sent to your email shotrly"
      }
    end
  end

  #def search
  #  #q = params[:search][:q]
  #  #q = q.gsub("'","''")
  #  #params[:search][:q] = q
  #  #
  #  #ar_field = [
  #  #    "receipts.id",
  #  #    "offers.name",
  #  #    "users.email",
  #  #    "chains.name",
  #  #    "restaurants.name",
  #  #    "receipt_transactions.receipt_number",
  #  #    "receipts.created_at",
  #  #    "receipts.status"
  #  #]
  #  #
  #  #order = params[:qry_order]
  #  #
  #  #if order.to_i < 1000
  #  #  qry_order = "#{ar_field[order.to_i]}"
  #  #  @receipts = []#Receipt.receipt_order_online.custom_search_online_orders(params[:search]).order(qry_order).reverse_order
  #  #  qry_order = qry_order + " desc"
  #  #elsif order.to_i > 999
  #  #  order = order.to_i - 1000
  #  #  qry_order = "#{ar_field[order]}"
  #  #  @receipts = []#Receipt.receipt_order_online.custom_search_online_orders(params[:search]).order(qry_order)
  #  #else
  #  #  qry_order = 'receipt_transactions.receipt_id desc'
  #  #  @receipts = []#Receipt.receipt_order_online.custom_search_online_orders(params[:search]).joins(:chain).order(qry_order)
  #  #end
  #  #page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
  #  ##count = Receipt.custom_search(params[:search]).joins(:chain).count(distinct: true)
  #  ##@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page, total_entries: count)
  #  #@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page)
  #  page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
  #  unless params['search']['q'].blank?
  #    @receipts = Receipt.active_chain.receipt_order_online.joins(:receipt_transactions).
  #      where('receipt_number like  ?', '%'+params['search']['q']+'%').select('distinct(receipts.*)').
  #      desc_list.paginate(page: params[:page], per_page: Setting.pagination.per_page) #Receipt.all
  #  else
  #    @receipts = Receipt.active_chain.receipt_order_online.desc_list.paginate(page:  params[:page], per_page: Setting.pagination.per_page) #Receipt.all
  #
  #  end
  #  #@action = '-search'
  #
  #  respond_to do |format|
  #    format.html {render :index}
  #    format.js { render :index }
  #    format.csv {
  #      #  @receipts = Receipt.custom_search(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order);render :layout => false, :action => :index
  #      Delayed::Job.enqueue(
  #          ExportCsvJob::ReceiptsJob.new(
  #              params,
  #              current_admin,
  #              nil, "barcode"),
  #          :delayable_type => "receipt_export_csv")
  #      redirect_to admin_receipt_barcodes_path, :notice => "Receipt CSV will sent to your email shotrly"
  #    }
  #  end
  #end

  def search
    q = params[:search][:q]
    q = q.gsub("'","''")
    params[:search][:q] = q

    ar_field = [
        "receipts.id",
        "offers.name",
        "users.email",
        "chains.name",
        "restaurants.name",
        "receipt_transactions.receipt_number",
        "receipts.created_at",
        "receipts.status"
    ]

    order = params[:qry_order]
    page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @receipts = Receipt.select("id, status, created_at, user_id, chain_id").receipt_olo.custom_search_online_orders(params[:search]).order(qry_order).reverse_order.paginate(page: page , per_page: Setting.pagination.per_page, total_entries: -1)
      qry_order = qry_order + " desc"
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @receipts = Receipt.select("id, status, created_at, user_id, chain_id").receipt_olo.custom_search_online_orders(params[:search]).order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, total_entries: -1)
    else
      qry_order = 'receipt_transactions.receipt_id desc'
      @receipts = Receipt.select("id, status, created_at, user_id, chain_id").receipt_olo.custom_search_online_orders(params[:search]).joins(:chain).order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, total_entries: -1)
    end
    #page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
    #count = Receipt.custom_search(params[:search]).joins(:chain).count(distinct: true)
    #@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page, total_entries: count)
    #@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page)
    @action = '-search'

    respond_to do |format|
      format.html {render :index}
      format.js { render :index }
      format.csv {
        #  @receipts = Receipt.custom_search(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order);render :layout => false, :action => :index
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsOloJob.new(
                params,
                current_admin,
                true),
            :delayable_type => "receipt_olo_order_csv")
        redirect_to admin_online_orders_path, :notice => "Receipt CSV will sent to your email shotrly"
      }
    end
  end

  # GET /receipts/1
  def show
    rescue_from_cancan(:show, Receipt)
    @receipt = Receipt.receipt_order_online.active_chain.find(params[:id])
    @receipt_transactions = @receipt.receipt_transactions.order('id desc')
  end

  # GET /receipts/1/edit
  def edit
    rescue_from_cancan(:edit, Receipt)
    @receipt = Receipt.receipt_order_online.active_chain.find(params[:id])
    @receipt_transaction = @receipt.last_transaction
  end

  # PUT /receipts/1
  def update
    rescue_from_cancan(:update, Receipt)
    @receipt = Receipt.active_chain.find(params[:id])
    @receipt.attributes = params[:receipt]
    @old_transaction = @receipt.last_transaction

    @receipt_transaction = ReceiptTransaction.new(@old_transaction.attributes.merge(params[:receipt_transaction]))
    @receipt_transaction.reviewer=current_admin
    @receipt_transaction.status = params[:receipt][:status]

    @receipt_transaction.receipt = @receipt
    @receipt_transaction.restaurant_offer = @old_transaction.restaurant_offer
    @receipt_transaction.reject_reason_id = 0 if @receipt.status!=4
    @receipt_transaction.instructions = {:rejected => "Reject Receipt"} if @receipt.status.to_i.eql?(4)
    @receipt_transaction.created_at = nil
    @receipt_transaction.updated_at = nil
    @receipt_transaction.admin_id = 1
    #@receipt_transaction.tax = params[:tax]
    #@receipt_transaction.cashier = params[:cashier]
    #@receipt_transaction.guests= params[:guests]
    #@receipt_transaction.total_payment = params[:total_payment]
    #@receipt_transaction.payment_method = params[:payment_method]
    #@receipt_transaction.check_number= params[:check_number]
    #@receipt_transaction.issue_date = params[:issue_date]
    @receipt_transaction.reviewer_validation = true
    @receipt_transaction.updated_manually = true
    @uniq_receipt = true
    @today_maxed = false
    @valid_max_subtotal = true
    if @receipt.status.to_i.eql?(Receipt::STATUS[:APPROVED])
      if @receipt.chain.max_subtotal > 0 && @receipt.chain.max_subtotal < @receipt_transaction.subtotal.to_f && !params[:ignore_subtotal].eql?("true")
        @valid_max_subtotal = false
      end
      if @receipt.chain.id.eql?(Setting.chain_id.zoes)
        receipt_number = @receipt_transaction.receipt_number.gsub(/\D/, '').to_i
        receipt_transactions = ReceiptTransaction.where("receipt_id != ? AND DATE(issue_date) = ? AND restaurant_id = ? AND LOWER(receipt_number) LIKE ? AND status = ?", @receipt.id, @receipt_transaction.issue_date.to_date, @receipt_transaction.restaurant_id, "%#{receipt_number}", Receipt::STATUS[:APPROVED]) rescue []
      else
        receipt_number = @receipt_transaction.receipt_number
        receipt_transactions = ReceiptTransaction.where("receipt_id != ? AND DATE(issue_date) = ? AND restaurant_id = ? AND receipt_number = ? AND status = ?", @receipt.id, @receipt_transaction.issue_date.to_date, @receipt_transaction.restaurant_id, receipt_number, Receipt::STATUS[:APPROVED]) rescue []
      end
      @uniq_receipt = false unless receipt_transactions.blank?
      @selected_receipts = receipt_transactions.map{|rt| rt.receipt}
    end
    if @receipt.status.to_i.eql?(Receipt::STATUS[:APPROVED]) && @uniq_receipt && @receipt.chain.today_max_user_limit?(@receipt, params[:receipt_transaction][:issue_date])
      @today_maxed = true
    end
    respond_to do |format|
      if @uniq_receipt && !@today_maxed && @valid_max_subtotal
        if @receipt_transaction.save and @receipt.update_attributes(params[:receipt])
          @is_save = true
          RestaurantUser.create(:user_id => @receipt.user_id, :restaurant_id => @receipt_transaction.restaurant_id) unless @receipt_transaction.restaurant_id.blank?
          if @receipt.status == Receipt::STATUS[:APPROVED]
            @receipt.push_one_time_reward(@receipt.user)
            @receipt.push_referral_code_reward ## reward from referral code
                                               #begin
                                               #  AverageChartTable.update_average(@receipt.last_transaction)
                                               #rescue => e
                                               #  puts "average chart table update #{e.inspect}"
                                               #end
          end
          #format.html { redirect_to admin_receipt_url(@receipt), notice: 'Receipt was successfully updated.' }
        else
          @is_save = false
          #format.html { render action: "edit" }
        end
      end
      format.js
    end
  end

  def reject_reasons
    @id = params[:id]
    @opt = RejectReason.select("reject_reasons.id, reject_reasons.description") #find_all_by_chain_id(@id)

    render :json => @opt
  end

  # DELETE /receipts/1
  def destroy
    rescue_from_cancan(:destroy, Receipt)
    @receipt = Receipt.active_chain.receipt_image.find(params[:id])
    @receipt.destroy

    respond_to do |format|
      format.html { redirect_to admin_receipts_url }
    end
  end

  # def check_custom_image_uploader
  #   require 'rubygems'
  #   require 'aws/s3'
  #   image_url = "http://3.bp.blogspot.com/_3I6eIowAe7I/TH8IfL8BHNI/AAAAAAAAAz0/ueyBcoDyU_0/s1600/krishna-christ.jpg"
  #   image_dimention = ["1250","200"]
  #   process_flag = 1
  #   @returning_data = image_processing image_url, image_dimention, process_flag
  #   render :text => @returning_data and return
  #   #render :text => "Looking ahead to see what can be done" and return
  # end

  def url
    @receipt = Receipt.find(params[:id])
    redirect_to @receipt.image.url
  end

  def blocked
    @receipt = Receipt.find(params[:id])
  end

  def goto
    @receipt = Receipt.find(params[:id]) rescue nil
  end

  def menu_receipt
    @receipt = Receipt.find(params[:id])
    if @receipt.is_receipt_barcode
      @transaction = @receipt.last_transaction rescue nil
      @pos_check_upload = @receipt.pos_check_upload rescue nil
      @pos_xml = PosReceiptXml.new(@pos_check_upload.xml_data) rescue nil
      @menu_items = @pos_xml.xml_in_nokogiri_format.search("menu-item") rescue []
      @menu_items_hash = {}
      @menu_items.map{|a| @menu_items_hash[a.attr("id").to_s] = (a.search("name").text rescue nil)}
      check_id = @pos_xml.check_id
      seq_num = @pos_xml.seq_num
      @reward_dicounts = RewardDiscount.where(:check_id => check_id, :seq_num =>  seq_num, :restaurant_id => @pos_check_upload.pos_location.restaurant_id)
      @discount_total = @pos_xml.discount_total
      @direct_discounts = @pos_xml.xml_in_nokogiri_format.search("discount") rescue []
    elsif @receipt.is_ncr_receipt
      @transaction = @receipt.last_transaction rescue nil
      @ncr_data_receipt = @receipt.ncr_data_receipt
      json_encode = @ncr_data_receipt.json_data.gsub("=>",":").gsub("nil", "0") rescue nil
      @receipt_data_in_json = ActiveSupport::JSON.decode(json_encode)
    end
  end


  def export_status
    #@receipts = Receipt.all
    respond_to do |format|
      format.csv {
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsOnlineStatusJob.new(
                params,
                current_admin,
                false, "onlen"),
            :delayable_type => "receipt_olo_order_csv")
        redirect_to admin_online_orders_path
      }
    end
  end
end
