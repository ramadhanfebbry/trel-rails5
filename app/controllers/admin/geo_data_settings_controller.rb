class Admin::GeoDataSettingsController <  Admin::BaseController
  # GET /admin/geo_data
  # GET /admin/geo_data.json
  def index
    @admin_geo_data = GeoData.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_geo_data }
    end
  end

  # GET /admin/geo_data/1
  # GET /admin/geo_data/1.json
  def show
    @chain = Chain.find(params[:chain_id])
    @geo_datum = @chain.geo_data_setting

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /admin/geo_data/new
  # GET /admin/geo_data/new.json
  def new
    @geo_datum = GeoDataSetting.new
    @chain = Chain.find   params["chain_id"]

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @admin_geo_datum }
    end
  end

  # GET /admin/geo_data/1/edit
  def edit
    @geo_datum = GeoDataSetting.find(params[:id])
    @chain = Chain.find params['chain_id']
  end

  # POST /admin/geo_data
  # POST /admin/geo_data.json
  def create
    @chain = Chain.find(params[:chain_id])
    @geo_datum = GeoDataSetting.new(params_geo_data_setting)

    respond_to do |format|
      if @geo_datum.save
        format.html { redirect_to admin_chain_geo_data_setting_path(@geo_datum.chain_id, @geo_datum), notice: 'Geo data was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /admin/geo_data/1
  # PUT /admin/geo_data/1.json
  def update
    @chain = Chain.find(params[:chain_id])
    @geo_datum = GeoDataSetting.find(params[:id])

    respond_to do |format|
      if @geo_datum.update(params_geo_data_setting)
        format.html { redirect_to admin_chain_geo_data_setting_path(@geo_datum.chain_id, @geo_datum), notice: 'Geo data was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @geo_datum.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/geo_data/1
  # DELETE /admin/geo_data/1.json
  def destroy
    @admin_geo_datum = Admin::GeoData.find(params[:id])
    @admin_geo_datum.destroy

    respond_to do |format|
      format.html { redirect_to admin_geo_data_index_url }
      format.json { head :ok }
    end
  end

  private

  def params_geo_data_setting
    params.required(:geo_data_setting).permit!
  end
end
