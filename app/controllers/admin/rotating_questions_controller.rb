class Admin::RotatingQuestionsController < Admin::BaseController

  before_action :find_group

  def index
    @rotating_questions = @rotating_group.questions.paginate :page => params[:page], :per_page => 10
  end

  def new
    @rotating_question = @rotating_group.questions.new
  end

  def edit
    @question = @rotating_group.questions.find params[:id]
  end

  def update
    @rotating_question = @rotating_group.questions.find params[:id]
    @update = @rotating_question.update_attributes(params[:question])
    @question = @rotating_question
  end

  def create
    @rotating_question = @rotating_group.questions.new params[:question]
    @save =  @rotating_question.save
    @question = @rotating_question
  end

  def new_choices
    @rotating_question = @rotating_group.questions.find params[:id]
    if @rotating_question.question_choices.blank?
      1.upto(2) do |x|
        @rotating_question.question_choices.build
      end
    end
  end

  def save_choices
    @rotating_question = @rotating_group.questions.find params[:id]
    @is_save =  @rotating_question.update_attributes(params[:question])
  end

  def question_with_type
    @value_type = params[:id].to_i
    @question = Question.new(:rotating_group_id => params[:rotating_group_id], :question_type => @value_type)
    1.upto(2) do |i|
      @question.question_choices.build
    end
  end

  def destroy
    @rotating_question = @rotating_group.questions.find params[:id]
    if @rotating_question
      choices = @rotating_question.question_choices
      choices.destroy_all
      @rotating_question.delete
    end
  end

  private

  def find_group
    @rotating_group = RotatingGroup.find params[:rotating_group_id]
  end

end