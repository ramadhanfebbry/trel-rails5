class Admin::RewardTransactionsController < Admin::BaseController

  before_action :get_user

  def index
    #@reward_transactions = @user.reward_transactions
    @reward_transactions = RewardTransaction.select("reward_transactions.reward_id, reward_transactions.restaurant_id,
 reward_transactions.user_id,  reward_transactions.*").where("user_id = ? and redeeming IS FALSE", @user.id)
  end
  

  protected
  def get_user
    @user = User.find(params[:user_id])
  end
end
