class Admin::DealsController < Admin::BaseController
  before_action :find_deal, :only => [:update_participation, :pick_incentive, :assign_incentive,:show,:edit,:create_reward_incentive,
    :create_offer_incentive,:create_offline_incentive, :show_participants, :load_ontime_reward, :load_points, :load_offline,
    :assign_with_existing_reward, :assign_with_existing_deal_offer, :remove_incentive
  ]

  def index

  end

  def pick_incentive
    # @deal = Deal.find params[:id]
  end
  
  def load_ontime_reward
    @rewards = @deal.chain.rewards.where(:reward_type => Reward::TYPES["INCENTIVE"]).paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  def load_points
    @points = DealOffer.where(:kind => 1).paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  def load_offline
    @offlines = DealOffer.where(:kind => 2).paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end


  def assign_with_existing_reward
    @reward = Reward.find(params[:reward_id])
    @deal.update_attributes(:incentive_type => "Reward", :incentive_id => @reward.id)
  end

  def assign_with_existing_deal_offer
    @deal_offer = DealOffer.find(params[:deal_offer_id])
    @deal.update_attributes(:incentive_type => "DealOffer", :incentive_id => @deal_offer.id)
  end

  def edit_incentive
    @offer = DealOffer.find(params[:id])
    @deal = @offer.deal
  end

  def update_incentive
    @offer = DealOffer.find(params[:id])
    @deal = Deal.where(:incentive_type => @offer.class.to_s, :incentive_id => @offer.id).first
    @is_save = false

    respond_to do |format|
      if @offer.update_attributes(params[:deal_offer])
        @is_save = true
        format.js { render :update_incentive }
      else
        format.js { render :update_incentive }
      end
    end
  end

  def assign_incentive
    # @deal = Deal.find params[:id]
    @type = params[:type]
    case @type
    when "reward"
      @reward = Reward.new
    when "offer", "offline"
      @offer = DealOffer.new
    end
  end

  def create_reward_incentive
    @is_save = false
    @reward = Reward.new(params[:reward])
    @reward.chain_id = @deal.chain.id
    reward_before = @deal.incentive
    @type = "reward"

    respond_to do |format|          
      if @reward.save and @deal.update_attributes(:incentive_type => "Reward", :incentive_id => @reward.id)
        @is_save = true
        reward_before.destroy if reward_before
        format.js { render :create_incentive }
      else
        format.js { render :create_incentive }
      end
    end
  end

  def create_offer_incentive
    @is_save = false
    @type = "offer"    
    @offer = DealOffer.new(params[:deal_offer])
    @offer.kind = DealOffer::TYPES["POINTS"]
    respond_to do |format|  
      if @offer.save
        @deal.update_attributes(:incentive_type => "DealOffer", :incentive_id => @offer.id)
        @is_save = true
        format.js { render :create_incentive }
      else
        format.js { render :create_incentive }
      end
    end
  end

  def remove_incentive
    @deal.update_attribute(:incentive, nil)
  end

  def create_offline_incentive
    @is_save = false
    @type = "offline"
    @offer = DealOffer.new(params[:deal_offer])
    @offer.kind = DealOffer::TYPES["OFFLINE"]

    respond_to do |format|
      if @offer.save
        @deal.update_attributes(:incentive_type => "DealOffer", :incentive_id => @offer.id)
        @is_save = true
        format.js { render :create_incentive }
      else
        format.js { render :create_incentive }
      end
    end
  end

  def deal_by_chain
    @chain = Chain.find(params[:chain_id]) rescue nil
    @deals = Deal.where(:chain_id => params[:chain_id]).order("updated_at DESC").paginate(page: params[:page], per_page: Setting.pagination.per_page) rescue []
    respond_to do |format|
      format.js
      format.html
    end
  end

  def new_deal
    @chain = Chain.find(params[:chain_id]) rescue nil
    @activity = params[:form_type].constantize.new
    @activity.build_deal(:chain_id => @chain.id)
    @activity.build_deal_asset_android
    @activity.build_deal_asset_iphone
  end

  def create_activity
    @chain = Chain.find(params[:chain_id])
    params[params[:form_type].underscore].merge!(
      :deal_asset_android_attributes => {:image => nil}
    )if params[params[:form_type].underscore][:deal_asset_android_attributes].blank?
    
    params[params[:form_type].underscore].merge!(
      :deal_asset_iphone_attributes => {:image => nil}
    )if params[params[:form_type].underscore][:deal_asset_iphone_attributes].blank?
    
    @activity = params[:form_type].constantize.new(params[params[:form_type].underscore])
    if @activity.save
      redirect_to deal_by_chain_admin_deals_path(@chain), :notice => "Deal has successfully created."
    else
      render "new_deal"
    end
  end

  def update_activity
    @chain = Chain.find(params[:chain_id])
    params[params[:form_type].underscore].merge!(
      :deal_asset_android_attributes => {:image => nil}
    )if params[params[:form_type].underscore][:deal_asset_android_attributes].blank?

    params[params[:form_type].underscore].merge!(
      :deal_asset_iphone_attributes => {:image => nil}
    )if params[params[:form_type].underscore][:deal_asset_iphone_attributes].blank?
    
    @activity = params[:form_type].constantize.find(params[:id])
    if @activity.update_attributes(params[params[:form_type].underscore])
      redirect_to deal_by_chain_admin_deals_path(@chain), :notice => "Deal has successfully updated."
    else
      render "edit"
    end
  end

  def show
    #  @deal = Deal.find(params[:id])
    @participants = @deal.restaurants
    @chain = @deal.chain
    ids = @participants.collect(&:id)
    @non_participants = Restaurant.where('chain_id = ? AND NOT id in (?)', @chain.id, ids.empty? ? '-1' : ids)
  end

  def update_participation
    rescue_from_cancan(:update_participation, Deal)
    submit= params[:commit]

    case submit
    when "Pull Out"
      @deal.pullout(params[:pullout])
    when "Participate"
      @deal.participate(params[:participate])
    end

    redirect_to admin_deal_path(@deal)
  end

  def edit
    #@deal = Deal.find(params[:id])
    @chain = @deal.chain
    @activity = @deal.activity
    @activity.build_deal_asset_android if @activity.deal_asset_android.blank?
    @activity.build_deal_asset_iphone if @activity.deal_asset_iphone.blank?
  end

  def destroy
    @deal = Deal.find(params[:id])
    @deal.destroy
    redirect_to admin_deals_path, :notice => "Deal has been successfully deactivate"
  end

  def deactivated
    @deals = Deal.deleted
  end

  def revive
    @deal = Deal.deleted.find(params[:id])
    @deal.revive

    redirect_to admin_deals_path, :notice => "Deal has been successfully Activate"
  end

  def show_participants
    if @deal.activity_type == "SubmitSurvey"
      @user_participants = @deal.user_deal_surveys
    else
      @user_participants = @deal.user_checkins rescue []
    end    
  end

  private

  def find_deal
    begin
      @deal = Deal.find params[:id]
    rescue
      # when the deal is deactivate
      @deal = Deal.deleted.find params[:id]
    end
  end
  
end
