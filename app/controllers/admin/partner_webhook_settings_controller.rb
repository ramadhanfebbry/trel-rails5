class Admin::PartnerWebhookSettingsController < Admin::BaseController

  def index
  end

  def search
    @chain = Chain.find(params["chain_id"])
    @partner_webhook_setting = PartnerWebhookSetting.find_by_chain_id(@chain.id)
  end

  def new
    @chain = Chain.find params['chain_id']
    @partner_webhook_setting = PartnerWebhookSetting.new(params[:partner_webhook_setting])
    render :layout => false
  end

  def edit
    @partner_webhook_setting = PartnerWebhookSetting.find params[:id]
    @chain = Chain.find @partner_webhook_setting.chain_id
    render :layout => false
  end

  def create
    @partner_webhook_setting = PartnerWebhookSetting.new(params[:partner_webhook_setting])
    if @partner_webhook_setting.save
      @save = true
    else
      @save = false
    end
  end

  def update
    @partner_webhook_setting = PartnerWebhookSetting.find params[:id]
    if @partner_webhook_setting.update_attributes(params[:partner_webhook_setting])
      @update = true
    else
      @update = false
    end
  end

  def destroy
    @partner_webhook_setting = PartnerWebhookSetting.find params[:id]
    @partner_webhook_setting.destroy unless @partner_webhook_setting.blank?

    redirect_to admin_partner_webhook_settings_path , :notice => "Partner Webhook Setting Deleted"
  end

end
