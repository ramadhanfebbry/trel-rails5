class Admin::NotificationSettingsController < Admin::BaseController

  before_action :find_notification, only: [:update, :edit, :create_we_miss, :update_we_miss, :create_no_scan]

  def index
    @chain = Chain.find(params['chain_id'])
    @chain.notification_setting_infos
  end

  def edit
    if @notification.notifiable_type.eql?("Reward")
      @chain = @notification.chain
      @definition = TaskDefinition.find_by_chain_id(@chain)
      if @definition.nil?
       @definition = TaskDefinition.new
       else
      @job = @definition.owner_jobs.last rescue nil
      @reward_id = @definition.reward_id
      end
       @rewards = @chain.rewards.where("reward_type = ?", Reward::TYPES["PUSH_REWARD"]).active
      elsif @notification.notifiable_type.eql?("NoScanDefinition")
        @chain = @notification.chain
        @definition = @chain.no_scan_definition
        if @definition.nil?
          @definition = NoScanDefinition.new
          @reward_id = @definition.reward_id
        else
          @job = @definition
        end
        @rewards = @chain.rewards.where("reward_type = ?", Reward::TYPES["PUSH_REWARD"]).active
      else
        @setting =  @notification.generate_setting
    end
  end

  def update
    if  @notification.update_email_template(params_notification)
      if @notification.type_send_email == "Fishbowl"
        notice = "Notification setting has updated to Fishbowl, Please make sure you have filled the Fishbowl Api Setting."
      else
        notice = "Notification setting has updated '#{@notification.name}"
      end
      redirect_to admin_chain_notification_settings_path(@notification.chain), notice: notice
    else
      @setting = @notification.chain
      render action: "edit"
    end
  end

  def create_we_miss
     date = params["by_one_time_date"]
    min = params["by_one_time_time"]["(5i)"].to_i
    hour = params["by_one_time_time"]["(4i)"].to_i
    puts "hour = #{hour}"
    hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
    plus_day = 0

    if hour > 24
      hour = hour - 24
      plus_day = 1
    end
    date = date.to_time.change(:hour => hour, :min => min)
    date = date + plus_day.days
    push_phone = params['task_definition']["push_phone"] == "1"
    push_email = params['task_definition']["push_email"] == "1"

    @definition = TaskDefinition.new(params[:task_definition].merge!(:status => 1,
                                                  :executed_at => date,
                                                  :push_phone => push_phone,
                                                  :push_email => push_email
                        )
    )

    case @definition.schedule_type
      when 1
        @definition.zone = params["user"]["time_zone_one"]
      when 2
        @definition.zone = params["user"]["time_zone_daily"]
      when 3
        @definition.zone = params["user"]["time_zone_weekly"]
      when 4
        @definition.zone = params["user"]["time_zone_monthly"]

    end
    @definition.task_scheduler = TaskScheduler.set_scheduler(params)

    if @definition.save! == false
      render :action => :edit
    else
      chain = @definition.chain rescue nil
      desc = "GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}" rescue nil
      @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,@definition.id)
      @success = OwnerJob.update_miss_you_task(chain,desc,nil,@definition.id, (@definition.owner_jobs.last.id rescue nil))
      @definition.owner_jobs.first.update_column(:status, 0) rescue nil
      @notification.update_attributes[:notification_setting]
      redirect_to admin_chain_notification_settings_path(@notification.chain), :notice => "Succesfully created"
    end
  end

  def update_we_miss
    date = params["by_one_time_date"]
    min = params["by_one_time_time"]["(5i)"].to_i
    hour = params["by_one_time_time"]["(4i)"].to_i
    puts "hour = #{hour}"
    hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
    plus_day = 0

    if hour > 24
      hour = hour - 24
      plus_day = 1
    end
    date = date.to_time.change(:hour => hour, :min => min)
    date = date + plus_day.days
    push_phone = params['task_definition']["push_phone"] == "1"
    push_email = params['task_definition']["push_email"] == "1"
    @definition = TaskDefinition.find(params[:task_definition_id])
    case @definition.schedule_type
      when 1
        @definition.zone = params["user"]["time_zone_one"]
      when 2
        @definition.zone = params["user"]["time_zone_daily"]
      when 3
        @definition.zone = params["user"]["time_zone_weekly"]
      when 4
        @definition.zone = params["user"]["time_zone_monthly"]

    end
    chain = @definition.chain
    desc = "GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}"
    @definition.task_scheduler = TaskScheduler.set_scheduler(params)

    if @definition.update_attributes(params[:task_definition].merge!(:status => 1,
                                                                     :executed_at => date,
                                                                     :push_phone => push_phone,
                                                                     :push_email => push_email
                                     )
    ) == false
      render :action => :edit
    else
     # @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,@definition.id)
      begin
      @success = OwnerJob.update_miss_you_task(chain,desc,nil,@definition.id, (@definition.owner_jobs.last.id rescue nil))
      @definition.owner_jobs.first.update_column(:status, 0) rescue nil

      rescue
       # @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,@definition.id)
        @success = OwnerJob.update_miss_you_task(chain,desc,nil,@definition.id, (@definition.owner_jobs.last.id rescue nil))
        @definition.owner_jobs.first.update_column(:status, 0) rescue nil
        end
      @notification.update_attributes(params[:notification_setting])

      redirect_to admin_chain_notification_settings_path(@notification.chain), :notice => "Succesfully Updated"
    end
  end


  def create_no_scan
    date = params["by_one_time_date"]
    min = params["by_one_time_time"]["(5i)"].to_i
    hour = params["by_one_time_time"]["(4i)"].to_i
    puts "hour = #{hour}"
    hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
    plus_day = 0

    if hour > 24
      hour = hour - 24
      plus_day = 1
    end
    date = date.to_time.change(:hour => hour, :min => min)
    date = date + plus_day.days
    push_phone = params['no_scan_definition']["push_phone"] == "1"
    push_email = params['no_scan_definition']["push_email"] == "1"

    @definition = NoScanDefinition.new(params[:no_scan_definition].merge!(:status => 1,
     :executed_at => date,
     :push_phone => push_phone,
     :push_email => push_email
     )
    )



    case @definition.schedule_type
    when 1
      @definition.zone = params["user"]["time_zone_one"]
    when 2
      @definition.zone = params["user"]["time_zone_daily"]
    when 3
      @definition.zone = params["user"]["time_zone_weekly"]
    when 4
      @definition.zone = params["user"]["time_zone_monthly"]

    end
    @definition.task_scheduler = NoScanScheduler.set_scheduler(params)

    if @definition.save! == false
      render :action => :new
    else
      chain = @definition.chain rescue @chain
      desc = "NO SCAN---GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}" rescue nil
      @success = NoScanJob.admin_create_no_scan_task(chain,desc,nil,@definition.id)
      @success = NoScanJob.update_no_scan_task(chain,desc,nil,@definition.id, (@definition.no_scan_jobs.last.id rescue nil))
      @definition.no_scan_jobs.first.update_column(:status, 0) rescue nil
      ap @notification
      @notification.update_attributes(params[:notification_setting])

      redirect_to admin_chain_notification_settings_path(@notification.chain), :notice => "Succesfully Created"
    end
  end


  private
  def find_notification
     @notification = NotificationSetting.find(params[:id])
  end

  def params_notification
    params.permit!
  end

end
