class Admin::EmailTemplatesController < Admin::BaseController

  cache_sweeper :email_template_sweeper, :only => [ :create, :update, :destroy ]
  before_action :find_chain

  def show
    @email_template = EmailTemplate.find(params[:id])
  end

  def new
    @email_template = EmailTemplate.new
    @email_template.chain_id = @chain.id
    template = ChainLocale::EMAILS
    @templates = template + ChainLocale::NCR_EMAIL
  end


  def create
    @email_template = EmailTemplate.new(params[:email_template])
    if @email_template.save
      redirect_to admin_chain_email_template_path(@chain, @email_template), :notice => "email template successfully updated."
    else
      template = ChainLocale::EMAILS
      @templates = template + ChainLocale::NCR_EMAIL
      render "new"
    end
  end

  def destroy
    @email_template = EmailTemplate.find(params[:id])
    @email_template.destroy
    redirect_to admin_chain_path(@email_template.chain), :notice => "email template successfully deleted."
  end

  def edit
    @email_template = EmailTemplate.find(params[:id])
    template = ChainLocale::EMAILS
    @templates = template + ChainLocale::NCR_EMAIL
  end

  def update
    @email_template = EmailTemplate.find(params[:id])
    if @email_template.update_attributes(params[:email_template])
      redirect_to admin_chain_email_template_path(@chain, @email_template), :notice => "email template successfully updated."
    else
      template = ChainLocale::EMAILS
      @templates = template + ChainLocale::NCR_EMAIL
      render "edit"
    end
  end

  private

  def find_chain
    @chain = Chain.find(params[:chain_id])
  end

end
