class Admin::PosMenuUploadsController < Admin::BaseController

  def index
    conditions = []
    conditions << "chains.id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "restaurants.id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    if conditions.blank?
      @pos_menu_uploads = PosMenuUpload.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    else
      @pos_menu_uploads = PosMenuUpload.includes(:pos_location => {:restaurant => :chain}).where(conditions.join(" AND ")).order("pos_menu_uploads.created_at DESC").paginate(:page => params[:page], :per_page => 10)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @pos_menu_upload = PosMenuUpload.find params[:id]
    respond_to do |format|
      format.html
      format.js
    end
  end


  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : (Restaurant.where("chain_id IN (?)", params[:chains]).order("name asc") rescue [])
  end


end