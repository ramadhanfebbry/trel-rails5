class Admin::BarcodeBatchsController < Admin::BaseController

  def index
    @barcode_batchs = BarcodeBatch.paginate(:page => params[:page], :per_page => 30)
  end

  def show
    @barcode_batch = BarcodeBatch.find params[:id]
    @codes = @barcode_batch.barcodes.paginate(:page => params[:page], :per_page => 30)
  end

end
