  class Admin::GeneralMenuItemsController < Admin::BaseController

  def index
    conditions = []
    conditions  << "general_menu_items.chain_id IN (#{params[:chains].join(',')})" unless params[:chains].blank?
    conditions  << "(LOWER(general_menu_items.item_name) LIKE '%#{(params[:q] || "").downcase}%' OR LOWER(general_menu_items.description) LIKE '%#{(params[:q] || "").downcase}%' OR LOWER(general_menu_items.item_keywords) LIKE '%#{(params[:q] || "").downcase}%')"
    @general_menu_items = GeneralMenuItem.where(conditions.join(" AND ")).order("chain_id").paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @general_menu_item = GeneralMenuItem.new
  end

  def create
    @general_menu_item = GeneralMenuItem.new(params[:general_menu_item])
    if @general_menu_item.save
      redirect_to admin_general_menu_items_path(@chain), :notice => "General Menu Item successfully created."
    else
      render :new
    end
  end

  def edit
    @general_menu_item = GeneralMenuItem.find params[:id]
  end

  def update
    @general_menu_item = GeneralMenuItem.find params[:id]
    if @general_menu_item.update_attributes(params[:general_menu_item])
      redirect_to admin_general_menu_items_path(@chain), :notice => "General Menu Item successfully updated."
    else
      render :edit
    end
  end

  def destroy
    @general_menu_item = GeneralMenuItem.find params[:id]
    @general_menu_item.destroy
    redirect_to admin_general_menu_items_path(@chain), :notice => "General Menu Item successfully destroyed."
  end

  def rewards
    @general_menu_item = @chain.general_menu_items.find params[:id]
    @rewards = @general_menu_item.rewards.paginate(:page => params[:page], :per_page => 10)
  end

  def add_reward
    @general_menu_item = @chain.general_menu_items.find params[:id]
    @reward_ids = @general_menu_item.rewards.map(&:id)
    @rewards = @chain.rewards.paginate(:page => params[:page], :per_page => 10)
  end

  def assign_reward
    @general_menu_item = @chain.general_menu_items.find params[:id]
    @reward = Reward.find params[:reward_id]
    RewardMenuItem.create(:reward_id => @reward.id, :general_menu_item_id => @general_menu_item.id)
    @reward_ids = @general_menu_item.rewards.map(&:id)
  end

  def remove_reward
    @general_menu_item = @chain.general_menu_items.find params[:id]
    @reward = Reward.find params[:reward_id]
    reward_menu_item = RewardMenuItem.where(:reward_id => @reward.id, :general_menu_item_id => @general_menu_item.id).first
    reward_menu_item.try :destroy
    @rewards = @general_menu_item.rewards.paginate(:page => params[:page], :per_page => 10)
  end

  def menu_items
    @general_menu_item = GeneralMenuItem.find params[:id]
    @chain = @general_menu_item.chain
    @pos_location = @chain.pos_locations.first
    @menu_items = @general_menu_item.pos_menu_items
    @menu_item_ids = @menu_items.map(&:id)
    @key = params[:q]
    conditions = []
    conditions << " item_number_olo is null "
    conditions  << "pos_locations.id = #{@pos_location.id}" if @pos_location
    conditions  << "(LOWER(CAST(pos_menu_items.item_number AS TEXT)) LIKE '%#{(@key || "").downcase}%' OR LOWER(pos_menu_items.item_name) LIKE '%#{(@key || "").downcase}%' OR LOWER(pos_menu_items.description) LIKE '%#{(@key || "").downcase}%')"
    @menu_items = GeneralPosMenuItem.select("pos_menu_items.*").joins("
      RIGHT JOIN pos_menu_items ON general_pos_menu_items.pos_menu_item_id = pos_menu_items.id AND general_pos_menu_items.general_menu_item_id = #{@general_menu_item.id}
      LEFT JOIN pos_locations ON pos_locations.id = pos_menu_items.pos_location_id
    ").order("general_pos_menu_items.updated_at DESC NULLS LAST")
    .where(conditions.join(" AND ")).paginate(:page => params[:page], :per_page => 10)
  end

  def add_menu_item
    @general_menu_item = GeneralMenuItem.find params[:id]
    @chain = @general_menu_item.chain
    @menu_item = PosMenuItem.find params[:menu_item_id]
    @chain.pos_menu_items.where(:item_number => @menu_item.item_number, :level_size_id => @menu_item.level_size_id).each do |pos_menu_item|
      GeneralPosMenuItem.create(:general_menu_item_id => @general_menu_item.id, :pos_menu_item_id => pos_menu_item.id)
    end
    #@menu_item.update_attribute(:general_menu_item_id, @general_menu_item.id)
    @menu_item_ids = @general_menu_item.pos_menu_items.map(&:id)
  end

  def add_olo_menu_item
    @general_menu_item = GeneralMenuItem.find params[:id]
    @chain = @general_menu_item.chain
    @menu_item = PosMenuItem.find params[:menu_item_id]
    @chain.pos_menu_items.where(:item_number_olo => @menu_item.item_number_olo, :level_size_id => @menu_item.level_size_id).each do |pos_menu_item|
      GeneralPosMenuItem.create(:general_menu_item_id => @general_menu_item.id, :pos_menu_item_id => pos_menu_item.id)
    end
    #@menu_item.update_attribute(:general_menu_item_id, @general_menu_item.id)
    @menu_item_ids = @general_menu_item.pos_menu_items.map(&:id)
  end

  def olo_menu_items
    @general_menu_item = GeneralMenuItem.find params[:id]
    @chain = @general_menu_item.chain
    @pos_location = @chain.pos_locations.first
    @menu_items = @general_menu_item.pos_menu_items
    @menu_item_ids = @menu_items.map(&:id)
    @key = params[:q]
    conditions = []
    conditions  << "pos_locations.id = #{@pos_location.id}" if @pos_location
    conditions  << "(LOWER(CAST(pos_menu_items.item_number AS TEXT)) LIKE '%#{(@key || "").downcase}%' OR LOWER(pos_menu_items.item_name) LIKE '%#{(@key || "").downcase}%' OR LOWER(pos_menu_items.description) LIKE '%#{(@key || "").downcase}%')"
    conditions << "item_number_olo IS NOT NULL"

    @menu_items = GeneralPosMenuItem.select("pos_menu_items.*").joins("
      RIGHT JOIN pos_menu_items ON general_pos_menu_items.pos_menu_item_id = pos_menu_items.id AND general_pos_menu_items.general_menu_item_id = #{@general_menu_item.id}
      LEFT JOIN pos_locations ON pos_locations.id = pos_menu_items.pos_location_id
    ").order("general_pos_menu_items.updated_at DESC NULLS LAST")
    .where(conditions.join(" AND ")).paginate(:page => params[:page], :per_page => 10)
  end

  def remove_menu_item
    @general_menu_item = GeneralMenuItem.find params[:id]
    @chain = @general_menu_item.chain
    @menu_item = PosMenuItem.find params[:menu_item_id]
    @chain.pos_menu_items.where(:item_number => @menu_item.item_number, :level_size_id => @menu_item.level_size_id).each do |pos_menu_item|
      gen_pos_menu_item = GeneralPosMenuItem.where(:general_menu_item_id => @general_menu_item.id, :pos_menu_item_id => pos_menu_item.id).first
      gen_pos_menu_item.destroy if gen_pos_menu_item
    end
    #@menu_item.try :update_attribute, :general_menu_item_id, nil
    @menu_item_ids = @general_menu_item.pos_menu_items.map(&:id)
  end

  def remove_olo_menu_item
    @general_menu_item = GeneralMenuItem.find params[:id]
    @chain = @general_menu_item.chain
    @menu_item = PosMenuItem.find params[:menu_item_id]
    @chain.pos_menu_items.where(:item_number_olo => @menu_item.item_number_olo, :level_size_id => @menu_item.level_size_id).each do |pos_menu_item|
      gen_pos_menu_item = GeneralPosMenuItem.where(:general_menu_item_id => @general_menu_item.id, :pos_menu_item_id => pos_menu_item.id).first
      gen_pos_menu_item.destroy if gen_pos_menu_item
    end
    #@menu_item.try :update_attribute, :general_menu_item_id, nil
    @menu_item_ids = @general_menu_item.pos_menu_items.map(&:id)
  end

  def add_range_start_end
    @general_menu_item = GeneralMenuItem.find(params[:id])
    if @general_menu_item.general_menu_item_range_value.blank?
      @general_menu_item.build_general_menu_item_range_value
    end
  end

  def submit_range_start_end
    @general_menu_item = GeneralMenuItem.find(params[:id])
    @is_save =  @general_menu_item.update_attributes(params[:general_menu_item])
  end

end
