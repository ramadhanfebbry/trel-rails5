class Admin::PushServiceNotificationsController < Admin::BaseController
  # GET /push_service_notifications
  # GET /push_service_notifications.json
  def index
    push_service_notifications = PushServiceNotification.select("id, chain_id, notification, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")
    push_service_points = PushServicePoint.select("id, chain_id, point, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")
    push_service_rewards = PushServiceReward.select("id, chain_id, reward_id, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")

    query_conditions = "lower(created_from) = 'admin' AND admin_id is not null"
    query_conditions += " AND chain_id = #{params[:chain_id]}" if params[:chain_id].present?
    query_reward_conditions = params[:reward_id].present? ? " AND reward_id = #{params[:reward_id]}" : ""

    @push_service_notifications = push_service_notifications.where(query_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)
    @push_service_points = push_service_points.where(query_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)
    @push_service_rewards = push_service_rewards.where(query_conditions+query_reward_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)

    @response_points = get_info(@push_service_points, 1) rescue []
    @response_rewards = get_info(@push_service_rewards, 2) rescue []
    respond_to do |format|
      format.html
      format.json { render json: @push_service_notifications }
      format.js {
        render "index.js"
      }
    end
  end

  def dashboard
    push_service_notifications = PushServiceNotification.select("id, chain_id, notification, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")
    push_service_points = PushServicePoint.select("id, chain_id, point, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")
    push_service_rewards = PushServiceReward.select("id, chain_id, reward_id, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")

    query_conditions = "lower(created_from) = 'dashboard' AND owner_email is not null"
    query_conditions += " AND chain_id = #{params[:chain_id]}" if params[:chain_id].present?
    query_reward_conditions = params[:reward_id].present? ? " AND reward_id = #{params[:reward_id]}" : ""

    @push_service_notifications = push_service_notifications.where(query_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)
    @push_service_points = push_service_points.where(query_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)
    @push_service_rewards = push_service_rewards.where(query_conditions+query_reward_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)

    @response_points = get_info(@push_service_points, 1) rescue []
    @response_rewards = get_info(@push_service_rewards, 2) rescue []
    respond_to do |format|
      format.html
      format.json { render json: @push_service_notifications }
      format.js {
        render "index.js"
      }
    end
  end

  def ipost
    push_service_notifications = PushServiceNotification.select("id, chain_id, notification, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")
    push_service_points = PushServicePoint.select("id, chain_id, point, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")
    push_service_rewards = PushServiceReward.select("id, chain_id, reward_id, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).order("created_at DESC")

    if params[:chain_id].present? && params[:user_id].present?
      query_conditions = "lower(created_from) = 'ipost'"
      query_conditions += " AND chain_id = #{params[:chain_id]} AND receiver_user_ids::text like '%#{params[:user_id]}%'" if params[:chain_id].present? && params[:user_id].present?
      query_reward_conditions = params[:reward_id].present? ? " AND reward_id = #{params[:reward_id]}" : ""

      @push_service_notifications = push_service_notifications.where(query_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)
      @push_service_points = push_service_points.where(query_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)
      @push_service_rewards = push_service_rewards.where(query_conditions+query_reward_conditions).paginate(:page => params[:page], :per_page => 10, total_entries: -1)
    end

    @response_points = get_info(@push_service_points, 1) rescue []
    @response_rewards = get_info(@push_service_rewards, 2) rescue []

    respond_to do |format|
      format.html
      format.json { render json: @push_service_notifications }
      format.js {
        render "index.js"
      }
    end
  end

  def sync
    @push_service_notifications = PushServiceNotification.select("id, chain_id, notification, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).
        order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    @push_service_points = PushServicePoint.select("id, chain_id, point, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).
        order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    @push_service_rewards = PushServiceReward.select("id, chain_id, reward_id, date_text_ttd, recipient_count, created_at, sent, status, admin_id, owner_email, is_all_user").includes(:chain).
        order("created_at DESC").paginate(:page => params[:page], :per_page => 10)

    p "* " * 44
    p @response_points = get_info(@push_service_points, 1) rescue []
    p @response_rewards = get_info(@push_service_rewards, 2) rescue []
    respond_to do |format|
      format.js {render "index.js"}
    end
  end

  # GET /push_service_notifications/1
  # GET /push_service_notifications/1.json
  def show
    using_api = true
    if params[:type].eql?("1")
      @push_service_notification = PushServicePoint.find(params[:id])
      url = "point"
    elsif params[:type].eql?("2")
      @push_service_notification = PushServiceReward.find(params[:id])
      url = "reward"
    else
      @push_service_notification = PushServiceNotification.find(params[:id])
      using_api = false
    end

    if using_api
      uri = URI.parse("http://104.236.198.63:8080/#{url}/#{@push_service_notification.id}")
      http = Net::HTTP.new(uri.host, uri.port)
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      request.basic_auth("admin", "password")
      request["Content-Type"] = "application/json; charset=utf-8"
      response = http.request(request)
      @response_json = JSON.load(response.body).first rescue nil
    end
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @push_service_notification }
    end
  end

  # GET /push_service_notifications/new
  # GET /push_service_notifications/new.json
  def new
    @push_service_notification = PushServiceNotification.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @push_service_notification }
    end
  end

  # GET /push_service_notifications/1/edit
  def edit
    @push_service_notification = PushServiceNotification.find(params[:id])
  end

  # POST /push_service_notifications
  # POST /push_service_notifications.json
  def create
    begin
      @push_service_notification = PushServiceNotification.new
      user_ids = []
      notification_user_ids = []

      @push_service_notification.errors.add(:service_type, "Can't be blank") if params["push_service_notification"]["service_type"].blank?
      service_type = params["push_service_notification"]["service_type"].to_i rescue nil
      if service_type.eql?(1)
        @push_service_notification.errors.add(:point, "can't be blank") if params["push_service_notification"]["point"].blank?
        @push_service_notification.errors.add(:point, "must be greater than zero") if params["push_service_notification"]["point"].to_i < 1
        notice_for = "POINTS"
      elsif service_type.eql?(2)
        @push_service_notification.errors.add(:reward, "can't be blank") if params["push_service_notification"]["reward_id"].blank?
        notice_for = "REWARDS"
      elsif service_type.eql?(3)
        @push_service_notification.errors.add(:notification, "can't be blank") if params["push_service_notification"]["notification"].blank?
        notice_for = "NOTIFICATIONS"
      elsif service_type.eql?(4)
        @push_service_notification.errors.add(:point, "can't be blank") if params["push_service_notification"]["point"].blank?
        @push_service_notification.errors.add(:point, "must be greater than zero") if params["push_service_notification"]["point"].to_i < 1
        @push_service_notification.errors.add(:notification, "can't be blank") if params["push_service_notification"]["notification"].blank?
        notice_for = "POINTS and NOTIFICATIONS"
      elsif service_type.eql?(5)
        @push_service_notification.errors.add(:reward, "can't be blank") if params["push_service_notification"]["reward_id"].blank?
        @push_service_notification.errors.add(:notification, "can't be blank") if params["push_service_notification"]["notification"].blank?
        notice_for = "REWARDS and NOTIFICATIONS"
      end

      chain = Chain.find params["push_service_notification"]["chain_id"] rescue nil
      @push_service_notification.errors.add(chain, "Chain can't be blank") if chain.blank?

      case params["select_type"]
        when "manual_enter"
          begin
            temp_receiver_user_ids = params["push_service_notification"]["temp_receiver_user_ids"]
            receiver_ids = params["push_service_notification"]["temp_receiver_user_ids"]
            user_ids = receiver_ids.squish.gsub("\\r", '').gsub('\\n', '').gsub(' ', '').split(',')
            notification_user_ids = receiver_ids.squish.gsub("\\r", '').gsub('\\n', '').gsub(' ', '').split(',')
            @push_service_notification.errors.add(:recepient, "Recepient can't be blank") if user_ids.blank?
          rescue => e
            @push_service_notification.errors.add(:recepient, e.message)
          end
        when "csv"
          test_file = params[:xls]
          unless test_file.blank?
            csv = CSV.read(test_file.path, {:headers => false})
            csv.each do |row|
              user_ids << row.first.gsub(/\D/, '').to_i rescue 0
              notification_user_ids << row.first.gsub(/\D/, '').to_i rescue 0
            end
          else
            @push_service_notification.errors.add(:xls, "Please Upload csv files !")
          end
        when "all_user"
          if @push_service_notification.errors.blank?
            user_ids = []
            notification_user_ids = []
          end
      end

      if params["immediate"] == "true"
        current_time = Time.now.utc
        full_date_time_text = "#{current_time}"
        time_now_in_utc = current_time
      else
        date_text = %w(1 2 3).map { |e| params["push_service_notification"]["sent_at(#{e}i)"]}.map{|x| x.rjust(2, '0')}.join("-")
        time_text = %w(4 5).map { |e| params["push_service_notification"]["sent_at(#{e}i)"] }.map{|x| x.rjust(2, '0')}.join(":")
        time_zone = params["push_service_notification"]['time_zone_default'].to_s
        full_date_time_text = "#{date_text} #{time_text} #{time_zone}"
        time_now_in_utc = full_date_time_text.to_datetime.utc
      end

      saved = false
      push_service = {
          temp_receiver_user_ids: temp_receiver_user_ids,
          chain_id: chain.id,
          date_text_ttd: full_date_time_text,
          sent_at: time_now_in_utc,
          admin_id: current_admin.try(:id),
          is_all_user: params["select_type"] == "all_user" ? true : false
      }

      if params["select_type"] == "all_user"
        user_ids_count = 0
        user_ids_list = "{0}"
        notification_user_ids_count = 0
        notification_user_ids_list = "{0}"
      else
        user_ids_count = user_ids.size
        user_ids_list = "{#{user_ids.join(',')}}"
        notification_user_ids_count = notification_user_ids.size rescue 0
        notification_user_ids_list = "{#{(notification_user_ids.join(',') rescue 0)}}" rescue 0
      end

      respond_to do |format|
        if @push_service_notification.errors.full_messages.blank?
          if service_type.eql?(1)
            push_service = PushServicePoint.new(push_service)
            push_service.point = params["push_service_notification"]["point"]
            push_service.recipient_count = user_ids_count
            push_service.receiver_user_ids = user_ids_list
            saved = true if push_service.save
            Delayed::Job.enqueue(PushServiceJob.new(push_service))
          elsif service_type.eql?(2)
            push_service = PushServiceReward.new(push_service)
            push_service.reward_id = params["push_service_notification"]["reward_id"]
            push_service.recipient_count = user_ids_count
            push_service.receiver_user_ids = user_ids_list
            saved = true if push_service.save
            Delayed::Job.enqueue(PushServiceJob.new(push_service))
          elsif service_type.eql?(3)
            push_service = PushServiceNotification.new(push_service)
            push_service.notification = params["push_service_notification"]["notification"]
            push_service.notification_url = params["push_service_notification"]["notification_url"]
            push_service.recipient_count = notification_user_ids_count
            push_service.receiver_user_ids = notification_user_ids_list
            saved = true if push_service.save
          elsif service_type.eql?(4) || service_type.eql?(5)
            if service_type.eql?(4)
              first_service = PushServicePoint.new(push_service)
              first_service.point = params["push_service_notification"]["point"]
            else
              first_service = PushServiceReward.new(push_service)
              first_service.reward_id = params["push_service_notification"]["reward_id"]
            end
            first_service.recipient_count = user_ids_count
            first_service.receiver_user_ids = user_ids_list
            first_service.save
            Delayed::Job.enqueue(PushServiceJob.new(first_service))

            second_service = PushServiceNotification.new(push_service)
            second_service.notification = params["push_service_notification"]["notification"]
            second_service.notification_url = params["push_service_notification"]["notification_url"]
            second_service.recipient_count = notification_user_ids_count
            second_service.receiver_user_ids = notification_user_ids_list
            second_service.save
            format.html { redirect_to admin_push_service_notifications_url, notice: "Plain push services #{notice_for} was successfully created." }
          end

          if saved
            format.html { redirect_to admin_push_service_notifications_url, notice: "Plain push services #{notice_for} was successfully created." }
          else
            format.html { render action: "new" }
          end
        else
          format.html { render action: "new" }
        end
      end
    rescue => e
      @push_service_notification.errors.add(:exeception, e.message)
      render action: "new"
    end
  end

  # PUT /push_service_notifications/1
  # PUT /push_service_notifications/1.json
  def update
    @push_service_notification = PushServiceNotification.find(params[:id])

    respond_to do |format|
      if @push_service_notification.update_attributes(params[:push_service_notification])
        format.html { redirect_to @push_service_notification, notice: 'Push service notification was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @push_service_notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /push_service_notifications/1
  # DELETE /push_service_notifications/1.json
  def destroy
    @push_service_notification = PushServiceNotification.find(params[:id])
    @push_service_notification.destroy

    respond_to do |format|
      format.html { redirect_to push_service_notifications_url }
      format.json { head :no_content }
    end
  end

  def progress
    @psn = PushServiceNotification.find(params[:id])
    @androids = PsnPushNotificationJob.select("id").where(:job_summary_id => @psn.id, :provider => 2)
    @ios = PsnPushNotificationJob.find_by_sql("SELECT recipient_count, success_count, failure_count from job_summary where job_id = #{@psn.id} AND provider = 1")
  end

  def cancel
    using_api = true
    if params[:type].eql?("1")
      @psn = PushServicePoint.find(params[:id])
      url = "point-update"
    elsif params[:type].eql?("2")
      @psn = PushServiceReward.find(params[:id])
      url = "reward-update"
    else
      @psn = PushServiceNotification.find(params[:id])
      using_api = false
      if !@psn.finished?
        @psn.cancel
        redirect_to admin_push_service_notifications_path, :notice => "Job canceled successfully."
      elsif !@psn.parsed?
        @psn.update_column("sent_at", Time.zone.now - 10.minutes)
        @psn.update_column("status", 3)
        redirect_to admin_push_service_notifications_path, :notice => "Job canceled successfully."
      else
        redirect_to admin_push_service_notifications_path, :notice => "Job canceled failed."
      end
    end

    if using_api
      begin
        uri = URI.parse("http://104.236.198.63:8080/#{url}/#{@psn.id}/3")
        http = Net::HTTP.new(uri.host, uri.port)
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.basic_auth("admin", "password")
        request["Content-Type"] = "application/json; charset=utf-8"
        http.request(request)
        notice = "Job canceled successfully."
      rescue
        notice = "Job canceled failed. please try again."
      end
      redirect_to admin_push_service_notifications_path, :notice => notice
    end
  end

  def pause
    using_api = true
    if params[:type].eql?("1")
      @psn = PushServicePoint.find(params[:id])
      url = "point-update"
    elsif params[:type].eql?("2")
      @psn = PushServiceReward.find(params[:id])
      url = "reward-update"
    else
      @psn = PushServiceNotification.find(params[:id])
      using_api = false
      if !@psn.finished?
        @psn.pause
        redirect_to admin_push_service_notifications_path, :notice => "Job paused successfully."
      else
        redirect_to admin_push_service_notifications_path, :notice => "Job paused failed."
      end
    end

    if using_api
      begin
        uri = URI.parse("http://104.236.198.63:8080/#{url}/#{@psn.id}/0")
        http = Net::HTTP.new(uri.host, uri.port)
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.basic_auth("admin", "password")
        request["Content-Type"] = "application/json; charset=utf-8"
        http.request(request)
        notice = "Job paused successfully."
      rescue
        notice = "Job paused failed. please try again."
      end
      redirect_to admin_push_service_notifications_path, :notice => notice
    end
  end

  def resume
    using_api = true
    if params[:type].eql?("1")
      @psn = PushServicePoint.find(params[:id])
      url = "point-update"
    elsif params[:type].eql?("2")
      @psn = PushServiceReward.find(params[:id])
      url = "reward-update"
    else
      @psn = PushServiceNotification.find(params[:id])
      using_api = false
      if @psn.paused?
        begin
          uri = URI.parse("http://104.236.198.63:8080/pn-update/#{@psn.id}/1")
          http = Net::HTTP.new(uri.host, uri.port)
          http.verify_mode = OpenSSL::SSL::VERIFY_NONE
          request = Net::HTTP::Post.new(uri.request_uri)
          request.basic_auth("admin", "password")
          request["Content-Type"] = "application/json; charset=utf-8"
          http.request(request)
          notice = "Job resume successfully."
        rescue
          notice = "Job resume failed. please try again."
        end
        redirect_to admin_push_service_notifications_path, :notice => "Job resumed successfully."
      else
        redirect_to admin_push_service_notifications_path, :notice => "Job resumed failed."
      end
    end

    if using_api
      begin
      uri = URI.parse("http://104.236.198.63:8080/#{url}/#{@psn.id}/1")
      http = Net::HTTP.new(uri.host, uri.port)
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Post.new(uri.request_uri)
      request.basic_auth("admin", "password")
      request["Content-Type"] = "application/json; charset=utf-8"
      http.request(request)
      notice = "Job resume successfully."
      rescue
        notice = "Job resume failed. please try again."
      end
      redirect_to admin_push_service_notifications_path, :notice => notice
    end

  end

  def load_user_list
    @chain = Chain.find params[:id] rescue nil
    #milestone_points = Steppingstone.where(chain_id: @chain.id, step_type: "points_earned").count rescue 0
    milestone_points = 0
    @rewards = @chain.rewards.where("reward_type != ?",Reward::TYPES["REGULAR"]).active.unexpired rescue []
    respond_to do |format|
      format.js
      format.json { render :json => {rewards: @rewards, milestone: milestone_points } }
    end
  end

  def get_time
    @time = Time.now.in_time_zone("EST")
    @time = Time.now.in_time_zone("EST") + 1.hours if params[:timezone_type] == "EDT"
  end

  private

  def get_info(pus_services, type)
    begin
      if pus_services.present?
        job_ids = pus_services.pluck(:id).join(",")
        if type.eql?(1)
          url = "point"
        elsif type.eql?(2)
          url = "reward"
        end
        uri = URI.parse("http://104.236.198.63:8080/#{url}/#{job_ids}")
        p uri
        http = Net::HTTP.new(uri.host, uri.port)
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Get.new(uri.request_uri)
        request.basic_auth("admin", "password")
        request["Content-Type"] = "application/json; charset=utf-8"
        response = http.request(request)
        response_json = JSON.load(response.body)
        return response_json
      else
        return response_json = []
      end
    rescue
      return response_json = []
    end
  end

end
