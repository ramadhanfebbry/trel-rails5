class Admin::RestaurantsController < Admin::BaseController

  # GET /restaurants
  def index
    rescue_from_cancan(:index, Restaurant)
    @name_tags = Tag.pluck(:name).join(",")
    if params['tag'].blank?
      @restaurants = Restaurant.includes(:tags).active_chain.paginate(page: params[:page], per_page: 10)#includes(:chain).all
    else
      @restaurants = Restaurant.includes(:tags).tagged_with(params['tag']).active_chain.paginate(page: params[:page], per_page: 10)#includes(:chain).all
    end
  end

  # GET /restaurants/1
  def show
    rescue_from_cancan(:show, Restaurant)
    @restaurant = Restaurant.active_chain.find(params[:id])
  end

  def owner_restaurant_list
    @params = params[:ch_id]

    unless @params.blank?
      @chain = Chain.find @params
      @rest_ids = @chain.restaurants.map(&:id)
      @rest_id = [params[:restaurant_id]] unless params[:restaurant_id].blank?
      @owners = Owner.where("role_id = 2 and restaurant_id in (?) and owners.is_active = ?", @rest_ids,true).joins(:restaurants_owners).uniq
      @chain_owners = Owner.where("role_id = 2 and chain_id in (?) and owners.is_active = ?", @chain.id,true).joins(:chains_owners).uniq
      @owners = @owners + @chain_owners
      @owners = @owners.uniq
      @selected_owner =Owner.where("role_id = 2 and restaurant_id in (?) and owners.is_active = ?", @rest_id,true).joins(:restaurants_owners).uniq
      @selected_owner = @selected_owner.uniq
    end
  end

  def change_status_restaurant
    @restaurant = Restaurant.find params[:id]
    # @restaurant.restaurant_detail.status
    @status = (@restaurant.restaurant_detail.try(:status) ? @restaurant.restaurant_detail.try(:status) : @restaurant.status) rescue nil
    render :layout => false
  end

  def save_status_restaurant
    p params
    @restaurant = Restaurant.find params[:id]

    case params['status'].to_i
      when Restaurant::TYPES["OPEN"]
        res_status = true
        res_detail_status = Restaurant::TYPES["OPEN"]
      when Restaurant::TYPES["CLOSED"]
        res_status = false
        res_detail_status =  Restaurant::TYPES["CLOSED"]
      when Restaurant::TYPES["COMING_SOON"]
        res_status =  true
        res_detail_status = Restaurant::TYPES["COMING_SOON"]
      when Restaurant::TYPES["TEMP_CLOSED"]
        res_status =  false
        res_detail_status = Restaurant::TYPES["TEMP_CLOSED"]
      when Restaurant::TYPES["TESTING"]
        res_status =  true
        res_detail_status = Restaurant::TYPES["TESTING"]
    end
    current_state = @restaurant.restaurant_detail.status
    @restaurant.perform_change_status(res_status, res_detail_status)
    @restaurant.update_change_status_log(current_state, params['status'].to_i)
    @restaurant.create_or_update_ldap rescue nil
    redirect_to admin_restaurants_path , :notice => "Status restaurant Updated"
  end

  # GET /restaurants/new
  def new
    rescue_from_cancan(:new, Restaurant)
    @restaurant = Restaurant.active_chain.new
  end

  # GET /restaurants/1/edit
  def edit
    rescue_from_cancan(:edit, Restaurant)
    @restaurant = Restaurant.active_chain.find(params[:id])
    @selected_tags = @restaurant.tag_list.split(",").map{|x| x.strip} unless @restaurant.tag_list.blank?
    0.upto(6) do |i|
      @restaurant.restaurant_hours.build(:day_of_week => i)
    end if @restaurant.restaurant_hours.blank?
  end

  # POST /restaurants
  def create
    rescue_from_cancan(:create, Restaurant)
    @restaurant = Restaurant.new(params[:restaurant])
    @restaurant.beacon_uuid.insert(4, '|') if @restaurant.beacon_uuid.length == 8
    @restaurant.ignored_zipcode = true ## Skip zipcode validations

    respond_to do |format|
      @restaurant.skip_generate_status = true
      if @restaurant.save
        ## save the RestaurantDetail
        # restaurant_detail = RestaurantDetail.new(params[:restaurant][:restaurant_detail])
        restaurant_detail = RestaurantDetail.find_or_initialize_by_restaurant_id(@restaurant.id)
        restaurant_detail.status = 0
        restaurant_detail.save
        ## save the owner
        owner_ids = params[:restaurant_select_id]
        unless owner_ids.blank?
          owner_ids.each do |owner|
            #owner.restaurant_ids << @restaurant.id
            #owner.save
          end
        end
        format.html { redirect_to admin_restaurant_path(@restaurant), notice: 'Restaurant was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /restaurants/1
  def update
    rescue_from_cancan(:update, Restaurant)
    @restaurant = Restaurant.active_chain.find(params[:id])
    @restaurant.ignored_zipcode = true ## Skip zipcode validations
    ch_id = @restaurant.chain_id
    params[:restaurant][:tag_list] = nil if params[:restaurant][:tag_list].blank?
    params[:restaurant][:beacon_uuid].insert(4, '|') if params[:restaurant][:beacon_uuid].length == 8

    respond_to do |format|
      if @restaurant.update_attributes(params[:restaurant])
        restaurant_detail = @restaurant.restaurant_detail
        if restaurant_detail.blank?
          restaurant_detail = RestaurantDetail.create(params[:restaurant][:restaurant_detail])
          restaurant_detail.restaurant_id = @restaurant.id
          restaurant_detail.save
        else
          restaurant_detail.update_attributes(params[:restaurant][:restaurant_detail])
        end
        owner_ids = params[:restaurant_select_id]
        begin
          @owners = Owner.where("role_id = 2 and restaurant_id in (?) and owners.is_active = ?",
                                [@restaurant],
                                true).
              joins(:restaurants_owners).uniq

          @owners = @owners + Owner.where("id in (?)", owner_ids) rescue []
          unless @owners.blank?
            @owners.each do |owner_tmp|
              owner = Owner.find(owner_tmp.id)
              puts owner.id

              if owner_ids and owner_ids.include?(owner.id.to_s)
                owner.restaurants_owners.where("restaurant_id in (?)", params[:id]).delete_all
                RestaurantsOwner.create(:owner_id => owner.id, :restaurant_id => params[:id])
              else
                owner.set_chain(owner.restaurant_list.first.chain.id) rescue nil
                owner.restaurants_owners.where("restaurant_id in (?)", params[:id]).delete_all
              end
            end if @owners
          else
            owner_ids.each do |owner|
              ow = Owner.find(owner)
              RestaurantsOwner.create(:owner_id => ow.id, :restaurant_id => params[:id])
            end if owner_ids
          end
        rescue => e
          puts "UPDATE::RESTAURANTS --- #{e.inspect}"
        end
        ### if restaurants are moved to another chain ## some rare case
        puts "OLD RESTAURANT_CHAIN_ID = #{ch_id.to_s}"
        puts "NEW RESTAURANT_CHAIN_ID = #{params["restaurant"]["chain_id"]}"
        if ch_id.to_s != params["restaurant"]["chain_id"]
          Restaurant.move_restaurant_owner_chain(@restaurant,ch_id,params["restaurant"]["chain_id"])
        end
        format.html { redirect_to admin_restaurant_path(@restaurant), notice: 'Restaurant was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /restaurants/1
  def destroy
    rescue_from_cancan(:destroy, Restaurant)
    @restaurant = Restaurant.active_chain.find(params[:id])
    @restaurant.update_column(:status, false)
    @restaurant.destroy rescue nil
    redirect_to admin_restaurants_url
  end

  def deleted
    rescue_from_cancan(:deleted, Restaurant)
    @restaurants = Restaurant.deleted.includes(:chain).paginate(page: params[:page], per_page: 10)#includes(:chain).all
  end

  def revive
    rescue_from_cancan(:revive, Restaurant)
    @restaurant = Restaurant.deleted.find(params[:id])
    @restaurant.revive
    redirect_to admin_restaurants_path, notice: 'Restaurant was successfully revived.'
  end

  def new_owner
    @owner = Owner.new(:role_id => 2)
  end

  def create_owner
    @owner = Owner.new(params[:owner].merge(:restaurant_ids => params[:owner][:restaurant_collections]))
    @owner.generate_password
    respond_to do |format|
      if @owner.save &&  !params[:owner][:restaurant_collections].blank?
        @owner.set_chain(params[:owner][:chain_id]) rescue nil
        #        @owner.update_participation_restaurant(params[:restaurants], params[:restaurant_ids] )
        Delayed::Job.enqueue(LdapJob.new(@owner.chain, @owner, @owner.password_generated), :run_at => Time.zone.now + 30.seconds)
        OwnerMailer.confirmation_email(@owner).deliver
        format.html { redirect_to detail_admin_owner_url(@owner), notice: 'Owner was successfully updated.' }
      elsif params[:owner][:restaurant_collections].blank?
        format.html { redirect_to "/admin/restaurants/new_owner", notice: 'Please select at least one restaurant.' }
      else
        format.html { render action: "new_owner" }
      end
    end
  end

  def by_chain
    chain = Chain.find(params[:chain_id]) rescue nil
    @restaurants = chain.restaurants rescue []
  end

  def group_chain_list
    sth = params[:search_text_hidden]
    ar_field = [
      "restaurants.id",
      "chains.name",
      "restaurants.name",
      "restaurants.address",
      "restaurants.latitude",
      "restaurants.longitude",
      "owners.first_name"
    ]

    order = params[:qry_order]
    qry_condition = "chains.status = 'active'"
    qry_condition += " AND chains.id = '#{params[:chn_id]}'" if params[:chn_id].present?
    qry_condition += " AND lower(restaurants.address) like '%#{params[:address].downcase}%'" if params[:address].present?

    sth_size = sth.split(" ").size rescue 2
    if sth_size.eql?(1) && /\D/.match(sth).blank?
      qry_condition += " AND (restaurants.id = '#{sth.gsub(" ","")}' OR lower(restaurants.name) like '%#{sth.downcase}%')"
    else
      qry_condition += " AND lower(restaurants.name) like '%#{sth.downcase}%'"
    end

    @restaurants = Restaurant.includes(:chain, :owners)
    if params[:tag].present?
      # @restaurants = @restaurants.tagged_with(params[:tag])
      qry_condition += " AND tags.name like '%#{params[:tag]}%'"
      @restaurants = @restaurants.joins(:tags)
    end
    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @restaurants = @restaurants.where(qry_condition).order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @restaurants = @restaurants.where(qry_condition).order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    else
      @restaurants = @restaurants.where(qry_condition).paginate(page: params[:page], per_page: 10)
    end
  end

  #show average visits
  def avg_visit
    @restaurant = Restaurant.find(params[:id])
    @ui_order_condition = {:email => "unsort", :date => "unsort"}
    order_condition = "user_id desc"
    if params[:sort_by_date].present?
      if params[:sort_by_date].to_s == "asc"
        @ui_order_condition[:date]= "desc"
        order_condition = "visit_at asc"
      elsif params[:sort_by_date].to_s == "desc"
        @ui_order_condition[:date]= "asc"
        order_condition = "visit_at desc"
      end
    end

    @day_visits = DayVisit.where(:restaurant_id => @restaurant.id).order(order_condition).includes(:user,:restaurant)
end

  def user_visits
    @restaurant = Restaurant.find(params[:id])
    @user = User.find params[:user_id]
    @day_visits = @user.day_visits.where("restaurant_id = ?",@restaurant.id).order("visit_at,restaurant_id asc")
    @hash_result = []

    unless @day_visits.blank?
      @day_visits.group_by(&:restaurant_id).delete_if { |k, v| !k }.each do |dv|
        dates = dv[1].map(&:visit_at) ## collect date
        puts dates
        date_size = dates.size
        if date_size > 1
          arr = dates.uniq.compact.each_cons(2).map { |a, b| ((b-a)/1.day).to_f * 100000 } rescue [0] ## calculate diff between days
        else
          arr = [0]
        end
        dividen = (date_size - 1) if date_size > 1
        dividen = 1 if dividen == 0 || date_size <= 1
        sum = (arr.sum.to_f / dividen.to_f) ## sum the diff
        #puts "average ==== #{sum}"
        begin
          @hash_result << {
            :restaurant_id => Restaurant.find(dv[0]).dashboard_display_text,
            :user_id => dv[1].first.user_id, :average => sum.round(2)
          }

        rescue
          next
        end
      end
    end
  end
  ## import from xls / csv

  def import_restaurants
    session[:import_restaurant] = nil
    regular_header = [ 'Name', 'App Display Text', 'DashboardDisplayName', 'Address', 'City', 'State', 'Country', 'ZipCode',
                       'PhoneNumber', 'Latitude', 'Longitude', 'Tags', 'SiteLive',
                       'SundayOpen', 'MondayOpen', 'TuesdayOpen', 'WednesdayOpen', 'ThursdayOpen', 'FridayOpen', 'SaturdayOpen',
                       'SundayClose', 'MondayClose', 'TuesdayClose', 'WednesdayClose', 'ThursdayClose', 'FridayClose', 'SaturdayClose',
                       'Beacon serial number', 'Beacon uuid', 'Location qrcode identifier', 'ExternalPartnerID']

    ftp_header = [ 'Name', 'App Display Text', 'DashboardDisplayName', 'Address', 'City', 'State', 'Country', 'ZipCode',
                   'PhoneNumber', 'Latitude', 'Longitude', 'Tags', 'SiteLive',
                   'SundayOpen', 'MondayOpen', 'TuesdayOpen', 'WednesdayOpen', 'ThursdayOpen', 'FridayOpen', 'SaturdayOpen',
                   'SundayClose', 'MondayClose', 'TuesdayClose', 'WednesdayClose', 'ThursdayClose', 'FridayClose', 'SaturdayClose',
                   'Beacon serial number', 'Beacon uuid', 'Location qrcode identifier', 'ExternalPartnerID']

    rest = [['42.967639','-95.801788','Omaha','Nebraska','United States'],
            ['41.761077','-88.242449','Aurora','Illinois','United States'],
            ['41.550719','-87.155659','Portage','Indiana','United States'],
            ['30.482474','-81.636993','Jacksonville','Florida','United States'],
            ['33.118706','-117.095258','Escondido','California','United States'],
            ['39.371629','-77.4125984','Frederick','Maryland','United States'],
            ['38.472667','-77.4117237','Stafford','Virginia','United States'],
            ['35.723841','-78.6474336','Garner','North Carolina','United States'],
            ['41.220464','-95.836025','Council Bluffs','Iowa','United States']]

    respond_to do |format|
      format.csv {
        if params[:ftp].blank?
          column_names = regular_header.join(",")

          file = "#{Rails.root}/tmp/csv-file.csv"
          File.open(file, "w") do |csv|
            csv << column_names
            csv << "\n"
            1.upto(9).each do |i|
              col = ["Restaurant Name (#{i+1})",
                     "Display Name (#{i+1})",
                     "Dashboard Display (#{i+1})",
                     "Street (#{i+1})",
                     "#{rest[i-1][2]}",
                     "#{rest[i-1][3]}",
                     "#{rest[i-1][4]}",
                     "#{(12345 + rand(9))}",
                     "#{(1000000000 + i)}",
                     "#{rest[i-1][0]}",
                     "#{rest[i-1][1]}",
                     "Tag #{i} ; Tag #{i+1}",
                     '1',
                     '09:00:00 AM', '09:00:00 AM', '09:00:00 AM', '09:00:00 AM', '09:00:00 AM', '09:00:00 AM', '09:00:00 AM',
                     '11:00:00 PM', '11:00:00 PM', '11:00:00 PM', '11:00:00 PM', '11:00:00 PM', '11:00:00 PM', '11:00:00 PM',
                     "0117C589F74#{(65 + i - 1).chr}",
                     "E9A7|9F3#{i}",
                     "aa4afd760#{i}", "1"]

              csv << col.join(",")
              csv << "\n"
            end
          end
          send_file(file)
        else
          column_names = ftp_header.join(",")

          file = "#{Rails.root}/tmp/ftp-csv-file.csv"
          File.open(file, "w") do |csv|
            csv << column_names
            csv << "\n"
            1.upto(9).each do |i|
              col = ["Restaurant Name (#{i+1})",
                     "Display Name (#{i+1})",
                     "Dashboard Display (#{i+1})",
                     "Street (#{i+1})",
                     "#{rest[i-1][2]}",
                     "#{rest[i-1][3]}",
                     "#{rest[i-1][4]}",
                     "#{(12345 + rand(9))}",
                     "#{(1000000000 + i)}",
                     "#{rest[i-1][0]}",
                     "#{rest[i-1][1]}",
                     "Tag #{i} ; Tag #{i+1}",
                     '1',
                     '09:00:00 AM', '09:00:00 AM', '09:00:00 AM', '09:00:00 AM', '09:00:00 AM', '09:00:00 AM', '09:00:00 AM',
                     '11:00:00 PM', '11:00:00 PM', '11:00:00 PM', '11:00:00 PM', '11:00:00 PM', '11:00:00 PM', '11:00:00 PM',
                     "0117C589F74#{(65 + i - 1).chr}",
                     "E9A7|9F3#{i}",
                     "aa4afd760#{i}", "1"]
              csv << col.join(",")
              csv << "\n"
            end
          end
          send_file(file)
        end
      }
      format.html {}
      format.xls {
        if params[:ftp].blank?
          book = Spreadsheet::Workbook.new
          sheet1 = book.create_worksheet
          sheet1.row(0).concat regular_header

          1.upto(9).each do |i|
            row = sheet1.row(i)
            row.push "Restaurant Name (#{i+1})" # Name
            row.push "Display Name (#{i+1})" # App Display Text
            row.push "Dashboard Display (#{i+1})" # Dashboard display text
            row.push "Street (#{i+1})" #Address
            row.push rest[i-1][2] #City
            row.push rest[i-1][3] #State
            row.push rest[i-1][4] #Country,
            row.push (12345 + rand(9)).to_s #Zipcode
            row.push (1000000000 + i).to_s #Phone Number
            row.push rest[i-1][0] #Latitude
            row.push rest[i-1][1] #Longitude
            row.push "Tag #{i} , Tag #{i+1}" #Tags
            row.push '1' # SiteLive

            1.upto(7).each do
              row.push '09:00:00 AM' #open time for a weeks
            end

            1.upto(7).each do
              row.push '11:00:00 PM' #close time for a weeks
            end

            row.push "0117C589F74#{(65 + i - 1).chr}"
            row.push "E9A7|9F3#{i}"
            row.push "aa4afd760#{i}"
            row.push '1' # ExternalPartnerID
          end

          book.write "#{Rails.root}/tmp/excel-file.xls"
          send_file("#{Rails.root}/tmp/excel-file.xls")

        else
          book = Spreadsheet::Workbook.new
          sheet1 = book.create_worksheet
          sheet1.row(0).concat  ftp_header

          1.upto(9).each do |i|
            row = sheet1.row(i)
            row.push "Restaurant Name (#{i+1})" # Name
            row.push "Display Name (#{i+1})" # App Display Text
            row.push "Dashboard Display (#{i+1})" # Dashboard display text
            row.push "Street (#{i+1})" #Address
            row.push rest[i-1][2] #City
            row.push rest[i-1][3] #State
            row.push rest[i-1][4] #Country,
            row.push (12345 + rand(9)).to_s #Zipcode
            row.push (1000000000 + i).to_s #Phone Number
            row.push rest[i-1][0] #Latitude
            row.push rest[i-1][1] #Longitude
            row.push "Tag #{i} , Tag #{i+1}" #Tags
            row.push '1' # SiteLive

            1.upto(7).each do
              row.push '09:00:00 AM' #open time for a weeks
            end

            1.upto(7).each do
              row.push '11:00:00 PM' #close time for a weeks
            end

            row.push "0117C589F74#{(65 + i - 1).chr}"
            row.push "E9A7|9F3#{i}"
            row.push "aa4afd760#{i}"
            row.push '1' # ExternalPartnerID
          end

          book.write "#{Rails.root}/tmp/ftp-excel-file.xls"
          send_file("#{Rails.root}/tmp/ftp-excel-file.xls")
        end

      }
    end
  end

  def import_preview
    test_file = params[:xls]
    begin
      book = Spreadsheet.open "#{test_file.path}"
    rescue
      return  redirect_to "/admin/restaurants/import_restaurants", :notice => "Pls upload the xls again"
    end
    #puts book
    sheet1 = book.worksheet 0
    @restaurants = []
    index = 0
    puts sheet1
    p sheet1
    p sheet1
    sheet1.each 1 do |row|
      #puts row
      res = Restaurant.new
      res.id = index

      if !row[8].to_s.match(/\A[-0-9 ()-+]+\z/).blank?
        valid_phone_number = row[8].to_s
      elsif !row[8].to_i.to_s.match(/\A[-0-9 ()-+]+\z/).blank?
        valid_phone_number = row[8].to_i.to_s
      else
        valid_phone_number = 0
      end

      p row
      p "Name : #{row[0]}"
      p "app display text #{row[1]}"
      p "dashboard_display_text #{row[2]}"
      p "address #{row[3]}"
      p "zipcode #{row[4].to_i.to_s}"
      p "phone_number #{valid_phone_number}"
      p "latitude #{row[6]}"
      p "longitude #{row[7]}"
      p "tags #{row[8]}"

      # Name
      res.name = row[0] if row[0].present?
      # App Display Text
      res.app_display_text = row[1].to_s.strip if row[1].present?
      # DashboardDisplayName
      res.dashboard_display_text = row[2].to_s.strip if row[2].present?
      # Address
      res.address = row[3].to_s.strip if row[3].present?
      # City
      res.city_import = row[4].to_s.strip.downcase if row[4].present?
      # State
      res.state_import = row[5].to_s.strip.downcase if row[5].present?
      # Country
      res.country_import = row[6].to_s.strip.downcase if row[6].present?
      # ZipCode
      res.zipcode = row[7].to_s.split(".").first rescue nil if row[7].present?
      # PhoneNumber
      res.phone_number = valid_phone_number if row[8].present?
      # Latitude
      res.latitude = row[9] if row[9].present?
      # Longitude
      res.longitude = row[10] if row[10].present?
      # Tags
      res.tag_list = (row[11].to_s.gsub(";",",").split(',').map{|x| x.strip} rescue nil) if row[11].present?
      # SiteLive
      res.status = (row[12].to_i == 1 ? true : false) if row[12].present?
      # Beacon serial number
      res.beacon_serial_number = row[27] if row[27].present?
      # Beacon uuid
      res.beacon_uuid = row[28] if row[28].present?
      # Location qrcode identifier
      res.location_qrcode_identifier = row[29] if row[29].present?
      # External Partner ID
      res.external_partner_id = row[30] if row[30].present?
      # City assignment to city_id
      res.city_id = (City.where("lower(name) like '%#{row[4].strip.downcase}%'").first.id rescue '') if row[4].present?

      index = index + 1
      return redirect_to import_restaurants_admin_restaurants_path, :notice => "Pls Fix || #{res.name} ---||  #{res.errors.full_messages}" if res.valid? == false and res.errors.full_messages.size > 2
      @restaurants << res
    end
    # Rails.cache.write('restaurant_import', @restaurants, :time_to_idle => 60.seconds, :timeToLive => 600.seconds)
    # current_admin.id
    REDIS.set("restaurant_import_via_web_#{current_admin.id}", @restaurants.to_json)
    REDIS.expire("restaurant_import_via_web_#{current_admin.id}", 1.hours)

    respond_to do |format|
      format.html {session[:import_restaurant]
      render :template => 'admin/restaurants/import_restaurants'
      }
      format.js
    end
  end

  def create_import
    tmp_restaurants = []
    tmp_index = []
    restaurants = JSON.parse(REDIS.get("restaurant_import_via_web_#{current_admin.id}"))
    redirect_to import_restaurants_admin_restaurants_path if restaurants.blank?
    # restaurants = Rails.cache.read('restaurant_import')
    #restaurants = restaurants.uniq.select{|x|  params[:restaurants].include?(x.id.to_s)} unless params[:restaurants].blank?
    restaurants.each_with_index do |x, index|
      unless params[:restaurants].blank?
        if  params[:restaurants].include?(x["id"].to_s)
          tmp_restaurants << x
          tmp_index << index
        end
      end
    end
    errors = nil
    res = []
    tmp_restaurants.each_with_index do |restaurant_data,index|
      restaurant = Restaurant.new(restaurant_data)
      #begin
        city = nil
        unless restaurant.city_import.blank?
          city = check_city_state_country(restaurant)
          return redirect_to import_restaurants_admin_restaurants_path, :notice => "Pls fix city, state and country OR just remove the data for that 3 column" if city.blank?
        end
        puts "CITY =============== #{city}"
        restaurant.id = nil
        if city.blank?
          restaurant.city = City.find(params[:city_ids][index])
        else
          restaurant.city = city
        end

        restaurant.chain_id = params[:chain_id]
        puts "processing..."
        restaurant.from_import = true ##  preventing owners to attached to this restaurant
        restaurant.tag_list = restaurant.tag_import
        restaurant.ignored_zipcode = true
        restaurant.save
        puts "save success"
        # restaurant.set_default_restaurant_hours
        # puts "save hours"
        restaurant.participate_to_rewards_and_offers
        puts "save rewards"
      # rescue => e
      #   res << [restaurant.name,restaurant.errors.full_messages]
      #   puts "ERROR RESTAURANT CONTROLLER::CREATE_IMPORT => #{e.inspect}"
      #   errors = restaurant.errors.full_messages
      #   break
      # end
    end

    if params[:restaurants].blank?
      notice = "Please select at least one restaurant"
    else
      notice = "Successfully import #{tmp_restaurants.size} restaurants"
      REDIS.del("restaurant_import_via_web_#{current_admin.id}")
    end
    # if res.blank?
    #   notice = params[:restaurants].blank? ? "Please select at least one restaurant" : "Successfully import #{tmp_restaurants.size} restaurants"
    # else
    #   notice = " #{res.join(',')}"
    # end
    redirect_to import_restaurants_admin_restaurants_path  , notice: notice
  end

  def upload_restaurants
  end

  def do_upload_restaurants
    return redirect_to "/admin/restaurants/upload_restaurants", :notice => "Please select chain" if params[:chain_id].blank?
    chain = Chain.find(params[:chain_id])
    # file_name = params[:rest_file].original_filename rescue ""
    # file_path = "#{params[:rest_file].path}"
      # file_ext = uploaded_io.original_filename.split(".").last.downcase rescue ""

    uploaded_io = params[:rest_file] rescue ""
    if uploaded_io.blank?
      return redirect_to "/admin/restaurants/upload_restaurants", :notice => "Pls upload the xls/csv again"
    else
      next_path = Rails.root.join('tmp', uploaded_io.original_filename)
      File.open(next_path, 'wb') do |file|
        file.write(uploaded_io.read)
      end

      if File.exist?(next_path)
        aw,bucket = Reward.initialize_aws
        #aw.buckets[bucket].objects["#{@id}_rewards_temporary"].delete
        aw.buckets[bucket].objects["#{chain.id}_#{uploaded_io.original_filename.split('.').first}_temporary"].write(:file => next_path.to_s)
        # @url_s3 = aw.buckets[bucket].objects["#{chain.id}_#{uploaded_io.original_filename}_temporary"].url_for(:read).to_s

        rih = RestaurantImportHistory.create(:chain_id => chain.id, :log_type => "manual via web", :file_name => uploaded_io.original_filename, :status => "in queue")
        Delayed::Job.enqueue(UploadRestaurantsJob.new(chain, uploaded_io.original_filename, rih), :delayable_type => "Upload Restaurants")

        return redirect_to "/admin/restaurants/upload_restaurants", :notice => "Processing File"
      else
        return redirect_to "/admin/restaurants/upload_restaurants", :notice => "Pls upload the xls/csv again"
      end
    end

  end

  def ftp_form

  end

  def import_by_ftp
    rih = RestaurantImportHistory.create(:chain_id => params[:chain], :log_type => "manual via ftp", :status => "in queue")
    Delayed::Job.enqueue(ImportRestaurantsJob.new(params[:chain], params[:fname], rih.id), :delayable_type => "Import Restaurants")
    redirect_to import_restaurants_histories_admin_restaurants_path
  end

  def import_restaurants_histories
    p params
    if params
      @histories = RestaurantImportHistory.where(:chain_id => params[:chain_id]).order("created_at desc").paginate(page: params[:page], per_page: 10)
    else
      @histories = RestaurantImportHistory.order("created_at desc").paginate(page: params[:page], per_page: 10)
    end
  end

  def import_restaurants_status
    p params
    @status = params[:status].to_i
    @id = params[:id]
    restaurant = RestaurantImportHistory.find(params[:id])
    p restaurant.success_restaurants
    if @status == 1
      @lists = restaurant.success_restaurants.paginate(page: params[:page], per_page: 100)
    elsif @status == 2
      @lists = restaurant.failed_restaurants.paginate(page: params[:page], per_page: 100)
    end
  end

  require 'net/ftp'
  def get_list_file_ftp
    chain = Chain.find(params[:chain])
    if chain.ftp_url && chain.ftp_port && chain.ftp_username && chain.ftp_password
      ftp =Net::FTP.new
      ftp.passive = true
      ftp.connect(chain.ftp_url,chain.ftp_port)
      ftp.login(chain.ftp_username,chain.ftp_password)
      @files = ftp.nlst("*.csv") | ftp.nlst("*.xls")
      ftp.close
    else
      @files = nil
    end
  end

  private

  def check_city_state_country(res)
    puts "okeeeeey"
     city = City.where("lower(name) like '#{res.city_import.downcase}'").first rescue nil
    unless city.blank?
      return city
    else
      # check if state city, country are correct
      #if !res.country_import.blank? and CS.countries.values.include?(res.country_import.titleize)
        country_exist = Country.where("lower(name) like '#{res.country_import.gsub("'", "''").downcase}'").first rescue nil
        if country_exist.blank?
          country_exist = Country.create(
            :name => res.country_import.titleize,
            :abbreviation => (CS.countries.key(res.country_import.titleize) rescue nil)
          )
        end
      #end

      # if state exist
      #if !res.state_import.blank? and !country_exist.blank? and CS.states(country_exist.abbreviation.to_sym).values.include?(res.state_import.titleize)
        state_exist = Region.where("country_id = #{country_exist.id} AND lower(name) like '#{res.state_import.gsub("'", "''").downcase}' AND abbreviation = '#{CS.states(country_exist.abbreviation).key(res.state_import.titleize)}'").first rescue nil
        if state_exist.blank?
          state_exist = Region.create(
              :name => res.state_import.titleize,
              :abbreviation => (CS.states(country_exist.abbreviation).key(res.state_import.titleize) rescue nil),
              :country_id => country_exist.id
          )
        end
      #end

      #if !res.city_import.blank? and !state_exist.blank? and CS.cities(state_exist.abbreviation.to_sym,country_exist.abbreviation.to_sym).include?(res.city_import.titleize)
        city_exist = City.where("lower(name) like '#{res.city_import.gsub("'", "''").downcase}'").first rescue nil
        if city_exist.blank?
          city_exist = City.create(
              :name => res.city_import.titleize,
              :region_id => state_exist.id
          )
          puts "CITY HEREE"
          return city_exist
        end
      #end
    end
  end

end
