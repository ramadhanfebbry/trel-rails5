class Admin::TagsController < Admin::BaseController
  # GET /admin/@tags
  # GET /admin/@tags.json
  def index
    @tags = Tag.order("category_type, created_at")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tags }
    end
  end

  # GET /admin/@tags/1
  # GET /admin/@tags/1.json
  def show
    @tag = Tag.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tag }
    end
  end

  # GET /admin/@tags/new
  # GET /admin/@tags/new.json
  def new
    @tag = Tag.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tag }
    end
  end

  # GET /admin/@tags/1/edit
  def edit
    @tag = Tag.find(params[:id])
  end

  # POST /admin/@tags
  # POST /admin/@tags.json
  def create
    @tag = Tag.new(params[:tag])
    @tag.for_user = true

    respond_to do |format|
      if @tag.save
        format.html { redirect_to admin_tags_path, notice: '@tag was successfully created.' }
        format.json { render json: @tag, status: :created, location: @tag }
      else
        format.html { render action: "new" }
        format.json { render json: @tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/@tags/1
  # PUT /admin/@tags/1.json
  def update
    @tag = Tag.find(params[:id])
    @tag.for_user = true

    respond_to do |format|
      if @tag.update_attributes(params[:tag])
        format.html { redirect_to admin_tags_url, notice: 'Tag was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @tag.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/@tags/1
  # DELETE /admin/@tags/1.json
  def destroy
    @tag = Tag.find(params[:id])
    @tag.destroy

    respond_to do |format|
      format.html { redirect_to admin_tags_url }
      format.json { head :ok }
    end
  end
end