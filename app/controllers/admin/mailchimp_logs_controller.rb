class Admin::MailchimpLogsController < Admin::BaseController

  def index
    @mailchimp_logs = MailchimpLog.order("created_at DESC").paginate(:per_page => 20, :page => params[:page])
  end

  def success_users
    @mailchimp_log = MailchimpLog.find params[:id]
    @users = @mailchimp_log.success_users.paginate(:per_page => 30, :page => params[:page])
  end

  def failed_users
    @mailchimp_log = MailchimpLog.find params[:id]
    @users = @mailchimp_log.failed_users.paginate(:per_page => 30, :page => params[:page])
  end

  def filter_log
    conditions = []
    conditions << "chain_id = #{params[:chain_id]}" unless params[:chain_id].blank?
    conditions << "status = '#{params[:status]}'" unless params[:status].blank?
    conditions = conditions.join(" AND ")
    @mailchimp_logs = MailchimpLog.where(conditions).order("created_at DESC").paginate(:per_page => 20, :page => params[:page])
    render :template => "admin/mailchimp_logs/index.html"
  end

end