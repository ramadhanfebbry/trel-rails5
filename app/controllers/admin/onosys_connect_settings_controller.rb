class Admin::OnosysConnectSettingsController < Admin::BaseController

  def index
  end

  def search
    @chain = Chain.find(params["chain_id"])
    @onosys_connect_setting = OnosysConnectSetting.find_by_chain_id(@chain.id)
  end

  def new
    @chain = Chain.find params['chain_id']
    @onosys_connect_setting = OnosysConnectSetting.new
    render :layout => false
  end

  def edit
    @onosys_connect_setting = OnosysConnectSetting.find params[:id]
    @chain = Chain.find @onosys_connect_setting.chain_id
    render :layout => false
  end

  def create
    @onosys_connect_setting = OnosysConnectSetting.new(params[:onosys_connect_setting])

    if @onosys_connect_setting.save
      @save = true
    else
      @save = false
    end
  end

  def update
    @onosys_connect_setting = OnosysConnectSetting.find params[:id]
    if @onosys_connect_setting.update_attributes(params[:onosys_connect_setting])
      @update = true
    else
      @update = false
    end
  end

  def destroy
    @onosys_connect_setting = OnosysConnectSetting.find params[:id]
    chain_id = @onosys_connect_setting.chain.try(:id)
    if @onosys_connect_setting.destroy
      Rails.cache.write("onosys_setting_#{chain_id}_api_root", nil)
      Rails.cache.write("onosys_setting_#{chain_id}_api_key", nil)
      Rails.cache.write("onosys_setting_#{chain_id}_iphone_api_key", nil)
      Rails.cache.write("onosys_setting_#{chain_id}_android_api_key", nil)
      Rails.cache.write("onosys_setting_#{chain_id}_client_id", nil)
      Rails.cache.write("onosys_setting_#{chain_id}_client_secret", nil)
      Rails.cache.write("onosys_setting_#{chain_id}_provider", nil)
      redirect_to admin_onosys_connect_settings_path , :notice => "Onosys Connect Setting Deleted"
    else
      redirect_to admin_onosys_connect_settings_path , :notice => "Failed to Delete Onosys Connect Setting"
    end
  end
end
