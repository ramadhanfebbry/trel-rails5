class Admin::SocialShareParticipantsController < Admin::BaseController
  before_action :find_social_share

  def index
    if params[:export].blank?
      search  = params['search'].blank?? nil : params['search']
      @participants = @social.social_share_participants.search(search).order('created_at desc').
          paginate(page: params[:page], per_page: 10)
    elsif params[:export] == "true"

      Delayed::Job.enqueue(
          ExportCsvJob::SocialParticipant.new(params,current_admin,@social),
          :delayable_type => "social_share")
    end

    respond_to do |format|
      format.html{}
      format.js{}
    end
  end


  private

  def find_social_share
    @social = SocialShare.find(params[:id])
  end

end