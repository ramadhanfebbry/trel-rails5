class Admin::TaskDefinitionsController < Admin::BaseController
  before_action :find_chain

  def index
    @definition = @chain.task_definition
    @job = @definition
  end

  def edit
    @definition = TaskDefinition.find params[:id]
    @job = @definition.owner_jobs.last rescue nil
    @reward_id = @definition.reward_id
    @rewards = @chain.rewards.where("reward_type = ?", Reward::TYPES["PUSH_REWARD"]).active
  end

  def generate_job
    chain = Chain.find params['chain_id']
    definition = TaskDefinition.find params[:id]
    desc = "GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}"
    @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,definition.id)
    @success = OwnerJob.update_miss_you_task(chain,desc,nil,definition.id, job.id)

    if @success.class != Hash
      definition.owner_jobs.first.update_column(:status, 0) rescue nil
      @jobs = OwnerJob.where(:owner_id => nil).order('created_at desc').
          paginate(page: params[:page], per_page: Setting.pagination.per_page)
      redirect_to :back , :notice => "JOB ALREADY CREATED"
    end
  end

  def testing_fishbowl_miss_you

  end

  def post_testing_fishbowl_miss_you
    # definition = TaskDefinition.find params[:id]
    # a= OwnerJob::JobFromOwner.new(definition.owner_jobs.first)
    # users = User.where("id in(?)", params["email_list"].squish.gsub("\\r",'').gsub('\\n','').gsub(' ','').split(','))
    # a.push_reward_or_point(definition.owner_jobs.first,users)
    puts "Owner scheduler running From admin..."
    OwnerScheduler.running_owner_scheduler
    puts "Owner scheduler completed From admin..."
    redirect_to :back, :notice => "Scheduler are ran"
  end

  def update_job
    job = OwnerJob.find params[:job_id]
    chain = Chain.find params['chain_id']
    definition = TaskDefinition.find params[:id]
    desc = "GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}"
    @success = OwnerJob.update_miss_you_task(chain,desc,nil,definition.id, job.id)

    if @success.class != Hash
      definition.owner_jobs.first.update_column(:status, 0) rescue nil
      @jobs = OwnerJob.where(:owner_id => nil).order('created_at desc').
          paginate(page: params[:page], per_page: Setting.pagination.per_page)
      redirect_to :back , :notice => "JOB ALREADY Updated"
    end
  end

  def update
    date = params["by_one_time_date"]
    min = params["by_one_time_time"]["(5i)"].to_i
    hour = params["by_one_time_time"]["(4i)"].to_i
    puts "hour = #{hour}"
    hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
    plus_day = 0

    if hour > 24
      hour = hour - 24
      plus_day = 1
    end
    date = date.to_time.change(:hour => hour, :min => min)
    date = date + plus_day.days
    push_phone = params['task_definition']["push_phone"] == "1"
    push_email = params['task_definition']["push_email"] == "1"
    @definition = TaskDefinition.find(params[:id])
    case @definition.schedule_type
      when 1
        @definition.zone = params["user"]["time_zone_one"]
      when 2
        @definition.zone = params["user"]["time_zone_daily"]
      when 3
        @definition.zone = params["user"]["time_zone_weekly"]
      when 4
        @definition.zone = params["user"]["time_zone_monthly"]

    end
    chain = @definition.chain
    desc = "GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}"
    @definition.task_scheduler = TaskScheduler.set_scheduler(params)

    if @definition.update_attributes(task_definitions_params.merge!(:status => 1,
                                                                     :executed_at => date,
                                                                     :push_phone => push_phone,
                                                                     :push_email => push_email
                                     )
    ) == false
      render :action => :new
    else
     # @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,@definition.id)
      begin
      @success = OwnerJob.update_miss_you_task(chain,desc,nil,@definition.id, (@definition.owner_jobs.last.id rescue nil))
      @definition.owner_jobs.first.update_column(:status, 0) rescue nil

      rescue
       # @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,@definition.id)
        @success = OwnerJob.update_miss_you_task(chain,desc,nil,@definition.id, (@definition.owner_jobs.last.id rescue nil))
        @definition.owner_jobs.first.update_column(:status, 0) rescue nil
        end
      redirect_to admin_chain_task_definitions_path, :notice => "Succesfully Updated"
    end
  end

  def new
    @definition = TaskDefinition.new
    @rewards = @chain.rewards.where("reward_type = ?", Reward::TYPES["PUSH_REWARD"]).active
  end

  def create
    date = params["by_one_time_date"]
    min = params["by_one_time_time"]["(5i)"].to_i
    hour = params["by_one_time_time"]["(4i)"].to_i
    puts "hour = #{hour}"
    hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
    plus_day = 0

    if hour > 24
      hour = hour - 24
      plus_day = 1
    end
    date = date.to_time.change(:hour => hour, :min => min)
    date = date + plus_day.days
    push_phone = params['task_definition']["push_phone"] == "1"
    push_email = params['task_definition']["push_email"] == "1"

    @definition = TaskDefinition.new(task_definitions_params.merge!(:status => 1, 
                                                  :executed_at => date,
                                                  :push_phone => push_phone,
                                                  :push_email => push_email
                        )
    )

    case @definition.schedule_type
      when 1
        @definition.zone = params["user"]["time_zone_one"]
      when 2
        @definition.zone = params["user"]["time_zone_daily"]
      when 3
        @definition.zone = params["user"]["time_zone_weekly"]
      when 4
        @definition.zone = params["user"]["time_zone_monthly"]

    end
    @definition.task_scheduler = TaskScheduler.set_scheduler(params)

    if @definition.save! == false
      render :action => :new
    else
      chain = @definition.chain rescue nil
      desc = "GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}" rescue nil
      @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,@definition.id)
      @success = OwnerJob.update_miss_you_task(chain,desc,nil,@definition.id, (@definition.owner_jobs.last.id rescue nil))
      @definition.owner_jobs.first.update_column(:status, 0) rescue nil
      redirect_to admin_chain_task_definitions_path, :notice => "Succesfully Created"
    end
  end


  ### NO SCAN SECTION
  def no_scan_tasks
    @definition = @chain.no_scan_definition
    @job = @definition
  end

  def edit_no_scan
    @definition = @chain.no_scan_definition
    @job = @definition
    @rewards = @chain.rewards.where("reward_type = ?", Reward::TYPES["PUSH_REWARD"]).active
  end

  def new_no_scan
    @definition = NoScanDefinition.new
    @rewards = @chain.rewards.where("reward_type != ?", Reward::TYPES["PUSH_REWARD"]).active
  end

  def post_no_scan
    date = params["by_one_time_date"]
    min = params["by_one_time_time"]["(5i)"].to_i
    hour = params["by_one_time_time"]["(4i)"].to_i
    puts "hour = #{hour}"
    hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
    plus_day = 0

    if hour > 24
      hour = hour - 24
      plus_day = 1
    end
    date = date.to_time.change(:hour => hour, :min => min)
    date = date + plus_day.days
    push_phone = params['no_scan_definition']["push_phone"] == "1"
    push_email = params['no_scan_definition']["push_email"] == "1"

    @definition = NoScanDefinition.new(no_scan_params.merge!(:status => 1,
                                                                     :executed_at => date,
                                                                     :push_phone => push_phone,
                                                                     :push_email => push_email
                                     )
    )



    case @definition.schedule_type
      when 1
        @definition.zone = params["user"]["time_zone_one"]
      when 2
        @definition.zone = params["user"]["time_zone_daily"]
      when 3
        @definition.zone = params["user"]["time_zone_weekly"]
      when 4
        @definition.zone = params["user"]["time_zone_monthly"]

    end
    @definition.task_scheduler = NoScanScheduler.set_scheduler(params)

    if @definition.save! == false
      render :action => :new
    else
      chain = @definition.chain rescue @chain
      desc = "NO SCAN---GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}" rescue nil
      @success = NoScanJob.admin_create_no_scan_task(chain,desc,nil,@definition.id)
      @success = NoScanJob.update_no_scan_task(chain,desc,nil,@definition.id, (@definition.no_scan_jobs.last.id rescue nil))
      @definition.no_scan_jobs.first.update_column(:status, 0) rescue nil
      if @success.class != Hash and @success.errors.any?
        redirect_to :back, :notice => "#{@success.errors.full_messages}"
      else
        redirect_to admin_chain_no_scan_task_path, :notice => "Succesfully Created"
      end

    end
  end

  def update_no_scan
    date = params["by_one_time_date"]
    min = params["by_one_time_time"]["(5i)"].to_i
    hour = params["by_one_time_time"]["(4i)"].to_i
    puts "hour = #{hour}"
    hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
    plus_day = 0

    if hour > 24
      hour = hour - 24
      plus_day = 1
    end
    date = date.to_time.change(:hour => hour, :min => min)
    date = date + plus_day.days

    @definition = @chain.no_scan_definition#NoScanDefinition.find(params[:no_scan_id])
    case @definition.schedule_type
      when 1
        @definition.zone = params["user"]["time_zone_one"]
      when 2
        @definition.zone = params["user"]["time_zone_daily"]
      when 3
        @definition.zone = params["user"]["time_zone_weekly"]
      when 4
        @definition.zone = params["user"]["time_zone_monthly"]

    end


    push_phone = params['no_scan_definition']["push_phone"] == "1"
    push_email = params['no_scan_definition']["push_email"] == "1"
    chain = @definition.chain rescue @chain
    desc = "NO SCAN---GENERATE JOB FROM ADMIN FOR CHAIN #{chain.name}"
    @definition.task_scheduler = NoScanScheduler.set_scheduler(params)

    if @definition.update_attributes(no_scan_params.merge!(:status => 1,
                                                                     :executed_at => date,
                                                                     :push_phone => push_phone,
                                                                     :push_email => push_email
                                     )
    ) == false
      render :action => :new
    else
      # @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,@definition.id)
      begin
        @success = NoScanJob.update_no_scan_task(chain,desc,nil,@definition.id, (@definition.no_scan_jobs.last.id rescue nil))
        @definition.no_scan_jobs.first.update_column(:status, 0) rescue nil

      rescue
        # @success = OwnerJob.admin_create_miss_you_task(chain,desc,nil,@definition.id)
        @success = NoScanJob.update_no_scan_task(chain,desc,nil,@definition.id, (@definition.no_scan_jobs.last.id rescue nil))
        @definition.no_scan_jobs.first.update_column(:status, 0) rescue nil
      end

      if @success.class != Hash and @success.errors.any?
        redirect_to :back, :notice => "#{@success.errors.full_messages}"
      else
        redirect_to admin_chain_no_scan_task_path, :notice => "Succesfully Updated"
      end
    end
  end

  private

  def find_chain
    @chain = Chain.find(params[:chain_id])
  end

  def no_scan_params
    params.require(:no_scan_definition).permit!
  end

  def task_definitions_params
    params.require(:task_definition).permit!
  end

end