class Admin::ReportDefinitionsController <  Admin::BaseController
  # GET /admin/report_definitions
  # GET /admin/report_definitions.json
  def index
    @admin_report_definitions = ReportDefinition.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @admin_report_definitions }
    end
  end

  # GET /admin/report_definitions/1
  # GET /admin/report_definitions/1.json
  def show
    @admin_report_definition = ReportDefinition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @admin_report_definition }
    end
  end

  def activate_deactivate
    @admin_report_definition = ReportDefinition.find(params[:id])
    status = @admin_report_definition.active

    if @admin_report_definition.update_attribute(:active,!status )
      redirect_to admin_report_definitions_path, notice: "Report definition updated."
    end
  end

  # GET /admin/report_definitions/new
  # GET /admin/report_definitions/new.json
  def new
    @admin_report_definition = ReportDefinition.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @admin_report_definition }
    end
  end

  # GET /admin/report_definitions/1/edit
  def edit
    @admin_report_definition = ReportDefinition.find(params[:id])
  end

  def edit_parameter
    @admin_report_definition = ReportDefinition.find(params[:id])
  end

  # POST /admin/report_definitions
  # POST /admin/report_definitions.json
  def create
    @admin_report_definition = ReportDefinition.new(params[:report_definition])
    res = {}
    unless params[:query].blank?
      params[:query].each_with_index do |qr, index|
         res.merge!(qr.to_sym => params[:data_type][index])
      end
    @admin_report_definition.query = res.to_json
    end

    respond_to do |format|
      if @admin_report_definition.save
        format.html { redirect_to admin_report_definitions_path, notice: 'Report definition was successfully created.' }
        format.json { render json: @admin_report_definition, status: :created, location: @admin_report_definition }
      else
        format.html { render action: "new" }
        format.json { render json: @admin_report_definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/report_definitions/1
  # PUT /admin/report_definitions/1.json
  def update
    @admin_report_definition = ReportDefinition.find(params[:id])

    res = {}
    unless params[:query].blank?
      params[:query].each_with_index do |qr, index|
        res.merge!(qr.to_sym => params[:data_type][index])
      end
      @admin_report_definition.query = res.to_json
    end

    respond_to do |format|
      if @admin_report_definition.update_attributes(params[:report_definition])
        format.html { redirect_to admin_report_definitions_path, notice: 'Report definition was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @admin_report_definition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/report_definitions/1
  # DELETE /admin/report_definitions/1.json
  def destroy
    @admin_report_definition = ReportDefinition.find(params[:id])
    @admin_report_definition.destroy

    respond_to do |format|
      format.html { redirect_to admin_report_definitions_path }
      format.json { head :ok }
    end
  end
end
