class Admin::ReceiptTransactionsController < Admin::BaseController

  before_action :get_user

   def index
  	@type = params[:type]
  	conditions = []
  	conditions << "receipts.user_id = #{@user.id}"
  	if @type.eql?("barcode")
  		conditions << "receipts.is_receipt_barcode IS TRUE" 
  	else
  		conditions << "receipts.is_receipt_barcode IS FALSE" 
  	end	
  	
  	conditions = conditions.join(" AND ")
    @receipt_transactions = ReceiptTransaction.includes(:receipt).where(conditions)
  end


  protected
  def get_user
    @user = User.find(params[:user_id])
  end
end
