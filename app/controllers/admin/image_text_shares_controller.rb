class Admin::ImageTextSharesController < Admin::BaseController

  before_action :find_chain

  def  index
    @chain_images = @chain.chain_image_shares.includes(:chain_image_share_locales)
  end

  def new
    @chain_image = @chain.chain_image_shares.new    
    @chain.locales.each do |locale|
      @chain_image.chain_image_share_locales.build(:locale_id => locale.id)
    end    
  end

  def create
    @chain_image = @chain.chain_image_shares.new(image_text_params)
    if @chain_image.save
      redirect_to admin_chain_image_text_shares_path(@chain), :notice => "Chain Image Share has created"
    else
      render :new
    end
  end

  def edit
    @chain_image = @chain.chain_image_shares.find(params[:id])        
    (@chain.locales - @chain_image.chain_image_share_locales.map{|gl| gl.locale}).each do |locale|
      @chain_image.chain_image_share_locales.build(:locale_id => locale.id)
    end
  end

  def update
    @chain_image = @chain.chain_image_shares.find(params[:id])
    
    if @chain_image.update(image_text_params)
      redirect_to admin_chain_image_text_shares_path(@chain), :notice => "Chain Image Share  has updated"
    else
      render :edit
    end
  end

  def destroy
    @chain_url = @chain.chain_image_shares.find(params[:id])
    @chain_url.destroy
    redirect_to admin_chain_image_text_shares_path(@chain), :notice => "Chain Image Share  has destroyed"
  end

  protected
  
  def find_chain
    @chain = Chain.find(params[:chain_id])
  end

  def image_text_params
    params.require(:chain_image_share).permit(:chain_image_share_locales_attributes, :title, :start_date, :expiry_date, :fb_title, :fb_description, :twitter_title, :twitter_description)
  end
end
