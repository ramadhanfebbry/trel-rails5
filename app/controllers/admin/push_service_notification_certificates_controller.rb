class Admin::PushServiceNotificationCertificatesController < Admin::BaseController

  def index
    unless params[:chain_id].blank?
      @notification_ertificates = PushServiceNotificationCertificate.where(chain_id: params[:chain_id]).paginate(:page => params[:page], :per_page => 20)
    else
      @notification_ertificates = PushServiceNotificationCertificate.paginate(:page => params[:page], :per_page => 20)
    end
  end

  def new
    @notification_ertificate = PushServiceNotificationCertificate.new
  end

  def create
    @notification_ertificate = PushServiceNotificationCertificate.new(params[:push_service_notification_certificate])
    @notification_ertificate.certificate = params[:push_service_notification_certificate][:certificate].read unless params[:push_service_notification_certificate][:certificate].blank?
    if @notification_ertificate.save
      flash[:notice] = "Your push notification certificate setting has been submitted"
      redirect_to :action => "index"
    else
      flash[:error] = "There was a problem submitting your setting."
      render :action => "new"
    end
  end

  def show
    @notification_ertificate = PushServiceNotificationCertificate.find(params[:id])
  end

  def edit
    @notification_ertificate = PushServiceNotificationCertificate.find(params[:id])
  end

  def update
    @notification_ertificate = PushServiceNotificationCertificate.find(params[:id])
    @notification_ertificate.chain_id = params[:push_service_notification_certificate][:chain_id]
    @notification_ertificate.environment = params[:push_service_notification_certificate][:environment]
    @notification_ertificate.certificate = params[:push_service_notification_certificate][:certificate].read unless params[:push_service_notification_certificate][:certificate].blank?
    if @notification_ertificate.save
      flash[:notice] = "Your push notification certificate setting has been updated"
      redirect_to :action => "index"
    else
      flash[:error] = "There was a problem submitting your setting."
      render :action => "edit"
    end
  end

  def destroy
    @notification_ertificate = PushServiceNotificationCertificate.find(params[:id])
    @notification_ertificate.destroy
    flash[:notice] = "Your push notification certificate setting has been deleted"
    redirect_to :action => "index"
  end

end
