class Admin::SessionsController < Devise::SessionsController
  layout 'admin'

  def after_sign_in_path_for(resource)
    if request.referer == new_admin_session_url
      admin_url
    else
      stored_location_for(resource) || request.referer || admin_url
    end

  end

  def after_sign_out_path_for(resource_name)
    new_admin_session_url
  end

end
