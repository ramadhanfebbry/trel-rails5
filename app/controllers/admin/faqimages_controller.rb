class Admin::FaqimagesController < Admin::BaseController

	before_action :find_chain
	# layout 'admin', :except => [:preview]

	def index
		@images = @chain.faqimages
	end

	def show
		@images = @chain.faqimages
	end

	def new
		@image = @chain.faqimages.build
	end

	def edit

	end

	def create
    @image = @chain.faqimages.build(params_faqimage)
    if @image.save
      redirect_to admin_chain_faqimages_path, :notice => "Images has been uploaded."
    else
      render :new
    end
  end

	def update

	end

	def destroy
    @image = @chain.faqimages.find(params[:id])
		@image.destroy
		redirect_to admin_chain_faqimages_path, :notice => "Images has been deleted."
	end

	private

	def find_chain
		@chain = Chain.find params[:chain_id]
	end

	def params_faqimage
		params.required(:faqimage).permit(:faqimage)
	end

end
