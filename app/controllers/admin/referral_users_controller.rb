class Admin::ReferralUsersController < Admin::BaseController
  before_action :find_ref_program_and_code
  
  def index
    @referral_users = @referral_code.referral_users.includes(:user)
  end

  private

  def find_ref_program_and_code
    @referral_program = ReferralProgram.find(params[:referral_program_id])
    @referral_code = ReferralCode.find(params[:referral_code_id])
  end
end
