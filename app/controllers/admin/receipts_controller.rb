class Admin::ReceiptsController < Admin::BaseController

  # GET /receipts
  def index
    rescue_from_cancan(:index, Receipt)
    #page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
    #@receipts = Receipt.active_chain.receipt_image.desc_list.paginate(page: page, per_page: Setting.pagination.per_page) #Receipt.all
    @receipts = []

    respond_to do |format|
      format.html
      format.js
      format.csv {
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsJob.new(
                params,
                current_admin,
                true, "image"),
            :delayable_type => "receipt_export_csv")
        redirect_to admin_receipts_path, :notice => "Receipt CSV will sent to your email shotrly"
      }
    end
  end

  def search
    q = params[:search][:q]
    q = q.gsub("'","''")
    params[:search][:q] = q

    ar_field = [
      "receipts.id",
      "offers.name",
      "users.email",
      "chains.name",
      "restaurants.name",
      "receipt_transactions.receipt_number",
      "receipts.created_at",
      "receipts.status"
    ]

    order = params[:qry_order]
    page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
    receipts_select = Receipt.select("id, status, created_at, user_id, chain_id")
    receipts = receipts_select.receipt_image.custom_search(params[:search])

    if  order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      if params[:search][:review_status].to_i.eql?(Receipt::STATUS[:RECEIVED]) || params[:search][:review_status].to_i.eql?(Receipt::STATUS[:OCRED])
        @receipts = receipts.order(qry_order).reverse_order.paginate(page: page , per_page: Setting.pagination.per_page)
      else
        @receipts = receipts.order(qry_order).reverse_order.paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => -1)
      end
      qry_order = qry_order + " desc"
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      if params[:search][:review_status].to_i.eql?(Receipt::STATUS[:RECEIVED]) || params[:search][:review_status].to_i.eql?(Receipt::STATUS[:OCRED])
        @receipts = receipts.order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page)
      else
        @receipts = receipts.order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => -1)
      end
    else
      qry_order = 'receipt_transactions.receipt_id desc'
      if params[:search][:review_status].to_i.eql?(Receipt::STATUS[:RECEIVED]) || params[:search][:review_status].to_i.eql?(Receipt::STATUS[:OCRED])
        @receipts = receipts.joins(:chain).order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page)
      else
        @receipts = receipts.joins(:chain).order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => -1)
      end
    end

    #count = Receipt.custom_search(params[:search]).joins(:chain).count(distinct: true)
    #@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page, total_entries: count)
    #@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page)
    @action = '-search'

    respond_to do |format|
      format.html {render :index}
      format.js { render :index }
      format.csv {
      #  @receipts = Receipt.custom_search(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order);render :layout => false, :action => :index
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsJob.new(
                params,
                current_admin,
                nil, "image"),
            :delayable_type => "receipt_export_csv")
        redirect_to admin_receipts_path, :notice => "Receipt CSV will sent to your email shotrly"
      }
    end
  end

  def review
    rescue_from_cancan(:index, Receipt)
    page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
    #@receipts = Receipt.active_chain.receipt_image.desc_list.paginate(page: page, per_page: Setting.pagination.per_page) #Receipt.all
    @receipts = []

    respond_to do |format|
      format.html
      format.js
      format.csv {
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsJob.new(
                params,
                current_admin,
                true, "image"),
            :delayable_type => "receipt_export_csv")
        redirect_to admin_receipts_path, :notice => "Receipt CSV will sent to your email shotrly"
      }
    end
  end

  def review_search
    q = params[:search][:q]
    q = q.gsub("'","''")
    params[:search][:q] = q

    ar_field = [
        "receipts.id",
        "offers.name",
        "users.email",
        "chains.name",
        "restaurants.name",
        "receipt_transactions.receipt_number",
        "receipts.created_at",
        "receipts.status"
    ]

    order = params[:qry_order]
    page = params['receipt_page'].blank? ? params[:page] : params['receipt_page']
    receipts = Receipt.select("id, status, created_at, user_id, chain_id, image_file_name, image_updated_at").receipt_image.custom_search_for_reviewer(params[:search])

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      if params[:search][:review_status].to_i.eql?(Receipt::STATUS[:RECEIVED]) || params[:search][:review_status].to_i.eql?(Receipt::STATUS[:OCRED])
        conditions = Receipt.build_conditions_search(params[:search]) + " AND receipts.is_receipt_barcode IS FALSE AND receipts.is_online_order is FALSE and receipts.is_olo is FALSE"
        total_entries = Receipt.select("id").where(conditions).count
        @receipts = receipts.order(qry_order).reverse_order.paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => total_entries)
      else
        @receipts = receipts.order(qry_order).reverse_order.paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => -1)
      end
      qry_order = qry_order + " desc"
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      if params[:search][:review_status].to_i.eql?(Receipt::STATUS[:RECEIVED]) || params[:search][:review_status].to_i.eql?(Receipt::STATUS[:OCRED])
        conditions = Receipt.build_conditions_search(params[:search]) + " AND receipts.is_receipt_barcode IS FALSE AND receipts.is_online_order is FALSE and receipts.is_olo is FALSE"
        total_entries = Receipt.select("id").where(conditions).count
        @receipts = receipts.order(qry_order).reverse_order.paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => total_entries)
      else
        @receipts = receipts.order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => -1)
      end
    else
      qry_order = 'receipt_transactions.receipt_id desc'
      if params[:search][:review_status].to_i.eql?(Receipt::STATUS[:RECEIVED]) || params[:search][:review_status].to_i.eql?(Receipt::STATUS[:OCRED])
        conditions = Receipt.build_conditions_search(params[:search]) + " AND receipts.is_receipt_barcode IS FALSE AND receipts.is_online_order is FALSE and receipts.is_olo is FALSE"
        total_entries = Receipt.select("id").where(conditions).count
        @receipts = receipts.order(qry_order).reverse_order.paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => total_entries)
      else
        @receipts = receipts.joins(:chain).order(qry_order).paginate(page: page , per_page: Setting.pagination.per_page, :total_entries => -1)
      end
    end

    #count = Receipt.custom_search(params[:search]).joins(:chain).count(distinct: true)
    #@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page, total_entries: count)
    #@receipts = @receipts.paginate(page: page , per_page: Setting.pagination.per_page)
    @action = '-search'

    respond_to do |format|
      format.html {render :index_reviewer}
      format.js { render :index_reviewer }
      format.csv {
        #  @receipts = Receipt.custom_search(params[:search]).where("chains.status = 'active'").joins(:chain).order(qry_order);render :layout => false, :action => :index
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsJob.new(
                params,
                current_admin,
                nil, "image"),
            :delayable_type => "receipt_export_csv")
        redirect_to admin_receipts_path, :notice => "Receipt CSV will sent to your email shotrly"
      }
    end
  end

  # GET /receipts/1
  def show
    rescue_from_cancan(:show, Receipt)
    @receipt = Receipt.receipt_image.active_chain.find(params[:id])
    @receipt_transactions = @receipt.receipt_transactions.order('id desc')
  end

  # GET /receipts/new
  def new
    rescue_from_cancan(:new, Receipt)
    @receipt = Receipt.new
    @receipt_transaction = ReceiptTransaction.new
  end

  # GET /receipts/1/edit
  def edit
    rescue_from_cancan(:edit, Receipt)
    @receipt = Receipt.receipt_image.active_chain.find(params[:id])
    @receipt_transaction = @receipt.last_transaction
    @receipt_transaction_detail = @receipt_transaction.receipt_transaction_detail
  end

  # POST /receipts
  def create
    rescue_from_cancan(:create, Receipt)
    application_key = ApplicationKey.where(:appkey => params[:appkey]).first rescue nil
    params[:receipt][:status]= application_key && application_key.ocr_send_message ? 5 : 1
    params[:receipt][:user_id] = params[:user]
    @receipt = Receipt.new(params[:receipt])
    @receipt_transaction = ReceiptTransaction.new()

    restaurant_offer = RestaurantOffer.find_by_offer_id_and_restaurant_id(params[:offer], params[:restaurant])

    offer = restaurant_offer.offer rescue nil
    @receipt.chain_id = offer.chain_id rescue nil
    @receipt_transaction.restaurant_offer = restaurant_offer
    @receipt_transaction.status = params[:receipt][:status]
    #    @receipt_transaction.receipt = @receipt
    @receipt.receipt_transactions << @receipt_transaction


    #@receipt_transaction = @receipt.receipt_transactions.create(params[:receipt_transaction])

    respond_to do |format|
      if @receipt.save
        Delayed::Job.enqueue(
          ReceiptProcess::ReceiptAdminImageProcess.new(@receipt,params)
        )
      # move to delayed job
      # if @receipt.save# and @receipt_transaction.save
      #   unless params[:restaurant].blank?
      #     ru = RestaurantUser.new(:user_id => params[:receipt][:user_id], :restaurant_id => params[:restaurant])
      #     ru.save
      #     EmailMarketing.update_email_marketing_location((User.find(params[:receipt][:user_id]) rescue nil), params[:restaurant])
      #     Chain.update_user_fishbowl_location((User.find(params[:receipt][:user_id]) rescue nil), params[:restaurant])
      #   end
      #   @receipt.push_referral_code_reward ## push referral reward
      #   if @receipt.status == Receipt::STATUS[:APPROVED]
      #     @receipt.push_one_time_reward(@receipt.user)
      #   end
      #   @receipt.sqs_send_message(params[:appkey])
        format.html { redirect_to admin_receipt_url(@receipt), notice: 'Receipt was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /receipts/1
  def update
    p params
    rescue_from_cancan(:update, Receipt)
    @receipt = Receipt.active_chain.find(params[:id])
    @receipt.attributes = params[:receipt]
    @old_transaction = @receipt.last_transaction
    @offer = @old_transaction.offer rescue nil

    @receipt_transaction = ReceiptTransaction.new(@old_transaction.attributes.merge(params[:receipt_transaction]))
    @receipt_transaction.reviewer=current_admin
    @receipt_transaction.status = params[:receipt][:status]

    @receipt_transaction.receipt = @receipt
    if params[:receipt_transaction] && !params[:receipt_transaction][:restaurant_id].blank? && @old_transaction && !@old_transaction.restaurant_id.eql?(params[:receipt_transaction][:restaurant_id].to_i)
      restaurant_offer = RestaurantOffer.where(:offer_id => @offer.id, :restaurant_id => params[:receipt_transaction][:restaurant_id]).first rescue nil
      @receipt_transaction.restaurant_offer_id = restaurant_offer.id if restaurant_offer
    else
      @receipt_transaction.restaurant_offer = @old_transaction.restaurant_offer
    end
    @receipt_transaction.reject_reason_id = 0 if @receipt.status!=4
    @receipt_transaction.instructions = {:rejected => "Reject Receipt"} if @receipt.status.to_i.eql?(4)
    @receipt_transaction.created_at = nil
    @receipt_transaction.updated_at = nil
    @receipt_transaction.admin_id = 1
    #@receipt_transaction.tax = params[:tax]
    #@receipt_transaction.cashier = params[:cashier]
    #@receipt_transaction.guests= params[:guests]
    #@receipt_transaction.total_payment = params[:total_payment]
    #@receipt_transaction.payment_method = params[:payment_method]
    #@receipt_transaction.check_number= params[:check_number]
    #@receipt_transaction.issue_date = params[:issue_date]
    @receipt_transaction.reviewer_validation = true
    @receipt_transaction.updated_manually = true
    @uniq_receipt = true
    @today_maxed = false
    @valid_max_subtotal = true
    @valid_min_subtotal = true
    @maximum_offer_points_earned_per_day_limit_crossed = false
    @future_check = false
    if @receipt.status.to_i.eql?(Receipt::STATUS[:APPROVED])
      if @receipt.chain.max_subtotal > 0 && @receipt.chain.max_subtotal < @receipt_transaction.subtotal.to_f && !params[:ignore_max_subtotal].eql?("true")
        @valid_max_subtotal = false
      end
      if @receipt.chain.id.eql?(Setting.chain_id.zoes)
        receipt_number = @receipt_transaction.receipt_number.gsub(/\D/, '').to_i
        #receipt_transactions = ReceiptTransaction.where("receipt_id != ? AND DATE(issue_date) = ? AND restaurant_id = ? AND receipt_number ILIKE ? AND status = ?", @receipt.id, @receipt_transaction.issue_date.to_date, @receipt_transaction.restaurant_id, "%#{receipt_number}", Receipt::STATUS[:APPROVED]) rescue []
        receipt_transactions = ReceiptTransaction.where("receipt_id != ? AND DATE(issue_date) = ? AND restaurant_id = ? AND receipt_number = ? AND status = ?", @receipt.id, @receipt_transaction.issue_date.to_date, @receipt_transaction.restaurant_id, "#{receipt_number}", Receipt::STATUS[:APPROVED]) rescue []
      else
        receipt_number = @receipt_transaction.receipt_number
        receipt_transactions = ReceiptTransaction.where("receipt_id != ? AND DATE(issue_date) = ? AND restaurant_id = ? AND receipt_number = ? AND status = ?", @receipt.id, @receipt_transaction.issue_date.to_date, @receipt_transaction.restaurant_id, receipt_number, Receipt::STATUS[:APPROVED]) rescue []
      end
      @uniq_receipt = false unless receipt_transactions.blank?
      @selected_receipts = receipt_transactions.map{|rt| rt.receipt}
    end
    if @receipt.status.to_i.eql?(Receipt::STATUS[:APPROVED]) && @uniq_receipt && @receipt.chain.today_max_user_limit?(@receipt, params[:receipt_transaction][:issue_date])
      @today_maxed = true
    end

    if @receipt.status.to_i.eql?(Receipt::STATUS[:APPROVED]) && !@receipt.chain.available_offer_points_earned_per_day?(@receipt, @receipt_transaction, params[:receipt_transaction][:total_points_earned], params[:receipt_transaction][:issue_date], @offer) && !params[:ignore_offer_per_day].eql?("true")
      @maximum_offer_points_earned_per_day_limit_crossed = true
    end

    if @receipt.is_future_check?(@receipt_transaction.issue_date)
      @future_check = true
    end

    respond_to do |format|
      if @uniq_receipt && !@today_maxed && @valid_max_subtotal && @valid_min_subtotal && !@maximum_offer_points_earned_per_day_limit_crossed
        if @receipt_transaction.save and @receipt.update_attributes(params[:receipt])
          ReceiptProcess::ReceiptSetIssuedate.setdatetime(@receipt.id, @receipt_transaction.issue_date)
          # offer rules
          if @receipt_transaction.total_points_earned.to_f == 0.0
            begin
              @receipt_transaction.status = Receipt::STATUS[:REJECTED]
              @receipt_transaction.receipt.status = Receipt::STATUS[:REJECTED]
              @receipt_transaction.instructions = {:offer_rules_not_met => "This receipt does not met any rules"}
              @receipt_transaction.receipt.save
            rescue
            end
          end

          @is_save = true
          RestaurantUser.create(:user_id => @receipt.user_id, :restaurant_id => @receipt_transaction.restaurant_id) unless @receipt_transaction.restaurant_id.blank?
        else
          @is_save = false
          #format.html { render action: "edit" }
        end
      end
      format.js
    end
  end

  def reject_reasons
    @id = params[:id]
    @opt = RejectReason.select("reject_reasons.id, reject_reasons.description") #find_all_by_chain_id(@id)

    render :json => @opt
  end

  # DELETE /receipts/1
  def destroy
    rescue_from_cancan(:destroy, Receipt)
    @receipt = Receipt.active_chain.receipt_image.find(params[:id])
    @receipt.destroy

    respond_to do |format|
      format.html { redirect_to admin_receipts_url }
    end
  end

  # def check_custom_image_uploader
  #   require 'rubygems'
  #   require 'aws/s3'
  #   image_url = "http://3.bp.blogspot.com/_3I6eIowAe7I/TH8IfL8BHNI/AAAAAAAAAz0/ueyBcoDyU_0/s1600/krishna-christ.jpg"
  #   image_dimention = ["1250","200"]
  #   process_flag = 1
  #   @returning_data = image_processing image_url, image_dimention, process_flag
  #   render :text => @returning_data and return
  #   #render :text => "Looking ahead to see what can be done" and return
  # end

  def url
    @receipt = Receipt.find(params[:id])
    redirect_to @receipt.image.url
  end

  def blocked
    @receipt = Receipt.find(params[:id])
  end

  def goto
    @receipt = Receipt.find(params[:id]) rescue nil
  end

  def export_status
    #@receipts = Receipt.all
    respond_to do |format|
      format.csv {
        Delayed::Job.enqueue(
            ExportCsvJob::ReceiptsStatusJob.new(
                params,
                current_admin,
                false, "image"),
            :delayable_type => "receipt_export_status_csv")
        redirect_to admin_receipts_path
      }
    end
  end

  def search_users
    @var_query = {email: "#{params[:email]}%", chain_id: params[:chain_id]}
    query = "email LIKE :email AND chain_id = :chain_id"
    if !params[:chain_app_key].blank?
      query += " AND chain_app_key = :chain_app_key"
      @var_query[:chain_app_key] = params[:chain_app_key]
    end
    @users = User.where(query, @var_query).
    paginate(page: params[:page], per_page: 2)
  end

end
