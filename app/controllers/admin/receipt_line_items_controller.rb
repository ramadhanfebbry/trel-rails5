class Admin::ReceiptLineItemsController < Admin::BaseController

  def new
    @search_menu_item = ReceiptMenuSearch.new(nil)
  end

  def create
    @search_menu_item = ReceiptMenuSearch.new(params[:receipt_menu_search])
    if @search_menu_item.valid?
      Delayed::Job.enqueue(ReceiptMenuItemJob.new(current_admin.email, @search_menu_item.chain_id, @search_menu_item.start_date, @search_menu_item.end_date, @search_menu_item.keyword))
      redirect_to new_admin_receipt_line_item_path, :notice => "Your query will be processed shortly and will be notified by email."
    else
      render "new"
    end
  end

end
