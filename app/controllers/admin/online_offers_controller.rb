class Admin::OnlineOffersController < Admin::BaseController

  # GET /offers
  def index
    rescue_from_cancan(:index, Offer)
    @offers = Offer.unscoped.online_offers.active_chain.paginate(page: params[:page], per_page: 10) #Offer.all

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /offers/1
  def show
    rescue_from_cancan(:show, Offer)
    @offer = Offer.unscoped.online_offers.active_chain.find(params[:id])
    @chain = @offer.chain
    @participants = @offer.restaurants
    ids = @participants.collect(&:id)
    @non_participants = Restaurant.where('chain_id = ? AND NOT id in (?)', @chain.id, ids.empty? ? '-1' : ids)
  end

  # GET /offers/new
  def new
    rescue_from_cancan(:new, Offer)
    @offer = Offer.new
  end

  # GET /offer/1/edit
  def edit
    rescue_from_cancan(:edit, Offer)
    @offer = Offer.unscoped.online_offers.active_chain.find(params[:id])
  end

  # POST /offers
  def create
    rescue_from_cancan(:create, Offer)
    @offer = Offer.new(params[:online_offer])
    params[:online_offer][:effectiveDate] = format_date(params[:online_offer][:effectiveDate]) if !params[:online_offer][:effectiveDate].blank?
    params[:online_offer][:expiryDate] = format_date(params[:online_offer][:expiryDate]) if !params[:online_offer][:expiryDate].blank?

    respond_to do |format|
      if @offer.save
        #@offer.update_participation(params[:restaurants], params[:ids])
        format.html { redirect_to admin_online_offer_path(@offer), notice: 'Offer was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end


  # PUT /offers/1
  def update
    rescue_from_cancan(:update, Offer)
    @offer = Offer.unscoped.online_offers.active_chain.find(params[:id])
    params[:online_offer][:effectiveDate] = format_date(params[:online_offer][:effectiveDate]) if !params[:online_offer][:effectiveDate].blank?
    params[:online_offer][:expiryDate] = format_date(params[:online_offer][:expiryDate]) if !params[:online_offer][:expiryDate].blank?

    respond_to do |format|
      if @offer.update_attributes(params[:online_offer])
        format.html { redirect_to admin_online_offer_path(@offer), notice: 'Offer was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /offers/1
  def destroy
    rescue_from_cancan(:destroy, Offer)
    @offer= Offer.unscoped.online_offers.active_chain.find(params[:id])

    if @offer.restaurant_offers.count == 0
      @offer.destroy
    end

    redirect_to admin_online_offers_url
  end

  def update_participation
    rescue_from_cancan(:update_participation, Offer)
    @offer= Offer.unscoped.online_offers.active_chain.find(params[:online_offer_id])
    submit= params[:commit]

    case submit
      when "Pull Out"
        @offer.pullout(params[:pullout])
      when "Participate"
        @offer.participate(params[:participate])
      else
    end

    redirect_to admin_online_offer_path(@offer)
  end

  def chain_offers
    #    list_offer = [Offer.new(:name => "Please select")]
    list_offer = [:id => '', :name => "Please select"]
    chain = Chain.find params[:id]
    @offers = chain.unscoped.online_offers
    list_offer = list_offer  + @offers
    render :json => list_offer
  end

  def group_chain_list
    sth = params[:search_text_hidden]
    ar_field = [
        "offers.id",
        "chains.name",
        "offers.name",
        '"effectiveDate"',
        '"expiryDate"',
        '"timeStart"',
        "offers.multiplier",
        "offers.bonus_points",
        "offers.bonus_points_ftu",
        "surveys.title"
    ]

    order = params[:qry_order]

    if params[:chn_id] != ""
      qry_condition = "chains.status = 'active' AND chains.id = '#{params[:chn_id]}' AND lower(offers.name) like '%#{sth.downcase}%'"
    else
      qry_condition = "chains.status = 'active' AND lower(offers.name) like '%#{sth.downcase}%'"
    end

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @offers = Offer.unscoped.online_offers.includes(:chain, :survey).joins(:chain).where(qry_condition).order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @offers = Offer.unscoped.online_offers.includes(:chain, :survey).joins(:chain).where(qry_condition).order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    else
      @offers = Offer.unscoped.online_offers.includes(:chain, :survey).joins(:chain).where(qry_condition).paginate(page: params[:page], per_page: 10)
    end
  end

  def getOfferRestaurants
    @id = params[:id]
    if @id != "null"
      @opt = RestaurantOffer.select('restaurants.id, restaurants.name').joins("inner join restaurants On restaurant_online_offers.restaurant_id=restaurants.id").where("restaurant_online_offers.online_offer_id = ?", @id) #find_all_by_chain_id(@id)
      render :json => @opt
    else
      render :nothing => true
    end
  end
end
