class Admin::ChainSettingAlertsController < Admin::BaseController
  # GET /chain_setting_alerts
  # GET /chain_setting_alerts.json
  before_action :set_chain
  def index
    @chain_setting_alerts = @chain.chain_setting_alert ? [@chain.chain_setting_alert] : []
    flag_string = "chain_setting_alert_#{@chain_setting_alerts.first.id}" rescue nil
    @receipt_status =JSON.parse REDIS.get(flag_string + "receipt_status") rescue nil
    @online_order_type = JSON.parse REDIS.get(flag_string + "online_order_type") rescue nil
    @mp = (REDIS.get(flag_string + "mp").to_s == "1" ? true : false) rescue nil
    @is_replicate = (REDIS.get(flag_string + "is_replicate").to_s == "1" ? true : false) rescue nil

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @chain_setting_alerts }
    end
  end

  def test_no_scan_alert_chain
    @chain = Chain.find params[:chain_id]
    @chain_setting_alert = ChainSettingAlert.find(params[:id])

    Delayed::Job.enqueue(NoScanAlert.new(@chain_setting_alert.chain_id))

    redirect_to admin_chain_chain_setting_alert_path(@chain, @chain_setting_alert), :notice => "It will sent to your email shortly"
  end

  # GET /chain_setting_alerts/1
  # GET /chain_setting_alerts/1.json
  def show
    @chain_setting_alert = ChainSettingAlert.find(params[:id])
    flag_string = "chain_setting_alert_#{@chain_setting_alert.id}"
    @receipt_status =JSON.parse REDIS.get(flag_string + "receipt_status") rescue nil
    @online_order_type = JSON.parse REDIS.get(flag_string + "online_order_type") rescue nil
    @mp = REDIS.get(flag_string + "mp").to_s == "1" ? true : false
    @is_replicate = REDIS.get(flag_string + "is_replicate").to_s == "1" ? true : false
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @chain_setting_alert }
    end
  end

  # GET /chain_setting_alerts/new
  # GET /chain_setting_alerts/new.json
  def new
    @chain_setting_alert = ChainSettingAlert.where(:chain_id => @chain.id).first

    redirect_to admin_chain_chain_setting_alert_path(@chain.id) unless @chain_setting_alert.blank?
    @chain_setting_alert = ChainSettingAlert.new(chain_id: @chain.id)
    if @chain_setting_alert.new_record?
      @chain_setting_alert.is_active = true 
      @chain_setting_alert.send_time = "06:00AM"
    end
    
    #redirect_to admin_chain_chain_setting_alert_path(@chain.id) unless @chain_setting_alert.blank?

    @receipt_status = ["2","1"]
    @online_order_type = ["2","1","3"]
    @mp = true
    @is_replicate = true
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @chain_setting_alert }
    end
  end

  # GET /chain_setting_alerts/1/edit
  def edit
    @chain_setting_alert = ChainSettingAlert.find(params[:id])
    flag_string = "chain_setting_alert_#{@chain_setting_alert.id}"
    @receipt_status =JSON.parse REDIS.get(flag_string + "receipt_status") rescue nil
    @online_order_type = JSON.parse REDIS.get(flag_string + "online_order_type") rescue nil
    @mp = REDIS.get(flag_string + "mp").to_s == "1" ? true : false
    @is_replicate = REDIS.get(flag_string + "is_replicate").to_s == "1" ? true : false
  end

  # POST /chain_setting_alerts
  # POST /chain_setting_alerts.json
  def create
    @chain_setting_alert = ChainSettingAlert.new(params_chain_setting_alert)

    respond_to do |format|
      if @chain_setting_alert.save
        flag_string = "chain_setting_alert_#{@chain_setting_alert.id}"
        REDIS.set(flag_string + "receipt_status", params["receipt_status"].to_s)
        REDIS.set(flag_string + "online_order_type", params["online_order_type"].to_s)
        REDIS.set(flag_string + "mp", params["mp"])
        REDIS.set(flag_string + "is_replicate", params["is_replicate"])

        format.html { redirect_to admin_chain_chain_setting_alerts_path(@chain), notice: 'Chain setting alert was successfully created.' }
        format.json { render json: @chain_setting_alert, status: :created, location: @chain_setting_alert }
      else
        format.html { render action: "new" }
        format.json { render json: @chain_setting_alert.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /chain_setting_alerts/1
  # PUT /chain_setting_alerts/1.json
  def update
    @chain_setting_alert = ChainSettingAlert.find(params[:id])

    respond_to do |format|
      if @chain_setting_alert.update_attributes(params[:chain_setting_alert])
        flag_string = "chain_setting_alert_#{@chain_setting_alert.id}"
        REDIS.set(flag_string + "receipt_status", params["receipt_status"].to_s)
        REDIS.set(flag_string + "online_order_type", params["online_order_type"].to_s)
        REDIS.set(flag_string + "mp", params["mp"])
        REDIS.set(flag_string + "is_replicate", params["is_replicate"])
        format.html { redirect_to admin_chain_chain_setting_alerts_path(@chain), notice: 'Chain setting alert was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @chain_setting_alert.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chain_setting_alerts/1
  # DELETE /chain_setting_alerts/1.json
  def destroy
    @chain_setting_alert = ChainSettingAlert.find(params[:id])
    @chain_setting_alert.destroy

    respond_to do |format|
      format.html { redirect_to admin_chain_chain_setting_alerts_path(@chain) }
      format.json { head :no_content }
    end
  end

  private

  def set_chain
    @chain = Chain.find(params[:chain_id])
  end

  def params_chain_setting_alert
    params.required(:chain_setting_alert).permit(:chain_id, :email, :is_active, :send_time, :send_zone, :email_subject)
  end
end
