class Admin::RewardsController < Admin::BaseController

  # GET /rewards
  before_action :send_message_on_sqs, :only => :index
  before_action :find_event, :only => [:assign_reward, :remove_reward]

  def index
    rescue_from_cancan(:index, Reward)
    # clear_filter
    unless params["chain_id"].blank?
      @chain = Chain.find(params["chain_id"])
      @rewards = @chain.rewards.where("reward_type != #{Reward::TYPES["PARTNER"]}").includes(:chain).order("updated_at DESC").paginate(page: params[:page], per_page: Setting.pagination.per_page)
      @reward_type = 1
      @reward = Reward.new
    else
      @rewards = []
      @reward = Reward.new
      @chain = nil
    end
  end

  def new_birthday_reward
    rescue_from_cancan(:new_birhday_reward, Reward)
    @reward = Reward.new
    @chain = Chain.find(params["chain_id"])
    @reward.chain = @chain
    #@chain.locales.each do |locale|
    #  @reward.notification_locales.build(:locale => locale)
    #end
    render :layout => false
  end

  def new_anniversary_reward
    rescue_from_cancan(:new_anniversary_reward, Reward)
    @reward = Reward.new
    @chain = Chain.find(params["chain_id"])
    @reward.chain = @chain
    render :layout => false
  end

  def search_users
    rescue_from_cancan(:index, Reward)
    restaurant_id = params["restaurant_id"]
    restaurants = []
    state = params["strState"]
    session[:restaurant_ids] = session[:restaurant_ids].nil? ? [] : session[:restaurant_ids]

    session[:restaurant_ids] << restaurant_id.to_i unless restaurant_id.blank?
    session[:restaurant_ids].delete(restaurant_id.to_i) if state == "0"
    begin
      restaurants = Restaurant.by_ids(session[:restaurant_ids].compact)|| [] unless session[:restaurant_ids].blank?
    rescue
    end
    @users = []
    restaurants.each{|restaurant| @users << restaurant.list_users} if restaurants.size > 0
    @users = @users.flatten.uniq! unless @users.blank?
  end

  # GET /rewards/1
  def show
    rescue_from_cancan(:show, Reward)
    @reward = Reward.find(params[:id])
    @chain = @reward.chain
    @participants = @reward.restaurants.includes(:city)
    ids = @participants.collect(&:id)
    @non_participants = Restaurant.includes(:city).where('chain_id = ? AND NOT id in (?)', @chain.id, ids.empty? ? '-111' : ids)
  end

  def preview_xls_data
    @email_from_xls = []
    test_file = params[:xls]
    manual_user_ids = params[:manual_user_ids].split(",")
    @notice = "Preview"
    unless test_file.blank?
      begin
        book = Spreadsheet.open "#{test_file.path}"
        sheet1 = book.worksheet 0
        sheet1.column(0).each { |row|     @email_from_xls << row.to_i }
        @email_from_xls = @email_from_xls.compact.uniq
      rescue
        @error = "Please upload the xls file"
      end
    end
    @email_from_xls << manual_user_ids
    @email_from_xls = @email_from_xls.flatten
    @email_from_xls = @email_from_xls.uniq
    respond_to do |format|
      format.html do
        render :layout => false
      end
      format.js
    end
  end

  # GET /rewards/new
  def new
    rescue_from_cancan(:new, Reward)
    @reward = Reward.new
    @chain = Chain.find(params["chain_id"])
    respond_to do |format|
      format.js { }
      format.html {render :layout => false}
    end
  end

  def search_rewards
    @chain = Chain.find(params["chain_id"])
    @effective_date = params["search"]["from"]
    @expiry_date = params["search"]["to"]
    @search_query = params["search"]["q"]
    @reward_select_type = params["search"]["reward_type"]

    ar_field = [
      "rewards.updated_at",
      "rewards.id",
      "chains.name",
      "rewards.name",
      '"effectiveDate"',
      '"expiryDate"',
      "rewards.points",
      "rewards.reward_type"
    ]

    order = params[:qry_order]
    @order = order.to_i

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @rewards = @chain.rewards.includes(:chain).order(qry_order).reverse_order.search(params[:search]).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @rewards = @chain.rewards.includes(:chain).order(qry_order).search(params[:search]).paginate(page: params[:page], per_page: 10)
    end

#    @rewards = @chain.rewards.includes(:chain).order("updated_at DESC").search(params[:search]).paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.js { render :index}
      format.html {render :layout => false}
    end
  end

  def new_regular_reward
    rescue_from_cancan(:new, Reward)
    @reward = Reward.new
    @chain = Chain.find(params["chain_id"])
    #    respond_to do |format|
    #      format.js { }
    #      format.html {render :layout => false}
    #    end
    render :layout => false
  end

  def new_dashboard_reward
    rescue_from_cancan(:new, Reward)
    @reward = Reward.new
    @chain = Chain.find(params["chain_id"])
    #    respond_to do |format|
    #      format.js { }
    #      format.html {render :layout => false}
    #    end
    render :layout => false
  end

  def new_push_reward
    rescue_from_cancan(:new_push_reward, Reward)
    @reward = Reward.new
    @chain = Chain.find(params["chain_id"])
    @reward.chain = @chain
    #@chain.locales.each do |locale|
    #  @reward.notification_locales.build(:locale => locale)
    #end
    render :layout => false
  end

  def new_promo_reward
    rescue_from_cancan(:new_push_reward, Reward)
    @reward = Reward.new
    @chain = Chain.find(params["chain_id"])
    render :layout => false
  end

  def new_gifting_reward
    @reward = Reward.new
    @reward.promotions.build
    @chain = Chain.find(params["chain_id"])
    @chain.locales.each do |locale|
      @reward.notification_locales.build(:locale => locale)
    end
    render :layout => false
  end

  def assign_reward
    @assign = true
    @reward = Reward.find(@reward_id)
    @chain = @reward.chain
    chainRewardEvent= ChainRewardEvent.new(:chain_id=>@chain.id, :event=>@event_id,:reward_id => @reward_id)
    chainRewardEvent.save
  end

  def remove_reward
    @assign = false
    @reward = Reward.find(@reward_id)
    c = ChainRewardEvent.find_by_reward_id(@reward_id)
    c.destroy
    respond_to do |format|
      format.js { render :action => "assign_reward"}
    end
  end

  def collection_list
    if request.format.js?
      @chain = Chain.find(params[:chain_id])
      @reward = Reward.new
      @event_id = params[:id]
      if request.format.js?
        @reward_ids = @chain.chain_reward_events.map(&:reward_id)
        @rewards = @chain.rewards.one_time.includes(:chain,:chain_reward_event).paginate(page: params[:page], per_page: 5)
      end
      render :layout => false
    else
      render :layout => false
    end
  end

  # GET /reward/1/edit
  def edit
    rescue_from_cancan(:edit, Reward)
    if request.format.js?
      @reward = Reward.find(params[:id])
      @chain = @reward.chain
      if @reward.reward_type == Reward::TYPES["PUSH_REWARD"]
        @restaurant_ids = @reward.restaurants.map(&:id)
      end
    end
    render :layout => false
  end

  # POST /rewards
  def create

    rescue_from_cancan(:create, Reward)
    @reward = Reward.new(params[:reward])
    ## if its from push rewards form /creating new reward bo push on select box/
    @is_from_push_reward = params['from_push_reward']
    respond_to do |format|
      if @reward.save and @reward.activate!
        @chain = Chain.find(@reward.chain_id)
        @reward.saving_images(@reward.id) unless params['reward']['name'].blank?
        @reward.saving_images_thumbnail(@reward.id) unless params['reward']['name'].blank?
        restaurant_ids = params["restaurant_ids"]

        unless restaurant_ids.blank?
          @reward.participate(restaurant_ids)
        end

        @rewards = @chain.rewards.includes(:chain).order("updated_at DESC").paginate(page: params[:page], per_page: Setting.pagination.per_page)
        case @reward.reward_type
        when Reward::TYPES["ONE_TIME"]
          @data_reward = @reward
          @reward.participate(@chain.restaurants.map(&:id))
          @event_id = params["event_id"] || nil
          format.js { render :action => "create_one_time" }
        when Reward::TYPES["PUSH_REWARD"] , params["users"]
          #          @reward.save_notifications(@chain, params["notifications"])
          #@reward.send_schedule_reward(params["restaurant_ids"])
          @reward.participate(@chain.restaurants.map(&:id))
          @saved_reward = @reward
          session[:restaurant_ids] = nil
          format.js {}
        when Reward::TYPES["BIRTHDAY"]
          @reward.participate(@chain.restaurants.map(&:id))
          @reward.check_uniq_birthday_reward
          format.js {}
        else
          p "asdf #{@reward.errors}"
          @reward.participate(@chain.restaurants.map(&:id))
          format.js {}
        end

        @reward = Reward.new
        format.html { redirect_to admin_reward_path(@reward), notice: 'Reward was successfully created.' }
      else
        p "asdf #{@reward.errors.full_messages}"
        format.js {}
        format.html { render action: "new" }
      end
    end
  end

  def upload_file_reward
    @url_s3 = nil
    @name =  params['file'].original_filename
    @id   = params['id']
    @id = nil if @id.blank?
    directory = "#{Rails.root}/tmp/"
    path = File.join(directory, @name.gsub('\n',''))
    File.open(path, "wb") { |f| f.write(params['file'].read) }
    puts "params file #{params['file'].content_type}"
    if params['file'].content_type.to_s.include?("jpg") or params['file'].content_type.to_s.include?("jpeg") or
        params['file'].content_type.to_s.include?("png")
      @url_s3 = nil
    else
      @url_s3 = "Wrong File extension"
    end

    if @url_s3.blank?
      aw,bucket = Reward.initialize_aws
      #aw.buckets[bucket].objects["#{@id}_rewards_temporary"].delete
      aw.buckets[bucket].objects["#{@id}_rewards_temporary"].write(:file => path)
      @url_s3 = aw.buckets[bucket].objects["#{@id}_rewards_temporary"].url_for(:read).to_s

    end
    File.delete(path)

    puts @url_s3
    #render :text => @url_s3
  end

  def upload_file_thumbnail_reward
    @url_s3 = nil
    @name =  params['file'].original_filename
    @id   = params['id']
    @id = nil if @id.blank?
    directory = "#{Rails.root}/tmp/"
    path = File.join(directory, @name.gsub('\n',''))
    File.open(path, "wb") { |f| f.write(params['file'].read) }
    puts "params file #{params['file'].content_type}"
    if params['file'].content_type.to_s.include?("jpg") or params['file'].content_type.to_s.include?("jpeg") or
        params['file'].content_type.to_s.include?("png")
      @url_s3 = nil
    else
      @url_s3 = "Wrong File extension"
    end

    if @url_s3.blank?
      aw,bucket = Reward.initialize_aws
      #aw.buckets[bucket].objects["#{@id}_rewards_temporary"].delete
      aw.buckets[bucket].objects["#{@id}_rewards_thumbnail_temporary"].write(:file => path)
      @url_s3 = aw.buckets[bucket].objects["#{@id}_rewards_thumbnail_temporary"].url_for(:read).to_s

    end
    File.delete(path)

    puts @url_s3
    #render :text => @url_s3
  end

  def get_uploaded_file
    @id   = params['id']
    aw,bucket = Reward.initialize_aws
    @url_s3 = aw.buckets[bucket].objects["#{@id}_rewards_temporary"].url_for(:read).to_s
  end

  def get_uploaded_file_thumbnail
    @id   = params['id']
    aw,bucket = Reward.initialize_aws
    @url_s3 = aw.buckets[bucket].objects["#{@id}_rewards_thumbnail_temporary"].url_for(:read).to_s
  end

  # PUT /rewards/1
  def update
    rescue_from_cancan(:update, Reward)
    @reward = Reward.find(params[:id])
    params[:reward][:reward_type] = @reward.reward_type

    respond_to do |format|
      if @reward.update_attributes(params[:reward])
        @reward.saving_images(params['id']) unless params['id'].blank?
        @reward.saving_images_thumbnail(params['id']) unless params['id'].blank?
        @chain = Chain.find(@reward.chain_id)
        @rewards = @chain.rewards.includes(:chain).order("updated_at DESC").paginate(page: params[:page], per_page: Setting.pagination.per_page)
        format.js {}

        if params[:reward][:reward_type].to_i == Reward::TYPES["PUSH_REWARD"] or params["users"]
          #@reward.reward_wallets.destroy_all
          #@reward.send_schedule_reward(params["restaurant_ids"])
          @reward.participate(@chain.restaurants.map(&:id))
          session[:restaurant_ids] = nil
        end
        #format.html { redirect_to admin_reward_path(@reward), notice: 'Reward was successfully updated.' }
      else
        format.js {"create"}
        #format.html { render action: "edit" }
      end
    end
  end

  def search
    p params
    # clear_filter
    session[:restaurant_ids] = []
    @rewards = []
    @reward = Reward.new
    unless params["chain_id"].blank?
      @chain = Chain.find(params["chain_id"])
      psearch_is_blank = params["search"].map{|x,v| v}.join.gsub(" ","").blank? rescue true
      @page = params[:page].blank? ? 1 : params[:page]
      if cookies[:psearch].present? && psearch_is_blank && params[:qry_order].blank?
        p "abus cookie"
        par = cookies[:psearch].split("-----")
        params["search"] = {} if params["search"].blank?
        params["search"]["from"] = par[0] rescue ""
        params["search"]["to"] = par[1] rescue ""
        params["search"]["reward_type"] = par[2].gsub(" ",",") rescue ""
        params["search"]["q"] = par[3] rescue ""
        params[:qry_order] = par[4] rescue ""
        @page = par[5] rescue nil if params[:page].blank?
      end
      if psearch_is_blank && params[:qry_order].blank?
        @rewards = @chain.rewards.where("reward_type != #{Reward::TYPES["PARTNER"]}").includes(:chain).order("updated_at DESC")
      else
        if params["search"]["reward_type"].kind_of?(String)
         params["search"]["reward_type"] = params["search"]["reward_type"].split(",").map{|x| x.to_i}
        end
        @effective_date = params["search"]["from"]
        @expiry_date = params["search"]["to"]
        @search_query = params["search"]["q"]
        @reward_select_type = params["search"]["reward_type"]

        ar_field = [
          "rewards.updated_at",
          "rewards.id",
          "chains.name",
          "rewards.name",
          '"effectiveDate"',
          '"expiryDate"',
          "rewards.points",
          "rewards.reward_type"
        ]

        order = params[:qry_order]
        @order = order.to_i

        if order.to_i < 1000
          qry_order = "#{ar_field[order.to_i]}"
          @rewards = @chain.rewards.where("reward_type != #{Reward::TYPES["PARTNER"]}").includes(:chain).order(qry_order).reverse_order.search(params[:search])
        elsif order.to_i > 999
          order = order.to_i - 1000
          qry_order = "#{ar_field[order]}"
          @rewards = @chain.rewards.where("reward_type != #{Reward::TYPES["PARTNER"]}").includes(:chain).order(qry_order).search(params[:search])
        end
      end
      available_page = (@rewards.size.to_f / Setting.pagination.per_page).to_f.ceil
      @page = available_page if @page.to_i > available_page && available_page > 0
      @page = nil if @page.to_i < 1
      @rewards = @rewards.paginate(page: @page, per_page: Setting.pagination.per_page)
    end
  end

  # DELETE /rewards/1
  def destroy
    rescue_from_cancan(:destroy, Reward)
    @reward= Reward.find(params[:id])

    if @reward.status == "active" #or @reward.restaurant_rewards.count == 0
      @reward.update_attribute(:status, "inactive")
      @reward_id = params[:id]
      message = "Reward successfuly deactived"
    else
      @reward.update_attribute(:status, "active")
      @reward_id = params[:id]
      message = "Reward successfuly actived"
    end
    #    redirect_to admin_rewards_url, notice: message
  end

  def update_participation
    @reward= Reward.find(params[:reward_id])
    submit= params[:commit]

    case submit
    when "Pull Out"
      @reward.pullout(params[:pullout])
    when "Participate"
      @reward.participate(params[:participate])
    else
    end

    redirect_to admin_reward_path(@reward)
  end

  def one_time_reward
    @chain = Chain.find params[:chain_id]
  end


  # GET /one_time_rewards
  def one_time_rewards
    @chain= Chain.find(params[:chain_id]) if params[:chain_id]
    @rewards = Reward.all
    if @chain
      rewardEvents= ChainRewardEvent.where('chain_id = ?', @chain.id)
      @chainRewardEvents= Hash[*rewardEvents.collect { |x| [x.event, x]}.flatten]
      @rewards= Reward.where('reward_type = ? and chain_id = ?', Reward::TYPES["ONE_TIME"], params[:chain_id])
      @rewards = Reward.all
    end
  end

  # POST /save_chain_reward_event
  def save_chain_reward_event
    @chain= Chain.find(params[:chain_id]) if params[:chain_id]
    @event= params[:event]

    #TODO update to 3.2.0 and use the new first_or_initialize method
    #chainRewardEvent= ChainRewardEvent.where(:chain_id=>@chain.id, :event=>@event).first_or_initialize(:reward_id=>@reward_id)

    chainRewardEvent= ChainRewardEvent.find_or_initialize_by_chain_id_and_event(:chain_id=>@chain.id, :event=>@event)

    if chainRewardEvent.update_attribute(:reward_id, params[:reward_id])
      #format.html { redirect_to :controller=>"admin/rewards", :action=>"one_time_rewards", :chain_id=>@chain.id, notice: 'Reward was successfully created.' }
      render :json=> {:notice=>'Reward was successfully assigned.'}
    else
      render :json=> {:error=>'Failed to assign this reward.'}
    end
  end

  def milestone_rewards
    @rewards= Reward.where('reward_type = ?', Reward::TYPES["MILESTONE"])
  end

  def send_test_email
    chain = Chain.find(params[:email_preview][:chain_id])
    RewardMailer.test_push_reward_mail(chain, params[:email_preview][:subject], params[:email_preview][:content]).deliver
  end

  #MILESTONE STUFF

  def milestones
    @chain = Chain.find(params[:id])
    @milestones = @chain.milestones.paginate(page: params[:page], per_page: 10)
    render :layout => false
  end

  def add_milestone
    @chain = Chain.find(params[:id])
    @milestone = @chain.milestones.new
  end

  def edit_milestone
    @chain = Chain.find(params[:chain_id])
    @milestone = @chain.milestones.find(params[:id])
  end

  def create_milestone
    @milestone = Milestone.new(params[:milestone])
    @chain = @milestone.chain
    if @milestone.save
      @is_save = true
    else
      @is_save = false
    end
  end

  def update_milestone
    @chain = Chain.find(params[:chain_id])
    @milestone = @chain.milestones.find(params[:id])
    if @milestone.update_attributes(params[:milestone])
      @is_save = true
    else
      @is_save = false
    end
  end

  def add_reward
    @milestone = Milestone.find(params[:id])
    @chain = @milestone.chain
    @rewards_ids = @chain.milestones.map(&:reward_id)
    @rewards = @chain.rewards.where(:reward_type => Reward::TYPES["MILESTONE"]).paginate(page: params[:page], per_page: 10)
  end

  def remove_milestone_reward
    @from_list = params[:list]
    @milestone = Milestone.find(params[:id])
    @chain = @milestone.chain
    @rewards_ids = @chain.milestones.map(&:reward_id)
    @reward = Reward.find(params[:reward_id])
    @milestone.update_attribute(:reward_id, nil)
  end

  def add_milestone_reward
    @milestone = Milestone.find(params[:id])
    @chain = @milestone.chain
    @rewards_ids = @chain.milestones.map(&:reward_id)
    @reward = Reward.find(params[:reward_id])
    @milestone.update_attribute(:reward_id, @reward.id )
  end

  def add_new_milestone_reward
    @milestone = Milestone.find(params[:id])
    @chain = @milestone.chain
    @reward = Reward.new
  end

  def create_new_milestone_reward
    @milestone = Milestone.find(params[:id])
    @chain = @milestone.chain
    @reward = Reward.new(params[:reward])
    if @reward.save
      @reward.participate(@chain.restaurants.map(&:id))
      @is_save = true
    else
      @is_save = false
    end
  end

  def load_user_list
    @chain = Chain.find params[:id] rescue nil
    @users = @chain.users rescue []
    @rewards = @chain.rewards.where("reward_type != ?",Reward::TYPES["REGULAR"]).active.unexpired.order("name asc") rescue []
    respond_to do |format|
      format.js
      format.json { render :json => @rewards}
    end
  end

  def push_reward
  end

  def push_reward_to_users
    p params
    @errors = []
    @user_failed = []
    @errors << {:chain => "Please pick a chain"}  if params[:push][:chain_id].blank?
    @errors << {:reward => "Please pick a reward"}  if params[:reward_id].blank?
    @errors << {:push_type => "Please pick push type"}  if params[:push][:push_type].blank?
    @errors << {:xls => "Can't be blank"}  if params[:push][:push_type].eql?(1) && params[:push][:xls].blank?
    @errors << {:user_ids => "Can't be blank"}  if params[:push][:push_type].eql?(2) && params[:push][:user_temp_ids].blank?

    if @errors.blank?
      reward = Reward.find(params["reward_id"])
      case params[:push][:push_type].to_i
        when 1
          unless params[:push][:xls].blank?
            begin
              user_ids = []
              book = Spreadsheet.open "#{params[:push][:xls].path}"
              sheet1 = book.worksheet 0
              sheet1.column(0).each do |row|
                user_ids << row.to_i
              end
              user_ids = user_ids.compact.uniq
            rescue
              @errors << {:xls => "File should be in xls format"}
            end
          end
        when 2
          user_ids = []
          user_ids = params[:push][:user_temp_ids]
          user_ids = user_ids.squish.gsub("\\r",'').gsub('\\n','').gsub(' ','').split(',')
        when 3
          user_ids = User.where("chain_id = ?", params[:push][:chain_id]).select('id').map(&:id)
      end

      dl = Delayed::Job.enqueue(PushRewardJob.new(user_selected, reward, params[:push][:chain_id], current_admin.email),
           :chain_id => params[:push][:chain_id], :delayable_type => "PushReward", :delayable_id => nil
      )
    end

    respond_to do |format|
      format.js
    end
  end

  def push_reward_jobs
    @djs = DelayedJob.where("delayable_type = ?",'PushReward').paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  def reward_sent_histories
    @chain = Chain.find params[:id]
    unless params["search_mail"].blank?
      @histories = RewardSentHistory.where("reward_sent_histories.chain_id = ? and LOWER(users.email) like '%#{params["search_mail"].downcase}%'", params[:id]).joins(:user).paginate(page: params[:page], per_page: Setting.pagination.per_page)
    else
      @histories = RewardSentHistory.where(:chain_id => @chain.id).paginate(page: params[:page], per_page: Setting.pagination.per_page)
    end
  end

  def view_claim_history
    @reward = Reward.find params[:id]
    respond_to do |format|
      format.html {
        #@reward_transactions = RewardTransaction.includes(:restaurant_reward => :reward).where("rewards.id = ?", @reward.id)#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
        #@reward_transactions = RewardTransaction.where("reward_id = ? AND redeeming is false", @reward.id).order("id DESC").paginate(page: params[:page], per_page: Setting.pagination.per_page)
        @reward_transactions = RewardTransaction.select("
          distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
reward_transactions.restaurant_id,
 reward_transactions.user_id)  reward_transactions.*").where("reward_id  = ? and redeeming IS FALSE", @reward.id).
        order("date(reward_transactions.created_at) DESC").paginate(page: params[:page], per_page: Setting.pagination.per_page)
      }
      format.csv{
        #export_transactions_csv(@reward, RewardTransaction.includes(:restaurant_reward => :reward).where("rewards.id = ?", @reward.id) )
        export_transactions_csv(@reward, RewardTransaction.select("
          distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
reward_transactions.restaurant_id,
 reward_transactions.user_id)  reward_transactions.*").where("reward_id  = ? and redeeming IS FALSE", @reward.id).
            order("date(reward_transactions.created_at) DESC") )
      }
    end
  end

  def claim_history_job
    # testing purpose
    test = ENV["test_duplicate"]
    email = current_admin.email
    email = "inoe.bainur@gmail.com" unless test.blank?

    Delayed::Job.enqueue(ExportCsvJob::RewardRedemptionJob.new(params[:id],email))
    render :nothing => true
  end

  def unused_pos_codes
    @reward = Reward.find params[:id]

    conditions = ["user_id IS NULL"]
    conditions << "code LIKE '%#{params[:search][:code]}%'" if params[:search] && params[:search][:code]
    conditions = conditions.join(" AND ")
    @codes = @reward.reward_pos_codes.where(conditions).paginate(:per_page => 50, :page => params[:page])
    render :template => "admin/rewards/reward_pos_codes.html"
  end

  def general_items
    @reward = Reward.find params[:id]
    chain = @reward.chain
    @general_items = RewardMenuItem.select("reward_menu_items.required, general_menu_items.*").joins("
      RIGHT JOIN general_menu_items ON general_menu_items.id = reward_menu_items.general_menu_item_id AND reward_menu_items.reward_id = #{@reward.id}
    ").order("reward_menu_items.updated_at DESC NULLS LAST")
    .where("general_menu_items.chain_id = #{@reward.chain_id} AND (LOWER(item_name) LIKE '%#{(params[:key] || "").downcase}%' OR LOWER(description) LIKE '%#{(params[:key] || "").downcase}%' OR LOWER(item_keywords) LIKE '%#{(params[:key] || "").downcase}%')")
      .paginate(:page => params[:page], :per_page => Setting.pagination.per_page)
    @general_item_ids = @reward.reward_menu_items.map(&:general_menu_item_id)
  end

  def add_general_item
    @reward = Reward.find params[:id]
    @general_item = GeneralMenuItem.find params[:menu_item_id]
    RewardMenuItem.create(:reward_id => @reward.id, :general_menu_item_id => @general_item.id)
    reward_menu_item = @general_item.reward_menu_items.first
    @general_item.required = reward_menu_item.required
    @general_item_ids = @reward.reward_menu_items.map(&:general_menu_item_id)
  end

  def remove_general_item
    @reward = Reward.find params[:id]
    @general_item = GeneralMenuItem.find params[:menu_item_id]
    rmi = RewardMenuItem.where(:reward_id => @reward.id, :general_menu_item_id => @general_item.id).first
    rmi.destroy if rmi
    @general_item_ids = @reward.reward_menu_items.map(&:general_menu_item_id)
  end

  def set_type
    @reward = Reward.find params[:id]
    @general_item = GeneralMenuItem.find params[:menu_item_id]
    rmi = RewardMenuItem.where(:reward_id => @reward.id, :general_menu_item_id => @general_item.id).first
    type = params[:type].eql?("required") ? true : false
    rmi.update_column(:required, type)
    @general_item.required = type
    @general_item_ids = @reward.reward_menu_items.map(&:general_menu_item_id)
  end


  def discount_type
    @reward = Reward.find params[:id]
    @pos_discount_type = @reward.pos_discount_type || @reward.build_pos_discount_type
  end

  def create_pos_discount_type
    @reward = Reward.find params[:id]
    @pos_discount_type = @reward.pos_discount_type
    if @pos_discount_type
      @is_saved = @pos_discount_type.update_attributes(params[:pos_discount_type])
    else
      @pos_discount_type = PosDiscountType.new(params[:pos_discount_type])
      @is_saved = @pos_discount_type.save
    end
  end

  def fishbowl_promotion_setting
    @reward = Reward.find params[:id]
    if @reward.fishbowl_reward_promotion_setting.blank?
      @reward.build_fishbowl_reward_promotion_setting
    end
    render :layout => false
  end

  def create_fishbowl_promotion_setting
    @reward = Reward.find(params['id'])
    if @reward.update_attributes(params['reward'])
      @error = false
    else
      @error = true
    end
  end

  def fishbowl_promotion_codes
    @reward = Reward.find(params[:id])
    conditions = ["reward_id = #{@reward.id}"]
    conditions << "users.email LIKE '%#{params[:search][:email]}%'" if params[:search] && !params[:search][:email].blank?
    conditions << "code LIKE '%#{params[:search][:code]}%'" if params[:search] && params[:search][:code]
    conditions = conditions.join(" AND ")
    @codes = FishbowlPromotionCode.joins("LEFT JOIN users ON fishbowl_promotion_codes.user_id = users.id").where(conditions).order("user_id DESC NULLS LAST").paginate(:page => params[:page], :per_page => 50)
  end

  private

  def export_transactions_csv(reward, transactions)

    transaction_csv = CSV.generate do |csv|
      # header row
      csv << [
        "Claimed at",
        "User ID",
        "Email",
        "Location"
      ]

      # data rows
      transactions.each do |transaction|
        user =  User.find(transaction.user_id) rescue ""
        csv << [
          transaction.created_at.strftime("%Y-%m-%d %H:%M"),
          (user.id rescue ''),
          (user.email rescue ''),
          "#{transaction.latitude}  #{transaction.longitude}"
        ]
      end
    end

    send_data(transaction_csv, :type => 'text/csv', :filename => "#{reward.name}_claim_history.csv")
  end

  def clear_filter
    @effective_date = nil;
    @expiry_date = nil;
    @search_query = nil;
    @reward_select_type = nil;
  end

  def find_event
    @reward_id = params[:reward_id]
    @event_id = params[:event_id]
  end
end
