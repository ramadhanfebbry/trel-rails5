class Admin::PosRewardChecksController < Admin::BaseController

  def index
    conditions = []
    conditions << "chains.id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "restaurants.id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    conditions << "users.email LIKE '%#{params[:email]}%'" unless params[:email].blank?
    if conditions.blank?
      #@pos_reward_checks = PosRewardCheck.order("id DESC").paginate(:page => params[:page], :per_page => 10, :total_entries => -1)
      @pos_reward_checks = PosRewardCheck.order("id DESC").
          select('pos_reward_checks.id,pos_reward_checks.chain_id,pos_reward_checks.pos_location_id,pos_reward_checks.status,
                  pos_reward_checks.created_at,pos_reward_checks.user_id,pos_reward_checks.reward_code,
                  pos_reward_checks.error_description,pos_reward_checks.pos_used_type, pos_reward_checks.transaction_type, pos_reward_checks.reward_transaction_id').
          paginate(:page => params[:page], :per_page => 10, :total_entries => -1)
    else
      @pos_reward_checks = PosRewardCheck.
          select('pos_reward_checks.id,pos_reward_checks.chain_id,pos_reward_checks.pos_location_id,pos_reward_checks.status,
                  pos_reward_checks.created_at,pos_reward_checks.user_id,pos_reward_checks.reward_code,
                  pos_reward_checks.error_description,pos_reward_checks.pos_used_type,pos_reward_checks.transaction_type, pos_reward_checks.reward_transaction_id').includes(:user, :pos_location => {:restaurant => :chain}).where(conditions.join(" AND ")).order("pos_reward_checks.id DESC").paginate(:page => params[:page], :per_page => 10, :total_entries => -1)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @pos_reward_check = PosRewardCheck.find params[:id]
    respond_to do |format|
      format.html
      format.js
    end
  end


  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : (Restaurant.where("chain_id IN (?)", params[:chains]).order("name asc") rescue [])
  end


end
