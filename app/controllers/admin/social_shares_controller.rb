class Admin::SocialSharesController < Admin::BaseController

  # GET /admin/games
  # GET /admin/games.json
  def index
    @social_shares = SocialShare.order('chain_id, social_shares.created_at desc').includes(:social_share_participants).paginate(page: params[:page], per_page: 10)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @social_shares }
    end
  end

  def pick_incentive
    @social = SocialShare.find(params[:id])
    @referer = params['referer']
  end

  def create_reward_incentive
    @social = SocialShare.find params[:id]
    @is_save = false
    @reward = Reward.new(params[:reward])
    @reward.status = "active"
    @reward.chain_id = @social.chain.id



    reward_before = @social.incentive

    @type = "reward"

    respond_to do |format|
      if @reward.save
        restaurant_ids = Chain.find(@reward.chain_id).restaurants.map(&:id)

        unless restaurant_ids.blank?
          @reward.participate(restaurant_ids)
        end


        attr = {:incentive_type => "Reward", :incentive_id => @reward.id}
        @social.update_attributes(attr)

        @is_save = true
        reward_before.destroy if reward_before
        format.js { render "admin/social_shares/incentive/create_incentive" }
      else
        format.js { render "admin/social_shares/incentive/create_incentive"  }
      end
    end
  end

  def load_onetime_reward
    @social = SocialShare.find(params[:id])
    @rewards = @social.chain.rewards.active.where(:reward_type => Reward::TYPES["INCENTIVE"]).
        paginate(page: params[:page], per_page: Setting.pagination.per_page)
    respond_to do |format|
      format.js { render "admin/social_shares/incentive/load_one_time_reward"}
    end
  end

  def load_points
    @social = SocialShare.find(params[:id])
    chain = @social.chain
    @points = SocialOffer.where(:kind => 1, :chain_id => chain.id ).
        paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
      format.js { render "admin/social_shares/incentive/load_points"}
    end
  end

  def assign_incentive
    @social = SocialShare.find(params[:id])
    @type = params[:type]
    case @type
      when "reward"
        @reward = Reward.new
      when "offer", "offline"
        @offer = SocialOffer.new
    end

    @chain = @social.chain
  end

  def activate
    notice = "default program are activated"
    @social = SocialShare.find(params[:id])
    if @social.is_active?
      @social.set_active_program
    else
      @error_on_date = "true"
      notice = "Date for this social are not Set !! please set it first"
    end

    @chain = @social.chain
    @socials = SocialShare.paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.html {redirect_to admin_social_shares_path, :notice => notice}
      format.json { render json: @social }
      format.js {render "create"}
    end
  end


  def update_incentive
    @offer = SocialOffer.find(params[:id])
    @social = SocialShare.where(:incentive_type => @offer.class.to_s, :incentive_id => @offer.id).first
    puts "xxxxxxxxxxxxxxxxxxxxxxxx"
    puts @social
    @is_save = false
    @type = "offer"

    respond_to do |format|
      if @offer.update_attributes(params[:social_offer])
        @is_save = true
        format.js { render "admin/social_shares/incentive/create_incentive" }
      else
        format.js { render "admin/social_shares/incentive/create_incentive"  }
      end
    end
  end

  def assign_with_existing_reward
    @referer = params['referer']
    @social = SocialShare.find params[:id]

    @type = params[:type]
    if @type == "reward"
      @reward = Reward.find(params[:reward_id])
      @social.update_attributes!(:incentive_type => "Reward", :incentive_id => @reward.id) if @referer.blank?
    else
      @offer = SocialOffer.find(params[:reward_id])
      @social.update_attributes!(:incentive_type => "SocialOffer", :incentive_id => @offer.id) if @referer.blank?
    end
  end



  def edit_incentive
    @social = SocialShare.find params[:id]
    @offer = SocialOffer.find(params[:id])

    @referral_program = @offer.social_share
    @chain = @offer.chain
    respond_to do |format|
      format.js { render "admin/social_shares/incentive/edit_incentive" }
    end
  end


  def create_offer_incentive
    @social = SocialShare.find params[:id]
    @is_save = false
    @type = "offer"
    @offer = SocialOffer.new(params[:social_offer])
    @referer = params['referer']
    @offer.kind = SocialOffer::TYPES["FIXED_POINTS"]
    respond_to do |format|
      if @offer.save!
        @social.update_attributes!(:incentive_type => "SocialOffer", :incentive_id => @offer.id)
        @is_save = true
        format.js { render "admin/social_shares/incentive/create_incentive" }
      else
        format.js { render "admin/social_shares/incentive/create_incentive" }
      end
    end
  end

  def remove_incentive
    @social = SocialShare.find params[:id]
    @social.update_attribute(:incentive, nil)

    respond_to do |format|
      format.js { render "admin/social_shares/incentive/remove_incentive" }
    end
  end
  # GET /admin/games/1
  # GET /admin/games/1.json
  def show
    @social_share = SocialShare.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @social_share }
    end
  end

  # GET /admin/games/new
  # GET /admin/games/new.json
  def new
    @social_share = SocialShare.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @social_share }
    end
  end

  # GET /admin/games/1/edit
  def edit
    @social_share = SocialShare.find(params[:id])
  end

  # POST /admin/games
  # POST /admin/games.json
  def create
    @social_share = SocialShare.new(params[:social_share])

    respond_to do |format|
      if @social_share.save
        format.html { redirect_to admin_social_shares_url, notice: 'Social Share was successfully created.' }
        format.json { render json: @social_share }
      else
        format.html { render action: "new" }
        format.json { render json: @social_share.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/games/1
  # PUT /admin/games/1.json
  def update
    @social_share = SocialShare.find(params[:id])

    respond_to do |format|
      if @social_share.update_attributes(params[:social_share])
        format.html { redirect_to admin_social_shares_url, notice: 'Social Share was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @social_share.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/games/1
  # DELETE /admin/games/1.json
  def destroy
    @social_share = SocialShare.find(params[:id])
    @social_share.update_column(:is_default, false)

    respond_to do |format|
      format.html { redirect_to admin_social_shares_url }
      format.json { head :ok }
    end
  end

  def group_chain_list
    @query ={}
    @query[:chain_id] = params[:chain_id] if !params[:chain_id].blank?
    @query[:id] = params[:social_share_id] if !params[:social_share_id].blank?
    @social_shares = SocialShare.order('chain_id, social_shares.created_at desc').includes(:social_share_participants).where(@query).paginate(page: params[:page], per_page: 10)
  end
end
