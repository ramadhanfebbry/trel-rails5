class Admin::GoalCategoriesController < Admin::BaseController

  def index

  end

  def categories_by_chain
    @chain = Chain.find(params[:id]) rescue nil
    @goal_categories = @chain.goal_categories.order("updated_at DESC") rescue []
    respond_to do |format|
      format.html {render "categories_by_chain.html"}
      format.js {render "categories_by_chain.js"}
    end
  end

  def new
    @chain = Chain.find(params[:chain_id])
    @goal_category = @chain.goal_categories.new
    @chain.locales.each do |locale|
      @goal_category.goal_category_locales.build(:locale_id => locale.id)
    end
  end

  def create
    @goal_category = GoalCategory.new(params[:goal_category])
    @chain = @goal_category.chain
    if @goal_category.save
      redirect_to categories_by_chain_admin_goal_category_path(@chain), :notice => "Goal Category Successfully created"
    else
      render :new
    end
  end

  def edit
    @goal_category = GoalCategory.find(params[:id])
    @chain = @goal_category.chain
    (@chain.locales - @goal_category.goal_category_locales.map{|gl| gl.locale}).each do |locale|
      @goal_category.goal_category_locales.build(:locale_id => locale.id)
    end
  end
  
  def update
    @goal_category = GoalCategory.find(params[:id])
    @chain = @goal_category.chain
    if @goal_category.update_attributes(params[:goal_category])
      redirect_to categories_by_chain_admin_goal_category_path(@chain), :notice => "Goal Category Successfully updated"
    else
      render :edit
    end
  end

  def deactivate
    @goal_category = GoalCategory.find(params[:id])
    @chain = @goal_category.chain
    @goal_category.update_attribute(:status, GoalCategory::STATUS[:INACTIVE])
    redirect_to categories_by_chain_admin_goal_category_path(@chain), :notice => "Goal Category Successfully deactived"
  end

  def activate
    @goal_category = GoalCategory.find(params[:id])
    @chain = @goal_category.chain
    @goal_category.update_attribute(:status, GoalCategory::STATUS[:ACTIVE])
    redirect_to categories_by_chain_admin_goal_category_path(@chain), :notice => "Goal Category Successfully actived"
  end
  
end
