class Admin::WebhooksController < Admin::BaseController

  def index
    @webhooks = OloWebhook.search_chain(params[:chains]).
    search_restaurant(params[:restaurant]).
    search_email(params[:email]).
    order('updated_at desc').paginate(per_page: 10, page: params[:page])
  end

  def edit
    @webhook = OloWebhook.find params[:id]
  end

  def update
    @webhook = OloWebhook.find params[:id]
    #@webhook.update_attributes(params[:olo_webhook])

    @webhook.content["customer"]["email"]  = params["olo_webhook"]["email"]
    @webhook.content["totals"]["subTotal"] = params["olo_webhook"]["subtotal"]
    @webhook.content["orderId"]            = params["olo_webhook"]["order_id"]

    if @webhook.save
      redirect_to admin_webhooks_path
    end
  end

  def process_receipt_olo
    @webhook = OloWebhook.find params[:id]
    @webhook.delay_receipt

    redirect_to admin_webhooks_path
  end
end
