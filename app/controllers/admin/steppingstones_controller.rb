class Admin::SteppingstonesController < Admin::BaseController

  before_action :get_chain

  def index
    @steppingstones = @chain.steppingstones.all
  end

  def new
    @steppingstone = @chain.steppingstones.new
  end

  def create
    @steppingstone = @chain.steppingstones.new(params[:steppingstone])
    if @steppingstone.save
      redirect_to admin_chain_steppingstones_path(@chain), :notice => "Steppingstone has been added"
    else
      render "new"
    end
  end

  def edit
    @steppingstone = @chain.steppingstones.find params[:id]
  end

  def update
    @steppingstone = @chain.steppingstones.find params[:id]
    if @steppingstone.update_attributes(params[:steppingstone])
      redirect_to admin_chain_steppingstones_path(@chain), :notice => "Steppingstone has been updated"
    else
      render "edit"
    end
  end

  def destroy
    @steppingstone = @chain.steppingstones.find params[:id]
    @steppingstone.destroy
    redirect_to admin_chain_steppingstones_path(@chain), :notice => "Steppingstone has been deleted"
  end


  protected

  def get_chain
    @chain = Chain.find params[:chain_id]
  end

  
end
