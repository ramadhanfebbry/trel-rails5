class Admin::VantivCredentialsController < Admin::BaseController

  def index

  end

  def search
    @chain = Chain.find(params["chain_id"])
    @vantiv_credential = VantivCredential.find_by_chain_id(@chain.id)
  end

  def new
    @chain = Chain.find params['chain_id']
    @vantiv_credential = VantivCredential.new
    render :layout => false
  end

  def edit
    @vantiv_credential = VantivCredential.find params[:id]
    @chain = Chain.find @vantiv_credential.chain_id
    render :layout => false
  end

  def create
    @vantiv_credential = VantivCredential.new(params[:vantiv_credential])

    if @vantiv_credential.save
      @save = true
    else
      @save = false
    end
  end

  def update
    @vantiv_credential = VantivCredential.find params[:id]
    if @vantiv_credential.update_attributes(params[:vantiv_credential])
      @update = true
    else
      @update = false
    end
  end

  def destroy
    @vantiv_credential = VantivCredential.find params[:id]
    @vantiv_credential.destroy unless @vantiv_credential.blank?

    redirect_to admin_vantiv_credentials_path , :notice => "Vantiv Credential Deleted"
  end

end
