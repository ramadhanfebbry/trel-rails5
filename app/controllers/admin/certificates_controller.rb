class Admin::CertificatesController < Admin::BaseController
  before_action :get_application

  def index
    rescue_from_cancan(:index, Rpush::Apns::App)
    @certificate = @application.rapns_app
  end

  def new
    rescue_from_cancan(:new, Rpush::Apns::App)
    @certificate = Rpush::Apns::App.new
  end
  
  def edit
    rescue_from_cancan(:edit, Rpush::Apns::App)
    @certificate = Rpush::Apns::App.find(params[:id])
  end

  def create
    rescue_from_cancan(:create, Rpush::Apns::App)
    @certificate = Rpush::Apns::App.new
    @certificate.name = params[:rpush_apns_app][:name]
    @certificate.environment = params[:rpush_apns_app][:environment]
    @certificate.certificate =   params[:rpush_apns_app][:certificate].read unless params[:rpush_apns_app][:certificate].blank?
    @certificate.connections = 1
    if @certificate.save
      flash[:notice] = "Your push notification setting has been submitted"
      redirect_to :action => "index"
    else
      flash[:error] = "There was a problem submitting your setting."
      render :action => "new"
    end
  end

  def update
    rescue_from_cancan(:update, Rpush::Apns::App)
    @certificate = Rpush::Apns::App.find(params[:id])
    @certificate.name = params[:rpush_apns_app][:name]
    @certificate.environment = params[:rpush_apns_app][:environment]
    @certificate.certificate =  params[:rpush_apns_app][:certificate].read unless params[:rpush_apns_app][:certificate].blank?
    if @certificate.save
      flash[:notice] = "Your push notification setting has been submitted"
      redirect_to :action => "index"
    else
      flash[:error] = "There was a problem submitting your setting."
      render :action => "edit"
    end
  end
  def download
    @certificate = @application.certificate
    send_data @certificate.cert_file, :filename => @certificate.filename, :type => @certificate.content_type
  end
  
  protected

  def get_application
    @application = Application.find(params[:application_id])
  end
end
