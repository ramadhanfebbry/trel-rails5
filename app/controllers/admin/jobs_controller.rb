class  Admin::JobsController < Admin::BaseController

  def index
    @jobs = DelayedJob.report_jobs.paginate(:per_page => 10,page: params[:page])
  end
end
