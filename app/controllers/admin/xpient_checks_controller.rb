class Admin::XpientChecksController < Admin::BaseController

  def index
    conditions = []
    conditions << "is_xpient_check IS TRUE"
    conditions << "chains.id IN (#{params[:chains].join(",")})" unless params[:chains].blank?
    conditions << "restaurants.id IN (#{params[:restaurants].join(",")})" unless params[:restaurants].blank?
    if conditions.blank?
      @pos_check_uploads = PosCheckUpload.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    else
      @pos_check_uploads = PosCheckUpload.includes(:pos_location => {:restaurant => :chain}).where(conditions.join(" AND ")).order("pos_check_uploads.created_at DESC").paginate(:page => params[:page], :per_page => 10)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @pos_check_upload = PosCheckUpload.find params[:id]
    respond_to do |format|
      format.html
      format.js
    end
  end

  def load_restaurants
    @restaurants = params[:chains].blank? ? [] : Restaurant.where("chain_id IN (?)", params[:chains]).order('name asc') rescue []
  end


end
