class Admin::CountriesController < Admin::BaseController
  before_action :find_country, :only => [:update_status, :show, :edit, :update]

  def index
    rescue_from_cancan(:index, Country)
    @country = Country.new
    @countries = Country.paginate(page: params[:page], per_page: 10)
  end

  def update_status
    rescue_from_cancan(:update_status, Country)
    respond_to do |format|

      if @country.update_attribute(:active, params[:country_status])
        flash[:notice] = "country has been updated"
        format.html { redirect_to admin_countries_path }
        format.json { render json: @country }
      else
        flash[:error] = "country failed to update"
        format.html { redirect_to countries_admin_path }
        format.json { render json: @country }
      end
    end
  end

  def show
    rescue_from_cancan(:show, Country)
  end

  def edit
    rescue_from_cancan(:edit, Country)
  end

  def create
    rescue_from_cancan(:create, Country)
    @country = Country.new(params[:country])

    respond_to do |format|
      if @country.save
        flash[:notice] = "country has been created"
        format.html { redirect_to admin_countries_path }
      else
        flash[:notice] = "country failed to create #{@country.errors.full_messages.each{|msg|}}"
        format.html { redirect_to  admin_countries_path }
        format.json { render json: @country }
      end
    end
  end

  def update
    rescue_from_cancan(:update, Country)
    respond_to do |format|
      if @country.update_attributes(params[:country])
        flash[:notice] = "country has been updated"
        format.html { redirect_to admin_countries_path }
      else
        flash[:notice] = "country failed to create"
        format.html { render :action => "edit" }
        format.json { render json: "failed" }
      end
    end
  end

  def search_country
    sth = params[:search_text_hidden]
    ar_field = [
      "id",
      "name",
      "abbreviation",
      '"ISO"',
      "id",
      "active"
    ]

    order = params[:qry_order]

    if order.to_i < 1000
      qry_order = "#{ar_field[order.to_i]}"
      @countries = Country.where("lower(countries.name) like '%#{sth.downcase}%' OR lower(countries.abbreviation) like '%#{sth.downcase}%'").order(qry_order).paginate(page: params[:page], per_page: 10)
    elsif order.to_i > 999
      order = order.to_i - 1000
      qry_order = "#{ar_field[order]}"
      @countries = Country.where("lower(countries.name) like '%#{sth.downcase}%' OR lower(countries.abbreviation) like '%#{sth.downcase}%'").order(qry_order).reverse_order.paginate(page: params[:page], per_page: 10)
    end

  end

  private

  def find_country
    @country = Country.find(params[:id])
  end

end
