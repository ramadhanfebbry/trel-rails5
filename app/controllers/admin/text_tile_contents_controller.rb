class Admin::TextTileContentsController < Admin::BaseController

  before_action :get_chain
  before_action :get_content_type

  def index
    @text_tile_contents = TextTileContent.where(:content_type => @model)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @text_tile_contents }
    end
  end

  # GET /admin/text_tile_contents/1
  # GET /admin/text_tile_contents/1.json
  def show
    @text_tile_content = TextTileContent.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @text_tile_content }
    end
  end

  # GET /admin/text_tile_contents/new
  # GET /admin/text_tile_contents/new.json
  def new
    @text_tile_content = TextTileContent.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @text_tile_content }
    end
  end

  # GET /admin/text_tile_contents/1/edit
  def edit
    @text_tile_content = TextTileContent.find(params[:id])
  end

  # POST /admin/text_tile_contents
  # POST /admin/text_tile_contents.json
  def create
    @text_tile_content = TextTileContent.new(title_content_params.merge(:content_type => @model, :chain_id => @chain.id))

    respond_to do |format|
      if @text_tile_content.save
        format.html { redirect_to admin_chain_text_tile_content_path(@model, @chain, @text_tile_content), notice: "#{@model.humanize} was successfully created." }
        format.json { render json: @text_tile_content, status: :created, location: @text_tile_content }
      else
        format.html { render action: "new" }
        format.json { render json: @text_tile_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/text_tile_contents/1
  # PUT /admin/text_tile_contents/1.json
  def update
    @text_tile_content = TextTileContent.find(params[:id])

    respond_to do |format|
      if @text_tile_content.update(title_content_params)
        format.html { redirect_to admin_chain_text_tile_content_path(@model, @chain, @text_tile_content), notice: "#{@model.humanize} was successfully updated."  }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @text_tile_content.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/text_tile_contents/1
  # DELETE /admin/text_tile_contents/1.json
  def destroy
    @text_tile_content = TextTileContent.find(params[:id])
    @text_tile_content.destroy

    respond_to do |format|
      format.html { redirect_to admin_chain_text_tile_contents_path(@model, @chain) }
      format.json { head :ok }
    end
  end

  private

  def get_content_type
    @model = params[:content_type]
  end

  def get_chain
    @chain = Chain.find params[:chain_id]
  end
  def title_content_params
    params.require(:text_tile_content).permit(:locale_id, :content_id, :chain_id, :content_type)
  end
end
