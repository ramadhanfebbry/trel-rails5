class Admin::ReferralProgramsController < Admin::BaseController
  # GET /admin/referral_programs
  # GET /admin/referral_programs.json
  before_action :find_chain, :only => [:create]
  before_action :find_ref, :only => [:load_onetime_reward, :load_points, :edit_ref, :show_ref,:create_offer_incentive,
    :activate,:pick_incentive, :show,:assign_with_existing_reward,:assign_incentive, :create_reward_incentive,:remove_incentive]

  def index
    #@referral_programs = ReferralProgram.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: [] }
    end
  end

  def activate
    if @referral_program.is_active?
      @referral_program.set_active_program      
    else
      @error_on_date = "true"
    end

    @chain = @referral_program.chain
    @referral_programs = ReferralProgram.by_chain(@chain).paginate(page: params[:page], per_page: 10)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @referral_programs }
      format.js {render "create"}
    end
  end

  def notifications
    @chains = Chain.active.paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  def edit_incentive
    @offer = ReferralOffer.find(params[:id])

    @referral_program = @offer.referral_program
    @chain = @offer.chain
    respond_to do |format|
      format.js { render "admin/referral_programs/incentive/edit_incentive" }
    end
  end

  def update_incentive
    @offer = ReferralOffer.find(params[:id])
    @referral_program = ReferralProgram.where(:incentive_type => @offer.class.to_s, :incentive_id => @offer.id).first
    @is_save = false
    @type = "offer"

    respond_to do |format|
      if @offer.update_attributes(params[:referral_offer])
        @is_save = true
        format.js { render "admin/referral_programs/incentive/create_incentive" }
      else
        format.js { render "admin/referral_programs/incentive/create_incentive"  }
      end
    end
  end

  def create_reward_incentive
    @is_save = false
    @reward = Reward.new(params[:reward])
    @reward.status = "active"
    @reward.chain_id = @referral_program.chain.id
    @referer = params['referer']

    if @referer.blank?
      reward_before = @referral_program.incentive
    else
      reward_before = @referral_program.incentive_referer
    end
    @type = "reward"

    respond_to do |format|
      if @reward.save
        restaurant_ids = Chain.find(@reward.chain_id).restaurants.map(&:id)

        unless restaurant_ids.blank?
          @reward.participate(restaurant_ids)
        end

        attr = {:incentive_referer_type => "Reward", :incentive_referer_id => @reward.id} unless @referer.blank?
        attr = {:incentive_type => "Reward", :incentive_id => @reward.id} if @referer.blank?
        @referral_program.update_attributes(attr)

        @is_save = true
        reward_before.destroy if reward_before
        format.js { render "admin/referral_programs/incentive/create_incentive" }
      else
        format.js { render "admin/referral_programs/incentive/create_incentive"  }
      end
    end
  end

  def remove_incentive
    @referer = params['referer']
    if @referer.blank?
      @referral_program.update_attribute(:incentive, nil)
    else
      @referral_program.update_attribute(:incentive_referer, nil)
    end

    respond_to do |format|
      format.js { render "admin/referral_programs/incentive/remove_incentive" }
    end
  end

  def new_ref
     @referral_program = ReferralProgram.new
     @chain = Chain.find params[:chain_id]
  end

  def show_ref     
     @chain = Chain.find params[:chain_id]
  end

  def edit_ref     
     @chain = Chain.find params[:chain_id]
     respond_to do |format|
       format.js { render "new_ref"}
     end
  end

  def pick_incentive
    @referer = params['referer']
  end
  
  def search
    clear_filter
    session[:restaurant_ids] = []
    unless params["chain_id"].blank?
      @chain = Chain.find(params["chain_id"])
      @referral_programs = @chain.referral_programs.paginate(page: params[:page], per_page: Setting.pagination.per_page)
      @ref_program = ReferralProgram.new
    else
      @referral_programs = []
      @ref_program = RefferalProgram.new
      #@chain = Chain.first
    end

    p "************ controller executed----------------"
  end

  def load_onetime_reward
    @referer = params['referer']
    @rewards = @referral_program.chain.rewards.active.where(:reward_type => Reward::TYPES["INCENTIVE"]).
      paginate(page: params[:page], per_page: Setting.pagination.per_page)
    respond_to do |format|
      format.js { render "admin/referral_programs/incentive/load_one_time_reward"}
    end
  end

  def load_points
    @referer = params['referer']
    chain = @referral_program.chain
    @points = ReferralOffer.where(:kind => 1, :chain_id => chain.id ).
      paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
       format.js { render "admin/referral_programs/incentive/load_points"}
     end
  end

 def assign_with_existing_reward
    @referer = params['referer']

    @type = params[:type]
    if @type == "reward"
      @reward = Reward.find(params[:reward_id])
      @referral_program.update_attributes!(:incentive_type => "Reward", :incentive_id => @reward.id) if @referer.blank?
      @referral_program.update_attributes!(:incentive_referer_type => "Reward", :incentive_referer_id => @reward.id) unless @referer.blank?
    else
      @offer = ReferralOffer.find(params[:reward_id])
      @referral_program.update_attributes!(:incentive_type => "ReferralOffer", :incentive_id => @offer.id) if @referer.blank?
      @referral_program.update_attributes!(:incentive_referer_type => "ReferralOffer", :incentive_referer_id => @offer.id) unless @referer.blank?
    end
  end

 def assign_incentive
    # @deal = Deal.find params[:id]
    @referer = params['referer']
    @type = params[:type]
    case @type
    when "reward"
      @reward = Reward.new
    when "offer", "offline"
      @offer = ReferralOffer.new
    end

   @chain = @referral_program.chain
  end

 def create_offer_incentive
    @is_save = false
    @type = "offer"
    @offer = ReferralOffer.new(params[:referral_offer])
    @referer = params['referer']
    @offer.kind = ReferralOffer::TYPES["FIXED_POINTS"]
    respond_to do |format|
      if @offer.save!
        @referer = params['referer']
        @referral_program.update_attributes!(:incentive_type => "ReferralOffer", :incentive_id => @offer.id) if params['referer'].blank?
        @referral_program.update_attributes!(:incentive_referer_type => "ReferralOffer", :incentive_referer_id => @offer.id) if params['referer'] == 'true'
        @is_save = true
        format.js { render "admin/referral_programs/incentive/create_incentive" }
      else
        format.js { render "admin/referral_programs/incentive/create_incentive" }
      end
    end
  end

  # GET /admin/referral_programs/1
  # GET /admin/referral_programs/1.json
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @referral_program }
    end
  end

  # GET /admin/referral_programs/new
  # GET /admin/referral_programs/new.json
  def new
    @referral_program = ReferralProgram.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @referral_program }
    end
  end

  # GET /admin/referral_programs/1/edit
  def edit
    @referral_program = ReferralProgram.find(params[:id])
  end

  # POST /admin/referral_programs
  # POST /admin/referral_programs.json
  def create    
    @referral_program = ReferralProgram.new(params[:referral_program])
    #@chain.referral_program = @referral_program

    respond_to do |format|
      if  @referral_program.save
        @referral_programs = ReferralProgram.where("chain_id = ?", @referral_program.chain_id).paginate(page: params[:page], per_page: 10)

        format.js {render "create"}
      else
        
      end
    end
  end

  # PUT /admin/referral_programs/1
  # PUT /admin/referral_programs/1.json
  def update
    @referral_program = ReferralProgram.find(params[:id])

    respond_to do |format|
      if @referral_program.update_attributes(params[:referral_program])
        @referral_programs = ReferralProgram.where("chain_id = ?", @referral_program.chain_id).paginate(page: params[:page], per_page: 10)
        @chain = @referral_program.chain
        format.html { redirect_to @referral_program, notice: 'Referral program was successfully updated.' }
        format.json { head :ok }
        format.js { render "create"}
      else
        @chain = @referral_program.chain
        format.html { render action: "edit" }
        format.json { render json: @referral_program.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/referral_programs/1
  # DELETE /admin/referral_programs/1.json
  def destroy
    @referral_program = ReferralProgram.find(params[:id])
    @referral_program.destroy

    respond_to do |format|
      format.html { redirect_to admin_referral_programs_url }
      format.json { head :ok }
    end
  end

  private

  def clear_filter
   
  end

  def find_ref
    @referral_program = ReferralProgram.find params[:id]
  end

  def find_chain
    @chain = Chain.find params["referral_program"]["chain_id"]
  end
end
