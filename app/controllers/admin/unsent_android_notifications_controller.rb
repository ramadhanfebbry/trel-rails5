class Admin::UnsentAndroidNotificationsController < Admin::BaseController

  def index
    @gcm_unsent_histories = GcmUnsentHistory.paginate(page: params[:page], per_page: 10)
  end

  def remove_regid
    @gcm_unsent_history = GcmUnsentHistory.find(params[:id])
    @gcm_unsent_history.remove_reg_id_from_user
    redirect_to admin_unsent_android_notifications_path
  end
end
