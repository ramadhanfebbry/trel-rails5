class Admin::RafflesController < Admin::BaseController
  
  def index

  end

  def search
    @chain = Chain.find(params[:chain_id]) rescue nil
    @raffles = @chain.raffles rescue nil
  end

  def new
    @chain = Chain.find(params[:chain_id]) rescue nil
    @raffle = Raffle.new(:chain_id => @chain.id)
  end

  def show
     
  end

  def edit
    @raffle = Raffle.find(params[:id])
  end

  def update
    params[:raffle][:start_date] = DateTime.strptime(params[:raffle][:start_date], "%Y-%m-%d").beginning_of_day
    params[:raffle][:end_date] = DateTime.strptime(params[:raffle][:end_date], "%Y-%m-%d").end_of_day

    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    if params[:raffle][:start_date] > params[:raffle][:end_date]
      @is_saved = false
    else
      @is_saved = @raffle.update_attributes(params[:raffle])
    end
    @raffles = @chain.raffles.order("id asc")
  end

  def create
    params[:raffle][:start_date] = DateTime.strptime(params[:raffle][:start_date], "%Y-%m-%d").beginning_of_day
    params[:raffle][:end_date] = DateTime.strptime(params[:raffle][:end_date], "%Y-%m-%d").end_of_day
    
    @raffle = Raffle.new(params[:raffle])
    @chain = @raffle.chain
    if params[:raffle][:start_date] > params[:raffle][:end_date]
      @is_saved = false
    else
      @is_saved = @raffle.save
    end
    @raffles = @chain.raffles.order("id asc")
  end

  def destroy
    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    @raffle.destroy if @raffle
    @raffle = nil
    @raffles = @chain.raffles.order("id asc")
  end

  def activate_deactivate
    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    @raffle.change_status
    @raffles = @chain.raffles.order("id asc")
  end

  def additional_informations
    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    @raffle_additional_informations = @raffle.raffle_additional_informations
  end

  def add_additional_info
    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    @raffle_additional_info = @raffle.raffle_additional_informations.new
  end

  def create_additional_info
    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    @raffle_additional_info = @raffle.raffle_additional_informations.new(params[:raffle_additional_information])
    @is_saved = @raffle_additional_info.save
    @raffle_additional_informations = @raffle.raffle_additional_informations.order("id asc").reload
  end

  def edit_additional_info
    @raffle_additional_info = RaffleAdditionalInformation.find(params[:id])
    @raffle = @raffle_additional_info.raffle
    @chain = @raffle.chain
  end

  def update_additional_info
    @raffle_additional_info = RaffleAdditionalInformation.find(params[:id])
    @raffle = @raffle_additional_info.raffle
    @is_saved = @raffle_additional_info.update_attributes(params[:raffle_additional_information])
    @chain = @raffle.chain
    @raffle_additional_informations = @raffle.raffle_additional_informations.order("id asc").reload
  end

  def remove_additional_info
    @raffle_additional_info = RaffleAdditionalInformation.find(params[:id])
    @raffle = @raffle_additional_info.raffle
    @raffle_additional_info.destroy
    @chain = @raffle.chain
    @raffle_additional_informations = @raffle.raffle_additional_informations.order("id asc").reload
  end

  def steps
    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    # @raffle_step = @raffle.raffle_steps.new
    @raffle_steps = @raffle.raffle_steps

  end

  def new_step
    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    @raffle_step = @raffle.raffle_steps.new
  end

  def create_step
    @raffle = Raffle.find(params[:id])
    @chain = @raffle.chain
    @raffle_step = @raffle.raffle_steps.new(params[:raffle_step])
    @is_saved = @raffle_step.save
    @raffle_steps = @raffle.raffle_steps.reload
  end

  def edit_step
    @raffle_step = RaffleStep.find(params[:id])
    @raffle = @raffle_step.raffle
    @chain = @raffle.chain
  end

  def update_step
    @raffle_step = RaffleStep.find(params[:id])
    @raffle = @raffle_step.raffle
    @is_saved = @raffle_step.update_attributes(params[:raffle_step])
    @chain = @raffle.chain
    @raffle_steps = @raffle.raffle_steps.reload
  end

  def remove_step
    @raffle_step = RaffleStep.find(params[:id])
    @raffle = @raffle_step.raffle
    @raffle_step.destroy
    @chain = @raffle.chain
    @raffle_steps = @raffle.raffle_steps.reload
  end

  def activate_deactivate_step
    @raffle_step = RaffleStep.find(params[:id])
    @raffle = @raffle_step.raffle
    @raffle_step.change_status
    @chain = @raffle.chain
    @raffle_steps = @raffle.raffle_steps.reload
  end

  def participation_histories
    @raffle = Raffle.find(params[:id])
    unless params[:query].blank?
      query = "#{params[:search_by]} = #{params[:query]}" if params[:search_by]=="user_id"
      query = "#{params[:search_by]} LIKE '%#{params[:query]}%'" if params[:search_by]=="user_email"
    end

    if query
      @participation_histories = @raffle.raffle_user_informations.where(query).order("created_at Desc").paginate(page: params[:page], per_page: Setting.pagination.per_page)
    else
      @participation_histories = @raffle.raffle_user_informations.order("created_at Desc").paginate(page: params[:page], per_page: Setting.pagination.per_page)
    end

  end

  def step_histories
    @participation_history = RaffleUserInformation.find(params[:id])
    @raffle = @participation_history.raffle
    @step_histories = @participation_history.raffle_participation_histories.order("created_at Asc")
  end

  def edit_step_users
    @raffle = Raffle.find(params[:id])
  end

  def update_step_users
    @raffle = Raffle.find(params[:id])
    @raffle.attributes = params[:raffle]
    p "00000"*100
    @raffle.errors.add(:selected_step, "Please select step") if @raffle.selected_step.blank?
    confirm = false if params['confirm'].blank?
    confirm = true unless params['confirm'].blank?
    case @raffle.push_type.to_i
      when 1
        if confirm == false
          begin
            user_ids = []
            book = Spreadsheet.open "#{@raffle.xls.path}"
            sheet1 = book.worksheet 0
            sheet1.column(0).each do |row|
              user_ids << row.to_i if !row.to_s.include?(" ")
            end
            @duplicate_count = user_ids.length - user_ids.compact.uniq.length
            user_ids = user_ids.compact.uniq
            @uniq_count =  user_ids.length
            error = false
            err_msg = ""

            if user_ids.size == 0
              @raffle.errors.add(:xls, "There are No Content on the xls files")
            end

            if user_ids.size > 0
              user_ids.each do |uid|
                if uid.to_i == 0
                  error = true
                  @raffle.errors.add(:xls, "Only User ids content are valid in the xls files")
                  break
                end
              end
            end
            REDIS.set "update_user_#{@raffle.id}_#{current_admin.id}", user_ids.to_json
            puts "saving redis"
            @raffle.all_user = false
          rescue => e
            puts "#{e.inspect}"
            @raffle.errors.add(:xls, "Please upload the xls file" )
          end
        else ## it means confirmations is == true
          puts "masuk sini boiiiiii"
          if confirm == true
            puts "masuk sini boiiiiii2"
            is_valid = @raffle.valid?
          else
            @raffle.errors.add(:xls, "Please upload the xls file" )
            is_valid = @raffle.valid?
          end
        end
        @raffle.all_user = false
      when 2
        user_ids = []
        user_ids = @raffle.user_temp_ids
        user_ids = user_ids.squish.gsub("\\r",'').gsub('\\n','').gsub(' ','').split(',')
        @duplicate_count = user_ids.length - user_ids.compact.uniq.length
        user_ids = user_ids.compact.uniq
        @uniq_count =  user_ids.length
        @raffle.errors.add(:user_ids, "Please add user ids" ) if user_ids.blank?
        REDIS.set "update_user_#{@raffle.id}_#{current_admin.id}", user_ids.to_json
        @raffle.all_user = false
      else
        @raffle.errors.add(:push_type, "Please pick type XLS or Manual User Id" )
    end
    p confirm
    is_valid = @raffle.errors.blank?
    if !is_valid || (is_valid && confirm == false)
      @confirmation_box = true if confirm == false && is_valid
      render :edit_step_users
    elsif is_valid && confirm == true
      p "uids"*100
      u_ids = JSON.parse(REDIS.get "update_user_#{@raffle.id}_#{current_admin.id}")
      Delayed::Job.enqueue(RaffleJob.new(u_ids, @raffle.id, @raffle.selected_step))
      redirect_to admin_update_admin_raffle_path(@raffle), :notice => "Raffle users step updated process has been created. It will be processed soon."
    end

  end

  def admin_update
    @raffle = Raffle.find(params[:id])
    @histories = RaffleManualHistory.where(:raffle_id => @raffle.id).order("raffle_manual_histories.group DESC").paginate(:per_page => 20, :page => params[:page])
  end

end