class Admin::PrivacyPoliciesController <  Admin::BaseController

  def index
    @privacy_policies = PrivacyPolicy.all

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /terms/1
  def show
    @privacy_policy = PrivacyPolicy.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /terms/new
  def new
    @privacy_policy = PrivacyPolicy.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /terms/1/edit
  def edit
    @privacy_policy = PrivacyPolicy.find(params[:id])
  end

  # POST /terms
  def create
    @privacy_policy = PrivacyPolicy.new(params[:privacy_policy])

    respond_to do |format|
      if @privacy_policy.save
        format.html { redirect_to admin_privacy_policy_path(@privacy_policy), notice: 'PrivacyPolicy was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /terms/1
  # PUT /terms/1.json
  def update
    @privacy_policy = PrivacyPolicy.find(params[:id])

    respond_to do |format|
      if @privacy_policy.update_attributes(params[:privacy_policy])
        format.html { redirect_to admin_privacy_policy_path(@privacy_policy), notice: 'PrivacyPolicy was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /terms/1
  def destroy
    @privacy_policy = PrivacyPolicy.find(params[:id])
    @privacy_policy.destroy

    respond_to do |format|
      format.html { redirect_to admin_privacy_policies_url }
    end
  end

  def preview
    @content =  params[:content]
    @content = @content

    respond_to do |format|
      format.js { }
      format.html { render :layout => false}
    end
    #render "menus/terms_of_use", :layout => false
  end
  
end
