class Owner::SurveysController < Owner::BaseController
  before_action :find_chain

  def index
  @chain_ids = [current_owner.restaurant_list.first.chain.id]# rescue []
  #if @is_avg == false
  #  survey_id = Survey.have_average(@chain_ids)
  #  return redirect_to all_survey_owner_surveys_path(:id => survey_id)
  # end


    @list_surveys = Survey.where("chain_id in (?)",@chain_ids)
   puts "chain_ids = #{@chain_ids}"
    @restaurants = []

    if !request.xhr? || params[:type] == "table"
      if params[:id]
        @survey = Survey.find params[:id]
        if !@chain_ids.include?(@survey.chain_id) #or @survey.chain_id or != @chain.id
          flash[:notice] = "You re not having to access #{@survey.title} survey !!"
          return redirect_to owner_surveys_path
        end


          @restaurants = current_owner.active_restaurant_list

      else
        @list_surveys.each do |survey|
          @restaurants << survey.restaurant_collections
        end
        #@survey = @list_surveys.first
        @survey = Survey.have_average(@chain_ids)

        @restaurants = current_owner.active_restaurant_list
      end
    else
      @survey = Survey.find params[:id]
      if (!@survey_id_cache.include?(params['id'].to_i) || !@survey_id_cache.include?(params['survey_id'].to_i) ) and !request.xhr?
        survey_id = Survey.have_average(@chain_ids)
        survey_id = survey_id.first.id if survey_id.class == Array and survey_id.size > 0
        return redirect_to all_survey_owner_surveys_path(:id => survey_id)
      end

      @list_surveys.each do |survey|
        @restaurants << survey.restaurant_collections
      end
      if current_owner.chain_owner?
        @restaurants = current_owner.restaurant_list
      else
        @restaurants = current_owner.restaurant_list
      end
    end

   @selected_restaurants = [@restaurants[0]] if @restaurants.size <= 10
   @selected_restaurants = @restaurants[0..2] if @restaurants.size > 10
   @all_restaurants = @restaurants
   @restaurants = @restaurants.paginate(page: params[:page], per_page: Setting.pagination.per_page)


  @tag_list = Tagging.
      where("taggable_type = 'Restaurant' and taggable_id in (select id from restaurants where chain_id = #{current_owner.chain.id} and restaurants.status  IS TRUE)").
      joins(:tag).select("distinct(tags.name)").map(&:name)


  ### searching features

  unless params['restaurant'].blank?
   @restaurants = @restaurants.select{|x|  x.dashboard_display_text.downcase.match(params['restaurant']) }
   puts @restaurants
   @all_restaurants = @restaurants
   puts "aaaaaaaaaaaaaaaaaa"
   @restaurants = @restaurants.paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

  ## tagging
  if !params["tag"].blank? and !params["tag"].include?('--')
    #@tag_filter = "true"
    sql_conditions = " restaurants.chain_id = #{current_owner.chain.id} "
    @all_restaurants = Restaurant.tagged_with(params["tag"],sql_conditions)
    @restaurants = @all_restaurants.paginate(page: params[:page], per_page: Setting.pagination.per_page)
  end

   respond_to do |format|
     format.html{}
     format.js{}
   end
  end

  def search_chart
    @survey = Survey.find params[:survey_id]
    @restaurants = @survey.restaurant_collections

    respond_to do |format|
      format.js {}
    end
  end

  def submit_request_csv
    if (params[:survey_id] || params['survey_id_hidden']) && params[:rotating_id].blank?
       survey = Survey.find(params[:survey_id] || params['survey_id_hidden'])
       Delayed::Job.enqueue(ReportJob::AnswerGeneralJob.new(survey.id, current_owner.email,params), :delayable_type => "export_answer_csv")
    else
       @rotating_group = RotatingGroup.find(params[:rotating_id])
       Delayed::Job.enqueue(ReportJob::AnswerRotatingJob.new(@rotating_group.id, current_owner.email,params), :delayable_type => "export_answer_csv")
    end

    #respond_to do |format|
    #  format.html{ redirect_to :back , :notice => "Please Wait Csv will be sent to your email"}
    #end
  end

  def search
    @list_surveys = Survey.all
    @survey = Surveys.find params[:id]
    @restaurants = @survey.restaurant_collections
    render :template => '/owner/surveys/index.html'
  end

  def request_csv
    if params[:survey_id] && params[:rotating].blank?
      #params[:start_date] = Survey.find(params[:survey_id]).created_at.strftime("%Y-%m-%d")
      #params[:end_date] = (Time.now).strftime("%Y-%m-%d")
      #server = if request.host.to_s.include?('trelevant')
      #           "trelevant"
      #         else
      #           "prelevant"
      #         end
      #puts "=============== server is on #{server}"
      #js = Jasper.new("/reports/#{server}/Survey_Answers", "csv", params, current_owner.email)
      #@path = js.delay.process_file_report
     # survey = Survey.find(params[:survey_id])
     # Delayed::Job.enqueue(ReportJob::AnswerGeneralJob.new(survey.id, current_owner.email), :delayable_type => "export_answer_csv")
    else
     # @rotating_group = RotatingGroup.find(params[:rotating_id])
     # Delayed::Job.enqueue(ReportJob::AnswerRotatingJob.new(@rotating_group.id, current_owner.email), :delayable_type => "export_answer_csv")
    end
    @survey_name =  Survey.find(params[:survey_id]).title rescue nil
    @rotating_name = RotatingGroup.find(params[:rotating_id]).name rescue nil
  end

  def export_survey_dashboard
    unless params['id'].blank?
      survey = Survey.find(params['id'])
      Delayed::Job.enqueue(OwnerJob::SurveyExportJob.new(survey,current_owner),
                         :delayable_type => "export_survey_csv")
      redirect_to all_survey_owner_surveys_path(:id => survey.id) , :notice => "Export is being sent to your email"
    else
      redirect_to :back , :notice => "Survey Not Found !! "
    end
  end

  def search_location_survey

  end


  def all_survey
    puts "here..................................."
    if params['id'].blank? and params['survey_id'].blank?
      puts "------------------- redicet here------------ "
      # if user type on the browser
      survey_id = Survey.have_average(@chain_ids)
      survey_id = survey_id.first.id if survey_id.class == Array and survey_id.size > 0
      return redirect_to all_survey_owner_surveys_path(:id => survey_id)
    end
    @chain_ids = current_owner.chain_ids
    #@surveys = [Survey.find(params['id'])]
    @surveys = Survey.by_chain_ids(@chain_ids)
    conditions_for_res_owner = nil
    conditions_for_res_owner = " AND restaurant_id in (#{current_owner.restaurant_list.map(&:id).join(',')})" if !current_owner.chain_owner?
    survey_ids = @surveys.map(&:id)
    params.merge!(:survey_id => params['id'])  unless params['id'].blank?
    params.merge!(:survey_id => params['survey_id'])  unless params['survey_id'].blank?

    if params[:survey_id].blank?
      total_count = REDIS.get "total_count_page_#{survey_ids}"

      if total_count.blank?
        total_count = SurveysUser.only_show.where("surveys_users.survey_id in (?) #{conditions_for_res_owner}",survey_ids).count
        REDIS.set "total_count_page_#{survey_ids}", total_count
      end

      @survey_users = SurveysUser.only_show.where("surveys_users.survey_id in (?) #{conditions_for_res_owner}",survey_ids).order("surveys_users.guest_count desc,surveys_users.created_at desc").paginate(page: params[:page], per_page: Setting.pagination.per_page,
:total_entries => total_count.to_i)
    else
      ## if user entered the id on the browsers.
      if !@survey_id_cache.include?(params['id'].to_i) and !@survey_id_cache.include?(params['survey_id'].to_i)
        survey_id = Survey.have_average(@chain_ids)
        survey_id = survey_id.first.id if survey_id.class == Array and survey_id.size > 0
        return redirect_to all_survey_owner_surveys_path(:id => survey_id)
      end
      total_count = REDIS.get "total_count_page_res_owner_#{params[:survey_id]}"
      if total_count.blank?
        total_count = SurveysUser.only_show.where("survey_id = ? #{conditions_for_res_owner}",params[:survey_id]).
            order("surveys_users.guest_count desc,surveys_users.created_at desc").count
        REDIS.set "total_count_page_res_owner_#{survey_ids}", total_count
      end
      @survey_users = SurveysUser.only_show.where("survey_id = ? #{conditions_for_res_owner}",params[:survey_id]).
          order("surveys_users.guest_count desc,surveys_users.created_at desc").
          paginate(page: params[:page],
                  per_page: Setting.pagination.per_page,
                  :total_entries =>total_count
      )
      @survey = Survey.find params[:survey_id]
    end

    #@survey_users =  @survey_users.paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

    respond_to do |format|
      format.html
      format.js
      format.csv { render :layout => false }

    end
  end

  def search_all_survey
    conditions_for_res_owner = nil
    conditions_for_res_owner = " AND restaurant_id in (#{current_owner.restaurant_list.map(&:id).join(',')})" if !current_owner.chain_owner?

    params.merge!(:survey_id => params['id'])  unless params['id'].blank?
    unless params[:search][:q].blank?
      params[:search][:chains] = @chain
      @surveys = Survey.by_chain_ids(@chain)
      #@surveys = [Survey.find(params['id'])]
      if params[:survey_id].blank?
        @survey_users = SurveysUser.where("surveys_users.id is not null #{conditions_for_res_owner}").search(params[:search]).includes(:survey)
      else
        @survey_users = SurveysUser.where("survey_id = ? #{conditions_for_res_owner}", params[:survey_id]).search(params[:search]).includes(:survey)
      end
    else
      @chain_ids = current_owner.chain_ids
      #@surveys = Survey.by_chain_ids(@chain_ids)
      @surveys = [Survey.find(params['id'])]
      survey_ids = @surveys.map(&:id)
      if params[:survey_id].blank?
        #@survey_users = SurveysUser.where("surveys_users.survey_id in (?)",survey_ids).includes(:survey, :answers).order("surveys_users.created_at desc")#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
        @survey_users = SurveysUser.where("surveys_users.survey_id in (?) #{conditions_for_res_owner}",survey_ids).includes(:survey, :answers,:receipt => :receipt_transactions).order("surveys_users.created_at desc")#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
      else
        #@survey_users = SurveysUser.where("survey_id = ?",params[:survey_id]).includes(:survey, :answers).order("surveys_users.created_at desc")#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
        @survey_users = SurveysUser.where("survey_id = ? #{conditions_for_res_owner}",params[:survey_id]).order("surveys_users.created_at desc").includes(:survey, :answers, :receipt => :receipt_transactions)#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
        @survey = Survey.find params[:survey_id]
      end
    end
    @survey_users =  @survey_users.paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

    respond_to do |format|
      format.html { }
      format.js { render :all_survey}
    end
  end


  # dashboard 2.3
  def request_guest_relation
    @su = SurveysUser.find params[:id]
    @user = @su.user
    begin
    @rewards = current_owner.chain.rewards.active.dashboard.unexpired
    rescue
      @rewards = current_owner.chain.rewards.active.unexpired
      end
    @is_reward = REDIS.get "guest_relation_setting_reward_#{@chain.first.id}" rescue nil
    @is_points = REDIS.get "guest_relation_setting_points_#{@chain.first.id}" rescue nil
  end


  def request_guest
    status = 1
    if current_owner.has_role?('create')
      status = 1 ## pending
    elsif current_owner.role.to_s.include?('process')
      status = 2 ## processing
    end

    su = SurveysUser.find(params[:id])

    @guest_relation = GuestRelation.new(params[:guest_relation].merge!(
                                            :surveys_user_id => su.id,
                                            :owner_id => current_owner.id ,
                                            :chain_id => su.user.chain_id,
    ### status is set by owner role / permission
    :status => status
    )
    )

    su.update_column(:guest_count, su.guest_count + 1)
    @is_save = false
    @is_save = @guest_relation.save
    if status == 2  and @is_save
      puts "Executing to push reward / points"
      Delayed::Job.enqueue(OwnerJob::GuestRelationJob.new(@guest_relation))
    end
  end

  def process_request
    unless params["request_ids"].blank?
      @ids = params["request_ids"].split('_').delete_if{|x| x.blank?}
      @ids = @ids.delete_if{|x| x.to_i == 0}
      status = 2
      status = 5 if params["mode"] == "reject"
      GuestRelation.update_all("status =  #{status} ","id in(#{@ids.join(',')})" )
      puts "Executing to push reward / points"
      Delayed::Job.enqueue(OwnerJob::GuestRelationJob.new(@ids)) if status == 2  ## if approved
    end
  end

  def new_all_survey
    ## if there are guest relations, then show the guest relations first
    @chain_ids = current_owner.chain_ids
    @surveys = Survey.by_chain_ids(@chain_ids)
    conditions_for_res_owner = nil
    conditions_for_res_owner = " AND surveys_users.restaurant_id in (#{current_owner.restaurant_list.map(&:id).join(',')})" if !current_owner.chain_owner?
    survey_ids = @surveys.map(&:id)

    if params[:survey_id].blank?
      ## union
      ## order by chain owner and restaurant owner

      if current_owner.is_chain_owner?
        order_by = "guest_relations.created_at desc, surveys_users.created_at desc"
        guest_owner = "guest_relations.status != 1 or"
      else
        order_by = "surveys_users.created_at desc"
        guest_owner = "guest_relations.status != 10 or"
      end

      @su = SurveysUser.where("surveys_users.survey_id in (?) and guest_relations.status = 1
#{conditions_for_res_owner}",
                              survey_ids).
          order(order_by).
          includes(:survey, :answers,:guest_relations, :receipt => :receipt_transactions).paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv" and current_owner.is_chain_owner?

      @sur = SurveysUser.where("surveys_users.survey_id in (?) and (#{guest_owner} guest_relations.status is null )
#{conditions_for_res_owner}",
                               survey_ids).
          order("surveys_users.created_at desc").
          includes(:survey, :answers,:guest_relations, :receipt => :receipt_transactions).paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

      if @su.blank?
        @survey_users = @sur
      else
        @survey_users = @su + @sur
      end


    else
      #@survey_users = SurveysUser.where("survey_id = ?",params[:survey_id]).includes(:survey, :answers).order("surveys_users.created_at desc")#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
      @su = SurveysUser.where("survey_id = ? and guest_relations.status = 1 #{conditions_for_res_owner}",params[:survey_id]).
          order("surveys_users.created_at desc").
          includes(:survey, :answers,:guest_relations, :receipt => :receipt_transactions).
          paginate(page: params[:page], per_page: Setting.pagination.per_page)

      @sur = SurveysUser.where("survey_id = ? and (#{guest_owner}  guest_relations.status is null ) #{conditions_for_res_owner}",params[:survey_id]).
          order("surveys_users.created_at desc").
          includes(:survey, :answers,:guest_relations, :receipt => :receipt_transactions).
          paginate(page: params[:page], per_page: Setting.pagination.per_page)
      @survey_users = @su + @sur
      @survey = Survey.find params[:survey_id]
    end

    @survey_users = @survey_users.paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

    respond_to do |format|
      format.html { render :template => 'owner/surveys/all_survey'}
      format.js { render :template => 'owner/surveys/all_survey'}
      format.csv { render :layout => false , :template => 'all_survey'}

    end
  end

  private

  def find_chain
    @chain = current_owner.chain_list rescue nil
    if current_owner.chain_owner?
      @chain_ids = current_owner.chain_list.map(&:id)
    else
      @chain_ids = current_owner.chain_list.map(&:id)
      @restaurant_list = current_owner.restaurant_list
    end

    ## chain survey_ids
    @survey_id_cache = Rails.cache.fetch("survey_ids_cache_db_#{current_owner.id}",
                                           :expires_in => 1.hours) do
      current_owner.chain.surveys.map(&:id)
    end

  end

end
