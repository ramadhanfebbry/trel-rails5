class Owner::SessionsController < Devise::SessionsController
  layout 'login'

  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_navigational_format?

    if self.resource.is_active.eql?(true)
      sign_in(resource_name, resource)
      respond_with resource, :location => after_sign_in_path_for(resource)
    else
      sign_out(resource_name)
      flash[:notice] = '';flash[:alert] = "Your user is inactive, please contact admin to activate it."
      redirect_to owner_log_in_path
    end
  end

  def after_sign_in_path_for(resource)
    owner_greetings_path    
  end

  def after_sign_out_path_for(resource)
    owner_log_in_path
  end
end