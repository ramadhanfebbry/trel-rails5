class Owner::RegistrationsController < DeviseController
  layout 'owner_d2'

  def edit
    @owner = current_owner
  end

  def update
    @owner = Owner.find(current_owner.id)
    if @owner.update_with_password(params[:owner])
      # Sign in the user by passing validation in case their password changed
      sign_in @owner, :bypass => true
      redirect_to root_path, :notice => "Pasword is updated"
    else
      render "edit"
    end
  end
end