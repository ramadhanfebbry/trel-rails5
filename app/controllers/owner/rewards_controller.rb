class Owner::RewardsController < Owner::BaseController
  before_action :find_chain_and_restaurants#, :except => [:index]

  def index
    @greeting = 'Welcome here'    
    @ongoing_rewards = @chain.rewards.regular.unexpired
    @one_time_rewards = ChainRewardEvent.where(:chain_id=>@chain.id).includes(:reward)
  end

  def one_time_chart
    @one_time_rewards = @chain.rewards.one_time.unexpired
  end


  private

  def find_chain_and_restaurants
    @chains = current_owner.chains.includes(:restaurants)
    @chain = current_owner.chains.first
    @restaurants = []
    @chains.each_with_index do |chain|
      @restaurants << chain.restaurants
    end
    @restaurants = @restaurants.flatten
  end

end
