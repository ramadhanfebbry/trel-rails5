class Owner::ActivitiesController < Owner::BaseController
  #layout "owner"
  #around_filter :select_shard
  before_action :find_restaurant , :except => [:generate_restaurant]
  #caches_action :generate_restaurant,:print_row_tables

  def index
    @tag_list = Tagging.
        where("taggable_type = 'Restaurant' and taggable_id in (select id from restaurants where chain_id = #{@chain.id} and restaurants.status is TRUE)").
        joins(:tag).select("distinct(tags.name)").map(&:name)

    ## tagging
    if !params["tag"].blank? and !params["tag"].include?('--')
      #@tag_filter = "true"
      @tag_filter = "true"
      sql_conditions = " restaurants.chain_id = #{current_owner.chain.id} "
      @restaurants = Restaurant.tagged_with(params["tag"],sql_conditions)
      @filter_tag = params["tag"]
    end

    respond_to do |format|
      format.html {@restaurants = @restaurants.paginate(page: params[:page], per_page: Setting.pagination.per_page)} # show.html.erb
      format.csv { }
    end
  end

  def show_detail_receipt
    week = params[:id]
    @date = Time.zone.now - (week.to_i).days
    @restaurant = Restaurant.find params[:restaurant_id]

    #@receipts = Receipt.dashboard_receipt_activity(@restaurant.id,(Time.zone.now - week.to_i.days).to_date)#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
    #begin
    #  rc = ReceiptTransaction.where(:restaurant_id => @restaurant.id).last.receipt
    #  if rc.is_receipt_image?
    #    receipt_images = true
    #  else
    #    receipt_images = false
    #  end
    #rescue
    #  receipt_images = false
    #end
    #
    #if receipt_images == false
    # @receipts = Receipt.receipt_date_dashboard_receipt_activity(@restaurant.id,(Time.zone.now - week.to_i.days).to_date)#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
    #else
    # @receipts =  Receipt.new_dashboard_receipt_activity(@restaurant.id,(Time.zone.now - week.to_i.days).to_date)#.paginate(page: params[:page], per_page: Setting.pagination.per_page)
    #end
    @receipts = Receipt.receipt_date_coalesce_dashboard_receipt_activity(@restaurant.id,
                                                                         (Time.zone.now -
                                                                             week.to_i.days).to_date)#.paginate(page: params[:page], per_page: Setting.pagination.per_page)

    respond_to do |format|
      format.html {render :template => "owner/activities/show_detail"} # show.html.erb
      format.csv { }
    end
  end

  def show_online_order
    @comment =  Receipt.find(params[:id]).comment

    unless @comment.blank?
      render :layout => false
    end
  end

  def show_olo_order
    @detail = OloOrderDetail.where(:receipt_id => params[:id]).first rescue nil
    @order = OloOrder.find @detail.olo_order_id rescue nil
    @receipt = Receipt.find(@detail.receipt_id)
    @transaction = @receipt.last_transaction rescue nil

    render :layout => false
  end

  def show_receipt_barcode
    @xpient_template = false
    @receipt = Receipt.find(params[:id])
    if @receipt.is_receipt_barcode
      @transaction = @receipt.last_transaction rescue nil
      @pos_check_upload = @receipt.pos_check_upload rescue nil
      if @pos_check_upload.is_xpient_check
        @xpient_template = true
        xpient_check = XpientCheck.new(YAML.load(@pos_check_upload.xml_data))
        @payment_total = xpient_check.total rescue 0
        @short_code = xpient_check.short_code
        @check_id = xpient_check.pos_order_id
        @menu_items = xpient_check.line_items rescue []
      else
        @pos_xml = PosReceiptXml.new(@pos_check_upload.xml_data) rescue nil
        @payment_total = @pos_xml.total rescue 0
        @menu_items = @pos_xml.xml_in_nokogiri_format.search("menu-item") rescue []
        @menu_items_hash = {}
        @menu_items.map{|a| @menu_items_hash[a.attr("id").to_s] = (a.search("name").text rescue nil)}
        check_id = @pos_xml.check_id
        seq_num = @pos_xml.seq_num
        @reward_dicounts = RewardDiscount.where(:check_id => check_id, :seq_num =>  seq_num, :restaurant_id => @pos_check_upload.pos_location.restaurant_id)
        @discount_total = @pos_xml.discount_total
        @direct_discounts = @pos_xml.xml_in_nokogiri_format.search("discount") rescue []
      end
    elsif @receipt.is_ncr_receipt
      @transaction = @receipt.last_transaction rescue nil
      @ncr_data_receipt = @receipt.ncr_data_receipt
      json_encode = @ncr_data_receipt.json_data.gsub("=>",":").gsub("nil", "0") rescue nil
      @receipt_data_in_json = ActiveSupport::JSON.decode(json_encode)
    end
    render :layout => false
  end


  def generate_restaurant
    @restaurant_ids = params[:restaurant_id]
    @restaurants = Restaurant.using(:shard_one).where("id in(?)", @restaurant_ids)
    #steps_count = @restaurants.in_groups_of(5).size
    #@step = 0 if params[:step].blank?
    #@step ||= params[:step]

    #if params[:step] == steps_count
    #  render :nothing => true
    #end
    end

  def generate_restaurant_rewards
    @restaurant_ids = params[:restaurant_id]
    @restaurants = Restaurant.where("id in(?)", @restaurant_ids)
  end

  def print_row_tables_rewards
    @restaurant_ids = params[:restaurant_id]
    @restaurants = Restaurant.where("id in(?)", @restaurant_ids)
  end

  def print_row_tables
    @restaurant_ids = params[:restaurant_id]
    @restaurants = Restaurant.where("id in(?)", @restaurant_ids)
  end

  def show_detail_reward
    week = params[:id]
    @week = week
    @restaurant = Restaurant.find params[:restaurant_id]
    #@rewards = RewardTransaction.by_restaurant_and_date(@restaurant.id, Time.zone.now - week.to_i.days).order("date(reward_transactions.created_at) asc, date_part('hour', reward_transactions.created_at) asc, date_part('minute', reward_transactions.created_at) asc")
    date_zone = "date(TIMEZONE('UTC', reward_transactions.created_at) AT TIME ZONE 'EST')"
    @rewards = RewardTransaction.select("
          distinct on(date(reward_transactions.created_at), reward_transactions.reward_id,
reward_transactions.restaurant_id,
 reward_transactions.user_id)  reward_transactions.*").
        where("restaurant_id = ? and redeeming IS FALSE AND #{date_zone} = date(?)",
     @restaurant.id,Time.zone.now.in_time_zone('EST') - week.to_i.days).
        order("date(reward_transactions.created_at) asc")

    @date = Time.zone.now - week.to_i.days

    respond_to do |format|
      format.html { } # show.html.erb
      format.csv { }
    end
  end

  def show_popup_reward_activities
    week = params[:id]
    hour = params[:hour]
    reward_id = params[:reward_id]

    @restaurant = Restaurant.find params[:restaurant_id]
    @rewards = RewardTransaction.by_restaurant_and_date(@restaurant.id, (Time.zone.now - week.to_i.days).to_date).
      where("date_part('hour',reward_transactions.created_at) = ? and reward_id = ? ", hour.to_i, reward_id)#.joins(:restaurant_reward)
    render :layout => false
  end

  def rewards
    @rewards = @chain.rewards.active
    @tag_list = Tagging.
        where("taggable_type = 'Restaurant' and taggable_id in (select id from restaurants where chain_id = #{@chain.id} and restaurants.status is TRUE)").
        joins(:tag).select("distinct(tags.name)").map(&:name)

    if !params["tag"].blank? and !params["tag"].include?('--')
      #@tag_filter = "true"
      @tag_filter = "true"
      sql_conditions = " restaurants.chain_id = #{current_owner.chain.id} "
      @restaurants = Restaurant.tagged_with(params["tag"],sql_conditions)
      @filter_tag = params["tag"]
    end

    respond_to do |format|
      format.html {@restaurants = @restaurants.paginate(page: params[:page], per_page: Setting.pagination.per_page)} # show.html.erb
      format.csv { }
    end
  end

  def export_rewards_redeemed
    @is_save = true
    err = nil
    #if params["hour_start_date"]["hours(4i)"].to_i > params["hour_end_date"]["hours(4i)"].to_i
    #  err = "Start Hour Cannot be Greater than End date Hour"
    #end
    if  !params["start_date"].blank? and !params['end_date'].blank? and err == nil
      Delayed::Job.enqueue(
          # OwnerJob::RewardRedeemedJob.new(
          #     @chain,
          #     current_owner,
          #     params),
          #     :delayable_type => "export_reward_redeemed_csv")
          OwnerJob::RewardRedeemedNoDupJob.new(
              @chain,
              current_owner,
              params),
              :delayable_type => "export_reward_redeemed_csv")
    else
      @err = err
      @is_save = false
      if params["start_date"].blank? || params['end_date'].blank?
        @err = @err.to_s + "Date Cannot be blank"
      end
    end
  end

  def export_activities
    @is_save = true
    err = nil
    #if params["hour_start_date"]["hours(4i)"].to_i > params["hour_end_date"]["hours(4i)"].to_i
    #  err = "Start Hour Cannot be Greater than End date Hour"
    #end
    if  !params["start_date"].blank? and !params['end_date'].blank? and err == nil
      Delayed::Job.enqueue(
          OwnerJob::ActivitiesNumberExportJob.new(
              @chain,
              current_owner,
              params),
          :delayable_type => "export_activities_csv")
    else
      @err = err
      @is_save = false
      if params["start_date"].blank? || params['end_date'].blank?
        @err = @err.to_s + "Date Cannot be blank"
      end
    end
  end

  def search_activities
     @restaurants = current_owner.restaurant_list.where('lower(dashboard_display_text) like ?',"%#{params['restaurant'].downcase}%").paginate(page: params[:page], per_page: Setting.pagination.per_page)
     @start_date = params['start_date'].blank? ? Time.zone.now - 10.days : params['start_date']
     @end_date = params['end_date'].blank? ? Time.zone.now : params['end_date']
     unless params['from_reward'].blank?
     @from_rewards = true
     else
       @from_rewards = false
       end
  end

  private
  def select_shard(&block)
    puts "SELECT SHARD Session--------------------------------- "
    Octopus.using(:shard_one, &block)  if ENV["S3_BUCKET"]  != "trelevant"
  end

  def find_restaurant
    #current_owner = Owner.find_by_email("gp@relevantmobile.com")
    #if current_owner.is_owner?
    #  @chains = current_owner.chains.includes(:restaurants)
    #  @chain = current_owner.chains.first
    #  @restaurants = []
    #  @chains.each_with_index do |chain|
    #    @restaurants << chain.restaurants
    #  end
    #  @restaurants = @restaurants.flatten
    #else
    #  @chains = [current_owner.chain_obj]
    #  @chain = current_owner.chain_obj
    #  @restaurants = current_owner.restaurants
    #end

    @restaurants = current_owner.restaurant_list
    @chain = current_owner.chain
    @chains = [@chain]
    @is_manager = current_owner.is_manager?
  end
end