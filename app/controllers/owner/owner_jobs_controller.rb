class Owner::OwnerJobsController < Owner::BaseController
  before_action :check_permission, :check_chain_owner

  def index
    session['skip'] = params['skip']
    restaurant_owner_ids = current_owner.restaurant_list.map(&:id)
    filter_owner = nil
    if !current_owner.is_chain_owner?
      filter_owner = "and restaurant_users.restaurant_id in (#{restaurant_owner_ids.join(',')})"
    else
      filter_owner = ""
    end

    @jobs = OwnerJob.select('distinct(owner_jobs.*)').
        where("owner_id = ? or (individual = ? and chain_id = ? #{filter_owner}) ",
                           current_owner.id, true, current_owner.chain.id).
        joins("left join restaurant_users on individual_user_id = restaurant_users.user_id").
        order('created_at desc').
        paginate(page: params[:page], per_page: Setting.pagination.per_page,
        :count => {:group => 'owner_jobs.id'})
  end

  def new
    @step = params[:step].to_i
    @next_step = @step + 1
    @previous_step = @step - 1
    @previous_step = 0 if @previous_step == -1
    @first_time = params[:first_time]
    @chain = current_owner.chain
    @users_count = @chain.users.count

    #if @step == 2
      ## populate question percentage
      @question1 = @chain.dashboard_questions.where('order_number = 1').first
      @question2 = @chain.dashboard_questions.where('order_number = 2').first
    #end
    if @step == 4
      a = params['user_count'].split('_')
      chain = current_owner.chain
      #restaurant_ids = chain.restaurants.select('restaurants.id')
      #if a.last ##
      restaurant_ids = chain.restaurants.map(&:id)
      restaurant_id_sql = restaurant_ids.join(',')

        activity_days = a.last.to_i
       wek = Time.now - activity_days.weeks
        #@user_preview_activity = RestaurantUser.select('count(distinct(user_id)) as count').
        #    where("restaurant_id in (?) and date(last_active) >= date(?)",
        #                              restaurant_ids, Time.now - activity_days.weeks).first.count.to_i

      @user_preview_percentage =  User.find_by_sql("select count(users.*) from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date <= date('#{wek}')
").first.count.to_i

      #elsif a.first
        #@user_preview_percentage = RestaurantUser.select('count(distinct(user_id)) as count').
        #    where("restaurant_id in (?) and date(last_active) <= date(?)",
        #                              restaurant_ids, Time.now - a.first.to_i.weeks).first.count.to_i
        #puts "yyyyyyyyyyyyyyyyyyyyyyyyyyy"
      #end

      @user_preview_activity =
          User.find_by_sql("select count(users.*) from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date > date('#{(Time.now - a.first.to_i.weeks).to_date}')
").first.count.to_i

    end
    @rewards = current_owner.chain.rewards.dashboard.active.unexpired
  end

  def search_job_type
    @email = params['email']
    @filter_job = params[:filter_job]
    restaurant_owner_ids = current_owner.restaurant_list.map(&:id)
    filter_owner = nil
    if !current_owner.is_chain_owner?
      filter_owner = "and restaurant_users.restaurant_id in (#{restaurant_owner_ids.join(',')})"
    else
      filter_owner = ""
    end
    p "loggg--- searching"

    p @email
    p @filter_job

    filter_query = nil
    case @filter_job.downcase
      when "all"
        p "masuk sini"
        unless @email.blank?
          filter_query = "AND individual_email like '%#{@email}%'"
        else
          filter_query = "AND owner_id = #{current_owner.id}
                    OR (individual = 't' and chain_id = #{current_owner.chain.id} #{filter_owner})"
          end
      when "group"
        p "masuk sini 1"
        filter_query = "AND individual = false AND owner_id = #{current_owner.id}"
      when "individual",""
        p "masuk sini 2"
        unless @email.blank?
          filter_query = "And individual_email like '%#{@email}%'  AND individual = 't' #{filter_owner}"
        else
          filter_query = "AND (individual = 't' and chain_id = #{current_owner.chain.id}) #{filter_owner}"
        end
    end
    p filter_owner
    p filter_query

    @jobs = OwnerJob.select('distinct(owner_jobs.*)').
        where("chain_id = ? #{filter_query}",
                           current_owner.chain.id).
        joins("left join restaurant_users on individual_user_id = restaurant_users.user_id").
        order('created_at desc').
        paginate(page: params[:page], per_page: Setting.pagination.per_page,
                 :count => {:group => 'owner_jobs.id'})
  end

  def create_individual
    @error_message = "";@reward_id = nil;@points = nil
    @validate = false
    @email = params['user_email'].strip
    @reward_id = params['owner_job']['reward_id'] if !params['owner_job']['reward_type'].blank? and params['owner_job']['reward_type'] == "1"
    @points = params['owner_job']['points'] if !params['owner_job']['reward_type'].blank? and params['owner_job']['reward_type'] == "2"
  #  puts "xxxxxxxxxxxxxxxxxxxxxxxxxx"
  #  puts params['owner_job']['reward_type']
  #  puts "points ==== #{@points}"
    params['owner_job']['points'] = @points
    params['owner_job']['reward_id'] = @reward_id
    @validate = true if !@email.blank? and (!@reward_id.blank? or (!@points.blank? and @points.to_i > 0))

    @error_message += " Who would you like to reach?" if @email.blank?
    @error_message += " | Please choose a reward or enter points to send" if @reward_id.blank? or @points.blank?
    @error_message += " | Please choose a reward or enter points to send" if @points.blank? and !@error_message.include?('reward')



    if @validate.eql?(true)
      user =  User.where("lower(email) = ? and chain_id =?", @email.downcase,current_owner.chain.id).first
      @oj = OwnerJob.new(params["owner_job"])
      @oj.individual_email = @email
      @oj.chain_id = current_owner.chain.id
      @oj.owner_id = current_owner.id
      @oj.total_users = 1
      @oj.description = "#{@points}  Points Successfully Pushed" if !@points.blank?
      @oj.description = "Reward: #{Reward.find(@reward_id).name} Successfully Pushed" if !@reward_id.blank?
      @oj.individual = true
      @oj.job_type = OwnerJob::TYPES["User"]
      @oj.individual_user_id = user.id unless user.blank?

      if user.blank?      # if failed
        @oj.description = "Failed to send #{@points} points" if !@points.blank?
        @oj.description = "Failed to send #{Reward.find(@reward_id).name} reward" if !@reward_id.blank?
        @oj.status = 4 #failed no users found
        @oj.save
        OwnerMailer.individual_user_not_found(current_owner,@email).deliver!
      else
        belong_to_restaurant = true
        if !current_owner.is_chain_owner? ## if restaurant owner, only check that the user is in that restaurant
          belong_to_restaurant = false
          restaurant_user_ids = user.restaurant_users.map(&:restaurant_id)
          restaurant_owner_ids = current_owner.restaurant_list.map(&:id)

          restaurant_owner_ids.each do |re|
             if restaurant_user_ids.include?(re)
               belong_to_restaurant = true
               break
             end
          end
        end

        if user.active == true  and belong_to_restaurant
         @oj.save
         @oj.individual_execution
        else
          if belong_to_restaurant == true
            @oj.description = "Failed to send #{@points} points , #{@email} has been deactivated" if !@points.blank?
            @oj.description = "Failed to send , #{@email} has been deactivated" if !@reward_id.blank?
          else
            @oj.description = "Failed to send #{@points} points / #{@email} is not belong to your restaurant" if @points
            @oj.description = "Failed to send #{Reward.find(@reward_id).name} reward / #{@email} not belong to your restaurant" if !@reward_id.blank?
          end
          @oj.status = 4 #failed no users found
          @oj.save
          OwnerMailer.individual_user_not_found(current_owner,@email).deliver!
        end
      end

    end
  end

  def new_miss_you
    @chain = current_owner.chain
    @definition = @chain.task_definition
  end

  def create_miss_job
    desc = params['owner_job']['description']
    chain = Chain.find params['chain_id']
    @success = OwnerJob.create_miss_you_task(chain,desc,current_owner.id)

    if @success.class != Hash
      @jobs = OwnerJob.where(:owner_id => current_owner.id).order('created_at desc').
          paginate(page: params[:page], per_page: Setting.pagination.per_page)
    end
  end

  def search_users
    chain = current_owner.chain
    restaurant_ids = chain.restaurants.map(&:id)
    restaurant_id_sql = restaurant_ids.join(',')
    activity_days = params["activity_days"]
    if params["activity_days"].blank?
      #unless params["min"].blank? and params['max'].blank?
      unless params["percentage"].blank?
        wek = (Time.now - params["percentage"].to_i.weeks)
        #@users = RestaurantUser.select('count(distinct(user_id)) as count').
        #    where("restaurant_id in (?) and date(last_active) <= date(?)",
                                      #restaurant_ids, (Time.now - params["percentage"].to_i.weeks).to_date)
        #@users = @users.first.count.to_i
         @users =  User.find_by_sql("select count(users.*) from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date <= date('#{wek}')
").first.count.to_i

      end
    else
      chain = current_owner.chain
      activity_days = params["activity_days"].to_i

      @users =
          User.find_by_sql("select count(users.*) from (
select user_id , date(max(max)) from (
select user_id, max from (
 SELECT user_id, max(last_active) FROM restaurant_users
 inner join users on users.id = restaurant_users.id
 WHERE users.active = 't ' and
 (restaurant_id in (#{restaurant_id_sql})) group by user_id
 ) as s
UNION
select user_id, max(created_at) from reward_transactions where
restaurant_id in(#{restaurant_id_sql})
group by user_id
) as x group by user_id ) as mm
inner join users on users.id = mm.user_id where users.active = 't' and users.chain_id = #{chain.id}
and date > date('#{(Time.now - activity_days.to_i.weeks).to_date}')
").first.count.to_i
    end
  end

#  def search_users
#    chain = current_owner.chain
#    restaurant_ids = chain.restaurants.map(&:id)
#    if params["activity_days"].blank?
#      #unless params["min"].blank? and params['max'].blank?
#      unless params["percentage"].blank?
#        wek = (Time.now.utc - params["percentage"].to_i.weeks)
#        #@users = RestaurantUser.select('count(distinct(user_id)) as count').
#        #    where("restaurant_id in (?) and date(last_active) <= date(?)",
#                                      #restaurant_ids, (Time.now - params["percentage"].to_i.weeks).to_date)
#        #@users = @users.first.count.to_i
#         @users =   RestaurantUser.find_by_sql("
#          select distinct x.user_id from (
#          select distinct(s.user_id) from (
#          SELECT user_id, max(last_active) FROM restaurant_users
#          inner join users on users.id = restaurant_users.id
#          WHERE users.active = 't ' and
#          (restaurant_id in (#{restaurant_ids.join(',')})) group by user_id
#          ) as s where
#         date(max) <= date('#{wek}')
#         UNION
#
#        select distinct(user_id) from reward_transactions
#        inner join users on users.id = reward_transactions.user_id
#         where users.active = 't' and
#        restaurant_id in(#{restaurant_ids.join(',')}) and date(reward_transactions.created_at) <= date('#{wek}')
#           ) as x
#          ").count
#
#      end
#    else
#      chain = current_owner.chain
#      activity_days = params["activity_days"].to_i
#     # @users = RestaurantUser.select('count(distinct(user_id)) as count').
#     #     where("restaurant_id in (?) and date(last_active) > date(?)",
#     #                     restaurant_ids, (Time.now.utc - activity_days.weeks).to_date)
#     # @users = @users.first.count.to_i
#
#      @users =   RestaurantUser.find_by_sql("
#       select distinct x.user_id from (
#      select  distinct(s.user_id) from (
#SELECT user_id, max(last_active) FROM
#restaurant_users    inner join users on users.id = restaurant_users.id
#WHERE users.active = 't' and
#(restaurant_id in (#{restaurant_ids.join(',')})) group by user_id
#) as s where date(max) >= date('#{(Time.now.utc - activity_days.weeks).to_date}')
#UNION
#
#        select distinct(user_id) from reward_transactions where
#        restaurant_id in(#{restaurant_ids.join(',')}) and date(created_at) >= date('#{(Time.now.utc - activity_days.weeks).to_date}')
#           ) as x
#
#").count
#    end
#  end

  def miss_you
    @chain = current_owner.chain
    @definition = @chain.task_definition
  end


  #def search_users
  #  chain = current_owner.chain
  #  puts "params-------------- #{params["activity_days"]}"
  #  if params["activity_days"].blank?
  #    #unless params["min"].blank? and params['max'].blank?
  #    unless params["percentage"].blank?
  #      percentage = params['percentage'].to_i
  #      @users = 0
  #      @users = OwnerJob.percentage_users(chain)
  #      @users = ((percentage.to_f / 100.to_f) * @users).to_f.ceil.to_i if @users > 10 ## get the percentage from all users
  #    end
  #  else
  #    chain = current_owner.chain
  #    restaurant_ids = chain.restaurants.map(&:id)
  #    activity_days = params["activity_days"].to_i
  #    @users = RestaurantUser.where("restaurant_id in (?) and date(last_active) >= date(?)",
  #                        restaurant_ids, Time.now - activity_days.days).count
  #
  #    #if @users.size >0
  #    #  @users =  User.where("id not in(?) and chain_id = ?", @users,chain.id).select('id, email')
  #    #else
  #    #  @users =  User.where("chain_id = ?", chain.id).select('id, email')
  #    #end
  #    #puts "search users ================================"
  #    #puts "users ================================ #{@users}"
  #    #puts "users ================================ #{@users.size}"
  #  end
  #end

  def create
    unless validate_form.blank?
      render :action => 'new'
    else
      date = params["date"]
      min = params["time_stamp"]["(5i)"].to_i
      hour = params["time_stamp"]["(4i)"].to_i
      puts "hour = #{hour}"
      hour = (hour + (Time.zone.now.strftime('%z').to_i * -1 / 100))
      plus_day = 0

      if hour > 24
        hour = hour - 24
        plus_day = 1
      end
      date = date.to_time.change(:hour => hour, :min => min)
      date = date + plus_day.days
      push_phone = !params['owner_job']["push_phone"].blank?
      push_email = !params['owner_job']["push_email"].blank?

      @job = OwnerJob.new(params[:owner_job].merge!(:owner_id => current_owner.id,
                                                    :status => 1, :executed_at => date,
                                                    :push_phone => push_phone,
                                                    :push_email => push_email
                          )
      )
      @job.owner_scheduler = OwnerScheduler.set_scheduler(params)

      if @job.save! == false
        render :action => 'new'
      else
        @jobs = OwnerJob.where(:owner_id => current_owner.id).order('created_at desc').
            paginate(page: params[:page], per_page: Setting.pagination.per_page)
      end
    end
  end

  def set_question

  end

  def completed_job
    @job = OwnerJob.find(params[:id])
    @history = @job.owner_job_history
  end

  private

  def check_permission
    if Owner.owner_chain_take_action?(current_owner) == "OFF"#owner_chain_take_action?#Owner.chain_take_action?(current_owner.chain) == "OFF"
      return render :status => :forbidden, :text => "You Dont have access"
    end
  end

  def validate_form
    @error = []
    if params[:commit] == "Ok"
      owner_job_params = params["owner_job"]
      if owner_job_params[:description].blank?
        @error << "Please insert the description, check the 1st step"
      end
      case owner_job_params[:job_type]
        when "1"
          if owner_job_params["percentage"].blank? or owner_job_params["amount_min"].blank? or owner_job_params["amount_max"].blank?
            @error << "All the field must be inserted.., check the 2nd step"
          end
        when "2"
        when "3"
          @error << nil
      end

      case owner_job_params[:reward_kind]
        when "1"
          @error << "Please select the reward check the 3rd step" if params[:reward_id].blank?
        when "2"
          @error << "Please Insert the point check the 3rd step" if params[:points].blank?
        when "3"
          @error << nil
      end
    end
  end

  def individual
    @rewards = current_owner.chain.rewards.dashboard.active.unexpired
    @chain = current_owner.chain
  end

  def check_chain_owner
    if current_owner.role_id == 1  || current_owner.role_id.blank?
      return true
    #else
    #  puts "Owner is her"
    #  render :text => "You dont have access to this pages. Only chain owner."
    end
  end
end