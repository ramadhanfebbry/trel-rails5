class Owner::PasswordsController < Devise::PasswordsController
  layout 'login'
  prepend_before_action :require_no_authentication
  # Render the #edit only if coming from a reset password email link
  append_before_action :assert_reset_token_passed, :only => :edit

  # GET /resource/password/new
  def new
    build_resource({})
  end

  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)

    if successfully_sent?(resource)
      respond_with({}, :location => ENV["AFTER_RESET_PWD_OWNER_URL"])
    else
      respond_with(resource)
    end
  end

  # GET /resource/password/edit?reset_password_token=abcdef
  def edit
    self.resource = resource_class.new
    resource.reset_password_token = params[:reset_password_token]
  end

  # PUT /resource/password
  def update
    self.resource = resource_class.reset_password_by_token(resource_params)

    if resource.errors.empty?
      flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
      set_flash_message(:notice, flash_message) if is_navigational_format?
      sign_in(resource_name, resource)
      ldap = Ldap.new
      ldap.update_owner_password(resource, request.request_parameters[:owner][:password])
      respond_with resource, :location => ENV["AFTER_RESET_PWD_OWNER_URL"]
    else
      respond_with resource
    end
  end

  protected

  # The path used after sending reset password instructions
  def after_resetting_password_path_for(resource)
    #Devise.sign_in_after_reset_password ? after_sign_in_path_for(resource) : new_session_path(resource_name)
    ENV["AFTER_RESET_PWD_OWNER_URL"]
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    ENV["AFTER_RESET_PWD_OWNER_URL"]
  end

  # Check if a reset_password_token is provided in the request
  def assert_reset_token_passed
    if params[:reset_password_token].blank?
      set_flash_message(:error, :no_token)
      redirect_to new_session_path(resource_name)
    end
  end

end