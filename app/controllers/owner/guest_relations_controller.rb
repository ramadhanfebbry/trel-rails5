class Owner::GuestRelationsController < Owner::BaseController
  layout "owner_d2"

  def index
    @chain_ids = current_owner.chain_ids
    @chain = current_owner.chain
    @surveys = Survey.by_chain_ids(@chain_ids)
    survey_ids = @surveys.map(&:id)
    @survey_users = GuestRelation.by_chain(@chain.id)


    @survey_users = @survey_users.paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

    respond_to do |format|
      format.html
      format.js
      format.csv { render :layout => false }

    end
  end

  def check_status_relation
    @readonly = true unless params[:read].blank?
    @gr = GuestRelation.find params[:id]
    begin
      @rewards = Owner.find(@gr.owner_id).chain.rewards.dashboard.active.unexpired
      rescue
      @rewards = Owner.find(@gr.owner_id).chain.rewards.active.unexpired
    end
    @is_reward = REDIS.get "guest_relation_setting_reward_#{@gr.chain_id}"
    @is_points = REDIS.get "guest_relation_setting_points_#{@gr.chain_id}"

    if current_owner.can_approve? == false
      return render :nothing => true
    end
  end

  def update_status_relation
    @gr = GuestRelation.find params[:id]
    @mode = params["mode_type"]
    case @mode
      when "approve"
        @gr.update_column(:status, 2)
      when "reject"
        @gr.update_column(:status, 5)
      when "edit"
        @gr.update_column(:status, 2)
        su = @gr.surveys_user
        @gr.update_attributes(params[:guest_relation].merge!(
                                  :surveys_user_id => su.id,
                                  :approved_owner_id => current_owner.id,
                                  :owner_id => current_owner.id,
                                  ### status is set by owner role / permission
                                  :status => 2))
    end
    puts "Executing to push reward / points"
    Delayed::Job.enqueue(OwnerJob::GuestRelationJob.new(@gr))    if @gr.status == 2 ## if approved
  end

  def search_status_relation
    @chain_ids = current_owner.chain_ids
    @chain = current_owner.chain
    @surveys = Survey.by_chain_ids(@chain_ids)
    conditions_for_res_owner = nil
    conditions_for_res_owner = " AND surveys_users.restaurant_id in (#{current_owner.restaurant_list.map(&:id).join(',')})" if !current_owner.chain_owner?

    if params[:survey_id].blank?
      survey_ids = @surveys.map(&:id)
    else
      survey_ids = params[:survey_id].to_i
    end

    conditions = "AND (lower(restaurants.name) like '%#{params['search']['q'].downcase}%' OR lower(users.email) like '%#{params['search']['q'].downcase}%')" if params['search'] and params['search']['q']
    conditions = "AND lower(restaurants.name) like '%#{params['search']['q'].downcase}%' " if params['search'] and params['search']['q'] and current_owner.role and !current_owner.role.include?('email')
    puts params['status'].to_i

    if params['status'].to_i == 0 and params['search']['from'].blank? and params['search']['to'].blank?  ## all surveys

      ## union
      ## order by chain owner and restaurant owner
      if current_owner.is_chain_owner?
        order_by = "guest_relations.created_at desc, surveys_users.created_at desc"
        guest_owner = "guest_relations.status != 1 or"
      else
        order_by = "surveys_users.created_at desc"
        guest_owner = "guest_relations.status != 10 or"
      end

      @su_all = SurveysUser.where("
      (surveys_users.survey_id in (?)) and guest_relations.status = 1 #{conditions} #{conditions_for_res_owner}",
                                        survey_ids).
          order(order_by).search(params['search'].except('q')).
          includes(:survey,:user,:restaurant, :answers,:guest_relations, :receipt => :receipt_transactions).paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv" and current_owner.is_chain_owner?

      @su_req =  SurveysUser.where("surveys_users.survey_id in (?) and  (#{guest_owner} guest_relations.status is null  )
                                         #{conditions_for_res_owner} #{conditions} ",
                                        survey_ids).
          order(" surveys_users.created_at desc").
          includes(:survey,:user,:restaurant, :answers,:guest_relations, :receipt => :receipt_transactions).paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"
      if @su_all.blank?
        @survey_users =  @su_req
      else
        @survey_users = @su_all + @su_req
      end

    else
      puts params['status'].to_i
      if params["search"]["from"] != "" && params["search"]["to"] != "" and params['status'].to_i == 0
        @survey_users = SurveysUser.includes(:guest_relations, :user,:restaurant ).
            where("surveys_users.survey_id in (?)
                   AND users.email LIKE ?
                   AND date(surveys_users.created_at) BETWEEN date(?) AND date(?)   #{conditions} #{conditions_for_res_owner}",
                  survey_ids,
                  "%#{params[:email]}%",
                  params[:search][:from], params[:search][:to]).paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

      elsif params["search"]["from"] != "" && params["search"]["to"] != "" and params['status'].to_i != 0
        @survey_users = SurveysUser.includes(:guest_relations, :user,:restaurant ).
            where("surveys_users.survey_id in (?) and (guest_relations.chain_id = ?) AND (guest_relations.status = ? )
                   AND users.email LIKE ?
                   AND date(surveys_users.created_at) BETWEEN date(?) AND date(?)   #{conditions} #{conditions_for_res_owner}",survey_ids,
                  @chain.id, params[:status], "%#{params[:email]}%",
                  params[:search][:from], params[:search][:to]).paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

      elsif params['search']['from'] != ""
        @survey_users = SurveysUser.includes(:guest_relations , :user,:restaurant).
            where("surveys_users.survey_id in (?) and guest_relations.chain_id = ?
                 AND guest_relations.status = ?
                AND users.email LIKE ? AND
                date(surveys_users.created_at) = date(?) #{conditions} #{conditions_for_res_owner} ",survey_ids, @chain.id, params[:status],
                  "%#{params[:email]}%", params[:search][:from]).paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

      elsif params['search']['to'] != ""
        @survey_users = SurveysUser.includes(:guest_relations , :user,:restaurant).
            where("surveys_users.survey_id in (?) and guest_relations.chain_id = ?
                 AND guest_relations.status = ?
                AND users.email LIKE ? AND
                date(guest_relations.created_at) = date(?) #{conditions} #{conditions_for_res_owner} ",survey_ids, @chain.id, params[:status],
                  "%#{params[:email]}%", params[:search][:to]).paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

      else
        @survey_users = SurveysUser.includes(:guest_relations , :user,:restaurant).
            where("surveys_users.survey_id in (?) and guest_relations.chain_id = ? AND guest_relations.status = ?
AND users.email LIKE ? #{conditions} #{conditions_for_res_owner}",survey_ids, @chain.id, params[:status], "%#{params[:email]}%").paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"
      end

    end


    #@search_email = params[:email]
    @survey_users = @survey_users.paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"

    respond_to do |format|
      format.js
      format.csv { render :layout => false }
    end
  end

  #def search_status_relation
  #  @chain_ids = current_owner.chain_ids
  #  @chain = current_owner.chain
  #  @surveys = Survey.by_chain_ids(@chain_ids)
  #  survey_ids = @surveys.map(&:id)
  #  @survey_users = GuestRelation.by_chain(@chain.id)
  #
  #  if params[:search][:from] != "" && params[:search][:to] != ""
  #    @survey_users = GuestRelation.includes(:surveys_user => :user).
  #        where("guest_relations.chain_id = ? AND guest_relations.status = ? AND users.email LIKE ? AND guest_relations.created_at BETWEEN ? AND ? ", @chain.id, params[:status], "%#{params[:email]}%", params[:search][:from], params[:search][:to])
  #  elsif params[:search][:from] != ""
  #    @survey_users = GuestRelation.includes(:surveys_user => :user).
  #        where("guest_relations.chain_id = ? AND guest_relations.status = ? AND users.email LIKE ? AND guest_relations.created_at > ? ", @chain.id, params[:status], "%#{params[:email]}%", params[:search][:from])
  #  elsif params[:search][:to] != ""
  #    @survey_users = GuestRelation.includes(:surveys_user => :user).
  #        where("guest_relations.chain_id = ? AND guest_relations.status = ? AND users.email LIKE ? AND guest_relations.created_at < ? ", @chain.id, params[:status], "%#{params[:email]}%", params[:search][:to])
  #  else
  #    @survey_users = GuestRelation.includes(:surveys_user => :user).
  #        where("guest_relations.chain_id = ? AND guest_relations.status = ? AND users.email LIKE ?", @chain.id, params[:status], "%#{params[:email]}%")
  #  end
  #
  #  #@search_email = params[:email]
  #  @survey_users = @survey_users.paginate(page: params[:page], per_page: Setting.pagination.per_page) if request.format.to_s != "text/csv"
  #
  #  respond_to do |format|
  #    format.js
  #    format.csv { render :layout => false }
  #  end
  #end

end