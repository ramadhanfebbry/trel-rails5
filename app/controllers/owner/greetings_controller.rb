include Geokit::Geocoders

class Owner::GreetingsController < Owner::BaseController
  #around_filter :select_shard
  before_action :find_restaurant, :only => [:index,:export_summaries]

  def index
    if current_owner.is_manager?
      @restaurants = current_owner.chain.restaurants
    else
      @restaurants = current_owner.restaurant_list
    end

    list_surveys = Survey.where("chain_id in (?)",@chain_ids)
    if current_owner.chain_owner?
      @survey = list_surveys.last
      @surveys = list_surveys
    else
      @survey = SurveysUser.where("restaurant_id in (?)", current_owner.restaurant_list).first.survey rescue list_surveys.first
      @surveys = SurveysUser.where("restaurant_id in (?)", current_owner.restaurant_list).select('distinct(survey_id)')
    end

    # sorting restaurant based on total members descending
    # move to redis in restaurant users.rb
    #restaurant_ids = RestaurantUser.select("count(restaurant_users.user_id),restaurant_id").
    #     where("restaurant_id in(?) and users.active = ?",@restaurants,true).group("restaurant_id").
    #     joins(:user).order("count desc")
    #
    # restaurant_ids = restaurant_ids.map(&:restaurant_id)
    # restaurant_ids = (restaurant_ids + @restaurants.map(&:id)).uniq

    restaurant_ids = REDIS.get "chain_#{current_owner.chain.id}_restaurant_desc"
    restaurant_ids = restaurant_ids[1..-2].split(',').collect! {|n| n.to_i}
    restaurant_ids = restaurant_ids.delete_if{|x| !@restaurants.map(&:id).include?(x) } rescue nil

    sql = []
    # if the restaurant is more than 20 then use usual, do not sorting, cause its cause bad load.
    # if restaurant_ids.size <= 20
    #   restaurant_ids.each do |ri|
    #     sql << "id = #{ri}"
    #   end
    #
    #   #hanle sql blank
    #   if sql.blank?
    #     current_owner.restaurant_list.each do |res|
    #       sql << "id = #{res.id}"
    #     end
    #   end
    #
    #   sql_tmp = sql.join(" OR ")
    #   order_sql = sql.reverse.join(' , ')
    #   puts "sql === #{sql}"
    #
    #
    #   @restaurants = Restaurant.where(sql_tmp).order(order_sql)
    # end
    # end

    begin
      restaurant_tmp = []
      unless @restaurants.blank?
        restaurant_ids.each do |r|
          restaurant_tmp = restaurant_tmp + @restaurants.select { |x| x.id.to_i == r.to_i }
        end

        @restaurants = restaurant_tmp
      end
    rescue
    end

    if !params["tag"].blank? and !params["tag"].include?('All')
      @tag_filter = "true"
      sql_conditions = " restaurants.chain_id = #{current_owner.chain.id} "
      @restaurants = Restaurant.tagged_with(params["tag"],sql_conditions)
      @filter_tag = params["tag"]
    end

    # tagging
    @tag_list = []
    @restaurants.each do |rs|
      unless rs.tag_list.blank?
        @tag_list  << rs.tag_list if rs.status == true
      end
    end
    @tag_list = @tag_list.uniq.compact
    # end

    #@distinct_members = RestaurantUser.distinct_users(current_owner,@chains)
    @all_restaurants = @restaurants
    @restaurants = @restaurants.delete_if{|x| x.status == false}

    @tag_list = Tagging.
        where("taggable_type = 'Restaurant' and taggable_id in (select id from restaurants where chain_id = #{@chain.id} and restaurants.status is TRUE) ").
        joins(:tag).select("distinct(tags.name)").map(&:name)
    @restaurants= @restaurants.paginate(page: params[:page], per_page: Setting.pagination.per_page) # show.html.erb

    respond_to do |format|
      format.html {}
      format.js {}
    end
  end

  def show

  end

  def export_summaries
    list_surveys = Survey.where("chain_id in (?)",@chain_ids)
    if current_owner.chain_owner?
      @survey = list_surveys.last
      @surveys = list_surveys
    else
      @survey = SurveysUser.where("restaurant_id in (?)", current_owner.restaurant_list).first.survey rescue list_surveys.first
      @surveys = SurveysUser.where("restaurant_id in (?)", current_owner.restaurant_list).select('distinct(survey_id)')
    end

    #@distinct_members = RestaurantUser.distinct_users(current_owner,@chains)
    @all_restaurants = @restaurants
    @restaurants = @restaurants.delete_if{|x| x.status == false}
    Delayed::Job.enqueue(OwnerJob::SummariesExportJob.new(@all_restaurants,
                                                          @restaurants,
                                                          @chain,current_owner),
                         :delayable_type => "export_summaries_csv")
    redirect_to owner_greetings_path , :notice => "Export is being sent to your email"
  end

  def search_summaries

    return redirect_to owner_greetings_path(:page => params["page"]) if request.format.html?
    if current_owner.is_manager?
      @restaurants = current_owner.chain.restaurants
    else
      @restaurants = current_owner.restaurant_list
    end

    @tag_list = []
    @restaurants.each do |rs|
      @tag_list  << rs.tag_list unless rs.tag_list.blank?
    end
    @tag_list = @tag_list.uniq.compact
    @chains = current_owner.chain_list
    list_surveys = Survey.where("chain_id = ?",current_owner.chain.id)
    if current_owner.chain_owner?
      @survey = list_surveys.last
      @surveys = list_surveys
    else
      @survey = SurveysUser.where("restaurant_id in (?)", current_owner.restaurant_list).first.survey rescue list_surveys.first
      @surveys = SurveysUser.where("restaurant_id in (?)", current_owner.restaurant_list).select('distinct(survey_id)')
    end
    @all_restaurants = @restaurants
    #@distinct_members = RestaurantUser.distinct_users(current_owner,@chains)
    @restaurants = current_owner.restaurant_list.where("lower(restaurants.dashboard_display_text) like ?",
                                     "%#{params['restaurant'].downcase}%")

    puts "params tag = #{params["tag"]}"
    puts "============================== surveys list ========================="
    puts "surveys = #{@surveys.map(&:id)}"
    ## tagging
    if !params["tag"].blank? and !params["tag"].include?('All')
      @tag_filter = "true"
      sql_conditions = " restaurants.chain_id = #{current_owner.chain.id} "
      @restaurants = Restaurant.tagged_with(params["tag"],sql_conditions)
      @filter_tag = params["tag"]
    end

    ## end

    #sorting restaurant based on total members descending
    #move to redis in restaurant users.rb
    # restaurant_ids = RestaurantUser.select("count(restaurant_users.user_id),restaurant_id").
    #     where("restaurant_id in(?) and users.active = ?",@restaurants,true).group("restaurant_id").
    #     joins(:user).order("count desc")

    #restaurant_ids = restaurant_ids.map(&:restaurant_id)
    restaurant_ids =  current_owner.restaurant_list.where("lower(restaurants.dashboard_display_text) like ?",
                                                          "%#{params['restaurant'].downcase}%").map(&:id).uniq
    puts "restaurant_ids = #{restaurant_ids}"

    if params['restaurant'].blank?
      restaurant_ids = REDIS.get "chain_#{current_owner.chain.id}_restaurant_desc"
      restaurant_ids = restaurant_ids[1..-2].split(',').collect! {|n| n.to_i}
    end

    restaurant_ids = restaurant_ids.delete_if{|x| !current_owner.restaurant_list.map(&:id).include?(x) } rescue nil
    puts "restaurant_ids = #{restaurant_ids}"

    sql = []
    restaurant_ids.each do |ri|
      sql << "id = #{ri}"
    end

    #hanle sql blank
    if sql.blank?
      current_owner.restaurant_list.each do |res|
        sql << "id = #{res.id}"
      end
    end

    sql_tmp = sql.join(" OR ")
    order_sql = sql.reverse.join(' , ')
    puts "sql === #{sql}"
    if sql_tmp.blank?
      @restaurants = current_owner.restaurant_list.where("lower(restaurants.dashboard_display_text) like ?",
                                                         "%#{params['restaurant'].downcase}%").paginate(page: params[:page], per_page: Setting.pagination.per_page) # show.html.erb
    else
      @restaurants = Restaurant.where(sql_tmp).order(order_sql).paginate(page: params[:page], per_page: Setting.pagination.per_page) # show.html.erb
    end
    @all_restaurants = @restaurants

    # end


    @search_summaries = "true"
  end

  private

  def select_shard(&block)
    puts "SELECT SHARD Session--------------------------------- "
    Octopus.using(:shard_one, &block)   if ENV["S3_BUCKET"]  != "trelevant"
  end
  
  def find_restaurant
    #current_owner = Owner.find_by_email("gp@relevantmobile.com")
    if current_owner.is_manager?
      @restaurants = current_owner.chain.restaurants.where('status is TRUE')
    else
      @restaurants = current_owner.restaurant_list
    end

    @chains = current_owner.chain_list
    @chain_ids = @chains.map(&:id) rescue []
    @chain = current_owner.chain
  end

end
