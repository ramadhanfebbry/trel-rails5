class Owner::OwnerUsersController < ApplicationController
  layout 'owner'
  skip_before_action :authenticate_user!, :only => [:export_pdf]

  def index
    @owner_users = current_owner.owner_users.paginate(page: params[:page], per_page: 10)
  end


  def show    
    @owner = Owner.find(params[:id])
  end

  def new    
    @owner = Owner.new
  end

  def create    
    @owner = Owner.new(params[:owner])
    respond_to do |format|
      if @owner.save
        @owner.update_attribute(:parent_id, current_owner.id)
        @owner.update_participation_restaurant(params[:restaurants], params[:ids])
        @owner.set_chain(@owner.chain_id)
        format.html { redirect_to owner_owner_users_url(@owner), notice: 'Owner was successfully updated.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  def export_pdf    
    if Digest::SHA1.hexdigest(params[:ref_id]+"secret") == params["secret_keys"]      
      respond_to do |format|
        format.html
        format.pdf do
          render :pdf => "file_name"
        end
      end
    else
      render :text => "You dont have any permission for this"
    end
  end

  def edit    
    @owner = Owner.find(params[:id])
  end

  def update    
    @owner = Owner.find(params[:id])
    respond_to do |format|
      if @owner.update_attributes(params[:owner]) && @owner.update_attribute(:parent_id, current_owner.id)
        @owner.update_participation_restaurant(params[:restaurants], params[:ids])
        format.html { redirect_to owner_owner_users_url(@owner), notice: 'Owner was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy    
    @owner = Owner.find(params[:id])

    if(@owner)
      @owner.restaurants_owners.destroy_all
      @owner.destroy
    end

    redirect_to owner_owner_users_url
  end
end
