class Owner::BaseController < ApplicationController
  layout 'owner_d2'
  before_action :authenticate_owner! , :find_email_to
#  before_action :find_chain
#
#  def find_chain
#    @chain = current_owner.chains.first
#    return @chain
#  end

  def find_email_to
    ch = current_owner.chain
    @email_to = REDIS.get "owner_question_email_chain_#{ch.id}"  rescue "support@relevantmobile.com"
    @email_to ||= "pleasesetonadmin@relevant.com"
    chain_ids = current_owner.chains.map(&:id)
    @request_count = GuestRelation.where("chain_id in(?) and status = 1", chain_ids).count if current_owner.is_chain_owner?
    @is_avg = REDIS.get "avg_survey_setting_#{ch.id}"
    @is_avg = @is_avg.blank?
  end

end