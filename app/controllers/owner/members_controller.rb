class Owner::MembersController < Owner::BaseController
  #around_filter :select_shard
  before_action :find_chain_for_chart

  def index
    @tag_list = []
    @filter_tag = params['tag']

    if params['tag'] and params['tag'].include?('All')
      return redirect_to owner_members_path
    end

    if !params["tag"].blank?
      sql_conditions = " restaurants.chain_id = #{current_owner.chain.id} "
      @restaurants = Restaurant.tagged_with(params["tag"],sql_conditions)
    end

    a = Time.now
    puts "New member today process =========="
    # @new_member_today = Rails.cache.fetch("member_index_new_member_#{current_owner.id}_", :expires_in => 3.hours) do
    #  Chain.today_users(@chains,current_owner, @all_restaurants)
    # end
    @new_member_today = Chain.today_users(@chains,current_owner, @all_restaurants)
    puts "New member today process ========== in #{Time.now - a}"


    #@all_member = Rails.cache.fetch("member_index_all_member_#{@all_restaurants.map(&:id).join('_')}",
    #@all_member = Rails.cache.fetch("member_index_all_member_#{current_owner.id}",
    #                                                  :expires_in => 15.minutes) do
    # move to redis
     @all_member =  Chain.count_members(current_owner, @chains, @all_restaurants)
    #end
    a = Time.now
    puts "active member process =========="
    # @active_members_count =Rails.cache.fetch("member_index_active_member_new_1#{current_owner.id}",
    #                                          :expires_in => 3.hours) do
    #   Chain.new_count_active(@chains.first,@all_restaurants, @all_member)
    #
    # end
    @active_members_count = Chain.new_count_active(@chains.first,@all_restaurants, @all_member)
    puts "active member process ==========  in #{Time.now - a}"
    #@new_member_today = Chain.today_users(@chains,current_owner, @restaurants)
    #@all_member = Chain.count_members(current_owner, @chains, @restaurants)
    #@active_members_count = Chain.count_active_members(@restaurants, @all_member)
    #@active_members_count = Chain.new_count_active(@chains.first,@restaurants, @all_member)
    if @filter_location.blank?
      @selected_restaurants = [@restaurants[0]] if @restaurants.size <= 10
      @selected_restaurants = @restaurants[0..2] if @restaurants.size > 10
    else
      @selected_restaurants = @restaurants
    end

    @tag_list = Tagging.
        where("taggable_type = 'Restaurant' and taggable_id in (select id from restaurants where chain_id = #{@chain.id} and restaurants.status is TRUE) ").
        joins(:tag).select("distinct(tags.name)").map(&:name)
  end

  def send_message
    send_to = REDIS.get "owner_question_email_chain_#{@chain.id}" rescue nil
    ## if send to is not set on admin sent to support@relevantmobile.com
    send_to = "support@relevantmobile.com" if send_to.blank?

    OwnerMailer.send_to_support(
        @chain, send_to, params[:subject], params[:detail], params[:from],
        current_owner.email
    ).deliver!

    redirect_to :back, :notice => "Thanks for your feedback."
  end

  def search
    respond_to do |format|
      format.js {}
    end
  end

  private


  def select_shard(&block)
    ## turn on if environtment != trelevant
    puts "SELECT SHARD Session--------------------------------- "
    Octopus.using(:shard_one, &block)  #if ENV["S3_BUCKET"]  != "trelevant"
  end

  def find_chain_for_chart
    #current_owner = Owner.find_by_email("gp@relevantmobile.com")
    # @restaurants = Rails.cache.fetch("find_restaurant_for_chart_#{current_owner.id}",
    #                                                       :expires_in => 6.hours) do
    #   current_owner.restaurant_list.to_a
    # end

    @restaurants = current_owner.restaurant_list
    @all_restaurants = @restaurants
    @chain = current_owner.chain
    @chains = current_owner.chain_list
    ## this is commented out since the feature is hold
    #@filter_location = REDIS.get "owner_dashboard_setting_#{@chain.id}_location"
  end

end
