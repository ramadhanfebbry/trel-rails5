class Pos::BaseController < ApplicationController

  before_action  :authorize_apikey!, :except => [:get_info, :apply_offers, :submit_order, :void_order,
                                                 :tw_get_info,:tw_apply_offers, :tw_submit_orders, :do_pull_payment_alert]


  #FOR V2 PURPOSES
  before_action :check_pos_query_string, :if => :is_v2_post_request?

  def authorize_apikey!
    check_and_build_params
    unless @pos_location = PosLocation.where(:apikey => params["apikey"]).first
      render :status => 404,
             :json => {status: false, notice: "Something wrong with your apikey"} and return
    end
  end

  def authorize_apikey_for_xpient(store_id)
    unless @pos_location = PosLocation.where(:apikey => store_id).first
      render :status => 404,
             :json => {status: false, notice: "Something wrong with your apikey"} and return
    end
  end

  def authorize_apikey_for_treatware(store_id, response_name)
    @pos_location = PosLocation.where(:apikey => store_id).first
    if @pos_location.blank?
      render :json => {response_name => {"statusCode" => 404, "resultDescription" => "Something wrong with your StoreId", "apiVersion" => "1.0"}} and return
    end
  end

  def check_and_build_params
    unless params["_json"].blank?
      new_params = params["_json"]
      if new_params.class.to_s == Hash
        params.merge!(new_params)
      else
        params.merge!(JSON.load(new_params))
      end
    end
  end

  def is_v2_post_request?
    p " process is_v2_post_request?"
    p request.fullpath.include?("/pos/v2/")
    p request.post?
    request.fullpath.include?("/pos/v2/") && request.post?
  end

  def check_pos_query_string
    p " process check_query_string?"
    p request.query_string
    p request.query_string.present?
    if request.query_string.present?
      p "----------------check_query_string!----- #{request.query_string} "
      return render :status => 404, :json => {status: false, notice: "Invalid API request."}
    end if request.fullpath.include?("/pos/v2/") && request.post?
  end

end
