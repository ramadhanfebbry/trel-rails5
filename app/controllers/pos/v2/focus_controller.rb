class Pos::V2::FocusController < Pos::V1::FocusController

  def get_program_description
    super
  end

  def get_status
    super
  end

  def add_credits
    super
  end

  def close_transaction_with_sku
    super
  end

  def get_coupon
    super
  end

  def redeem_coupon
    super
  end

end
