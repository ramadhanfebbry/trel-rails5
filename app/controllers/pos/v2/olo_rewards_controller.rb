class Pos::V2::OloRewardsController < Pos::V1::OloRewardsController

  def check_auth
    super
  end

  def authenticate_new_olo_user!
    super
  end

  def authorize_apikey!
    super
  end

  def check_and_build_params
    super
  end

  def validate
    super
  end

  def process_code_olo
    super
  end

  def void_redeem_olo
    super
  end

end
