class Pos::V2::UploadController < Pos::V1::UploadController

  def check_status
    super
  end

  def check_upload
    super
  end

  def menu_status
    super
  end

  def menu_upload
    super
  end

  def barcode
    super
  end

  def barcode_check_submit
    super
  end

  def usercode
    super
  end

  def get_code_type
    super
  end

  def pull_check_payment
    super
  end

  def update_mp_processed_paycode
    super
  end

  def get_check_detail
    super
  end

  def do_pull_payment_alert
    super
  end

  def pull_payment_alert
    super
  end

end