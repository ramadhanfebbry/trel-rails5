class Pos::V2::TreatwareController < Pos::V1::TreatwareController

  def tw_get_info
    super
  end

  def tw_apply_offers
    super
  end

  def tw_void_offers
    super
  end

  def tw_submit_orders
    super
  end

  def auto_loyalty_for_giftcode(pos_location, user, treatware_check, code, selected_code_type, user_code_type, user_session, code_type)
    super
  end

  def authorize_health_apikey!
    super
  end

  def check_and_build_health_params
    super
  end
end
