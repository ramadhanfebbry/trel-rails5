class Pos::V2::XpientController < Pos::V1::XpientController

  def get_info
    super
  end

  def apply_offers
    super
  end

  def submit_order
    super
  end

  def void_order
    super
  end

  def get_line_items(chain, line_items)
    super
  end

end