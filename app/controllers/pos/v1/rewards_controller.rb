require 'nokogiri'
class Pos::V1::RewardsController < Pos::BaseController

  def process_code
    chain = @pos_location.chain
    location = @pos_location.restaurant
    reward_ids = chain.rewards.map(&:id)

    #PROCESS REWARD FOR BRIDG POS, REWARD IS PARTNER REWARD
    if chain.pos_used_type == Chain::POS_USED_TYPE["BRIDG"]
      return process_bridg_discount
    end

    case params["request_type"]
      when "0"
        reward_ids = chain.rewards.map(&:id)
        reward_code = params["reward_code"].to_s.upcase

        reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
        reward_code_setting = JSON.parse(reward_code_setting) rescue nil
        reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
        if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
          if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
            reward_code = reward_code.to_ean_13_format
          end
        end
        reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
        pos_reward_check = PosRewardCheck.new(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params["check"], :reward_code => reward_code)
        reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => reward_code, :active => true).first
        if reward_code_user.blank?
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = "Reward code invalid"
          pos_reward_check.save
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code invalid"} and return
        end
        reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", reward_code, reward_ids)
        if reward_transactions.blank?
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = "Reward code invalid."
          pos_reward_check.save
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code invalid"} and return
        end

        reward_code_expire = reward_code_setting["timer"].to_i
        if reward_code_expire != 0
          reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
        else
          reward_transaction = reward_transactions.order("created_at DESC").first
        end
        if reward_transaction.blank?
          reward_code_user.destroy if reward_code_user
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = "Reward code expired."
          pos_reward_check.save
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code expired"} and return
        end

        #xml_in_nokogiri_format = Nokogiri::XML(params[:check])
        p "request.content_type"
        p request.content_type
        p "request.content_type #{request.content_type}"
        p "----------------"
        xml_receipt = params["check"]
        if request.content_type == "application/json" && xml_receipt.class.to_s != "String"
          p "NORTHKEY POS PROCESS REWARD HERE _--------"
          return northkey_reward_discount_process(chain, location, reward_transaction, pos_reward_check, reward_code_user)
        else
          check = Nokogiri::XML(xml_receipt)
          render :json => {:confirm => 0, :response_code => 1} and return if check.children.blank?
          check_id = check.root["id"] rescue nil
          seq_number = check.root["seq_num"] rescue nil
          revenue_center = check.search("rvc-num").text.to_s rescue nil
          employee_num =  check.search("emp-num").text.to_s rescue nil
          tbl_num =  check.search("tbl-num").text.to_s rescue nil
          payment_included = check.search("tender//type").map(&:text).include?("MobileTender") rescue false
          menu_items_list = check.search('menu-item')
          due_total = 0
          pos_used_type = "micros"
        end

        p "here is check id and seq number -----"
        check_id = check_id.strip rescue nil
        seq_number = seq_number.strip rescue nil
        p "check id is #{check_id}"
        p "seq num is #{seq_number}"

        items_discount = []
        reward_transaction.update_column(:check_number, [check_id, seq_number, @pos_location.id].join("-"))
        reward = reward_transaction.reward
        user = reward_transaction.user
        reward_general_menu_item_ids = reward.general_menu_items.map(&:id)
        reward_menu_items = reward.reward_menu_items
        required_menu_items = reward_menu_items.select{|a| a.required }
        optional_menu_items = reward_menu_items.select{|a| !a.required }
        items_check_buy = []
        xml_menu_items = menu_items_list
        parent_qualified = true
        xml_menu_items.each do |menu_item|
          item = {}
          item[:menu_item_id] = menu_item.attr("id").strip rescue nil
          item[:menu_item_id] = menu_item["ItemId"].to_s.strip if item[:menu_item_id].blank?

          item[:name] = menu_item.search("name").text.strip rescue nil
          item[:name] = menu_item["Name"].to_s.strip if item[:name].blank?

          item[:quantity] = menu_item.search("quantity").text.to_i rescue 0
          item[:quantity] = menu_item["Quantity"].to_i if item[:quantity] == 0

          item[:total_amount] = menu_item.search("total-amount").text.to_f rescue 0
          item[:total_amount] = menu_item["TotalPrice"].to_f if item[:total_amount] == 0

          item[:price_per_item] = ((item[:total_amount]/item[:quantity]) rescue 0)
          item[:price_per_item] = menu_item["UnitPrice"].to_f if item[:price_per_item] == 0

          item[:level_size_id] = menu_item.search("size-id").text rescue nil
          item[:level_size_id] = menu_item["SizeId"].to_i  if item[:level_size_id].blank? &&  !menu_item["SizeId"].blank?
          item[:level_size_id] = nil if item[:level_size_id].blank?

          p "---- item number  #{item[:menu_item_id]}"
          p "------- chain = #{chain.id}--"
          p "=------ pos location = #{@pos_location.id}"
          arr_general_menu_item_id = GeneralMenuItemRangeValue.where("start_value <= #{item[:menu_item_id]} and end_value >= #{item[:menu_item_id]} and chain_id = #{chain.id}").map(&:general_menu_item_id)
          pos_menu_items = PosMenuItem.select("id").where(:item_number => item[:menu_item_id], :chain_id => chain.id, :level_size_id => item[:level_size_id])
          pos_menu_items.each do |pos_menu_item|
            arr_general_menu_item_id << (pos_menu_item.general_menu_items.map(&:id) rescue [])
            break unless arr_general_menu_item_id.blank?
          end
          item[:general_menu_item_id] = arr_general_menu_item_id.flatten
          item[:required_menu_item] = !(item[:general_menu_item_id] & required_menu_items.map(&:general_menu_item_id)).blank?
          if item[:general_menu_item_id].blank? && !item[:required_menu_item]
            parent_qualified = false
          elsif !item[:general_menu_item_id].blank? && item[:required_menu_item]
            parent_qualified = true
          end
          items_check_buy << item if parent_qualified
        end

        #ordered_item_check_buy = items_check_buy.sort{|a,b| a[:price_per_item] <=> b[:price_per_item]}
        #filtered_items_check = items_check_buy.select{|a| !(reward_general_menu_item_ids && a[:general_menu_item_id]).blank?}

        grouped_items_check_buy = []
        items_check_buy.each_with_index do |item_check, i|
          if item_check[:required_menu_item]
            item_check[:all_price_with_optional_item] = item_check[:price_per_item]
            menu_item_combo_sum = [item_check[:menu_item_id]]
            p '----'
            p item_check[:menu_item_combo_sum]
            (i+1).upto(items_check_buy.length-1) do |idx|
              optional_item = items_check_buy[idx]
              unless optional_item[:required_menu_item]
                item_check[:all_price_with_optional_item] += optional_item[:price_per_item]
                menu_item_combo_sum << optional_item[:menu_item_id]
              else
                break
              end
            end
          end
          item_check[:menu_item_combo_sum] = menu_item_combo_sum.join("-") if menu_item_combo_sum
          grouped_items_check_buy << item_check
        end

        if chain.pos_discount_setting.eql?(Chain::POS_DISCOUNT_TYPE["Discount Least priced qualifying item"])
          ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| a[:all_price_with_optional_item] <=> b[:all_price_with_optional_item]}
        else
          ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| b[:all_price_with_optional_item] <=> a[:all_price_with_optional_item]}
        end

        pos_discount_type = reward.pos_discount_type

        #non menu item discount based
        if pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?
          if request.content_type == "application/json" && xml_receipt.class.to_s != "String"
            pos_check = params["check"]
            check_subtotal = pos_check["Subtotal"]
          else
            pos_xml = PosReceiptXml.new(params["check"])
            check_subtotal = pos_xml.sub_total
          end
          total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", check_id, seq_number, @pos_location.restaurant_id])
          subtotal_after_discount = check_subtotal - total_discount_registered
          if subtotal_after_discount > 0
            discount_applied = subtotal_after_discount > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : subtotal_after_discount
            reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
            RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
            pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
            pos_reward_check.user_id = (user.id rescue nil)
            pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
            pos_reward_check.save
            if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
              reward.claimed_by(user)
              Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
            end
            if chain.auto_loyalty && due_total == 0
              xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, discount_applied)
              Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
            end
            render :json => {:status => true, :discount => (discount_applied*-1), :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
          else
            pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
            pos_reward_check.user_id = (user.id rescue nil)
            pos_reward_check.error_description = "Reward requirement is not met."
            pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
            pos_reward_check.save
            #reward_transaction.update_column(:pos_used, true)
            render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
          end
        elsif pos_discount_type && pos_discount_type.is_non_menu_item_based_percentage_off?
          if request.content_type == "application/json" && xml_receipt.class.to_s != "String"
            pos_check = params["check"]
            check_subtotal = pos_check["Subtotal"]
          else
            pos_xml = PosReceiptXml.new(params["check"])
            check_subtotal = pos_xml.sub_total
          end
          total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", check_id, seq_number, @pos_location.restaurant_id])
          subtotal_after_discount = check_subtotal - total_discount_registered
          if subtotal_after_discount > 0
            discount_applied = ((pos_discount_type.discount_amount.to_f/100) * subtotal_after_discount).round(2)
            reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
            RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
            pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
            pos_reward_check.user_id = (user.id rescue nil)
            pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
            pos_reward_check.save
            if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
              reward.claimed_by(user)
              Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
            end
            if chain.auto_loyalty && due_total == 0
              xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, discount_applied)
              Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
            end
            render :json => {:status => true, :discount => (discount_applied*-1), :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
          else
            pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
            pos_reward_check.user_id = (user.id rescue nil)
            pos_reward_check.error_description = "Reward requirement is not met."
            pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
            pos_reward_check.save
            #reward_transaction.update_column(:pos_used, true)
            render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
          end
        end

        #check min no items ordered
        qualified_items_ordered_validation = ordered_item_check_buy.select{|a| !(a[:general_menu_item_id] & reward_menu_items.map(&:general_menu_item_id)).blank?}
        return [items_discount, "Discount is not activated, because minimum number of items ordered validation."] if pos_discount_type.min_no_items_ordered < 1
        return [items_discount, "Discount is not activated, because number of items to be discounted validation."] if pos_discount_type.min_no_items_discounted < 1

        return [items_discount, "The Items that you bought is not met with the requirement minimum number of items ordered."] if qualified_items_ordered_validation.count < pos_discount_type.min_no_items_ordered && pos_discount_type.min_no_items_ordered > 1
        item_discounted = 0
        #what discount that reward offered
        required_menu_items.each do |reward_general_menu_item|
          #reward_general_menu_item_ids.each do |reward_general_menu_item_id|
          ordered_item_check_buy.each do |filtered_item|
            next if filtered_item[:price_per_item] <= 0 && pos_used_type == "northkey"
            if filtered_item[:general_menu_item_id].include?(reward_general_menu_item.general_menu_item_id)
              reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]).count
              qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
              if reward_discount_count < qty_selected
                items_discount << {:menu_item_id => filtered_item[:menu_item_id], :price => filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]}
                item_discounted += 1
                break if item_discounted >= pos_discount_type.min_no_items_discounted
                #CODE FOR OPTIONAL MENU ITEM
                optional_menu_discount_items = []
                optional_menu_items.each do |opt_reward_general_menu_item|
                  (grouped_items_check_buy.index(filtered_item)+1).upto(grouped_items_check_buy.length-1) do |idx|
                    opt_filtered_item = grouped_items_check_buy[idx] rescue nil
                    break if opt_filtered_item.blank?
                    break if opt_filtered_item[:required_menu_item]
                    if opt_filtered_item[:general_menu_item_id].include?(opt_reward_general_menu_item.general_menu_item_id)
                      reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => opt_filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]).count
                      qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
                      if reward_discount_count < qty_selected
                        #optional_menu_discount_items <<  opt_filtered_item
                        items_discount << {:menu_item_id => opt_filtered_item[:menu_item_id], :price => opt_filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => opt_filtered_item[:level_size_id]}
                        item_discounted += 1
                        break if item_discounted >= pos_discount_type.min_no_items_discounted
                      end
                    end
                  end
                end
                # break
              end
            end
          end
        end

        #preset item discount if there not suitable combo
        items_discount = [] if !items_discount.blank? && !(required_menu_items.count <= items_discount.count)

        unless items_discount.blank?
          #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
          reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
          reward_code_user.destroy if reward_code_user
          p "here are the items that got the discount"
          p items_discount
          all_discount_item = items_discount.sum{ |a| a[:price]}
          if pos_discount_type.blank?
            calculated_discount_from_reward = all_discount_item
          elsif pos_discount_type.is_discount_percentage?
            calculated_discount_from_reward = ((pos_discount_type.discount_amount.to_f/100) * all_discount_item).round(2)
          elsif pos_discount_type.is_discount_price?
            calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? (all_discount_item - pos_discount_type.discount_amount) : all_discount_item).round(2)
          elsif pos_discount_type.is_discount_fixed_price?
            calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : all_discount_item).round(2)
          end
          items_discount.each do |item|
            RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => calculated_discount_from_reward, :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum] )
          end
          pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
          pos_reward_check.user_id = (user.id rescue nil)
          pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
          pos_reward_check.save
          if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
            reward.claimed_by(user)
            Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
          end
          if chain.auto_loyalty && due_total == 0
            xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, calculated_discount_from_reward)
            Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
          end
          render :json => {:status => true, :discount => (calculated_discount_from_reward*-1), :discount_items => items_discount, :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
        else
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.user_id = (user.id rescue nil)
          pos_reward_check.error_description = "Reward requirement is not met."
          pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
          pos_reward_check.save
          #reward_transaction.update_column(:pos_used, true)
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
        end
      when "1"
        reward_transaction = RewardTransaction.where("id = ? AND pos_used IS TRUE", params[:transaction_id].blank? ? 0 : params[:transaction_id]).first rescue nil
        reward = reward_transaction.reward rescue nil
        return render :json => {:status => true, :discount_voided => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Transaction reversal invalid"}   if reward_transaction.blank? || reward.blank?
        user = reward_transaction.user
        reward.void_redeem_reward(user)
        voided_discount = RewardDiscount.where(:reward_transaction_id => reward_transaction.id).sum(:discount)
        RewardDiscount.where(:reward_transaction_id => reward_transaction.id, :restaurant_id => @pos_location.restaurant_id).delete_all
        pos_reward_check = PosRewardCheck.create(
            :pos_location_id => @pos_location.id,
            :chain_id => @pos_location.chain_id,
            :transaction_type => 1,
            :reward_transaction_id => reward_transaction.try(:id),
            :user_id => user.try(:id),
            :status => PosRewardCheck::STATUS["SUCCESS"]
        )
        render :json => {:status => true, :discount_voided => voided_discount, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => "Transaction reversed"} and return
    end
  end

  def northkey_reward_discount_process(chain, location, reward_transaction, pos_reward_check, reward_code_user)
    xml_receipt = params["check"]
    check = xml_receipt
    render :json => {:confirm => 0, :response_code => 1} and return if check.blank?
    check_id = check["CheckId"].to_s
    render :json => {:confirm => 0, :response_code => 1} and return if check_id.blank?
    seq_number = check["SeqNum"].to_s
    render :json => {:confirm => 0, :response_code => 1} and return if seq_number.blank?
    revenue_center = check["RvcNum"].to_s
    employee_num = check["EmpNum"].to_s
    tbl_num = check["TblNum"].to_s
    payment_included = check["Payments"].map{|a| a["Type"]}.include?("MobileTender") rescue false
    menu_items_list = check["Items"]
    due_total = check["DueTotal"].to_f
    pos_used_type = "northkey"

    p "here is check id and seq number -----"
    check_id = check_id.strip rescue nil
    seq_number = seq_number.strip rescue nil
    p "check id is #{check_id}"
    p "seq num is #{seq_number}"

    items_discount = []
    reward_transaction.update_column(:check_number, [check_id, seq_number, @pos_location.id].join("-"))
    reward = reward_transaction.reward
    user = reward_transaction.user
    reward_general_menu_item_ids = reward.general_menu_items.map(&:id)
    reward_menu_items = reward.reward_menu_items
    required_menu_items = reward_menu_items.select{|a| a.required }
    optional_menu_items = reward_menu_items.select{|a| !a.required }
    items_check_buy = []
    xml_menu_items = menu_items_list
    parent_qualified = true
    xml_menu_items.each do |menu_item|
      item = {}
      item[:menu_item_id] = menu_item.attr("id").strip rescue nil
      item[:menu_item_id] = menu_item["ItemId"].to_s.strip if item[:menu_item_id].blank?

      item[:name] = menu_item.search("name").text.strip rescue nil
      item[:name] = menu_item["Name"].to_s.strip if item[:name].blank?

      item[:quantity] = menu_item.search("quantity").text.to_i rescue 0
      item[:quantity] = menu_item["Quantity"].to_i if item[:quantity] == 0

      item[:total_amount] = menu_item.search("total-amount").text.to_f rescue 0
      item[:total_amount] = menu_item["TotalPrice"].to_f if item[:total_amount] == 0

      item[:price_per_item] = ((item[:total_amount]/item[:quantity]) rescue 0)
      item[:price_per_item] = menu_item["UnitPrice"].to_f if item[:price_per_item] == 0

      item[:level_size_id] = menu_item.search("size-id").text rescue nil
      item[:level_size_id] = menu_item["SizeId"].to_i  if item[:level_size_id].blank? &&  !menu_item["SizeId"].blank?
      item[:level_size_id] = nil if item[:level_size_id].blank?

      p "---- item number  #{item[:menu_item_id]}"
      p "------- chain = #{chain.id}--"
      p "=------ pos location = #{@pos_location.id}"
      arr_general_menu_item_id = GeneralMenuItemRangeValue.where("start_value <= #{item[:menu_item_id]} and end_value >= #{item[:menu_item_id]} and chain_id = #{chain.id}").map(&:general_menu_item_id)
      pos_menu_items = PosMenuItem.select("id").where(:item_number => item[:menu_item_id], :chain_id => chain.id, :level_size_id => item[:level_size_id])
      pos_menu_items.each do |pos_menu_item|
        arr_general_menu_item_id << (pos_menu_item.general_menu_items.map(&:id) rescue [])
        break unless arr_general_menu_item_id.blank?
      end
      item[:general_menu_item_id] = arr_general_menu_item_id.flatten
      item[:required_menu_item] = !(item[:general_menu_item_id] & required_menu_items.map(&:general_menu_item_id)).blank?
      if item[:general_menu_item_id].blank? && !item[:required_menu_item]
        parent_qualified = false
      elsif !item[:general_menu_item_id].blank? && item[:required_menu_item]
        parent_qualified = true
      end
      items_check_buy << item if parent_qualified
    end

    #ordered_item_check_buy = items_check_buy.sort{|a,b| a[:price_per_item] <=> b[:price_per_item]}
    #filtered_items_check = items_check_buy.select{|a| !(reward_general_menu_item_ids && a[:general_menu_item_id]).blank?}

    grouped_items_check_buy = []
    items_check_buy.each_with_index do |item_check, i|
      if item_check[:required_menu_item]
        item_check[:all_price_with_optional_item] = item_check[:price_per_item]
        menu_item_combo_sum = [item_check[:menu_item_id]]
        p '----'
        p item_check[:menu_item_combo_sum]
        (i+1).upto(items_check_buy.length-1) do |idx|
          optional_item = items_check_buy[idx]
          unless optional_item[:required_menu_item]
            item_check[:all_price_with_optional_item] += optional_item[:price_per_item]
            menu_item_combo_sum << optional_item[:menu_item_id]
          else
            break
          end
        end
      end
      item_check[:menu_item_combo_sum] = menu_item_combo_sum.join("-") if menu_item_combo_sum
      grouped_items_check_buy << item_check
    end

    if chain.pos_discount_setting.eql?(Chain::POS_DISCOUNT_TYPE["Discount Least priced qualifying item"])
      ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| a[:all_price_with_optional_item] <=> b[:all_price_with_optional_item]}
    else
      ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| b[:all_price_with_optional_item] <=> a[:all_price_with_optional_item]}
    end

    pos_discount_type = reward.pos_discount_type

    #non menu item discount based
    if pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?
      if request.content_type == "application/json" && xml_receipt.class.to_s != "String"
        pos_check = params["check"]
        check_subtotal = pos_check["Subtotal"]
      else
        pos_xml = PosReceiptXml.new(params["check"])
        check_subtotal = pos_xml.sub_total
      end
      total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", check_id, seq_number, @pos_location.restaurant_id])
      subtotal_after_discount = check_subtotal - total_discount_registered
      if subtotal_after_discount > 0
        discount_applied = subtotal_after_discount > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : subtotal_after_discount
        reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
        RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
        pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
        pos_reward_check.user_id = (user.id rescue nil)
        pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
        pos_reward_check.save

        if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
          reward.claimed_by(user)
          Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
        end
        if chain.auto_loyalty && due_total == 0
          xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, discount_applied)
          Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
        end
        render :json => {:status => true, :discount => (discount_applied*-1), :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
      else
        pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
        pos_reward_check.user_id = (user.id rescue nil)
        pos_reward_check.error_description = "Reward requirement is not met."
        pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
        pos_reward_check.save
        #reward_transaction.update_column(:pos_used, true)
        render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
      end
    end

    #what discount that reward offered
    required_menu_items.each do |reward_general_menu_item|
      #reward_general_menu_item_ids.each do |reward_general_menu_item_id|
      ordered_item_check_buy.each do |filtered_item|
        next if filtered_item[:price_per_item] <= 0 && pos_used_type == "northkey"
        if filtered_item[:general_menu_item_id].include?(reward_general_menu_item.general_menu_item_id)
          reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id], :discount => filtered_item[:price_per_item]).count
          qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum] && a[:level_size_id] == filtered_item[:level_size_id]}.count
          if reward_discount_count < qty_selected
            items_discount << {:menu_item_id => filtered_item[:menu_item_id], :price => filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]}
            #CODE FOR OPTIONAL MENU ITEM
            optional_menu_discount_items = []
            optional_menu_items.each do |opt_reward_general_menu_item|
              (grouped_items_check_buy.index(filtered_item)+1).upto(grouped_items_check_buy.length-1) do |idx|
                opt_filtered_item = grouped_items_check_buy[idx] rescue nil
                break if opt_filtered_item.blank?
                break if opt_filtered_item[:required_menu_item]
                if opt_filtered_item[:general_menu_item_id].include?(opt_reward_general_menu_item.general_menu_item_id)
                  reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => opt_filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => opt_filtered_item[:level_size_id], :discount => opt_filtered_item[:price_per_item]).count
                  qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum] && a[:level_size_id] == opt_filtered_item[:level_size_id]}.count
                  if reward_discount_count < qty_selected
                    #optional_menu_discount_items <<  opt_filtered_item
                    items_discount << {:menu_item_id => opt_filtered_item[:menu_item_id], :price => opt_filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => opt_filtered_item[:level_size_id]}
                  end
                end
              end
            end
            break
          end
        end
      end
    end

    #preset item discount if there not suitable combo
    items_discount = [] if !items_discount.blank? && !(required_menu_items.count <= items_discount.count)

    unless items_discount.blank?
      #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
      reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
      reward_code_user.destroy if reward_code_user
      p "here are the items that got the discount"
      p items_discount
      all_discount_item = items_discount.sum{ |a| a[:price]}
      if pos_discount_type.blank?
        calculated_discount_from_reward = all_discount_item
      elsif pos_discount_type.is_discount_percentage?
        calculated_discount_from_reward = ((pos_discount_type.discount_amount.to_f/100) * all_discount_item).round(2)
      elsif pos_discount_type.is_discount_price?
        calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? (all_discount_item - pos_discount_type.discount_amount) : all_discount_item).round(2)
      elsif pos_discount_type.is_discount_fixed_price?
        calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : all_discount_item).round(2)
      end
      items_discount.each do |item|
        RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => item[:price], :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum], :level_size_id => item[:level_size_id] )
      end
      pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
      pos_reward_check.user_id = (user.id rescue nil)
      pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
      pos_reward_check.save
      if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
        reward.claimed_by(user)
        Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
      end
      if chain.auto_loyalty && due_total == 0
        xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, calculated_discount_from_reward)
        Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
      end
      render :json => {:status => true, :discount => (calculated_discount_from_reward*-1), :discount_items => items_discount, :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
    else
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.user_id = (user.id rescue nil)
      pos_reward_check.error_description = "Reward requirement is not met."
      pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
      pos_reward_check.save
      #reward_transaction.update_column(:pos_used, true)
      render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
    end
  end

  def process_bridg_discount
    chain = @pos_location.chain
    location = @pos_location.restaurant
    case params["request_type"]
      when "0"
        reward_ids = chain.rewards.map(&:id)
        reward_code = params["reward_code"].to_s.upcase
        reward_code_setting = PartnerCodeSetting.where("chain_id = #{chain.id}").first rescue nil
        reward_code_setting = JSON.parse(reward_code_setting.to_json) rescue nil
        reward_code_setting ||= PartnerCodeSetting::DEFAULT_SETTING_REWARDCODE
        if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"])
          if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
            reward_code = reward_code.to_ean_13_format
          end
        end
        reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
        pos_reward_check = PosRewardCheck.new(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params["check"], :reward_code => reward_code)
        reward_code_user = PartnerRewardCode.where(:chain_id => chain.id, :code => reward_code, :used => false).first
        if reward_code_user.blank?
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = "Reward code invalid"
          pos_reward_check.save
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code invalid"} and return
        end

        if reward_code_user.expiration_date < Time.current
          reward_code_user.destroy if reward_code_user
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = "Reward code expired."
          pos_reward_check.save
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code expired"} and return
        end

        #getting check attributes
        xml_receipt = params["check"]
        check = Nokogiri::XML(xml_receipt)
        pos_xml = PosReceiptXml.new(xml_receipt)
        render :json => {:confirm => 0, :response_code => 1} and return if check.children.blank?
        check_id = check.root["id"] rescue nil
        seq_number = check.root["seq_num"] rescue nil
        revenue_center = check.search("rvc-num").text.to_s rescue nil
        employee_num =  check.search("emp-num").text.to_s rescue nil
        tbl_num =  check.search("tbl-num").text.to_s rescue nil
        payment_included = check.search("tender//type").map(&:text).include?("MobileTender") rescue false
        menu_items_list = check.search('menu-item')
        due_total = 0
        pos_used_type = "micros"

        p "here is check id and seq number -----"
        check_id = check_id.strip rescue nil
        seq_number = seq_number.strip rescue nil
        p "check id is #{check_id}"
        p "seq num is #{seq_number}"

        #reward identity
        reward = reward_code_user.reward
        render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward is invalid"} and return if reward.blank?
        partner_reward = reward.partner_reward
        render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward is invalid"} and return if partner_reward.blank?


        #validate location includes on reward
        unless partner_reward.valid_all_locations
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "POS Location is not included on Reward location settings."} and return unless partner_reward.valid_locations.parameterize.split("-").include?(location.id.to_s)
        end

        #validate day and time
        if partner_reward.partner_hours.count > 0
          receipt_date = DateTime.strptime(pos_xml.receipt_date, "%m/%d/%y %H:%M:%S")
          wday = receipt_date.wday
          hour = receipt_date.hour
          min = receipt_date.min
          tmp_date = DateTime.new(2000, 1, 1, hour, min, 0)
          partner_hour = partner_reward.partner_hours.where("partner_hours.day_of_week = ? AND partner_hours.start <= ? AND partner_hours.end >= ?", wday, tmp_date, tmp_date ).first
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward is inactive and not met with receipt date."} and return if partner_hour.blank?
        end
        
        #validate order minimum
        if partner_reward.order_minimum.to_f != 0 && partner_reward.order_minimum.to_f > pos_xml.sub_total
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Minimum Order requirement is not met."} and return
        end

        order_from_categories = partner_reward.partner_reward_categories rescue []

        if partner_reward.discount_source_price == 0
          #discount from check sub total
          if partner_reward.discount_type.blank?
            calculated_discount_from_reward = pos_xml.sub_total
          elsif partner_reward.discount_type == "1"
            calculated_discount_from_reward = ((partner_reward.discount_value.to_f/100) * pos_xml.sub_total).round(2)
          elsif partner_reward.discount_type == "0"
            calculated_discount_from_reward = (pos_xml.sub_total > partner_reward.discount_value.to_f ? partner_reward.discount_value.to_f : pos_xml.sub_total).round(2)
          end
          reward_code_user.destroy if reward_code_user
          pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
          # pos_reward_check.user_id = (user.id rescue nil)
          # pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
          pos_reward_check.save
          # if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
          #   reward.claimed_by(user)
          #   Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
          # end
          p "RENDER JSON"
          render :json => {:status => true, :discount => (calculated_discount_from_reward*-1), :discount_items => [] , :tender_id => nil, :response_code => 0, :POS_message => ""} and return
        end


        mandatory_order_from_categories = order_from_categories.select{|ofc| ofc.mandatory == true}
        optional_order_from_categories = order_from_categories.select{|ofc| ofc.mandatory == false}
        render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met.", :dev_message =>  "Order from Categories is not setup."} and return if mandatory_order_from_categories.blank?

        discount_from_category = partner_reward.partner_reward_discounts rescue []
        render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met.", :dev_message =>  "Discount from Categories is not setup."} and return if discount_from_category.blank?

        mandatory_discount_from_categories = discount_from_category.select{|dfc| dfc.mandatory == true}
        optional_discount_from_categories = discount_from_category.select{|dfc| dfc.mandatory == false}
        render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met.", :dev_message =>  "Discount from Categories is not setup."} and return if mandatory_discount_from_categories.blank?

        #order from category item :
        mandatory_order_from_sub_categories = mandatory_order_from_categories.map{|a| a.partner_category.partner_sub_categories}
        mandatory_order_from_sub_categories = mandatory_order_from_sub_categories.flatten
        mandatory_menu_items = PartnerSubCategoryMenuItem.where("partner_sub_category_id IN(?) AND restaurant_id = ?", mandatory_order_from_sub_categories.map(&:id), @pos_location.restaurant_id)

        optional_order_from_sub_categories = optional_order_from_categories.map{|a| a.partner_category.partner_sub_categories}
        optional_order_from_sub_categories = optional_order_from_sub_categories.flatten
        optional_menu_items = PartnerSubCategoryMenuItem.where("partner_sub_category_id IN(?) AND restaurant_id = ?", optional_order_from_sub_categories.map(&:id), @pos_location.restaurant_id)

        #discount from category item :
        mandatory_discount_from_sub_categories = mandatory_discount_from_categories.map{|a| a.partner_category.partner_sub_categories}
        mandatory_discount_from_sub_categories = mandatory_discount_from_sub_categories.flatten
        mandatory_discount_menu_items = PartnerSubCategoryMenuItem.where("partner_sub_category_id IN(?) AND restaurant_id = ?", mandatory_discount_from_sub_categories.map(&:id), @pos_location.restaurant_id)

        optional_discount_from_sub_categories = optional_discount_from_categories.map{|a| a.partner_category.partner_sub_categories}
        optional_discount_from_sub_categories = optional_discount_from_sub_categories.flatten
        optional_discount_menu_items = PartnerSubCategoryMenuItem.where("partner_sub_category_id IN(?) AND restaurant_id = ?", optional_discount_from_sub_categories.map(&:id), @pos_location.restaurant_id)

        items_discount = []
        qualified_order = 0
        items_check_buy = []
        xml_menu_items = menu_items_list
        xml_menu_items.each do |menu_item|
          item = {}
          item[:menu_item_id] = menu_item.attr("id").strip rescue nil
          item[:menu_item_id] = menu_item["ItemId"].to_s.strip if item[:menu_item_id].blank?

          item[:name] = menu_item.search("name").text.strip rescue nil
          item[:name] = menu_item["Name"].to_s.strip if item[:name].blank?

          item[:quantity] = menu_item.search("quantity").text.to_i rescue 0
          item[:quantity] = menu_item["Quantity"].to_i if item[:quantity] == 0

          item[:total_amount] = menu_item.search("total-amount").text.to_f rescue 0
          item[:total_amount] = menu_item["TotalPrice"].to_f if item[:total_amount] == 0

          item[:price_per_item] = ((item[:total_amount]/item[:quantity]) rescue 0)
          item[:price_per_item] = menu_item["UnitPrice"].to_f if item[:price_per_item] == 0

          item[:level_size_id] = menu_item.search("size-id").text rescue nil
          item[:level_size_id] = menu_item["SizeId"].to_i  if item[:level_size_id].blank? &&  !menu_item["SizeId"].blank?
          item[:level_size_id] = nil if item[:level_size_id].blank?

          p "---- item number  #{item[:menu_item_id]}"
          p "------- chain = #{chain.id}--"
          p "=------ pos location = #{@pos_location.id}"

          qualified = false
          mandatory = false
          #checking qualified for order category
          pos_menu_item = PartnerMenuItem.select("id").where("item_number = ? AND chain_id = ? AND id IN (?)", item[:menu_item_id], chain.id,  mandatory_menu_items.map(&:partner_menu_item_id)).first
          if pos_menu_item
            qualified = true
            mandatory = true
            qualified_order += 1
          else
            pos_menu_item = PartnerMenuItem.select("id").where("item_number = ? AND chain_id = ? AND id IN (?)", item[:menu_item_id], chain.id, optional_menu_items.map(&:partner_menu_item_id)).first
            if pos_menu_item
              qualified = true
              mandatory = false
            end
          end
          item[:qualified] = qualified
          item[:mandatory] = mandatory

          qualified = false
          mandatory = false
          #checking qualified for discount category
          pos_menu_item = PartnerMenuItem.select("id").where("item_number = ? AND chain_id = ? AND id IN (?)", item[:menu_item_id], chain.id,  mandatory_discount_menu_items.map(&:partner_menu_item_id)).first
          if pos_menu_item
            qualified = true
            mandatory = true
          else
            pos_menu_item = PartnerMenuItem.select("id").where("item_number = ? AND chain_id = ? AND id IN (?)", item[:menu_item_id], chain.id, optional_discount_menu_items.map(&:partner_menu_item_id)).first
            if pos_menu_item
              qualified = true
              mandatory = false
            end
          end
          item[:qualified_discount] = qualified
          item[:mandatory_discount] = mandatory
          items_check_buy << item
        end

        #validate min order from category
        #validate order minimum
        if partner_reward.minimum_count_order_from_category.to_i != 0 && partner_reward.minimum_count_order_from_category.to_i > qualified_order
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Minimum Order requirement from category is not met."} and return
        end

        p "ITEM CHECK BUY"
        p items_check_buy

        grouped_items_check_buy = []
        items_check_buy.each_with_index do |item_check, i|
          if item_check[:mandatory]
            item_check[:all_price_with_optional_item] = item_check[:price_per_item]
            menu_item_combo_sum = [item_check[:menu_item_id]]
            p '----'
            p item_check[:menu_item_combo_sum]
            (i+1).upto(items_check_buy.length-1) do |idx|
              optional_item = items_check_buy[idx]
              unless optional_item[:mandatory]
                item_check[:all_price_with_optional_item] += optional_item[:price_per_item]
                menu_item_combo_sum << optional_item[:menu_item_id]
              else
                break
              end
            end
          end
          item_check[:menu_item_combo_sum] = menu_item_combo_sum.join("-") if menu_item_combo_sum
          grouped_items_check_buy << item_check
        end

        p "GROUPED ITEMS CHECK BUY"
        p grouped_items_check_buy

        if partner_reward.discount_order == 0
          ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:mandatory]}.sort{|a,b| a[:all_price_with_optional_item] <=> b[:all_price_with_optional_item]}
        else
          ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:mandatory]}.sort{|a,b| b[:all_price_with_optional_item] <=> a[:all_price_with_optional_item]}
        end

        p "ORDERED ITEMS CHECK BUY"
        p ordered_item_check_buy

        item_discounted = 0
        #what discount that reward offered
        #reward_general_menu_item_ids.each do |reward_general_menu_item_id|
        ordered_item_check_buy.each do |filtered_item|
          next if filtered_item[:price_per_item] <= 0 && pos_used_type == "northkey"
          if filtered_item[:qualified]
            reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]).count
            qty_selected = ordered_item_check_buy.select{|a| a[:mandatory] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
            if filtered_item[:qualified_discount] && filtered_item[:mandatory_discount]
              items_discount << {:menu_item_id => filtered_item[:menu_item_id], :price => filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]}
              item_discounted += 1
              break if item_discounted >= partner_reward.discount_count_from_category.to_i
              #CODE FOR OPTIONAL MENU ITEM
              optional_menu_discount_items = []
              (grouped_items_check_buy.index(filtered_item)+1).upto(grouped_items_check_buy.length-1) do |idx|
                opt_filtered_item = grouped_items_check_buy[idx] rescue nil
                break if opt_filtered_item.blank?
                break if opt_filtered_item[:mandatory]
                if opt_filtered_item[:qualified]
                  reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => opt_filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]).count
                  qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
                  if opt_filtered_item[:qualified_discount] && !opt_filtered_item[:mandatory_discount]
                    #optional_menu_discount_items <<  opt_filtered_item
                    items_discount << {:menu_item_id => opt_filtered_item[:menu_item_id], :price => opt_filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => opt_filtered_item[:level_size_id]}
                    item_discounted += 1
                    break if item_discounted >= partner_reward.discount_count_from_category.to_i
                  end
                end
              end
            end
            # break
          end
        end

        unless items_discount.blank?
          #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
          # reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
          reward_code_user.destroy if reward_code_user
          p "here are the items that got the discount"
          p items_discount
          all_discount_item = items_discount.sum{ |a| a[:price]}
          if partner_reward.discount_type.blank?
            calculated_discount_from_reward = all_discount_item
          elsif partner_reward.discount_type == "1"
            calculated_discount_from_reward = ((partner_reward.discount_value.to_f/100) * all_discount_item).round(2)
          elsif partner_reward.discount_type == "0"
            calculated_discount_from_reward = (all_discount_item > partner_reward.discount_value.to_f ? partner_reward.discount_value.to_f : all_discount_item).round(2)
          end
          items_discount.each do |item|
            RewardDiscount.create(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => calculated_discount_from_reward, :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum] )
          end
          pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
          # pos_reward_check.user_id = (user.id rescue nil)
          # pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
          pos_reward_check.save
          # if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
          #   reward.claimed_by(user)
          #   Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
          # end
          p "RENDER JSON"
          render :json => {:status => true, :discount => (calculated_discount_from_reward*-1), :discount_items => items_discount, :tender_id => nil, :response_code => 0, :POS_message => ""} and return
        else
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.user_id = (user.id rescue nil)
          pos_reward_check.error_description = "Reward requirement is not met."
          # pos_reward_check.reward_transaction_id = reward_transaction.try(:id)
          pos_reward_check.save
          #reward_transaction.update_column(:pos_used, true)
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
        end
      when "1"
    end
  end

end
