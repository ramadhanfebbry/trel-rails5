require 'nokogiri'
class Pos::V1::UploadController < Pos::BaseController

  def check_status
    render :json => {:status => @pos_location.check_status ? 0 : 1, :response_code => 0, :last_received_at => @pos_location.last_check_upload_received_at}
  end

  def check_upload
    xml_in_nokogiri_format = Nokogiri::XML(params[:check_batch])
    render :json => {:confirm => 0, :response_code => 1} and return if !@pos_location.check_status or xml_in_nokogiri_format.children.blank?
    PosCheckUpload.create(
        :pos_location_id => @pos_location.id,
        :chain_id => @pos_location.chain_id,
        :xml_data => params[:check_batch],
        :check_id => (xml_in_nokogiri_format.root["id"] rescue nil),
        :sequence_number =>  (xml_in_nokogiri_format.root["seq_num"] rescue nil),
        :barcode =>  xml_in_nokogiri_format.xpath('//barcode').text,
        :status => PosCheckUpload::STATUS[:NEW]
    )
    #pos_receipt = PosReceipt.new
    #pos_receipt.status = PosReceipt::STATUS[:RECEIVED]
    #pos_receipt.barcode = xml_in_nokogiri_format.xpath('//barcode').text
    #pos_receipt.receipt_time = xml_in_nokogiri_format.xpath('//date').text
    #pos_receipt.total = xml_in_nokogiri_format.xpath('//total').text
    #pos_receipt.tips = xml_in_nokogiri_format.xpath('//direct-tips').text
    #pos_receipt.tax = xml_in_nokogiri_format.xpath('//tax').text
    #pos_receipt.item_sales = xml_in_nokogiri_format.xpath('//item-sales').text
    #pos_receipt.menu_items = []
    #xml_in_nokogiri_format.xpath('//menu-item').each do |menu_item|
    #  item = {}
    #  item[:id] = menu_item.attr("id")
    #  item[:quantity] =  menu_item.xpath('quantity').text
    #  item[:name] = menu_item.xpath('name').text
    #  item[:total_amount] =  menu_item.xpath('total-amount').text
    #  item[:category] = menu_item.xpath('category').text
    #  pos_receipt.menu_items << item
    #end
    #pos_receipt.save

    render :json => {:confirm => 1, :response_code => 0}

  end

  def menu_status
    render :json => {:status => @pos_location.menu_upload_status ? 0 : 1, :response_code => 0, :last_received_at => @pos_location.last_menu_upload_received_at}
  end

  def menu_upload
    xml_in_nokogiri_format = Nokogiri::XML(params[:menu_data])
    p "---------------"
    p @pos_location
    p @pos_location.check_status
    p params[:menu_data]
    p
    render :json => {:confirm => 0, :response_code => 1} and return if !@pos_location.check_status or xml_in_nokogiri_format.children.blank?
    pos_menu = PosMenu.create(
        :pos_location_id => @pos_location.id,
        :chain_id => @pos_location.chain_id,
        :menu_data => params[:menu_data]
    )
    Delayed::Job.enqueue(MenuExtractionJob.new(pos_menu))
    PosMenuUpload.create(
        :pos_location_id => @pos_location.id,
        :chain_id => @pos_location.chain_id,
        :xml_data => params[:menu_data]
    )
    render :json => {:confirm => 1, :response_code => 0}

  end

  def barcode
    chain = @pos_location.chain
    codes = []
    size = params[:size].to_i
    batch = BarcodeBatch.create(:pos_location_id => @pos_location.id, :size => params[:size], :status => 1)
    1.upto(size) do |i|
      codes << Barcode.generate(batch, chain)
      end
    render :json => {:status => true, :barcode => codes, :batch => batch.id}
  end

  def barcode_check_submit
    xml_receipt = params["check_xml"] || params["checkxml"]
    barcode = params["barcode"]
    #p xml_receipt
    #p barcode
    xml_in_nokogiri_format = Nokogiri::XML(xml_receipt)
    render :json => {:confirm => 0, :response_code => 1} and return if xml_in_nokogiri_format.children.blank?
    check_id = xml_in_nokogiri_format.root["id"]  rescue nil
    sequence_number =   xml_in_nokogiri_format.root["seq_num"] rescue nil
    revenue_center = xml_in_nokogiri_format.search("rvc-num").text.to_s rescue nil
    employee_num =  xml_in_nokogiri_format.search("emp-num").text.to_s rescue nil
    tbl_num =  xml_in_nokogiri_format.search("tbl-num").text.to_s rescue nil
    payment_included = xml_in_nokogiri_format.search("tender//type").map(&:text).include?("MobileTender") rescue false

    check_id = check_id.strip rescue nil
    sequence_number = sequence_number.strip rescue nil
    revenue_center = revenue_center.strip rescue nil

    p "----barcode submitted is #{barcode}"
    p "----check id is #{check_id}"
    p "---- sequence number is #{sequence_number}"
    p "---- revenue center is #{revenue_center}"

    #p check_id
    #p sequence_number

    render :json => {:confirm => 0, :response_code => 1} and return if check_id.blank?
    render :json => {:confirm => 0, :response_code => 1} and return if sequence_number.blank?

    pos_check_upload = PosCheckUpload.where(:pos_location_id => @pos_location.id, :sequence_number => sequence_number, :check_id => check_id).first
    #p pos_check_upload
    if pos_check_upload.blank?
      PosCheckUpload.create(
          :pos_location_id => @pos_location.id,
          :chain_id => @pos_location.chain_id,
          :xml_data => xml_receipt,
          :check_id => check_id,
          :barcode =>  barcode,
          :status => PosCheckUpload::STATUS[:NEW],
          :sequence_number =>  sequence_number,
          :rvc_num => revenue_center,
          :emp_num => employee_num,
          :tbl_num => tbl_num,
          :payment_included => payment_included
      )
      check_type = 0
      render :json => {:confirm => 1, :response_code => 0, :check_type => check_type} and return
    elsif pos_check_upload &&  pos_check_upload.service.eql?(PosCheckUpload::SERVICE[:TABLE]) && pos_check_upload.code_type.eql?("paycode")
      pos_check_upload.update_attributes(
          :barcode => barcode,
          :xml_data => xml_receipt,
          :rvc_num => revenue_center,
          :emp_num => employee_num,
          :tbl_num => tbl_num,
          :payment_included => payment_included
      )
      render :json => {:confirm => 1, :response_code => 0, :check_type => 1} and return
    elsif pos_check_upload && pos_check_upload.service.eql?(PosCheckUpload::SERVICE[:TABLE]) && pos_check_upload.status.eql?(3)
      render :json => {:confirm => 0, :response_code => 1} and return
    elsif pos_check_upload && pos_check_upload.service.eql?(PosCheckUpload::SERVICE[:COUNTER])
      barcode ||= pos_check_upload.barcode
      #p barcode
      pos_check_upload.update_attributes(
          :barcode => barcode,
          :xml_data => xml_receipt,
          :rvc_num => revenue_center,
          :emp_num => employee_num,
          :tbl_num => tbl_num,
          :payment_included => payment_included
      )
      #p pos_check_upload
      check_type = 1
      render :json => {:confirm => 1, :response_code => 0, :check_type => check_type} and return
    elsif pos_check_upload && !pos_check_upload.status.eql?(3)
      old_barcode = pos_check_upload.barcode
      barcode ||= pos_check_upload.barcode
      #p barcode
      pos_check_upload.update_attributes(
          :old_barcode => old_barcode,
          :barcode => barcode,
          :xml_data => xml_receipt,
          :status => PosCheckUpload::STATUS[:UPDATED],
          :rvc_num => revenue_center,
          :emp_num => employee_num,
          :tbl_num => tbl_num,
          :payment_included => payment_included
      )
      #p pos_check_upload
      check_type = 1
      render :json => {:confirm => 1, :response_code => 0, :check_type => check_type} and return
    else
      render :json => {:confirm => 0, :response_code => 1} and return
    end
  end

  def usercode
    p "usercode"*50
    p params

    xml_receipt = params["check_xml"]
    xml_receipt = params["checkxml"] if xml_receipt.blank?
    xml_receipt = params["check_json"] if xml_receipt.blank?

    usercode = params["usercode"]

    @chain = @pos_location.chain
    location = @pos_location.restaurant
    user_code_user = nil

    if request.content_type == "application/json"
      check = xml_receipt
      render :json => {:confirm => 0, :response_code => 1} and return if check.blank?
      check_id = check["CheckId"].to_s
      render :json => {:confirm => 0, :response_code => 1} and return if check_id.blank?
      sequence_number = check["SeqNum"].to_s
      render :json => {:confirm => 0, :response_code => 1} and return if sequence_number.blank?
      revenue_center = check["RvcNum"].to_s
      employee_num = check["EmpNum"].to_s
      tbl_num = check["TblNum"].to_s
      payment_included = check["Payments"].map{|a| a["Type"]}.include?("MobileTender") rescue false
      code_type = check["CodeType"]
      pos_type = "northkey"
    else
      check = Nokogiri::XML(xml_receipt)
      render :json => {:confirm => 0, :response_code => 1} and return if check.children.blank?
      check_id = check.root["id"] rescue nil
      sequence_number =   check.root["seq_num"] rescue nil
      revenue_center = check.search("rvc-num").text.to_s rescue nil
      employee_num =  xml_in_nokogiri_format.search("emp-num").text.to_s rescue nil
      tbl_num =  xml_in_nokogiri_format.search("tbl-num").text.to_s rescue nil
      payment_included = check.search("tender//type").map(&:text).include?("MobileTender") rescue false
      code_type = check.root["code_type"]
      pos_type = "micros"
    end
    p "----code type is #{code_type}"

    if code_type.eql?("paycode")
      barcode, user, user_session = AppcodeSetting.paycode_convert(@chain, usercode)
    else
      code_type = "usercode" if code_type.blank?
      barcode, user, user_session, static = AppcodeSetting.usercode_convert(@chain, usercode)
      unless static
        user_code_user = UserCodeUser.where(:chain_id => @chain.try(:id), :user_code => barcode).first
        user = user_code_user.try(:user)
        if user_code_user.blank? && pos_type == "northkey"
          rt = RewardTransaction.where(:id => usercode, :created_at => Time.zone.now.beginning_of_day..Time.zone.now.end_of_day).first
          user = rt.try(:user)
          user_session = nil
          user_code_user = nil
          barcode = usercode
        end
      end
    end
    loyalty_response = Micros::Loyalty.execute(@chain, @pos_location, user, user_session, user_code_user, location, xml_receipt, check_id, sequence_number, revenue_center, payment_included, code_type, barcode, employee_num, tbl_num)
    p "this is loyalty response for #{user.try(:id)} - #{user.try(:email)}, check_id = #{check_id}, sequence_number = #{sequence_number} = #{loyalty_response}"
    render :json => loyalty_response
  end

  def get_code_type
    chain = @pos_location.chain
    code, code_type, user, user_code_type, user_session = Barcode.convert_and_get_code_type(chain, params[:code])
    if code && code_type
      response_code = 0
      code_type = code_type
      if user_session && user_session.expired_at && user_session.expired_at < Time.zone.now
        code_status = 4
      elsif user_session && user_session.used
        code_status = 3
      elsif user_session
        code_status = 1
      end
    else
      response_code = 1
      code_type = 0
      code_status = 2
    end
    # code_status 1=valid 2=notfound 3=used 4=expired
    render :json =>  {confirm: 0, response_code: response_code, code_type: code_type, code_status: code_status, :user_session_id => user_session.try(:id)}
  end

  def pull_check_payment
    check_id = params[:check_num].to_s
    seq_num = params[:check_seq].to_s
    rvc_num = params[:revenue_center].to_s
    chain = @pos_location.chain
    pos_check_upload = PosCheckUpload.where(:chain_id => chain.id, :check_id => check_id, :sequence_number => seq_num, :rvc_num => rvc_num).first
    render :json =>  {status: false, :status_code => 1} and return if pos_check_upload.blank?
    # user = pos_check_upload.try(:user)
    receipts = Receipt.where(:chain_id => chain.id, :pos_check_upload_id => pos_check_upload.id)
    render :json => {status: false, :status_code => 2} and return if receipts.blank?
    user = receipts.map(&:user).uniq.first rescue nil
    render :json => {status: false, :status_code => 2} and return if user.blank?
    uph = UserPaymentHistory.where(:receipt_id => receipts.map(&:id), :pos_check_upload_id => pos_check_upload.try(:id)).first
    authorization_code = uph.try(:authorization_code)
    if uph && authorization_code.blank?
      start_time = uph.created_at - 30.seconds
      end_time = uph.created_at + 30.seconds
      payment = PaymentHistory.where(:chain_id => chain.id, :user_id => user.try(:id), :status => 0, :action => "payment", :created_at => start_time .. end_time).first
      authorization_code = payment.try(:mp_payment_id)
    end

    if authorization_code.blank?
      if uph.blank?
        json_response = {status_code: 2, status: false}
      else
        json_response = {status_code: 3, status: false, :message => uph.try(:error_description) }
      end
      render :json => json_response and return
    else
      pos_check_upload.pull_payment!
      render :json =>  {status_code: 0, status: true, :amount => uph.try(:amount), :added_tip_amount => uph.try(:tip), :authorization => authorization_code, :pay_code => pos_check_upload.try(:user_code) } and return
    end
  end


  #update paycode status used when payment successfully created. Called by Mobile Payment MP
  def update_mp_processed_paycode
    chain = @pos_location.chain
    user_session = UserSession.where("code = ? AND chain_id = ? AND code_type = ? AND used IS FALSE AND active IS TRUE", params[:code], chain.id, AppcodeSetting::CODE_TYPES["PAYCODE"]).order("created_at DESC").first
    if user_session && user_session.expired_at && (user_session.expired_at < Time.zone.now)
      render :json => {:status => true, :user_session_id => nil, :message => "expired"}
    elsif user_session.blank?
      render :json => {:status => true, :user_session_id => nil, :message => "invalid"}
    else
      render :json => {:status => true, :user_session_id => user_session.try(:id), :chain_id => user_session.try(:chain_id), :user_id => user_session.try(:user_id)}
    end
  end

  def get_check_detail
    chain = @pos_location.chain
    pos_check_upload = PosCheckUpload.where(:chain_id => chain.id, :pos_location_id => @pos_location.id, :check_id => params[:check_id], :sequence_number => params[:seq_num]).first
    render :json =>  {status: false, :message => "Could not find the check"} and return if pos_check_upload.blank?
    receipt = pos_check_upload.receipt.last_transaction rescue nil
    render :json =>  {status: false, :message => "Could not find the receipt"} and return if receipt.blank?
    transaction_date = receipt.receipt_date.blank? ? receipt.issue_date : receipt.receipt_date rescue "-"
    transaction_time = receipt.time_stamp rescue "-"
    pos_xml = PosReceiptXml.new(pos_check_upload.xml_data)
    render :json =>  {
        status: true,
        :check => {
            :chain_id => chain.id, :chain_name => chain.name, :restaurant_id => @pos_location.restaurant_id,
            :restaurant_name => @pos_location.restaurant.try(:name), :barcode => pos_check_upload.barcode,
            :check_id => pos_xml.check_id, :seq_num => pos_xml.seq_num, :items => pos_xml.list_item,
            :transaction_date => transaction_date, :transaction_time => transaction_time,
            :payment_total => pos_xml.total, :sub_total => pos_xml.sub_total, :tax => pos_xml.tax
        }
    }
  end

  def do_pull_payment_alert
    redirect_to "/pos/v1/check/payments/#{params["emp_num"]}/#{params["rvc"]}?apikey=#{params["apikey"]}"
  end

  def pull_payment_alert
    request_time = Time.zone.now
    chain = @pos_location.chain
    pos_check_uploads = MicrosCheckData.where(:chain_id => chain.id, :pos_location_id => @pos_location.id, :rvc_num => params[:rvc], :emp_num => params[:emp_num], :created_at => [request_time.beginning_of_day..request_time.end_of_day]).unpulled_payment
    checks = []
    pos_check_uploads.each do |pcu|
      uph_count = UserPaymentHistory.where(:pos_check_upload_id => pcu.pos_check_upload_id).count
      if uph_count > 0
        checks << {table_id: pcu.table_num, check_num: pcu.check_id, seq_num: pcu.seq_num}
      end
    end
    render :json =>  {
        status_code: 0,
        status: true,
        payment_count: checks.size,
        payments: checks
    }
  end


end