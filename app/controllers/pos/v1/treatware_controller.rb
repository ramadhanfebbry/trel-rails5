class Pos::V1::TreatwareController < Pos::BaseController

  skip_before_action :authorize_apikey!, :only => :tw_void_offers
  before_action :authorize_health_apikey!, :only => :tw_get_info

  def tw_get_info
    render :json => {"posLoyaltyGetInfoResult" => {"statusCode" => 0, "resultDescription" => "Success", "apiVersion" => "1.0"}} and return
  end

  def tw_apply_offers
    p params
    treatware_params_check = params["treatware"]
    treatware_json_check = treatware_params_check["posLoyaltyApplyOffersRequest"]
    render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "Invalid check", "statusCode" => 5}} and return if treatware_json_check.blank?
    treatware_check = TreatwareCheck.new(treatware_json_check)
    store_id = treatware_check.store_id
    authorize_apikey_for_treatware(store_id, "posLoyaltyApplyOffersResult")
    chain = @pos_location.chain
    code, code_type, user, user_code_type = Barcode.convert_and_get_code_type(chain, treatware_check.short_code.to_s)

    render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "#{AppcodeSetting::CODE_TYPES.invert[code_type]} added", "statusCode" => 0, "shortCode" => treatware_check.short_code.to_s}} and return if code && code_type && [AppcodeSetting::CODE_TYPES["USERCODE"], AppcodeSetting::CODE_TYPES["PAYCODE"]].include?(code_type)

    if code && code_type && code_type.eql?(AppcodeSetting::CODE_TYPES["GIFTCODE"])
      if treatware_check.sub_total == 0
        render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "GIFTCODE added", "statusCode" => 0, "shortCode" => treatware_check.short_code.to_s, :dev_message => "because subtotal is 0, payment will be ignored."}} and return
      end

      gift_code_user = GiftCodeUser.where(:chain_id => chain.id, :gift_code => code).first
      if gift_code_user.blank? || (gift_code_user && gift_code_user.expired_at && (gift_code_user.expired_at < Time.zone.now))
        render :json => {"posLoyaltyApplyOffersResult" => treatware_json_check.merge!({"resultDescription" => "Invalid Giftcard Code", "statusCode" => 15, "shortCode" => treatware_check.short_code.to_s})} and return
      end

      user = gift_code_user.user

      # paid_checks = GiftCardRedemHistory.where("receipt_number = ? AND chain_id = ? AND date(TIMEZONE('UTC', created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') = date('#{(Time.zone.now).to_date}')", treatware_check.receipt_number, chain.id).order("created_at ASC")
      # redeems =  paid_checks.select{|pc| pc.transaction_type == 0}
      # voids = paid_checks.select{|pc| pc.transaction_type == 1}
      # if redeems.count > voids.count
      #   render :json => {"posLoyaltyApplyOffersResult" => treatware_json_check.merge!({"resultDescription" => "One giftcard payment per check", "statusCode" => 16, "shortCode" => treatware_check.short_code.to_s})} and return
      # end
      response_status, response_code, response_message, card_number, balance = GiftCard.get_credit_card_number_and_detail(store_id, user)
      unless response_status
        render :json => {"posLoyaltyApplyOffersResult" => treatware_json_check.merge!({"resultDescription" => "[#{response_code}] - #{response_message}", "statusCode" => 14, "shortCode" => treatware_check.short_code.to_s})} and return
      end

      should_be_paid_amount = treatware_check.total
      if balance > 0 && balance < treatware_check.total
        should_be_paid_amount = balance
      end
      response_status, response_code, response_message, transaction_id = GiftCard.redemption(store_id, card_number, should_be_paid_amount)
      unless response_status
        render :json => {"posLoyaltyApplyOffersResult" => treatware_json_check.merge!({"resultDescription" => "[#{response_code}] - #{response_message}", "statusCode" => 14, "shortCode" => treatware_check.short_code.to_s})} and return
      end

      gcrh = GiftCardRedemHistory.create(:user_id => user.try(:id), :receipt_number => treatware_check.receipt_number, :transaction_id => transaction_id, :amount => should_be_paid_amount, :chain_id => chain.id)
      result =  {"posLoyaltyApplyOffersResult" =>  treatware_json_check.merge!({"statusCode" => 0 })}
      result["posLoyaltyApplyOffersResult"]["order"]["payments"] =  [] if result["posLoyaltyApplyOffersResult"]["order"]["payments"].blank?
      result["posLoyaltyApplyOffersResult"]["order"]["payments"] << {
          "type" => "MOBILE_GIFT_CARD",
          "amount" => should_be_paid_amount.to_f,
          "shortCode" => treatware_check.short_code.to_s,
          "reference" => "giftcard_transaction_#{gcrh.id}"
      }
      #auto_loyalty_for_giftcode(@pos_location, user, treatware_check, code, "GiftCode", user_code_type, user_session, code_type)
      render :json => result and return
    end

    reward_ids = chain.rewards.map(&:id)

    pos_reward_check = PosRewardCheck.new(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params["treatware"]["posLoyaltyApplyOffersRequest"], :reward_code => code,
                                          :pos_used_type => Chain::POS_USED_TYPE["TREATWARE"])
    reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => code, :active => true).first
    if reward_code_user.blank?
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "Reward code invalid."
      pos_reward_check.save
      render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "Reward code invalid", "statusCode" => 5, "shortCode" => treatware_check.short_code.to_s}} and return
    end
    reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", code, reward_ids)
    if reward_transactions.blank?
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "Reward code invalid."
      pos_reward_check.save
      render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "Reward code invalid", "statusCode" => 5, "shortCode" => treatware_check.short_code.to_s}} and return
    end

    reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
    reward_code_setting = JSON.parse(reward_code_setting) rescue nil
    reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
    reward_code_expire = reward_code_setting["timer"].to_i
    if reward_code_expire != 0
      reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
    else
      reward_transaction = reward_transactions.order("created_at DESC").first
    end

    if reward_transaction.blank?
      reward_code_user.destroy if reward_code_user
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "Reward code expired."
      pos_reward_check.save
      render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "Reward code expired", "statusCode" => 5, "shortCode" => treatware_check.short_code.to_s}} and return
    end

    reward_transaction.update_column(:check_number, treatware_check.receipt_identifier)
    reward = reward_transaction.reward
    user = reward_transaction.user

    reward_menu_items = reward.reward_menu_items
    required_menu_items = reward_menu_items.select{|a| a.required }
    optional_menu_items = reward_menu_items.select{|a| !a.required }
    reward_general_menu_item_ids = reward_menu_items.map(&:general_menu_item_id)

    pos_discount_type = reward.pos_discount_type

    items_check_buy = TreatwareCheck.get_line_items(chain, treatware_check.line_items)
    lowest = chain.pos_discount_setting.eql?(Chain::POS_DISCOUNT_TYPE["Discount Least priced qualifying item"]) ? true : false
    items_check_buy = TreatwareCheck.sort_items_by_price(items_check_buy, lowest)
    #
    # grouped_items_check_buy = []
    # ungrouped_check_buy.each do |item_check|
    #   existing_group = grouped_items_check_buy.select{|a| a[:menu_item_id] == item_check[:menu_item_id]}.first
    #   if existing_group
    #     existing_group[:quantity] += item_check[:quantity]
    #     existing_group[:price] = item_check[:price]
    #     grouped_items_check_buy.delete_if{|group| group[:menu_item_id] == existing_group[:menu_item_id]}
    #     grouped_items_check_buy << existing_group
    #   else
    #     grouped_items_check_buy << item_check
    #   end
    # end

    #non menu item discount based
    if pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?
      check_subtotal = treatware_check.sub_total
      total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", treatware_check.pos_order_id, treatware_check.short_code, @pos_location.restaurant_id])
      subtotal_after_discount = check_subtotal - total_discount_registered
      if subtotal_after_discount > 0
        discount_applied = subtotal_after_discount > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : subtotal_after_discount
        reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
        RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => treatware_check.pos_order_id, :seq_num => treatware_check.short_code, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
        pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
        pos_reward_check.user_id = (user.id rescue nil)
        pos_reward_check.save
        if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
          reward.claimed_by(user)
          Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
        end
        child_item_for_xpient_discount = [{
            "discount" => discount_applied,
            "description" => "$#{discount_applied} Discount",
            "reference" => "reward_transaction_#{reward_transaction.id}"
        }]
        response_discount_loyalty_applied = treatware_check.build_discount_for_discount_applied_treatware(treatware_json_check, child_item_for_xpient_discount)
        render :json => response_discount_loyalty_applied and return
      else
        pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
        pos_reward_check.user_id = (user.id rescue nil)
        pos_reward_check.error_description = "Reward requirement is not met."
        pos_reward_check.save
        render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
      end
    elsif pos_discount_type && pos_discount_type.is_non_menu_item_based_percentage_off?
      check_subtotal = treatware_check.sub_total
      total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", treatware_check.pos_order_id, treatware_check.short_code, @pos_location.restaurant_id])
      subtotal_after_discount = check_subtotal - total_discount_registered
      if subtotal_after_discount > 0
        discount_applied = ((pos_discount_type.discount_amount.to_f/100) * subtotal_after_discount).round(2)
        reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
        RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => treatware_check.pos_order_id, :seq_num => treatware_check.short_code, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
        pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
        pos_reward_check.user_id = (user.id rescue nil)
        pos_reward_check.save
        if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
          reward.claimed_by(user)
          Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
        end
        child_item_for_xpient_discount = [{
                                              "discount" => discount_applied,
                                              "description" => "#{pos_discount_type.discount_amount}% Discount Off",
                                              "reference" => "reward_transaction_#{reward_transaction.id}"
                                          }]
        response_discount_loyalty_applied = treatware_check.build_discount_for_discount_applied_treatware(treatware_json_check, child_item_for_xpient_discount)
        render :json => response_discount_loyalty_applied and return
      else
        pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
        pos_reward_check.user_id = (user.id rescue nil)
        pos_reward_check.error_description = "Reward requirement is not met."
        pos_reward_check.save
        render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
      end
    end

    items_discount, error_message = treatware_check.check_discount(@pos_location, pos_discount_type, reward_menu_items, items_check_buy)

    #preset item discount if there not suitable combo
    #items_discount = [] if !items_discount.blank? && !required_menu_items.count.eql?(items_discount.count)

    unless items_discount.blank?
      #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
      reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
      reward_code_user.destroy if reward_code_user

      p "here are the items that got the discount"
      p items_discount
      child_item_for_xpient_discount = treatware_check.parent_line_item_discount(@pos_location, items_discount, reward_transaction)
      child_item_for_xpient_discount = child_item_for_xpient_discount.flatten

      p "======LOYALTY DISCOUNT HASH========="
      p treatware_check.line_items
      p child_item_for_xpient_discount
      p "===================================="
      p "====== RESPONSE DISCOUNT LOYALTY======"
      response_discount_loyalty_applied = treatware_check.build_discount_for_discount_applied_treatware(treatware_json_check, child_item_for_xpient_discount)
      p response_discount_loyalty_applied
      p "======================================="
      pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
      pos_reward_check.user_id = (user.id rescue nil)
      pos_reward_check.save
      if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])  || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
        reward.claimed_by(user)
        Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
      end
      render :json => response_discount_loyalty_applied and return
    else
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.user_id = (user.id rescue nil)
      pos_reward_check.error_description = "Reward requirement is not met."
      pos_reward_check.save
      #reward_transaction.update_column(:pos_used, true)
      error_message = error_message.blank? ? "Reward requirement is not met." : error_message
      render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => error_message, "statusCode" => 5, "shortCode" => treatware_check.short_code.to_s}} and return
    end
  end

  def tw_void_offers
    p params
    treatware_params_check = params["treatware"]
    treatware_json_check = treatware_params_check["posLoyaltyVoidOffersRequest"]
    render :json => {"posLoyaltyVoidOffersResult" => {"resultDescription" => "Invalid check", "statusCode" => 5}} and return if treatware_json_check.blank?
    voided_value = treatware_json_check["voided"]
    render :json => {"posLoyaltyVoidOffersResult" => {"resultDescription" => "Invalid request to void", "statusCode" => 5}} and return if voided_value.blank?
    treatware_check = TreatwareCheck.new(treatware_json_check)
    store_id = treatware_check.store_id
    authorize_apikey_for_treatware(store_id, "posLoyaltyVoidOffersResult")
    chain = @pos_location.chain
    if voided_value.include?("giftcard_transaction")
      gcrh_id = voided_value.split("_").last
      gcrh = GiftCardRedemHistory.find(gcrh_id)

      response_status, response_code, response_message, card_number, balance = GiftCard.get_credit_card_number_and_detail(store_id, gcrh.user)
      unless response_status
        render :json => {"posLoyaltyVoidOffersResult" => treatware_json_check.merge!({"resultDescription" => "[#{response_code}] - #{response_message}", "statusCode" => 14, "shortCode" => treatware_check.short_code.to_s})} and return
      end

      response_status, response_code, response_message, transaction_id = GiftCard.void_redeem(store_id, gcrh.user, card_number, gcrh.transaction_id)
      unless response_status
        render :json => {"posLoyaltyVoidOffersResult" => treatware_json_check.merge!({"resultDescription" => "[#{response_code}] - #{response_message}", "statusCode" => 14, "shortCode" => treatware_check.short_code.to_s})} and return
      end
      result =  {"posLoyaltyVoidOffersResult" =>  treatware_json_check.merge!({"statusCode" => 0 })}
      payments = result["posLoyaltyVoidOffersResult"]["order"]["payments"].select{|p| p["reference"] != "giftcard_transaction_#{gcrh.id}"}
      result["posLoyaltyVoidOffersResult"]["order"]["payments"] =  payments
      render :json => result and return
    elsif voided_value.include?("reward_transaction")
      reward_transaction_id = voided_value.split("_").last
      reward_transaction = RewardTransaction.find(reward_transaction_id)
      user = reward_transaction.user
      reward = reward_transaction.reward
      rt = user.reward_wallets.where("reward_id = ? AND expiry_date > ? AND status = ?", reward.id, Time.current, RewardWallet::STATUS[:CLAIMED] ).order("id DESC").first
      rt.update_attributes(:status => RewardWallet::STATUS[:ACTIVE]) if rt
      user.earn(reward.points)
      PointHistory.add_to_history(user.id, "Void Reward Transaction", reward.points)
      result =  {"posLoyaltyVoidOffersResult" =>  treatware_json_check.merge!({"statusCode" => 0 })}
      discounts = result["posLoyaltyVoidOffersResult"]["order"]["discounts"].select{|p| p["reference"] != "reward_transaction_#{rt.id}"}
      result["posLoyaltyVoidOffersResult"]["order"]["discounts"] =  discounts
      render :json => result and return
    else
      render :json => {"posLoyaltyVoidOffersResult" => treatware_json_check.merge!({"resultDescription" => "Invalid request", "statusCode" => 15})} and return
    end
  end

  def tw_submit_orders
    p params
    treatware_params_check = params["treatware"]
    p treatware_params_check
    treatware_json_check = treatware_params_check["posLoyaltySubmitOrderRequest"]
    p treatware_json_check
    render :json => {"posLoyaltySubmitOrderResult" => {"resultDescription" => "Invalid check", "statusCode" => 5}} and return if treatware_json_check.blank?
    treatware_check = TreatwareCheck.new(treatware_json_check)
    p treatware_check
    store_id = treatware_check.store_id
    authorize_apikey_for_treatware(treatware_check.store_id, "posLoyaltySubmitOrderResult")
    @chain = @pos_location.chain
    location = @pos_location.restaurant
    code, code_type, user, user_code_type = Barcode.convert_and_get_code_type(@chain, treatware_check.short_code.to_s)
    user_session = nil
    user_code_user = nil
    gift_code_user = nil
    selected_code_type = ""
    if code && code_type.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
      selected_code_type = "RewardCode"
      reward_ids = @chain.rewards.map(&:id)
      reward_transaction = RewardTransaction.where("restaurant_id = ? AND UPPER(staffcode) = ? AND reward_id IN (?) AND pos_used IS TRUE AND xpient_processed IS FALSE AND created_at >= ? AND check_number = ?", location.id, code, reward_ids, Time.current - 1.days, treatware_check.receipt_identifier).first
      unless reward_transaction.blank?
        user = reward_transaction.user
      end
    elsif code && code_type.eql?(AppcodeSetting::CODE_TYPES["GIFTCODE"])
      selected_code_type = "GiftCode"
      unless user_code_type.eql?("STATIC")
        gift_code_user = GiftCodeUser.where(:chain_id => @chain.id, :gift_code => code).first
        user = gift_code_user.try(:user)
      end
    elsif code && code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"])
      selected_code_type = "UserCode"
      unless user_code_type.eql?("STATIC")
        user_code_user = UserCodeUser.where(:chain_id => @chain.id, :user_code => code).first
        user = user_code_user.try(:user)
      end
      # user_session = UserSession.where("code = ? AND chain_id = ? AND user_id = ? AND code_type = ? AND executed IS FALSE", code, @chain.id, user.id, AppcodeSetting::CODE_TYPES["USERCODE"]).first
    end

    pos_check_upload = PosCheckUpload.where("pos_location_id = ? AND check_id = ? AND date(TIMEZONE('UTC', pos_check_uploads.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') = date('#{(Time.zone.now).to_date}')", @pos_location.id, treatware_check.pos_order_id).first
    if pos_check_upload.blank?
      pos_check_upload = PosCheckUpload.create(
      :pos_location_id => @pos_location.id,
      :chain_id => @pos_location.chain_id,
      :xml_data => params["treatware"]["posLoyaltySubmitOrderRequest"],
          :check_id => treatware_check.pos_order_id,
          :status => PosCheckUpload::STATUS[:NEW],
          :is_xpient_check => true,
          :user_id => user.try(:id),
          :user_code => code,
          :service => PosCheckUpload::SERVICE[:COUNTER],
          :code_type => selected_code_type,
          :pos_used_type => Chain::POS_USED_TYPE["TREATWARE"],
          :payment_included => treatware_check.gift_card_included?
      )
    elsif ![PosCheckUpload::STATUS[:PROCESSED], PosCheckUpload::STATUS[:CONVERTED]].include?(pos_check_upload.status)
      old_barcode = pos_check_upload.barcode
      barcode ||= pos_check_upload.barcode
      p barcode
      pos_check_upload.update_attributes(
          :code_type => selected_code_type,
          :user_code => code,
          :old_barcode => old_barcode,
          :barcode => barcode,
          :xml_data => params["treatware"]["posLoyaltySubmitOrderRequest"],
          :status => PosCheckUpload::STATUS[:UPDATED],
          :payment_included => treatware_check.gift_card_included?
      )
    else
      p "REAL RESPONSE for #{treatware_check.receipt_identifier} IS ERROR CODE : 13, ERROR DESCRIPTION : Loyalty has been processed before."
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "resultDescription" => "Success! Your Loyalty will be processed.",
              "statusCode" => 0
          }} and return
    end

    #------UPLOAD BARCODE USER PROCESS -----------------
    if user_code_type && user_code_type.eql?("DYNAMIC") && user_session.blank? && code_type.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
      pos_check_upload.update_attributes(:error_code => 10, :user_id => nil)
      p "REAL RESPONSE for #{treatware_check.receipt_identifier} IS ERROR CODE : 10, ERROR DESCRIPTION : Invalid Code or Expired Code."
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "resultDescription" => "Success! Your Loyalty will be processed.",
              "statusCode" => 0
          }} and return
    end

    if code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"]) && user_code_user && user_code_user.expired_at && (user_code_user.expired_at < Time.zone.now)
      pos_check_upload.update_attributes(:error_code => 10, :user_id => nil)
      p "REAL RESPONSE for #{treatware_check.receipt_identifier} IS ERROR CODE : 10, ERROR DESCRIPTION : Invalid Code or Expired Code."
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "resultDescription" => "Success! Your Loyalty will be processed.",
              "statusCode" => 0
          }} and return
    end

    if code_type.eql?(AppcodeSetting::CODE_TYPES["GIFTCODE"]) && gift_code_user && gift_code_user.expired_at && (gift_code_user.expired_at < Time.zone.now)
      pos_check_upload.update_attributes(:error_code => 10, :user_id => nil)
      p "REAL RESPONSE for #{treatware_check.receipt_identifier} IS ERROR CODE : 10, ERROR DESCRIPTION : Invalid Code or Expired Code."
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "resultDescription" => "Success! Your Loyalty will be processed.",
              "statusCode" => 0
          }} and return
    end

    if user.blank?
      pos_check_upload.update_column(:error_code, 11)
      p "REAL RESPONSE for #{treatware_check.receipt_identifier} IS ERROR CODE : 11, ERROR DESCRIPTION : Code is not associated to user."
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "resultDescription" => "Success! Your Loyalty will be processed.",
              "statusCode" => 0
          }} and return
    end

    if @chain.id != user.chain_id
      pos_check_upload.update_attributes(:error_code => 12, :user_id => nil)
      p "REAL RESPONSE for #{treatware_check.receipt_identifier} IS ERROR CODE : 12, ERROR DESCRIPTION : User is not associated to Restaurant chain."
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "resultDescription" => "Success! Your Loyalty will be processed.",
              "statusCode" => 0
          }} and return
    end

    offer = location.first_active_offer
    receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true, :pos_check_upload_id => pos_check_upload.id)
    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.status = Receipt::STATUS[:RECEIVED]
    receipt.receipt_transactions << receipt_transaction
    receipt.save
    Delayed::Job.enqueue(TreatwareReceiptJobV2.new(pos_check_upload.id, user.id, @pos_location.restaurant_id, offer.id, receipt.id), :run_at => Time.zone.now + @chain.pos_receipt_processing_delay.minutes)
    user_session.update_attributes(:executed => true, :receipt_id => receipt.id) if user_session
    reward_transaction.update_column(:xpient_processed, true) if reward_transaction
    user_code_user.destroy if user_code_user
    gift_code_user.destroy if gift_code_user
    p "REAL RESPONSE for #{treatware_check.receipt_identifier} IS SUCCESS CODE : 0, ERROR DESCRIPTION : Success! Your Loyalty will be processed."
    render :json => {
        "posLoyaltySubmitOrderResult" => {
            "resultDescription" => "Success! Your Loyalty will be processed.",
            "statusCode" => 0
        }}
  end


  def auto_loyalty_for_giftcode(pos_location, user, treatware_check, code, selected_code_type, user_code_type, user_session, code_type)
    location = pos_location.restaurant
    chain = pos_location.chain
    pos_check_upload = PosCheckUpload.where("pos_location_id = ? AND check_id = ? AND date(TIMEZONE('UTC', pos_check_uploads.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') = date('#{(Time.zone.now).to_date}')", pos_location.id, treatware_check.pos_order_id).first
    if pos_check_upload.blank?
      pos_check_upload = PosCheckUpload.create(
          :pos_location_id => pos_location.id,
          :chain_id => pos_location.chain_id,
          :xml_data => params["treatware"]["posLoyaltyApplyOffersRequest"],
          :check_id => treatware_check.pos_order_id,
          :status => PosCheckUpload::STATUS[:NEW],
          :is_xpient_check => true,
          :user_id => user.try(:id),
          :user_code => code,
          :service => PosCheckUpload::SERVICE[:COUNTER],
          :code_type => selected_code_type,
          :pos_used_type => Chain::POS_USED_TYPE["TREATWARE"],
          :payment_included => treatware_check.gift_card_included?
      )
    elsif ![PosCheckUpload::STATUS[:PROCESSED], PosCheckUpload::STATUS[:CONVERTED]].include?(pos_check_upload.status)
      old_barcode = pos_check_upload.barcode
      barcode ||= pos_check_upload.barcode
      p barcode
      pos_check_upload.update_attributes(
          :code_type => selected_code_type,
          :user_code => code,
          :old_barcode => old_barcode,
          :barcode => barcode,
          :xml_data => params["treatware"]["posLoyaltyApplyOffersRequest"],
          :status => PosCheckUpload::STATUS[:UPDATED],
          :payment_included => treatware_check.gift_card_included?
      )
    else
      return
    end

    #------UPLOAD BARCODE USER PROCESS -----------------
    if user_code_type && user_code_type.eql?("DYNAMIC") && user_session.blank?
      pos_check_upload.update_attributes(:error_code => 10, :user_id => nil)
      return
    end

    if code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"]) && user_session && user_session.expired_at && (user_session.expired_at < Time.zone.now)
      pos_check_upload.update_attributes(:error_code => 10, :user_id => nil)
      return
    end

    if user.blank?
      pos_check_upload.update_column(:error_code, 11)
      return
    end

    if chain.id != user.chain_id
      pos_check_upload.update_attributes(:error_code => 12, :user_id => nil)
      return
    end

    offer = location.first_active_offer
    receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true, :pos_check_upload_id => pos_check_upload.id)
    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.status = Receipt::STATUS[:RECEIVED]
    receipt.receipt_transactions << receipt_transaction
    receipt.save
    Delayed::Job.enqueue(TreatwareReceiptJob.new(pos_check_upload, user, pos_location.restaurant_id, offer.id, receipt.id), :run_at => Time.zone.now + chain.pos_receipt_processing_delay.minutes)
    user_session.update_attributes(:executed => true, :receipt_id => receipt.id) if user_session
  end

  def authorize_health_apikey!
    check_and_build_health_params
    unless @pos_location = PosLocation.where(:apikey => params["apikey"]).first
      render :json => {"posLoyaltyGetInfoResult" => {"statusCode" => 1, "resultDescription" => "Something Wrong with your apikey!", "apiVersion" => "1.0"}} and return and return
    end
  end

  def check_and_build_health_params
    unless params["_json"].blank?
      new_params = params["_json"]
      if new_params.class.to_s == Hash
        params.merge!(new_params)
      else
        params.merge!(JSON.load(new_params))
      end
    end
  end
end
