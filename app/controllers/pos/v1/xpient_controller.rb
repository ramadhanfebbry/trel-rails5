require 'nokogiri'
class Pos::V1::XpientController < Pos::BaseController

  def get_info
    p "GETINFO"*100
    p params
    p "8888"*100
    render :json => {:status => 0}
  end

  def apply_offers
    p "APPLYOFFERS"*100
    p params
    xpient_check = XpientCheck.new(params["xpient"]["posLoyaltyApplyOffersRequest"])
    store_id = xpient_check.store_id
    authorize_apikey_for_xpient(xpient_check.store_id)
    chain = @pos_location.chain
    code, code_type, user, appcode_type = Barcode.convert_and_get_code_type(chain, xpient_check.short_code.to_s)
    if code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"])
      unless appcode_type.eql?(AppcodeSetting::TYPES["STATIC"])
        user_code_user = UserCodeUser.where(:chain_id => chain.id, :user_code => code).first
        user = user_code_user.try(:user)
      end
    end

    render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "#{AppcodeSetting::CODE_TYPES.invert[code_type]} added", "statusCode" => 0}} and return if code && code_type && !code_type.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])

    reward_ids = chain.rewards.map(&:id)

    pos_reward_check = PosRewardCheck.new(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params["xpient"]["posLoyaltyApplyOffersRequest"], :reward_code => code,
                                          :pos_used_type => Chain::POS_USED_TYPE["XPIENT"])

    reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => code, :active => true).first
    if reward_code_user.blank?
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "Reward code invalid"
      pos_reward_check.save
      render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code invalid"} and return
    end
    reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", code, reward_ids)
    if reward_transactions.blank?
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "Reward code invalid."
      pos_reward_check.save
      render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "Reward code invalid", "statusCode" => 5}} and return
    end

    reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
    reward_code_setting = JSON.parse(reward_code_setting) rescue nil
    reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
    reward_code_expire = reward_code_setting["timer"].to_i
    if reward_code_expire != 0
      reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
    else
      reward_transaction = reward_transactions.order("created_at DESC").first
    end

    if reward_transaction.blank?
      reward_code_user.destroy if reward_code_user
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "Reward code expired."
      pos_reward_check.save
      render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => "Reward code expired", "statusCode" => 5}} and return
    end

    reward_transaction.update_column(:check_number, xpient_check.receipt_identifier)
    reward = reward_transaction.reward
    user = reward_transaction.user

    reward_menu_items = reward.reward_menu_items
    required_menu_items = reward_menu_items.select{|a| a.required }
    optional_menu_items = reward_menu_items.select{|a| !a.required }
    reward_general_menu_item_ids = reward_menu_items.map(&:general_menu_item_id)

    pos_discount_type = reward.pos_discount_type

    items_check_buy = XpientCheck.get_line_items(chain, xpient_check.line_items)
    items_check_buy = XpientCheck.sort_items_by_price(items_check_buy)
    #ordered_item_check_buy = items_check_buy.sort{|a,b| a[:price_per_item] <=> b[:price_per_item]}

    grouped_items_check_buy = []
    items_check_buy.each do |item_check|
      existing_group = grouped_items_check_buy.select{|a| a[:menu_item_id] == item_check[:menu_item_id]}.first
      if existing_group
        existing_group[:quantity] += item_check[:quantity]
        existing_group[:price] = item_check[:price]
        grouped_items_check_buy.delete_if{|group| group[:menu_item_id] == existing_group[:menu_item_id]}
        grouped_items_check_buy << existing_group
      else
        grouped_items_check_buy << item_check
      end
    end

    items_discount, error_message = xpient_check.check_discount(@pos_location, pos_discount_type, reward_menu_items, grouped_items_check_buy)

    #preset item discount if there not suitable combo
    #items_discount = [] if !items_discount.blank? && !required_menu_items.count.eql?(items_discount.count)

    unless items_discount.blank?
      #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
      reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
      reward_code_user.destroy if reward_code_user

      p "here are the items that got the discount"
      p items_discount
      child_item_for_xpient_discount = xpient_check.parent_line_item_discount(@pos_location, items_discount, reward_transaction)
      child_item_for_xpient_discount = child_item_for_xpient_discount.flatten

      p "======LOYALTY DISCOUNT HASH========="
      p xpient_check.line_items
      p child_item_for_xpient_discount
      p "===================================="
      p "====== RESPONSE DISCOUNT LOYALTY======"
      response_discount_loyalty_applied = xpient_check.build_response_for_discount_applied(child_item_for_xpient_discount)
      p response_discount_loyalty_applied
      p "======================================="
      pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
      pos_reward_check.user_id = (user.id rescue nil)
      pos_reward_check.save
      if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
        reward.claimed_by(user)
        Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
      end
      render :json => response_discount_loyalty_applied and return
    else
      pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
      pos_reward_check.user_id = (user.id rescue nil)
      pos_reward_check.error_description = "Reward requirement is not met."
      pos_reward_check.save
      #reward_transaction.update_column(:pos_used, true)
      error_message = error_message.blank? ? "Reward requirement is not met." : error_message
      render :json => {"posLoyaltyApplyOffersResult" => {"resultDescription" => error_message, "statusCode" => 5}} and return
    end
  end

  def submit_order
    p "SUBMITORDER"*100
    xpient_check = XpientCheck.new(params["xpient"]["posLoyaltySubmitOrderRequest"])
    store_id = xpient_check.store_id
    authorize_apikey_for_xpient(xpient_check.store_id)
    @chain = @pos_location.chain
    location = @pos_location.restaurant
    code, code_type, user, user_code_type = Barcode.convert_and_get_code_type(@chain, xpient_check.short_code.to_s)
    user_code_user = nil
    user_session = nil
    selected_code_type = ""
    if code && code_type.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
      selected_code_type = "RewardCode"
      reward_ids = @chain.rewards.map(&:id)
      reward_transaction = RewardTransaction.where("restaurant_id = ? AND UPPER(staffcode) = ? AND reward_id IN (?) AND pos_used IS TRUE AND xpient_processed IS FALSE AND created_at >= ? AND check_number = ?", location.id, code, reward_ids, Time.current - 1.days, xpient_check.receipt_identifier).first
      unless reward_transaction.blank?
        user = reward_transaction.user
      end
    elsif code && code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"])
      selected_code_type = "UserCode"
      unless user_code_type.eql?(AppcodeSetting::TYPES["STATIC"])
        user_code_user = UserCodeUser.where(:chain_id => @chain.id, :user_code => code).first
        user = user_code_user.try(:user)
      end
      # user_session = UserSession.where("code = ? AND chain_id = ? AND user_id = ? AND code_type = ? AND executed IS FALSE", code, @chain.id, user.id, AppcodeSetting::CODE_TYPES["USERCODE"]).first
    end

    pos_check_upload = PosCheckUpload.where(:pos_location_id => @pos_location.id, :check_id => xpient_check.pos_order_id).first
    if pos_check_upload.blank?
      pos_check_upload = PosCheckUpload.create(
          :pos_location_id => @pos_location.id,
          :chain_id => @pos_location.chain_id,
          :xml_data => params["xpient"]["posLoyaltySubmitOrderRequest"],
          :check_id => xpient_check.pos_order_id,
          :status => PosCheckUpload::STATUS[:NEW],
          :is_xpient_check => true,
          :user_id => user.try(:id),
          :user_code => code,
          :service => PosCheckUpload::SERVICE[:COUNTER],
          :code_type => selected_code_type,
          :pos_used_type => Chain::POS_USED_TYPE["XPIENT"]
      )
    elsif ![PosCheckUpload::STATUS[:PROCESSED], PosCheckUpload::STATUS[:CONVERTED]].include?(pos_check_upload.status)
      old_barcode = pos_check_upload.barcode
      barcode ||= pos_check_upload.barcode
      p barcode
      pos_check_upload.update_attributes(:code_type => selected_code_type, :user_code => code, :old_barcode => old_barcode, :barcode => barcode, :xml_data => params["xpient"]["posLoyaltySubmitOrderRequest"], :status => PosCheckUpload::STATUS[:UPDATED])
    else
      render :json => {:status => false} and return
    end

    #------UPLOAD BARCODE USER PROCESS -----------------
    if user_code_type && user_code_type.eql?("DYNAMIC") && user_session.blank? && !code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"])
      pos_check_upload.update_attributes(:error_code => 10, :user_id => nil)
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "statusCode" => 0,
          }} and return
    end

    if code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"]) && user_code_user && user_code_user.expired_at && (user_code_user.expired_at < Time.zone.now)
      pos_check_upload.update_attributes(:error_code => 10, :user_id => nil)
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "statusCode" => 0,
          }} and return
    end

    if user.blank?
      pos_check_upload.update_column(:error_code, 11)
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "statusCode" => 0,
          }} and return
    end

    if @chain.id != user.chain_id
      pos_check_upload.update_attributes(:error_code => 12, :user_id => nil)
      render :json => {
          "posLoyaltySubmitOrderResult" => {
              "statusCode" => 0,
          }} and return
    end


    offer = location.first_active_offer
    receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true, :pos_check_upload_id => pos_check_upload.id)
    receipt_transaction = ReceiptTransaction.new
    receipt_transaction.status = Receipt::STATUS[:RECEIVED]
    receipt.receipt_transactions << receipt_transaction
    receipt.save
    Delayed::Job.enqueue(XpientReceiptJob.new(pos_check_upload, user, @pos_location.restaurant_id, offer.id, receipt.id), :run_at => Time.zone.now + @chain.pos_receipt_processing_delay.minutes)
    user_session.update_attributes(:executed => true, :receipt_id => receipt.id) if user_session
    reward_transaction.update_column(:xpient_processed, true) if reward_transaction
    user_code_user.destroy if user_code_user

    p "8888"*100
    render :json => {
        "posLoyaltySubmitOrderResult" => {
        "statusCode" => 0,
        "promoMessage" => "Loyalty Usercode applied",
    }}
  end

  def void_order
    p "VOIDORDER"*100
    p params
    p "8888"*100
    render :json => {:status => 0}
  end

  def get_line_items(chain, line_items)
    items_check_buy = []
    line_items.each do |menu_item|
      item = {}
      item[:description] = menu_item["description"] rescue ""
      item[:menu_item_id] = menu_item["id"].strip rescue nil
      item[:quantity] = menu_item["quantity"].to_i rescue 0
      item[:total_amount] = menu_item["price"].to_f rescue 0
      arr_general_menu_item_id = []
      pos_menu_items = PosMenuItem.select("id").where(:item_number => item[:menu_item_id], :chain_id => chain.id)
      pos_menu_items.each do |pos_menu_item|
        arr_general_menu_item_id = (pos_menu_item.general_menu_items.map(&:id) rescue [])
        break unless arr_general_menu_item_id.blank?
      end
      item[:general_menu_item_id] = arr_general_menu_item_id
      item[:child_items] = []
      item[:child_items] << get_line_items(chain, menu_item["childItems"])
      items_check_buy << item
    end unless line_items.blank?
    items_check_buy
  end



end