require 'nokogiri'
class Pos::V1::OloRewardsController < ApplicationController
  before_action :check_auth,:authenticate_new_olo_user!, :authorize_apikey!


  # def http_basic_auth
  #   if ENV['HTTP_AUTH'] =~ %r{(.+)\:(.+)}
  #     unless authenticate_with_http_basic { |user, password|  user == $1 && password == $2 }
  #       request_http_basic_authentication
  #     end
  #   end
  # end

  def check_auth
    authenticate_or_request_with_http_basic do |username,password|
      application = ApplicationKey.where(:appkey => username).first
      !application.blank? && password == Setting.olo_reward_api["nekter_secret_key"]
    end
  end


  def authenticate_new_olo_user!
    # user = authenticate_oauth_user!(params)
    #application = ApplicationKey.where(:appkey => params[:client_id]).first

      @user = User.find params['user_id'] rescue nil
      if @user.blank?
        return render :json => {"status" => false, "notice" => "USER NOT FOUND ON SYSTEM"}
      end
  end


  def authorize_apikey!
    p "params----before"
    p params
    check_and_build_params
    p "params ----after"
    p params
    unless @pos_location = RestaurantDetail.where(:olo_order_id => params["location_id"]).first#@pos_location = PosLocation.where(:apikey => params[:apikey]).first
      render :status => 404,
             :json => {status: false, notice: "Something wrong with your OLO ID"} and return
    end
  end

  def check_and_build_params
    unless params["_json"].blank?
      new_params = params["_json"]
      if new_params.class.to_s == Hash
        params.merge!(new_params)
      else
        params.merge!(JSON.load(new_params))
      end
    end
  end

  def validate
      chain = @pos_location.chain rescue nil
      location = @pos_location.restaurant
      chain = location.chain
      reward_ids = chain.rewards.map(&:id)
      reward_code = params["reward_code"].to_s.upcase

      reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"

      reward_code_setting = JSON.parse(reward_code_setting) rescue nil
      reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
      if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
        if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
          reward_code = reward_code.to_ean_13_format
        end
      end
      reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
      pos_reward_check = PosRewardCheck.new(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params["check"], :reward_code => reward_code)
      reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => reward_code, :active => true).first
      if reward_code_user.blank?
        # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
        # pos_reward_check.error_description = "Reward code invalid"
        # pos_reward_check.save
        render :json => {:message => "failure"} and return
      end
      reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", reward_code, reward_ids)
      if reward_transactions.blank?
        # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
        # pos_reward_check.error_description = "Reward code invalid."
        # pos_reward_check.save
        #render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code invalid"} and return
        render :json => {:message => "failure"} and return
      end

      reward_code_expire = reward_code_setting["timer"].to_i
      if reward_code_expire != 0
        reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
      else
        reward_transaction = reward_transactions.order("created_at DESC").first
      end
      if reward_transaction.blank?
        # reward_code_user.destroy if reward_code_user
        # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
        # pos_reward_check.error_description = "Reward code expired."
        # pos_reward_check.save
        #render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code expired"} and return
        render :json => {:message => "failure"} and return
      end

      #render :json => {:message => "success"} and return
      #xml_in_nokogiri_format = Nokogiri::XML(params[:check])
      p "request.content_type"
      p request.content_type
      p "request.content_type #{request.content_type}"
      p "----------------"
      xml_receipt = params["check"]

      # check params first.

      if params["check"].class == String
        check_to_hash =  YAML.load params["check"].gsub(/=>/, ': ')
      else params["check"].class == Hash
      check_to_hash = params["check"]
      end

      puts params["check"].class
      puts "aaaaaaaaaaaaaaaaaaaaaaa #{params["check"]}"

      olo_check = OloWebhook::Parser.new(check_to_hash)
      p "here is check id and seq number -----"
      puts check_to_hash
      check_id = check_to_hash['orderId']
      if check_id.blank?
        check_id = "#{Time.now.strftime('%H%m')}#{@pos_location.id}"
      end
      check_id = check_id.to_s
      seq_number = "olo"

      items_discount = []
      reward_transaction.update_column(:check_number, [check_id, seq_number, @pos_location.apikey].join("-"))
      reward = reward_transaction.reward
      user = reward_transaction.user
      reward_general_menu_item_ids = reward.general_menu_items.map(&:id)
      reward_menu_items = reward.reward_menu_items
      required_menu_items = reward_menu_items.select{|a| a.required }
      optional_menu_items = reward_menu_items.select{|a| !a.required }
      items_check_buy = []
      xml_menu_items = olo_check.menu_items_list
      parent_qualified = true
      xml_menu_items.each do |menu_item|
        item = {}
        item[:menu_item_id] = menu_item[:product_id] rescue 0

        item[:name] = menu_item[:description] rescue nil

        item[:quantity] = menu_item[:quantity].to_i rescue 0

        item[:total_amount] = menu_item[:total_amount].to_f rescue 0


        item[:price_per_item] = menu_item[:price].to_f rescue 0

        # item[:level_size_id] = menu_item.search("size-id").text rescue nil
        # item[:level_size_id] = menu_item["SizeId"].to_i  if item[:level_size_id].blank? &&  !menu_item["SizeId"].blank?
        # item[:level_size_id] = nil if item[:level_size_id].blank?

        p "---- item number  #{item[:menu_item_id]}"
        p "------- chain = #{chain.id}--"
        p "=------ pos location = #{@pos_location.id}"
        #arr_general_menu_item_id = GeneralMenuItemRangeValue.where("start_value <= #{item[:menu_item_id]} and end_value >= #{item[:menu_item_id]} and chain_id = #{chain.id}").map(&:general_menu_item_id)
        arr_general_menu_item_id = []
        pos_menu_items = PosMenuItem.select("id").where(:item_number_olo => item[:menu_item_id], :chain_id => chain.id, :level_size_id => item[:level_size_id])
        p "--- pos menu items = #{pos_menu_items.count}"
        pos_menu_items.each do |pos_menu_item|
          arr_general_menu_item_id << (pos_menu_item.general_menu_items.map(&:id) rescue [])
          break unless arr_general_menu_item_id.blank?
        end
        arr_general_menu_item_id = arr_general_menu_item_id.flatten
        item[:general_menu_item_id] = arr_general_menu_item_id.flatten

        item[:required_menu_item] = !(item[:general_menu_item_id] & required_menu_items.map(&:general_menu_item_id)).blank?
        if item[:general_menu_item_id].blank? && !item[:required_menu_item]
          parent_qualified = false
        elsif !item[:general_menu_item_id].blank? && item[:required_menu_item]
          parent_qualified = true
        end
        items_check_buy << item if parent_qualified
      end

      #ordered_item_check_buy = items_check_buy.sort{|a,b| a[:price_per_item] <=> b[:price_per_item]}
      #filtered_items_check = items_check_buy.select{|a| !(reward_general_menu_item_ids && a[:general_menu_item_id]).blank?}

      grouped_items_check_buy = []
      items_check_buy.each_with_index do |item_check, i|
        if item_check[:required_menu_item]
          item_check[:all_price_with_optional_item] = item_check[:price_per_item]
          menu_item_combo_sum = [item_check[:menu_item_id]]
          p '----'
          p item_check[:menu_item_combo_sum]
          (i+1).upto(items_check_buy.length-1) do |idx|
            optional_item = items_check_buy[idx]
            unless optional_item[:required_menu_item]
              item_check[:all_price_with_optional_item] += optional_item[:price_per_item]
              menu_item_combo_sum << optional_item[:menu_item_id]
            else
              break
            end
          end
        end
        item_check[:menu_item_combo_sum] = menu_item_combo_sum.join("-") if menu_item_combo_sum
        grouped_items_check_buy << item_check
      end

      if chain.pos_discount_setting.eql?(Chain::POS_DISCOUNT_TYPE["Discount Least priced qualifying item"])
        ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| a[:all_price_with_optional_item] <=> b[:all_price_with_optional_item]}
      else
        ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| b[:all_price_with_optional_item] <=> a[:all_price_with_optional_item]}
      end

      pos_discount_type = reward.pos_discount_type

      #non menu item discount based
      if pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?
        # if request.content_type == "application/json" && xml_receipt.class.to_s != "String"
        #   pos_check = params["check"]
        #   check_subtotal = pos_check["Subtotal"]
        # else
        #   pos_xml = PosReceiptXml.new(params["check"])
        #   check_subtotal = pos_xml.sub_total
        # end

        check_subtotal = olo_check.get_subtotal
        puts "Check subtotal olo === #{check_subtotal}"

        total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", check_id, seq_number, @pos_location.restaurant_id])
        subtotal_after_discount = check_subtotal - total_discount_registered

        puts "Subtotal after discount olo ===  #{subtotal_after_discount}"
        if subtotal_after_discount > 0
          discount_applied = subtotal_after_discount > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : subtotal_after_discount
#          reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
          # RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
          # pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
          # pos_reward_check.user_id = (user.id rescue nil)
          # pos_reward_check.save

          #Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction)) if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
          if chain.auto_loyalty && due_total == 0
  #          xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, discount_applied)
 #           Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
          end
          render :json => {:status => true, :discount => (discount_applied*-1), :message => "success"} and return
        else
          # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          # pos_reward_check.user_id = (user.id rescue nil)
          # pos_reward_check.error_description = "Reward requirement is not met. subtotal after discount"
          # pos_reward_check.save
          # #reward_transaction.update_column(:pos_used, true)
          render :json => {:status => false,  :message => "Reward requirement is not met."} and return
        end
      end

      #what discount that reward offered
      required_menu_items.each do |reward_general_menu_item|
        #reward_general_menu_item_ids.each do |reward_general_menu_item_id|
        ordered_item_check_buy.each do |filtered_item|
          if filtered_item[:general_menu_item_id].include?(reward_general_menu_item.general_menu_item_id)
            reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]).count
            qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
            if reward_discount_count < qty_selected
              items_discount << {:menu_item_id => filtered_item[:menu_item_id], :price => filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]}
              #CODE FOR OPTIONAL MENU ITEM
              optional_menu_discount_items = []
              optional_menu_items.each do |opt_reward_general_menu_item|
                (grouped_items_check_buy.index(filtered_item)+1).upto(grouped_items_check_buy.length-1) do |idx|
                  opt_filtered_item = grouped_items_check_buy[idx] rescue nil
                  break if opt_filtered_item.blank?
                  break if opt_filtered_item[:required_menu_item]
                  if opt_filtered_item[:general_menu_item_id].include?(opt_reward_general_menu_item.general_menu_item_id)
                    reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => opt_filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]).count
                    qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
                    if reward_discount_count < qty_selected
                      #optional_menu_discount_items <<  opt_filtered_item
                      items_discount << {:menu_item_id => opt_filtered_item[:menu_item_id], :price => opt_filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]}
                    end
                  end
                end
              end
              break
            end
          end
        end
      end

      #preset item discount if there not suitable combo
      items_discount = [] if !items_discount.blank? && !(required_menu_items.count <= items_discount.count)

      unless items_discount.blank?
        #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
        #reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
        #reward_code_user.destroy if reward_code_user
        p "here are the items that got the discount"
        p items_discount
        all_discount_item = items_discount.sum{ |a| a[:price]}
        if pos_discount_type.blank?
          calculated_discount_from_reward = all_discount_item
        elsif pos_discount_type.is_discount_percentage?
          calculated_discount_from_reward = ((pos_discount_type.discount_amount.to_f/100) * all_discount_item).round(2)
        elsif pos_discount_type.is_discount_price?
          calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? (all_discount_item - pos_discount_type.discount_amount) : opt_filtered_item[:price_per_item]).round(2)
        elsif pos_discount_type.is_discount_fixed_price?
          calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : all_discount_item).round(2)
        end
        reward_discount_tmp = []
        items_discount.each do |item|
          #RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => item[:price], :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum])
          reward_discount_tmp << {:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => item[:price],
                                  :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum]
          }
        end
        # pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
        # pos_reward_check.user_id = (user.id rescue nil)
        # pos_reward_check.save
        # Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction)) if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"])
        if chain.auto_loyalty && due_total == 0
          #xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, calculated_discount_from_reward)
          #Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
        end
        render :json => {:status => true, :discount => (calculated_discount_from_reward*-1), :message => "success"} and return
      else
        # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
        # pos_reward_check.user_id = (user.id rescue nil)
        # pos_reward_check.error_description = "Reward requirement is not met."
        # pos_reward_check.save
        #reward_transaction.update_column(:pos_used, true)
        render :json => {:status => false, :discount => nil, :message => "Reward requirement is not met."} and return
      end
    end

  # def validate
  #   chain = @pos_location.chain
  #   location = @pos_location.restaurant
  #   reward_ids = chain.rewards.map(&:id)
  #   reward_code = params["reward_code"].to_s.upcase
  #
  #   reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
  #
  #   reward_code_setting = JSON.parse(reward_code_setting) rescue nil
  #   reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
  #   if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
  #     if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
  #       reward_code = reward_code.to_ean_13_format
  #     end
  #   end
  #   reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
  #   pos_reward_check = PosRewardCheck.new(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params["check"], :reward_code => reward_code)
  #   reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => reward_code, :active => true).first
  #   if reward_code_user.blank?
  #     # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
  #     # pos_reward_check.error_description = "Reward code invalid"
  #     # pos_reward_check.save
  #     render :json => {:message => "failure"} and return
  #   end
  #   reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", reward_code, reward_ids)
  #   if reward_transactions.blank?
  #     # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
  #     # pos_reward_check.error_description = "Reward code invalid."
  #     # pos_reward_check.save
  #     #render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code invalid"} and return
  #     render :json => {:message => "failure"} and return
  #   end
  #
  #   reward_code_expire = reward_code_setting["timer"].to_i
  #   if reward_code_expire != 0
  #     reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
  #   else
  #     reward_transaction = reward_transactions.order("created_at DESC").first
  #   end
  #   if reward_transaction.blank?
  #     # reward_code_user.destroy if reward_code_user
  #     # pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
  #     # pos_reward_check.error_description = "Reward code expired."
  #     # pos_reward_check.save
  #     #render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code expired"} and return
  #     render :json => {:message => "failure"} and return
  #   end
  #
  #   render :json => {:message => "success"} and return
  # end


  def process_code_olo
    chain = @pos_location.chain rescue nil
    location = @pos_location.restaurant
    chain = location.chain
    reward_ids = chain.rewards.map(&:id)
    unless params["transaction_id"].blank?
      params["request_type"] = "1"
    else
      params["request_type"] = "0"
    end

    case params["request_type"]
      when "0"
        reward_ids = chain.rewards.map(&:id)
        reward_code = params["reward_code"].to_s.upcase

        reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
        reward_code_setting = JSON.parse(reward_code_setting) rescue nil
        reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
        if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
          if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
            reward_code = reward_code.to_ean_13_format
          end
        end
        reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
        pos_reward_check = PosRewardCheck.new(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params["check"], :reward_code => reward_code)
        reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => reward_code, :active => true).first
        if reward_code_user.blank?
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = "Reward code invalid"
          pos_reward_check.save
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code invalid"} and return
        end
        reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", reward_code, reward_ids)
        if reward_transactions.blank?
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = "Reward code invalid."
          pos_reward_check.save
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code invalid"} and return
        end

        reward_code_expire = reward_code_setting["timer"].to_i
        if reward_code_expire != 0
          reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
        else
          reward_transaction = reward_transactions.order("created_at DESC").first
        end
        if reward_transaction.blank?
          reward_code_user.destroy if reward_code_user
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.error_description = "Reward code expired."
          pos_reward_check.save
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward code expired"} and return
        end

        #xml_in_nokogiri_format = Nokogiri::XML(params[:check])
        p "request.content_type"
        p request.content_type
        p "request.content_type #{request.content_type}"
        p "----------------"
        xml_receipt = params["check"]

        # check params first.

        if params["check"].class == String
          check_to_hash =  YAML.load params["check"].gsub(/=>/, ': ')
        else params["check"].class == Hash
          check_to_hash = params["check"]
        end

        puts params["check"].class
        puts "aaaaaaaaaaaaaaaaaaaaaaa #{params["check"]}"

        olo_check = OloWebhook::Parser.new(check_to_hash)
        p "here is check id and seq number -----"
        puts check_to_hash
        check_id = check_to_hash['orderId']
        if check_id.blank?
          check_id = "#{Time.now.strftime('%H%m')}#{@pos_location.id}"
        end
        check_id = check_id.to_s
        seq_number = "olo"

        items_discount = []
        reward_transaction.update_column(:check_number, [check_id, seq_number, @pos_location.apikey].join("-"))
        reward = reward_transaction.reward
        user = reward_transaction.user
        reward_general_menu_item_ids = reward.general_menu_items.map(&:id)
        reward_menu_items = reward.reward_menu_items
        required_menu_items = reward_menu_items.select{|a| a.required }
        optional_menu_items = reward_menu_items.select{|a| !a.required }
        items_check_buy = []
        xml_menu_items = olo_check.menu_items_list
        parent_qualified = true
        xml_menu_items.each do |menu_item|
          item = {}
          item[:menu_item_id] = menu_item[:product_id] rescue 0

          item[:name] = menu_item[:description] rescue nil

          item[:quantity] = menu_item[:quantity].to_i rescue 0

          item[:total_amount] = menu_item[:total_amount].to_f rescue 0


          item[:price_per_item] = menu_item[:price].to_f rescue 0

          # item[:level_size_id] = menu_item.search("size-id").text rescue nil
          # item[:level_size_id] = menu_item["SizeId"].to_i  if item[:level_size_id].blank? &&  !menu_item["SizeId"].blank?
          # item[:level_size_id] = nil if item[:level_size_id].blank?

          p "---- item number  #{item[:menu_item_id]}"
          p "------- chain = #{chain.id}--"
          p "=------ pos location = #{@pos_location.id}"
          #arr_general_menu_item_id = GeneralMenuItemRangeValue.where("start_value <= #{item[:menu_item_id]} and end_value >= #{item[:menu_item_id]} and chain_id = #{chain.id}").map(&:general_menu_item_id)
          arr_general_menu_item_id = []
          pos_menu_items = PosMenuItem.select("id").where(:item_number_olo => item[:menu_item_id], :chain_id => chain.id, :level_size_id => item[:level_size_id])
          p "--- pos menu items = #{pos_menu_items.count}"
          pos_menu_items.each do |pos_menu_item|
            arr_general_menu_item_id << (pos_menu_item.general_menu_items.map(&:id) rescue [])
            break unless arr_general_menu_item_id.blank?
          end
          arr_general_menu_item_id = arr_general_menu_item_id.flatten
          item[:general_menu_item_id] = arr_general_menu_item_id.flatten

          item[:required_menu_item] = !(item[:general_menu_item_id] & required_menu_items.map(&:general_menu_item_id)).blank?
          if item[:general_menu_item_id].blank? && !item[:required_menu_item]
            parent_qualified = false
          elsif !item[:general_menu_item_id].blank? && item[:required_menu_item]
            parent_qualified = true
          end
          items_check_buy << item if parent_qualified
        end

        #ordered_item_check_buy = items_check_buy.sort{|a,b| a[:price_per_item] <=> b[:price_per_item]}
        #filtered_items_check = items_check_buy.select{|a| !(reward_general_menu_item_ids && a[:general_menu_item_id]).blank?}

        grouped_items_check_buy = []
        items_check_buy.each_with_index do |item_check, i|
          if item_check[:required_menu_item]
            item_check[:all_price_with_optional_item] = item_check[:price_per_item]
            menu_item_combo_sum = [item_check[:menu_item_id]]
            p '----'
            p item_check[:menu_item_combo_sum]
            (i+1).upto(items_check_buy.length-1) do |idx|
              optional_item = items_check_buy[idx]
              unless optional_item[:required_menu_item]
                item_check[:all_price_with_optional_item] += optional_item[:price_per_item]
                menu_item_combo_sum << optional_item[:menu_item_id]
              else
                break
              end
            end
          end
          item_check[:menu_item_combo_sum] = menu_item_combo_sum.join("-") if menu_item_combo_sum
          grouped_items_check_buy << item_check
        end

        if chain.pos_discount_setting.eql?(Chain::POS_DISCOUNT_TYPE["Discount Least priced qualifying item"])
          ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| a[:all_price_with_optional_item] <=> b[:all_price_with_optional_item]}
        else
          ordered_item_check_buy = grouped_items_check_buy.select{|a| a[:required_menu_item]}.sort{|a,b| b[:all_price_with_optional_item] <=> a[:all_price_with_optional_item]}
        end

        pos_discount_type = reward.pos_discount_type

        #non menu item discount based
        if pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?
          # if request.content_type == "application/json" && xml_receipt.class.to_s != "String"
          #   pos_check = params["check"]
          #   check_subtotal = pos_check["Subtotal"]
          # else
          #   pos_xml = PosReceiptXml.new(params["check"])
          #   check_subtotal = pos_xml.sub_total
          # end

          check_subtotal = olo_check.get_subtotal
          puts "Check subtotal olo === #{check_subtotal}"

          total_discount_registered = RewardDiscount.sum(:discount, :conditions => ["check_id = ? AND seq_num = ?  AND restaurant_id = ?", check_id, seq_number, @pos_location.restaurant_id])
          subtotal_after_discount = check_subtotal - total_discount_registered

          puts "Subtotal after discount olo ===  #{subtotal_after_discount}"
          if subtotal_after_discount > 0
            discount_applied = subtotal_after_discount > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : subtotal_after_discount
            reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
            RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
            pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
            pos_reward_check.user_id = (user.id rescue nil)
            pos_reward_check.save

            if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
              Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
            end

            if chain.auto_loyalty && due_total == 0
              puts "chain.auto_loyalty && due_total == 0 "
              xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, discount_applied)
              Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
              #user.burn(reward.points) rescue puts ""
            end
            render :json => {:status => true, :discount => (discount_applied*-1), :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
          else
            pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
            pos_reward_check.user_id = (user.id rescue nil)
            pos_reward_check.error_description = "Reward requirement is not met. subtotal after discount"
            pos_reward_check.save
            #reward_transaction.update_column(:pos_used, true)
            render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
          end
        end

        #what discount that reward offered
        required_menu_items.each do |reward_general_menu_item|
          #reward_general_menu_item_ids.each do |reward_general_menu_item_id|
          ordered_item_check_buy.each do |filtered_item|
            if filtered_item[:general_menu_item_id].include?(reward_general_menu_item.general_menu_item_id)
              reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]).count
              qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
              if reward_discount_count < qty_selected
                items_discount << {:menu_item_id => filtered_item[:menu_item_id], :price => filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]}
                #CODE FOR OPTIONAL MENU ITEM
                optional_menu_discount_items = []
                optional_menu_items.each do |opt_reward_general_menu_item|
                  (grouped_items_check_buy.index(filtered_item)+1).upto(grouped_items_check_buy.length-1) do |idx|
                    opt_filtered_item = grouped_items_check_buy[idx] rescue nil
                    break if opt_filtered_item.blank?
                    break if opt_filtered_item[:required_menu_item]
                    if opt_filtered_item[:general_menu_item_id].include?(opt_reward_general_menu_item.general_menu_item_id)
                      reward_discount_count = RewardDiscount.where(:check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :menu_item_id => opt_filtered_item[:menu_item_id], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum], :level_size_id => filtered_item[:level_size_id]).count
                      qty_selected = ordered_item_check_buy.select{|a| a[:required_menu_item] && a[:menu_item_combo_sum] == filtered_item[:menu_item_combo_sum]}.count
                      if reward_discount_count < qty_selected
                        #optional_menu_discount_items <<  opt_filtered_item
                        items_discount << {:menu_item_id => opt_filtered_item[:menu_item_id], :price => opt_filtered_item[:price_per_item], :menu_item_combo_sum => filtered_item[:menu_item_combo_sum]}
                      end
                    end
                  end
                end
                break
              end
            end
          end
        end

        #preset item discount if there not suitable combo
        items_discount = [] if !items_discount.blank? && !(required_menu_items.count <= items_discount.count)

        unless items_discount.blank?
          #PosRewardCheck.create(:pos_location_id => @pos_location.id, :chain_id => @pos_location.chain_id, :xml_data => params[:check])
          reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
          reward_code_user.destroy if reward_code_user
          p "here are the items that got the discount"
          p items_discount
          all_discount_item = items_discount.sum{ |a| a[:price]}
          if pos_discount_type.blank?
            calculated_discount_from_reward = all_discount_item
          elsif pos_discount_type.is_discount_percentage?
            calculated_discount_from_reward = ((pos_discount_type.discount_amount.to_f/100) * all_discount_item).round(2)
          elsif pos_discount_type.is_discount_price?
            calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? (all_discount_item - pos_discount_type.discount_amount) : opt_filtered_item[:price_per_item]).round(2)
          elsif pos_discount_type.is_discount_fixed_price?
            calculated_discount_from_reward = (all_discount_item > pos_discount_type.discount_amount ? pos_discount_type.discount_amount : all_discount_item).round(2)
          end
          items_discount.each do |item|
            RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :seq_num => seq_number, :restaurant_id => @pos_location.restaurant_id, :discount => item[:price], :menu_item_id => item[:menu_item_id], :menu_item_combo_sum => item[:menu_item_combo_sum])
          end
          pos_reward_check.status = PosRewardCheck::STATUS["SUCCESS"]
          pos_reward_check.user_id = (user.id rescue nil)
          pos_reward_check.save
          if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
            puts "chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW[FROM POS]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))"
            Delayed::Job.enqueue(PosRewardClaimedJob.new(reward_transaction))
            #user.burn(reward.points)
          end

          if chain.auto_loyalty && due_total == 0
            puts "chain.auto_loyalty && due_total == 0 Micros::Check.update_check_with_discount(xml_receipt, calculated_discount_from_reward)"
            xml_receipt = Micros::Check.update_check_with_discount(xml_receipt, calculated_discount_from_reward)
            Delayed::Job.enqueue(AutoLoyaltyJob.new(chain, @pos_location, user, nil, nil, location, xml_receipt, check_id, seq_number, revenue_center, payment_included, "usercode", nil))
            #user.burn(reward.points)
          end
          render :json => {:status => true, :discount => (calculated_discount_from_reward*-1), :tender_id => nil, :transaction_id => reward_transaction.id, :response_code => 0, :POS_message => ""} and return
        else
          pos_reward_check.status = PosRewardCheck::STATUS["ERROR"]
          pos_reward_check.user_id = (user.id rescue nil)
          pos_reward_check.error_description = "Reward requirement is not met."
          pos_reward_check.save
          #reward_transaction.update_column(:pos_used, true)
          render :json => {:status => false, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Reward requirement is not met."} and return
        end
      when "1"
        #reward_transaction = RewardTransaction.where("reward_id IN (?) AND pos_used IS FALSE", params[:transaction_id].blank? ? 0 : params[:transaction_id], reward_ids).first
        #render :json => {:status => true, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 1, :POS_message => "Transaction reversal invalid"}
        void_redeem_olo
      #reward_transaction.update_column(:pos_used, true)
      #render :json => {:status => true, :discount => nil, :tender_id => nil, :transaction_id => nil, :response_code => 0, :POS_message => "Transaction reversed"} and return
    end
  end

  def void_redeem_olo
    t_id = params["transaction_id"]
    reward_transaction = RewardTransaction.find(t_id)
    user_tmp = User.find reward_transaction.user_id
    rt = reward_transaction
    reward = rt.reward
    unless rt.blank?
      #pos_reward_history("success",rt.user_id)
      user_tmp.earn(reward.points) rescue nil
      PointHistory.add_to_history(user_tmp.id, "Void Redeem for ##{reward.name}", (reward.points * 1)) rescue nil
      RewardWallet.where(:user_id => user_tmp.id, :reward_id => reward.id).first.update_attribute(:status, RewardWallet::STATUS[:ACTIVE]) rescue nil
      rt.update_column(:pos_used, false) rescue nil

      return   render :json => {:status => true,  :response_code => 1, :message => "Void SUCCESS"}
    else
      return render :json => {:status => false, :response_code => 0 , :message => "Could not find the transaction !!"}
    end
  end


end
