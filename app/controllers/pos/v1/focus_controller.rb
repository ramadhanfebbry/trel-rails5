class Pos::V1::FocusController < ApplicationController

  before_action :authorize_apikey!

  def get_program_description
    p params
    render :xml => {:status => "OK", :description => "Description OK"} and return
  end

  def get_status
    render :xml => <<-EOF
                    <LoyaltyProgramStatus xmlns="http://api.loyaltycomapnyloyalty.com/v3_0/Data" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                      <Customer>
                        <Address/>
                        <Anniversary>1900-01-01T00:00:00</Anniversary>
                        <Birthday>1900-01-01T00:00:00</Birthday>
                        <City/>
                        <EmailAddress>mhamm@focuspos.com</EmailAddress>
                        <FaxNumber/>
                        <FirstName>Mike</FirstName>
                        <FullyRegistered>true</FullyRegistered>
                        <ID>36</ID>
                        <LastName>Hamm</LastName>
                        <MobileNumber>17135301180</MobileNumber>
                        <Options xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
                        <State/>
                        <Zipcode/>
                      </Customer>
                      <Global>
                        <CurrentCredits>1.00</CurrentCredits>
                        <LastVisit xmlns:a="http://schemas.datacontract.org/2004/07/System">
                        <a:DateTime>2012-07-07T21:48:55Z</a:DateTime>
                        <a:OffsetMinutes>0</a:OffsetMinutes>
                        </LastVisit>
                        <LifetimeCredits>1.00</LifetimeCredits>
                        <LifetimeRevenue>1.00</LifetimeRevenue>
                        <Rating>5</Rating>
                        <RatingName>Platinum</RatingName>
                        <RewardsEarned>0</RewardsEarned>
                      </Global>
                      <Local>
                        <CurrentCredits>1.00</CurrentCredits>
                        <LastVisit xmlns:a="http://schemas.datacontract.org/2004/07/System">
                        <a:DateTime>2012-07-07T21:48:55Z</a:DateTime>
                        <a:OffsetMinutes>0</a:OffsetMinutes>
                        </LastVisit>
                        <LifetimeCredits>1.00</LifetimeCredits>
                        <LifetimeRevenue>1.00</LifetimeRevenue>
                        <Rating>5</Rating>
                        <RatingName>Platinum</RatingName>
                        <RewardsEarned>0</RewardsEarned>
                      </Local>
                      <OpenCoupons/>
                    </LoyaltyProgramStatus>
                    EOF
  end

  def add_credits
    p params
  
    code =  params["customeridentifier"]
    render :status => 902, :json => {:status => false, :description => "Invalid code"} and return if code.blank?

    chain = @pos_location.chain
    location = @pos_location.restaurant

    ticket_id = params["ticket_id"].split("-")
    t_num = ticket_id[0]
    t_year = ticket_id[1]
    t_month = ticket_id[2]
    t_date = ticket_id[3]
    t_hour = ticket_id[4]
    t_minute = ticket_id[5]
    check_id = "#{@pos_location.restaurant_id}-#{t_num}-#{t_year}-#{t_month}-#{t_date}"

    code, code_type, user, user_code_type = Barcode.convert_and_get_code_type(chain, code)
    p "* " * 77
    p "code: #{code}"
    p "code_type: #{code_type}"
    p "user: #{user}"
    if code && code_type.eql?(AppcodeSetting::CODE_TYPES["USERCODE"])
      selected_code_type = "UserCode"
      pos_check_upload = FocusPos::CheckUpload.where("pos_location_id = ? AND check_id = ? AND date(TIMEZONE('UTC', focus_check_uploads.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') = date('#{(Time.zone.now).to_date}')", @pos_location.id, check_id).first
      unless user_code_type.eql?("STATIC")
        user_code_user = UserCodeUser.where(:chain_id => chain.id, :user_code => code).first
        user = user_code_user.try(:user)
        if user_code_user && user_code_user.expired_at && (user_code_user.expired_at < Time.zone.now)
          render :status => 904, :json => {:status => false, :description => "Expired code"} and return
        end
        if user.blank?
          render :status => 902, :json => {:status => false, :description => "Invalid code"} and return
        end
      end
      if pos_check_upload.blank?
        pos_check_upload = FocusPos::CheckUpload.create(
            :pos_location_id => @pos_location.id,
            :chain_id => @pos_location.chain_id,
            :check_id => check_id,
            :status => FocusPos::CheckUpload::STATUS[:NEW],
            :user_id => user.try(:id),
            :user_code => code,
            :code_type => selected_code_type
        )
        offer = location.first_active_offer
        if offer.blank?
          render :status => 900, :json => {:status => false, :description => "Invalid Offer"} and return
        end

        receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true)
        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.status = Receipt::STATUS[:RECEIVED]
        receipt.receipt_transactions << receipt_transaction
        receipt.save
        Delayed::Job.enqueue(FocusPos::ReceiptJob.new(pos_check_upload.id, user.id, @pos_location.restaurant_id, offer.id, receipt.id), :run_at => Time.zone.now + chain.pos_receipt_processing_delay.minutes, :delayable_type => "FocusCheckUpload", :delayable_id => pos_check_upload.id)
        user_session.update_attributes(:executed => true, :receipt_id => receipt.id) if user_session
        FocusPos::ReceiptCheckUpload.create(:receipt_id => receipt.id, :focus_check_upload_id => pos_check_upload.id)
        user_code_user.destroy if user_code_user
      elsif ![FocusPos::CheckUpload::STATUS[:PROCESSED], FocusPos::CheckUpload::STATUS[:CONVERTED]].include?(pos_check_upload.status)
        pos_check_upload.update_attributes(
            :code_type => selected_code_type,
            :user_code => code,
            :status => FocusPos::CheckUpload::STATUS[:UPDATED]
        )
        offer = location.first_active_offer
        if offer.blank?
          render :status => 900, :json => {:status => false, :description => "Invalid Offer"} and return
        end
        receipt = Receipt.new(:chain_id => user.chain_id, :user_id => user.id, :status => Receipt::STATUS[:RECEIVED], :is_receipt_barcode => true)
        receipt_transaction = ReceiptTransaction.new
        receipt_transaction.status = Receipt::STATUS[:RECEIVED]
        receipt.receipt_transactions << receipt_transaction
        receipt.save
        Delayed::Job.enqueue(FocusPos::ReceiptJob.new(pos_check_upload.id, user.id, @pos_location.restaurant_id, offer.id, receipt.id), :run_at => Time.zone.now + chain.pos_receipt_processing_delay.minutes, :delayable_type => "FocusCheckUpload", :delayable_id => pos_check_upload.id)
        user_session.update_attributes(:executed => true, :receipt_id => receipt.id) if user_session
        FocusPos::ReceiptCheckUpload.create(:receipt_id => receipt.id, :focus_check_upload_id => pos_check_upload.id)
        user_code_user.destroy if user_code_user
      else
        render :status => 901, :json => {:status => false, :description => "Already processed"} and return
      end
    else
      render :status => 902, :json => {status: false, :description => "invalid code"} and return
    end

    render :xml => '<decimal xmlns="http://schemas.microsoft.com/2003/10/Serialization/">3.00</decimal>' and return
  end

  def close_transaction_with_sku
    p params rescue "error params"
    p "params customeridentifier #{params['customeridentifier']}"
    p "params ArrayOfSKUItem #{params['ArrayOfSKUItem']}"
    code =  params["customeridentifier"]
    chain = @pos_location.chain

    ticket_id = params["ticket_id"].split("-")
    t_num = ticket_id[0]
    t_year = ticket_id[1]
    t_month = ticket_id[2]
    t_date = ticket_id[3]
    t_hour = ticket_id[4]
    t_minute = ticket_id[5]
    check_id = "#{@pos_location.restaurant_id}-#{t_num}-#{t_year}-#{t_month}-#{t_date}"

    pos_check_upload = FocusPos::CheckUpload.where("pos_location_id = ? AND check_id = ? AND date(TIMEZONE('UTC', focus_check_uploads.created_at) AT TIME ZONE '#{Time.zone.now.strftime('%Z')}') = date('#{(Time.zone.now).to_date}')", @pos_location.id, check_id).first
    if pos_check_upload.blank?
      pos_check_upload = FocusPos::CheckUpload.new(
          :pos_location_id => @pos_location.id,
          :chain_id => @pos_location.chain_id,
          :check_id => check_id,
          :status => FocusPos::CheckUpload::STATUS[:NEW],
          :check_data => params["ArrayOfSKUItem"].to_json,
      )
      pos_check_upload.save
    elsif ![FocusPos::CheckUpload::STATUS[:PROCESSED], FocusPos::CheckUpload::STATUS[:CONVERTED]].include?(pos_check_upload.status)
      pos_check_upload.check_data = params.to_json
      pos_check_upload.status = FocusPos::CheckUpload::STATUS[:UPDATED]
      pos_check_upload.save
    end
    pos_check_upload.create_detail rescue nil
    #RUN JOB IMEDIATELLY WHEN CHECK CLOSED BY SKU, DO NOT WAIT ANYMORE!!
    dj = DelayedJob.where(:delayable_type => "FocusCheckUpload", :delayable_id => pos_check_upload.id, :last_error => nil, :failed_at => nil).first
    if dj
      dj.run_at = Time.now
      dj.locked_at = nil
      dj.locked_by = nil
      dj.attempts = 0
      dj.save
    end
    render :xml => "<int xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">#{pos_check_upload.id}</int>"
  end

  def get_coupon
    p params
    transaction_time = Time.zone.now
    chain = @pos_location.chain
    location = @pos_location.restaurant
    reward_ids = chain.rewards.map(&:id)

    reward_code = params["code"].to_s.upcase
    reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
    reward_code_setting = JSON.parse(reward_code_setting) rescue nil
    reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
    if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
      if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
        reward_code = reward_code.to_ean_13_format
      end
    end
    reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
    reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => reward_code).first
    reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", reward_code, reward_ids)
    if reward_transactions.blank?
      render :status => 903, :json => {:status => false, :POS_message => "Reward code invalid."} and return
    end

    reward_code_expire = reward_code_setting["timer"].to_i
    if reward_code_expire != 0
      reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
    else
      reward_transaction = reward_transactions.order("created_at DESC").first
    end
    if reward_transaction.blank?
      reward_code_user.destroy if reward_code_user
      render :status => 904, :json => {:status => false, :POS_message => "Reward code expired"} and return
    end

    reward = reward_transaction.reward
    user = reward_transaction.user

    pos_discount_type = reward.pos_discount_type

    #non menu item discount based
    if (pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?) || (pos_discount_type && pos_discount_type.is_non_menu_item_based_percentage_off?)
      if pos_discount_type.is_non_menu_item_based_discount_fixed_price?
        discount_applied = pos_discount_type.discount_amount
        discount_type = "Currency"
      elsif pos_discount_type.is_non_menu_item_based_percentage_off?
        discount_applied = pos_discount_type.discount_amount
        discount_type = "Percentage"
      end
    
      created_date = transaction_time.strftime("%Y-%m-%dT%H:%M:%SZ")
      expiry_date = (transaction_time + 1.days).strftime("%Y-%m-%dT%H:%M:%SZ")

      render :xml => <<-EOF
                      <LoyaltyCoupon xmlns="http://api.loyaltycomapnyloyalty.com/v3_0/Data" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                      <Code>#{reward_code}</Code>
                      <CouponClass>Unknown</CouponClass>
                      <CouponType>EntireTicket</CouponType>
                      <CreateDate xmlns:a="http://schemas.datacontract.org/2004/07/System">
                        <a:DateTime>#{created_date}</a:DateTime><a:OffsetMinutes>-240</a:OffsetMinutes>
                      </CreateDate>
                      <Description>Free #{reward.name} for you!!</Description>
                      <Disclaimer i:nil="true"/>
                      <Discount>#{discount_applied}</Discount>
                      <DiscountType>#{discount_type}</DiscountType>
                      <ExpireDate xmlns:a="http://schemas.datacontract.org/2004/07/System">
                        <a:DateTime>#{expiry_date}</a:DateTime>
                        <a:OffsetMinutes>-240</a:OffsetMinutes>
                      </ExpireDate>
                      <QuantityDiscounted>0</QuantityDiscounted>
                      <RedeemDate xmlns:a="http://schemas.datacontract.org/2004/07/System">
                          <a:DateTime>#{created_date}</a:DateTime><a:OffsetMinutes>-300</a:OffsetMinutes>
                      </RedeemDate>
                      <SKUBasket xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
                      <a:string/>
                      </SKUBasket>
                      <SKUDiscounted/>
                      <SKUQuantityRequired i:nil="true" xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
                      <Source/>
                      <Title>#{reward.name}</Title>
                    </LoyaltyCoupon>
                  EOF
    else
      render :status => 906, :json => {:status => false, :POS_message => "DISCOUNT ERROR"} and return
    end
  end

  def redeem_coupon
    p params
    transaction_time = Time.zone.now
    chain = @pos_location.chain
    location = @pos_location.restaurant
    reward_ids = chain.rewards.map(&:id)

    reward_code = params["code"].to_s.upcase
    reward_code_setting = REDIS.get "rewardcode_chain_#{chain.id}"
    reward_code_setting = JSON.parse(reward_code_setting) rescue nil
    reward_code_setting ||= AppcodeSetting::DEFAULT_SETTING_REWARDCODE
    if reward_code_setting && reward_code_setting["appcode_type"].to_i.eql?(AppcodeSetting::TYPES["DYNAMIC"]) && reward_code_setting["code_type"].to_i.eql?(AppcodeSetting::CODE_TYPES["REWARDCODE"])
      if reward_code_setting["barcode_format_type"].to_i == Chain::BARCODE_FORMAT_TYPES["EAN 13"] && reward_code_setting["barcode_format"].to_i == 1
        reward_code = reward_code.to_ean_13_format
      end
    end
    reward_code = reward_code.remove_zero_padding(reward_code_setting["zero_padding"].to_i)
    pos_reward_check = FocusPos::RewardCheck.new(
        :pos_location_id => @pos_location.id,
        :chain_id => @pos_location.chain_id,
        :reward_code => reward_code
    )
    reward_code_user = RewardCodeUser.where(:chain_id => chain.id, :staffcode => reward_code).first
    reward_transactions = RewardTransaction.where("staffcode = ? AND reward_id IN (?) AND pos_used IS FALSE", reward_code, reward_ids)
    if reward_transactions.blank?
      pos_reward_check.status = FocusPos::RewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "Reward code invalid."
      pos_reward_check.save
      render :status => 903, :json => {:status => false, :POS_message => "Reward code invalid."} and return
    end

    reward_code_expire = reward_code_setting["timer"].to_i
    if reward_code_expire != 0
      reward_transaction = reward_transactions.where("created_at >= ?", Time.current - reward_code_expire.seconds).order("created_at DESC").first
    else
      reward_transaction = reward_transactions.order("created_at DESC").first
    end
    if reward_transaction.blank?
      reward_code_user.destroy if reward_code_user
      pos_reward_check.status = FocusPos::RewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "Reward code expired."
      pos_reward_check.save
      render :status => 904, :json => {:status => false, :POS_message => "Reward code expired"} and return
    end

    p "here is check id and seq number -----"
    reward = reward_transaction.reward
    user = reward_transaction.user

    p "here is check id and seq number -----"
    check_id = "focus-#{transaction_time.strftime("%Y/%m/%dT%H:%M:%S")}"

    if user.points < reward.points
      render :status => 905, :json => {:status => false, :POS_message => "User Point Does not enough to redeem"} and return
    end

    if chain.chain_setting.hybrid_milestone_regular_rewards_enabled && (user.point_threshold || 0) < reward.points
      render :status => 905, :json => {:status => false, :POS_message => "User Point Does not enough to redeem"} and return
    end

    reward_transaction.update_column(:check_number, [check_id, @pos_location.restaurant_id].join("-"))
    pos_discount_type = reward.pos_discount_type

    #non menu item discount based
    if (pos_discount_type && pos_discount_type.is_non_menu_item_based_discount_fixed_price?) || (pos_discount_type && pos_discount_type.is_non_menu_item_based_percentage_off?)
      if pos_discount_type.is_non_menu_item_based_discount_fixed_price?
        discount_applied = pos_discount_type.discount_amount
        discount_type = "Currency"
      elsif pos_discount_type.is_non_menu_item_based_percentage_off?
        discount_applied = pos_discount_type.discount_amount
        discount_type = "Percentage"
      end
      RewardDiscount.create(:reward_transaction_id => reward_transaction.id, :check_id => check_id, :restaurant_id => @pos_location.restaurant_id, :discount => discount_applied)
      pos_reward_check.status = FocusPos::RewardCheck::STATUS["SUCCESS"]
      pos_reward_check.user_id = (user.id rescue nil)
      pos_reward_check.save
      if chain.user_reward_redeemption_flow.eql?(Chain::REWARD_REDEEM_FLOW["FROM POS"]) || (chain.on_hybrid_setting_reward_redeem_flow? && chain.on_hybrid_setting_pos_based_rule?(@pos_location.restaurant))
        reward.claimed_by(user)
        Delayed::Job.enqueue(FocusPosRewardClaimedJob.new(reward_transaction.id))
      end

    reward_transaction.update_attributes(:pos_used => true, :restaurant_id => @pos_location.restaurant_id, :redeeming => false )
    reward_code_user.destroy if reward_code_user

    render :xml => <<-EOF
        <RedeemCouponResponse xmlns="http://api.loyaltycomapnyloyalty.com/v3_0/Data" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
          <CouponType>EntireTicket</CouponType>
          <Customer>
            <Address/>
            <Anniversary></Anniversary>
            <Birthday></Birthday>
            <City/>
            <EmailAddress>#{user.email}</EmailAddress>
            <FaxNumber/>
            <FirstName>#{user.first_name}</FirstName>
            <FullyRegistered>true</FullyRegistered>
            <ID>#{user.id}</ID>
            <LastName>#{user.last_name}</LastName>
            <MobileNumber>#{user.phone_number}</MobileNumber>
            <Options xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"/>
            <State/>
            <Zipcode/>
          </Customer>
        </RedeemCouponResponse>
      EOF
    else
      pos_reward_check.status = FocusPos::RewardCheck::STATUS["ERROR"]
      pos_reward_check.error_description = "DISCOUNT ERROR."
      pos_reward_check.save
      render :status => 906, :json => {:status => false, :POS_message => "DISCOUNT ERROR"} and return
    end
  end

  private

  def authorize_apikey!
    unless @pos_location = PosLocation.where(:apikey => params["apikey"]).first
      render :status => 404,
             :json => {status: false, notice: "Something wrong with your apikey"} and return
    end
  end

end
