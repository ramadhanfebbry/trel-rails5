/**
 * Copyright (c) 2012-2013 Dawid Kraczkowski. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 * TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
(function(e){var t;e.fn.ajaxUpload=function(n){var r=e.extend({accept:["*"],name:"file",method:"POST",url:"/",data:false,onSubmit:function(){return true},onComplete:function(){return true}},n);return this.each(function(){var n=e(this);n.css("position","relative");n.setData=function(e){r.data=e};var i=e('<form style="margin: 0px !important; padding: 0px !important; position: absolute; top: 0px; left: 0px;"'+' method="'+r.method+'" enctype="multipart/form-data" action="'+r.url+'">'+' <input name="'+r.name+'" type="file" /></form>');var s=i.find("input[name="+r.name+"]");s.css("display","block");s.css("overflow","hidden");s.css("width","100%");s.css("height","100%");s.css("text-align","right");s.css("opacity","0");s.css("z-index","999999");s.change(function(t){i.find("input[type=hidden]").remove();r.onSubmit.call(n,e(this));if(r.data){e.each(r.data,function(t,n){i.append(e('<input type="hidden" name="'+t+'" value="'+n+'">'))})}i.submit();e(i).find("input[type=file]").attr("disabled","disabled")});e(n).append(i);if(!t){t=e('<iframe id="__ajaxUploadIFRAME" name="__ajaxUploadIFRAME"></iframe>').attr("style",'style="width:0px;height:0px;border:0px solid #fff;"').hide();t.attr("src","");e(document.body).append(t)}var o=function(){e(i).find("input[type=file]").removeAttr("disabled");var s=e(this).contents().find("html body").text();r.onComplete.call(n,s);t.unbind()};i.submit(function(e){t.load(o);i.attr("target","__ajaxUploadIFRAME");e.stopPropagation()})})}})(jQuery)