// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require cocoon
//= require amcharts
//= require bootstrap
//= require chart
//= require cocoon
//= require fullcalendar.js
//= require fullcalendar.min
//= require highcharts
//= require jquery-latest
//= require jquery_ui_min
//= require jquery.ajaxloader.1.5.1
//= require jquery.bpopup-0.5.1.min
//= require jquerydatepick
//= require jquery.fancybox-1.3.4
//= require jquery.fancybox-1.3.4.pack
//= require jquery.jscrollpane
//= require jquery.jscrollpane.min
//= require jquery.mousewheel-3.0.4.pack
//= require jquery.mousewheel
//= require jquery.nyroModal
//= require jquery.simplemodal
//= require jquery.tablescroll
//= require jquery.wysiwyg
//= require modal.popup
//= require modernzr
//= require_directory ./admin


$(function() {
//    $("#offer_effectiveDate").datepicker({ dateFormat: 'yy-mm-dd' });
//    $("#offer_expiryDate").datepicker({ dateFormat: 'mm-dd-yy' });

//
//    $('#offer_timeEnd').timepicker({
//        ampm: true,
//        timeFormat: 'hh:mmTT',
//        stepHour: 0.5,
//        stepMinute: 5
//    });
//    $('#offer_timeStart').timepicker({
//        ampm: true,
//        timeFormat: 'hh:mmTT',
//        stepHour: 0.5,
//        stepMinute: 5
//    });
    $("#multiplier_slider").slider({
        value: $("#offer_multiplier").val(),        
        min: 1,
        max: 5,
        step: 0.1,
        slide: function(event, ui) {
            $("#offer_multiplier").val(ui.value);
            $("#perk_multiplier").val(ui.value);
        }
    });

    $('.x-time-picker').timepicker({
        ampm: true,
        timeFormat: 'hh:mmTT',
        stepHour: 0.5,
        stepMinute: 1
    });

    $(".x-datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $(".x-search-datepicker").datepicker({
        dateFormat: 'yy-mm-dd'
    });

    $(".multiselect").multiselect({
        selectedText: "# of # selected"
    });


    $(".user-types").bind("change", function() {
        if (this.value == 1) {
            $("#pwd").css("display", "")
        } else {
            $("#pwd").css("display", "none")
        }
    });
//    $.setAjaxPagination = function() {
//      return $('.pagination a').click(function(event) {
//        var loading;
//        event.preventDefault();
//        loading = $('<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>');
//        $('#x-records').prepend(loading);
//        loading.fadeIn();
//        $.ajax({
//          type: 'GET',
//          url: $(this).attr('href'),
//          dataType: 'script',
//          success: (function() {
//            return loading.fadeOut(function() {
//              return loading.remove();
//            });
//          })
//        });
//        return false;
//      });
//    };
//    return $.setAjaxPagination();
    $('.x-delete').bind('ajax:success', function(evt, data, status, xhr) {
        if (data == true || data == 'true') {
            $(this).closest('li').remove();
        } else {
            errorMessage(this, 'li', data.error);
        }
    });

    errorMessage = function(_this, tag, message) {
        var error = $("<span class='error'>" + message + "</span>");
        if (tag == 'after') {
            $(_this).after(error).fadeIn(400);
        } else {
            $(_this).closest(tag).append(error).fadeIn(400);
        }
        error.delay(8000).fadeOut(400);
    }
    successMessage = function(_this, tag, message) {
        var error = $("<span class='success'>" + data.error + "</span>");
        $(_this).closest('li').append(error).fadeIn(400);
        error.delay(8000).fadeOut(400);
    }

    $('.fancy-submit').bind('ajax:success', function(evt, data, status, xhr) {
        if (data.head) {
            var question = '<li>\
                    <span class="question" url="/admin/questions/' + data.question.id + '/edit">' + data.question.text + '</span>\
                    <a class="x-delete" rel="nofollow" data-remote="true" data-method="delete" data-confirm="Are you sure you want to delete selected choice?" href="/admin/questions/' + data.question.id + '">\
                        <img src="/assets/delete1.png" alt="Delete1"></a>';
            if (data.question.question_choices != '') {
                question += '<ul class="x-choice-list" id="question-' + data.question.id + '">';
                $.each(data.question.question_choices, function(key, choice) {
                    question += '<li>\
                            <span class="question-choice" name="label" url="/admin/choices/' + choice.id + '/edit">' + choice.label + '</span> --- \
                            <span class="question-choice" name="value" url="/admin/choices/' + choice.id + '/edit">' + choice.value + '</span>\
                            <a class="x-delete" rel="nofollow" data-remote="true" data-method="delete" data-confirm="Are you sure you want to delete selected choice?" href="/admin/choices/' + choice.id + '">\
                                <img src="/assets/delete1.png" alt="Delete1"></a>\
                          </li>';
                })

                question += '</ul>\
                            <a class="question-choice-add" url="/admin/choices/new" name="question-' + data.question.id + '" href="javascript:void(0)">\
                                <img src="/assets/add.png" alt="Add">\
                            </a>';
            }
            question += '</li>';
            $('.x-question-box').append(question)
            $.fancybox.close()
        } else {
            var errors = "<div><ul>";
            $.each(data, function(key, value) {
                errors += "<li><b>" + key + ": </b>" + value + "</li>";
            })
            errors += '</ul></div>';
            errors = $(errors)
            $(this).find('.errors').html(errors).fadeIn(400);
        }
    });

    $('.fancylink').fancybox({
        opacity          : true,
        overlayShow      : false,
        transitionIn     : 'elastic',
        scrolling        : 'no',
        autoScale        : true,
        autoDimensions   : true,
        centerOnScroll   : true
    })

    $("#formsubmit_push_point").click(function(){
        var iframe = $('<iframe name="postiframe_push_point" id="postiframe_push_point" style="display: none" />')
        $("body").append(iframe)

        var form = $('#theuploadform_push_point')
        form.attr("action", "/admin/push_points/preview_xls_data")
        form.attr("method", "post")
        form.attr("enctype", "multipart/form-data")
        form.attr("encoding", "multipart/form-data")
        form.attr("target", "postiframe_push_point")
        form.attr("xls", $('#userfile').val());
        form.submit()
        $("#postiframe_push_point").load(function(){
            iframeContents = $("#postiframe_push_point")[0].contentWindow.document.body.innerHTML
            $("#email_from_xls_area").html(iframeContents)
        })
        return false
    })


    $("#formsubmit").click(function () {

        var iframe = $('<iframe name="postiframe" id="postiframe" style="display: none" />');

        $("body").append(iframe);

        var form = $('#theuploadform');
        form.attr("action", "/admin/rewards/preview_xls_data");
        form.attr("method", "post");
        form.attr("enctype", "multipart/form-data");
        form.attr("encoding", "multipart/form-data");
        form.attr("target", "postiframe");
        form.attr("xls", $('#userfile').val());
        form.submit();

        $("#postiframe").load(function () {
            iframeContents = $("#postiframe")[0].contentWindow.document.body.innerHTML;
            $("#email_from_xls_area").html(iframeContents);
        });

        return false;

    });

    $('#email_template_code').bind('change', function(){
        $('#legend-email').slideUp();
        text = "";
        val = $(this).val();
        if(val == "welcome_chain_owner_email_body"){
          text = "Add this to content :<br />  <i> %{chain_owner_email} <br /> %{chain_owner_pwd} <br />  %{chain_name}   <br />   %{chain_locations} - lists all locations </i><br /><br />  ";
        }else if(val == "welcome_rest_owner_email_body"){
            text = "Add this to content : <br /> <i> %{rest_owner_email} <br /> %{rest_owner_pwd} <br />  %{chain_name}   <br />   %{rest_owner_locations} - lists all locations </i><br /> <br /> ";
        }

        $('#legend-email').html(text);
        $('#legend-email').slideDown();
    })
})

function show_max_winner_box(deal_type){
  if(deal_type == 2){
    $("#max_winner_box").slideDown();
  }else{
    $("#max_winner_box").slideUp();
  }
}