# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $('#x-search-survey').submit( ->
      loading = $ '<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>'
      $('#x-records').prepend loading
      loading.fadeIn()

      $.get(this.action, $(this).serialize(), null, 'script', success: (-> loading.fadeOut -> loading.remove()))
      false
  )

  $("#x-pagination-search .pagination a").bind("click", (event)->
    event.preventDefault()
    loading = $ '<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>'
    $('#x-records').prepend loading
    loading.fadeIn()
    page = $(@).attr('href').split("?")
    form = $('#receipts_search');
    $.post(form.attr('action'), form.serialize()+"&amp;"+page[1], null, 'script', success: (-> loading.fadeOut -> loading.remove()))
    false
  )

  $("#x-pagination .pagination a").bind("click", (event)->
    event.preventDefault()
    loading = $ '<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>'
    $('#x-records').prepend loading
    loading.fadeIn()
    $.ajax type: 'GET', url: $(@).attr('href'), dataType: 'script', success: (-> loading.fadeOut -> loading.remove())
    false
  )