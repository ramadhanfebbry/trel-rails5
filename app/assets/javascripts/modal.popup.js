var align = 'center';									//Valid values; left, right, center
var top = 0; 											//Use an integer (in pixels)
var width = 700; 										//Use an integer (in pixels)
var padding = 10;										//Use an integer (in pixels)
var backgroundColor = '#FFFFFF'; 						//Use any hex code
var source = ''; 								//Refer to any page on your server, external pages are not valid e.g. http://www.google.co.uk
var borderColor = '#333333'; 							//Use any hex code
var borderWeight = 4; 									//Use an integer (in pixels)
var borderRadius = 5; 									//Use an integer (in pixels)
var fadeOutTime = 300; 									//Use any integer, 0 = no fade
var disableColor = '#666666'; 							//Use any hex code
var disableOpacity = 40; 								//Valid range 0-100
var loadingImage = '/assets/loading.gif';		//Use relative path from this page

function modalPopup(align, top, width, padding, disableColor, disableOpacity, backgroundColor, borderColor, borderWeight, borderRadius, fadeOutTime, url, loadingImage){
    //Change these values to style your modal popup

    document.getElementsByTagName("BODY")[0].style.overflow = 'hidden';
    var containerid = "innerModalPopupDiv";

    var popupDiv = document.createElement('div');
    var popupMessage = document.createElement('div');
    var blockDiv = document.createElement('div');

    popupDiv.setAttribute('id', 'outerModalPopupDiv');
    popupDiv.setAttribute('class', 'outerModalPopupDiv');

    popupMessage.setAttribute('id', 'innerModalPopupDiv');
    popupMessage.setAttribute('class', 'innerModalPopupDiv');

    blockDiv.setAttribute('id', 'blockModalPopupDiv');
    blockDiv.setAttribute('class', 'blockModalPopupDiv');
    blockDiv.setAttribute('onClick', 'closePopup(' + fadeOutTime + ')');

    document.body.appendChild(popupDiv);
    popupDiv.appendChild(popupMessage);
    document.body.appendChild(blockDiv);

    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ //test for MSIE x.x;
        var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
        if(ieversion>6) {
            getScrollHeight(top);
        }
    } else {
        getScrollHeight(top);
    }
    // document.getElementById('outerModalPopupDiv').style.width = width + 'px';
    document.getElementById('outerModalPopupDiv').style.display='block';
    // document.getElementById('outerModalPopupDiv').style.width = width + 'px';
    document.getElementById('outerModalPopupDiv').style.width = '80%';
    document.getElementById('outerModalPopupDiv').style.padding = borderWeight + 'px';
    document.getElementById('outerModalPopupDiv').style.background = borderColor;
    document.getElementById('outerModalPopupDiv').style.borderRadius = borderRadius + 'px';
    document.getElementById('outerModalPopupDiv').style.MozBorderRadius = borderRadius + 'px';
    document.getElementById('outerModalPopupDiv').style.WebkitBorderRadius = borderRadius + 'px';
    document.getElementById('outerModalPopupDiv').style.borderWidth = 0 + 'px';
    document.getElementById('outerModalPopupDiv').style.position = 'fixed';
    document.getElementById('outerModalPopupDiv').style.zIndex = 100;
    document.getElementById('outerModalPopupDiv').style.overflowY = 'auto';
    document.getElementById('innerModalPopupDiv').style.padding = padding + 'px';
    document.getElementById('innerModalPopupDiv').style.background = backgroundColor;
    document.getElementById('innerModalPopupDiv').style.borderRadius = (borderRadius - 3) + 'px';
    document.getElementById('innerModalPopupDiv').style.MozBorderRadius = (borderRadius - 3) + 'px';
    document.getElementById('innerModalPopupDiv').style.WebkitBorderRadius = (borderRadius - 3) + 'px';

    document.getElementById('blockModalPopupDiv').style.width = 100 + '%';
    document.getElementById('blockModalPopupDiv').style.border = 0 + 'px';
    document.getElementById('blockModalPopupDiv').style.padding = 0 + 'px';
    document.getElementById('blockModalPopupDiv').style.margin = 0 + 'px';
    document.getElementById('blockModalPopupDiv').style.background = disableColor;
    document.getElementById('blockModalPopupDiv').style.opacity = (disableOpacity / 100);
    document.getElementById('blockModalPopupDiv').style.filter = 'alpha(Opacity=' + disableOpacity + ')';
    document.getElementById('blockModalPopupDiv').style.zIndex = 99;
    document.getElementById('blockModalPopupDiv').style.position = 'fixed';
    document.getElementById('blockModalPopupDiv').style.top = 0 + 'px';
    document.getElementById('blockModalPopupDiv').style.left = 0 + 'px';

    if(align=="center") {
        document.getElementById('outerModalPopupDiv').style.marginLeft = (-1 * (width / 2)) + 'px';
        document.getElementById('outerModalPopupDiv').style.left = 50 + '%';
    } else if(align=="left") {
        document.getElementById('outerModalPopupDiv').style.marginLeft = 0 + 'px';
        document.getElementById('outerModalPopupDiv').style.left = 10 + 'px';
    } else if(align=="right") {
        document.getElementById('outerModalPopupDiv').style.marginRight = 0 + 'px';
        document.getElementById('outerModalPopupDiv').style.right = 10 + 'px';
    } else {
        document.getElementById('outerModalPopupDiv').style.marginLeft = (-1 * (width / 2)) + 'px';
        document.getElementById('outerModalPopupDiv').style.left = 50 + '%';
    }

    document.getElementById('outerModalPopupDiv').style.position = 'fixed';
    document.getElementById('outerModalPopupDiv').style.top = '10%';
    document.getElementById('outerModalPopupDiv').style.float = 'none';
    document.getElementById('outerModalPopupDiv').style.margin = 'auto';
    document.getElementById('outerModalPopupDiv').style.left = '0';
    document.getElementById('outerModalPopupDiv').style.right = '0';
    document.getElementById('outerModalPopupDiv').style.maxHeight = '600px';

    blockPage();

    var page_request = false;
    if (window.XMLHttpRequest) {
        page_request = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        try {
            page_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                page_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) { }
        }
    } else {
        return false;
    }


    page_request.onreadystatechange=function(){
        if((url.search(/.jpg/i)==-1) && (url.search(/.jpeg/i)==-1) && (url.search(/.png/i)==-1) && (url.search(/.bmp/i)==-1)) {
            pageloader(page_request, containerid, loadingImage);
        } else {
            imageloader(url, containerid, loadingImage);
        }
    }

    page_request.open('GET', url, true);
    page_request.send(null);

}

function pageloader(page_request, containerid, loadingImage){

    document.getElementById(containerid).innerHTML = '<div align="center"><img src="' + loadingImage + '" border="0" /></div>';

    if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1)) {
        document.getElementById(containerid).innerHTML=page_request.responseText;
    }

}

function imageloader(url, containerid, loadingImage) {

    document.getElementById(containerid).innerHTML = '<div align="center"><img src="' + loadingImage + '" border="0" /></div>';
    document.getElementById(containerid).innerHTML='<div align="center"><img src="' + url + '" border="0" /></div>';

}

function blockPage() {

    var blockdiv = document.getElementById('blockModalPopupDiv');
    var height = screen.height;

    blockdiv.style.height = height + 'px';
    blockdiv.style.display = 'block';

}

function getScrollHeight(top) {

    var h = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop;

    if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {

        var ieversion=new Number(RegExp.$1);
        if(ieversion>6) {
            document.getElementById('outerModalPopupDiv').style.top = h + top + 'px';
        } else {
            document.getElementById('outerModalPopupDiv').style.top = top + 'px';
        }

    } else {
        document.getElementById('outerModalPopupDiv').style.top = '50%';
    }

}

function closePopup(fadeOutTime) {
    //$('#sub').click();
    if (document.getElementById('blockModalPopupDiv') == null) {
        //console.log('new');
    } else {
        //console.log('ayaa')
      // $('#sub').click();
        var a = window.location.href;
        console.log('asdfasdfasfsadf');
        document.getElementsByTagName("BODY")[0].style.overflow = 'auto';
        if (a.indexOf('/admin/rewards') > -1){
            ch_id = $('#chain').val();
            window.location.href = '/admin/rewards?chain_id='+ch_id;
            return false;
        }
    }


    fade('outerModalPopupDiv', fadeOutTime);
    document.getElementById('blockModalPopupDiv').style.display='none';

}

function fade(id, fadeOutTime) {

    var el = document.getElementById(id);

    if(el == null) {
        return;
    }

    if(el.FadeState == null) {

        if(el.style.opacity == null || el.style.opacity == '' || el.style.opacity == '1') {
            el.FadeState = 2;
        } else {
            el.FadeState = -2;
        }

    }

    if(el.FadeState == 1 || el.FadeState == -1) {

        el.FadeState = el.FadeState == 1 ? -1 : 1;
        el.fadeTimeLeft = fadeOutTime - el.fadeTimeLeft;

    } else {

        el.FadeState = el.FadeState == 2 ? -1 : 1;
        el.fadeTimeLeft = fadeOutTime;
        setTimeout("animateFade(" + new Date().getTime() + ",'" + id + "','" + fadeOutTime + "')", 33);

    }

}

function animateFade(lastTick, id, fadeOutTime) {

    var currentTick = new Date().getTime();
    var totalTicks = currentTick - lastTick;

    var el = document.getElementById(id);

    if(el.fadeTimeLeft <= totalTicks) {

        el.style.opacity = el.FadeState == 1 ? '1' : '0';
        el.style.filter = 'alpha(opacity = ' + (el.FadeState == 1 ? '100' : '0') + ')';
        el.FadeState = el.FadeState == 1 ? 2 : -2;
        document.body.removeChild(el);
        return;

    }

    el.fadeTimeLeft -= totalTicks;
    var newOpVal = el.fadeTimeLeft / fadeOutTime;

    if(el.FadeState == 1) {
        newOpVal = 1 - newOpVal;
    }

    el.style.opacity = newOpVal;
    el.style.filter = 'alpha(opacity = ' + (newOpVal*100) + ')';

    setTimeout("animateFade(" + currentTick + ",'" + id + "','" + fadeOutTime + "')", 33);

}