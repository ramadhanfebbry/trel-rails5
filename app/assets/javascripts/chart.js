$(document).ready(function() {
  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'chart-container',
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      backgroundColor: '#E3E3E4',
      height: 200,
      marginTop: -10,
      animation: false,
      width: 226
    },
    title: {
      /* text: '1 Free Topping <br /><h5>December, 1 2012</h5>',
            style: {
                fontWeight: 'bold'
            }*/
      text : false
    },
    credits: {
      enabled:false
    },
    tooltip: {
      formatter: function() {
        return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          color: '#000000',
          connectorColor: '#000000',
          formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
      ['Firefox',   45.0],
      ['IE',       26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari',    8.5],
      ['Opera',     6.2],
      ['Others',   0.7]
      ]
    }]
  });


  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'chart-container3',
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      backgroundColor: '#E3E3E4',
      height: 200,
      marginTop: -10,
      width: 226
    },
    title: {
      /* text: '1 Free Topping <br /><h5>December, 1 2012</h5>',
            style: {
                fontWeight: 'bold'
            }*/
      text : false
    },
     credits: {
      enabled:false
    },
    tooltip: {
      formatter: function() {
        return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          color: '#000000',
          connectorColor: '#000000',
          formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
      ['Firefox',   45.0],
      ['IE',       26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari',    8.5],
      ['Opera',     6.2],
      ['Others',   0.7]
      ]
    }]
  });

  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'chart-container2',
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      backgroundColor: '#E3E3E4',
      height: 200,
      marginTop: -10,
      width: 226
    },
    title: {
      /* text: '1 Free Topping <br /><h5>December, 1 2012</h5>',
            style: {
                fontWeight: 'bold'
            }*/
      text : false
    },
     credits: {
      enabled:false
    },
    tooltip: {
      formatter: function() {
        return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          color: '#000000',
          connectorColor: '#000000',
          formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
      ['Firefox',   45.0],
      ['IE',       26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari',    8.5],
      ['Opera',     6.2],
      ['Others',   0.7]
      ]
    }]
  });

  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'chart-container4',
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      backgroundColor: '#E3E3E4',
      height: 200,
      marginTop: -10,
      width: 226
    },
    title: {
      /* text: '1 Free Topping <br /><h5>December, 1 2012</h5>',
            style: {
                fontWeight: 'bold'
            }*/
      text : false
    },
     credits: {
      enabled:false
    },
    tooltip: {
      formatter: function() {
        return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          color: '#000000',
          connectorColor: '#000000',
          formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
      ['Firefox',   45.0],
      ['IE',       26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari',    8.5],
      ['Opera',     6.2],
      ['Others',   0.7]
      ]
    }]
  });

  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'chart-container5',
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      backgroundColor: '#E3E3E4',
      height: 200,
      marginTop: -10,
      width: 226
    },
     credits: {
      enabled:false
    },
    title: {
      /* text: '1 Free Topping <br /><h5>December, 1 2012</h5>',
            style: {
                fontWeight: 'bold'
            }*/
      text : false
    },
    tooltip: {
      formatter: function() {
        return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          color: '#000000',
          connectorColor: '#000000',
          formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
      ['Firefox',   45.0],
      ['IE',       26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari',    8.5],
      ['Opera',     6.2],
      ['Others',   0.7]
      ]
    }]
  });
  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'chart-container6',
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      backgroundColor: '#E3E3E4',
      height: 200,
      marginTop: -10,
      width: 226
    },
     credits: {
      enabled:false
    },
    title: {
      /* text: '1 Free Topping <br /><h5>December, 1 2012</h5>',
            style: {
                fontWeight: 'bold'
            }*/
      text : false
    },
    tooltip: {
      formatter: function() {
        return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          color: '#000000',
          connectorColor: '#000000',
          formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
      ['Firefox',   45.0],
      ['IE',       26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari',    8.5],
      ['Opera',     6.2],
      ['Others',   0.7]
      ]
    }]
  });

  chart = new Highcharts.Chart({
    chart: {
      renderTo: 'chart-container7',
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      backgroundColor: '#E3E3E4',
      height: 200,
      marginTop: -10,
      width: 226
    },
     credits: {
      enabled:false
    },
    title: {
      /* text: '1 Free Topping <br /><h5>December, 1 2012</h5>',
            style: {
                fontWeight: 'bold'
            }*/
      text : false
    },
    tooltip: {
      formatter: function() {
        return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: false,
          color: '#000000',
          connectorColor: '#000000',
          formatter: function() {
            return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Browser share',
      data: [
      ['Firefox',   45.0],
      ['IE',       26.8],
      {
        name: 'Chrome',
        y: 12.8,
        sliced: true,
        selected: true
      },
      ['Safari',    8.5],
      ['Opera',     6.2],
      ['Others',   0.7]
      ]
    }]
  });
});