# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


$(document).ready ->
  $('#pos_location_chain_id').change ->
    if this.value
      options = ''
      $('#loader').show()
      $.getJSON '/admin/pos_locations/' + this.value + '/chain_restaurants', (response) ->
        $.each response, (i, obj) ->
          options += '<option value="' + obj.id + '">' + obj.name + '</option>'
        $("#pos_location_restaurant_id").html(options)
        $('#loader').hide()
    else
      $('#pos_location_restaurant_id').html("<option value=''>Please select</option>")
