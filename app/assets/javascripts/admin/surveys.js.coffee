# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $(".question").bind('click', ->
    $(".question").editInPlace({
          callback:(unused, enteredText, original)->
            $.get($(this).attr('url'), "question[text]="+enteredText, (response)->
              if(response != true)
                _this.html(original)
                errorMessage(_this, 'li', response.error['text'])
            )
            return  enteredText
          bg_over: "#cff"
          field_type: "text"
          value_required: true
          default_text: "(Click here to add text)"
         # saving_image: "./images/ajax-loader.gif"
      })
  )

  $(".question-choice").bind('click', ->
    $(".question-choice").editInPlace({
          callback:(unused, enteredText, original)->
            _this = $(this)
            $.get($(this).attr('url'), "choice["+$(this).attr('name')+"]="+enteredText, (response)->
              if(response != true)
                _this.html(original)
                errorMessage(_this, 'li', response.error[_this.attr('name')])
            )
            return  enteredText
          bg_over: "#cff"
          field_type: "text"
          value_required: true
          default_text: "(Click here to add text)"
         # saving_image: "./images/ajax-loader.gif"
      })
  )


  $(".question-choice-new").bind('click', ->
    $(".question-choice-new").editInPlace({
          callback:(unused, enteredText)->
            field = $(this).attr('xname')
            $("#"+field).val(enteredText)
            return  enteredText
          bg_over: "#cff"
          field_type: "text"
          value_required: true
          default_text: "(Click here to add text)"
         # saving_image: "./images/ajax-loader.gif"
      })
  )

#  $(".question-choice").editInPlace({
#      callback:(unused, enteredText)->
#        $.get($(this).attr('url'), "choice[text]="+enteredText, (response)->
#        )
#        return  enteredText
#      bg_over: "#cff"
#      value_required: true
#      field_type: "text"
#     # saving_image: "./images/ajax-loader.gif"
#  })

  $("#question_question_type").bind('change', ->
    $("#question-wrapper").show()
    if(this.value=='1')
      $("#choice-wrapper").remove()
      $("#x-question").fadeIn()
    else if(this.value=='2')
      options = "<div id='choice-wrapper'><ul class='x-choice-list' id='form-choices-box'>"
      count=0

      while (count < 5)
        count += 1
        options += "<li>\
                <span class='question-choice-new' xname='label"+count+"' name='label'>Choice"+count+" </span> --- \
                <span class='question-choice-new' xname='value"+count+"' name='value'>0</span> \

                <input type='hidden' id='label"+count+"' name='question[question_choices_attributes][][label]' value='Choice"+count+"'/>\
                <input type='hidden' id='value"+count+"' name='question[question_choices_attributes][][value]' value='0'/>\
                <a class='x-choice-del' href='javascript:void(0)'><img src='/assets/delete1.png'></a>\
              </li>"

      options += "</ul><a class='x-choice-add' href='javascript:void(0)'><img src='/assets/add.png'></a></div>"
      $("#x-question").append(options)
    else
      $("#question-wrapper").hide()
  )

  $(".x-choice-add").bind('click', ->
    len = $("#form-choices-box li").size()
    if(len<10)
      choice = "<li>\
                <span class='question-choice-new' xname='label"+(len+1)+"' name='label'>Choice"+(len+1)+" </span> --- \
                <span class='question-choice-new' xname='value"+(len+1)+"' name='value'>0</span>\
                <input type='hidden' id='label"+(len+1)+"' name='question[question_choices_attributes][][label]' value='Choice"+(len+1)+"'/>\
                <input type='hidden' id='value"+(len+1)+"' name='question[question_choices_attributes][][value]' value='0'/>\
                <a class='x-choice-del' href='javascript:void(0)'><img src='/assets/delete1.png'></a>\
              </li>"
      $("#form-choices-box").append(choice)
    else
       errorMessage(this, 'after', "You cannot add more then 10 choices.")
  )

  $(".x-choice-del").bind('click', ->
     len = $("#form-choices-box li").size()
     if(len<=2)
       errorMessage(this, 'li', "question must have at least 2 choices")
     else
       $(this).parent().remove()
  )


  $(".question-choice-add").bind('click', ->
    question = this.name.split('-')
    $this = this
    $.getJSON $(this).attr('url'),"choice[label]=New Choice&amp;choice[question_id]="+question[1],(response) ->
      form= "<li>\
        <span class='question-choice' name='label' url='"+response.url+"'>New Choice</span> --- \
        <span class='question-choice' name='value' url='"+response.url+"'>0</span>\
        <a class='x-delete' rel='nofollow' data-remote='true' data-method='delete' data-confirm='Are you sure you want to delete selected choice?' href='"+response.delete_url+"'>\
        <img src='/assets/delete1.png' alt='Delete1'>\
        </a>\
      </li>"
      if(response.head==true || response.head=='true')
        $("#"+$this.name).append(form)
      else
        errorMessage(this, 'li', response.error)
  )

  $('.chain_surveys').change ->
     if this.value
         options = ''
         $.getJSON '/admin/chains/'+this.value+"/surveys",(response) ->
         		$.each response, (i, obj) ->
         			options += '<option value="'+obj.id+'">'+obj.title+'</option>';

         		$("#surveys").html(options)
         		$("#surveys").multiselect('refresh')
     else
          $('#surveys').html("<option>Please select</option>")

  $('.chain_surveys_online').change ->
    if this.value
      options = ''
      $.getJSON '/admin/chains/'+this.value+"/surveys?online=true",(response) ->
        $.each response, (i, obj) ->
          options += '<option value="'+obj.id+'">'+obj.title+'</option>';

        $("#surveys").html(options)
        $("#surveys").multiselect('refresh')
    else
      $('#surveys').html("<option>Please select</option>")