# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(document).ready ->
  $("div#full_token").hide();
  $('body').delegate ".short_token", 'click', (evt) ->
      elem_id = $(this).attr("id");
      arr_elem = elem_id.split("_");
      id = arr_elem[arr_elem.length-1];
      token = $("#full_token_"+id).text();
      $('.modal_text').text(token);
      $('#myModal').show();
      return false;

  $('body').delegate ".get-auth-token", 'click', (evt) ->
      email = $(this).attr("email");
      token = $(this).attr("token");
      $('span.authUser').text(email);
      $('.authentication_token_value').text(token);
      $('#getAuthToken').show();
      return false;