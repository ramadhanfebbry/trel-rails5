# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $("#receipt_chain_id").change ->
    if this.value
      offer_options = ''
      user_options = ''
      app_options = ''
      $('#loader_chain').show()
      $.getJSON '/admin/chain_offers/'+this.value,(response) ->
        $.each response, (i, obj) ->
          offer_options += '<option value="'+obj.id+'">'+obj.name+'</option>';
        $("#offer").html(offer_options)

      $.getJSON '/admin/chain_appkeys/'+this.value,(response) ->
        $.each response, (i, obj) ->
          app_options += '<option value="'+obj.appkey+'">'+obj.appkey+'</option>';
        $("#appkey").html(app_options)
        $("#search_users_admin_receipts").show()
        $("#chain_app_key").val($("#appkey").val())
        $('#loader_chain').hide()
    else
      $('#offer').html("<option value=''>Please select</option>")
      $('#appkey').html("<option value=''>Please select App</option>")

  $('#offer').change ->
      if this.value
         options = ''
         $('#loader').show()
         $.getJSON 'offer_restaurants/'+this.value,(response) ->
         	 $.each response, (i, obj) ->
         		 options += '<option value="'+obj.id+'">'+obj.name+'</option>';
         	 $("#restaurant").html(options)
           $('#loader').hide()
      else
         $('#restaurant').html("<option value=''>Please select</option>")

  $('#receipt_status').change ->
      if this.value != '4'
          $("#receipt_transaction_reject_reason_id").html("<option>Please select</option>")
      else
          options = ""
          $.getJSON '../reject_reasons/'+this.value,(response) ->
            $.each response, (i, obj) ->
                options += '<option value="'+obj.id+'">'+obj.description+'</option>';

            $("#receipt_transaction_reject_reason_id").html(options)


  $('#x-search').submit( ->
    loading = $ '<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>'
    $('#x-records').prepend loading
    loading.fadeIn()

    $.get(this.action, $(this).serialize(), null, 'script', success: (-> loading.fadeOut -> loading.remove()))
    false
  )

  $("#x-records").delegate(".x-csv-search", "click", (event)->
    event.preventDefault()
    page = $(@).attr('href').split("?")
    $.post(page[0], page[1], null, 'script', success: (-> loading.fadeOut -> loading.remove()))
  )

  $('#x-search-submit-btn, #x-reset-search-btn, .r-paging a').click( ->
    $("#list-receipts").ajaxloader('show')
  )

  $("#search_users_admin_receipts").hide()
  $("input#user").val('')
  $("#receipt_chain_id").val($("#receipt_chain_id option:first").val());
  $("#receipt_chain_id").change()

#  $("#x-pagination-search .pagination a").bind("click", (event)->
#    event.preventDefault()
#    loading = $ '<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>'
#    $('#x-records').prepend loading
#    loading.fadeIn()
#    page = $(@).attr('href').split("?")
#    form = $('#receipts_search');
#    $.post(form.attr('action'), form.serialize()+"&amp;"+page[1], null, 'script', success: (-> loading.fadeOut -> loading.remove()))
#    false
#  )
#
#  $("#x-pagination .pagination a").bind("click", (event)->
#    event.preventDefault()
#    loading = $ '<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>'
#    $('#x-records').prepend loading
#    loading.fadeIn()
#    $.ajax type: 'GET', url: $(@).attr('href'), dataType: 'script', success: (-> loading.fadeOut -> loading.remove())
#    false
#  )

#  $.setAjaxPagination = ->
#    $('#x-pagination .pagination a').click (event) ->
#      event.preventDefault()
#      loading = $ '<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>'
#      $('#x-records').prepend loading
#      loading.fadeIn()
#      $.ajax type: 'GET', url: $(@).attr('href'), dataType: 'script', success: (-> loading.fadeOut -> loading.remove())
#      false
#
#    $('#x-pagination-search .pagination a').click (event) ->
#      event.preventDefault()
#      loading = $ '<div id="loading" style="display: none;"><span><img src="/assets/loading.gif" alt="cargando..."/></span></div>'
#      $('#x-records').prepend loading
#      loading.fadeIn()
#      page = $(@).attr('href').split("?")
#      form = $('#receipts_search');
#      $.post(form.attr('action'), form.serialize()+"&amp;"+page[1], null, 'script', success: (-> loading.fadeOut -> loading.remove()))
#      false
#
#  $.setAjaxPagination()