$(document).ready ->
  $('#country').change ->
      if this.value
         options = ''
         $('#loader').show()
         $.getJSON '/admin/regions_by_country/'+this.value,(response) ->
         	 $.each response, (i, obj) ->
         		 options += '<option value="'+obj.id+'">'+obj.name+'</option>';
         	 $("#city_region_id").html(options)
           $('#loader').hide()
      else
         $('#city_region_id').html("<option value=''>Please select</option>")

  $('#citycountry').change ->
      if this.value
         options = '<option value="">Please select state</option>'
         $('#loader').show()
         $.getJSON '/admin/regions_by_country/'+this.value,(response) ->
         	 $.each response, (i, obj) ->
         		 options += '<option value="'+obj.id+'">'+obj.name+'</option>';
         	 $("#cityregion").html(options)
           $('#loader').hide()
      else
         $('#cityregion').html("<option value=''>Please select</option>")

  $.ajaxSetup error: (xhr, status, err) ->
    $('#session_exp_link').click() if xhr.status is 401