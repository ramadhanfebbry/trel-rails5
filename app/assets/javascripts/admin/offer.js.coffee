# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $('#time_limit_show_or_hide').toggle ->
    $('#offer_timeStart').data('time_start', $('#offer_timeStart').val())
    $('#offer_timeEnd').data('time_end', $('#offer_timeEnd').val())
    $('#offer_timeStart').val('')
    $('#offer_timeEnd').val('')
    $("#time_limit").hide()
    $("#time_limit_show_or_hide").html("Show Offer Hours Option")
  ,->
    $('#offer_timeStart').val($('#offer_timeStart').data('time_start'))
    $('#offer_timeEnd').val($('#offer_timeEnd').data('time_end'))
    $("#time_limit").show()
    $("#time_limit_show_or_hide").html("Hide Offer Hours Option")
  try
    $('#all_participants').checkAll('input:checkbox[name="pullout[]"]')
  catch e
    console.log "The value " +e

  try
    $('#all_non_participants').checkAll('input:checkbox[name="participate[]"]')
  catch e
    console.log "The value " +e

  $('.bonus_option').click ->
    if this.value == 'yes'
       $('#offer_'+this.name).attr("disabled", false)
    else
       $('#offer_'+this.name).attr("disabled", true)
       $('#offer_'+this.name).attr("value",0);


  $('#offer_chain_id').change ->
     if this.value
         options = ''
         $.getJSON '/admin/chains/'+this.value+"/restaurants",(response) ->
         		$.each response, (i, obj) ->
         			options += '<option value="'+obj.id+'">'+obj.name+'</option>';

         		$("#restaurants").html(options)
         		$("#restaurants").multiselect('refresh')
     else
          $('#offer_restaurant_id').html("<option>Please select</option>")
