# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $('#restaurant_city_text').autocomplete(
    source: '/admin/cities/lookup',
    minLength: 2,
    delay: 1000,
    focus: (event, ui) ->
      $('#restaurant_city_text').val(ui.item.label)
      false
    ,
    select: (event, ui) ->
      $('#restaurant_city_text').val(ui.item.label)
      $('#restaurant_city_id').val(ui.item.id)
      false
    ,
    change: (event, ui) ->
      # Selected an item, nothing to do
      if ui.item
        return
      # Search for a match (case-insensitive)
      value = $(this).val()
      valueLowerCase = value.toLowerCase()
      valid = false
      $(this).children('option').each ->
        if $(this).text().toLowerCase() == valueLowerCase
          @selected = valid = true
          return false
        return
      # Found a match, nothing to do
      if valid
        return
      # Remove invalid value
      $(this).val ''
      $('#restaurant_city_text').val ''
      $('#restaurant_city_id').val ''
      return
  )

  $('#chain').change ->
    $('#loader_restaurant').show();
    if $(@).val() == ''
      $.get '/admin/restaurants/by_chain?chain_id=0'
    else
      $.get '/admin/restaurants/by_chain?chain_id='+$(@).val()
      