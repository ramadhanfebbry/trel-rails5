# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $('#owner_city_text').autocomplete(
    source: '/admin/cities/lookup',
    minLength: 2,
    focus: (event, ui) ->
    	$('#owner_city_text').val(ui.item.label)
    	false
    ,
    select: (event, ui) ->
      $('#owner_city_text').val(ui.item.label)
      $('#owner_city_id').val(ui.item.id)
      false
  )