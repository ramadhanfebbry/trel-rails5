$(document).ready ->
  $("div#full_job_handler").hide();
  $("div#full_job_last_error").hide();
  $('body').delegate ".short_token_job_handler", 'click', (evt) ->
    elem_id = $(this).attr("id");
    arr_elem = elem_id.split("_");
    id = arr_elem[arr_elem.length-1];
    token = $("#full_job_handler_"+id).text();
    $('.modal_text').text(token);
    $('#myModal').show();
    return false;

  $('body').delegate ".short_token_job_last_error", 'click', (evt) ->
    elem_id = $(this).attr("id");
    arr_elem = elem_id.split("_");
    id = arr_elem[arr_elem.length-1];
    token = $("#full_job_last_error_"+id).text();
    $('.modal_text').text(token);
    $('#myModal').show();
    return false;