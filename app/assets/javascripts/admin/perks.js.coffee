$(document).ready ->
  $('.chain_perks').change ->
     if this.value
         options = ''
         $.getJSON '/admin/chains/'+this.value+"/restaurants",(response) ->
         		$.each response, (i, obj) ->
         			options += '<option value="'+obj.id+'">'+obj.name+'</option>';

         		$("#restaurant_ids_").html(options)
         		$("#restaurant_ids_").multiselect('refresh')
         		$("#restaurant_ids_").multiselect('uncheckAll')
     else
          $('#surveys').html("<option>Please select</option>")