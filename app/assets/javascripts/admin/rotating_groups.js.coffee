# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


$(document).ready ->
  $('#chain_rotating_select').change ->
    $('#data-container').ajaxloader('show')
    if($(this).val() == "")
      $.get('/admin/rotating_groups/0/search_by_chain')
    else
      $.get('/admin/rotating_groups/'+$(this).val()+'/search_by_chain')