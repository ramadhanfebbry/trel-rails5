# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->
  $(".calculate_points").blur ->
    subtotal = parseFloat($("#receipt_transaction_subtotal").val()) || 0
    multiplier = $("#multiplier").val() || 0
    rounding_opts = $("#rounding_preference").val() || "ROUND"
    is_multiplier_offer = $('#is_multiplier').val() || "TRUE"
    if(is_multiplier_offer == "TRUE")
      bonus_points = parseFloat($("#receipt_transaction_bonus_earned").val()) || 0
      base_points = parseFloat(subtotal*multiplier)
    else
      bonus_points = 0
      base_points = parseFloat(multiplier)
    total_points_earned = parseFloat((base_points+bonus_points))
    if(rounding_opts == "ROUND")
      total_points_rounding = Math.round(total_points_earned)
    else if(rounding_opts == "ROUND UP")
      total_points_rounding = Math.ceil(total_points_earned)
    else if(rounding_opts == "ROUND DOWN")
      total_points_rounding = Math.floor(total_points_earned)
    $("#tmp_total_points_earned").val(total_points_rounding);
    $("#receipt_transaction_base_points_earned").val(base_points)
    $("#receipt_transaction_total_points_earned").val(total_points_earned)


  subtotal = parseFloat($("#receipt_transaction_subtotal").val()) || 0
  multiplier = $("#multiplier").val() || 0
  rounding_opts = $("#rounding_preference").val() || "ROUND"
  is_multiplier_offer = $('#is_multiplier').val() || "TRUE"
  if(is_multiplier_offer == "TRUE")
    bonus_points = parseFloat($("#receipt_transaction_bonus_earned").val()) || 0
    base_points = parseFloat(subtotal*multiplier)
  else
    bonus_points = 0
    base_points = parseFloat(multiplier)
  total_points_earned =  parseFloat((base_points+bonus_points))
  if(rounding_opts == "ROUND")
    total_points_rounding = Math.round(total_points_earned)
  else if(rounding_opts == "ROUND UP")
    total_points_rounding = Math.ceil(total_points_earned)
  else if(rounding_opts == "ROUND DOWN")
    total_points_rounding = Math.floor(total_points_earned)
  $("#tmp_total_points_earned").val(total_points_rounding);
  $("#receipt_transaction_base_points_earned").val(base_points)
  $("#receipt_transaction_total_points_earned").val(total_points_earned)