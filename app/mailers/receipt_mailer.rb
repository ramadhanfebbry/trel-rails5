class ReceiptMailer < ActionMailer::Base
  default from: Setting.email.default_from

  def receipt_validation_failed(to, reason, receipt = nil, message_body = nil)
    @receipt = receipt
    @reason = reason
    @message_body_sqs = message_body
    mail(:to => to,
      :subject => "#{ENV["NEW_RELIC_APP_NAME"]} - Receipt ##{receipt.id if receipt} Transaction process failed",
      :importance => "High", 'X-Priority'=>'1' )
  end

  def no_transaction_receipt(to, receipt)
    @receipt = receipt
    mail(:to => to,
      :subject => "#{ENV["NEW_RELIC_APP_NAME"]} - Receipt Transaction process failed",
      :importance => "High", 'X-Priority'=>'1' )
  end

  def receipt_subtotal_maxed(to, receipt)
    @receipt = receipt
    mail(:to => to,
         :subject => "Receipt ##{receipt.id if receipt} Transaction process failed",
         :importance => "High", 'X-Priority'=>'1' )
  end

end
