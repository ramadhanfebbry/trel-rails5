class OwnerMailer < ActionMailer::Base
  default from:  Setting.email.default_from

  def new_owner_confirmation(owner)
    @owner = owner
    restaurant = @owner.restaurants.first rescue nil
    @chain = restaurant.chain rescue nil
    @access_page = ENV["DEFAULT_HOST_EMAIL"].include?("prelevant")? "http://db.relevantmobile.com" : "http://"+ENV["DEFAULT_HOST_EMAIL"]  rescue nil
    html_template = "new_owner_confirmation"

    @email_subject = REDIS.get "owner_confirmation_email_subject"
    @email_content = REDIS.get "owner_confirmation_email_content"


    if @email_subject or @email_content
      ch = @owner.chain.name rescue nil
      html_template = "confirmation_with_redis"
      @html_body = (@email_content %{
          :owner_email => @owner.email,
          :password => @owner.password_generated,
          :restaurants => generate_html(@owner.restaurants),
          :chain_name =>  ch
      }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")
    end


    from = @chain.email.blank? ? "\"#{@chain.name}\" <#{Setting.email.default_from}>" : @chain.email rescue Setting.email.default_from
    subject = owner.role_id.eql?(1) ? "Welcome to #{@chain.name}" : "Welcome to #{@chain.name}" rescue "Welcome"
    mail(:to => @owner.email, :subject => subject, :from => from ) do |format|
      format.html { render html_template }
    end
  end

  def confirmation_email(owner, pwd_generated = nil)
    chain = owner.chain rescue nil
    locale = chain.locales.first || Locale.first
    locale_key = locale.key
    if owner.chain_owner?
      content_email = EmailTemplate.where(:locale_id => locale.id, :chain_id => chain.id, :template_name => "welcome_chain_owner").first
      subject = content_email.mail_subject
      @html_body = content_email.mail_content_html
      @text_body = content_email.mail_content_text
      changes_keys = {
          :chain_owner_email => owner.email,
          :chain_owner_pwd => owner.password_generated || pwd_generated,
          :chain_name => (chain.name rescue nil),
          :chain_locations => generate_html(owner.restaurants),
          :itunes_link => chain.itunes_link,
          :play_store_link => chain.play_store_link
      }
    else
      content_email = EmailTemplate.where(:locale_id => locale.id, :chain_id => chain.id, :template_name => "welcome_rest_owner").first
      subject = content_email.mail_subject
      @html_body = content_email.mail_content_html
      @text_body = content_email.mail_content_text
      changes_keys = {
          :rest_owner_email => owner.email,
          :rest_owner_pwd => owner.password_generated || pwd_generated,
          :chain_name => (chain.name rescue nil),
          :rest_owner_locations => generate_html(owner.restaurants),
          :itunes_link => chain.itunes_link,
          :play_store_link => chain.play_store_link
      }
    end

    @html_body = (@html_body % changes_keys)
    @text_body = (@text_body % changes_keys)

    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email rescue Setting.email.default_from
    subject = subject
    mail(:to => owner.email, :subject => subject, :from => Setting.email.default_from)

  end


  def send_to_support(chain,send_to,subject, description,question_from,email_login)
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email rescue Setting.email.default_from
    @chain_name = chain.name
    @description = description
    @question_from = question_from
    @title = subject
    @email_login   = email_login

    mail(:to => send_to, :subject => "#{subject}", :from => from, :bcc => "support@relevantmobile.com" )
  end

  def generate_html(restaurants)
    str = ""
    str += "Restaurants :<br />"
    str += "<ul>"
    restaurants.each do |restaurant|
      str +=  "<li>#{restaurant.dashboard_display_text}</li>"
    end
    str += "</ul>"

    return str
  end

  def send_survey_dashboard(chain, send_to, file_path,attachment_name, subject = nil)
    subject = subject unless subject.blank?
    subject = "Dashboard Survey" if subject.blank?
    attachments[attachment_name] = File.read(file_path)
    puts "#{attachment_name}"
    puts "#{send_to}"
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
    @subject = subject
    mail(:to => send_to, :from => from, :subject => subject,
         :bcc => Setting.report_email["cc"], :body => "Please See the attachment.."
         )
  end

  def send_survey_dashboard_encoding(chain, send_to, file_path,attachment_name, subject = nil)
    subject = subject unless subject.blank?
    email_to = send_to
    subject = "Dashboard Survey" if subject.blank?
    #attachments[attachment_name] = File.read(file_path)
    attachments[attachment_name] = { :data=> ActiveSupport::Base64.encode64(File.read(file_path)), :encoding => 'base64' }
    puts "#{attachment_name}"
    puts "#{send_to}"
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
    @subject = subject
    mail_setup = {:to => email_to, :subject => "#{subject}"}
    mail_setup[:from] = from unless from.blank?
    # mail(:to => send_to, :from => from, :subject => subject,
    #      :bcc => Setting.report_email["cc"]
    # )
    mail(mail_setup) do |format|
      format.html(content_transfer_encoding: "base64")
    end
  end

  def send_reward_redeemed(chain, send_to, file_path,attachment_name,subject_overide = nil)
    unless subject_overide.blank?
      sub =  subject_overide
    else
      sub = "Reward Redeemed Dashboard Export"
    end
    @title = sub#subject_overide.blank? ? "Reward Redeemed" : "Receipts Export"

    attachments[attachment_name] = File.read(file_path)
    puts "#{attachment_name}"
    puts "#{send_to}"
    begin
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
    rescue
      from = Setting.email.default_from
      end

    mail(:to => send_to, :from => from, :subject => sub, :body => "Please see the attachment below.",
         :bcc => Setting.report_email["cc"]
    )
  end

  def report_error_notification(failed_ids, job_id,err_text)
    ## for report subscriptions error
    @rs_name = "Report Subscription failed ids = #{failed_ids} <br /> Delayed Id #{job_id}"
    from = Setting.email.default_from
    @error_text = err_text
    send_to = Setting.email.default_from
    mail(:to => send_to, :from => from, :subject => "Report Subscription Error!!",
         :bcc => Setting.report_email["cc"]
    )
  end

  def db_job_error_notification(failed_ids, job_id,err_text)
    ## for report subscriptions error
    @rs_name =  "(#{ENV['NEW_RELIC_APP_NAME']}) Job From Owner Failed !!  <br /> Delayed Id #{job_id}"
    from = Setting.email.default_from
    @error_text = err_text
    send_to = Setting.email.default_from
    mail(:to => send_to, :from => from, :subject => "(#{ENV['NEW_RELIC_APP_NAME']}) Job From Owner Failed !!",
         :bcc => Setting.report_email["cc"]
    )
  end

  def individual_user_not_found(current_owner,wrong_email)
    chain = current_owner.chain
    @wrong_email = wrong_email
    from = Setting.email.default_from
    @suggested_email =  User.where("lower(email) like ? and chain_id = ?", '%'+wrong_email+'%', chain.id).map(&:email)

    mail(:to => current_owner.email, :from => from, :subject => "Dashboard Error - invalid customer email address",
         :bcc => Setting.report_email["cc"]
    )
  end

end
