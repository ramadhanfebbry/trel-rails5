class RewardMailer < ActionMailer::Base
  default from: Setting.email.default_from

  def reward_detail(reward, user_ids)
    @reward = reward
    @user_ids = user_ids
    chain_name = reward.chain.name rescue ""
    mail(:to => Setting.email.sys_admin, :subject => "[#{chain_name}] Reward Detail Info ##{@reward.id} - #{@reward.name}")
  end
  
  def reward_error(reward, exception)
    @reward = reward
    @exception = exception
    chain_name = reward.chain.name rescue ""
    mail(:to => Setting.email.sys_admin,
      :subject => "#{ENV["NEW_RELIC_APP_NAME"]} - [#{chain_name}] Reward Error Info ##{@reward.id} - #{@reward.name}",
      :importance => "High", 'X-Priority'=>'1' )
  end

  def send_pn_android_error(reward, job_error, exception)
    @reward = reward
    @job_error = job_error
    @exception = exception
    chain_name = reward.chain.name rescue ""
    mail(:to => Setting.email.sys_admin, 
      :subject => "#{ENV["NEW_RELIC_APP_NAME"]} - [#{chain_name}] Reward Error Send PN Android Error ##{@reward.id} - #{@reward.name}",
      :importance => "High", 'X-Priority'=>'1' )
  end

  def send_pn_iphone_error(reward, job_error, exception)
    @reward = reward
    @job_error = job_error
    @exception = exception
    chain_name = reward.chain.name rescue ""
    mail(:to => Setting.email.sys_admin, 
      :subject => "#{ENV["NEW_RELIC_APP_NAME"]} - [#{chain_name}] Reward Error Send PN IPhone Error ##{@reward.id} - #{@reward.name}",
      :importance => "High", 'X-Priority'=>'1' )
  end

  def send_pn_email_error(reward, job_error, exception)
    @reward = reward
    @job_error = job_error
    @exception = exception
    chain_name = reward.chain.name rescue ""
    mail(:to => Setting.email.sys_admin, 
      :subject => "#{ENV["NEW_RELIC_APP_NAME"]} - [#{chain_name}] Reward Error Send PN Email Error ##{@reward.id} - #{@reward.name}",
      :importance => "High", 'X-Priority'=>'1' )
  end


  def push_reward_notice_email(reward, user_email, email_subject, email_content)
    from = reward.chain.email || Setting.email.default_from
    @notification = email_content
    @notification.gsub!(/\r\n?/, "<br />")
    @notification.gsub!(/\n/, "<br />")
    mail(:to => user_email, :subject => "#{email_subject}", :from => from )
  end

  def test_push_reward_mail(chain, email_subject, email_content)
    from = chain.email || Setting.email.default_from
    @notification = email_content
    mail(:to => Setting.email.sys_admin, :subject => "#{email_subject}", :from => from )
  end

  def milestone_notification(chain, user_email,email_subject, email_content)
    @notification = email_content
    @notification.gsub!(/\r\n?/, "<br />")
    @notification.gsub!(/\n/, "<br />")
    from = chain.email || Setting.email.default_from
    mail(:to => user_email, :subject => "#{email_subject}", :from => from )
  end

  def push_reward_success_nofitication(user_selected,failed_users,chain,email_subject,file_path,admin_email)
    from = chain.email || Setting.email.default_from
    @user_selected = user_selected
    @failed_users = failed_users
    attachment_name = "User id from chain #{chain.name}.csv"
    attachments[attachment_name] = File.read(file_path)
    mail(:to => Setting.email.sys_admin, :subject => "#{email_subject}", :from => from , :cc => admin_email)
  end

  def reward_transaction_files(reward,user_email, file_path, chain,email_subject, email_content)
    from = chain.email || Setting.email.default_from
    attachments["#{reward.name}_claim_history.csv"] = File.read(file_path)
    @content = email_content
    mail(:to => user_email, :subject => "#{email_subject}", :from => from)
  end

  #RewardMailer.test_performance
  def test_performance(user_email, subject)
    from =  Setting.email.default_from
    mail(:to => user_email, :subject => "#{subject}", :from => from)
  end

  #RewardMailer.admin_push_notification('test','body', job, start_or_end)
  def admin_push_notification(subject, body, job, start_or_end)
    begin
    user_emails = Setting.admin_email_push_notif["cc"] || "inoe.bainur@gmail.com"
    rescue
      user_emails = "inoe.bainur@gmail.com"
    end
    @start_or_end =  start_or_end
    @job = job.id
    @job_type = job.class.to_s.humanize
    from =  Setting.email.default_from
    @body = body
    mail(:to => user_emails, :subject => "#{subject}", :from => from)
  end

end
