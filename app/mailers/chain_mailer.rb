class ChainMailer < ActionMailer::Base
  default from: "from@example.com"
  
  #  helper ActionView::Helpers::NumberHelper
  helper :application

  def welcome_email(chain)
    #   I18n.locale = chain.locale.value
    chain.owners.map(&:email).each do |email|
   		mail(:to => mail, :subject => "#{t(:welcome_subject)}")
    end
  end

  def report_owner(owner_email,s3_url,report, test_emails = [], rs = nil, report_result =nil)
    require 'RMagick'


    file_type = ReportSubscription::FILE_TYPES.key(rs.file_type.to_i)# rescue "pdf"

    chain = report.chain
    if chain.blank?
      from =  Setting.email.default_from
    else
      from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
    end
    start_date = nil
    report_end_date = nil

    start_date = report.start_date.strftime("%Y-%m-%d") if report.start_date
    report_end_date = report.end_date.strftime("%Y-%m-%d") if report.end_date
    #switch if start_date > than end_date
    tmp_date = nil
    if !start_date.blank? and !report_end_date.blank?
      tmp_date = start_date
      if start_date > report_end_date
        start_date = report_end_date
        report_end_date = tmp_date
      end
    end

    if report.chain
      subject = "#{report.kind} Report - #{report.chain.name}, #{start_date}"
      subject = "#{report.kind} Report - #{report.chain.name}, #{start_date} to #{report_end_date}" if report.kind != "daily"
    else
      subject = "#{report.kind} Report, #{start_date}"
      subject = "#{report.kind} Report, #{start_date} to #{report_end_date}" if report.kind != "daily"
    end

    # if user set the report definitino email name
    if rs and rs.report_definition and rs.report_definition.email_subject
    subject = (rs.report_definition.email_subject % {
        :chain => (chain.name rescue ""),
        :start_date => start_date,
        :end_date => report_end_date
        }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")

    begin
    subject = filename_for_papa_murphy(rs,start_date) if rs.report_definition.jasper_path.include?("papa_murphy")
    rescue
    end
    end

    subject = subject.gsub(' ','_')
    @attachment_name = "#{subject}.#{file_type}"
    unless rs.subscription_format.blank?
      subject = rs.subscription_format.filename({"start_date" => start_date, "end_date" =>  report_end_date})[0] rescue subject
      @attachment_name = "#{rs.subscription_format.filename({"start_date" => start_date, "end_date" =>  report_end_date})[1]}" + ".#{file_type}" rescue @attachment_name
    end
    pdf_url = s3_url

    @@content = search_file(report_result,pdf_url)

    if @@content == nil
      puts "no file 1"
      puts "no file -- retry"
      r_file = report_result.write_to_file
      File.open("#{Rails.root}/tmp/test_send_pdf.#{file_type}","w+"){|f| f << File.open(r_file.path).read.force_encoding("utf-8")}
    else
      File.open("#{Rails.root}/tmp/test_send_pdf.#{file_type}","w+"){|f| f << @@content.force_encoding("utf-8")}
    end



    ## convert to images
    if file_type == "pdf"
      pdf_file_name = "#{Rails.root}/tmp/test_send_pdf.#{file_type}"
      im = Magick::Image.read(pdf_file_name)
      im[0].write("#{Rails.root}/tmp/image.jpg")
    end
    ## making it as an attachment inline
    attachments[@attachment_name] = File.read("#{Rails.root}/tmp/test_send_pdf.#{file_type}")
    attachments.inline['image.jpg'] = File.read("#{Rails.root}/tmp/image.jpg") if file_type == "pdf"
    @file_type = file_type
    @link_html = pdf_url
    mail_to_address = owner_email
    #mail_to_address = "inoe.bainur@gmail.com" if test == true
    #email_subject = [Relevant Report] - Zoes Kitchen Reward_Redeem_043013_050713
    if test_emails.blank?
      mail(:to => mail_to_address, :from => from, :subject => "[Relevant Report] - #{subject}",
         :bcc => Setting.report_email["cc"], :body => "Please see the attachment below."
      )
    else
      mail(:to => test_emails, :from => from, :subject => "[Relevant Report] - #{subject}",
           :bcc => test_emails, :body => "Please see the attachment below.")
    end
  end

  def search_file(report_result,pdf_url)
    if File.exist?("#{Rails.root}/tmp/#{report_result.custom_filename}")
      content = File.open("#{Rails.root}/tmp/#{report_result.custom_filename}").read
    else
      content = RestClient.get(pdf_url) rescue nil
    end
    content
  end

  def report_owner_base64(owner_email,s3_url,report, test_emails = [], rs = nil, report_result =nil)
    require 'RMagick'

    puts "report_owner_base64 * 100"
    file_type = ReportSubscription::FILE_TYPES.key(rs.file_type.to_i)# rescue "pdf"

    chain = report.chain
    if chain.blank?
      from =  Setting.email.default_from
    else
      from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
    end
    start_date = nil
    report_end_date = nil

    start_date = report.start_date.strftime("%Y-%m-%d") if report.start_date
    report_end_date = report.end_date.strftime("%Y-%m-%d") if report.end_date
    #switch if start_date > than end_date
    tmp_date = nil
    if !start_date.blank? and !report_end_date.blank?
      tmp_date = start_date
      if start_date > report_end_date
        start_date = report_end_date
        report_end_date = tmp_date
      end
    end

    if report.chain
      subject = "#{report.kind} Report - #{report.chain.name}, #{start_date}"
      subject = "#{report.kind} Report - #{report.chain.name}, #{start_date} to #{report_end_date}" if report.kind != "daily"
    else
      subject = "#{report.kind} Report, #{start_date}"
      subject = "#{report.kind} Report, #{start_date} to #{report_end_date}" if report.kind != "daily"
    end

    # if user set the report definitino email name
    if rs and rs.report_definition and rs.report_definition.email_subject
      subject = (rs.report_definition.email_subject % {
          :chain => (chain.name rescue ""),
          :start_date => start_date,
          :end_date => report_end_date
      }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")

      subject = filename_for_papa_murphy(rs,start_date) if rs.report_definition.jasper_path.include?("papa_murphy")
    end

    subject = subject.gsub(' ','_')
    @attachment_name = "#{subject}.#{file_type}"

    unless rs.subscription_format.blank?
      subject = rs.subscription_format.filename({"start_date" => start_date, "end_date" =>  report_end_date})[0] rescue subject
      @attachment_name = "#{rs.subscription_format.filename({"start_date" => start_date, "end_date" =>  report_end_date})[1]}" + ".#{file_type}" rescue @attachment_name
    end

    pdf_url = s3_url

    #@@content = RestClient.get(pdf_url) rescue nil
    @@content = search_file(report_result,pdf_url)

    if @@content == nil
      puts "no file -- retry"
      r_file = report_result.write_to_file
      File.open("#{Rails.root}/tmp/test_send_pdf.#{file_type}","w+"){|f| f << File.open(r_file.path).read.force_encoding("utf-8")}
    else
      File.open("#{Rails.root}/tmp/test_send_pdf.#{file_type}","w+"){|f| f << @@content.force_encoding("utf-8")}
    end


    ## convert to images
    if file_type == "pdf"
      pdf_file_name = "#{Rails.root}/tmp/test_send_pdf.#{file_type}"
      im = Magick::Image.read(pdf_file_name)
      im[0].write("#{Rails.root}/tmp/image.jpg")
    end
    ## making it as an attachment inline
    attachments[@attachment_name] = { :data=> ActiveSupport::Base64.encode64(File.read("#{Rails.root}/tmp/test_send_pdf.#{file_type}")), :encoding => 'base64' }
    attachments.inline['image.jpg'] = File.read("#{Rails.root}/tmp/image.jpg") if file_type == "pdf"
    @file_type = file_type
    @link_html = pdf_url
    mail_to_address = owner_email
    #mail_to_address = "inoe.bainur@gmail.com" if test == true
    #email_subject = [Relevant Report] - Zoes Kitchen Reward_Redeem_043013_050713

    if test_emails.blank?
      puts "test email blanks" * 10
      mail_setup = {:to => mail_to_address, :from => from, :subject => "[Relevant Report] - #{subject}",
                          :bcc => Setting.report_email["cc"]}
      mail_setup[:from] = from unless from.blank?

      mail(mail_setup) do |format|
        format.html(content_transfer_encoding: "base64")
      end
      # mail(:to => mail_to_address, :from => from, :subject => "[Relevant Report] - #{subject}",
      #      :bcc => Setting.report_email["cc"], :body => "Please see the attachment below."
      # ) do |format|
      #   format.html(content_transfer_encoding: "base64")
      # end
    else
      puts "test email exists" * 10
      mail_setup = {:to => test_emails, :from => from, :subject => "[Relevant Report] - #{subject}",
                         :bcc => test_emails}
      mail_setup[:from] = from unless from.blank?

      mail(mail_setup) do |format|
        format.html(content_transfer_encoding: "base64")
      end
      # mail(:to => test_emails, :from => from, :subject => "[Relevant Report] - #{subject}",
      #      :bcc => test_emails, :body => "Please see the attachment below.") do |format|
      #   format.html(content_transfer_encoding: "base64")
      # end
    end
  end

  def filename_for_papa_murphy(rs,start_date)
    chain = Chain.find(rs.chain_id) rescue nil
    subject = (rs.report_definition.email_subject % {
        :chain => (chain.name rescue ""),
        :start_date => start_date.gsub('-','')
    }).gsub(/\r\n?/, "<br />").gsub(/\n/, "<br />")
    return subject
  end

  def mailchimp_sync_log_error_report(chain, error_text, mail_partner = "Mailchimp")
    @error_text = error_text
    @chain = chain
    mail(:to => Setting.email.sys_admin,
         :subject => "#{mail_partner} TRELEVANT Sync Log Error Report #{@chain.name}",
         :importance => "High", 'X-Priority'=>'1' )
  end

  def emma_sync_log_error_report(chain, error_text, mail_partner = "EMMA")
    @error_text = error_text
    @chain = chain
    mail(:to => Setting.email.sys_admin,
         :subject => "#{mail_partner} TRELEVANT Sync Log Error Report #{@chain.name}",
         :importance => "High", 'X-Priority'=>'1' )
  end

  def push_service_notification_log_error_report(chain, error_text, mail_partner = "Push Service Notification")
    @error_text = error_text
    @chain = chain
    mail(:to => Setting.email.sys_admin,
         :subject => "#{mail_partner} TRELEVANT Push Service Notification Log Error Report #{@chain.name}",
         :importance => "High", 'X-Priority'=>'1' )
  end

  #send email result automatic test
  def report_automatic_test(to, content)
    @automatic_test = content
    attachments['automatic-test-excel-file.xls'] = File.read("#{Rails.root}/tmp/automatic-test-excel-file.xls")
    mail(:to => to,
         :from => "support@relevantmobile.com",
         :subject => "[Automatic Test] Report Date #{Time.zone.now}",
         :importance => "High", 'X-Priority' => '1'
         )
  end

  def no_scan_alert_mailer(to,from,subject, content,  file_path,attachment_name)
    @content = content
    attachments[attachment_name] = File.read(file_path)
    mail(:to => to, :subject => subject, :from => from )
  end

  def no_scan_alert_multi_mailer(to,from,subject, content,  file_paths,attachment_name)
    @content = content
    unless file_paths.blank?
      file_paths.each do |fl|
        attachments[fl.split('/').last] = File.read(fl)
      end
    end

    mail(:to => to, :subject => subject, :from => from )
  end

  def giftcard_error_report(chain, error_text, mail_partner = "GIFTING CARD")
    @error_text = error_text
    @chain = chain
    mail(:to => Setting.email.sys_admin,
         :subject => "#{mail_partner} GIFTING CARD Log Error Report #{@chain.name}",
         :importance => "High", 'X-Priority'=>'1' )
  end

  def less_available_card_report(chain, error_text, mail_partner = "VANTIV")
    @error_text = error_text
    @chain = chain
    mail(:to => Setting.email.sys_admin,
         :subject => "#{mail_partner} : Available CARD Report #{@chain.name}",
         :importance => "High", 'X-Priority'=>'1' )
  end

  def queue_notification(env_process,queue,count,emails)
    # send email for queue
    @env_process = env_process
    @queue = queue
    @count = count

    emails.each do |email|
      mail(:to => email,
           :subject => "QUEUE NOTIFICATION #{env_process} -- queue_name : #{queue}",
           :importance => "High", 'X-Priority'=>'1' )
    end
  end

  def location_json_report(chain, pos_location, receiver)
    @pos_locations = pos_location
    @chain = chain
    mail(:to => receiver,
         :from => "support@relevantmobile.com",
         :subject => "Location JSON Report #{@chain.name}",
         :importance => "High", 'X-Priority'=>'1' )
  end

  def error_report_for_tester(chain, error_text, mail_partner = "REPORT")
    @error_text = error_text
    @chain = chain
    mail(:to => "tester@pushjaw.com",
         :from => "support@relevantmobile.com",
         :subject => "#{mail_partner} Report #{@chain.name}",
         :importance => "High", 'X-Priority'=>'1' )
  end

end
