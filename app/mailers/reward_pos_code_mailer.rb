class RewardPosCodeMailer < ActionMailer::Base
  default from: Setting.email.default_from

  def send_notification_lt_two_thousand(reward)
    @reward = reward
    @chain_name = reward.chain.name rescue ""
    mail(:to => Setting.email.sys_admin,
         :subject => "Reward #{@reward.name} POS Codes Notification",
         :cc => "sr@dailygobble.com",
         :importance => "High", 'X-Priority'=>'1' )
  end

  def send_notification_when_tag_updated(chains)
    @chains = chains
    mail(:to => Setting.email.sys_admin,
         :subject => "Chain Users Tag Updated Notification",
         :cc => "sr@dailygobble.com",
         :importance => "High")
  end
end
