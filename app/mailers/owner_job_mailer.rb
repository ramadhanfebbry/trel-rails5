class OwnerJobMailer < ActionMailer::Base
  #default from:  Setting.email.default_from

  def owner_job_confirmation(email, owner_id, chain, job)
    @owner = Owner.find owner_id
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
    mail(:to => email,
         :from => from,
         :subject => "[#{chain.name}] Owner Job ##{job.description}")
  end

  def push_chain_plain_notification(subject, user_emails, body_cont, chain)
    @chain = chain
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
    mail_setup = {:subject => "#{subject}",
                  :to => user_emails.join(','), :from => from}
    @text_body = body_cont
    @text_body.gsub!(/\r\n?/, "<br />")
    @text_body.gsub!(/\n/, "<br />")
    mail(mail_setup)
  end
end