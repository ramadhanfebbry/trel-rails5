class GameMailer < ActionMailer::Base

  default from: Setting.email.default_from

  def point_notification(chain, email_to, email_subject, email_content, email_content_html = nil)
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
    mail_setup = {:to => email_to, :subject => "#{email_subject}", :from => from}
    @html_body = email_content_html || email_content
    @text_body = email_content
    mail(mail_setup)
  end

end
