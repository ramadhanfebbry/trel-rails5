class PromotionMailer < ActionMailer::Base

  default from: Setting.email.default_from

  def promo_code_detail(promotion,current_admin)
    @promotion = promotion
    chain_name = @promotion.chain.name rescue ""
    mail(
         :to => current_admin.email,:cc => Setting.email.sys_admin,
         :subject => "[#{chain_name}] Promotion Detail ##{@promotion.id} - #{@promotion.name}"
    )
  end

  def failed_generate_code(promotion, exception)
    @promotion = promotion
    @exception = exception
    chain_name = @promotion.chain.name rescue ""
    mail(:to => Setting.email.sys_admin, 
      :subject => "[#{chain_name}] Generate Code Failed ##{@promotion.id} - #{@promotion.name}",
      :importance => "High", 'X-Priority'=>'1' )
  end

end
