class UserMailer < ActionMailer::Base
  default from: Setting.email.default_from

  def welcome_email(user)
    from = user.chain.email.blank? ? "\"#{user.chain.name}\" <#{Setting.email.default_from}>" : user.chain.email
    subject, body= fetch_email_template user.chain.id, user.locale.key, "welcome"
    mail_setup = {:to => user.email, :subject => "#{subject}", :from => from}
    @text_body = body % { :relevant => "Relevant",:itunes_link => user.chain.itunes_link,
                          :play_store_link => user.chain.play_store_link}
    @text_body.gsub!(/\r\n?/, "<br />")
    @text_body.gsub!(/\n/, "<br />")
    @text_body = @text_body.force_encoding("ISO-8859-1")
    mail(mail_setup)
#    send_email(user.email, subject, body % { :relevant => "Relevant"}, true, from)
  end

  def forgot_password_email(user, password)
    from = user.chain.email.blank? ? "\"#{user.chain.name}\" <#{Setting.email.default_from}>" : user.chain.email
    subject, body= fetch_email_template user.chain.id, user.locale.key, "forgot_password"
    #    send_email(user.email, subject, body % { :temp_pwd => password}, false, from)
    mail_setup = {:to => user.email, :subject => "#{subject}", :from => from}
    @text_body = body % { :temp_pwd => password,:itunes_link => user.chain.itunes_link,
                          :play_store_link => user.chain.play_store_link}
    @text_body.gsub!(/\r\n?/, "<br />")
    @text_body.gsub!(/\n/, "<br />")
    mail(mail_setup) 
  end

  def reject_receipt_email(user, receipt, reject_reason)
    subject, body= fetch_email_template receipt.chain.id, user.locale.key, "receipt_reject"
    #    send_email(user.email, subject % {:receipt_id => receipt.id} , body % { :reject_reason => reject_reason}, false)
    mail_setup = {:to => user.email, :subject => "#{subject}" % {:receipt_id => receipt.id}}
    @text_body = email_template.mail_content_text % { :reject_reason => reject_reason, :receipt_id_link => "<a href='#{receipt.image.url}' target='_blank'>#{receipt.id}</a>",:itunes_link => user.chain.itunes_link,
                                                      :play_store_link => user.chain.play_store_link}
    @text_body.gsub!(/\r\n?/, "<br />")
    @text_body.gsub!(/\n/, "<br />")
    mail(mail_setup) 
  end

  def email_parse_rejected(chain, user)
  begin
    from = chain.email.blank? ? "\"#{chain.name}\" <#{Setting.email.default_from}>" : chain.email
  rescue
  from = Setting.email.default_from
  end
    @subject = REDIS.get "subject_email_parse_user_reject_#{chain.id}" rescue "Order Online Rejected"
    @content = REDIS.get "content_email_parse_user_reject_#{chain.id}" rescue "#{user} doesnt find in our system"
    @subject = @subject.blank? ? "Order Online Rejected" : @subject
    @content = @content.blank? ? " %{user_email} doesnt find in %{chain}" : @content

    #begin
    @content = @content %{:user_email => user, :chain => chain.blank? ? '-' : chain.name}
    #rescue
    #@text_body = @content
    #end

    @content.gsub!(/\r\n?/, "<br />")
    @content.gsub!(/\n/, "<br />")
    mail_setup = {:to => user, :subject => "#{@subject}", :from => from}
    mail(mail_setup)
  end

  def push_reward_email(user, body_cont)
    #subject, body = fetch_push_reward_template(user.chain.id, user.locale.key, "push_reward_notification")
    from = user.chain.email.blank? ? "\"#{user.chain.name}\" <#{Setting.email.default_from}>" : user.chain.email
    subject = "Push Reward"
    mail_setup = {:to => user.email, :subject => "#{subject}", :from => from}
    @text_body = body_cont
    @text_body.gsub!(/\r\n?/, "<br />")
    @text_body.gsub!(/\n/, "<br />")
    mail(mail_setup) 
    #    send_email(user.email, subject, body, false, from)
  end
  
  def push_reward_notice_email(reward, user_email, body_cont)
    from = reward.chain.email.blank? ? "\"#{reward.chain.name}\" <#{Setting.email.default_from}>" : reward.chain.email
    subject = "[#{reward.chain.name}] Push Reward ##{reward.id} - #{reward.name}"
    mail_setup = {:to => user_email, :subject => "#{subject}", :from => from}
    @text_body = body_cont
    @text_body.gsub!(/\r\n?/, "<br />")
    @text_body.gsub!(/\n/, "<br />")
    mail(mail_setup) 
    #    mail(:to => user_email, :subject => subject, :from => from )
  end

  def push_reward_email_group(reward, user_emails, body_cont)
    #subject, body = fetch_push_reward_template(user.chain.id, user.locale.key, "push_reward_notification")
    subject = "Push Reward - ##{reward.id}"
    mail_setup = {:to => user_emails, :subject => "#{subject}"}
    @text_body = body_cont
    @text_body.gsub!(/\r\n?/, "<br />")
    @text_body.gsub!(/\n/, "<br />")
    mail(mail_setup) 
    #    send_email(user_emails, subject, body, false)
  end

  def fetch_email_template(chain_id, user_locale, email_name)
    subject= REDIS.hget "chain_#{chain_id}", "#{user_locale}_#{email_name}_email_subject"
    body= REDIS.hget "chain_#{chain_id}", "#{user_locale}_#{email_name}_email_body"

    return subject, body
  end

  def fetch_push_reward_template(chain_id, user_locale, email_name)
    subject = "Reward Push"
    body = REDIS.hget "chain_#{chain_id}", "#{user_locale}_#{email_name}_body"
    return subject, body
  end

  def send_email(email_to, subject, body, html_format, from = nil)
    mail_setup = {:to => email_to, :subject => "#{subject}"}
    mail_setup[:from] = from unless from.blank?
    @body = body
    mail(mail_setup) do |format|
      format.html {}
      format.text {}
    end
  end

  def receipt_email(email_to, receipt)
    subject = "New receipt ##{receipt.id} submitted - Rails App"
    @receipt = receipt
    mail(:to => email_to, :subject => "#{subject}")
  end

  def push_chain_plain_notification(subject,user_email, body_cont)
    #subject, body = fetch_push_reward_template(user.chain.id, user.locale.key, "push_reward_notification")
    #subject = "Email From Admin"
    mail_setup = {:subject => subject, :to => user_email}
    @text_body = body_cont
    @text_body.gsub!(/\r\n?/, "<br />")
    @text_body.gsub!(/\n/, "<br />")
    mail(mail_setup)
  end

  def push_chain_plain_error(emails, error_text)
    @emails = emails
    @exception = exception
    chain_name = reward.chain.name rescue ""
    mail(:to => Setting.email.sys_admin,
      :subject => "#{ENV["NEW_RELIC_APP_NAME"]} - [Admin Push Plain Notification Error]",
      :importance => "High", 'X-Priority'=>'1' )
  end

  def push_point_notice_email(chain, email_to, email_subject, email_content, email_content_html = nil)
    from = chain.email || Setting.email.default_from
    @html_content = email_content_html || email_content
    @notification = email_content
    mail(:to => email_to, :subject => "#{email_subject}", :from => from )
  end

  ## mailer to send to a referer that has sent referral code to users
  def mail_to_referer(user)
    from = user.chain.email.blank? ? "\"#{user.chain.name}\" <#{Setting.email.default_from}>" : user.chain.email
    subject, body = fetch_email_template user.chain.id, user.locale.key, "referral_referer"
    @body = body % {:itunes_link => user.chain.itunes_link,
                    :play_store_link => user.chain.play_store_link}
    @body = body
    mail_setup = {:to => user.email, :subject => "#{subject}", :from => from}
    mail(mail_setup)
  end

  def reset_password_email(user, password)
    from = user.chain.email || Setting.email.default_from
    reset_email_notification =  ResetEmailNotification.get_email_template(user)
    email_subject = reset_email_notification[:subject] % { :chain_name => user.chain.name, :password => password}
    @notification =  reset_email_notification[:content] % { :chain_name => user.chain.name, :password => password}
    @notification.gsub!(/\r\n?/, "<br />")
    @notification.gsub!(/\n/, "<br />")
    mail(:to => user.email, :subject => email_subject, :from => from )
  end

  def send_push_confirmation(push_record)
    @push_record = push_record
    @type = push_record.class.to_s
    subject = @type.eql?("PushPoint") ? "Push Point has been sent" : "Push Reward has been sent"
    mail(:to => Setting.email.sys_admin,
         :subject => subject,
         :importance => "High", 'X-Priority'=>'1' )
  end

  def send_push_confirmation(push_record)
    @push_record = push_record
    @type = push_record.class.to_s
    subject = @type.eql?("PushPoint") ? "Push Point has been sent" : "Push Reward has been sent"
    mail(:to => Setting.email.sys_admin,
         :subject => subject,
         :importance => "High", 'X-Priority'=>'1' )
  end

  def giftcard_notification_email(chain, giftcard_notification, giftee_email, giftee_name, custom_message, gifter_name, giftcard_number, amount, image_header_url, barcode_image )
    begin
      from = chain.email || Setting.email.default_from
      @text_body = giftcard_notification.content_email % {
          :giftee_name => giftee_name,
          :custom_message => custom_message,
          :gifter_name => gifter_name,
          :giftcard_number => giftcard_number,
          :amount => amount,
          :image_header_url => image_header_url,
          :barcode_image => barcode_image
      }
      @text_body.gsub!(/\r\n?/, "")
      @text_body.gsub!(/\n/, "")
      mail(:to => giftee_email,
           :from => from,
           :subject => giftcard_notification.subject_email,
           :importance => "High", 'X-Priority'=>'1')
    rescue => e
      p "ERORRR on send giftcard_notification_email"
      p e.message
    end

  end

  def registration_confirmation(user)
    p "registration confirmation mailer"
    puts "#{user}"
    from = user.chain.email.blank? ? "\"#{user.chain.name}\" <#{Setting.email.default_from}>" : user.chain.email
    subject, body = fetch_email_template user.chain.id, user.locale.key, "signup_activation_email"
    @user = user.reload
    @body = body % {:username => user.username,
                    :activation_link => confirm_email_url(user.user_confirmation.reload.confirm_token)}
    @body = body
    mail_setup = {:to => user.email, :subject => "#{subject}", :from => from}
    puts "registration confirmation mailer completed"
    mail(mail_setup)
  end

end
