# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
# #
# # Examples:
# #
# #   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
# #   Mayor.create(name: 'Emanuel', city: cities.first)

def seed_dummy_data()
  puts "Seeding dummy data for #{Rails.env}"

  #create super admin
  super_admin = Admin.new
  super_admin.email = 'xd@dailygobble.com'
  super_admin.password = 'P@ssw0rd1'
  super_admin.password_confirmation = 'P@ssw0rd1'
  super_admin.role = ADMIN_ROLES[:ADMIN]
  super_admin.save

  #create admin tester
  admin_tester = Admin.new
  admin_tester.email = 'tester@dg.com'
  admin_tester.password = 'tester'
  admin_tester.password_confirmation = 'tester'
  admin_tester.role = ADMIN_ROLES[:TESTER]
  admin_tester.save

  Application.delete_all
  Application.create([{:chain => Chain.find_by_name("Vive la Crepe"),:name => "VLC iphone app"},
                      {:chain => Chain.find_by_name("McDonalds"),:name => "MCD android app"},
                      {:chain => Chain.find_by_name("Subway"),:name => "SBW  iphone app"}])

  ApplicationKey.delete_all
  #ApplicationKey.create([{:application}])

  Chain.delete_all
  Chain.create([{:name => "Naked Pizza", :points => 2},{:name => "Vive la Crepe",:points => 1},{:name => "Subway",:points => 1}, {:name => "McDonalds",:points => 1}])

  Owner.delete_all
  Owner.create([
      {:first_name => "Jeremy", :email => "jr@test.dailygobble.com", :last_name => "Lin", :city_id => City.find_by_name("New York"), :encrypted_password => "", :address => "6 E 39th Street Suite 701", :zipcode => "10016", :work_contact_number => "1234567890", :cell_contact_number => "1234567890" },
      {:first_name => "Tim", :email => "tt@test.dailygobble.com", :last_name => "Tebow", :city_id => City.find_by_name("New York"), :encrypted_password => "", :address => "6 E 39th Street Suite 701", :zipcode => "10016", :work_contact_number => "1234567890", :cell_contact_number => "1234567890"},
      {:first_name => "Eli", :email => "em@test.dailygobble.com", :last_name => "Manning",:city_id => City.find_by_name("New York"),  :encrypted_password => "", :address => "6 E 39th Street Suite 701", :zipcode => "10016",:work_contact_number => "(234)567-8900", :cell_contact_number => "(234)567-8900"},
      {:first_name => "Kimi", :email => "kr@test.dailygobble.com", :last_name => "Raikkonen",:city_id => City.find_by_name("New York"), :encrypted_password => "", :address => "6 E 39th Street Suite 701", :zipcode => "10016",:work_contact_number => "(234)567-8900", :cell_contact_number => "(234)567-8900"}])

  ChainsOwner.delete_all
  ChainsOwner.create([{:owner => Owner.find_by_first_name('Jeremy'), :chain => Chain.find_by_name('Naked Pizza')},{:owner => Owner.find_by_first_name(['Eli']),:chain => Chain.find_by_name("Vive la Crepe")},{:owner => Owner.find_by_first_name('Kimi'),:chain => Chain.find_by_name("Subway")},{:owner => Owner.find_by_first_name('Tim'),:chain => Chain.find_by_name("McDonalds")}])

  #TODO get confirmation from Seema to know about the Restaurant owner name and then run the Seed
  Restaurant.delete_all
  Restaurant.create([
      {:city => City.find_by_name("New York"),
        :chain => Chain.find_by_name("Vive la Crepe"),
        :name => "UPPER WEST - 189 Columbus",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Vive la Crepe")).first.owner,
        :address => "189 Columbus Avenue", :zipcode => "10014",
        :phone_number => "(121)222-2221",
        :dashboard_display_text => "UPPER WEST - 189 Columbus",
        :app_display_text => "UPPER WEST - 189 Columbus"},
      {:city => City.find_by_name("New York"),
        :chain => Chain.find_by_name("Vive la Crepe"),
        :name => "UNION SQUARE - 114 Univ Pl",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Vive la Crepe")).first.owner,
        :address => "114 University Pl",
        :zipcode => "10003",
        :phone_number => "(121)222-2221",
        :dashboard_display_text => "UNION SQUARE - 114 Univ Pl",
        :app_display_text => "UNION SQUARE - 114 Univ Pl"},
      {:city => City.find_by_name("New York"),
        :chain => Chain.find_by_name("Vive la Crepe"),
        :name => "BLEECKER - 300 Bleecker",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Vive la Crepe")).first.owner,
        :address => "300 Bleecker St",
        :zipcode => "10014",
        :phone_number => "(121)222-2221",
        :dashboard_display_text => "BLEECKER - 300 Bleecker",
        :app_display_text => "BLEECKER - 300 Bleecker"},
      {:city => City.find_by_name("New York"),
        :chain => Chain.find_by_name("Vive la Crepe"),
        :name => "SPRING STREET - 51 Spring St",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Vive la Crepe")).first.owner,
        :address => "SPRING STREET - 51 Spring St", :zipcode => "10012", :phone_number => "(121)222-2221",:dashboard_display_text => "SPRING STREET - 51 Spring St", :app_display_text => "SPRING STREET - 51 Spring St"},
      {:city => City.find_by_name("Miami"),
        :chain => Chain.find_by_name("Naked Pizza"),
        :name => "South Beach FL - Washington Ave",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Naked Pizza")).first.owner,
        :address => "1260 Washington Ave",
        :zipcode => "33139",
        :phone_number => "(565)568-3455",
        :dashboard_display_text => "South Beach FL - Washington Ave",
        :app_display_text => "South Beach FL - Washington Ave"},
      {:city => City.find_by_name("Phoenix"),
        :chain => Chain.find_by_name("Naked Pizza"),
        :name => "Happy Valley AZ - 67th Ave",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Naked Pizza")).first.owner,
        :address => "25155 N 67th Ave",
        :zipcode => "10014",
        :phone_number => "(121)222-2221",
        :dashboard_display_text => "Happy Valley AZ - 67th Ave",
        :app_display_text => "Happy Valley AZ - 67th Ave"},
      {:city => City.find_by_name("Leominster"),
        :chain => Chain.find_by_name("Subway"),
        :name => "Leominster - 1290 Main St",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Subway")).first.owner,
        :address => "1290 Main St",
        :zipcode => "14530",
        :phone_number => "(676)222-9809",
        :dashboard_display_text => "Leominster - 1290 Main St",
        :app_display_text => "Leominster - 1290 Main St"},
      {:city => City.find_by_name("Bedford"),
        :chain => Chain.find_by_name("Subway"),
        :name => "Bedford - Route 101",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Subway")).first.owner,
        :address => "393 Route 101 West",
        :zipcode => "14530",
        :phone_number => "(676)222-9809",
        :dashboard_display_text => "Bedford - Route 101 ",
        :app_display_text => "Bedford - Route 101 "},
      {:city => City.find_by_name("Salem"),
        :chain => Chain.find_by_name("Subway"),
        :name => "Salem - Broadway",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Subway")).first.owner,
        :address => "41 S Broadway",
        :zipcode => "14530",
        :phone_number => "(676)222-9809",
        :dashboard_display_text => "Salem - Broadway",
        :app_display_text => "Salem - Broadway"},
      {:city => City.find_by_name("West Lebanon"),
        :chain => Chain.find_by_name("Subway"),
        :name => " W Lebanon - Plainville Rd",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Subway")).first.owner,
        :address => "263 Plainville Rd Rte 12A",
        :zipcode => "14530",
        :phone_number => "(676)222-9809",
        :dashboard_display_text => "W Lebanon - Plainville Rd",
        :app_display_text => "W Lebanon - Plainville Rd"},
      {:city => City.find_by_name("Brewer"),
        :chain => Chain.find_by_name("Subway"),
        :name => "Brewer - 710 Wilson St",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("Subway")).first.owner,
        :address => "710 Wilson St",
        :zipcode => "4412",
        :phone_number => "(676)222-9809",
        :dashboard_display_text => "Brewer - 710 Wilson St",
        :app_display_text => "Brewer - 710 Wilson St"},
      {:city => City.find_by_name("New York"),
        :chain => Chain.find_by_name("McDonalds"),
        :name => "McDonalds- Dine In NYC",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("McDonalds")).first.owner,
        :address => "330 W Broadway",
        :zipcode => "10013",
        :phone_number => "(432)678-5378",
        :dashboard_display_text => "McDonalds- Dine In NYC",
        :app_display_text => "McDonalds- Dine In NYC"},
      {:city => City.find_by_name("New York"),
        :chain => Chain.find_by_name("McDonalds"),
        :name => "McDonalds- West Village NYC",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("McDonalds")).first.owner,
        :address => "361 6th Ave",
        :zipcode => "10014",
        :phone_number =>"(432)678-5378",
        :dashboard_display_text => "McDonalds- West Village NYC",
        :app_display_text => "McDonalds- West Village NYC"},
      {:city => City.find_by_name("New York"),
        :chain => Chain.find_by_name("McDonalds"),
        :name => "McDonalds- UES NYC",
        :owner => ChainsOwner.joins(:chain).where(:chain_id => Chain.find_by_name("McDonalds")).first.owner,
        :address => "1567 Lexington Av",
        :zipcode => "10029",
        :phone_number => "(432)678-5378",
        :dashboard_display_text => "McDonalds- UES NYC",
        :app_display_text => "McDonalds- UES NYC"}])

end

def seed_common_data()
  puts "Seeding common data for #{Rails.env}"

  Country.delete_all
  Country.create([{:name=>"United States", :abbreviation=>"US", :ISO => "USA"},
                  {:name=>"Canada", :abbreviation=>"CA", :ISO => "CAD"}])

  Region.delete_all
  usa=Country.find_by_ISO("USA")
  Region.create([{:name => "Alabama", :abbreviation => "AL", :country => usa},
                 {:name => "Alaska", :abbreviation => "AK", :country => usa},
                 {:name => "Arizona", :abbreviation => "AZ", :country => usa},
                 {:name => "Arkansas", :abbreviation => "AR", :country => usa},
                 {:name => "California", :abbreviation => "CA", :country => usa},
                 {:name => "Colorado", :abbreviation => "CO", :country => usa},
                 {:name => "Connecticut", :abbreviation => "CT", :country => usa},
                 {:name => "Delaware", :abbreviation => "DE", :country => usa},
                 {:name => "Florida", :abbreviation => "FL", :country => usa},
                 {:name => "Georgia", :abbreviation => "GA", :country => usa},
                 {:name => "Hawaii", :abbreviation => "HI", :country => usa},
                 {:name => "Idaho", :abbreviation => "ID", :country => usa},
                 {:name => "Illinois", :abbreviation => "IL", :country => usa},
                 {:name => "Indiana", :abbreviation => "IN", :country => usa},
                 {:name => "Iowa", :abbreviation => "IA", :country => usa},
                 {:name => "Kansas", :abbreviation => "KS", :country => usa},
                 {:name => "Kentucky", :abbreviation => "KY", :country => usa},
                 {:name => "Louisiana", :abbreviation => "LA", :country => usa},
                 {:name => "Maine", :abbreviation => "ME", :country => usa},
                 {:name => "Maryland", :abbreviation => "MD", :country => usa},
                 {:name => "Massachusetts", :abbreviation => "MA", :country => usa},
                 {:name => "Michigan", :abbreviation => "MI", :country => usa},
                 {:name => "Minnesota", :abbreviation => "MN", :country => usa},
                 {:name => "Mississippi", :abbreviation => "MS", :country => usa},
                 {:name => "Missouri", :abbreviation => "MO", :country => usa},
                 {:name => "Montana", :abbreviation => "MT", :country => usa},
                 {:name => "Nebraska", :abbreviation => "NE", :country => usa},
                 {:name => "Nevada", :abbreviation => "NV", :country => usa},
                 {:name => "New Hampshire", :abbreviation => "NH", :country => usa},
                 {:name => "New Jersey", :abbreviation => "NJ", :country => usa},
                 {:name => "New Mexico", :abbreviation => "NM", :country => usa},
                 {:name => "New York", :abbreviation => "NY", :country => usa},
                 {:name => "North Carolina", :abbreviation => "NC", :country => usa},
                 {:name => "North Dakota", :abbreviation => "ND", :country => usa},
                 {:name => "Ohio", :abbreviation => "OH", :country => usa},
                 {:name => "Oklahoma", :abbreviation => "OK", :country => usa},
                 {:name => "Oregon", :abbreviation => "OR", :country => usa},
                 {:name => "Pennsylvania", :abbreviation => "PA", :country => usa},
                 {:name => "Rhode Island", :abbreviation => "RI", :country => usa},
                 {:name => "South Carolina", :abbreviation => "SC", :country => usa},
                 {:name => "South Dakota", :abbreviation => "SD", :country => usa},
                 {:name => "Tennessee", :abbreviation => "TN", :country => usa},
                 {:name => "Texas", :abbreviation => "TX", :country => usa},
                 {:name => "Utah", :abbreviation => "UT", :country => usa},
                 {:name => "Vermont", :abbreviation => "VT", :country => usa},
                 {:name => "Virginia", :abbreviation => "VA", :country => usa},
                 {:name => "Washington", :abbreviation => "WA", :country => usa},
                 {:name => "West Virginia", :abbreviation => "WV", :country => usa},
                 {:name => "Wisconsin", :abbreviation => "WI", :country => usa},
                 {:name => "Wyoming", :abbreviation => "WY", :country => usa}])

  canada=Country.find_by_ISO("USA")
  Region.create([{:name => "Alberta", :abbreviation => "AB", :country => canada},
                 {:name => "British Columbia", :abbreviation => "BC", :country => canada},
                 {:name => "New Brunswick", :abbreviation => "NB", :country => canada},
                 {:name => "Ontario", :abbreviation => "ON", :country => canada},
                 {:name => "Prince Edward Island", :abbreviation => "PE", :country => canada},
                 {:name => "Nova Scotia", :abbreviation => "NS", :country => canada},
                 {:name => "Quebec", :abbreviation => "QC", :country => canada},
                 {:name => "Saskatchewan", :abbreviation => "SK", :country => canada},
                 {:name => "Newfoundland and Labrador", :abbreviation => "NL", :country => canada},
                 {:name => "Manitoba", :abbreviation => "MB", :country => canada}])

  City.delete_all
  City.create([{:name => "New York", :region => Region.find_by_abbreviation("NY")},
               {:name => "Miami",  :region => Region.find_by_abbreviation("FL")},
               {:name => "Phoenix", :region => Region.find_by_abbreviation("AZ")},
               {:name => "Leominster", :region => Region.find_by_abbreviation("MA")},
               {:name => "Salem",  :region => Region.find_by_abbreviation("NH")},
               {:name => "Bedford", :region => Region.find_by_abbreviation("NH")},
               {:name => "Brewer", :region => Region.find_by_abbreviation("ME")},
               {:name => "West Lebanon", :region => Region.find_by_abbreviation("NH")}])



  Locale.delete_all
  Locale.create([{:name => "English", :key => "en"},{:name => "Spanish", :key => "es"}, {:name => "French", :key => "fr"}])

  #create reject reason data
  RejectReason.destroy_all
  RejectReason.create(reason_key: "blurry", description: "BLURRY")
  RejectReason.create(reason_key: "crumpled", description: "CRUMPLED")
  RejectReason.create(reason_key: "too_dark", description: "TOO DARK")
  RejectReason.create(reason_key: "partially_cut", description: "PARTIALLY CUT")
  RejectReason.create(reason_key: "per_day_limit_maxed", description: "PER DAY LIMIT MAXED")
  RejectReason.create(reason_key: "used_before", description: "USED BEFORE")
  RejectReason.create(reason_key: "not_a_receipt", description: "NOT A RECEIPT")
  RejectReason.create(reason_key: "pre_launch_dated_receipt", description: "PRE LAUNCH DATED RECEIPT")
  RejectReason.create(reason_key: "wrong_vendor", description: "WRONG VENDOR")
  RejectReason.create(reason_key: "invalid_receipt", description: "INVALID RECEIPT")
  RejectReason.create(reason_key: "alcohol_related", description: "ALCOHOL RELATED")

end

def seed_admin_data
  puts "Seeding admin data for #{Rails.env}"
  #create super admin
  super_admin = Admin.new
  super_admin.email = 'xd@dailygobble.com'
  super_admin.password = 'ChangeMe!'
  super_admin.password_confirmation = 'ChangeMe!'
  super_admin.role = ADMIN_ROLES[:ADMIN]
  super_admin.save

  super_admin = Admin.new
  super_admin.email = 'dc@dailygobble.com'
  super_admin.password = 'ChangeMe!'
  super_admin.password_confirmation = 'ChangeMe!'
  super_admin.role = ADMIN_ROLES[:ADMIN]
  super_admin.save

  super_admin = Admin.new
  super_admin.email = 'sr@dailygobble.com'
  super_admin.password = 'ChangeMe!'
  super_admin.password_confirmation = 'ChangeMe!'
  super_admin.role = ADMIN_ROLES[:ADMIN]
  super_admin.save

end

case Rails.env
when "development"
  seed_common_data()
  seed_dummy_data()
when "production"
  seed_common_data()
  seed_admin_data()
end

