class CreateTableUserConfirmations < ActiveRecord::Migration[5.0]
  def up
    create_table :user_confirmations do |t|
      t.boolean :email_confirmed, :default => false
      t.string :confirm_token
      t.integer :user_id

      t.timestamps
    end

    add_index :user_confirmations, :user_id
  end

  def down
  end
end
