class AddDeletedAtToNewTables < ActiveRecord::Migration[5.0]
  def change
    add_column :deals, :deleted_at, :datetime
    add_column :checkins, :deleted_at, :datetime
    add_column :submit_surveys, :deleted_at, :datetime
    add_column :submit_receipts, :deleted_at, :datetime
    add_column :deal_restaurants, :deleted_at, :datetime
    add_column :deal_offers, :deleted_at, :datetime
  end
end
