class AddAgeCheckToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :check_age_required, :boolean, :default => false
    add_column :chains, :check_age_value, :integer
  end
end
