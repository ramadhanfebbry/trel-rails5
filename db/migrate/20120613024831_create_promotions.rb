class CreatePromotions < ActiveRecord::Migration[5.0]
  def change
    create_table :promotions do |t|
      t.string  :name
      t.integer :promotable_id
      t.string  :promotable_type
      t.datetime :effective_date
      t.datetime :expiry_date
      t.references :chain

      t.timestamps
    end

    add_index :promotions, [:promotable_id, :promotable_type]
  end
end
