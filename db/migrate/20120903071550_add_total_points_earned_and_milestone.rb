class AddTotalPointsEarnedAndMilestone < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :total_points_earned, :float, :default => 0
    add_column :users, :milestone, :float
  end

  def down
    remove_column :users, :total_points_earned
    remove_column :users, :milestone
  end
end
