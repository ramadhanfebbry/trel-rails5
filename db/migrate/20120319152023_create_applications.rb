class CreateApplications < ActiveRecord::Migration[5.0]
  def change
    create_table :applications do |t|
      t.string :name
      t.string :appkey
      t.references :chain

      t.timestamps
    end

    add_index :applications, :appkey
  end
end
