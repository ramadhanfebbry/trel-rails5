class AddColumnStatusOnRewards < ActiveRecord::Migration[5.0]
  def up
    add_column :rewards,:status,:string,:default => 'inactive'
  end

  def down
    remove_column :rewards,:status,:string
  end
end
