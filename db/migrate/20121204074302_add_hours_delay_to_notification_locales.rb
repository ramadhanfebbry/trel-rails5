class AddHoursDelayToNotificationLocales < ActiveRecord::Migration[5.0]
  def change
    add_column :notification_locales, :hours_delay, :float, :default => 0
  end
end
