class AddPosLocationIdToGeneralMenuItems < ActiveRecord::Migration[5.0]
  def change
    add_column :general_menu_items, :pos_location_id, :integer
  end
end