class AddIndexToFishbowlPromotionCodes < ActiveRecord::Migration[5.0]
  def change
    add_index :fishbowl_promotion_codes, [:reward_id, :user_id]
  end
end
