class AddGifterToRewardWallets < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_wallets, :gifter, :boolean
  end
end