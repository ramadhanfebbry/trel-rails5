class AddSuccessAndErrorLogsToPushPoints < ActiveRecord::Migration[5.0]
  def change
    add_column :push_points, :success_logs, :text
    add_column :push_points, :error_logs, :text
  end
end
