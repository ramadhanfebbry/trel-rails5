class CreateSteppingstoneNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :steppingstone_notifications do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.string :subject
      t.text :content

      t.timestamps
    end
  end
end
