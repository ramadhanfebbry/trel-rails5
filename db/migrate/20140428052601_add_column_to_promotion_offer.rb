class AddColumnToPromotionOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :promotion_offers, :chain_id, :integer
    add_index :promotion_offers, :chain_id
  end
end
