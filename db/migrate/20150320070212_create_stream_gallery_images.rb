class CreateStreamGalleryImages < ActiveRecord::Migration[5.0]
  def change
    create_table :stream_gallery_images do |t|
      t.text :hyperlink
      t.text :title
      t.text :description
      t.text :url
      t.string :embed_file_name
      t.integer :embed_file_size
      t.string :embed_content_type
      # t.timestamps :embed_updated_at #unused
      t.timestamps
    end
  end
end
