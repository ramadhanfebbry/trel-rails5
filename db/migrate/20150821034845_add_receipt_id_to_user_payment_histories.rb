class AddReceiptIdToUserPaymentHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :user_payment_histories, :receipt_id, :integer
  end
end
