class AddIsConsideredForNegativeResponseToQuestions < ActiveRecord::Migration[5.0]
  def change
    add_column :questions, :is_considered_for_negative_response, :boolean, :default => false
  end
end
