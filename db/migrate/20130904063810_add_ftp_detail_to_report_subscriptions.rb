class AddFtpDetailToReportSubscriptions < ActiveRecord::Migration[5.0]
  def change
    add_column :report_subscriptions, :ftp_host, :string
    add_column :report_subscriptions, :ftp_username, :string
    add_column :report_subscriptions, :ftp_password, :string
    add_column :report_subscriptions, :ftp_upload_path, :string
  end
end
