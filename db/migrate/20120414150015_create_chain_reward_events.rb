class CreateChainRewardEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :chain_reward_events do |t|
      t.integer :event
      t.references :reward
      t.references :chain

      t.timestamps
    end
  end
end
