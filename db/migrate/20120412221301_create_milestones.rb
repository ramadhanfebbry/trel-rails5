class CreateMilestones < ActiveRecord::Migration[5.0]
  def change
    create_table :milestones do |t|
      t.string :name
      t.integer :type
      t.references :chain

      t.timestamps
    end
  end
end
