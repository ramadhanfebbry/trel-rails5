class AddMailchimpIgnoreMarketingOptinToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :mailchimp_ignore_marketing_optin, :boolean, :default => false

  end
end
