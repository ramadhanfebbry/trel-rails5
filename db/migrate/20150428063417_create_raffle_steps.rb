class CreateRaffleSteps < ActiveRecord::Migration[5.0]
  def change
    create_table :raffle_steps do |t|
      t.integer :raffle_id
      t.string :display_title
      t.string :display_subtitle
      t.string :display_description
      t.string :code

      t.timestamps
    end
  end
end
