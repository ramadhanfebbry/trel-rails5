class ChangeColumnHoursDelayFromRewards < ActiveRecord::Migration[5.0]
  def up
    change_column :rewards, :hours_delay, :float
  end

  def down
    change_column :rewards, :hours_delay, :integer
  end
end
