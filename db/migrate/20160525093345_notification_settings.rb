class NotificationSettings < ActiveRecord::Migration[5.0]

  def change
     create_table :notification_settings do |t|
      t.references :chain
      t.integer :notifiable_id
      t.string :notifiable_type
      t.integer :send_type
      t.string :email_template
      t.string :apikey_mandrill

      t.timestamps
    end
      add_index :notification_settings, [:notifiable_id, :notifiable_type], :name => 'notification_setting_index'      
  end

end
