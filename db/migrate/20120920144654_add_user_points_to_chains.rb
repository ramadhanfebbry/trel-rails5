class AddUserPointsToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :user_points, :float
  end
end
