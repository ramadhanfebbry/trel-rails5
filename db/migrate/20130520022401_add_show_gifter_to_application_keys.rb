class AddShowGifterToApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :application_keys, :show_gifter, :boolean, :default => true
  end
end
