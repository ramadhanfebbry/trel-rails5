class ChangeColumnOnGeoData < ActiveRecord::Migration[5.0]
  def up
    remove_column :geo_data_settings,   :next_date_collect
    remove_column :geo_data_settings,   :date_to_retry

    add_column :geo_data_settings,:n_days_to_post_from_today, :integer
    add_column :geo_data_settings,:num_days_retry, :integer

  end

  def down
  end
end
