class CreateTableDayVisits < ActiveRecord::Migration[5.0]
  def change
    create_table :day_visits do |t|
      t.integer :user_id, :chain_id, :restaurant_id
      t.date :visit_at
      t.timestamps
    end

    add_index(:day_visits, :user_id)
    add_index(:day_visits, :chain_id)
    add_index(:day_visits, :restaurant_id)
    add_index(:day_visits, [:chain_id,:user_id,:restaurant_id])

    add_index(:day_visits, [:restaurant_id, :user_id, :visit_at], :unique => true)
  end
end
