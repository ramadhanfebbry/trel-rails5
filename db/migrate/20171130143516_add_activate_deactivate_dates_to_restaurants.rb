class AddActivateDeactivateDatesToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :activated_at, :datetime
    add_column :restaurants, :deactivated_at, :datetime

  end
end
