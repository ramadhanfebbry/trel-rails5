class AddColumnEmailPushNotificationSettingToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users,:push_email,:boolean, :default => true
    add_column :users,:push_device,:boolean, :default => true
  end
end
