class AddColumnDeletedAtOnRefOffer < ActiveRecord::Migration[5.0]
  def up
    add_column :referral_offers, :deleted_at, :datetime
    add_column :referral_programs, :incentive_type, :string
    add_column :referral_programs, :incentive_id, :integer
  end

  def down
    remove_column :referral_offers, :deleted_at, :datetime
    remove_column :referral_programs, :incentive_type, :string
    remove_column :referral_programs, :incentive_id, :integer
  end
end
