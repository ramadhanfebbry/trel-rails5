class ChangeColumnChainsForRewardTimer < ActiveRecord::Migration[5.0]
  def up
    remove_column :chains, :reward_timer
    add_column :chains, :reward_timer, :integer, :default => 24
  end

  def down
  end
end
