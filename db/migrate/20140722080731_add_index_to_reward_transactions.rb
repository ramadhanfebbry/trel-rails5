class AddIndexToRewardTransactions < ActiveRecord::Migration[5.0]
  def change
    add_index :reward_transactions, :user_id
    add_index :reward_transactions, :reward_id
    add_index :reward_transactions, :staffcode
    add_index :reward_transactions, :restaurant_id
    add_index :reward_transactions, :pos_used
    add_index :reward_transactions, [:staffcode, :reward_id, :pos_used], :name => "staffcode_reward_pos"
  end
end
