class AddColumnOnQuestionValueOrLabl < ActiveRecord::Migration[5.0]
  def up
    add_column :questions , :non_app_display , :string , :limit => 8 , :default => "value"
    add_column :question_choices , :non_app_display , :string , :limit => 8 , :default => "value"
  end

  def down
    remove_column :questions , :non_app_display
    remove_column :question_choices , :non_app_display
  end
end
