class AddIDsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :twitterID, :string
    add_column :users, :facebookID, :string
    add_column :users, :deviceID, :string
    add_column :users, :register_device_type, :string
  end
end
