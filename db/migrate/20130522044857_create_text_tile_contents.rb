class CreateTextTileContents < ActiveRecord::Migration[5.0]
  def change
    create_table :text_tile_contents do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.text :content
      t.string :content_type

      t.timestamps
    end
  end
end
