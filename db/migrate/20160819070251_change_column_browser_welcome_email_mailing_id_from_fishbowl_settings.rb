class ChangeColumnBrowserWelcomeEmailMailingIdFromFishbowlSettings < ActiveRecord::Migration[5.0]
  def up
    change_column :fishbowl_settings, :browser_welcome_email_mailing_id, :string
  end

  def down
    change_column :fishbowl_settings, :browser_welcome_email_mailing_id, :string
  end
end
