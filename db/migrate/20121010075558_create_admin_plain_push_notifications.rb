class CreateAdminPlainPushNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :plain_push_notifications do |t|
      t.text :email
      t.text :content
      t.integer :chain_id

      t.timestamps
    end

    add_index :plain_push_notifications, :chain_id
  end
end
