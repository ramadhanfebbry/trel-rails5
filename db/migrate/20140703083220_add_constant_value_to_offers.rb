class AddConstantValueToOffers < ActiveRecord::Migration[5.0]
  def change
    add_column :offers, :constant_value, :integer
  end
end
