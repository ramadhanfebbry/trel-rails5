class AddUserCodeCountToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :user_code_count, :bigint, :default => 0
    Chain.all.each do |chain|
      user_code = chain.user_code_setting
      user_code_setting = JSON.parse(user_code.to_json) rescue nil
      user_code_setting ||= AppcodeSetting::DEFAULT_SETTING_USERCODE
      if user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        code_count = UserSession.where("code ~ ? AND chain_id = ? AND code_type = ? AND LENGTH(code) = ?", '^\d+$', chain.id, AppcodeSetting::CODE_TYPES["USERCODE"], user_code_setting["pos_digit_barcode"].to_i).count
      elsif user_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        code_count = UserSession.where("code ~ ? AND chain_id = ? AND code_type = ? AND LENGTH(code) = ?", '[a-zA-Z]', chain.id, AppcodeSetting::CODE_TYPES["USERCODE"], user_code_setting["pos_digit_barcode"].to_i).count
      end
      chain.update_column(:user_code_count, code_count)
    end
  end
end
