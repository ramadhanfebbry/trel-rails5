class AddErrorMessageToRestaurantImportHistory < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurant_import_histories, :error_message, :string
  end
end
