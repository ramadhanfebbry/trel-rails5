class AddRvcNumToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :rvc_num, :string
    execute "create index idx_rvc_num_pcu on pos_check_uploads (rvc_num);"
    execute "create index idx_chain_id_seq_num_check_id_rvc_num_pcu on pos_check_uploads (chain_id, check_id, sequence_number, rvc_num);"
  end
end
