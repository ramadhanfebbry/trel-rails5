class CreateVextRewardClaims < ActiveRecord::Migration[5.0]
  def change
    create_table :vext_reward_claims do |t|
      t.integer :chain_id
      t.integer :user_id
      t.integer :reward_id
      t.string :reward_code
      t.integer :location_id

      t.timestamps
    end
  end
end
