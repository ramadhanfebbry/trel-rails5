class AddColumnConstantValueOnOfferRules < ActiveRecord::Migration[5.0]
  def up
    add_column :offer_rules,:constant_value, :float, :default => nil
    add_column :offer_rules,:multiplier, :float, :default => nil
    add_column :offer_rules,:day_of_week_converter, :string
  end

  def down
  end
end
