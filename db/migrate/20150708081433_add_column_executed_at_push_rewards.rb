class AddColumnExecutedAtPushRewards < ActiveRecord::Migration[5.0]
  def up
    add_column :push_rewards,:executed_at, :datetime
  end

  def down
    remove_column :push_rewards,:executed_at
  end
end
