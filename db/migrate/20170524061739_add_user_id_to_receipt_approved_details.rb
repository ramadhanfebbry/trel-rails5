class AddUserIdToReceiptApprovedDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :receipt_approved_details, :user_id, :integer
    add_index :receipt_approved_details, :receipt_id
  end
end
