class AddNewColumnForFishbowlIntegration < ActiveRecord::Migration[5.0]
  def up
    add_column :fishbowl_settings, :fishbowl_log_id, :integer
    add_column :user_fishbowls, :email, :string
  end

  def down
    remove_column :fishbowl_settings, :fishbowl_log_id
    remove_column :user_fishbowls, :email
  end
end
