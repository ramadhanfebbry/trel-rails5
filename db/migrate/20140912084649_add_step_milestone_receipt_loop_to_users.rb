class AddStepMilestoneReceiptLoopToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :step_milestone_receipt_loop, :boolean, :default => false
  end
end
