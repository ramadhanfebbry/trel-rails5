class CreateMessageIncentiveLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :message_incentive_locales do |t|
      t.integer :goal_id
      t.integer :locale_id
      t.string :message

      t.timestamps
    end
    add_index :message_incentive_locales, :goal_id
    add_index :message_incentive_locales, :locale_id
    add_index :message_incentive_locales, [:goal_id, :locale_id]
  end
end
