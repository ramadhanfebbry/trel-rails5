class CreateTechStudioUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :tech_studio_users, :force => true do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :zip
      t.string :favorite_store
      t.string :phone_number
      t.integer :points
      t.date :birthday
      t.string :account_key
      t.date :signup_date
      t.integer :user_id, :default => nil
      t.boolean :migrated, :boolean, :default => false
      t.datetime :migration_date
      t.timestamps
    end
    execute("ALTER TABLE tech_studio_users ADD COLUMN optin optin;")
    add_index :tech_studio_users, :email
    add_index :tech_studio_users, :user_id
  end
end


