class RemoveChangedStatusLogAddStatusChangedFromAndStatusChangedToAndStatusChangedDateOnRestaurantDetail < ActiveRecord::Migration[5.0]
  def up
    if table_exists?(:restaurant_details) && column_exists?(:restaurant_details, :changed_status_log)
      remove_column :restaurant_details, :changed_status_log
    end

    if table_exists?(:restaurant_details)
      add_column :restaurant_details, :status_changed_from, :integer
      add_column :restaurant_details, :status_changed_to, :integer
      add_column :restaurant_details, :status_changed_date, :datetime
    end

  end

  def down
    add_column :restaurant_details, :changed_status_log, :text
    if column_exists? :restaurant_details, :status_changed_from
      remove_column :restaurant_details, :status_changed_from
    end
    if column_exists? :restaurant_details, :status_changed_to
      remove_column :restaurant_details, :status_changed_to
    end
    if column_exists? :restaurant_details, :status_changed_date
      remove_column :restaurant_details, :status_changed_date
    end
  end
end
