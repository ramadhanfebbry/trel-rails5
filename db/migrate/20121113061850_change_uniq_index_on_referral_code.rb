class ChangeUniqIndexOnReferralCode < ActiveRecord::Migration[5.0]
  def up
    remove_index :referral_codes,:name => :index_referral_codes_on_user_id
    #remove_index :referral_codes,:name => :index_referral_codes_on_code
    add_index :referral_codes, [:user_id], :unique => true
    add_index :referral_codes, [:code], :unique => true
  end

  def down
  end
end
