class AddParentIdOnOwner < ActiveRecord::Migration[5.0]
  def up
    add_column :owners, :parent_id, :integer
    add_index(:owners,:parent_id)
  end

  def down
    remove_column :owners, :parent_id
  end
end
