class AddOnlineOrderSupportStatusToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :online_order_support_status, :boolean, :default => true

  end
end
