class CreateGiftCodeUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :gift_code_users do |t|
      t.string :gift_code
      t.integer :user_id
      t.integer :chain_id
      t.datetime :expired_at
      t.timestamps
    end

    add_index :gift_code_users, :user_id
    add_index :gift_code_users, :chain_id
    add_index :gift_code_users, :gift_code
    add_index :gift_code_users, [:user_id, :gift_code ]
    add_index :gift_code_users, [:chain_id, :gift_code ], :unique => true
  end
end
