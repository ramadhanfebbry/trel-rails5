class CreateApplicationKeyDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :application_key_details do |t|
      t.integer :application_key_id
      t.boolean :is_required, default: false

      t.timestamps
    end
  end
end
