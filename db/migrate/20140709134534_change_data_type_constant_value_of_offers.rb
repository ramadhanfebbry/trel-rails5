class ChangeDataTypeConstantValueOfOffers < ActiveRecord::Migration[5.0]
  def up
    change_column :offers, :constant_value, :decimal
  end

  def down
    change_column :offers, :constant_value, :integer
  end
end
