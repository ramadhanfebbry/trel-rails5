class AddRewardIdToMilestones < ActiveRecord::Migration[5.0]
  def change
    add_column :milestones, :reward_id, :integer
    add_column :milestones, :milestone_number, :float
    add_column :milestones, :threshold_points, :float
  end
end
