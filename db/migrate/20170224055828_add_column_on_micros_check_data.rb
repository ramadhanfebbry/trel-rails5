class AddColumnOnMicrosCheckData < ActiveRecord::Migration[5.0]
  def up
    add_column :micros_check_data,:check_id , :string
    add_column :micros_check_data, :check_num, :string
    add_column :micros_check_data, :seq_num,:string
    add_column :micros_check_data, :receipt_number,:string
    add_column :micros_check_data, :table_num,:string
    add_column :micros_check_data, :check_open_time,:string
    add_column :micros_check_data, :receipt_date,:date
    add_column :micros_check_data, :subtotal,:float
    add_column :micros_check_data, :sales_total,:float
    add_column :micros_check_data, :total,:float
    add_column :micros_check_data, :tax,:float
    add_column :micros_check_data, :discount_total,:float
    add_column :micros_check_data, :direct_tips,:float
    add_column :micros_check_data, :tip_total,:float
    add_column :micros_check_data, :due_total,:float

    add_index :micros_check_data, :pos_check_upload_id
  end

  def down
    remove_column :micros_check_data, :check_id,:check_num, :seq_num,:subtotal , :sales_total, :total,:tax, :discount_total,:direct_tips, :tip_total,:due_total,
                  :receipt_date,:receipt_number,:table_num, :check_open_time

    remove_index :micros_check_data,:pos_check_upload_id
  end

end
