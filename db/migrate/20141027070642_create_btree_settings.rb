class CreateBtreeSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :btree_settings do |t|
      t.integer :chain_id
      t.string :public_key
      t.string :private_key
      t.string :environment
      t.string :merchant_id

      t.timestamps
    end
  end
end
