class AddNotifySysAdminForPosCodesToRewards < ActiveRecord::Migration[5.0]
  def change
    add_column :rewards, :notify_sys_admin_for_pos_codes, :boolean, :default => false
    add_column :rewards, :last_notification_sent_at, :datetime
  end
end
