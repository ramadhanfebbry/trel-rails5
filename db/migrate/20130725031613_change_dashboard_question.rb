class ChangeDashboardQuestion < ActiveRecord::Migration[5.0]
  def up
    rename_column :dashboard_choices,:question_id, :dashboard_question_id
    remove_column :dashboard_choices, :label
  end

  def down
    add_column :dashboard_choices, :label, :string
    rename_column :dashboard_choices, :dashboard_question_id,:question_id
  end
end
