class CreateVantivGiftCardEmailSentLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :vantiv_gift_card_email_sent_logs do |t|
      t.integer :chain_id
      t.string :giftcard_notification_email
      t.string :card_number
      t.float :giftcard_amount
      t.string :giftee_email
      t.string :giftee_name
      t.text :custom_message
      t.string :gifter_name
      t.text :image_header_url

      t.timestamps
    end
    add_index  :vantiv_gift_card_email_sent_logs, :chain_id
    add_index  :vantiv_gift_card_email_sent_logs, :giftee_email
  end
end
