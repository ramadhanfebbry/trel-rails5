class AddColumnStackableAndBonusThresholdToPerks < ActiveRecord::Migration[5.0]
  def up
    add_column :perks, :stackable, :boolean, :default => false
    add_column :perks, :bonus_threshold, :integer
    add_index :promo_codes, [:promotion_id, :chain_id]
  end

  def down
    remove_column :perks, :stackable, :bonus_threshold
    remove_index :promo_codes, :column => [:promotion_id, :chain_id]
  end
end
