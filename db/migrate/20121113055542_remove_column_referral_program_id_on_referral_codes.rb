class RemoveColumnReferralProgramIdOnReferralCodes < ActiveRecord::Migration[5.0]
  def up
    remove_column(:referral_codes, :referral_program_id)
  end

  def down
    add_column(:referral_codes, :referral_program_id, :integer)
  end
end
