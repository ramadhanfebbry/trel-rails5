class AddMaximumOfferPointsEarnedPerDayToChain < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :maximum_offer_points_earned_per_day, :float, :default => 0
  end
end
