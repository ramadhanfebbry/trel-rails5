class CreateGcmUnsentHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :gcm_unsent_histories do |t|
      t.string :message_id
      t.string :error_code
      t.string :reg_id
      t.datetime :failed_at
      t.timestamps
    end
    add_index :gcm_unsent_histories, :message_id
    add_index :gcm_unsent_histories, :reg_id
  end
end
