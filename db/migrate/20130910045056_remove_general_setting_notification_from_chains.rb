class RemoveGeneralSettingNotificationFromChains < ActiveRecord::Migration[5.0]
  def up
    remove_column :chains, :general_setting_push_to_email
    remove_column :chains, :general_setting_push_to_device
  end

  def down
    add_column :chains, :general_setting_push_to_email, :boolean, :default => true
    add_column :chains, :general_setting_push_to_device, :boolean, :default => true
  end
end
