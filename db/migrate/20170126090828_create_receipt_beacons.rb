class CreateReceiptBeacons < ActiveRecord::Migration[5.0]
  def change
    create_table :receipt_beacons do |t|
      t.integer :receipt_id
      t.string :beacon_info
      t.string :beacon_uuid
      t.string :beacon_serial_number

      t.timestamps
    end
    add_index :receipt_beacons, :receipt_id
  end
end
