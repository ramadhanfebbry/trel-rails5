class AddOnlineOrderLinkToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :online_order_link, :string
  end
end
