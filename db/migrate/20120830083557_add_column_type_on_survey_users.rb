class AddColumnTypeOnSurveyUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :surveys_users, :type, :string
  end

  def down
  end
end
