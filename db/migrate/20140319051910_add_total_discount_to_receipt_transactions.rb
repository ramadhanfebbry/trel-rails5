class AddTotalDiscountToReceiptTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :receipt_transactions, :total_discount, :decimal
  end
end
