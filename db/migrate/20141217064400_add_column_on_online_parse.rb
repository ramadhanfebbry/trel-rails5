class AddColumnOnOnlineParse < ActiveRecord::Migration[5.0]
  def up
    remove_column :online_orders, :issue_time
    add_column :online_parse_results, :issue_time, :string
  end

  def down
    remove_column :online_parse_results, :issue_time
  end
end
