class CreateRaffleUserInformations < ActiveRecord::Migration[5.0]
  def change
    create_table :raffle_user_informations do |t|
      t.integer :raffle_id
      t.integer :user_id
      t.string :user_email
      t.string :information_submited
      t.timestamps
    end

    remove_column :raffle_participation_histories, :information_submited
    remove_column :raffle_participation_histories, :user_email
    remove_column :raffle_participation_histories, :date_of_submission

  end
end
