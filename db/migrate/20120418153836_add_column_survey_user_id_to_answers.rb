class AddColumnSurveyUserIdToAnswers < ActiveRecord::Migration[5.0]
  def change
    add_column :answers, :surveys_user_id, :integer
    add_index(:answers,:surveys_user_id)
  end
end
