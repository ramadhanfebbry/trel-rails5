class AddColumnOnRewardsForDelay < ActiveRecord::Migration[5.0]
  def up
    add_column :rewards, :hours_delay, :integer, :default => 0
  end

  def down
    remove_column(:rewards,:hours_delay)
  end
end
