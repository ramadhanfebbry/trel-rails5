class AddColumnMissYouDateRestaurantUser < ActiveRecord::Migration[5.0]
  def up
    add_column :restaurant_users , :miss_you_date , :date
    add_column :owner_jobs, :miss_you, :boolean , :default => false
  end

  def down
    remove_column :restaurant_users , :miss_you_date , :date
    remove_column :owner_jobs, :miss_you
  end
end
