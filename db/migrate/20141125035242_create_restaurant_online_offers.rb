class CreateRestaurantOnlineOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurant_online_offers do |t|
      t.integer :restaurant_id
      t.integer :online_offer_id
      t.datetime :deleted_at
      t.timestamps
    end

    add_index :restaurant_online_offers, :restaurant_id
    add_index :restaurant_online_offers, :online_offer_id
    add_index :restaurant_online_offers, [:online_offer_id,:restaurant_id], :name => "by_restaurant_offer"
  end
end
