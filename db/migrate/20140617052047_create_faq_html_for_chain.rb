class CreateFaqHtmlForChain < ActiveRecord::Migration[5.0]
  def change
    create_table :faqs do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.text 	:html_content
      t.text 	:css_content

      t.timestamps
    end
  end
end
