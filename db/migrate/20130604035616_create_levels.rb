class CreateLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :levels do |t|
      t.integer :game_id
      t.integer :number
      t.float :ratio

      t.timestamps
    end
  end
end