class AddEmailSubmittedOnosysOnlineOrders < ActiveRecord::Migration[5.0]
  def up
    add_column :onosys_online_orders, :email, :string
  end

  def down
    remove_column :onosys_online_orders, :email
  end
end
