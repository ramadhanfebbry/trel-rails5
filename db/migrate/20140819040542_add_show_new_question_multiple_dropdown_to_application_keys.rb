class AddShowNewQuestionMultipleDropdownToApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :application_keys, :show_new_question_multiple_dropdown, :boolean, :default => false
  end
end