class CreateReceiptTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :receipt_transactions do |t|

	    t.datetime :issue_date
      t.string :time_stamp
	    t.string :check_number
	    t.string :cashier
      t.string :server_name
	    t.integer :status
	    t.integer :payment_method
	    t.integer :guests
      t.integer :order_id
	    t.decimal :tax
	    t.decimal :subtotal
	    t.decimal :food_total
	    t.decimal :beverage_total
	    t.decimal :preps_total
	    t.decimal :total_payment

      t.decimal :base_points_earned
      t.decimal :bonus_earned
      t.decimal :total_points_earned

	    t.references :receipt
	    t.references :restaurant_offer
	    t.references :reject_reason
	    t.references :admin
	    t.text :note

      t.timestamps
    end
  end
end
