class CreateTableMicrosCheckData < ActiveRecord::Migration[5.0]
  def change
    create_table :micros_check_data do |t|
      t.integer :pos_check_upload_id
      t.integer :chain_id
      t.integer :pos_location_id
      t.string :rvc_num, :emp_num
      t.integer :check_state
      t.datetime :created_at
    end

    add_index :micros_check_data, ["chain_id", "pos_location_id",
          "check_state", "rvc_num","created_at"],:name => 'micros_check_index'
  end
end
