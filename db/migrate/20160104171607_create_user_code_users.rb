class CreateUserCodeUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :user_code_users do |t|
      t.string :user_code
      t.integer :user_id
      t.integer :chain_id
      t.datetime :expired_at
      t.timestamps
    end

    add_index :user_code_users, :user_id
    add_index :user_code_users, :chain_id
    add_index :user_code_users, :user_code
    add_index :user_code_users, [:user_id, :user_code ]
    add_index :user_code_users, [:chain_id, :user_code ], :unique => true
  end
end
