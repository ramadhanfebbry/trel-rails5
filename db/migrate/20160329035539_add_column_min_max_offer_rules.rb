class AddColumnMinMaxOfferRules < ActiveRecord::Migration[5.0]
  def up
    add_column :offer_rules, :min_amount, :float, :default => 0 unless column_exists?(:offer_rules, :min_amount)
    add_column :offer_rules, :max_amount, :float, :default => 0 unless column_exists?(:offer_rules, :max_amount)
  end

  def down
    remove_column :offer_rules, :min_amount if column_exists?(:offer_rules, :min_amount)
    remove_columns :offer_rules, :max_amount if column_exists?(:offer_rules, :max_amount)
  end
end
