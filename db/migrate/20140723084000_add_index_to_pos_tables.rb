class AddIndexToPosTables < ActiveRecord::Migration[5.0]
  def change
    add_index :reward_menu_items, :chain_id
    add_index :reward_menu_items, :reward_id
    add_index :reward_menu_items, :general_menu_item_id

    add_index :reward_discounts, :reward_transaction_id
    add_index :reward_discounts, :check_id
    add_index :reward_discounts, :seq_num
    add_index :reward_discounts, :restaurant_id
    add_index :reward_discounts, [:check_id, :seq_num, :restaurant_id], :name => "check_seq_num_location"

    add_index :general_menu_items, :item_name
    add_index :general_menu_items, :item_keywords
    add_index :general_menu_items, :chain_id
    add_index :general_menu_items, :pos_location_id

    add_index :pos_check_uploads, :pos_location_id
    add_index :pos_check_uploads, :chain_id
    add_index :pos_check_uploads, :barcode
    add_index :pos_check_uploads, :check_id
    add_index :pos_check_uploads, :sequence_number

    add_index :receipts, :is_receipt_barcode
    add_index :receipts, :pos_check_upload_id

  end
end
