class ChangeColumnTypeOnNotificationLocales < ActiveRecord::Migration[5.0]
  def up
    change_column :notification_locales,:email_content, :text
  end

  def down
    change_column :notification_locales,:email_content, :string
  end
end
