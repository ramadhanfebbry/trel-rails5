class AddPolymorphToRefProgram < ActiveRecord::Migration[5.0]
  def up
    add_column :referral_programs, :incentive_referer_type, :string
    add_column :referral_programs, :incentive_referer_id, :integer
  end

  def down
    remove_column :referral_programs, :incentive_referer_type, :incentive_referer_id
  end
end
