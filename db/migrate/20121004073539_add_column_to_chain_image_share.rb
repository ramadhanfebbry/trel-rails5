class AddColumnToChainImageShare < ActiveRecord::Migration[5.0]
  def change
    add_column :chain_image_shares, :image_file_name,:string
    add_column :chain_image_shares, :image_file_size,:integer
    add_column :chain_image_shares, :image_content_type,:string
    add_column :chain_image_shares, :image_updated_at,:datetime        
  end
end
