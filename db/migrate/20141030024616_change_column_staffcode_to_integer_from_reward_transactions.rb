class ChangeColumnStaffcodeToIntegerFromRewardTransactions < ActiveRecord::Migration[5.0]
  def up
    change_column :reward_transactions, :staffcode, :string
  end

  def down
    change_column :reward_transactions, :staffcode, :integer
  end
end
