class AddEmailToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :email, :string
  end
end
