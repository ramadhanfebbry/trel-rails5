class AddFavoriteMenuItemToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :favorite_menu_item, :string
  end
end
