class AddSignupDateToGuestDnaUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :guest_dna_users, :signup_date, :date

  end
end
