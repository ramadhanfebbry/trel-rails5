class AddColumnOnReceiptsForOnlineOrder < ActiveRecord::Migration[5.0]
  def up
    add_column :receipts, :is_online_order ,:boolean, :default => false
    add_column :offers, :is_online_order,:boolean, :default => false
  end

  def down
    remove_column :receipts, :is_online_order
    remove_column :offers, :is_online_order
  end
end
