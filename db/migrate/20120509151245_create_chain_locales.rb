class CreateChainLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :chain_locales do |t|
      t.integer :chain_id
      t.integer :locale_id

      t.timestamps
    end
  end
end
