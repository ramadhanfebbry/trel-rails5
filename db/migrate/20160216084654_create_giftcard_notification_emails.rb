class CreateGiftcardNotificationEmails < ActiveRecord::Migration[5.0]
  def change
    create_table :giftcard_notification_emails do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.string :subject_email
      t.text :content_email
      t.timestamps
    end
  end
end
