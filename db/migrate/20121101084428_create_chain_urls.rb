class CreateChainUrls < ActiveRecord::Migration[5.0]
  def change
    create_table :chain_urls, :force => true do |t|
      t.integer :chain_id
      t.string :name
      t.string :url
      t.string :slug
      t.timestamps
    end
    add_index :chain_urls, :slug
    add_index :chain_urls, :chain_id
  end
end
