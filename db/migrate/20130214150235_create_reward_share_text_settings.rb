class CreateRewardShareTextSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_share_texts do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.text :share_text_twitter
      t.text :share_text_facebook

      t.timestamps
    end
  end
end