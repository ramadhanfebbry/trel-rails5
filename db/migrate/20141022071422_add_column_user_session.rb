class AddColumnUserSession < ActiveRecord::Migration[5.0]
  def up
    add_column :user_sessions, :used , :boolean , :default => false
    add_column :user_sessions,:location_id ,:integer
    add_index :user_sessions, [:user_id, :used]
    add_index :user_sessions, [:chain_id, :code]
    add_index :user_sessions, [:user_id, :code]
  end

  def down
    remove_column :user_sessions, :used , :boolean , :default => false
  end
end
