class AddColumnOnChainsForOnlineOrders < ActiveRecord::Migration[5.0]
  def up
    add_column :chains, :email_parse_url, :string
  end

  def down
    remove_column :chains, :email_parse_url
  end
end
