class AddNoOfDaysMicrosTableReceiptGeneratedToChain < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :no_of_days_micros_table_receipt_generated, :integer, :default => 0
  end
end
