class CreatePerkWallets < ActiveRecord::Migration[5.0]
  def change
    create_table :perk_wallets do |t|
      t.references :user
      t.references :perk
      t.datetime :expiry_date
      t.timestamps
    end
  end
end
