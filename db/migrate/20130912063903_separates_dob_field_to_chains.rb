class SeparatesDobFieldToChains < ActiveRecord::Migration[5.0]
  def up
    remove_column :users, :birth_day
    add_column :users, :dob_day, :integer
    add_column :users, :dob_month, :integer
    add_column :users, :dob_year, :integer
  end

  def down
    add_column :users, :birth_day, :date
    remove_column :users, :dob_day
    remove_column :users, :dob_month
    remove_column :users, :dob_year
  end
end
