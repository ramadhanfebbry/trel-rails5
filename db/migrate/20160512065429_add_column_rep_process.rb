class AddColumnRepProcess < ActiveRecord::Migration[5.0]
  def up
    ##
    if table_exists?(:rep_ncr_data_receipt_ncrs)
      add_column :rep_ncr_data_receipt_ncrs, :created_at, :timestamp
      add_column :rep_ncr_data_receipt_ncrs, :updated_at, :timestamp

      add_column :rep_point_history_ncrs, :created_at, :timestamp
      add_column :rep_point_history_ncrs, :updated_at, :timestamp
    end
  end

  def down
    remove_columns :rep_ncr_data_receipt_ncrs, :created_at,:updated_at
    remove_columns :rep_point_history_ncrs, :created_at,:updated_at
  end
end
