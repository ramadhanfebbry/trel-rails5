class CreateRewardPosCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_pos_codes do |t|
      t.integer :reward_id
      t.string :code
      t.integer :user_id
      t.datetime :used_at

      t.timestamps
    end
  end
end