class ChangeColumnGeoDatasSeting < ActiveRecord::Migration[5.0]
  def up
    rename_column :geo_data_settings, :post_data_time, :post_end_time
  end

  def down
    rename_column :geo_data_settings, :post_end_time,:post_data_time
  end
end
