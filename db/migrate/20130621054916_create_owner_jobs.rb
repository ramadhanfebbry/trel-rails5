class CreateOwnerJobs < ActiveRecord::Migration[5.0]
  def change
    create_table :owner_jobs do |t|
      t.text :description
      t.integer :job_type , :reward_type, :points, :owner_id, :status
      t.integer :amount_min, :amount_max, :total_users,:chain_id,:reward_id , :activity_days
      t.integer :percentage

      t.timestamps
    end
  end
end
