class CreateNoscanjobLogs < ActiveRecord::Migration[5.0]
    def change
      create_table :noscanjob_logs do |t|
        t.integer :no_scan_job_id
        t.integer :status
        t.integer :user_id
        t.string :email
        t.date :date_sent
        t.integer :success_notification
        #{:id=>181623, :email=>"kennynmd73@aol.com", :date_sent=>"07/25/14", :success_notif=>"success"}, {:id=>274114, :email=>"lonestar_lady@comcast.net", :date_sent=>"07/25/14"}
        t.timestamps
      end

      add_index :noscanjob_logs, :no_scan_job_id
      add_index :noscanjob_logs, [:no_scan_job_id,:status]
      add_index :noscanjob_logs, :user_id

      remove_column :noscanjob_logs, :success_notification
      add_column :noscanjob_logs, :notification_status, :integer
    end

    def down
    end
end
