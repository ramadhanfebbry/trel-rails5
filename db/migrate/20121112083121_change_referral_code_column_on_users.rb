class ChangeReferralCodeColumnOnUsers < ActiveRecord::Migration[5.0]
  def up
    rename_column(:users, :referral_code, :ref_code)
  end

  def down
    rename_column(:users, :ref_code, :referral_code)
  end
end
