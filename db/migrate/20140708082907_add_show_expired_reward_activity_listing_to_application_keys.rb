class AddShowExpiredRewardActivityListingToApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :application_keys, :show_expired_reward_activity_listing, :boolean, :default => true
  end
end
