class RemoveStateFromOwner < ActiveRecord::Migration[5.0]
  def change
    remove_index :owners, :state_id
    remove_column :owners, :state_id
  end

end
