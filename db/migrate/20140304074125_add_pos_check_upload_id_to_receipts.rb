class AddPosCheckUploadIdToReceipts < ActiveRecord::Migration[5.0]
  def change
    add_column :receipts, :pos_check_upload_id, :integer
  end
end