class AddNewColumnOnTextImageShareChains < ActiveRecord::Migration[5.0]
  def up
    add_column :chain_image_shares, :fb_description, :text
    add_column :chain_image_shares, :twitter_description, :text
    add_column :chain_image_shares, :fb_title, :string
    add_column :chain_image_shares, :twitter_title, :string
  end

  def down
    remove_column :chain_image_shares, :fb_description, :fb_title, :twitter_title, :twitter_description
  end
end
