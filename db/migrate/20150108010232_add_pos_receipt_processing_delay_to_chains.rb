class AddPosReceiptProcessingDelayToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :pos_receipt_processing_delay, :integer, :default => 0
    add_index :chains, :pos_receipt_processing_delay
  end
end
