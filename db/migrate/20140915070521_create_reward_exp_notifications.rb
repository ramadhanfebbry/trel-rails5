class CreateRewardExpNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_exp_notifications do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.string :email_subject
      t.text :email_body_html
      t.text :email_body_text
      t.text :notification
      t.timestamps
    end
  end
end