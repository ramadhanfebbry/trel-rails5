class AddColumnRegionOnRestaurants < ActiveRecord::Migration[5.0]
  def up
    add_column :restaurants, :region, :string, :limit => 64
  end

  def down
    remove_column :restaurants, :region
  end
end
