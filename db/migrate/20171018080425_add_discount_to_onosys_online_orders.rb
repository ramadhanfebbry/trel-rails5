class AddDiscountToOnosysOnlineOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :onosys_online_order_details, :discount, :decimal
  end
end
