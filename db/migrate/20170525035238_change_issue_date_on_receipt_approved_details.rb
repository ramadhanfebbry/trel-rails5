class ChangeIssueDateOnReceiptApprovedDetails < ActiveRecord::Migration[5.0]
  def up
     change_column :receipt_approved_details, :issue_date, :datetime
     remove_index :receipt_approved_details, :name => "chain_idsubtotalrestaurantidx"
     add_index :receipt_approved_details, [:chain_id ,:restaurant_id,:subtotal, :issue_date, :time_stamp, :receipt_number], :name => 'chain_idsubtotalrestaurantidx_new'
  end

  def down
    change_column :receipt_approved_details, :issue_date, :date
    add_index :receipt_approved_details,[:chain_id ,:restaurant_id,:subtotal, :issue_date, :time_stamp, :receipt_number], :name => 'chain_idsubtotalrestaurantidx'
    remove_index :receipt_approved_details, :name => 'chain_idsubtotalrestaurantidx_new'
  end
end
