class RemoveActivatedAtAndDeactivatedAtFromRestaurant < ActiveRecord::Migration[5.0]
  def up
    begin
      remove_column :restaurants, :activated_at
      remove_column :restaurants, :deactivated_at
    rescue
    end
  end

  def down
    add_column :restaurants, :activated_at, :datetime
    add_column :restaurants, :deactivated_at, :datetime
  end
end

