class CreateCertificates < ActiveRecord::Migration[5.0]
  def change
    create_table :certificates do |t|
      t.integer :application_id
      t.binary :cert_file
      t.string :filename
      t.string :content_type
      t.timestamps
    end
  end
end
