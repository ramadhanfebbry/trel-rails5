class AddCodeSeqToUserSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_sessions, :code_sequence, :integer unless column_exists?(:user_sessions, :code_sequence)    
  end
end

