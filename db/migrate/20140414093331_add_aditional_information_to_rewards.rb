class AddAditionalInformationToRewards < ActiveRecord::Migration[5.0]
  def change
    add_column :rewards, :additional_information, :boolean, :default => false
  end
end
