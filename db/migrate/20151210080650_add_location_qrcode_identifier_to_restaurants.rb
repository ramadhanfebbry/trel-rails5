class AddLocationQrcodeIdentifierToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :location_qrcode_identifier, :string
    add_index :restaurants, :location_qrcode_identifier
  end
end
