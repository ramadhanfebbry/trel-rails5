class ChangeColumnTypeForFavoriteMenuItemInUsers < ActiveRecord::Migration[5.0]
  def up
    change_column :users, :favorite_menu_item, :text
  end

  def down
    change_column :users, :favorite_menu_item, :string
  end
end
