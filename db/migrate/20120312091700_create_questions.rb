class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.string :text
      t.integer :question_type

      t.references :survey
      t.timestamps
    end

  end
end
