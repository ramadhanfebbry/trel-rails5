class ChangeDefaultValueForShowRewardActivityListingOnApplicationKeys < ActiveRecord::Migration[5.0]
  def up
    change_column :application_keys, :show_expired_reward_activity_listing, :boolean, :default => false
    ApplicationKey.all.each do |ap|
      ap.update_column(:show_expired_reward_activity_listing, false)
    end
  end

  def down
    change_column :application_keys, :show_expired_reward_activity_listing, :boolean, :default => true
  end
end
