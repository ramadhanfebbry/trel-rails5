class AddPhoneInfoToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :phone_model, :string
    add_column :users, :os_version, :string
    add_column :users, :chain_app_key, :string
  end
end
