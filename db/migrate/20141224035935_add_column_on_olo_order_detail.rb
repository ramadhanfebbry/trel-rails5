class AddColumnOnOloOrderDetail < ActiveRecord::Migration[5.0]
  def up
    add_column :olo_order_details, :processed, :boolean , :default => false
  end

  def down
    remove_column :olo_order_details, :processed
  end
end
