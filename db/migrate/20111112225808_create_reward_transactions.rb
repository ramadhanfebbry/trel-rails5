class CreateRewardTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_transactions do |t|
      t.decimal :longitude
      t.decimal :latitude
      t.integer :points

      t.timestamps
    end
  end
end
