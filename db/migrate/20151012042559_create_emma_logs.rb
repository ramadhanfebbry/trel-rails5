class CreateEmmaLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :emma_logs do |t|
      t.integer :chain_id
      t.integer :delayed_job_id
      t.string :status
      t.text :success_users
      t.text :failed_users
      t.string :log_type
      t.datetime :initiate_at
      t.datetime :completed_at
      t.text :error_report

      t.timestamps
    end
    add_index :emma_logs, :chain_id
    add_index :emma_logs, :delayed_job_id

  end
end
