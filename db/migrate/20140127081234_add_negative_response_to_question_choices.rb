class AddNegativeResponseToQuestionChoices < ActiveRecord::Migration[5.0]
  def change
    add_column :question_choices, :is_considered_for_negative_response, :boolean, :default => false
  end
end
