class AddPointSignupIncentiveToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :require_point_signup_incentive, :boolean, :default => false
    add_column :chains, :point_signup_incentive, :integer
  end
end
