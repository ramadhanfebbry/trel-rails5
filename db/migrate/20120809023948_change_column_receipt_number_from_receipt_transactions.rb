class ChangeColumnReceiptNumberFromReceiptTransactions < ActiveRecord::Migration[5.0]
  def up
    change_column :receipt_transactions, :receipt_number, :string
  end

  def down
    change_column :receipt_transactions, :receipt_number, :integer
  end
end
