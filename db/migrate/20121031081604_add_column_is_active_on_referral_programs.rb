class AddColumnIsActiveOnReferralPrograms < ActiveRecord::Migration[5.0]
  def up
    add_column :referral_programs, :is_active, :boolean, :default => false
  end

  def down
    remove_column :referral_programs, :is_active
  end
end
