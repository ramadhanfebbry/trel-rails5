class CreatePartnerMenuItems < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_menu_items do |t|
      t.integer :partner_category_id
      t.string :item_name
      t.integer :item_number
      t.text :description
      t.text :xml_data
      t.integer :chain_id

      t.timestamps
    end

    add_index :partner_menu_items, :partner_category_id
    add_index :partner_menu_items, :item_name
    add_index :partner_menu_items, :item_number
    add_index :partner_menu_items, :chain_id
    add_index :partner_menu_items, [:item_number, :chain_id, :partner_category_id], :name =>  :item_number_chain_and_partner_category
  end
end
