class AddKnownKeyToApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :application_keys, :known_key, :string
  end
end
