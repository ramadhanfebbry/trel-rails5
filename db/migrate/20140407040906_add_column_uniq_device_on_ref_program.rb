class AddColumnUniqDeviceOnRefProgram < ActiveRecord::Migration[5.0]
  def up
    add_column :referral_programs, :uniq_device , :boolean , :default => false
  end

  def down
    remove_column :referral_programs, :uniq_device
  end
end
