class CreateTableSubscriptionFormats < ActiveRecord::Migration[5.0]
 def change
   create_table :subscription_formats do |t|
     t.integer :subscription_id
     t.string :date_format
     t.string :file_name
     t.string :email_subject
     t.timestamps
   end
   add_index :subscription_formats, :subscription_id
 end
end
