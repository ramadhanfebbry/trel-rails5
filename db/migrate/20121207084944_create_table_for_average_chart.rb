class CreateTableForAverageChart < ActiveRecord::Migration[5.0]
  def change
    create_table :average_chart_tables do |t|      
      t.date :receipt_date
      t.integer :average,:chain_id, :restaurant_id
      t.integer :receipt_count
      t.timestamps
    end
    add_index :average_chart_tables,:chain_id
    add_index :average_chart_tables,:restaurant_id
    add_index :average_chart_tables,[:chain_id, :restaurant_id]
     
  end
end
