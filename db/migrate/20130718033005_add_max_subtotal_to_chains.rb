class AddMaxSubtotalToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :max_subtotal, :float, :default => 0
  end
end
