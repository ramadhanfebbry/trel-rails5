class CreateGeneralMenuItemRangeValues < ActiveRecord::Migration[5.0]
  def change
    create_table :general_menu_item_range_values do |t|
      t.integer :general_menu_item_id
      t.integer :start_value
      t.integer :end_value
      t.integer :chain_id
      t.timestamps
    end

    execute "create index index_general_menu_item_id_general_menu_item_range_values on general_menu_item_range_values (general_menu_item_id);"
    execute "create index index_chain_id_general_menu_item_range_values on general_menu_item_range_values (chain_id);"
  end
end
