class CreateTableUserGeoSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :user_geo_settings do |t|
      t.integer :user_id
      t.integer :geo_data_setting_id
      t.string :keystore
      t.string :android_id
      t.integer :device_type
      t.datetime :next_date
      t.datetime :last_run_at

      t.timestamps
    end

    add_index :user_geo_settings, :user_id
    add_index :user_geo_settings, :geo_data_setting_id
    add_index :user_geo_settings, [:geo_data_setting_id,:user_id]
  end
end
