class CreateEmmaMarketings < ActiveRecord::Migration[5.0]
  def change
    create_table :emma_marketings do |t|
      t.integer :chain_id
      t.integer :user_id
      t.string :email
      t.integer :restaurant_id
      t.integer :signup_id
      t.string :member_identifier

      t.timestamps
    end
    add_index :emma_marketings, :chain_id
    add_index :emma_marketings, :user_id
    add_index :emma_marketings, :restaurant_id
    add_index :emma_marketings, [:chain_id, :user_id]
  end
end
