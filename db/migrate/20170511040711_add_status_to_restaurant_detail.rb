class AddStatusToRestaurantDetail < ActiveRecord::Migration[5.0]
  def up
  	if table_exists?(:restaurant_details)
	    add_column :restaurant_details, :status, :integer
	    add_index :restaurant_details, :status
	  end
  end

  def down
    remove_column :restaurant_details, :status, :integer
  end
end
