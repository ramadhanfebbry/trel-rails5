class AddColumnRemovedToRewardWallets < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_wallets, :status, :integer, :default => 1
    rename_column :reward_wallets, :expiryDate, :expiry_date
  end
end
