class AddNotificationUrlToGamePointNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :game_point_notifications, :notification_url, :text
  end
end