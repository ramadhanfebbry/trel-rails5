class AddColumnTimeWantedOloDetail < ActiveRecord::Migration[5.0]
  def up
    add_column :olo_order_details, :time_wanted, :date
  end

  def down
    remove_column :olo_order_details, :time_wanted
  end
end
