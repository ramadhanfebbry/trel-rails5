class AddUsedByToPromoCodes < ActiveRecord::Migration[5.0]
  def change
    add_column :promo_codes, :used_by, :integer
  end
end
