class AddReceiptDateAndReceiptTimeToReceiptTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :receipt_transactions, :receipt_date, :date
    add_column :receipt_transactions, :receipt_time, :time
    add_index :receipt_transactions, :receipt_date
    add_index :receipt_transactions, :receipt_time
    add_index :receipt_transactions, [:receipt_date, :receipt_time]
  end
end
