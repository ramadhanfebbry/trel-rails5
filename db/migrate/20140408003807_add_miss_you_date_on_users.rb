class AddMissYouDateOnUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :miss_you_date, :date
  end

  def down
    remove_column :users, :miss_you_date, :date
  end
end
