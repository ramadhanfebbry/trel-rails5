class CreateGames < ActiveRecord::Migration[5.0]
  def change
    create_table :games do |t|
      t.string :name
      t.text :description
      t.float :daily_cap
      t.float :weekly_cap
      t.float :monthly_cap
      t.integer :chain_id

      t.timestamps
    end
  end
end