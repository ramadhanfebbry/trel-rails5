class AddColumnOnOloWebhook < ActiveRecord::Migration[5.0]
  def up
    add_column :olo_webhooks, :receipt_id, :integer
  end

  def down
    remove_column :olo_webhooks, :receipt_id
  end
end
