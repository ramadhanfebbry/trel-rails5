class CreateResetEmailNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :reset_email_notifications do |t|
      t.integer :locale_id
      t.integer :chain_id
      t.string :subject
      t.text :content
      t.timestamps
    end
  end
end