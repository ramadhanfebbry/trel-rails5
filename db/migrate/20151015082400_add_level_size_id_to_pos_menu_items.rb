class AddLevelSizeIdToPosMenuItems < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_menu_items, :level_size_id, :integer
    add_index :pos_menu_items, :level_size_id
  end
end
