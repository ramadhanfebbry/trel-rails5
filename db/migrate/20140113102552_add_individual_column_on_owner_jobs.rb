class AddIndividualColumnOnOwnerJobs < ActiveRecord::Migration[5.0]
  def up
    add_column :owner_jobs, :individual, :boolean , :default => false
    add_column :owner_jobs,:individual_user_id, :integer
    add_column :owner_jobs, :individual_email, :string
  end

  def down
    remove_column :owner_jobs, :individual  , :individual_id, :individual_email
  end
end
