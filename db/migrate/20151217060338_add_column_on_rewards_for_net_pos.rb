class AddColumnOnRewardsForNetPos < ActiveRecord::Migration[5.0]
  def up
    add_column :rewards, :targetType, :string
    add_column :rewards, :targetId, :string
  end

  def down
    remove_column :rewards, :targetId
    remove_column :rewards, :targetType
  end
end
