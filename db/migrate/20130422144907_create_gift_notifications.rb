class CreateGiftNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :gift_notifications do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.string :email_subject
      t.text :email_content
      t.text :warning_message

      t.timestamps
    end
  end
end