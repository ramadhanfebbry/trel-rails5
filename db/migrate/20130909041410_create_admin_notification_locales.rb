class CreateAdminNotificationLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_notification_locales do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.string :notification
      t.string :email_subject
      t.text :email_content
      t.integer :send_type

      t.timestamps
    end
  end
end
