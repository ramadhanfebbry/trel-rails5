class CreateNetposSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :netpos_settings do |t|
      t.integer :chain_id
      t.string :root_url
      t.string :login
      t.string :password

      t.timestamps
    end
    add_index :netpos_settings, :chain_id
  end
end
