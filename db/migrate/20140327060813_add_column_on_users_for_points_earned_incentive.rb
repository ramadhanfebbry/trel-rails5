class AddColumnOnUsersForPointsEarnedIncentive < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :point_threshold, :integer
  end

  def down
    remove_columns :users, :point_threshold, :integer
  end
end
