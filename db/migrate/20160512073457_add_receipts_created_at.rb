class AddReceiptsCreatedAt < ActiveRecord::Migration[5.0]
  def up
    if table_exists?(:rep_receipt_ncrs)
      add_column :rep_receipt_ncrs , :created_at, :datetime
      add_column :rep_receipt_ncrs , :updated_at, :datetime

    end
    
    if table_exists?(:rep_receipt_transaction_ncrs)
      add_column :rep_receipt_transaction_ncrs , :created_at, :datetime
      add_column :rep_receipt_transaction_ncrs , :updated_at, :datetime

      add_column :rep_reward_transaction_ncrs , :created_at, :datetime
      add_column :rep_reward_transaction_ncrs , :updated_at, :datetime
    end
  end

  def down
  end
end
