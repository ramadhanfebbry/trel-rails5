class CreatePosReceiptUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :pos_receipt_users do |t|
      t.integer :user_id
      t.integer :offer_id
      t.integer :restaurant_id
      t.integer :restaurant_offer_id
      t.integer :chain_id
      t.string :barcode
      t.integer :status, :default => 1

      t.timestamps
    end
  end
end