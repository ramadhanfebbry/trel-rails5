class CreatePosMenus < ActiveRecord::Migration[5.0]
  def change
    create_table :pos_menus do |t|
      t.string :pos_menu_type
      t.integer :pos_location_id
      t.text :menu_data
      t.integer :chain_id

      t.timestamps
    end
  end
end