class AddColumnDescriptionToRewardWallets < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_wallets, :description , :string
  end
end
