class RemoveStatusFromApplications < ActiveRecord::Migration[5.0]
  def up
    remove_column :applications, :status
  end

  def down
    add_column :applications, :status, :string
  end
end
