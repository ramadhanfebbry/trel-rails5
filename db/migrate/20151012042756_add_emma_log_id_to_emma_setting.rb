class AddEmmaLogIdToEmmaSetting < ActiveRecord::Migration[5.0]
  def change
    add_column :emma_settings, :emma_log_id, :integer
    add_index :emma_settings, :emma_log_id
  end
end
