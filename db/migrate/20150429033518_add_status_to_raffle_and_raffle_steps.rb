class AddStatusToRaffleAndRaffleSteps < ActiveRecord::Migration[5.0]
  def change
  	add_column :raffles, :active, :boolean, :default => false
  	add_column :raffle_steps, :active, :boolean, :default => false

  end
end
