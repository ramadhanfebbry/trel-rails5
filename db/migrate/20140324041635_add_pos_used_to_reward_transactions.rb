class AddPosUsedToRewardTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_transactions, :pos_used, :boolean, :default => false
  end
end
