class AddColumnOnHistoryPlainNotification < ActiveRecord::Migration[5.0]
  def up
     add_column :history_notifications,:plain_kind, :integer
    add_index :history_notifications, :plain_kind
  end

  def down
    remove_column :history_notifications,:plain_kind, :integer
  end
end
