class CreateRejectReasons < ActiveRecord::Migration[5.0]
  def change
    create_table :reject_reasons do |t|
      t.string :description

      t.timestamps
    end
  end
end
