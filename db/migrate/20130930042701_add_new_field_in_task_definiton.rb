class AddNewFieldInTaskDefiniton < ActiveRecord::Migration[5.0]
  def change
  	add_column :task_definitions,:description,:text
  	add_column :task_definitions,:job_type,:integer
  	add_column :task_definitions,:reward_type,:integer
  	add_column :task_definitions,:owner_id,:integer
  	add_column :task_definitions,:status,:integer
  	add_column :task_definitions,:amount_min,:integer
  	add_column :task_definitions,:amount_max,:integer
  	add_column :task_definitions,:total_users,:integer
  	add_column :task_definitions,:activity_days,:integer
  	add_column :task_definitions,:percentage,:integer
  	add_column :task_definitions,:notification,:boolean
  end
end
