class RemoveColumnContentInEmailList < ActiveRecord::Migration[5.0]
  def up
    remove_column :online_orders, :content
  end

  def down
    add_column :online_orders, :content, :text
  end
end
