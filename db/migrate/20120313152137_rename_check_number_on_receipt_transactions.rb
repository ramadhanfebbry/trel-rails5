class RenameCheckNumberOnReceiptTransactions < ActiveRecord::Migration[5.0]
  def change
    rename_column :receipt_transactions, :check_number, :card_number
    rename_column :receipt_transactions, :order_id, :receipt_number
  end

end
