class AddColumnRestaurantIdOnReports < ActiveRecord::Migration[5.0]
  def up
    add_column :reports, :restaurant_id, :integer
    add_index(:reports,:restaurant_id)
  end

  def down
    remove_column :reports, :restaurant_id, :integer
  end
end
