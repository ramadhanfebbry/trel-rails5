class CreateNotificationLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :notification_locales do |t|
      t.integer :notifiable_id
      t.string :notifiable_type
      t.string :notification
      t.string :email_subject
      t.string :email_content
      t.references :locale

      t.timestamps
    end
    add_index :notification_locales, [:notifiable_id, :notifiable_type]    
  end
end
