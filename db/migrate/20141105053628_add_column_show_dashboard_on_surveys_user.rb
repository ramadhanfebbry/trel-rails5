class AddColumnShowDashboardOnSurveysUser < ActiveRecord::Migration[5.0]
  def up
    add_column :surveys_users, :show_dashboard, :boolean, :default => true

    Survey.all.each do |sur|
      question_type_array = sur.questions.map(&:question_type)
      puts "mmmm"

      if question_type_array.size == 1 && question_type_array.include?(Question::TYPES["Comments"])
        puts "xxxxxxxxxxxxxxxx"
        SurveysUser.where(:survey_id => sur.id).each do |x|
          puts "yyyyyy"
          begin
            answers = x.answers.map(&:value)
            x.update_column(:show_dashboard, false) if answers.first == ""
          rescue
            puts "zzzzzzz"
            next
          end
        end
      end
    end
  end

  def down
    remove_column :surveys_users, :show_dashboard
  end

end
