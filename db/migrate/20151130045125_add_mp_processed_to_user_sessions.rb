class AddMpProcessedToUserSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_sessions, :mp_processed, :boolean, :default => false
    add_index :user_sessions, :mp_processed
  end
end
