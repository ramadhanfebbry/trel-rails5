class AddColumnIsNewestRewardPushToChain < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :new_push_reward, :text  #, :default => false
    add_column :users, :new_reward_notification, :boolean, :default => true  #, :default => false
  end
end
