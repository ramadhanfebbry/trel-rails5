class CreateReferralNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :referral_notifications do |t|
      t.integer :notifiable_id,:locale_id
      t.string :notifiable_type,:email_content ,:email_subject,:notification
      t.datetime :deleted_at
      t.timestamps
    end

    add_index(:referral_notifications,[:notifiable_id, :notifiable_type], :name => "polymorph_notif_referral")
    add_index(:referral_notifications,:locale_id)
  end
end
