class AddColumnNoScanDateOnUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :no_scan_date, :date
  end

  def down
    remove_column :users, :no_scan_date, :date
  end
end
