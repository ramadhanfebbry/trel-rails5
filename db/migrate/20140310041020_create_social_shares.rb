class CreateSocialShares < ActiveRecord::Migration[5.0]
  def change
    create_table :social_shares do |t|
      t.string :name
      t.text :description
      t.integer :chain_id
      t.date :start_date
      t.date :end_date
      t.boolean :active
      t.float    "daily_cap"
      t.float    "weekly_cap"
      t.float    "monthly_cap"
      t.timestamps
    end

    create_table :social_offers do |t|
      t.string :title
      t.integer :points,:kind, :chain_id
    end



    add_index :social_shares,:chain_id
  end
end
