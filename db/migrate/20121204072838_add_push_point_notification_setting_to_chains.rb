class AddPushPointNotificationSettingToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :is_send_push_point_notif_to_email, :boolean, :default => false
    add_column :chains, :is_send_push_point_notif_to_phone, :boolean, :default => false
  end
end
