class NetposScheduledJobFrequencyToChains < ActiveRecord::Migration[5.0]
  def up
    add_column :chains, :netpos_batch_check_request, :integer, :default => 60
  end

  def down
    remove_column :chains, :netpos_batch_check_request, :integer
  end
end
