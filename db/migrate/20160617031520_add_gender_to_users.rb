class AddGenderToUsers < ActiveRecord::Migration[5.0]
  def change
    execute("ALTER TABLE users ADD COLUMN gender gender DEFAULT '0';")
  end
end
