class AddColumnOnReportSubscriptionLastExecutedAt < ActiveRecord::Migration[5.0]
  def up
    add_column :report_subscriptions, :last_executed_at , :date
  end

  def down
    remove_column :report_subscriptions, :last_executed_at , :date
  end
end
