class AddQualifiedSurveyIncentiveToSurveysUser < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys_users, :qualified_survey_incentive, :boolean, :default => false
    add_index(:surveys_users, :qualified_survey_incentive)
  end
end
