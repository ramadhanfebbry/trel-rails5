class CreateRewardSentHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_sent_histories do |t|
      t.integer :reward_id
      t.integer :user_id
      t.integer :chain_id
      t.timestamps
    end
    add_index :reward_sent_histories, :reward_id
    add_index :reward_sent_histories, :user_id
    add_index :reward_sent_histories, :chain_id
    add_index :reward_sent_histories, [:reward_id, :user_id]
  end
end
