class ChangeColumnFbPointToInteger < ActiveRecord::Migration[5.0]
  def up
    change_column :chains, :fb_signup_incentive_point, :integer
  end

  def down
    change_column :chains, :fb_signup_incentive_point, :decimal
  end
end
