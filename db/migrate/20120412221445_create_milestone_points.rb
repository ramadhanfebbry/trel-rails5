class CreateMilestonePoints < ActiveRecord::Migration[5.0]
  def change
    create_table :milestone_points do |t|
      t.integer :points
      t.references :reward

      t.timestamps
    end
  end
end
