class AddColumnChainIdOnPerks < ActiveRecord::Migration[5.0]
  def up
    add_column :perks, :chain_id, :integer
    add_index(:perks, :chain_id)
  end

  def down
    remove_column(:perks, :chain_id)
    remove_index :perks, :chain_id
  end
end
