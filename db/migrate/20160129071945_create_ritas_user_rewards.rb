class CreateRitasUserRewards < ActiveRecord::Migration[5.0]
  def change
    create_table :ritas_user_rewards do |t|
      t.string :email
      t.integer :current_redeemable_cards

      t.timestamps
    end

  end
end
