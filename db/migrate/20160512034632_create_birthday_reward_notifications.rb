class CreateBirthdayRewardNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :birthday_reward_notifications do |t|
      t.integer :notifiable_id
      t.string :notifiable_type
      t.string :notification
      t.string :email_subject
      t.string :email_content
      t.text :email_content_html
      t.references :locale

      t.timestamps
    end

    if !index_exists?(:birthday_reward_notifications, [:notifiable_id, :notifiable_type])
      add_index :birthday_reward_notifications, [:notifiable_id, :notifiable_type], :name => 'birthday_reward_index'
    end
    
    if !index_exists?(:birthday_reward_notifications, :locale_id)
      add_index :birthday_reward_notifications, :locale_id
    end    
  end
end
