class CreateGoalUserIncentives < ActiveRecord::Migration[5.0]
  def change
    create_table :goal_user_incentives do |t|
      t.integer :incentive_id
      t.string :incentive_type
      t.integer :goal_id
      t.integer :week_id
      t.integer :user_id

      t.timestamps
    end

    add_index :goal_user_incentives, :incentive_id
    add_index :goal_user_incentives, :incentive_type
    add_index :goal_user_incentives, :user_id
    add_index :goal_user_incentives, :goal_id
    add_index :goal_user_incentives, :week_id
    add_index :goal_user_incentives, [:incentive_id, :incentive_type]

  end
end
