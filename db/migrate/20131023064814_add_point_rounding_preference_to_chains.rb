class AddPointRoundingPreferenceToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :point_rounding_preference, :string, :default => "ROUND"
  end
end
