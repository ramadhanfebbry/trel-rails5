class CreateOfferRules < ActiveRecord::Migration[5.0]
  def change
    create_table :offer_rules do |t|
      t.integer :offer_id
      t.date :start_date
      t.date :end_date
      t.string :time_start, :time_end
      t.integer :days_of_week
      t.integer :rule_type
      t.integer :value

      t.timestamps
    end

    add_index :offer_rules, :offer_id
    add_index :offer_rules, :rule_type

  end
end
