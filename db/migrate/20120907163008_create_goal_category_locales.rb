class CreateGoalCategoryLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :goal_category_locales do |t|
      t.integer :category_id
      t.integer :locale_id
      t.text :text

      t.timestamps
    end
  end
end
