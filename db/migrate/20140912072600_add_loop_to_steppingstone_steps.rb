class AddLoopToSteppingstoneSteps < ActiveRecord::Migration[5.0]
  def change
    add_column :steppingstone_steps, :loop, :boolean, :default => true
  end
end
