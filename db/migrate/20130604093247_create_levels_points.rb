class CreateLevelsPoints < ActiveRecord::Migration[5.0]
  def change
    create_table :levels_points do |t|
      t.integer :level_number
      t.float :points
      t.integer :user_id
      t.integer :game_id

      t.timestamps
    end
  end
end