class AddIsGetIncentiveToReferralUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :referral_users, :is_get_incentive, :boolean, default: false
  end
end
