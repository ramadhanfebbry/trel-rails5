class CreateRestaurantHours < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurant_hours, :force => true do |t|
      t.integer :restaurant_id
      t.integer :day_of_week
      t.string :open_at
      t.string :close_at

      t.timestamps
    end

    add_index :restaurant_hours, :restaurant_id
  end
end
