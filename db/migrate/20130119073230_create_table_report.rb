class CreateTableReport < ActiveRecord::Migration[5.0]
  def change
    create_table :reports do |t|
      t.date :start_date, :end_date
      t.integer :chain_id
      t.string :s3_path
      t.timestamps
    end
    add_index :reports,:chain_id
    add_index :reports,[:start_date, :end_date]  
  end
end
