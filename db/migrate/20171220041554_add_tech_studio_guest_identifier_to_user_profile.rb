class AddTechStudioGuestIdentifierToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :tech_studio_guest_identifier, :string

  end
end
