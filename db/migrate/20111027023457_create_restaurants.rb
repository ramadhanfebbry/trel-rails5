class CreateRestaurants < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :app_display_text
      t.string :dashboard_display_text
      t.string :address
      t.string :zipcode
      t.string :phone_number
      t.string :contact
      t.string :latitude
      t.string :longitude
      t.string :city
      t.string :state
      t.references :chain
      t.references :city
      t.references :state
      
      t.timestamps
    end
    
  end
  
end
