class CreateLineItems < ActiveRecord::Migration[5.0]
  def change
    create_table :line_items do |t|
      t.string :item
      t.decimal :price
      t.integer :number_of_item
      t.references :receipt

      t.timestamps
    end
  end
end
