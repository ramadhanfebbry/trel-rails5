class CreateRestaurantsOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurants_owners do |t|
      t.integer :owner_id, :restaurant_id
      t.datetime :deleted_at
      t.timestamps
    end

    add_index :restaurants_owners, :owner_id
    add_index :restaurants_owners, :restaurant_id
    add_index :restaurants_owners, [:restaurant_id, :owner_id]
  end
end
