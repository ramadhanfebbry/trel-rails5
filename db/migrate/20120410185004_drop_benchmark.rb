class DropBenchmark < ActiveRecord::Migration[5.0]
  def change
    drop_table :benchmarks if self.table_exists?("benchmarks")
    drop_table :benchmark_points if self.table_exists?("benchmark_points")
    drop_table :user_benchmark_points if self.table_exists?("user_benchmark_points")

    rename_column :answers, :valueId, :value_id
  end

  def self.table_exists?(name)
      ActiveRecord::Base.connection.tables.include?(name)
  end
end
