class AddLeidToEmailMarketings < ActiveRecord::Migration[5.0]
  def change
    add_column :email_marketings, :leid, :string
  end
end