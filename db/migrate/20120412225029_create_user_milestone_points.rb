class CreateUserMilestonePoints < ActiveRecord::Migration[5.0]
  def change
    create_table :user_milestone_points do |t|
      t.integer :current_redeem_at
      t.integer :last_redeem_at

      t.references :user
      t.references :milestone
      t.timestamps
    end
  end
end
