class AddColumnPointHistoriesForNcr < ActiveRecord::Migration[5.0]
  def up
    add_column :point_histories, :assignment_id, :integer
    add_index :point_histories, :assignment_id
  end

  def down
    remove_column :point_histories, :assignment_id
  end
end
