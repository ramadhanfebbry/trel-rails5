class ChangeLocaleColumns < ActiveRecord::Migration[5.0]
  def change
    remove_column :locales, :value
   	add_column :locales, :name, :string
  end

end
