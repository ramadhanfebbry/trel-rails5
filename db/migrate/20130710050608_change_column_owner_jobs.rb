class ChangeColumnOwnerJobs < ActiveRecord::Migration[5.0]
  def up
    rename_column :owner_jobs, :subject, :subject_email
    rename_column :owner_jobs, :content, :content_email

    add_column :owner_jobs, :subject_phone, :string
    add_column :owner_jobs, :content_phone, :text
  end

  def down
  end
end
