class CreateEmailTemplates < ActiveRecord::Migration[5.0]
  def change
    create_table :email_templates do |t|
      t.string :template_name
      t.integer :chain_id
      t.string :mail_subject
      t.text :mail_content_html
      t.text :mail_content_text
      t.integer :locale_id
      t.string :send_as
      t.timestamps
    end
  end
end