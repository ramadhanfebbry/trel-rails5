class CreateUserRemovePoints < ActiveRecord::Migration[5.0]
  def change
    create_table :user_remove_points do |t|
      t.integer :remove_point_id
      t.integer :user_id
      t.string :email
      t.timestamps
    end

    add_index :user_remove_points, :remove_point_id
    add_index :user_remove_points, :user_id
    add_index :user_remove_points, [:remove_point_id,:user_id]
  end
end
