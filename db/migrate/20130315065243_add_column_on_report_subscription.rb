class AddColumnOnReportSubscription < ActiveRecord::Migration[5.0]
  def up
    add_column :report_subscriptions, :report_settings , :text
    add_column :report_subscriptions, :run_at , :time
    add_column :report_definitions, :active , :boolean, :default => true
  end

  def down
    remove_column :report_subscriptions, :report_settings, :run_at
  end
end
