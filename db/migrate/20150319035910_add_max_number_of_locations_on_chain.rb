class AddMaxNumberOfLocationsOnChain < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :max_number_of_locations, :integer, :default => 10

  end
end
