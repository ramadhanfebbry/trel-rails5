class AddDefaultCodeToAppcodeSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :appcode_settings, :default_code, :boolean, default: false
  end
end
