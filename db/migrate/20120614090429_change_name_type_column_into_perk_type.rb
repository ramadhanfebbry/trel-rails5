class ChangeNameTypeColumnIntoPerkType < ActiveRecord::Migration[5.0]
  def up
    remove_column(:perks, :type)
    add_column(:perks, :perk_type,:integer)
  end

  def down
    add_column(:perks, :type,:integer)
    remove_column :perks, :perk_type
  end
end
