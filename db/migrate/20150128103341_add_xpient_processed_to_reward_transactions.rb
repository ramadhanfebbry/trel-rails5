class AddXpientProcessedToRewardTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_transactions, :xpient_processed, :boolean, :default => false
    add_index :reward_transactions, :xpient_processed
  end
end
