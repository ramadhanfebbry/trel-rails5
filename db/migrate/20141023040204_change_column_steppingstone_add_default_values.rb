class ChangeColumnSteppingstoneAddDefaultValues < ActiveRecord::Migration[5.0]
  def up
    remove_column :steppingstones, :loop_consideration
  end

  def down
    add_column :steppingstones, :loop_consideration, :boolean, :default => false
  end
end
