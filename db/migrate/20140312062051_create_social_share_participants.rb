class CreateSocialShareParticipants < ActiveRecord::Migration[5.0]
  def change
    create_table :social_share_participants do |t|
      t.integer :user_id
      t.integer :reward_id
      t.integer :points
      t.boolean :received
      t.text :log
      t.string :email
      t.integer :medium_id
      t.string :medium
      t.timestamps
    end

    remove_column :social_shares, :daily_cap
    remove_column :social_shares, :weekly_cap
    remove_column :social_shares,  :monthly_cap
    add_column :social_shares, :daily_cap, :integer , :default => 0
    add_column :social_shares, :weekly_cap, :integer , :default => 0
    add_column :social_shares, :monthly_cap, :integer , :default => 0
    add_index :social_share_participants, :user_id
    add_index :social_share_participants, :medium_id
    add_index :social_share_participants, :email
  end
end
