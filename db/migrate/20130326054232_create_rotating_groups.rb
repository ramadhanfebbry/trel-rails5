class CreateRotatingGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :rotating_groups do |t|
      t.string :name
      t.integer :group_type
      t.boolean :active, :default => true
      t.integer :priority
      t.integer :max_renders
      t.integer :survey_id
      t.integer :chain_id

      t.timestamps
    end
  end
end