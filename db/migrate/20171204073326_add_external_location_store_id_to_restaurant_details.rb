class AddExternalLocationStoreIdToRestaurantDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurant_details, :external_location_store_id, :string if table_exists?(:restaurant_details)
  end
end
