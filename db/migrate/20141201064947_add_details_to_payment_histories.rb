class AddDetailsToPaymentHistories < ActiveRecord::Migration[5.0]
  def change
    change_column :payment_histories, :mp_payment_id, :string
    remove_column :payment_histories, :receipt_id
    add_column :payment_histories, :status, :integer
    add_column :payment_histories, :action, :string
    change_column :payment_histories, :updated_at, :datetime, :null => true

    add_index :payment_histories, :status
    add_index :payment_histories, :action
  end
end
