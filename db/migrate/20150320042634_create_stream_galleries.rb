class CreateStreamGalleries < ActiveRecord::Migration[5.0]
  def change
    create_table :stream_galleries do |t|
      t.integer :chain_id
      t.text :title
      t.text :description
      t.integer :animation_style
      t.integer :transition_duration
      t.integer :display_duration

      t.timestamps
    end
  end
end
