class AddSequenceNumberToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :sequence_number, :string
  end
end
