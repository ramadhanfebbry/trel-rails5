class AddActivateMilestoneReceiptToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :activate_milestone_receipt, :boolean, :default => true
    add_column :chains, :activate_stepping_stone, :boolean, :default => false
  end
end