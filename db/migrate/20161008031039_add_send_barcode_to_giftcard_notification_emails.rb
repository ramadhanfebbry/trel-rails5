class AddSendBarcodeToGiftcardNotificationEmails < ActiveRecord::Migration[5.0]
  def change
    add_column :giftcard_notification_emails, :send_barcode, :boolean, default: false
  end
end
