class CreateUserSessions < ActiveRecord::Migration[5.0]
  def change
    if UserSession.table_exists?
      puts "Table Exist Drop first"
      drop_table :user_sessions
      create_table :user_sessions do |t|
        t.integer :user_id
        t.integer :chain_id
        t.integer :application_id
        t.string :code, :code_with_prefix
        t.float :lat
        t.float :long
        t.string :city
        t.string :postal_code
        t.string :country_code
        t.text :address

        t.timestamps
      end

      add_index :user_sessions, :user_id
      add_index :user_sessions, :code
    else


      create_table :user_sessions do |t|
        puts "Table Not Exist create"
        t.integer :user_id
        t.integer :chain_id
        t.integer :application_id
        t.string :code, :code_with_prefix
        t.float :lat
        t.float :long
        t.string :city
        t.string :postal_code
        t.string :country_code
        t.text :address

        t.timestamps
      end

      add_index :user_sessions, :user_id
      add_index :user_sessions, :code
    end
  end
end
