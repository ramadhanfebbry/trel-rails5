class AddColumnExecutedAtOnRewards < ActiveRecord::Migration[5.0]
  def up
    add_column :rewards, :executed_at, :datetime
  end

  def down
    remove_column :rewards, :executed_at
  end
end
