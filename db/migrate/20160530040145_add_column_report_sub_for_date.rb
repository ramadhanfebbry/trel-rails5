class AddColumnReportSubForDate < ActiveRecord::Migration[5.0]
  def up
    add_column :report_subscriptions, :date_format, :string, :default => "%Y-%m-%d"
  end

  def down
    remove_column :report_subscriptions, :date_format
  end
end
