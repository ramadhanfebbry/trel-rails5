class AddNewColoumnToRemovePoint < ActiveRecord::Migration[5.0]
  def change
    add_column :remove_points, :user_ids , :text
    add_column :remove_points, :success_logs, :text
    add_column :remove_points, :error_logs, :text
    add_column :remove_points, :pusher_id, :integer
  end
end
