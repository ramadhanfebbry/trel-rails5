class AddFileTypeOnReportSubscriptions < ActiveRecord::Migration[5.0]
  def up
    add_column :report_subscriptions, :file_type, :string
  end

  def down
    remove_column :report_subscriptions, :file_type, :string
  end
end
