class AddUserIdAndErrorCodeToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :user_id, :integer
    add_column :pos_check_uploads, :error_code, :integer

    add_index :pos_check_uploads, :user_id
  end
end
