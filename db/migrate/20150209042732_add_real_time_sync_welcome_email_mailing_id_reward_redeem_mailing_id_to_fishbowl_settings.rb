class AddRealTimeSyncWelcomeEmailMailingIdRewardRedeemMailingIdToFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :real_time_sync, :boolean, :default => false
    add_column :fishbowl_settings, :welcome_email_mailing_id, :integer
    add_column :fishbowl_settings, :reward_redeem_mailing_id, :integer
    add_column :fishbowl_settings, :reward_redeem_email_title, :string
    add_column :fishbowl_settings, :reward_redeem_expirations_date, :string

  end
end