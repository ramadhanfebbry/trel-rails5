class AddColumnLimitOnPromotion < ActiveRecord::Migration[5.0]
  def up
    add_column :promotions, :limit, :integer , :default => 0
  end

  def down
    remove_column :promotions, :limit, :integer
  end
end
