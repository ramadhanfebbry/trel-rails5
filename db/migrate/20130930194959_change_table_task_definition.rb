class ChangeTableTaskDefinition < ActiveRecord::Migration[5.0]
  def change
    drop_table :task_definitions

    create_table :task_definitions do |t|
      t.integer  :job_type,:reward_type, :points, :owner_id, :status,  :total_users,:chain_id, :reward_id,
                 :activity_days,:percentage,:schedule_type
      t.datetime :executed_at
      t.string :subject_email  ,:subject_phone
      t.text :content_email,:content_phone
      t.boolean :push_email, :push_phone   #"notification"=>false, "miss_you"=>false
      t.timestamps
    end

    add_index :task_definitions, :chain_id
    add_index :task_definitions, :reward_id
    add_index :task_definitions, [:chain_id,:reward_id]
  end

end
