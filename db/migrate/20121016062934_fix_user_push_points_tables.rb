class FixUserPushPointsTables < ActiveRecord::Migration[5.0]
  def up
    drop_table(:user_push_points)

    create_table :user_push_points do |t|
      t.integer :push_point_id
      t.integer :user_id
      t.string :email
      t.timestamps
    end

    add_index :user_push_points, :push_point_id
    add_index :user_push_points, :user_id
    add_index :user_push_points, [:push_point_id,:user_id]
  end

  def down
  end
end
