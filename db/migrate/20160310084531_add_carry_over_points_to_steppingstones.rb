class AddCarryOverPointsToSteppingstones < ActiveRecord::Migration[5.0]
  def change
    add_column :steppingstones, :carry_over_points, :boolean, default: false
  end
end
