class RemoveDeviceIdFromUsers < ActiveRecord::Migration[5.0]
  def up
    remove_column :users, :deviceID
  end

  def down
    add_column :users, :deviceID, :string
  end
end
