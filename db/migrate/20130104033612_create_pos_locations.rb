class CreatePosLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :pos_locations do |t|
      t.integer :chain_id
      t.string :apikey, :unique => true
      t.integer :restaurant_id
      t.timestamps
    end
  end
end