class AddColumnOnOfferRuleMenuItems < ActiveRecord::Migration[5.0]
  def up
    add_column :offer_rules, :name , :string
    add_column :offer_rules, :description , :text
  end

  def down
    remove_column :offer_rules, :name, :description
  end
end
