class ChangeNameApikeyMadrillToApikey < ActiveRecord::Migration[5.0]
  def up
    rename_column :notification_settings, :apikey_mandrill, :apikey
  end

  def down
    rename_column :notification_settings, :apikey, :apikey_mandrill
  end
end
