class CreateTableNoScanScheduler < ActiveRecord::Migration[5.0]
  def up
      unless table_exists?("no_scan_job_schedulers")
        execute "-- DROP TABLE no_scan_schedulers;

    CREATE TABLE no_scan_job_schedulers
    (
      id serial NOT NULL,
      job_id integer,
      run_type character varying(255),
      start_at timestamp without time zone,
      executed_at timestamp without time zone,
      last_executed timestamp without time zone,
      next_run timestamp without time zone,
      end_at timestamp without time zone,
      created_at timestamp without time zone NOT NULL,
      updated_at timestamp without time zone NOT NULL,
      CONSTRAINT no_scan_job_schedulers_pkey PRIMARY KEY (id)
    )
    WITH (
      OIDS=FALSE
    );"
    end
  end

  def down
    drop_table :no_scan_job_schedulers
  end
end
