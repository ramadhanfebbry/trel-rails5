class AddGenerateReportColumnToOwners < ActiveRecord::Migration[5.0]
  def change
    add_column :owners, :title, :string
    add_column :owners, :daily, :boolean, :default => false
    add_column :owners, :weekly, :boolean, :default => false
    add_column :owners, :monthly, :boolean, :default => false
    add_column :owners, :periodic, :boolean, :default => false
  end
end
