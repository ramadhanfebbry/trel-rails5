class ChangeColumnReferralNofifContent < ActiveRecord::Migration[5.0]
  def up
    change_column :referral_notifications, :email_content, :text
  end

  def down
    change_column :referral_notifications, :email_content, :string
  end
end
