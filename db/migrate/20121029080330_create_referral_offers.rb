class CreateReferralOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :referral_offers do |t|
      t.string :title
      t.integer :points
      t.integer :kind
      t.timestamps
    end

    add_index :referral_offers, :kind
  end
end
