class CreateReceiptFocusCheckUploads < ActiveRecord::Migration[5.0]
  def change
    create_table :receipt_focus_check_uploads do |t|
      t.integer :receipt_id
      t.integer :focus_check_upload_id
      t.timestamps
    end
    add_index :receipt_focus_check_uploads, :receipt_id
    add_index :receipt_focus_check_uploads, :focus_check_upload_id
  end
end
