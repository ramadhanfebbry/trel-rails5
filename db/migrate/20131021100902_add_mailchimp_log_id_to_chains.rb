class AddMailchimpLogIdToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :mailchimp_log_id, :integer
  end
end