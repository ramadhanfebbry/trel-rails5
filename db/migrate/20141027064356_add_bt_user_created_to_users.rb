class AddBtUserCreatedToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :braintree_user_created, :boolean, :default => false
  end
end
