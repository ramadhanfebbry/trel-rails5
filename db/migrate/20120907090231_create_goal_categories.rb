class CreateGoalCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :goal_categories do |t|
      t.integer :chain_id
      t.text :description

      t.timestamps
    end
  end
end
