class AddUseGeneratedCodeToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :use_generated_code, :boolean, :default => true
  end
end