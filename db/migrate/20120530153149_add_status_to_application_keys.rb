class AddStatusToApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :application_keys, :status, :string, :default => 'active'
    add_column :application_keys, :is_newest, :boolean, :default => false
  end
end
