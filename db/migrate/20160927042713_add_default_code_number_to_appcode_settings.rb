class AddDefaultCodeNumberToAppcodeSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :appcode_settings, :default_code_number, :string
  end
end
