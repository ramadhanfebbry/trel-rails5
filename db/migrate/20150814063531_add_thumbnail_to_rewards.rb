class AddThumbnailToRewards < ActiveRecord::Migration[5.0]
  def change
    add_column :rewards, :thumbnail_file_name, :string
    add_column :rewards, :thumbnail_file_size, :integer
    add_column :rewards, :thumbnail_content_type, :string
    add_column :rewards, :thumbnail_updated_at, :datetime
  end
end
