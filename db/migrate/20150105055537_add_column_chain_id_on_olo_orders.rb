class AddColumnChainIdOnOloOrders < ActiveRecord::Migration[5.0]
  def up
    add_column :olo_orders, :chain_id , :integer

    add_index :olo_orders, :chain_id
    add_index :olo_orders, [:chain_id, :batch_id]

  end

  def down
    remove_column :olo_orders, :chain_id , :integer
  end
end
