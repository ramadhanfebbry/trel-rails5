class AddUserSessionIdToPaymentHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :payment_histories, :user_session_id, :integer
    add_index :payment_histories, :user_session_id
    add_index :user_sessions, :receipt_id
  end
end
