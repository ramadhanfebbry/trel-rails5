class AddColumnOnSteppingstonestep < ActiveRecord::Migration[5.0]
  def up
    add_column :steppingstone_steps, :point_threshold, :integer
  end

  def down
    remove_column :steppingstone_steps, :point_threshold
  end
end
