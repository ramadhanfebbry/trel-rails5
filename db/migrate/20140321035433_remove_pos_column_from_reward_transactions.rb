class RemovePosColumnFromRewardTransactions < ActiveRecord::Migration[5.0]
  def up
    remove_column :reward_transactions, :check_id
    remove_column :reward_transactions, :seq_num
    remove_column :reward_transactions, :total_discount
  end

  def down
    add_column :reward_transactions, :check_id, :integer
    add_column :reward_transactions, :seq_num, :integer
    add_column :reward_transactions, :total_discount, :decimal
  end
end
