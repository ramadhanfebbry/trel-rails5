class CreateChainImageShares < ActiveRecord::Migration[5.0]
  def change
    create_table :chain_image_shares do |t|
      t.integer :chain_id
      t.string :title
      t.date :start_date, :expiry_date
      t.text :description
      t.timestamps
    end

    add_index(:chain_image_shares, :chain_id)
  end
end
