class AddUserMetadataToAppcodeSetting < ActiveRecord::Migration[5.0]
  def change
    add_column :appcode_settings, :user_metadata, :integer, default: 1
  end
end
