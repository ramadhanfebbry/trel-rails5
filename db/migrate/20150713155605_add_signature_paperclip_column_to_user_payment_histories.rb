class AddSignaturePaperclipColumnToUserPaymentHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :user_payment_histories, :signature_file_name, :string
    add_column :user_payment_histories, :signature_content_type, :string
    add_column :user_payment_histories, :signature_file_size, :integer
    add_column :user_payment_histories, :signature_updated_at, :datetime
  end
end
