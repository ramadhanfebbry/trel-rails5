class AddColumnIsPhoneEmailPushToReward < ActiveRecord::Migration[5.0]
  def change
    add_column :rewards, :is_push_phone, :boolean
    add_column :rewards, :is_push_email, :boolean
  end

  def self.down
    remove_column(:rewards, :is_push_phone, :is_push_email)
  end
end
