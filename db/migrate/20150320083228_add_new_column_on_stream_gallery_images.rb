class AddNewColumnOnStreamGalleryImages < ActiveRecord::Migration[5.0]
  def up
    add_column :stream_gallery_images, :stream_gallery_id, :integer
    add_column :stream_gallery_images, :chain_id, :integer
    add_column :stream_gallery_images, :input_source, :integer
  end

  def down
    remove_column :stream_gallery_images, :stream_gallery_id
    remove_column :stream_gallery_images, :chain_id
    remove_column :stream_gallery_images, :input_source
  end
end
