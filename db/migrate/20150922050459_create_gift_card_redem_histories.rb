class CreateGiftCardRedemHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :gift_card_redem_histories do |t|
      t.integer :user_id
      t.string :receipt_number
      t.float :amount
      t.string :transaction_id
      t.integer :chain_id
      t.timestamps
    end
    add_index :gift_card_redem_histories, :user_id
    add_index :gift_card_redem_histories, :receipt_number
    add_index :gift_card_redem_histories, :transaction_id
    add_index :gift_card_redem_histories, :chain_id
    add_index :gift_card_redem_histories, [:user_id, :transaction_id]
  end
end
