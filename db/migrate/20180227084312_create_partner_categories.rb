class CreatePartnerCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_categories do |t|
      t.integer :chain_id
      t.string :name
      t.text :description
      t.boolean :mandatory, default: false

      t.timestamps
    end

    add_index :partner_categories, :chain_id
  end
end
