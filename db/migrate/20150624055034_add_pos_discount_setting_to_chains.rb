class AddPosDiscountSettingToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :pos_discount_setting, :integer, :default => 1

  end
end
