class AddColumnRoleOnOwner < ActiveRecord::Migration[5.0]
  def up
    add_column :owners, :role, :string
  end

  def down
    remove_column :owners, :role, :string
  end
end
