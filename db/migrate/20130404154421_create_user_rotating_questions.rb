class CreateUserRotatingQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :user_rotating_questions do |t|
      t.integer :render_count
      t.integer :answer_id
      t.boolean :completed, :boolean => false
      t.string :survey_user_ids
      t.integer :user_id
      t.integer :question_id

      t.timestamps
    end
  end
end