class AddIndexOnSeveralTables < ActiveRecord::Migration[5.0]
  def up
    add_index :email_marketings, :user_id
    add_index :email_marketings, :chain_id
    add_index :email_marketings, :restaurant_id
    add_index :email_marketings, [:status, :chain_id]
    add_index :email_marketings, [:email, :chain_id]
    add_index :user_steps, [:user_id, :steppingstone_id]
    if table_exists?(:aoo_customers) && column_exists?(:aoo_customers, :user_id)
        add_index :aoo_customers, :user_id
    end
    
    add_index :levels_points, :user_id
    add_index :levels_points, [:game_id, :user_id]
    add_index :email_templates, [:chain_id, :locale_id, :template_name], :name => "ch_loc_name"
    add_index :user_tags, :user_id
    add_index :user_tags, :tag_id
    add_index :push_log_entries, [:pushable_id, :pushable_type]
    add_index :user_rotating_questions, [:question_id, :user_id]
    add_index :access_grants, :user_id
    add_index :access_grants, [:access_token, :access_token_expires_at]
    add_index :raffle_user_informations, :user_id
    add_index :raffle_user_informations, :user_email
    add_index :raffle_user_informations, :raffle_id
    if column_exists?(:receipts, :assignment_id)
        add_index :receipts, :assignment_id
    end
  end

  def down
    remove_index :email_marketings, :user_id
    remove_index :email_marketings, :chain_id
    remove_index :email_marketings, :restaurant_id
    remove_index :email_marketings, [:status, :chain_id]
    remove_index :email_marketings, [:email, :chain_id]
    remove_index :user_steps, [:user_id, :steppingstone_id]
    if column_exists?(:aoo_customers, :user_id)
        remove_index :aoo_customers, :user_id
    end
    
    remove_index :levels_points, :user_id
    remove_index :levels_points, [:game_id, :user_id]
    remove_index :email_templates, [:chain_id, :locale_id, :template_name]
    remove_index :user_tags, :user_id
    remove_index :user_tags, :tag_id
    remove_index :push_log_entries, [:pushable_id, :pushable_type]
    remove_index :user_rotating_questions, [:question_id, :user_id]
    remove_index :access_grants, :user_id
    remove_index :access_grants, [:access_token, :access_token_expires_at]
    remove_index :raffle_user_informations, :user_id
    remove_index :raffle_user_informations, :user_email
    remove_index :raffle_user_informations, :raffle_id
    if column_exists?(:receipts, :assignment_id)
        remove_index :receipts, :assignment_id
    end
  end
end
