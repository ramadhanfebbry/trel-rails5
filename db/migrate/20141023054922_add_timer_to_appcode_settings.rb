class AddTimerToAppcodeSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :appcode_settings, :timer, :integer, :default => 0
  end
end
