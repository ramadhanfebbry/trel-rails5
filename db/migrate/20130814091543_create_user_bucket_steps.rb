class CreateUserBucketSteps < ActiveRecord::Migration[5.0]
  def change
    create_table :user_bucket_steps do |t|
      t.integer :user_id
      t.integer :bucket_path_id
      t.integer :current_step, :default => 0
      t.integer :current_count, :default => 0
      t.integer :total_count, :default => 0

      t.timestamps
    end
  end
end