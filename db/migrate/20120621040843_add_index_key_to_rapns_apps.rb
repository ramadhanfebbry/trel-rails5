class AddIndexKeyToRapnsApps < ActiveRecord::Migration[5.0]
  def up
    add_index :rapns_apps, :key
  end

  def down
    remove_index :rapns_apps, :key
  end
end
