class CreateGeneralMenuItems < ActiveRecord::Migration[5.0]
  def change
    create_table :general_menu_items do |t|
      t.string :item_name
      t.text :description
      t.string :item_keywords
      t.integer :chain_id

      t.timestamps
    end
  end
end