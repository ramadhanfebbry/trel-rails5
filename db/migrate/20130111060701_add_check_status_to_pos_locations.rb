class AddCheckStatusToPosLocations < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_locations, :check_status, :boolean, :default => true
  end
end