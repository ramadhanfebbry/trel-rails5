class AddEmailContentHtmlToGamePointNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :game_point_notifications, :email_content_html, :text
    GamePointNotification.all.each do |gpn|
      gpn.update_column(:email_content_html, gpn.email_content)
    end
  end
end