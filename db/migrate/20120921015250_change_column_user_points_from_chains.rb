class ChangeColumnUserPointsFromChains < ActiveRecord::Migration[5.0]
  def up
    change_column :chains, :user_points, :float, :default => 1
  end

  def down
    change_column :chains, :user_points, :float
  end
end
