class CreateUserDeviceLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :user_device_logs do |t|
      t.integer :user_id
      t.string :keychain_value
      t.string :device_type
      t.string :access_type
      t.datetime :access_date_time

      t.timestamps
    end
  end
end
