class AddColumnEmailListType < ActiveRecord::Migration[5.0]
  def up
     add_column :olo_order_details, :issue_time  , :string
  end

  def down
    remove_column :olo_order_details, :issue_time
  end
end
