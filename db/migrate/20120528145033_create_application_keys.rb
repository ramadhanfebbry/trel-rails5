class CreateApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    create_table :application_keys do |t|
      t.string :appkey
      t.integer :application_id
      t.string :version

      t.timestamps
    end
  end
end
