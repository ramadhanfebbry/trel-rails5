class RemoveLocaleIdFromChains < ActiveRecord::Migration[5.0]
  def up
    remove_column :chains, :locale_id
  end

  def down
    add_column :chains, :locale_id, :integer
  end
end
