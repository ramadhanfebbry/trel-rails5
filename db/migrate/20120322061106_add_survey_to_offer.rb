class AddSurveyToOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :offers, :survey_id, :integer
  end
end
