class CreateHistoryNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :history_notifications do |t|
      t.integer :plain_id
      t.integer :user_id
      t.timestamps
    end

    add_index :history_notifications, :plain_id
    add_index :history_notifications, :user_id
    add_index :history_notifications, [:user_id, :plain_id]
  end
end
