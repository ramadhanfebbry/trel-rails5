class AddStatusToGiftCardRedeemHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :gift_card_redeem_histories, :status, :integer, :default => 0 if table_exists?(:gift_card_redeem_histories)
  end
end
