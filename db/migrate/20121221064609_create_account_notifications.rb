class CreateAccountNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :account_notifications do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.text :deactivation_confirmation_text
      t.text :reactivation_confirmation_text
      t.string :confirm_button_text
      t.string :cancel_button_text

      t.timestamps
    end
  end
end
