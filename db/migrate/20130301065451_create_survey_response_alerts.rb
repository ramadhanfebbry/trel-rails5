class CreateSurveyResponseAlerts < ActiveRecord::Migration[5.0]
  def change
    create_table :survey_response_alerts do |t|
      t.integer :chain_id
      t.boolean :send_email_alert, :default => false
      t.boolean :send_alert_for_negative_review, :default => false
      t.boolean :send_alert_for_comment, :default => false
      t.timestamps
    end
  end
end