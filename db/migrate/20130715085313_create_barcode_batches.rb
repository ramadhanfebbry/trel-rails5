class CreateBarcodeBatches < ActiveRecord::Migration[5.0]
  def change
    create_table :barcode_batches do |t|
      t.integer :pos_location_id
      t.string :barcode
      t.integer :size
      t.integer :status

      t.timestamps
    end
  end
end