class AddAppUsagePurposeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :app_usage_purpose, :string
  end
end
