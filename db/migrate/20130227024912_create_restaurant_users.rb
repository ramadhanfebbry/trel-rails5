class CreateRestaurantUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurant_users do |t|
      t.integer :restaurant_id
      t.integer :user_id

      t.timestamps
    end

    add_index :restaurant_users, :restaurant_id
    add_index :restaurant_users, :user_id
    add_index :restaurant_users, [:restaurant_id, :user_id]
  end
end
