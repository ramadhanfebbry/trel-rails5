class ChangeListIdFromFishbowlSettings < ActiveRecord::Migration[5.0]
  def up
    change_column :fishbowl_settings, :list_id, :string
  end

  def down
    change_column :fishbowl_settings, :list_id, :integer
  end
end