class CreateRaffles < ActiveRecord::Migration[5.0]
  def change
    create_table :raffles do |t|
      t.integer :chain_id
      t.string :title
      t.string :description
      t.datetime :start_date
      t.datetime :end_date
      t.timestamps
    end
  end
end
