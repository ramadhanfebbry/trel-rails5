class AddAdminIdToPushServiceNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :push_service_notifications, :admin_id, :integer
  end
end
