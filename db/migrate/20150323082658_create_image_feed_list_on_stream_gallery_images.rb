class CreateImageFeedListOnStreamGalleryImages < ActiveRecord::Migration[5.0]
  def change
    add_column :stream_gallery_images, :image_feed_lists, :text
  end
end
