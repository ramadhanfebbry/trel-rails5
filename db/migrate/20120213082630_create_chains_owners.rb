class CreateChainsOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :chains_owners do |t|
      t.references :chain
      t.references :owner
      t.timestamps
    end    
  end
end
