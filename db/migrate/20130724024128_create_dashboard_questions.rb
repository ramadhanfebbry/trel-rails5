class CreateDashboardQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :dashboard_questions do |t|
      t.string :text
      t.integer :order_number, :chain_id
      t.timestamps
    end

    add_index :dashboard_questions, :chain_id
  end
end
