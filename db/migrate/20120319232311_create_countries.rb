class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
      t.string :name
      t.string :abbreviation
      t.string :ISO

      t.timestamps
    end

    change_table :regions do |t|
      t.references :country
    end
  end
end
