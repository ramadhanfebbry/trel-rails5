class AddReportColumnToRestaurantsOwners < ActiveRecord::Migration[5.0]

  def change
    add_column :restaurants_owners, :daily, :boolean, :default => false
    add_column :restaurants_owners, :weekly, :boolean, :default => false
    add_column :restaurants_owners, :monthly, :boolean, :default => false
    add_column :restaurants_owners, :periodic, :boolean, :default => false
  end
  
end
