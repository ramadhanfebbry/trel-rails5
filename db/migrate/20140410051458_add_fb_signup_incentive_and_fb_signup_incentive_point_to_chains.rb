class AddFbSignupIncentiveAndFbSignupIncentivePointToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :fb_signup_incentive, :boolean, :default => false
    add_column :chains, :fb_signup_incentive_point, :decimal
  end
end
