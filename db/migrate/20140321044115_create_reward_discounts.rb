class CreateRewardDiscounts < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_discounts do |t|
      t.integer :reward_transaction_id
      t.integer :check_id
      t.integer :seq_num
      t.decimal :discount
      t.integer :restaurant_id

      t.timestamps
    end
  end
end
