class AddColumnFileNameOnRestaurantImportHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurant_import_histories, :file_name , :text

  end
end
