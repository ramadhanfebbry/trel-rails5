class CreatePushLogEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :push_log_entries do |t|
      t.string :pushable_type
      t.integer :pushable_id
      t.text :logs
      t.integer :log_type

      t.timestamps
    end
  end
end
