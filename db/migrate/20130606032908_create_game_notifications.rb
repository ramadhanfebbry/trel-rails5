class CreateGameNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :game_notifications do |t|
      t.integer :locale_id
      t.integer :chain_id
      t.text :notification

      t.timestamps
    end
  end
end