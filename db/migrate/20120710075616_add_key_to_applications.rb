class AddKeyToApplications < ActiveRecord::Migration[5.0]
  def change
    add_column :applications, :key, :string
  end
end
