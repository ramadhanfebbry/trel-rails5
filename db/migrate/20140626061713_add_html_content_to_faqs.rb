class AddHtmlContentToFaqs < ActiveRecord::Migration[5.0]
  def change
    add_column :faqs, :generated_html_content, :text
  end
end
