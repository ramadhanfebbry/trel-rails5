class AddColumnHoursDelayOnChains < ActiveRecord::Migration[5.0]
  def up
    add_column :chains, :hours_delay_reward, :integer, :default => 0
  end

  def down
    remove_column(:chains, :hours_delay_reward)
  end
end
