class MakeRaffleuserinfromationToParrent < ActiveRecord::Migration[5.0]
  def change
  	remove_column :raffle_participation_histories, :raffle_id
    remove_column :raffle_participation_histories, :user_id

    add_column :raffle_participation_histories, :raffle_user_information_id, :integer
  end
end
