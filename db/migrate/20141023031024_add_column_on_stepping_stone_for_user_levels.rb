class AddColumnOnSteppingStoneForUserLevels < ActiveRecord::Migration[5.0]
  def up
    add_column :steppingstones,:randomize, :boolean , :default => false
    add_column :steppingstones,:loop_consideration, :boolean , :default => false
    add_column :steppingstones, :category_type,:integer
    add_column :steppingstones, :min_amount, :float
    add_column :steppingstones, :max_amount, :float

  end

  def down
    remove_column :steppingstones, :randomize, :loop_consideration, :category_type
  end
end
