class CreateUserDealSurveys < ActiveRecord::Migration[5.0]
  def change
    remove_column(:surveys_users, :type)
    create_table :user_deal_surveys, :force => true do |t|
      t.integer :deal_id, :survey_id, :survey_user_id, :user_id
      t.timestamps
    end
  end
end
