class AddColumnPlainPushNotif < ActiveRecord::Migration[5.0]
  def up
    add_column :plain_push_notifications,:admin_id, :integer
  end

  def down
    remove_column :plain_push_notifications,:admin_id, :integer
  end
end
