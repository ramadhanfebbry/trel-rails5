class AddApplicationIdToRapnsApps < ActiveRecord::Migration[5.0]
  def change
    add_column :rapns_apps, :application_id, :integer
  end
end
