class CreateOloWebhooks < ActiveRecord::Migration[5.0]
  def change
    create_table :olo_webhooks do |t|
      t.integer :chain_id
      t.text :content

      t.timestamps
    end
  end
end
