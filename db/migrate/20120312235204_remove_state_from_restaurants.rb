class RemoveStateFromRestaurants < ActiveRecord::Migration[5.0]
  def change
    remove_column :restaurants, :state
    remove_column :restaurants, :city
    remove_index :restaurants, :state_id
    remove_column :restaurants, :state_id
  end

end
