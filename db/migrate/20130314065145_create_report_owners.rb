class CreateReportOwners < ActiveRecord::Migration[5.0]
  def change
    create_table :report_owners do |t|
      t.integer :owner_id, :subscription_id
      t.timestamps
    end

    add_index :report_owners, :owner_id
    add_index :report_owners, :subscription_id
  end
end
