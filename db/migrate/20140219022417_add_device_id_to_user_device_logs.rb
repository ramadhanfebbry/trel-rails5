class AddDeviceIdToUserDeviceLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :user_device_logs, :device_id, :string
  end
end
