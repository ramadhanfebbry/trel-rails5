class CreateOnosysConnectSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :onosys_connect_settings do |t|
      t.string :api_root
      t.string :api_key
      t.integer :chain_id
      t.timestamps
    end
  end
end
