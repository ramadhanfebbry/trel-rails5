class CreateAdminReportDefinitions < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_report_definitions do |t|
      t.string :name
      t.text :jasper_path
      t.integer :report_kind
      t.text :query
      t.date :start_at
      t.date :end_at

      t.timestamps
    end
  end
end
