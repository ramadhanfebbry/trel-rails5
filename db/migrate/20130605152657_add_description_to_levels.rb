class AddDescriptionToLevels < ActiveRecord::Migration[5.0]
  def change
    add_column :levels, :description, :text
  end
end