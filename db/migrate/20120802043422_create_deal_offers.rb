class CreateDealOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :deal_offers, :force => true do |t|
      t.string :title
      t.integer :kind, :points, :multiplier
      t.text :fine_print
      t.timestamps
    end
  end
end
