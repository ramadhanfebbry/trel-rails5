class ChangeColumnOrderNumer < ActiveRecord::Migration[5.0]
  def up
    change_column :online_parse_results, :order_id , :string
  end

  def down
    change_column :online_parse_results, :order_id , :integer
  end
end
