class AddOldBarcodeToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :old_barcode, :string
    add_index :pos_check_uploads, :old_barcode
  end
end
