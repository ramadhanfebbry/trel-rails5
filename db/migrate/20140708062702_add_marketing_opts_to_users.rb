class AddMarketingOptsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :marketing_optin, :boolean, :default => false
  end
end
