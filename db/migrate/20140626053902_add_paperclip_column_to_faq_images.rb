class AddPaperclipColumnToFaqImages < ActiveRecord::Migration[5.0]
  def change
    remove_column :faqimages, :url
    add_column :faqimages, :image_file_name, :string
    add_column :faqimages, :image_file_size, :integer
    add_column :faqimages, :image_content_type, :string
    add_column :faqimages, :image_updated_at, :datetime
  end
end
