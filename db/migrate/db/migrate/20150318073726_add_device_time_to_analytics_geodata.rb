class AddDeviceTimeToAnalyticsGeodata < ActiveRecord::Migration[5.0]
  def change
    add_column :analytics_geodata, :device_time, :string

  end
end