class AddApplicationIdToReceipt < ActiveRecord::Migration[5.0]
  def change
    add_column :receipts, :application_id, :integer
  end
end
