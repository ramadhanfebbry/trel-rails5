class AddRevelFtpToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :revel_to_relevant_ftp_url, :string
    add_column :chains, :revel_to_relevant_ftp_port, :integer
    add_column :chains, :revel_to_relevant_ftp_username, :string
    add_column :chains, :revel_to_relevant_ftp_password, :string

    add_column :chains, :relevant_to_revel_ftp_url, :string
    add_column :chains, :relevant_to_revel_ftp_port, :integer
    add_column :chains, :relevant_to_revel_ftp_username, :string
    add_column :chains, :relevant_to_revel_ftp_password, :string
  end
end
