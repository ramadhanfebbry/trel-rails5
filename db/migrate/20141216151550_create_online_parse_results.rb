class CreateOnlineParseResults < ActiveRecord::Migration[5.0]
  def change
    create_table :online_parse_results do |t|
      t.integer :online_order_id, :order_id,:user_id, :chain_id, :restaurant_id
      t.string :location, :order_no, :customer_no, :restaurant , :chain, :email
      t.float :subtotal
      t.float :total
      t.float :tax

      t.date :date_pickup, :date_placed

      t.timestamps
    end

    add_index :online_parse_results, [:online_order_id, :chain_id]
  end
end
