class AddColumnActiveOnCountryRegionsCities < ActiveRecord::Migration[5.0]
  def up
    add_column :countries, :active, :boolean, :default => true
    add_column :regions, :active, :boolean, :default => true
    add_column :cities, :active, :boolean, :default => true
    ## cache counter for each tables
    add_column :countries, :regions_count, :integer, :default => 0
    add_column :regions, :cities_count, :integer, :default => 0
  end

  def down
    remove_column :countries, :active, :regions_count
    remove_column :regions, :active, :cities_count
    remove_column :cities, :active
  end
end
