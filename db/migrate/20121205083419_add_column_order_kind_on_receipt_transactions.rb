class AddColumnOrderKindOnReceiptTransactions < ActiveRecord::Migration[5.0]
  def up
    add_column :receipts, :order_kind , :string
  end

  def down
    remove_column :receipts, :order_kind
  end
end
