class AddNoScan7DaysMailingIdWeMissUMailingIdToFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :no_scans_7_days_mailing_id, :string
    add_column :fishbowl_settings, :we_miss_u_mailing_id, :string
  end
end
