class AddColumnForNetPosReward < ActiveRecord::Migration[5.0]
  def up
    add_column :rewards,:adj_type, :string
    add_column :rewards,:adj_value, :float, :default => 0.0
  end

  def down
    remove_column :rewards, :adj_type, :adj_value
  end
end
