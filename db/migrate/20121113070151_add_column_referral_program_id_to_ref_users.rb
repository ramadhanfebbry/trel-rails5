class AddColumnReferralProgramIdToRefUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :referral_users, :referral_program_id, :integer
  end
end
