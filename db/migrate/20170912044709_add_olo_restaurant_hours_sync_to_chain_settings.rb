class AddOloRestaurantHoursSyncToChainSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :chain_settings, :olo_restaurant_hour_sync, :boolean, :default => false

  end
end
