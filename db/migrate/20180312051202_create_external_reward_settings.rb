class CreateExternalRewardSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :external_settings do |t|
      t.integer :chain_id
      t.string :basic_auth
      t.string :basic_pwd

      t.timestamps
    end
  end
end
