class AddDelayToUserActivityNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :user_activity_notifications, :delay_time, :integer, :default => 0

  end
end
