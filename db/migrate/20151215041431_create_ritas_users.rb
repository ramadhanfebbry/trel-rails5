class CreateRitasUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :ritas_users do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.datetime :member_sign_up_date
      t.decimal :number_of_punches
      t.boolean :migrated, :default => false
      t.timestamps
    end
    add_index :ritas_users, :email
  end
end
