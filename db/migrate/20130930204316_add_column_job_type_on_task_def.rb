class AddColumnJobTypeOnTaskDef < ActiveRecord::Migration[5.0]
  def up
    add_column :task_definitions,:notification , :boolean , :default => true
  end

  def down
    remove_column :task_definitions,:notification , :boolean , :default => true
  end
end
