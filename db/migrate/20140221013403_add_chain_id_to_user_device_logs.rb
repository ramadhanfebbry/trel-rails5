class AddChainIdToUserDeviceLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :user_device_logs, :chain_id, :integer

    add_index :user_device_logs, :chain_id
    add_index :user_device_logs, :user_id
    add_index :user_device_logs, :keychain_value
    add_index :user_device_logs, [:chain_id, :user_id]
    add_index :user_device_logs, :access_type
    add_index :user_device_logs, [:chain_id, :user_id, :keychain_value], :name => 'user_device_log_cukey_index'
    add_index :user_device_logs, [:chain_id, :keychain_value, :access_type], :name => 'user_device_log_cukeyacctype_index'

    UserDeviceLog.all.each do |device|
    	user = device.user
    	device.update_column(:chain_id, user.chain_id) if user
    end	
  end
end
