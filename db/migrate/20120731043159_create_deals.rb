class CreateDeals < ActiveRecord::Migration[5.0]
  def change
    create_table :deals, :force => true do |t|
      t.integer :chain_id
      t.string :title
      t.integer :deal_type
      t.integer :max_winner 
      t.date :start_date
      t.date :end_date
      t.integer :restaurant_id
      t.integer :limit_user_per_day
      t.integer :max_user
      t.string :activity_type
      t.integer :activity_id
      t.timestamps
    end
    add_index :deals, :chain_id
    add_index :deals, :restaurant_id
    add_index :deals, [:activity_type, :activity_id]
  end
end
