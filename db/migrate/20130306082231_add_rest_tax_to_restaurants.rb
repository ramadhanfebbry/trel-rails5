class AddRestTaxToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :rest_tax, :float, :default => 0
  end
end