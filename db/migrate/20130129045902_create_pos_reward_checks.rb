class CreatePosRewardChecks < ActiveRecord::Migration[5.0]
  def change
    create_table :pos_reward_checks do |t|
      t.integer :pos_location_id
      t.integer :chain_id
      t.text :xml_data

      t.timestamps
    end
  end
end