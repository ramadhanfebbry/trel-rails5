class AddColumnStatusOnRestaurants < ActiveRecord::Migration[5.0]
  def up
    unless column_exists? :restaurants, :status
      add_column :restaurants, :status, :boolean, :default => true
    end
  end

  def down
    remove_column :restaurants, :status
  end
end
