class AddUserRewardRedeemptionFlowToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :user_reward_redeemption_flow, :integer, :default => 1
  end
end
