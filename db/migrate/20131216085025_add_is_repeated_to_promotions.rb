class AddIsRepeatedToPromotions < ActiveRecord::Migration[5.0]
  def change
    add_column :promotions, :is_repeated, :boolean, :default => false
    add_column :promotions, :duration, :integer, :default => 0
  end
end
