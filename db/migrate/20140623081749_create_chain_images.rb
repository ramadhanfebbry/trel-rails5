class CreateChainImages < ActiveRecord::Migration[5.0]
  def change
    create_table :faqimages do |t|
      t.integer :chain_id
      t.text 	:url
      t.text 	:description

      t.timestamps
    end
  end
end
