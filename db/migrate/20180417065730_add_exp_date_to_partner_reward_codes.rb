class AddExpDateToPartnerRewardCodes < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_reward_codes, :expiration_date, :datetime
  end
end
