class AddColumnQuantityOnPromotions < ActiveRecord::Migration[5.0]
  def up
    add_column :promotions, :quantity, :integer
  end

  def down
    remove_column(:promotions, :quantity)
  end
end
