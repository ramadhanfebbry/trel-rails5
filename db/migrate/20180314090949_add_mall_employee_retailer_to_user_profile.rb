class AddMallEmployeeRetailerToUserProfile < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :mall_employee, :boolean
    add_column :user_profiles, :retailer, :string
  end
end
