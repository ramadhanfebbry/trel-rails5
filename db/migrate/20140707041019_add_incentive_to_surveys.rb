class AddIncentiveToSurveys < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :point, :integer
    add_column :surveys, :reward_id, :integer
  end
end
