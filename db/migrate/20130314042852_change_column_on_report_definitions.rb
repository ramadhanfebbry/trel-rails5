class ChangeColumnOnReportDefinitions < ActiveRecord::Migration[5.0]
  def up
    add_column :report_definitions,:sending_method, :integer
    add_column :report_definitions,:asynchronous, :boolean, :default => false
  end

  def down
    remove_column :report_definitions, :sending_method, :asynchronous
  end
end
