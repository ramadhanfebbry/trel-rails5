class AddColumnScoresOnSurveyUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :surveys_users, :scores, :float
  end

  def down
    remove_column(:surveys_users, :scores)
  end
end
