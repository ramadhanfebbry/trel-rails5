class AddLowerIndexLocationQrcodeIdentifier < ActiveRecord::Migration[5.0]
  def up
    execute "create index idx_upper_qrcode_identifier_restaurants on restaurants(upper(location_qrcode_identifier));"
  end

  def down
    execute "drop index idx_upper_qrcode_identifier_restaurants"
  end
end
