class CreateTableOfferRuleMenuItems < ActiveRecord::Migration[5.0]
  def up
    create_table :offer_rule_menu_items do |t|
      t.integer :chain_id
      t.integer :offer_rule_id
      t.integer :general_menu_item_id
      t.boolean :required , :default => false
      t.timestamps
    end

    add_index :offer_rule_menu_items, :offer_rule_id, :name => 'offer_rule_index_id'
    add_index :offer_rule_menu_items, [:offer_rule_id, :general_menu_item_id] , :name => 'offer_general_offer_index'
  end

  def down
    drop_table :offer_rule_menu_items
  end
end
