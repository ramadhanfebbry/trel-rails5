class CreateRemovedRegids < ActiveRecord::Migration[5.0]
  def change
    create_table :removed_regids, :force => true do |t|
      t.integer :user_id
      t.string :reg_id

      t.timestamps
    end

    add_index :removed_regids, :user_id
    add_index :removed_regids, :reg_id
  end
end
