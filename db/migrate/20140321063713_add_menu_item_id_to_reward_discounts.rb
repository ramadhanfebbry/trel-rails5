class AddMenuItemIdToRewardDiscounts < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_discounts, :menu_item_id, :integer
  end
end
