class CreateRewardInformations < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_informations do |t|
      t.integer :reward_id
      t.string :title
      t.text :description
      t.string :app_text
      t.timestamps
    end

    add_index :reward_informations, :reward_id
  end
end
