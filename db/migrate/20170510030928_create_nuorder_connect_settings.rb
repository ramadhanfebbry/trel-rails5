class CreateNuorderConnectSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :nuorder_connect_settings do |t|
      t.string :api_root
      t.string :requester_id
      t.string :license_code
      t.string :passcode
      t.string :subscriber_id
      t.string :location_id
      t.integer :chain_id
      t.timestamps
    end
  end
end
