class CreateOloOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :olo_orders do |t|
      t.integer :batch_id
      t.text :xml_data
      t.timestamps
    end
  end
end
