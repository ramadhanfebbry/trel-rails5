class AddUniqueIndexOnSurveysUserTables < ActiveRecord::Migration[5.0]
  def up
    ### removing duplicate data
    sur = SurveysUser.find_by_sql('SELECT  receipt_id,COUNT(receipt_id) AS offer_id FROM surveys_users GROUP BY receipt_id HAVING ( COUNT(receipt_id) > 1 )')
    sur.each do |x|
      x.offer_id.to_i.downto(2).each do |m|
        puts "#{x.receipt_id}"
        puts "removing #{x.receipt_id} for duplicate"
        SurveysUser.find_by_receipt_id(x.receipt_id).delete
    end
  end

  remove_index :surveys_users, :receipt_id
  add_index :surveys_users, :receipt_id, :unique => true
end

def down
  remove_index :surveys_users, :receipt_id
end

end
