class AddCodeTypeToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :code_type, :string
  end
end
