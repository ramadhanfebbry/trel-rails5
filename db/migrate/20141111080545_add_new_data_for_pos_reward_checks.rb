class AddNewDataForPosRewardChecks < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_reward_checks, :user_id, :integer
    add_column :pos_reward_checks, :reward_code, :string
    add_column :pos_reward_checks, :status, :integer
    add_column :pos_reward_checks, :error_description, :text

    add_index :pos_reward_checks, :user_id
    add_index :pos_reward_checks, :reward_code
    add_index :pos_reward_checks, :status
  end

end
