class AddFtpPathDirectoryAtFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :ftp_path_directory, :string
  end
end
