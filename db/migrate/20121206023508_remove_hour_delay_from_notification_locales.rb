class RemoveHourDelayFromNotificationLocales < ActiveRecord::Migration[5.0]
  def up
    remove_column :notification_locales, :hours_delay
  end

  def down
    add_column :notification_locales, :hours_delay, :float, :default => 0
  end
end
