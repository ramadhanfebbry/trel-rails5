class CreateOwnerSchedules < ActiveRecord::Migration[5.0]
  def change
    create_table :owner_schedulers do |t|
      t.integer :job_id#,:schedule_type
      t.string :run_type
      t.datetime :start_at
      t.datetime :executed_at
      t.datetime :last_executed
      t.datetime :next_run
      t.datetime :end_at
      t.timestamps
    end

    add_index :owner_schedulers, :job_id
    #add_index :owner_schedulers, :schedule_type
    add_column :owner_jobs, :schedule_type , :integer
  end
end
