class AddColumnOnReportDefinitionsForEmailType < ActiveRecord::Migration[5.0]
  def up
    add_column :report_definitions, :is_email_content, :boolean, :default => false
  end

  def down
    remove_column :report_definitions, :is_email_content
  end
end
