class CreateUserCheckins < ActiveRecord::Migration[5.0]
  def change
    create_table :user_checkins, :force => true do |t|
      t.integer :user_id
      t.integer :deal_id
      t.integer :restaurant_id
      t.datetime :deleted_at
      t.float :user_latitude
      t.float :user_longitude

      t.timestamps
    end
  end
end
