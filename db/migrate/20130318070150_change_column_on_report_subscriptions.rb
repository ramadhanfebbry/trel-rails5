class ChangeColumnOnReportSubscriptions < ActiveRecord::Migration[5.0]
  def up
    rename_column :report_subscriptions, :report_description_id, :report_definition_id
  end

  def down
    rename_column :report_subscriptions, :report_definition_id, :report_description_id
  end
end
