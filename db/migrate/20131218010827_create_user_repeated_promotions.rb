class CreateUserRepeatedPromotions < ActiveRecord::Migration[5.0]

  def change
    create_table :user_repeated_promotions do |t|
      t.integer :user_id
      t.integer :promotion_id
      t.date :applied_at
      t.boolean :expired, :default => false

      t.timestamps
    end
  end
end
