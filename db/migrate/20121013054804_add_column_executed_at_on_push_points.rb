class AddColumnExecutedAtOnPushPoints < ActiveRecord::Migration[5.0]
  def up
    add_column :push_points, :executed_at, :datetime
  end

  def down
    remove_column :push_points, :executed_at, :datetime
  end
end
