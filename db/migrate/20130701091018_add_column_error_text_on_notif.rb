class AddColumnErrorTextOnNotif < ActiveRecord::Migration[5.0]
  def up
    add_column :history_notifications, :error_text, :text
  end

  def down
    remove_column :history_notifications,  :error_text, :text
  end
end
