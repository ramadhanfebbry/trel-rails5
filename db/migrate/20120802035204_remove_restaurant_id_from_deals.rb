class RemoveRestaurantIdFromDeals < ActiveRecord::Migration[5.0]
  def up
    remove_column :deals, :restaurant_id
  end

  def down
    add_column :deals, :restaurant_id, :integer
  end
end
