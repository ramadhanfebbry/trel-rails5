class AddRestaurantRewardToRewardTransaction < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_transactions, :restaurant_reward_id, :integer
    add_index :reward_transactions, :restaurant_reward_id
  end
end
