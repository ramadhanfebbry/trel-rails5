class CreateCheckins < ActiveRecord::Migration[5.0]
  def change
    create_table :checkins, :force => true do |t|
      t.float :minimum_distance
      t.text :fine_print

      t.timestamps
    end
  end
end
