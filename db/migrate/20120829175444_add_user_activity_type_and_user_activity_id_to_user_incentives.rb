class AddUserActivityTypeAndUserActivityIdToUserIncentives < ActiveRecord::Migration[5.0]
  def change
    add_column :user_incentives, :user_activity_id, :integer
    add_column :user_incentives, :user_activity_type, :string
  end
end
