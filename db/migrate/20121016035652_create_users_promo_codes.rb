class CreateUsersPromoCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :users_promo_codes do |t|
      t.integer :user_id
      t.integer :promo_code_id
      t.timestamps
    end
    add_index :users_promo_codes, :user_id
    add_index :users_promo_codes, :promo_code_id
    remove_column :promo_codes, :used_by
  end
end
