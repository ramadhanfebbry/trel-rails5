class ChangeDataTypeForWelcomeAndRewardMailingFishbowl < ActiveRecord::Migration[5.0]
  def up
    change_column :fishbowl_settings, :welcome_email_mailing_id, :string
    change_column :fishbowl_settings, :reward_redeem_mailing_id, :string
  end

  def down
    change_column :fishbowl_settings, :welcome_email_mailing_id, :integer
    change_column :fishbowl_settings, :reward_redeem_mailing_id, :integer
  end
end