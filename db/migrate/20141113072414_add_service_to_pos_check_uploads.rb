class AddServiceToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :service, :integer
  end
end
