class AddWeekGeneratedAtToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :week_generated_at, :datetime
  end
end
