class AddColumnDefinitionAndOwnerJob < ActiveRecord::Migration[5.0]
  def up
    add_column :task_definitions, :zone, :string
    add_column :owner_jobs, :zone, :string
  end

  def down
    remove_column :task_definitions, :zone
    remove_column :owner_jobs, :zone
  end
end
