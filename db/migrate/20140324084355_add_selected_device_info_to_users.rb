class AddSelectedDeviceInfoToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :selected_device_info, :string
  end
end
