class AddLastSentNoticeAtToApplications < ActiveRecord::Migration[5.0]
  def change
    add_column :applications, :last_sent_notice, :datetime
  end
end
