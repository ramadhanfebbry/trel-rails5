class AddNotificationUrlToPushServiceNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :push_service_notifications, :notification_url, :string
  end
end
