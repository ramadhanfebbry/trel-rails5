class CreateTableAvgVisit < ActiveRecord::Migration[5.0]
  def change
    create_table :avg_visits do |t|
      t.integer :user_id, :restaurant_id
      t.float :average
      t.timestamps
    end

    add_index(:avg_visits, :user_id)
    add_index(:avg_visits, :restaurant_id)
  end
end
