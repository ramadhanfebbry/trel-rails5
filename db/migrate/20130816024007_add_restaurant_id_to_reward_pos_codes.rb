class AddRestaurantIdToRewardPosCodes < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_pos_codes, :restaurant_id, :integer
  end
end