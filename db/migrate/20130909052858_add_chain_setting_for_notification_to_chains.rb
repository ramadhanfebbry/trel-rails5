class AddChainSettingForNotificationToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :general_setting_push_to_email, :boolean, :default => true
    add_column :chains, :general_setting_push_to_device, :boolean, :default => true
  end
end
