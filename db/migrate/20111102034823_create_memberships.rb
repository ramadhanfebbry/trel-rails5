class CreateMemberships < ActiveRecord::Migration[5.0]
  def change
    create_table :memberships do |t|
      t.references :user
      t.references :chain
      t.boolean :firstTime, :default => false
      t.integer :pointBalance, :default => 0
      t.integer :pointsLifetime
      t.decimal :cashLifetime
      
      t.timestamps
    end
    
  end
end
