class CreateRestaurantImportHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurant_import_histories do |t|
      t.integer :delayed_job_id
      t.string :status
      t.text :success_restaurants
      t.text :failed_restaurants
      t.integer :chain_id
      t.string :log_type

      t.timestamps
    end
  end
end