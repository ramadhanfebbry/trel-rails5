class AddTotalDiscountAndSeqNumToRewardTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_transactions, :seq_num, :integer
    add_column :reward_transactions, :total_discount, :decimal
  end
end
