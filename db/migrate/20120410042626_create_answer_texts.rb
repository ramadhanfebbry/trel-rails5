class CreateAnswerTexts < ActiveRecord::Migration[5.0]
  def change
    create_table :answer_texts do |t|
      t.string :text
      t.references :answer

      t.timestamps
    end
  end
end
