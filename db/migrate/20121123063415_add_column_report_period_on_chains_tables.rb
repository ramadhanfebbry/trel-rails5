class AddColumnReportPeriodOnChainsTables < ActiveRecord::Migration[5.0]
  def up
    add_column :chains, :report_period, :integer
    add_column :chains, :report_start_date , :date
    add_column :chains, :report_end_date , :date
  end

  def down
    remove_column(:chains, :report_period, :report_start_date, :report_end_date)
  end
end
