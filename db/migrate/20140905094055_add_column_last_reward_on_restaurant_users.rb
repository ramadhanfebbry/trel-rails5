class AddColumnLastRewardOnRestaurantUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :restaurant_users,:last_redeem,:date
    add_index :restaurant_users, [:restaurant_id, :miss_you_date, :last_redeem], :name => "miss_you_task_index"
  end

  def down
    remove_column :restaurant_users,:last_redeem
  end
end
