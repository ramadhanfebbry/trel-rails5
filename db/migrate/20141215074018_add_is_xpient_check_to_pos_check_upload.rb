class AddIsXpientCheckToPosCheckUpload < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :is_xpient_check, :boolean, :default => false
    add_index :pos_check_uploads, :is_xpient_check

  end
end
