class CreatePaymentHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_histories do |t|
      t.integer :mp_payment_id
      t.integer :user_id
      t.integer :restaurant_id
      t.float :amount
      t.integer :receipt_id
      t.string :pay_code
      t.integer :chain_id

      t.timestamps
    end

    add_index :payment_histories, :user_id
    add_index :payment_histories, :restaurant_id
    add_index :payment_histories, :receipt_id
    add_index :payment_histories, :chain_id
    add_index :payment_histories, [:chain_id, :user_id]
    add_index :payment_histories, [:user_id, :restaurant_id]
    add_index :payment_histories, [:user_id, :receipt_id]
    add_index :payment_histories, [:receipt_id, :restaurant_id]
  end
end
