class CreateRewards < ActiveRecord::Migration[5.0]
  def change
    create_table :rewards do |t|
      t.string :name
      t.integer :points
      t.text :fineprint
      t.datetime :effectiveDate
      t.datetime :expiryDate
      t.boolean :isActive
      t.string :POSCode
      t.references :chain
      
      t.timestamps
    end
  end
end
