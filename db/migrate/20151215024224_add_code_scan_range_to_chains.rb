class AddCodeScanRangeToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :code_scan_range, :decimal, :default => 0.5
  end
end
