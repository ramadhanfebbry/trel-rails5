class CreateOwnerjobLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :ownerjob_logs do |t|
      t.integer :owner_job_id
      t.integer :status
      t.integer :user_id
      t.string :email
      t.date :date_sent
      t.integer :success_notification
      #{:id=>181623, :email=>"kennynmd73@aol.com", :date_sent=>"07/25/14", :success_notif=>"success"}, {:id=>274114, :email=>"lonestar_lady@comcast.net", :date_sent=>"07/25/14"}
      t.timestamps
    end

    add_index :ownerjob_logs, :owner_job_id
    add_index :ownerjob_logs, [:owner_job_id,:status]
    add_index :ownerjob_logs, :user_id
  end
end
