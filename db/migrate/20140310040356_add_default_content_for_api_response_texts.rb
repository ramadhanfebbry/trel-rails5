class AddDefaultContentForApiResponseTexts < ActiveRecord::Migration[5.0]
  def up
  	ApiResponseText.destroy_all
  	Chain.all.each do |chain|
  		chain.locales.each do |locale|
  			ApiResponseText.create(:chain_id => chain.id, :locale_id => locale.id, 
  				:message => ApiResponseText::DEFAULT_MESSAGE[locale.key]["Receipt Submission Success"],
  				:message_for => ApiResponseText::MESSAGE_FOR["Receipt Submission Success"])
  			ApiResponseText.create(:chain_id => chain.id, :locale_id => locale.id, 
  				:message => ApiResponseText::DEFAULT_MESSAGE[locale.key]["Receipt Submission Fail"],
  				:message_for => ApiResponseText::MESSAGE_FOR["Receipt Submission Fail"])
  			ApiResponseText.create(:chain_id => chain.id, :locale_id => locale.id, 
  				:message => ApiResponseText::DEFAULT_MESSAGE[locale.key]["Signup Success With Referral Code"],
  				:message_for => ApiResponseText::MESSAGE_FOR["Signup Success With Referral Code"])
  			ApiResponseText.create(:chain_id => chain.id, :locale_id => locale.id, 
  				:message => ApiResponseText::DEFAULT_MESSAGE[locale.key]["Signup Success Without Referral Code"],
  				:message_for => ApiResponseText::MESSAGE_FOR["Signup Success Without Referral Code"])
  			ApiResponseText.create(:chain_id => chain.id, :locale_id => locale.id, 
  				:message => ApiResponseText::DEFAULT_MESSAGE[locale.key]["Survey Submission Success"],
  				:message_for => ApiResponseText::MESSAGE_FOR["Survey Submission Success"])
  			ApiResponseText.create(:chain_id => chain.id, :locale_id => locale.id, 
  				:message => ApiResponseText::DEFAULT_MESSAGE[locale.key]["Survey Submission Fail"],
  				:message_for => ApiResponseText::MESSAGE_FOR["Survey Submission Fail"])
  			ApiResponseText.create(:chain_id => chain.id, :locale_id => locale.id, 
  				:message => ApiResponseText::DEFAULT_MESSAGE[locale.key]["Barcode Scanned Successfully"],
  				:message_for => ApiResponseText::MESSAGE_FOR["Barcode Scanned Successfully"])
  		end	
  	end	
  end

  def down
  end
end
