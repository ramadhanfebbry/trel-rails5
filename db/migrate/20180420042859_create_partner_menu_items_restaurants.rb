class CreatePartnerMenuItemsRestaurants < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_menu_items_restaurants do |t|
      t.integer :partner_menu_item_id
      t.integer :restaurant_id
      t.timestamps
    end
    add_index :partner_menu_items_restaurants, :partner_menu_item_id
    add_index :partner_menu_items_restaurants, :restaurant_id
  end
end
