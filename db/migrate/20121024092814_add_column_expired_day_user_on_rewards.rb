class AddColumnExpiredDayUserOnRewards < ActiveRecord::Migration[5.0]
  def up
    add_column :rewards, :expired_day_user, :integer , :default => 0
  end

  def down
    remove_column(:rewards, :expired_day_user)
  end
end
