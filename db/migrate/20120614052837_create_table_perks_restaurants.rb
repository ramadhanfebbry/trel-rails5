class CreateTablePerksRestaurants < ActiveRecord::Migration[5.0]
  def up
    create_table :perks_restaurants, :id => false do |t|
        t.references :perk
        t.references :restaurant
    end
    add_index :perks_restaurants, [:restaurant_id, :perk_id]
    add_index :perks_restaurants, [:perk_id, :restaurant_id]
  end

  def down
    drop_table :perks_restaurants
  end

end
