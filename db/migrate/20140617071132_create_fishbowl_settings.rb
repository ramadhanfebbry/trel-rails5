class CreateFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :fishbowl_settings do |t|
      t.integer :chain_id
      t.string :api_username
      t.string :api_password
      t.integer :list_id
      t.integer :site_id
      t.timestamps
    end
  end
end
