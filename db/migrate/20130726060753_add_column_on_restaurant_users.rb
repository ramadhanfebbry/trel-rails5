class AddColumnOnRestaurantUsers < ActiveRecord::Migration[5.0]
  def up
    if !ActiveRecord::Base.connection.tables.include?('restaurant_users')
      create_table :restaurant_users do |t|
        t.integer :restaurant_id, :user_id
        t.date :joined_date, :last_active
        t.float :total_transaction
        t.timestamps
      end
    else
      add_column :restaurant_users, :last_active, :datetime
      add_column :restaurant_users, :total_transaction, :float
    end
  end

  def down
    remove_column :restaurant_users, :last_active
    remove_column :restaurant_users, :total_transaction
  end

end
