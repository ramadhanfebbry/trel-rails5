class AddInitiateAtAndCompletedAtToMailchimpLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :mailchimp_logs, :initiate_at, :datetime
    add_column :mailchimp_logs, :completed_at, :datetime
    add_column :mailchimp_logs, :error_report, :text
  end
end