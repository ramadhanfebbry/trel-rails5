class AddIsActiveToGiftcardSkins < ActiveRecord::Migration[5.0]
  def change
    add_column :giftcard_skins, :is_active, :boolean, default: false
  end
end
