class RemoveRestaurantBackupsTableAndColumn < ActiveRecord::Migration[5.0]
  def up
    drop_table :restaurant_backups if table_exists?(:restaurant_backups)
    drop_table :restaurant_detail_backups if table_exists?(:restaurant_detail_backups)
    remove_column :restaurants, :backups if column_exists?(:restaurants, :backups)
  end

  def down
    create_table 'restaurant_backups', :force => true do |t|
      t.string   'name'
      t.string   'app_display_text'
      t.string   'dashboard_display_text'
      t.string   'address'
      t.string   'zipcode'
      t.string   'phone_number'
      t.string   'contact'
      t.integer  'chain_id'
      t.integer  'city_id'
      t.datetime 'created_at', :null => false
      t.datetime 'updated_at', :null => false
      t.float    'latitude'
      t.float    'longitude'
      t.datetime 'deleted_at'
      t.boolean  'own_coordinates', :default => false
      t.float    'rest_tax', :default => 0.0
      t.boolean  'status', :default => true
      t.string   'region', :limit => 64
      t.string   'ncr_store_id'
      t.string   'online_order_link'
      t.string   'fishbowl_restaurant_identifier'
      t.string   'ncr_aloha_loyalty_store_id'
      t.boolean  'online_order_support_status'
      t.string   'aloha_online_id'
      t.string   'ncr_aoo_menu_level'
      t.string   'ncr_aoo_price_level'
      t.string   'delivery_radius_miles'
      t.string   'ldap_identity'
      t.string   'beacon_serial_number'
      t.string   'beacon_uuid'
      t.string   'location_qrcode_identifier'
      t.string   'external_id'
      t.string   'social_link'
      t.boolean  'agm_support'
      t.integer  'external_partner_id'
      t.string   'olo_store_id'
      t.boolean  'ncr_provisioned_rewards'
      t.boolean  'ncr_provisioned_loyalty'
    end

    create_table :restaurant_detail_backups do |t|
      t.integer :restaurant_id
      t.boolean  :ts_pay_support, :default => false, :null => false

      t.timestamps
    end

    add_index :restaurant_detail_backups, :restaurant_id

    add_column :restaurants, :backups, :text
  end
end
