class DashboardChoices < ActiveRecord::Migration[5.0]
  def change
    create_table :dashboard_choices do |t|
      t.string :label
      t.integer :question_id
      t.float :value
    end

    add_index :dashboard_choices , :question_id
  end
end