class AddAllowToUpdateGoalSoonToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :allow_to_update_goals, :boolean, :default => true
  end
end