class CreateTaskDefinitions < ActiveRecord::Migration[5.0]
  def change
    create_table :task_definitions do |t|
      t.integer :chain_id, :reward_id, :points
      t.boolean :push_email, :push_phone
      t.string :subject_email, :schedule_type
      t.text :content_email
      t.text :content_phone
      t.datetime :executed_at, :start_at, :end_date
      t.timestamps
    end

    add_index :task_definitions, :chain_id
    add_index :task_definitions, :reward_id
    add_index :task_definitions, [:chain_id,:reward_id]
  end
end
