class CreateDailyUserTables < ActiveRecord::Migration[5.0]
  def change
    create_table :daily_users do |t|
      t.integer :restaurant_id
      t.integer :total
      t.date :active_date
      t.integer :chain_id

      t.timestamps
    end

    add_index :daily_users, :restaurant_id
    add_index :daily_users, [:restaurant_id, :chain_id]
    add_index :daily_users, [:restaurant_id, :active_date]
  end
end
