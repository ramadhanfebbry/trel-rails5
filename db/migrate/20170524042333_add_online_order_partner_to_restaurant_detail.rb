class AddOnlineOrderPartnerToRestaurantDetail < ActiveRecord::Migration[5.0]
  def change
  	if table_exists?(:restaurant_details)
	    add_column :restaurant_details, :online_order_partner, :integer, :default => 0
	    add_index :restaurant_details, :online_order_partner
	  end
  end
end
