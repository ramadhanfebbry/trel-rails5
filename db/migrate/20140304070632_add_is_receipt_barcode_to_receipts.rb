class AddIsReceiptBarcodeToReceipts < ActiveRecord::Migration[5.0]
  def change
    add_column :receipts, :is_receipt_barcode, :boolean, :default => false
  end
end