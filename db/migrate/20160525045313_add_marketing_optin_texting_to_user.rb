class AddMarketingOptinTextingToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :marketing_optin_texting, :boolean, :default => false

  end
end
