class CreatePartnerRewards < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_rewards do |t|
      t.integer :reward_id
      t.datetime :valid_days_of_week_and_time
      t.boolean :valid_all_locations
      t.integer :order_minimum
      t.string :discount_type
      t.string :discount_value
      t.string :order_from_category
      t.string :minimum_count_order_from_category
      t.string :discount_count_from_category
      t.string :discount_from_category

      t.timestamps
    end
    add_index :partner_rewards, :reward_id

    execute "ALTER TABLE partner_rewards ADD COLUMN valid_locations BIGINT[]"
  end
end
