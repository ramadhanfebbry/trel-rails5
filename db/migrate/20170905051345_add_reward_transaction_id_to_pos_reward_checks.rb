class AddRewardTransactionIdToPosRewardChecks < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_reward_checks, :reward_transaction_id, :integer
    add_index :pos_reward_checks, :reward_transaction_id
    add_column :pos_reward_checks, :transaction_type, :integer, :default => 0
  end
end
