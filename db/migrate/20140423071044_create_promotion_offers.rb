class CreatePromotionOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :promotion_offers do |t|
      t.string :title
      t.integer :points
      t.timestamps
    end
  end
end
