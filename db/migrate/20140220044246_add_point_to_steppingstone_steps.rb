class AddPointToSteppingstoneSteps < ActiveRecord::Migration[5.0]
  def change
    add_column :steppingstone_steps, :point, :integer
  end
end
