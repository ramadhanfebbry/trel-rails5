class RemoveUniqIndexOnAnswer < ActiveRecord::Migration[5.0]
  def up
     remove_index :answers, [:user_id, :question_id]
     add_index :answers , [:user_id, :question_id]
  end

  def down
  end
end
