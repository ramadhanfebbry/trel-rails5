class CreateOnosysOnlineOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :onosys_online_orders do |t|
      t.integer :chain_id
      t.integer :restaurant_id
      t.integer :user_id
      t.string :online_order_identifier
      t.integer :loyalty_status
      t.integer :platform
      t.timestamps
    end

    add_index :onosys_online_orders, :chain_id
    add_index :onosys_online_orders, :restaurant_id
    add_index :onosys_online_orders, :user_id
    add_index :onosys_online_orders, :online_order_identifier
  end
end
