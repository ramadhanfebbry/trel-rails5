class ChangeColumnGeodataOnDb < ActiveRecord::Migration[5.0]
  def change
    create_table :analytics_geodata do |t|
      t.integer :user_id
      t.string :keychain
      t.string :android_id
      t.float :lat
      t.float :long

      t.timestamps
    end

    drop_table :user_locations
    drop_table :user_geo_settings
  end


end
