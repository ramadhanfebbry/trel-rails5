class AddPrefixAndPostfixToAppcodeSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :appcode_settings, :prefix, :string
    add_column :appcode_settings, :postfix, :string

  end
end
