class CreateRewardWallets < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_wallets do |t|
      t.references :reward
      t.references :user
      t.datetime :expiryDate
      
      t.timestamps
    end
  end
end
