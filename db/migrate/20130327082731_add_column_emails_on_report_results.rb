class AddColumnEmailsOnReportResults < ActiveRecord::Migration[5.0]
  def up
    add_column :report_results,:emails, :text
  end

  def down
    remove_column :report_results,:emails, :text
  end
end
