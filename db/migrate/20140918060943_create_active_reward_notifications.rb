class CreateActiveRewardNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :active_reward_notifications do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.text :notification
      t.text :cancel_button_text
      t.text :continue_button_text

      t.timestamps
    end
  end
end