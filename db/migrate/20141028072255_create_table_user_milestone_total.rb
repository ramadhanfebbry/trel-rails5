class CreateTableUserMilestoneTotal < ActiveRecord::Migration[5.0]
  def change
    create_table :user_total_receipts do |t|
      t.integer :user_id
      t.integer :chain_id
      t.integer :receipt_count
      t.float :subtotal
      t.float :average
      t.date :last_milestone
      t.integer :reward_pushed_id

      t.timestamps
    end

    add_index :user_total_receipts, :user_id
    add_index :user_total_receipts, [:user_id,:chain_id]
  end
end
