class AddPaymentIncludedToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :payment_included, :boolean, :default => false

  end
end
