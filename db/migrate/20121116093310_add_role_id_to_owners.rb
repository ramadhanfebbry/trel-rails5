class AddRoleIdToOwners < ActiveRecord::Migration[5.0]
  def change
    add_column :owners, :role_id, :integer
  end
end
