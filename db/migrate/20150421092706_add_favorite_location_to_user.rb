class AddFavoriteLocationToUser < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :favorite_location, :integer
  end
end
