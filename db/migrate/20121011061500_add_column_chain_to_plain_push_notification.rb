class AddColumnChainToPlainPushNotification < ActiveRecord::Migration[5.0]
  def change
    add_column :plain_push_notifications, :chains, :text
  end
end
