class AddExternalPartnerToChainSetting < ActiveRecord::Migration[5.0]
  def change
    add_column :chain_settings, :external_partner, :integer
  end
end
