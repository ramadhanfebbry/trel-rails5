class AddPhoneNumberIsMandatoryToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :phone_number_is_required, :boolean, :default => false
  end
end
