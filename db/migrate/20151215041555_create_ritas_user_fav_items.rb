class CreateRitasUserFavItems < ActiveRecord::Migration[5.0]
  def change
    create_table :ritas_user_fav_items do |t|
      t.string :email
      t.integer :fid
      t.timestamps
    end
    add_index :ritas_user_fav_items, :email
  end
end
