class AddStatusOnOfferRules < ActiveRecord::Migration[5.0]
  def up
    add_column :offer_rules, :status ,:boolean, :default => true
  end

  def down
    remove_column :offer_rules, :status
  end
end
