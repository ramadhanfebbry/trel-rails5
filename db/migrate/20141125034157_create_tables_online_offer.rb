class CreateTablesOnlineOffer < ActiveRecord::Migration[5.0]
  def change
    create_table :online_offers do |t|
      t.string :name
      t.decimal :multiplier
      t.date :effectiveDate
      t.date :expiryDate
      t.string :timeStart
      t.string :timeEnd
      t.integer :daysOfWeek
      t.boolean :isActive
      t.text :fineprint
      t.float :bonus_points
      t.float :bonus_points_ftu
      t.integer :chain_id
      t.integer :survey_id
      t.datetime :deleted_at
      t.boolean :is_multiplier, :default => true
      t.decimal :constant_value
      t.integer :smg_survey_id
      t.timestamps
    end

    add_index :online_offers, :chain_id
  end
end
