class CreateDbSummary < ActiveRecord::Migration[5.0]
  def change
    create_table :db_summaries do |t|
      t.integer :chain_id, :restaurant_id
      t.integer :total_member, :member_this_month, :member_last_month
      t.float :avg_general,:avg_visit_month


      t.timestamps
    end

    add_index :db_summaries, :chain_id
    add_index :db_summaries, :restaurant_id
    add_index :db_summaries, [:restaurant_id, :chain_id]
  end
end
