class AddPosUsedTypeToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :pos_used_type, :integer, :default => 1
    add_index :chains, :pos_used_type
  end
end
