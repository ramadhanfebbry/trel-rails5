class AddLatLongRestaurantIdToUserDealSurvey < ActiveRecord::Migration[5.0]
  def change
    add_column :user_deal_surveys, :latitude, :float
    add_column :user_deal_surveys, :longitude, :float
    add_column :user_deal_surveys,:restaurant_id, :integer

    add_index(:user_deal_surveys, :deal_id)
    add_index(:user_deal_surveys, :user_id)
    add_index(:user_deal_surveys, :restaurant_id)
    add_index(:user_deal_surveys, :survey_id)
  end
end
