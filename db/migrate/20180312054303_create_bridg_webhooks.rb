class CreateBridgWebhooks < ActiveRecord::Migration[5.0]
  def change
    create_table :bridg_webhooks do |t|
      t.integer :chain_id
      t.text :check
      t.integer :reward_id
      t.integer :restaurant_id
      t.string :reward_code
      t.string :discount
      t.string :user_id
      t.string :campaign_id
      t.boolean :status
      t.string :status_description

      t.timestamps
    end
  end
end
