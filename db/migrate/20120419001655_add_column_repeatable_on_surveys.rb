class AddColumnRepeatableOnSurveys < ActiveRecord::Migration[5.0]
  def up
    add_column :surveys, :repeatable, :boolean, :default => true
  end

  def down
    remove_column :surveys, :repeatable
  end
end
