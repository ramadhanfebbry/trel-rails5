class AddNewDeviceSignupStatusToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :signup_device_status, :integer
  end
end
