class AddTypeToRewards < ActiveRecord::Migration[5.0]
  def change
    add_column :rewards, :reward_type, :integer
  end
end
