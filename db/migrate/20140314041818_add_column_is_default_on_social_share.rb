class AddColumnIsDefaultOnSocialShare < ActiveRecord::Migration[5.0]
  def up
    add_column :social_shares, :is_default, :boolean, :default => false
  end

  def down
    remove_column :social_shares, :is_default
  end
end
