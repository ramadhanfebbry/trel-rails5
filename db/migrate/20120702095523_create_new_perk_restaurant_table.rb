class CreateNewPerkRestaurantTable < ActiveRecord::Migration[5.0]
  def up
    create_table :perks_restaurants, :force => true do |t|
      t.integer :perk_id
      t.integer :restaurant_id
      t.datetime :deleted_at
    end
  end

  def down
    drop_table :perks_restaurants
    create_table :perk_restaurants, :id => false do |t|
      t.integer :perk_id
      t.integer :restaurant_id
      t.datetime :deleted_at
    end
  end
end
