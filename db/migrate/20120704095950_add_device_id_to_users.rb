class AddDeviceIdToUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :device_id, :string
    add_index :users, :device_id
  end
  
  def down
    remove_column :users, :device_id
    remove_index :users, :device_id
  end
end
