class AddPhoneNumberToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :phone_number, :string
    add_column :users, :birthday, :integer
    add_column :users, :birthmonth, :integer
  end
end
