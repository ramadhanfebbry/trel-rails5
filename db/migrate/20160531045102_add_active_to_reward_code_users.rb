class AddActiveToRewardCodeUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_code_users, :active, :boolean, :default => true
  end
end
