class CreateTaskSchedulers < ActiveRecord::Migration[5.0]
  def change
    create_table :task_schedulers do |t|
      t.integer :job_id
      t.string :run_type
      t.datetime :start_at
      t.datetime :executed_at
      t.datetime :last_executed
      t.datetime :next_run
      t.datetime :end_at
      t.timestamps
    end

    add_index :task_schedulers, :job_id
  end
end
