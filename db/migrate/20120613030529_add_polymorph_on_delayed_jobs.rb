class AddPolymorphOnDelayedJobs < ActiveRecord::Migration[5.0]
  def up
    add_column :delayed_jobs, :chain_id, :integer
    add_column :delayed_jobs, :delayable_type, :string
    add_column :delayed_jobs, :delayable_id, :integer

    add_index(:delayed_jobs,[:delayable_type,:delayable_id])
    add_index(:delayed_jobs, :chain_id)
  end

  def down
    remove_column :delayed_jobs, :chain_id, :delayable_type, :delayable_id
  end
end
