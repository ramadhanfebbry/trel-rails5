class CreatePushServiceNotificationCertificates < ActiveRecord::Migration[5.0]
  def change
    create_table :push_service_notification_certificates do |t|
      t.integer :chain_id
      t.text :certificate
      t.string :environment
      t.timestamps
    end
    add_index :push_service_notification_certificates, :chain_id
  end
end