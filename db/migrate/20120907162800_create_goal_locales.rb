class CreateGoalLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :goal_locales do |t|
      t.integer :goal_id
      t.integer :locale_id
      t.text :text

      t.timestamps
    end
  end
end
