class AddTimeStampOnOnlineOrders < ActiveRecord::Migration[5.0]
  def up
    add_column :online_orders, :issue_time , :string
  end

  def down
    remove_column :online_orders, :issue_time
  end
end
