class AddColumnOnReportSubscriptions < ActiveRecord::Migration[5.0]
  def up
    add_column :report_subscriptions, :report_kind , :integer
    add_column :report_subscriptions, :sending_method , :integer
    add_column :report_subscriptions, :start_at , :date
    add_column :report_subscriptions, :end_at , :date

    remove_column :report_definitions, :report_kind
    remove_column :report_definitions, :sending_method
    remove_column :report_definitions, :start_at
    remove_column :report_definitions, :end_at
  end

  def down
    remove_column :report_subscriptions, :report_kind
    remove_column :report_subscriptions, :sending_method
    remove_column :report_subscriptions, :start_at
    remove_column :report_subscriptions, :end_at
  end
end
