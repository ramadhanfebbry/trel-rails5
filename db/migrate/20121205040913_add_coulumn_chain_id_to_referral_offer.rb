class AddCoulumnChainIdToReferralOffer < ActiveRecord::Migration[5.0]
  def change
    add_column :referral_offers, :chain_id , :integer
    add_index :referral_offers, :chain_id
  end
end
