class AddNewIndexForReceiptsApril122016 < ActiveRecord::Migration[5.0]
  def up
    add_index :receipts, [:created_at, :chain_id], order: {created_at: :desc}, :name => "idx_createdat_chaind_id";
  end

  def down
    remove_index :receipts, [:created_at, :chain_id]
  end
end
