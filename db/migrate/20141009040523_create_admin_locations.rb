class CreateAdminLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :user_locations do |t|
      t.integer :user_id
      t.string :city, :postal_code, :country_code
      t.string :address
      t.float :latitude
      t.float :longitude

      t.timestamps
    end

    add_index :user_locations, :user_id
  end
end
