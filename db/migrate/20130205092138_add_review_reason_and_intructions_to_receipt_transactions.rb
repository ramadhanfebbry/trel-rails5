class AddReviewReasonAndIntructionsToReceiptTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :receipt_transactions, :review_reason, :string
    add_column :receipt_transactions, :instructions, :text
  end
end