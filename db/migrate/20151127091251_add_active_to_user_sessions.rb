class AddActiveToUserSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_sessions, :active, :boolean
    add_index :user_sessions, :active
  end
end
