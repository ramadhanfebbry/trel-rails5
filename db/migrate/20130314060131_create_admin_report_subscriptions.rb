class CreateAdminReportSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :report_subscriptions do |t|
      t.integer :chain_id, :report_description_id
      t.text :custom_params
      t.timestamps
    end


  end
end
