class CreateActivitySummaries < ActiveRecord::Migration[5.0]
  def change
    create_table :activity_summaries do |t|
      t.date :transaction_date
      t.integer :total, :restaurant_id, :chain_id
      t.timestamps
    end

    create_table :reward_summaries do |t|
      t.date :transaction_date
      t.integer :total, :restaurant_id, :chain_id
      t.timestamps
    end


    add_index :activity_summaries, :restaurant_id
    add_index :activity_summaries, :chain_id
    add_index :activity_summaries, [:chain_id,:restaurant_id]

    add_index :reward_summaries, :restaurant_id
    add_index :reward_summaries, :chain_id
    add_index :reward_summaries, [:chain_id,:restaurant_id]
  end


end
