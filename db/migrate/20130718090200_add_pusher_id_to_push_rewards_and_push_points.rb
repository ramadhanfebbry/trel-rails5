class AddPusherIdToPushRewardsAndPushPoints < ActiveRecord::Migration[5.0]

  def change
    add_column :push_points, :pusher_id, :integer
    add_column :push_rewards, :pusher_id, :integer
  end

end
