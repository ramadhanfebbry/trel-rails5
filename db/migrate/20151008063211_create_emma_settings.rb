class CreateEmmaSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :emma_settings do |t|
      t.integer :chain_id
      t.string :public_api_key
      t.string :private_api_key
      t.string :account_id
      t.string :group_id
      t.timestamps
    end
    add_index :emma_settings, :chain_id
  end
end
