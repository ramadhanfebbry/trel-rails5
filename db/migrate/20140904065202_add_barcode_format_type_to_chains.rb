class AddBarcodeFormatTypeToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :barcode_format_type, :integer, :default => 13
  end
end
