class ChangeColumnTypeIsGetIncentiveDefaultToTrue < ActiveRecord::Migration[5.0]
  def up
    remove_column :referral_users, :is_get_incentive
    add_column :referral_users, :is_get_incentive, :boolean, default: true
  end

  def down
    remove_column :referral_users, :is_get_incentive
    add_column :referral_users, :is_get_incentive, :boolean, default: false
  end
end
