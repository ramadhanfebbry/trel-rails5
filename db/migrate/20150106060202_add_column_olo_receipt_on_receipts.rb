class AddColumnOloReceiptOnReceipts < ActiveRecord::Migration[5.0]
  def up
    add_column :receipts, :is_olo, :boolean, :default => false
  end

  def down
    remove_column :receipts, :is_olo
  end
end
