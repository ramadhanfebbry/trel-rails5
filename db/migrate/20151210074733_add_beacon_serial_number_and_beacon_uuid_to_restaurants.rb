class AddBeaconSerialNumberAndBeaconUuidToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :beacon_serial_number, :string
    add_column :restaurants, :beacon_uuid, :string
    add_index :restaurants, :beacon_serial_number
    add_index :restaurants, :beacon_uuid
  end
end
