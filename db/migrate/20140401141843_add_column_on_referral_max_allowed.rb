class AddColumnOnReferralMaxAllowed < ActiveRecord::Migration[5.0]
  def up
    add_column :referral_programs,:max_incentive, :integer , :default => 0
  end

  def down
    remove_column :referral_programs,:max_incentive, :integer
  end
end
