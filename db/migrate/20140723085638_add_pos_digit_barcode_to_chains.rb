class AddPosDigitBarcodeToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :pos_digit_barcode, :integer, :default => 9
    add_index :barcodes, :code
  end
end
