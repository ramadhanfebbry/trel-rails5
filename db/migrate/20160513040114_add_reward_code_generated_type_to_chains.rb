class AddRewardCodeGeneratedTypeToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :reward_code_generated_type, :integer, :default => 1

    Chain.select("id, use_generated_code, reward_code_generated_type").each do |chain|
      chain.update_column(:reward_code_generated_type, chain.use_generated_code ? 1 : 2)
    end
  end
end
