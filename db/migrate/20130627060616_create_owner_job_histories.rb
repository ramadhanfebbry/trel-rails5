class CreateOwnerJobHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :owner_job_histories do |t|
      t.integer :owner_job_id
      t.text :success
      t.text :failed
      t.timestamps
    end
  end
end
