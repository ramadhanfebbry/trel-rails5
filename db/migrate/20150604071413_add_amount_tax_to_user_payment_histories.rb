class AddAmountTaxToUserPaymentHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :user_payment_histories, :amount, :float, :default => 0
    add_column :user_payment_histories, :tip, :float, :default => 0
    add_column :user_payment_histories, :ncr_receipt_number, :string
    add_column :user_payment_histories, :qrcode, :string
    execute "create index index_ncr_receipt_number_user_payment_histories on user_payment_histories (ncr_receipt_number);"
    execute "create index index_qrcode_user_payment_histories on user_payment_histories (qrcode);"
  end
end
