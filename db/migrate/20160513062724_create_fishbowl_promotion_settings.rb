class CreateFishbowlPromotionSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :fishbowl_promotion_settings do |t|
      t.integer :chain_id
      t.string :username
      t.string :password
      t.string :client_id
      t.string :client_secret
      t.string :brand_id

      t.timestamps
    end
    add_index :fishbowl_promotion_settings, :chain_id
  end
end
