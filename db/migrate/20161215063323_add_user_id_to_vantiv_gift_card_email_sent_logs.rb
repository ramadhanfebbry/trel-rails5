class AddUserIdToVantivGiftCardEmailSentLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :vantiv_gift_card_email_sent_logs, :user_id, :integer
  end
end
