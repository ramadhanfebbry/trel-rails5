class ChangeColumnOnOwnerJobLog < ActiveRecord::Migration[5.0]
  def up
    remove_column :ownerjob_logs, :success_notification
    add_column :ownerjob_logs, :notification_status, :integer
  end

  def down
    add_column :ownerjob_logs , :success_notification,:integer
    remove_column :ownerjob_logs , :notification_status
  end
end
