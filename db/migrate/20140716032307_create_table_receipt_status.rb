class CreateTableReceiptStatus < ActiveRecord::Migration[5.0]
  def change
    create_table :receipt_statuses do |t|
      t.integer :receipt_transaction_id
      t.integer :status
      t.integer :delayed_status
      t.integer :delayed_id

      t.timestamps
    end

    add_index :receipt_statuses , :receipt_transaction_id
    add_index :receipt_statuses , :delayed_id
    add_index :receipt_statuses , [:receipt_transaction_id, :status], :name => "receipt_status_index"
    add_index :receipt_statuses , [:receipt_transaction_id, :delayed_status], :name => "receipt_dj_status_index"
  end
end
