class AddLocaleToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :locale, :string, :default => "en"
  end
end
