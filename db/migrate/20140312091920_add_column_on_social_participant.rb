class AddColumnOnSocialParticipant < ActiveRecord::Migration[5.0]
  def up
    add_column :social_share_participants, :social_share_id , :integer
    add_index :social_share_participants, :social_share_id
  end

  def down
  end
end
