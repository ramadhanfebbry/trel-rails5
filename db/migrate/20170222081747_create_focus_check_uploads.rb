class CreateFocusCheckUploads < ActiveRecord::Migration[5.0]
  def change
    create_table :focus_check_uploads do |t|
      t.integer :pos_location_id
      t.integer :chain_id
      t.string :check_id
      t.integer :status
      t.integer :user_id
      t.string :user_code
      t.string :code_type
      t.timestamps
    end
    add_column :focus_check_uploads, :check_data, :json
    add_index :focus_check_uploads, [:pos_location_id, :check_id, :created_at], :name => "idx_pos_location_check_date"
  end
end
