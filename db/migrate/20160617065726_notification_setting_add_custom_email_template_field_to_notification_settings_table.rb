class NotificationSettingAddCustomEmailTemplateFieldToNotificationSettingsTable < ActiveRecord::Migration[5.0]
   def change
    add_column :notification_settings, :custom_email_template, :string
  end
end
