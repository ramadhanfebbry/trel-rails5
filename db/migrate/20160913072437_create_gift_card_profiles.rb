class CreateGiftCardProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :gift_card_profiles do |t|
      t.integer :user_id
      t.string :gift_card_number
      t.float :amount
      t.datetime :activate_at
      t.string :expiration_month
      t.string :expiration_year
      t.string :cvv
      t.string :cvv2
      t.string :security_code
      t.integer :vantiv_gift_card_id
      t.timestamps
    end
    add_index :gift_card_profiles, :user_id
    add_index :gift_card_profiles, :gift_card_number
  end
end
