class AddMailchimpSyncAndMailchimpApiKeyToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :mailchimp_sync, :boolean, :default => false
    add_column :chains, :mailchimp_api_key, :string
    add_column :chains, :mailchimp_list_id, :string
  end
end