class AddColumnOnReports < ActiveRecord::Migration[5.0]
  def up
    add_column :reports, :subscription_id, :integer
  end

  def down
    remove_column :reports, :subscription_id
  end
end
