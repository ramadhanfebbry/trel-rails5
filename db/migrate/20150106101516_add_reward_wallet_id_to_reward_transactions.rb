class AddRewardWalletIdToRewardTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_transactions, :reward_wallet_id, :integer
    add_index :reward_transactions, :reward_wallet_id
  end
end
