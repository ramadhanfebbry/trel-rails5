class AddFishbowlRestaurantIdentifierToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :fishbowl_restaurant_identifier, :string
  end
end
