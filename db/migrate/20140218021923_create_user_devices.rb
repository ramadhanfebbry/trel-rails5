class CreateUserDevices < ActiveRecord::Migration[5.0]
  def change
    create_table :user_devices do |t|
      t.integer :user_id
      t.string :keychain_value

      t.timestamps
    end

    add_index :user_devices, :user_id
    add_index :user_devices, :keychain_value
    add_index :user_devices, [:user_id, :keychain_value]
  end
end
