class CreateOnosysReceiptOnlineOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :onosys_receipt_online_orders do |t|
      t.integer :receipt_id
      t.integer :onosys_online_order_id

      t.timestamps
    end
  end
end
