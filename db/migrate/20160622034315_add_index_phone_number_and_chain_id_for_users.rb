class AddIndexPhoneNumberAndChainIdForUsers < ActiveRecord::Migration[5.0]
  def up
    add_index :users, [:phone_number, :chain_id]
  end

  def down
    remove_index :users, [:phone_number, :chain_id]
  end
end
