class AddColumnDescriptionOnRewards < ActiveRecord::Migration[5.0]
  def up
    add_column :rewards,:description , :text
    add_column :rewards, :visible_dashboard,:boolean, :default => false
  end

  def down
    remove_column :rewards,:description
    remove_column :rewards,:visible_dashboard
  end
end
