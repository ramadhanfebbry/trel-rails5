class ChangeColumnErrorDescriptionOnUserPaymentHistories < ActiveRecord::Migration[5.0]
  def up
    change_column :user_payment_histories, :error_description, :text
  end

  def down
    change_column :user_payment_histories, :error_description, :string
  end
end
