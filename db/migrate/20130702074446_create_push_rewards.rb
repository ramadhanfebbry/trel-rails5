class CreatePushRewards < ActiveRecord::Migration[5.0]
  def change
    create_table :push_rewards do |t|
      t.integer :chain_id
      t.integer :reward_id
      t.integer :push_type
      t.text :user_ids
      t.text :success_logs
      t.text :error_logs
      t.timestamps
    end
  end
end
