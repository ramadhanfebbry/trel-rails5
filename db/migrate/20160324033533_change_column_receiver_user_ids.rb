class ChangeColumnReceiverUserIds < ActiveRecord::Migration[5.0]
  def up
    remove_column :push_service_notifications, :receiver_user_ids
    execute "ALTER TABLE push_service_notifications ADD COLUMN receiver_user_ids BIGINT[]"
  end

  def down
    remove_column :push_service_notifications, :receiver_user_ids
    add_column :push_service_notifications, :receiver_user_ids, :text, array: true, default: []
  end
end
