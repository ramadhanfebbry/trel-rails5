class AddColumnStaffcodeOnRewardTransactions < ActiveRecord::Migration[5.0]
  def up
    add_column :reward_transactions, :staffcode, :integer
  end

  def down
    remove_column(:reward_transactions, :staffcode)
  end
end
