class CreatePosMenuItems < ActiveRecord::Migration[5.0]
  def change
    create_table :pos_menu_items do |t|
      t.integer :pos_menu_id
      t.string :item_name
      t.integer :item_number
      t.text :description
      t.integer :general_menu_item_id
      t.integer :chain_id

      t.timestamps
    end
  end
end