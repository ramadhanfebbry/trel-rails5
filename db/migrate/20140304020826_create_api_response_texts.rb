class CreateApiResponseTexts < ActiveRecord::Migration[5.0]
  def change
    create_table :api_response_texts do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.text :message
      t.integer :message_for

      t.timestamps
    end
  end
end
