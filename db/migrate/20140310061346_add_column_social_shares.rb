class AddColumnSocialShares < ActiveRecord::Migration[5.0]
  def up
    add_column :social_shares, :incentive_type, :string
    add_column :social_shares, :incentive_id, :integer
    add_column :social_shares, :fb_text, :text
    add_column :social_shares, :twitter_text, :text
    add_column :social_shares, :instagram_text, :text
  end

  def down
    remove_column :social_shares, :incentive_type, :incentive_id
  end
end
