class CreateTableMilestoneNotification < ActiveRecord::Migration[5.0]
   def change
    create_table :milestone_notifications do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.string :email_subject
      t.text :email_content

      t.timestamps
    end

     add_index(:milestone_notifications, :chain_id)
     add_index(:milestone_notifications, :locale_id)
     add_index(:milestone_notifications, [:locale_id,:chain_id])
  end
end
