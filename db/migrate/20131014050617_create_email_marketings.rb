class CreateEmailMarketings < ActiveRecord::Migration[5.0]
  def change
    create_table :email_marketings do |t|
      t.integer :chain_id
      t.integer :user_id
      t.string :email
      t.integer :restaurant_id
      t.datetime :signup_date
      t.integer :status

      t.timestamps
    end
  end
end