class AddBarcodeFormatToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :barcode_format, :integer, :default => 1
  end
end
