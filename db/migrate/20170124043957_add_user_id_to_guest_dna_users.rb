class AddUserIdToGuestDnaUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :guest_dna_users, :user_id, :integer, :default => nil
    add_index :guest_dna_users, :user_id
  end
end
