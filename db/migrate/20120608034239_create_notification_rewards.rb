class CreateNotificationRewards < ActiveRecord::Migration[5.0]
  def change
    create_table :notification_rewards do |t|
      t.integer :reward_id
      t.text :text

      t.timestamps
    end
  end
end
