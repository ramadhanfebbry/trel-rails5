class CreateReceipts < ActiveRecord::Migration[5.0]
  def change
    create_table :receipts do |t|

	    t.references :chain
	    t.references :user
      t.integer :status
	    t.string :image_url
	    t.string :thumbnail_url

      t.timestamps
    end
  end
end
