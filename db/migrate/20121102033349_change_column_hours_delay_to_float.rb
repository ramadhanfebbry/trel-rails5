class ChangeColumnHoursDelayToFloat < ActiveRecord::Migration[5.0]
  def up
    change_column :chains, :hours_delay_reward,:float
  end

  def down
    change_column :chains, :hours_delay_reward,:integer
  end
end
