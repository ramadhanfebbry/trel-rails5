class AddAuthorizationCodeToUserPaymentHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :user_payment_histories, :authorization_code, :string
    add_index :user_payment_histories, :authorization_code
  end
end
