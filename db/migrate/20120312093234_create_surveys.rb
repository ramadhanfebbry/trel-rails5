class CreateSurveys < ActiveRecord::Migration[5.0]
  def change
    create_table :surveys do |t|
      t.string :title
      t.date :valid_from_date
      t.date :valid_to_date
      t.references :chain

      t.timestamps
    end
  end
end
