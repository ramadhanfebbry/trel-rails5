class CreateUserActivityNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :user_activity_notifications do |t|
      t.integer :chain_id
      t.string :description
      t.text :notification
      t.string :unique_identifier

      t.timestamps
    end
    add_index :user_activity_notifications, :chain_id
    add_index :user_activity_notifications, :unique_identifier
  end
end
