class AddOrderNumberAndRotatingGroupIdToQuestions < ActiveRecord::Migration[5.0]
  def change
    add_column :questions, :order_number, :integer
    add_column :questions, :rotating_group_id, :integer
  end
end