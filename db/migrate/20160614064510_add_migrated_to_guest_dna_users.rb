class AddMigratedToGuestDnaUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :guest_dna_users, :migrated, :boolean, :default => false
  end
end
