class AddUserLimitReceiptApprovedToChain < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :user_limit_receipt_approved_per_day, :integer, :default => 0
  end
end