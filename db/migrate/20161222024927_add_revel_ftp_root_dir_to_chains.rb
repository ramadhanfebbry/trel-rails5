class AddRevelFtpRootDirToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :revel_to_relevant_ftp_root_dir, :string
    add_column :chains, :relevant_to_revel_ftp_root_dir, :string
  end
end
