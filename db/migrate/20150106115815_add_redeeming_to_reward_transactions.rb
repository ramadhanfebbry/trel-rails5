class AddRedeemingToRewardTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_transactions, :redeeming, :boolean, :default => false
    add_index :reward_transactions, :redeeming
    remove_index :reward_transactions, :reward_wallet_id
    remove_column :reward_transactions, :reward_wallet_id
  end
end
