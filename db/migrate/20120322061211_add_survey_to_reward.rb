class AddSurveyToReward < ActiveRecord::Migration[5.0]
  def change
    add_column :rewards, :survey_id, :integer
  end
end
