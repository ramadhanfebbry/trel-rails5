class CreateRaffleAdditionalInformations < ActiveRecord::Migration[5.0]
  def change
    create_table :raffle_additional_informations do |t|
      t.integer :raffle_id
      t.string :field_name
      t.string :app_display_text
      t.string :description
      t.timestamps
    end
  end
end
