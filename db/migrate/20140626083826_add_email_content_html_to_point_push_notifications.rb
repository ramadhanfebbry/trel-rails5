class AddEmailContentHtmlToPointPushNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :point_push_notifications, :email_content_html, :text
    PointPushNotification.all.each do |ppn|
      ppn.update_column(:email_content_html, ppn.email_content)
    end
  end
end