class CreateSurveysUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :surveys_users do |t|
      t.integer :user_id, :survey_id
      t.timestamps
    end

    add_index(:surveys_users, :user_id)
    add_index(:surveys_users, :survey_id)
  end
end
