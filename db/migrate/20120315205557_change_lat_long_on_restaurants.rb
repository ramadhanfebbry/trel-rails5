class ChangeLatLongOnRestaurants < ActiveRecord::Migration[5.0]
  def up
    remove_column :restaurants, :latitude
    remove_column :restaurants, :longitude
    add_column :restaurants, :latitude, :float
    add_column :restaurants, :longitude, :float
  end

  def down
    remove_column :restaurants, :latitude
    remove_column :restaurants, :longitude
    add_column :restaurants, :latitude, :string
    add_column :restaurants, :longitude, :string
  end
end
