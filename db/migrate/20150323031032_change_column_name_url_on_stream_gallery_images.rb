class ChangeColumnNameUrlOnStreamGalleryImages < ActiveRecord::Migration[5.0]
  def up
    rename_column :stream_gallery_images, :url, :feed_link
  end

  def down
    rename_column :stream_gallery_images, :feed_link, :url
  end
end
