class FixRelationPartners < ActiveRecord::Migration[5.0]
  def up
    remove_column :restaurant_categories, :partner_category_id
    remove_column :partner_menu_items, :partner_category_id

    add_column :restaurant_categories, :partner_sub_category_id, :integer
    add_index :restaurant_categories, :partner_sub_category_id

    add_column :partner_menu_items, :partner_sub_category_id, :integer
    add_index :partner_menu_items, :partner_sub_category_id
  end

  def down
    add_column :restaurant_categories, :partner_category_id, :integer
    add_index :restaurant_categories, :partner_category_id

    add_column :partner_menu_items, :partner_category_id, :integer
    add_index :partner_menu_items, :partner_category_id

    remove_column :restaurant_categories, :partner_sub_category_id
    remove_column :partner_menu_items, :partner_sub_category_id
  end
end
