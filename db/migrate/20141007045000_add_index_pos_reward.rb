class AddIndexPosReward < ActiveRecord::Migration[5.0]
  def up
    add_index :reward_pos_codes, :reward_id
    add_index :reward_pos_codes, :user_id
    add_index :reward_pos_codes, [:user_id, :reward_id]
  end

  def down
    remove_index :reward_pos_codes, :reward_id
    remove_index :reward_pos_codes, :user_id
    remove_index :reward_pos_codes, [:user_id, :reward_id]
  end
end
