class CreatePromotionLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :promotion_locales do |t|
      t.integer :chain_id
      t.string :locale
      t.string :alert

      t.timestamps
    end
    add_index :promotion_locales, :chain_id
    add_index :promotion_locales, :locale
  end
end
