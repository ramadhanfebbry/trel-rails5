class CreateTableReceiptApprovedDetails < ActiveRecord::Migration[5.0]
  def change

    create_table :receipt_approved_details do |t|
      t.integer :chain_id
      t.integer :restaurant_id
      t.integer :receipt_id
      t.float :subtotal
      t.date :issue_date
      t.string :time_stamp
      t.string :receipt_number
      t.timestamps
    end

    add_index :receipt_approved_details,[:chain_id ,:restaurant_id,:subtotal, :issue_date, :time_stamp, :receipt_number], :name => 'chain_idsubtotalrestaurantidx'
  end
end
