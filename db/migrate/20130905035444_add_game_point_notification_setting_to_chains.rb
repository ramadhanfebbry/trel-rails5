class AddGamePointNotificationSettingToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :game_point_notif_send_to_device, :boolean, :default => false
    add_column :chains, :game_point_notif_send_to_email, :boolean, :default => false
  end
end
