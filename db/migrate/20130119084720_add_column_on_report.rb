class AddColumnOnReport < ActiveRecord::Migration[5.0]
  def up
    add_column :reports, :kind, :string
    add_column :reports, :url, :string
  end

  def down
    remove_column(:reports, :kind, :url)
  end
end
