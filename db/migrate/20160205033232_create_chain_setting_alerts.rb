class CreateChainSettingAlerts < ActiveRecord::Migration[5.0]
  def change
    create_table :chain_setting_alerts do |t|
      t.integer :chain_id
      t.string :email
      t.boolean :is_active, default: false
      t.string :send_time
      t.string :send_zone

      t.timestamps
    end

    add_index :chain_setting_alerts, :chain_id
  end
end
