class AddCheckIdToRewardTransactions < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_transactions, :check_id, :integer
  end
end