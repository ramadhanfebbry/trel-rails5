class AddColumnOloSettings < ActiveRecord::Migration[5.0]
  def up
    add_column :olo_connect_settings, :iphone_api_key, :string
    add_column :olo_connect_settings, :android_api_key, :string
  end

  def down
    remove_column :olo_connect_settings, :iphone_api_key, :android_api_key
  end
end
