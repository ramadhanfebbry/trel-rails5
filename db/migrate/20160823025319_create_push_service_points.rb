class CreatePushServicePoints < ActiveRecord::Migration[5.0]
  def up
    create_table :push_service_points do |t|
      t.integer :chain_id
      t.integer :point
      t.datetime :sent_at
      t.integer :recipient_count
      t.integer  :admin_id
      t.integer  :status, :default => 0
      t.string   :date_text_ttd
      t.boolean  :sent,  :default => false, :null => false

      t.timestamps
    end

    execute "ALTER TABLE push_service_points ADD COLUMN receiver_user_ids BIGINT[]"
  end

  def down
    drop_table :push_service_points
  end
end