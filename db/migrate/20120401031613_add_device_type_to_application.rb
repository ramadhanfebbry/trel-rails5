class AddDeviceTypeToApplication < ActiveRecord::Migration[5.0]
  def change
    add_column :applications, :device_type, :integer
    add_index :applications, :device_type
  end
end
