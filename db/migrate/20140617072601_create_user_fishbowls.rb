class CreateUserFishbowls < ActiveRecord::Migration[5.0]
  def change
    create_table :user_fishbowls do |t|
      t.integer :chain_id
      t.integer :user_id
      t.integer :restaurant_id
      t.integer :status
      t.integer :member_id
      t.datetime :signup_date
      t.timestamps
    end
  end
end
