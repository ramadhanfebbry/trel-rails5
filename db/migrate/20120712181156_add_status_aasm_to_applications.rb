class AddStatusAasmToApplications < ActiveRecord::Migration[5.0]
  def change
    add_column :applications, :status, :string, :default => 'active'
  end
end
