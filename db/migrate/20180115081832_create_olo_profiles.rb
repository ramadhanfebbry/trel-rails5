class CreateOloProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :olo_profiles do |t|
      t.integer :user_id
      t.string :account_token

      t.timestamps
    end
    add_index :olo_profiles, :user_id
  end
end
