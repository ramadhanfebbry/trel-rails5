class AddNewIndexOptimizing < ActiveRecord::Migration[5.0]
  def up
    add_index :delayed_jobs, :locked_by  unless index_exists?(:delayed_jobs, :locked_by)
    add_index :delayed_jobs, :failed_at unless index_exists?(:delayed_jobs, :failed_at)
    add_index :rpush_notifications, [:delivered, :failed, :deliver_after], :name => "rpush_d_f_da_idx" unless index_exists?(:rpush_notifications, [:delivered, :failed, :deliver_after])
    add_index :stream_gallery_images, :stream_gallery_id unless index_exists?(:stream_gallery_images, :stream_gallery_id)
    add_index :surveys, [:id, :deleted_at] unless index_exists?(:surveys, [:id, :deleted_at])
    if table_exists?(:aoo_cloud_connect_credentials) && column_exists?(:aoo_cloud_connect_credentials, :chain_id ) && !index_exists?(:aoo_cloud_connect_credentials, :chain_id)
      add_index :aoo_cloud_connect_credentials, :chain_id 
    end
    if table_exists?(:bp_redeem_histories) && column_exists?(:bp_redeem_histories, [:bpid, :user_id] ) && !index_exists?(:bp_redeem_histories, [:bpid, :user_id])
      add_index :bp_redeem_histories, [:bpid, :user_id] 
    end
  end

  def down
    remove_index :delayed_jobs, :locked_by
    remove_index :delayed_jobs, :failed_at
    remove_index :rpush_notifications, [:delivered, :failed, :deliver_after]
    remove_index :stream_gallery_images, :stream_gallery_id
    remove_index :surveys, [:id, :deleted_at]
    remove_index :aoo_cloud_connect_credentials, :chain_id
    remove_index :aloha_configs, :chain_id
    remove_index :bp_redeem_histories, [:bpid, :user_id]
  end
end
