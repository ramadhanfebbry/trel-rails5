class RenameColumnOloRewardsSettings < ActiveRecord::Migration[5.0]
  def up
    rename_column :olorewards_settings,:basic_password, :basic_pwd
  end

  def down
    rename_column :olorewards_settings,:basic_pwd, :basic_password
  end
end