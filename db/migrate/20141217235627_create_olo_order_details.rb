class CreateOloOrderDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :olo_order_details do |t|
      t.integer :olo_order_id
      t.integer :olo_ref, :limit =>8
      t.string :store_name
      t.integer :store_ref
      t.string :email
      t.boolean :is_guest, :defaul => false
      t.float :subtotal, :discount, :tax, :total
      t.date :time_placed
      t.date :time_closed
      t.timestamps
    end


    add_index :olo_order_details, :olo_order_id
  end
end
