class AddColumnRuleBooleanOnOffers < ActiveRecord::Migration[5.0]
  def up
    add_column :offers, :having_rule, :boolean , :default => false
  end

  def down
    remove_column :offers, :having_rule
  end
end
