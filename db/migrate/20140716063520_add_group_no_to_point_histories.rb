class AddGroupNoToPointHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :point_histories, :group_no, :integer, :default => 0
    add_index :point_histories, :group_no
    add_index :point_histories, [:user_id, :group_no]
    #Delayed::Job.enqueue(PointHistoryGroupDescriptionJob.new("yes"))
  end
end
