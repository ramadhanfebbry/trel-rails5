class AddGamePointSubmittedToLevelsPoints < ActiveRecord::Migration[5.0]
  def change
    add_column :levels_points, :game_point_submitted, :float
  end
end