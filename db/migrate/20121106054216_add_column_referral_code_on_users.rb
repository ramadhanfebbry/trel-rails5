class AddColumnReferralCodeOnUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :referral_code, :string, :default => nil
  end

  def down
    remove_column :users, :referral_code
  end
end
