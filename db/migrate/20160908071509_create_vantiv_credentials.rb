class CreateVantivCredentials < ActiveRecord::Migration[5.0]
  def change
    create_table :vantiv_credentials do |t|
      t.string :account_id
      t.string :account_token
      t.string :application_id
      t.string :acceptor_id
      t.integer :chain_id

      t.timestamps
    end

    add_index :vantiv_credentials, :chain_id
  end
end


