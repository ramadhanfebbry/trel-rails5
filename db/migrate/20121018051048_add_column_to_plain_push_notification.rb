class AddColumnToPlainPushNotification < ActiveRecord::Migration[5.0]
  def up
    add_column :plain_push_notifications, :is_push_email, :boolean,:default => true
    add_column :plain_push_notifications, :is_push_phone, :boolean,:default => true
    add_column :plain_push_notifications, :executed_at, :datetime
  end

  def down
    remove_column(:plain_push_notifications, :is_push_email, :is_push_phone, :executed_at)
  end
end
