class AddLastSetTagAtToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :last_set_tag_at, :datetime
  end
end
