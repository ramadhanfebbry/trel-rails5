class AddColumnTestingMode < ActiveRecord::Migration[5.0]
  def up
    add_column :plain_push_notifications, :testing_mode , :integer, :default => nil
    add_column :push_rewards, :testing_mode , :integer, :default => nil
    add_column :push_points, :testing_mode , :integer, :default => nil
  end

  def down
  end
end
