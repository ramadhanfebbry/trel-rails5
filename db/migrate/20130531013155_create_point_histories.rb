class CreatePointHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :point_histories do |t|
      t.integer :user_id
      t.string :description
      t.float :point

      t.timestamps
    end
  end
end
