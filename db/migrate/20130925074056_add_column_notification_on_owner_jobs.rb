class AddColumnNotificationOnOwnerJobs < ActiveRecord::Migration[5.0]
  def up
    add_column :owner_jobs,:notification,:boolean, :default => false
  end

  def down
    remove_column :owner_jobs,:notification
  end
end
