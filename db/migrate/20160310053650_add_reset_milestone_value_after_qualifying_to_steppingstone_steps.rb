class AddResetMilestoneValueAfterQualifyingToSteppingstoneSteps < ActiveRecord::Migration[5.0]
  def change
    add_column :steppingstone_steps, :reset_milestone_value_after_qualifying, :boolean, default: true
  end
end
