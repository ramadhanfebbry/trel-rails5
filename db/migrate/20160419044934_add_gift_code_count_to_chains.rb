class AddGiftCodeCountToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :gift_code_count, :bigint, :default => 0
    Chain.all.each do |chain|
      gift_code = chain.gift_code_setting
      gift_code_setting = JSON.parse(gift_code.to_json) rescue nil
      gift_code_setting ||= AppcodeSetting::DEFAULT_SETTING_GIFTCODE
      if gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Numeric"]
        regex = '^\d{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).count
      elsif gift_code_setting["barcode_format"].to_i == Chain::BARCODE_FORMAT["Alphanumeric"]
        regex = '^[[A-Z]+[0-9]+|[0-9]+[A-Z]]{' + gift_code_setting["pos_digit_barcode"].to_s + '}$'
        code_count = UserSession.select("code").where("code ~ ? AND chain_id = ? AND code_type = ?", regex, chain.id, AppcodeSetting::CODE_TYPES["GIFTCODE"]).count
      end
      chain.update_column(:gift_code_count, code_count)
    end

  end
end
