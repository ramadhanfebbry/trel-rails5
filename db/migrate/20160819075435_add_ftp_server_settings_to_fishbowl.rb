class AddFtpServerSettingsToFishbowl < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :ftp_url, :string
    add_column :fishbowl_settings, :ftp_port, :string
    add_column :fishbowl_settings, :ftp_username, :string
    add_column :fishbowl_settings, :ftp_password, :string
  end
end
