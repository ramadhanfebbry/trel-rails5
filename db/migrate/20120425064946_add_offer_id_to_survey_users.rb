class AddOfferIdToSurveyUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys_users, :offer_id, :integer
  end
end
