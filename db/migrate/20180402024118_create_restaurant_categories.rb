class CreateRestaurantCategories < ActiveRecord::Migration[5.0]
  def up
    create_table :restaurant_categories do |t|
      t.integer :partner_category_id
      t.integer :restaurant_id

      t.timestamps
    end

    add_index :restaurant_categories, :partner_category_id
    add_index :restaurant_categories, :restaurant_id

    remove_column :partner_categories, :restaurant_id
  end

  def down
    drop_table :restaurant_categories
    add_column :partner_categories, :restaurant_id, :integer
  end
end
