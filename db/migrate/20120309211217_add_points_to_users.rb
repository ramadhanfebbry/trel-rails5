class AddPointsToUsers < ActiveRecord::Migration[5.0]

  def self.up
    change_table :users do |t|
      t.change :points, :integer, :default => 0
    end
    #User.update_all ["points = ?", 0]
  end

  def self.down
    remove_column :users, :points
  end
end
