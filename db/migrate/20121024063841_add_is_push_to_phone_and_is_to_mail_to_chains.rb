class AddIsPushToPhoneAndIsToMailToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :is_push_to_phone, :boolean, :default => false
    add_column :chains, :is_push_to_email, :boolean, :default => false
  end
end
