class AddShowExpiredRewardToApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :application_keys, :show_expired_reward, :boolean, :default => true
  end
end
