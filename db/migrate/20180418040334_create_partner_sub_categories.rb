class CreatePartnerSubCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_sub_categories do |t|
      t.integer :partner_category_id
      t.string :name
      t.text :description
      t.integer :chain_id

      t.timestamps
    end

    add_index :partner_sub_categories, :partner_category_id
    add_index :partner_sub_categories, :name
    add_index :partner_sub_categories, :chain_id
    add_index :partner_sub_categories, [:name, :chain_id, :partner_category_id], :name =>  :name_chain_and_partner_category
  end
end
