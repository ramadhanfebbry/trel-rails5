class AddColumnForNewQuestionTypesToQuestions < ActiveRecord::Migration[5.0]
  def change
    add_column :questions, :display_value_bar, :boolean, :default => true
    add_column :questions, :orientation_choice_view, :integer, :default => 1
    add_column :questions, :display_choice_text_next_to_radio, :boolean, :default => true
    add_column :questions, :display_opposing_icon, :boolean, :default => true
    add_column :questions, :min_threshold, :integer, :default => 0
    add_column :questions, :max_threshold, :integer, :default => 0
  end
end