class AddNewGenderToUsers < ActiveRecord::Migration[5.0]
  def change
    execute("CREATE TYPE new_gender AS ENUM ('0', '1', '2','3')")
    execute("ALTER TABLE users DROP COLUMN gender;")
    execute("ALTER TABLE users ADD COLUMN gender new_gender DEFAULT NULL;")
  end
end
