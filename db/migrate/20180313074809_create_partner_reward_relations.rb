class CreatePartnerRewardRelations < ActiveRecord::Migration[5.0]
  def self.up
    create_table :partner_reward_categories do |t|
      t.integer :partner_reward_id
      t.integer :category_id
      t.boolean :mandatory

      t.timestamps
    end

    create_table :partner_reward_discounts do |t|
      t.integer :partner_reward_id
      t.integer :category_id
      t.boolean :mandatory

      t.timestamps
    end

    add_column :partner_categories, :restaurant_id, :integer
    remove_column :partner_categories, :mandatory
    remove_column :partner_rewards, :order_from_category
    remove_column :partner_rewards, :discount_from_category
  end

  def self.down
    drop_table :partner_reward_categories
    drop_table :partner_reward_discounts
    remove_column :partner_categories, :restaurant_id
    add_column :partner_categories, :mandatory, :boolean
    add_column :partner_rewards, :order_from_category, :string
    add_column :partner_rewards, :discount_from_category, :string
  end
end
