class AddColumnOnCommentOpenStruct < ActiveRecord::Migration[5.0]
  def up
    add_column :comments,:content, :text
  end

  def down
    remove_column :comments,:content, :text
  end
end
