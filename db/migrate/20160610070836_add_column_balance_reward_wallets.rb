class AddColumnBalanceRewardWallets < ActiveRecord::Migration[5.0]
  def up
    add_column :reward_wallets,:rest_balance, :float, :default => nil
  end

  def down
    remove_column :reward_wallets,:rest_balance
  end
end
