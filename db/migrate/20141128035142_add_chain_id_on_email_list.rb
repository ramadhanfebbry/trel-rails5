class AddChainIdOnEmailList < ActiveRecord::Migration[5.0]
  def up
    add_column :comments, :chain_id, :integer
    add_index :comments, :chain_id
  end

  def down
    remove_column :comments, :chain_id
  end
end
