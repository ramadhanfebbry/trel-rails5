class AddFromUserIdToPromoCodes < ActiveRecord::Migration[5.0]
  def change
    add_column :promo_codes, :from_user_id, :integer
  end
end