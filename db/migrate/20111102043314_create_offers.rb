class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.string :name
      t.decimal :multiplier, precision: 8, scale: 2
      t.date :effectiveDate
      t.date :expiryDate
      t.string  :timeStart
      t.string  :timeEnd
      t.integer :daysOfWeek
      t.boolean :isActive
      t.text :fineprint
      t.integer :bonus_points
      t.integer :bonus_points_ftu
      t.references :chain
      
      t.timestamps
    end
    
  end
end
