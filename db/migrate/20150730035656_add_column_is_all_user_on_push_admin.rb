class AddColumnIsAllUserOnPushAdmin < ActiveRecord::Migration[5.0]
  def up
    add_column :plain_push_notifications, :is_all_user , :boolean, :default => false
    add_column :push_rewards, :is_all_user , :boolean, :default => false
    add_column :push_points, :is_all_user , :boolean, :default => false
  end

  def down
    remove_column :plain_push_notifications, :is_all_user
    remove_column :push_rewards, :is_all_user
    remove_column :push_points, :is_all_user
  end
end
