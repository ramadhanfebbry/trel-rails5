class AddBirthyearToUsers < ActiveRecord::Migration[5.0]
  def up
    remove_column :users, :birthday
    remove_column :users, :birthmonth
    add_column :users, :birth_day, :date
  end

  def down
    add_column :users, :birthday, :integer
    add_column :users, :birthmonth, :integer
    remove_column :users, :birth_day
  end
end
