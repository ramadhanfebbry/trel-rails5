class CreateGiftcardSkins < ActiveRecord::Migration[5.0]
  def change
    create_table :giftcard_skins do |t|
      t.integer :chain_id
      t.string :title
      t.text :description
      t.has_attached_file :app_image
      t.has_attached_file :email_header_image
      t.timestamps
    end
    add_index :giftcard_skins, :chain_id
  end
end
