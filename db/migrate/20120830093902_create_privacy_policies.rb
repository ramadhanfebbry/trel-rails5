class CreatePrivacyPolicies < ActiveRecord::Migration[5.0]
  def change
    create_table :privacy_policies, :force => true do |t|
      t.text :content
      t.integer :locale_id

      t.timestamps
    end
  end
end
