class AddSyncNowStatusToEmmaSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :emma_settings, :sync_now_status, :string

  end
end
