class CreateSubmitReceipts < ActiveRecord::Migration[5.0]
  def change
    create_table :submit_receipts, :force => true do |t|
      t.float :minimum_subtotal
      t.text :fine_print
      t.integer :survey_id

      t.timestamps
    end
  end
end
