class ChangeSurveyUserShowDashboard < ActiveRecord::Migration[5.0]
  def up
    change_column :surveys_users , :show_dashboard, :boolean,:default => true
  end

  def down
    change_column :surveys_users , :show_dashboard, :boolean,:default => false
  end
end
