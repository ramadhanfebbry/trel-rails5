class CreatePosDiscountTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :pos_discount_types do |t|
      t.integer :reward_id
      t.string :title
      t.integer :discount_type
      t.decimal :discount_amount
      t.timestamps
    end
    add_index :pos_discount_types, :reward_id
  end
end
