class AddPosUsedTypeToPosRewardChecks < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_reward_checks, :pos_used_type, :integer, :default => 1

  end
end
