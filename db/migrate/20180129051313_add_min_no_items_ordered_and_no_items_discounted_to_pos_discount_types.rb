class AddMinNoItemsOrderedAndNoItemsDiscountedToPosDiscountTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_discount_types, :min_no_items_ordered, :integer, :default => 1
    add_column :pos_discount_types, :min_no_items_discounted, :integer, :default => 1
  end
end
