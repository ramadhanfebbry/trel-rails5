class ChangeComment < ActiveRecord::Migration[5.0]
  def up
    add_column :comments, :subject, :string
  end

  def down
    remove_column :comments, :subject
  end
end
