class ChangeColumnOnChainsTables < ActiveRecord::Migration[5.0]
  def up
    remove_column :chains, :new_push_reward
    add_column :chains, :reward_timer, :integer , :default => 0
  end

  def down
  end
end
