class ChangeDefaultValueForMaxRotatingQuestionsOnSurveys < ActiveRecord::Migration[5.0]
  def up
    change_column :surveys, :max_rotating_questions, :integer, :default => 0
    Survey.all.each do |survey|
      survey.update_attribute(:max_rotating_questions, 0)
    end
  end

  def down
    change_column :surveys, :max_rotating_questions, :integer, :default => 2
  end
end
