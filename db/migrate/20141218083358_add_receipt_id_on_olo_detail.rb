class AddReceiptIdOnOloDetail < ActiveRecord::Migration[5.0]
  def up
    add_column :olo_order_details, :receipt_id , :integer
    add_index :olo_order_details , :receipt_id
  end

  def down
    remove_column :olo_order_details, :receipt_id
  end
end
