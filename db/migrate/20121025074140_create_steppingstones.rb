class CreateSteppingstones < ActiveRecord::Migration[5.0]
  def change
    create_table :steppingstones do |t|
      t.string :step_type
      t.boolean :resetable, :default => false
      t.integer :chain_id

      t.timestamps
    end
  end
end
