class AddLastPushedAtToUserRepeatedPromotions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_repeated_promotions, :last_pushed_at, :datetime
  end
end
