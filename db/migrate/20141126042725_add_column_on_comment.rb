class AddColumnOnComment < ActiveRecord::Migration[5.0]
  def up
    add_column :comments, :receipt_id , :integer
  end

  def down
    remove_column :comments, :receipt_id , :integer
  end
end
