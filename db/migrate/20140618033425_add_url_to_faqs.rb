class AddUrlToFaqs < ActiveRecord::Migration[5.0]
  def self.up
    add_column :faqs, :url, :string
  end

  def self.down
    remove_column :faqs, :url
  end
end
