class RemoveAppkeyFromApplications < ActiveRecord::Migration[5.0]
  def up
    remove_column :applications, :appkey
  end

  def down
    add_column :applications, :appkey, :string
  end
end
