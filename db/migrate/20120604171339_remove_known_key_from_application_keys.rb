class RemoveKnownKeyFromApplicationKeys < ActiveRecord::Migration[5.0]
  def up
    remove_column :application_keys, :known_key
  end

  def down
    add_column :application_keys, :known_key, :string
  end
end
