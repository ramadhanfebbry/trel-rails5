class AddCheckPhpMigrationToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :check_php_migration, :boolean
  end
end
