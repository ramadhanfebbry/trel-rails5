class AddPushTypeToPushPoints < ActiveRecord::Migration[5.0]
  def change
    add_column :push_points, :push_type, :integer, :default => 1
    add_column :push_points, :user_ids, :text
  end
end
