class CreateFacebookInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :facebook_infos do |t|
      t.integer :chain_id
      t.integer :user_id
      t.string :fb_id
      t.string :fb_name
      t.string :fb_fname
      t.string :fb_lname
      t.string :fb_link
      t.string :fb_gender
      t.string :fb_locale
      t.string :fb_age_range

      t.timestamps
    end
    add_index :facebook_infos, :chain_id
    add_index :facebook_infos, :user_id
    add_index :facebook_infos, :fb_id
  end
end
