class AddColumnOnOwnerJob < ActiveRecord::Migration[5.0]
  def up
    add_column :owner_jobs,:executed_at ,:datetime
    add_column :owner_jobs, :subject, :text
    add_column :owner_jobs, :content, :text
    add_column :owner_jobs, :push_email, :boolean, :default => true
    add_column :owner_jobs, :push_phone, :boolean, :default => true
  end

  def down
  end
end
