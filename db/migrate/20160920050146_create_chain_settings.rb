class CreateChainSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :chain_settings do |t|
      t.integer :chain_id
      t.boolean :hybrid_milestone_regular_rewards_enabled, :default => false
      t.timestamps
    end

    add_index :chain_settings, :chain_id

    Chain.all.each do |chain|
      ChainSetting.create(:chain_id => chain.id)
    end
  end
end
