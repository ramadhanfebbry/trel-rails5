class CreateFishbowlRewardPromotionSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :fishbowl_reward_promotion_settings do |t|
      t.integer :reward_id
      t.string :promotion_id
      t.integer :code_batch_size

      t.timestamps
    end
    add_index :fishbowl_reward_promotion_settings, :reward_id
  end
end
