class AddColumnTaskDefId < ActiveRecord::Migration[5.0]
  def up
    add_column :owner_jobs, :task_definition_id, :integer, :default => nil
    add_column :owner_jobs, :miss_you_reward_fishbowl_mailing_id, :string
    add_column :owner_jobs, :miss_you_point_fishbowl_mailing_id, :string
    add_column :owner_jobs, :is_fishbowl, :boolean, :default => false
    add_column :owner_jobs, :is_standard_email, :boolean, :default => false
  end

  def down
    remove_column :owner_jobs, :task_definition_id, :miss_you_reward_fishbowl_mailing_id,:miss_you_point_fishbowl_mailing_id,:is_fishbowl,:is_standard_email
  end
end
