class ChangeColumnPartnerRewardsAndCreatePartnerHours < ActiveRecord::Migration[5.0]
  def up
    create_table :partner_hours do |t|
      t.integer :partner_reward_id
      t.integer :day_of_week
      t.time :start
      t.time :end

      t.timestamps
    end

    remove_column :partner_rewards, :valid_days_of_week_and_time
    remove_column :partner_rewards, :order_from_category
    remove_column :partner_rewards, :discount_from_category

    execute "ALTER TABLE partner_rewards ADD COLUMN order_from_category BIGINT[]"
    execute "ALTER TABLE partner_rewards ADD COLUMN discount_from_category BIGINT[]"
    add_index :partner_hours, :partner_reward_id
  end

  def down
    drop_table :partner_hours

    add_column :partner_rewards, :valid_days_of_week_and_time, :datetime
    add_column :partner_rewards, :order_from_category, :string
    add_column :partner_rewards, :discount_from_category, :string
  end
end
