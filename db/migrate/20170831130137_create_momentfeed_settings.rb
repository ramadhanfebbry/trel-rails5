class CreateMomentfeedSettings < ActiveRecord::Migration[5.0]
  def change
    unless table_exists?(:momentfeed_settings)    
      create_table :momentfeed_settings do |t|
        t.string :api_key
        t.string :base_url
        t.string :update_type
        t.integer :update_interval
        t.string :update_interval_option
        t.integer :chain_id

        t.timestamps
      end
    end
  end
end
