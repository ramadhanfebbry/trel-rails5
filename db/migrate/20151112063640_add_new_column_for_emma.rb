class AddNewColumnForEmma < ActiveRecord::Migration[5.0]
  def up
    add_column :chains, :emma_ignore_marketing_optin, :boolean, default: false
    add_column :emma_marketings, :status, :integer
    add_column :emma_marketings, :signup_date, :datetime
  end

  def down
    remove_column :chains, :emma_ignore_marketing_optin
    remove_column :emma_marketings, :status
    remove_column :emma_marketings, :signup_date
  end
end
