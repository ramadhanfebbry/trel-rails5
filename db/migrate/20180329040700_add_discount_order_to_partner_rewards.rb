class AddDiscountOrderToPartnerRewards < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_rewards, :discount_order, :integer, default: 0
  end
end
