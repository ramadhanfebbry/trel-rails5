class AddAutoLoyaltyToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :auto_loyalty, :boolean, :default => false
  end
end
