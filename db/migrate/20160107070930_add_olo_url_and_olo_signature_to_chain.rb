class AddOloUrlAndOloSignatureToChain < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :olo_url, :string

    add_column :chains, :olo_signature, :string

  end
end
