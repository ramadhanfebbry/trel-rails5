class AddYearRequiredToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :birthday_required, :boolean, :default => false
  end
end
