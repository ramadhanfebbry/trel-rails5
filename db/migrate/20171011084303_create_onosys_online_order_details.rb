class CreateOnosysOnlineOrderDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :onosys_online_order_details do |t|
      t.integer :onosys_online_order_id
      t.text :order_detail
      t.decimal :sub_total
      t.decimal :tax
      t.decimal :total
      t.decimal :points, :default => 0
      t.timestamps
    end
    add_index :onosys_online_order_details, :onosys_online_order_id
  end
end
