class AddEmployeeNumAndTblNumToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :emp_num, :string
    add_column :pos_check_uploads, :tbl_num, :string
    add_index :pos_check_uploads, :emp_num
    add_index :pos_check_uploads, :tbl_num
    add_index :pos_check_uploads, [:rvc_num, :emp_num]
  end
end
