class ChangeTableReferralCodes < ActiveRecord::Migration[5.0]
  def up
    drop_table(:referral_codes)

    create_table :referral_programs do |t|
      t.string :name
      t.text :description
      t.integer :chain_id
      t.date :start_date
      t.date :expiry_date

      t.timestamps
    end

    add_index(:referral_programs, :chain_id)
  end

  def down
    drop_table :referral_programs
  end
end
