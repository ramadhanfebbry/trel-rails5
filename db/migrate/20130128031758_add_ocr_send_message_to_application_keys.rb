class AddOcrSendMessageToApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :application_keys, :ocr_send_message, :boolean, :default => false
  end
end