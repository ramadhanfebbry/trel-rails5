class AddPushedToRitasUserRewards < ActiveRecord::Migration[5.0]
  def change
    add_column :ritas_user_rewards, :pushed, :boolean, :default => false
    add_index :ritas_user_rewards, :email

  end
end
