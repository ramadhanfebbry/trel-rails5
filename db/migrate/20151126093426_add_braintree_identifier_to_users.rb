class AddBraintreeIdentifierToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :braintree_identifier, :string
    User.select("id, braintree_user_created").where(:braintree_user_created => true).each do |user|
      user.update_column(:braintree_identifier, user.id)
    end
  end
end
