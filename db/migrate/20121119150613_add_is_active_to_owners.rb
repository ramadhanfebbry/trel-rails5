class AddIsActiveToOwners < ActiveRecord::Migration[5.0]
  def change
    add_column :owners, :is_active, :boolean, :default => true
  end
end
