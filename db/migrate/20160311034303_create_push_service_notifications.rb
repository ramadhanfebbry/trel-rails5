class CreatePushServiceNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :push_service_notifications do |t|
      t.integer :chain_id
      t.text :notification
      t.text :receiver_user_ids, array: true, default: []
      t.datetime :sent_at
      t.integer :recipient_count

      t.timestamps
    end
  end
end
