class AddMinMaxLimitToTags < ActiveRecord::Migration[5.0]
  def change
    add_column :tags, :min, :integer, :default => 0
    add_column :tags, :max, :integer, :default => 0
  end
end