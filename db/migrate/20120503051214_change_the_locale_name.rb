class ChangeTheLocaleName < ActiveRecord::Migration[5.0]
  def up
  	# rename_column :users, :locale, :locale_id
  	# rename_column :chains ,:locale, :locale_id
  	# change_column :users, :locale_id,  :integer
  	# change_column :chains, :locale_id, :integer
  	remove_column :users, :locale
  	remove_column :chains, :locale
  	add_column :users,:locale_id, :integer
  	add_column :chains,:locale_id, :integer
  end

  def down
  end
end
