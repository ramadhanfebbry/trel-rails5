class CreateGeneralPosMenuItems < ActiveRecord::Migration[5.0]
  def change
    create_table :general_pos_menu_items do |t|
      t.integer :general_menu_item_id
      t.integer :pos_menu_item_id

      t.timestamps
    end
    add_index :general_pos_menu_items, :general_menu_item_id
    add_index :general_pos_menu_items, :pos_menu_item_id
  end
end
