class CreateGiftcardTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :giftcard_transactions do |t|
      t.integer :chain_id
      t.string :barcode_file_name
      t.string :barcode_content_type
      t.string :barcode_file_size
      t.string :barcode_updated_at
      t.string :giftcard_amount
      t.string :giftee_email
      t.string :giftee_name
      t.text :custom_message
      t.string :gifter_name

      t.timestamps
    end
    add_index :giftcard_transactions, :chain_id
  end

end