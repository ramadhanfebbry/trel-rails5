class DropRoleTable < ActiveRecord::Migration[5.0]
  def change
    drop_table :roles if self.table_exists?("roles")
    remove_column :admins, :role_id
    add_column :admins, :role, :string, :default=>'admin'

  end
  def self.table_exists?(name)
    ActiveRecord::Base.connection.tables.include?(name)
  end
end