class AddBackupsToRestaurantDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurant_details, :backups, :text if table_exists?(:restaurant_details)
  end
end
