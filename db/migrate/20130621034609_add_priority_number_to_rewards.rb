class AddPriorityNumberToRewards < ActiveRecord::Migration[5.0]

  def change
    add_column :rewards, :priority_number, :integer, :default => 0
  end

end
