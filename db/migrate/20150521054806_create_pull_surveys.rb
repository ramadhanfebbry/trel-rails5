class CreatePullSurveys < ActiveRecord::Migration[5.0]
  def change
    create_table :pull_surveys do |t|
      t.integer :chain_id
      t.integer :receipt_id
      t.integer :survey_id
      t.integer :user_id
      t.boolean :skipped, :default => false
      t.timestamps
    end
    execute "create index index_receipt_id_pull_surveys on pull_surveys (receipt_id);"
    execute "create index index_chain_id_pull_surveys on pull_surveys (chain_id);"
    execute "create index index_survey_id_pull_surveys on pull_surveys (survey_id);"
    execute "create index index_user_id_pull_surveys on pull_surveys (user_id);"
  end
end