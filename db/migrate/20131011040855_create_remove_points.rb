class CreateRemovePoints < ActiveRecord::Migration[5.0]
  def change
    create_table :remove_points do |t|
      t.integer :chain_id
      t.integer :points
      t.text :notes

      t.timestamps
    end

    add_index :remove_points, :chain_id
  end
end
