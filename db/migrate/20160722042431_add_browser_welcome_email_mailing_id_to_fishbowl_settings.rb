class AddBrowserWelcomeEmailMailingIdToFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :browser_welcome_email_mailing_id, :integer
  end
end
