class CreateUserActivityNotificationFields < ActiveRecord::Migration[5.0]
  def change
    create_table :user_activity_notification_fields, :force => true do |t|
      t.integer :user_activity_notification_id
      t.string :name
      t.string :description
      t.string :identifier

      t.timestamps
    end
    add_index :user_activity_notification_fields, [:user_activity_notification_id, :identifier], :name => "notif_by_data_key_index"
  end
end
