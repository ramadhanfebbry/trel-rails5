class AddCodeTypeToUserSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_sessions, :code_type, :integer
    add_index :user_sessions, :code_type
    add_index :user_sessions, [:chain_id, :code, :code_type]
  end
end
