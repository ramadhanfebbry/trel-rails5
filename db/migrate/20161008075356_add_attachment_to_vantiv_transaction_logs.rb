class AddAttachmentToVantivTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    add_column :vantiv_transaction_logs, :barcode_file_name, :string
    add_column :vantiv_transaction_logs, :barcode_content_type, :string
    add_column :vantiv_transaction_logs, :barcode_file_size, :integer
    add_column :vantiv_transaction_logs, :barcode_updated_at, :datetime
  end
end
