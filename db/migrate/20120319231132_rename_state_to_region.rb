class RenameStateToRegion < ActiveRecord::Migration[5.0]
  def change
    remove_column :states, :id
    rename_table :states, :regions
    add_column :regions, :id, :primary_key

    change_table :cities do |t|
      t.references :region
    end

    remove_column :cities, :state_id
    
  end

end
