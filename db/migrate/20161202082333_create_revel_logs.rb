class CreateRevelLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :revel_logs do |t|
      t.integer :delayed_job_id
      t.string :status
      t.text :success_users
      t.text :failed_users
      t.integer :chain_id
      t.string :log_type
      t.datetime :initiate_at
      t.datetime :completed_at
      t.text :error_report
      t.string :file_name
      t.timestamps
    end

    add_index :revel_logs, :chain_id
  end
end
