class AddTagIdToBucketPaths < ActiveRecord::Migration[5.0]
  def change
    add_column :bucket_paths, :tag_id, :integer
  end
end