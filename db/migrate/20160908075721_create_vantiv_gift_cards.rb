class CreateVantivGiftCards < ActiveRecord::Migration[5.0]
  def change
    create_table :vantiv_gift_cards do |t|
      t.string :card_number
      t.string :expiration_month
      t.string :expiration_year
      t.string :cvv
      t.string :cvv2
      t.string :security_code
      t.integer :chain_id
      t.boolean :used, default: false

      t.timestamps
    end

    add_index :vantiv_gift_cards, :chain_id
    add_index :vantiv_gift_cards, :card_number
  end
end