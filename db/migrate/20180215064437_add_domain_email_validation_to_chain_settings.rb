class AddDomainEmailValidationToChainSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :chain_settings, :domain_email_validation, :boolean, default: true
  end
end
