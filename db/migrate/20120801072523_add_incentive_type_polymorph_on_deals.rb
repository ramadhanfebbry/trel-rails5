class AddIncentiveTypePolymorphOnDeals < ActiveRecord::Migration[5.0]
  def up
    add_column :deals, :incentive_type, :string
    add_column :deals, :incentive_id, :integer
  end

  def down
    remove_column(:deals, :incentive_type,:incentive_id)
  end
end
