class AddSocialLinkToRestaurants < ActiveRecord::Migration[5.0]
  def change
  	add_column :restaurants, :social_link, :string unless column_exists?(:restaurants, :social_link)    
  end
end
