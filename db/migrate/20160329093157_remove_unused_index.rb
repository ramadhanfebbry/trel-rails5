class RemoveUnusedIndex < ActiveRecord::Migration[5.0]
  def up
    remove_index :user_device_logs, [:chain_id, :user_id, :keychain_value] if index_exists?(:user_device_logs, [:chain_id, :user_id, :keychain_value])
    remove_index :user_device_logs, :keychain_value  if index_exists?(:user_device_logs, :keychain_value)
    remove_index :barcodes, :code if index_exists?(:user_device_logs, :keychain_value)
    remove_index :barcodes, :chain_id if index_exists?(:barcodes, :code)
    remove_index :receipts, :application_id if index_exists?(:receipts, :application_id)
    remove_index :receipt_transactions, :reject_reason_id if index_exists?(:receipt_transactions, :reject_reason_id)
    remove_index :receipt_transactions, :receipt_time if index_exists?(:receipt_transactions, :receipt_time)
    remove_index :receipt_transactions, [:receipt_date, :receipt_time] if index_exists?(:receipt_transactions, [:receipt_date, :receipt_time])
    remove_index :receipt_transactions, :receipt_date if index_exists?(:receipt_transactions, :receipt_date)
    remove_index :pos_check_uploads, :rvc_num if index_exists?(:pos_check_uploads, :rvc_num)
    remove_index :pos_check_uploads, :user_code if index_exists?(:pos_check_uploads, :user_code)
    remove_index :pos_check_uploads, :is_xpient_check if index_exists?(:pos_check_uploads, :is_xpient_check)
    remove_index :receipt_statuses , [:receipt_transaction_id, :status] if index_exists?(:receipt_statuses , [:receipt_transaction_id, :status])
    remove_index :receipt_statuses , :receipt_transaction_id if index_exists?(:receipt_statuses , :receipt_transaction_id )
    remove_index :receipt_statuses , :delayed_id if index_exists?(:receipt_statuses , :delayed_id)
    remove_index :reward_pos_codes, :reward_id if index_exists?(:reward_pos_codes, :reward_id)
    remove_index :reward_pos_codes, :user_id if index_exists?(:reward_pos_codes, :user_id )
    remove_index :reward_pos_codes, [:user_id, :reward_id] if index_exists?( :reward_pos_codes, [:user_id, :reward_id])
    remove_index(:day_visits, :chain_id) if index_exists?(:day_visits, :chain_id)
    remove_index :users, :last_name if index_exists?(:users, :last_name)
    remove_index :users, :first_name if index_exists?(:users, :first_name)
    remove_index :users, [:first_name, :last_name] if index_exists?(:users, [:first_name, :last_name])
    remove_index :user_devices, [:user_id, :keychain_value] if index_exists?(:user_devices, [:user_id, :keychain_value])
    remove_index(:delayed_jobs, :chain_id) if index_exists?(:delayed_jobs, :chain_id)
    remove_index :surveys_users, :reward_id if index_exists?(:surveys_users, :reward_id)
    remove_index(:surveys_users, :qualified_survey_incentive) if index_exists?(:surveys_users, :qualified_survey_incentive)
    remove_index :surveys_users, :offer_id if index_exists?(:surveys_users, :offer_id)
    remove_index :point_histories, :group_no if index_exists?(:point_histories, :group_no)
    remove_index :promo_codes, :code, name: 'index_promo_codes_on_code' if index_exists?(:promo_codes, :code, name: 'index_promo_codes_on_code' )
    remove_index :user_sessions, :mp_processed if index_exists?(:user_sessions, :mp_processed)
    remove_index :user_sessions, :receipt_id if index_exists?(:user_sessions, :receipt_id)
    remove_index :user_sessions, [:chain_id, :code] if index_exists?(:user_sessions, [:chain_id, :code])
  end


  def down
    add_index :user_device_logs, [:chain_id, :user_id, :keychain_value], :name => 'user_device_log_cukey_index'
    add_index :user_device_logs, :keychain_value
    add_index :barcodes, :code
    add_index :barcodes, :chain_id
    add_index :receipts, :application_id
    add_index :receipt_transactions, :reject_reason_id
    add_index :receipt_transactions, :receipt_time
    add_index :receipt_transactions, [:receipt_date, :receipt_time]
    add_index :receipt_transactions, :receipt_date
    add_index :pos_check_uploads, :rvc_num, :name => "idx_rvc_num_pcu"
    add_index :pos_check_uploads, :user_code
    add_index :pos_check_uploads, :is_xpient_check
    add_index :receipt_statuses , [:receipt_transaction_id, :status], :name => "receipt_status_index"
    add_index :receipt_statuses , :receipt_transaction_id
    add_index :receipt_statuses , :delayed_id
    add_index :reward_pos_codes, :reward_id
    add_index :reward_pos_codes, :user_id
    add_index :reward_pos_codes, [:user_id, :reward_id]
    add_index(:day_visits, :chain_id)
    add_index :users, :last_name
    add_index :users, :first_name
    add_index :users, [:first_name, :last_name]
    add_index :user_devices, [:user_id, :keychain_value]
    add_index(:delayed_jobs, :chain_id)
    add_index :surveys_users, :reward_id
    add_index(:surveys_users, :qualified_survey_incentive)
    add_index :surveys_users, :offer_id
    add_index :point_histories, :group_no
    add_index :promo_codes, :code
    add_index :user_sessions, :mp_processed
    add_index :user_sessions, :receipt_id
    add_index :user_sessions, [:chain_id, :code], :unique => true
  end
end
