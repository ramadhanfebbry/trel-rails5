class AddIsStaticToUserSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_sessions, :is_static, :boolean, :default => false
    remove_index :user_sessions, [:chain_id, :code]
    add_index :user_sessions, [:chain_id, :code], :unique => true
  end
end
