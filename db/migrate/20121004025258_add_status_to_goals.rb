class AddStatusToGoals < ActiveRecord::Migration[5.0]
  def change
    add_column :goals, :status, :integer, :default => 1
  end
end
