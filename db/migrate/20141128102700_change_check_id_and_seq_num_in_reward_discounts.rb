class ChangeCheckIdAndSeqNumInRewardDiscounts < ActiveRecord::Migration[5.0]
  def up
    change_column :reward_discounts, :check_id, :string
    change_column :reward_discounts, :seq_num, :string
  end

  def down
    change_column :reward_discounts, :check_id, :integer
    change_column :reward_discounts, :seq_num, :integer
  end
end
