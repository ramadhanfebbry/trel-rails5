class AddColumnPosLocationForNetPos < ActiveRecord::Migration[5.0]
  def up
    add_column :pos_locations,:POS_StoreID , :string
    add_column :pos_locations,:POS_ChainID , :string

  end

  def down
    remove_column :pos_locations, :POS_ChainID
    remove_column :pos_locations, :POS_ChainID
  end
end
