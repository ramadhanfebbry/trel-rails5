class CreateArchivedXmlReceipts < ActiveRecord::Migration[5.0]
  def change
    create_table :archived_xml_receipts do |t|
      t.integer :receipt_id
      t.text :xml_data_receipt

      t.timestamps
    end
  end
end
