class CreateHybridRewardRedemptionFlows < ActiveRecord::Migration[5.0]
  def change
    execute "CREATE TYPE hybrid_redemption_flow AS ENUM ('app_based', 'pos_based');"
    create_table :hybrid_reward_redemption_flows do |t|
      t.integer :chain_id
      t.integer :restaurant_id
      t.column :flow,  "hybrid_redemption_flow"
      t.timestamps
    end
  end
end
