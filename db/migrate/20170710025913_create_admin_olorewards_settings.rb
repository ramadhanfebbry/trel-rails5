class CreateAdminOlorewardsSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :olorewards_settings do |t|
      t.string :basic_auth
      t.integer :chain_id
      t.string :basic_password

      t.timestamps
    end
  end
end