class AddSurveyEmailToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :survey_email, :string
  end
end