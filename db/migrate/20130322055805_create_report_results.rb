class CreateReportResults < ActiveRecord::Migration[5.0]
  def change
    create_table :report_results do |t|
      t.integer :subscription_id
      t.string :uuid, :filename
      t.text :jasper_session
      t.timestamps
    end

    add_index :report_results, :subscription_id
  end
end
