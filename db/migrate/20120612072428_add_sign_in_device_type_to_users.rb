class AddSignInDeviceTypeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :sign_in_device_type, :string
  end
end
