class AddIncentiveTypeAndIncentiveIdToGoals < ActiveRecord::Migration[5.0]
  def change
    add_column :goals, :incentive_type, :string
    add_column :goals, :incentive_id, :integer
  end
end
