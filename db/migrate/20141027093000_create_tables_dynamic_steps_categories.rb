class CreateTablesDynamicStepsCategories < ActiveRecord::Migration[5.0]
  def change

    #remove_column :steppingstone_steps , :randomize, :category_type, :min_amount, :max_amount
    create_table :userstep_categories do |t|
      t.integer :steppingstone_step_id
      t.integer :user_type
      t.integer :reward_id
      t.timestamps
    end

    add_index :userstep_categories,:steppingstone_step_id
    add_index :userstep_categories,[:steppingstone_step_id, :user_type], :name => "index_user_and_id"
  end
end
