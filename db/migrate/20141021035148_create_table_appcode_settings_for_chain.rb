class CreateTableAppcodeSettingsForChain < ActiveRecord::Migration[5.0]
  def change
    create_table :appcode_settings do |t|
      t.integer :chain_id
      t.string :barcode_format
      t.string :barcode_format_type
      t.string :pos_digit_barcode
      t.integer :appcode_type

      t.timestamps
    end

    add_index :appcode_settings, :chain_id
  end
end
