class AddPointsAndPaymentNotificationMailingIdToFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :points_notification_mailing_id, :string
    add_column :fishbowl_settings, :payment_notification_mailing_id, :string
  end
end
