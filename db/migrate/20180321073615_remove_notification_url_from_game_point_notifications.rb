class RemoveNotificationUrlFromGamePointNotifications < ActiveRecord::Migration[5.0]
  def change
    remove_column :game_point_notifications, :notification_url
  end
end
