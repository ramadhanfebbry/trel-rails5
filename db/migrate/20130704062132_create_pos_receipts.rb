class CreatePosReceipts < ActiveRecord::Migration[5.0]
  def change
    create_table :pos_receipts do |t|
      t.datetime :receipt_time
      t.float :total
      t.float :tips
      t.float :tax
      t.float :item_sales
      t.string :barcode
      t.text :menu_items
      t.timestamps
    end
  end
end