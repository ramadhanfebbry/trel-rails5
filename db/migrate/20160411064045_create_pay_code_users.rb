class CreatePayCodeUsers < ActiveRecord::Migration[5.0]
  def change
    unless table_exists?(:pay_code_users)    
      create_table :pay_code_users do |t|
        t.string :pay_code
        t.integer :user_id
        t.integer :chain_id
        t.datetime :expired_at
        t.timestamps
      end

      add_index :pay_code_users, :user_id
      add_index :pay_code_users, :chain_id
      add_index :pay_code_users, :pay_code
      add_index :pay_code_users, [:user_id, :pay_code ]
      add_index :pay_code_users, [:chain_id, :pay_code ], :unique => true
    end
  end
end
