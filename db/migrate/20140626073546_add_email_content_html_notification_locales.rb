class AddEmailContentHtmlNotificationLocales < ActiveRecord::Migration[5.0]
  def up
    add_column :notification_locales, :email_content_html, :text
    NotificationLocale.all.each do |nl|
      nl.update_column(:email_content_html, nl.email_content)
    end
  end

  def down
    remove_column :notification_locales, :email_content_html
  end
end