class CreateBucketPaths < ActiveRecord::Migration[5.0]
  def change
    create_table :bucket_paths do |t|
      t.integer :chain_id
      t.string :name
      t.string :define_as
      t.boolean :active
      t.boolean :resettable
      t.float :percentage_of_user

      t.timestamps
    end
    add_index :bucket_paths, :chain_id
  end
end