class CreateRewardMenuItems < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_menu_items do |t|
      t.integer :chain_id
      t.integer :reward_id
      t.integer :general_menu_item_id

      t.timestamps
    end
  end
end