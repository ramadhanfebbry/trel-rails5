class CreateReferralCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :referral_code_users do |t|
      t.integer :referral_program_id, :user_id
      t.string :code
      t.timestamps
    end

    add_index :referral_code_users, [:referral_program_id, :user_id, :code], :unique => true, :name => :uniq_referal_code
    add_index(:referral_code_users, :referral_program_id)
    add_index(:referral_code_users, :user_id)    
  end
end
