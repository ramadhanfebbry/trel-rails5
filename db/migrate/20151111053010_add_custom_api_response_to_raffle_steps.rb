class AddCustomApiResponseToRaffleSteps < ActiveRecord::Migration[5.0]
  def change
    add_column :raffle_steps, :custom_api_response, :text
    add_column :raffle_participation_histories, :custom_api_executed, :boolean, :default => false

  end
end
