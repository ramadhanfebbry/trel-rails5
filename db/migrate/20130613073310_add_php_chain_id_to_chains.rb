class AddPhpChainIdToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :php_chain_id, :integer
  end
end