class AddStatusToPushServiceNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :push_service_notifications, :status, :integer, :default => 0
  end
end
