class AddMigrationDateToGuestDnaUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :guest_dna_users, :migration_date, :datetime

  end
end
