class CreateUserPaymentHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :user_payment_histories do |t|
      t.integer :user_id
      t.integer :pos_check_upload_id
      t.boolean :success, :default => false
      t.integer :error_code
      t.string  :error_description
      t.integer :payment_type
      t.timestamps
    end
  end
end
