class AddFtpServerConfigToChain < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :ftp_url , :string
    add_column :chains, :ftp_port , :integer
    add_column :chains, :ftp_username , :string
    add_column :chains, :ftp_password , :string
    
  end
end