class AddReferralProgramTypeToReferralUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :referral_users, :referral_program_type, :integer, default: nil
  end
end
