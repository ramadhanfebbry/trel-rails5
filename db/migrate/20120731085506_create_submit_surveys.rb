class CreateSubmitSurveys < ActiveRecord::Migration[5.0]
  def change
    create_table :submit_surveys, :force => true do |t|
      t.integer :survey_id
      t.text :fine_print

      t.timestamps
    end
  end
end
