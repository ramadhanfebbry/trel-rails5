class AddColumnChainSettingNotifEmail < ActiveRecord::Migration[5.0]
  def up
    add_column :chain_settings, :user_email_confirmation,:boolean, :default => false
  end

  def down
    remove_column :chain_settings, :user_email_confirmation
  end
end
