class ChangeColumnOnReferralProgramAddDefault < ActiveRecord::Migration[5.0]
  def up
    remove_column :referral_programs, :min_approve, :approve
    add_column :referral_programs, :min_approve, :integer, :default => 0
    
    if !column_exists?(:referral_programs, :approve)
    	add_column :referral_programs, :approve, :boolean, :default => false
    end
    
  end

  def down
    remove_column :referral_programs, :min_approve, :approve
  end
end
