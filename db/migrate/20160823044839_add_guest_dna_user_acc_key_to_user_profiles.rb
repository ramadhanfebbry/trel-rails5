class AddGuestDnaUserAccKeyToUserProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :guest_dna_user_acc_key, :string
  end
end
