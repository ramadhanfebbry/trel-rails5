class AddColumnEmailSubjectOnReportDefinitions < ActiveRecord::Migration[5.0]
  def up
    add_column :report_definitions,:email_subject , :string
  end

  def down
    remove_column :report_definitions,:email_subject , :string
  end
end
