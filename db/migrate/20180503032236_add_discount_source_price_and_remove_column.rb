class AddDiscountSourcePriceAndRemoveColumn < ActiveRecord::Migration[5.0]
  def up
    add_column :partner_rewards, :discount_source_price, :integer, default: 0
    remove_column :partner_menu_items, :xml_data
  end

  def down
    remove_column :partner_rewards, :discount_source_price
    add_column :partner_menu_items, :xml_data, :string
  end
end
