class AddBarcodeBatchIdToBarcodes < ActiveRecord::Migration[5.0]
  def change
    add_column :barcodes, :barcode_batch_id, :integer
  end
end