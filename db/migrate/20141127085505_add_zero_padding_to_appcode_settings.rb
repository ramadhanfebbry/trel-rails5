class AddZeroPaddingToAppcodeSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :appcode_settings, :zero_padding, :integer, :default => 0
  end
end
