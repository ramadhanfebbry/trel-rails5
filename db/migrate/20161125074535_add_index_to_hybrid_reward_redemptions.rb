class AddIndexToHybridRewardRedemptions < ActiveRecord::Migration[5.0]
  def change
    add_index :hybrid_reward_redemption_flows, :chain_id
    add_index :hybrid_reward_redemption_flows, :flow
    add_index :hybrid_reward_redemption_flows, :restaurant_id
  end
end
