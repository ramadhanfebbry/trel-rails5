class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.references :reward
      t.references :offer
      t.references :user
      t.references :chain
      t.integer :balanceBefore
      t.integer :balanceAfter
      t.integer :balanceDiff
      t.boolean :isUndo
      
      t.timestamps
    end
  end
end
