class AddPatronIdToUserProfiles < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles, :patron_id, :string
    add_column :user_profiles, :external_relevant_user_id, :string
  end
end
