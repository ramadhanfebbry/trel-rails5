class CreateDelayedJobArchives < ActiveRecord::Migration[5.0]
  def change

    #<DelayedJob id: 58397, priority: 0, attempts: 0,
    #handler: "--- !ruby/struct:FishbowlJob \nchain: !ruby/ActiveRe...",
    #last_error: nil, run_at: "2014-09-01 07:15:00", locked_at: nil, failed_at: nil, locked_by: nil, created_at: "2014-09-01 04:05:41", updated_at: "2014-09-01 04:05:41", chain_id: nil, delayable_type: nil, delayable_id: nil, queue: "trel_job">
    create_table :delayed_job_archives do |t|
      t.integer :delayed_job_id, :prioriy, :attempts
      t.text :handler, :last_error
      t.datetime :run_at , :locked_at, :failed_at, :created_at, :updated_at
      t.integer :chain_id, :delayable_id
      t.string :delayable_type, :queue
      t.string :locked_by
      t.timestamps
    end
  end
end
