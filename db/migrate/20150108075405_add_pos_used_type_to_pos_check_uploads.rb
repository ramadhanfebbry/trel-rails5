class AddPosUsedTypeToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :pos_used_type, :integer, :default => 1
  end
end
