class CreateRestaurantRewards < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurant_rewards do |t|
      t.references :restaurant
      t.references :reward
      t.timestamps
    end
  end
end
