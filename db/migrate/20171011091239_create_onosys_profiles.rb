class CreateOnosysProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :onosys_profiles do |t|
      t.integer :user_id
      t.string :account_id
      t.text :access_token
      t.timestamps
    end
    add_index :onosys_profiles, :user_id
    add_index :onosys_profiles, :account_id
  end
end
