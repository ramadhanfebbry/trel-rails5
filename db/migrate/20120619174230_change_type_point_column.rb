class ChangeTypePointColumn < ActiveRecord::Migration[5.0]
  def up
    change_column :users, :points, :float
    change_column :rewards, :points, :float
    change_column :reward_transactions, :points, :float
    change_column :chains, :points, :float
    change_column :perks, :bonus_points, :float
    change_column :offers, :bonus_points, :float
    change_column :offers, :bonus_points_ftu, :float
  end

  def down
    change_column :users, :points, :integer
    change_column :rewards, :points, :integer
    change_column :reward_transactions, :points, :integer
    change_column :chains, :points, :integer
    change_column :perks, :bonus_points, :integer
    change_column :offers, :bonus_points, :integer
    change_column :offers, :bonus_points_ftu, :integer
  end
end
