class CreateGoals < ActiveRecord::Migration[5.0]
  def change
    create_table :goals do |t|
      t.integer :chain_id
      t.integer :category_id
      t.text :description

      t.timestamps
    end
  end
end
