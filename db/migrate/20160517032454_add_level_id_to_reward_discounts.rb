class AddLevelIdToRewardDiscounts < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_discounts, :level_size_id, :integer
    add_index :reward_discounts, :level_size_id
  end
end
