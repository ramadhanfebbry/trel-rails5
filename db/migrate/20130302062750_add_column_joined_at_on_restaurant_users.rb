class AddColumnJoinedAtOnRestaurantUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :restaurant_users, :joined_date, :date
  end

  def down
    remove_column :restaurant_users, :joined_date, :date
  end
end
