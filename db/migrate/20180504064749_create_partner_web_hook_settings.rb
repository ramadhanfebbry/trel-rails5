class CreatePartnerWebHookSettings < ActiveRecord::Migration[5.0]
  def change
  	unless table_exists?(:partner_webhook_settings)    
	    create_table :partner_webhook_settings do |t|
	      t.integer :chain_id
	      t.string :url

	      t.timestamps
	    end
	  end
  end
end
