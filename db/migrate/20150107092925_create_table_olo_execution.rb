class CreateTableOloExecution < ActiveRecord::Migration[5.0]
  def change
    create_table :olo_executions do |t|
      t.integer :chain_id
      t.date :executed_at
      t.integer :status
      t.timestamps
    end

    add_index :olo_executions , :executed_at
  end

end
