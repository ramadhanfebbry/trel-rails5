class CreateAdminGeoData < ActiveRecord::Migration[5.0]
  def change
    create_table :geo_data_settings do |t|
      t.integer :chain_id
      t.time :collect_start_time
      t.time :collect_end_time
      t.time :post_start_time
      t.time :post_data_time
      t.datetime :next_date_collect
      t.datetime :date_to_retry

      t.timestamps
    end

    add_index :geo_data_settings, :chain_id
  end
end
