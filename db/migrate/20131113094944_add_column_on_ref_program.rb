class AddColumnOnRefProgram < ActiveRecord::Migration[5.0]
  def up
    add_column :referral_programs, :min_approve, :integer
    add_column :referral_programs, :approve, :boolean
  end

  def down
    remove_column :referral_programs, :min_approve, :integer
  end
end
