class RemoveApplicationIdFromRapnsApp < ActiveRecord::Migration[5.0]
  def up
    remove_column :rapns_apps, :application_id
  end

  def down
    add_column :rapns_apps, :application_id, :integer
  end
end
