class CreatePromoCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :promo_codes do |t|
      t.string :code
      t.boolean :used, :default => false
      t.boolean :shared, :default => false
      t.references :promotion
      t.references :chain

      t.timestamps
    end
    add_index :promo_codes, :code
  end
  
end
