class ChangeMemberIdOnUserFishbowlToString < ActiveRecord::Migration[5.0]
  def up
    change_column :user_fishbowls, :member_id, :string
  end

  def down
    change_column :user_fishbowls, :member_id, :integer
  end
end