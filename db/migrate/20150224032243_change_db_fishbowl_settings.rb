class ChangeDbFishbowlSettings < ActiveRecord::Migration[5.0]
  def up
    change_column :fishbowl_settings, :site_id, :string
  end

  def down
    change_column :fishbowl_settings, :site_id, :integer
  end
end