class AddColumnReasonKeyToRejectReasons < ActiveRecord::Migration[5.0]
  def change
    add_column :reject_reasons, :reason_key, :string
  end
end
