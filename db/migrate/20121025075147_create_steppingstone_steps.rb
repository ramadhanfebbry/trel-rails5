class CreateSteppingstoneSteps < ActiveRecord::Migration[5.0]
  def change
    create_table :steppingstone_steps do |t|
      t.integer :steppingstone_id
      t.integer :property_one, :default => 0
      t.float :property_two, :default => 0
      t.integer :step_number, :default => 0
      t.integer :reward_id

      t.timestamps
    end
  end
end
