class AddChangeStatusLogToRestaurantDetail < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurant_details, :changed_status_log, :text if table_exists?(:restaurant_details)
  end
end
