class CreateReferralUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :referral_users do |t|
      t.integer :code_id, :user_id
      t.timestamps
    end

    add_index :referral_users, :code_id
    add_index :referral_users, :user_id
    add_index :referral_users, [:code_id,:user_id]
  end
end
