class AddStatusAasmToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :status, :string, :default => 'active'
  end
end
