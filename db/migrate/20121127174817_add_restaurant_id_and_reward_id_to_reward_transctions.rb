class AddRestaurantIdAndRewardIdToRewardTransctions < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_transactions, :restaurant_id, :integer
    add_column :reward_transactions, :reward_id, :integer
  end
end
