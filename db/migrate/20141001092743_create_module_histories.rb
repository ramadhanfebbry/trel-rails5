class CreateModuleHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :module_histories do |t|
      t.integer :user_id
      t.string :keychain
      t.string :android_id
      t.float :lat
      t.float :long
      t.string :module_name

      t.timestamps
    end
  end
end
