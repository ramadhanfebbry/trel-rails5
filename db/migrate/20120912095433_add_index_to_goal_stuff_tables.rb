class AddIndexToGoalStuffTables < ActiveRecord::Migration[5.0]
  def change
    add_index :goal_categories, :chain_id
    
    add_index :goal_category_locales, :category_id
    add_index :goal_category_locales, :locale_id
    add_index :goal_category_locales, [:category_id, :locale_id]
    
    add_index :goal_locales, :goal_id	
    add_index :goal_locales, :locale_id
    add_index :goal_locales, [:goal_id, :locale_id]

    add_index :goals, :chain_id
    add_index :goals, :category_id	
    add_index :goals, [:chain_id, :category_id]

    add_index :user_goal_categories, :user_id
    add_index :user_goal_categories, :category_id
    add_index :user_goal_categories, [:user_id, :category_id]

    add_index :user_goals, :user_id	
    add_index :user_goals, :goal_id
    add_index :user_goals, :week_id
    add_index :user_goals, [:user_id, :goal_id]
    add_index :user_goals, [:user_id, :goal_id, :week_id]
    
  end
end
