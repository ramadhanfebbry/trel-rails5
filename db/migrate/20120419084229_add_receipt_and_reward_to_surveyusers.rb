class AddReceiptAndRewardToSurveyusers < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys_users, :receipt_id, :integer
    add_column :surveys_users, :reward_id, :integer
  end
end
