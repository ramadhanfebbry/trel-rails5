class CreateAdminReferralCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :referral_codes do |t|
      t.string :code
      t.integer :chain_id
      t.date :start_date
      t.date :expiry_date

      t.timestamps
    end

    add_index(:referral_codes, :chain_id)
  end
end
