class CreateRestaurantOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :restaurant_offers do |t|
      t.references :restaurant
      t.references :offer
      t.timestamps
    end
  end
end
