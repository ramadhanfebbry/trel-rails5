class AddIndexUsersRefCode < ActiveRecord::Migration[5.0]
  def up
    add_index :users, :ref_code
  end

  def down
    remove_index :users, :ref_code
  end
end