class CreateGamePointNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :game_point_notifications do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.text :email_content
      t.string :email_subject
      t.text :notification

      t.timestamps
    end
  end
end
