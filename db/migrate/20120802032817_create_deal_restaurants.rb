class CreateDealRestaurants < ActiveRecord::Migration[5.0]
  def change
    create_table :deal_restaurants, :force => true do |t|
      t.integer :deal_id
      t.integer :restaurant_id

      t.timestamps
    end
  end
end
