class AddExecutedToUserSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_sessions, :executed, :boolean, :default => false

  end
end
