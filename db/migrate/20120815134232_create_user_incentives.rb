class CreateUserIncentives < ActiveRecord::Migration[5.0]
  def change
    create_table :user_incentives, :force => true do |t|
      t.string :incentive_type
      t.integer :incentive_id,:deal_id,:user_id
      t.timestamps
    end

    add_index(:user_incentives, [:incentive_type, :incentive_id])
    add_index(:user_incentives, :user_id)
    add_index(:user_incentives, :deal_id)
  end
end
