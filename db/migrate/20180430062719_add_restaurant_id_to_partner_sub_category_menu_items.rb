class AddRestaurantIdToPartnerSubCategoryMenuItems < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_sub_category_menu_items, :restaurant_id, :integer
    add_index :partner_sub_category_menu_items, :restaurant_id
  end
end
