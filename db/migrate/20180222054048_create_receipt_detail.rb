class CreateReceiptDetail < ActiveRecord::Migration[5.0]
  def change
    create_table :receipt_details do |t|
      t.integer :receipt_id
      t.date :issue_date
      t.time :issue_time
      t.string :timezone

      t.timestamps
    end
    add_index :receipt_details, :receipt_id
  end
end
