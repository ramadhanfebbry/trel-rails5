class AddIgnoreMarketingOptinToFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :ignore_marketing_optin, :boolean, :default => false

  end
end
