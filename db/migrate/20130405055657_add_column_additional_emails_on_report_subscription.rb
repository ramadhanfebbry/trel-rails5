class AddColumnAdditionalEmailsOnReportSubscription < ActiveRecord::Migration[5.0]
  def up
    add_column :report_subscriptions, :additional_emails, :text
  end

  def down
    remove_column :report_subscriptions, :additional_emails
  end
end
