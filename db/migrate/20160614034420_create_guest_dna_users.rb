class CreateGuestDnaUsers < ActiveRecord::Migration[5.0]
  def change
    execute("CREATE TYPE gender AS ENUM ('0', '1', '2')")
    execute("CREATE TYPE optin AS ENUM ('0', '1')")
    create_table :guest_dna_users do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :zip
      t.string :favorite_store
      t.integer :points
      t.date :birthday
      t.string :account_key
      t.timestamps
    end

    execute("ALTER TABLE guest_dna_users ADD COLUMN gender gender;")
    execute("ALTER TABLE guest_dna_users ADD COLUMN optin optin;")
    add_index :guest_dna_users, :email
  end
end
