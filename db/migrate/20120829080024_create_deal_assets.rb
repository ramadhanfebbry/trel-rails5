class CreateDealAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :deal_assets, :force => true do |t|
      t.string :image_file_name
      t.integer :image_file_size
      t.string :image_content_type
      t.datetime :image_updated_at
      t.integer :activity_id
      t.string :activity_type
      t.string :type

      t.timestamps
    end
  end
end
