class AddTransactionTypeToGiftCardRedemHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :gift_card_redem_histories, :transaction_type, :integer, :default => 0
    add_index :gift_card_redem_histories, :transaction_type

  end
end
