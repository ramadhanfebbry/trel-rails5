class CreateVextOnlineOrder < ActiveRecord::Migration[5.0]
  def up
    create_table :vext_online_orders do |t|
      t.integer :chain_id, :user_id, :loyalty_status, :platform
      t.integer :restaurant_id
      t.string :email, :online_order_identifier
      t.timestamps
    end

    add_index :vext_online_orders, :chain_id
    add_index :vext_online_orders, :online_order_identifier
    add_index :vext_online_orders, :restaurant_id
    add_index :vext_online_orders, :user_id

    create_table :vext_online_order_details do |t|
      t.integer :vext_online_order_id
      t.text :order_detail
      t.decimal :sub_total
      t.decimal :tax
      t.decimal :total
      t.decimal :points, :default => 0
      t.decimal :discount
    end
    add_index :vext_online_order_details, :vext_online_order_id

    create_table :vext_receipt_online_orders do |t|
      t.integer :receipt_id
      t.integer :vext_online_order_id
    end

    add_index :vext_receipt_online_orders, :receipt_id
    add_index :vext_receipt_online_orders, :vext_online_order_id
  end

  def down
    drop_table :vext_receipt_online_orders
    drop_table :vext_online_order_details
    drop_table :vext_online_orders
  end
end
