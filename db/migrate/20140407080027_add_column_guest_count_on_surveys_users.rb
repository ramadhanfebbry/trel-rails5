class AddColumnGuestCountOnSurveysUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :surveys_users, :guest_count, :integer, :default => 0
    GuestRelation.all.each do |gr|
      puts "Executing guest relation"
      sr = SurveysUser.find(gr.surveys_user_id)
      puts gr.status
      sr.update_column(:guest_count, sr.guest_count + 1) if gr.status == 1
    end
    puts "Done Executing guest relation"
  end

  def down
    remove_column :surveys_users, :guest_count
  end
end
