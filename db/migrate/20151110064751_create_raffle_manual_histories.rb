class CreateRaffleManualHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :raffle_manual_histories do |t|
      t.string :group
      t.integer :submitted_user_id
      t.integer :user_id
      t.integer :chain_id
      t.integer :raffle_id
      t.integer :raffle_step_id
      t.boolean :success
      t.string :message
      t.timestamps
    end

    add_index :raffle_manual_histories, :group
    add_index :raffle_manual_histories, :user_id
    add_index :raffle_manual_histories, :chain_id
    add_index :raffle_manual_histories, :raffle_id
    add_index :raffle_manual_histories, :raffle_step_id
  end
end
