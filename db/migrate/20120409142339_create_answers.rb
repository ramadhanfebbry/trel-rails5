class CreateAnswers < ActiveRecord::Migration[5.0]
  def change
    create_table :answers do |t|
      t.integer :valueId
      t.integer :answer_type
      t.references :user
      t.references :question

      t.timestamps
    end

    add_index :answers, [:user_id, :question_id], :unique => true
  end
end
