class AddReceiptIdToUserSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :user_sessions, :receipt_id, :integer
  end
end
