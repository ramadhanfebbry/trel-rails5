class AddListIdsToFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :list_ids, :text
  end
end
