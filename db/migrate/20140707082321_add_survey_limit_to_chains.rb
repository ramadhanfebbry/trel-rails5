class AddSurveyLimitToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :survey_incentive_daily_cap, :integer, :default => 0
    add_column :chains, :survey_incentive_weekly_cap, :integer, :default => 0
    add_column :chains, :survey_incentive_monthly_cap, :integer, :default => 0
  end
end
