class AddColumnLastModificationOnRestaurantImportHistories < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurant_import_histories, :last_modification , :datetime
    
  end
end