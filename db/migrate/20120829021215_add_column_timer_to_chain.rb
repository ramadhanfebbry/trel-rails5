class AddColumnTimerToChain < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :timer, :integer
  end
end
