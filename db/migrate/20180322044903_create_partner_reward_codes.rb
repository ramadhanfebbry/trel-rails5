class CreatePartnerRewardCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_reward_codes do |t|
      t.integer :chain_id
      t.string :code
      t.integer :reward_id
      t.string :user_id
      t.string :champain_id
      t.integer :number_of_valid_days
      t.boolean :used, :default => false

      t.timestamps
    end

    add_index :partner_reward_codes, :chain_id
    add_index :partner_reward_codes, :code
    add_index :partner_reward_codes, :used
    add_index :partner_reward_codes, :user_id
    add_index :partner_reward_codes, :champain_id
  end

end
