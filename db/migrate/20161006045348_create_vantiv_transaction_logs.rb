class CreateVantivTransactionLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :vantiv_transaction_logs do |t|
      t.integer :chain_id
      t.integer :user_id
      t.integer :transaction_type
      t.string :card_number
      t.string :ref_num
      t.string :transaction_id
      t.float :amount
      t.text :relevant_response
      t.text :vantiv_response
      t.text :mp_response
      t.boolean :success, :default => false
      t.timestamps
    end

    add_index :vantiv_transaction_logs, :chain_id
    add_index :vantiv_transaction_logs, :user_id
    add_index :vantiv_transaction_logs, :card_number
    add_index :vantiv_transaction_logs, :ref_num
    add_index :vantiv_transaction_logs, :transaction_id
    add_index :vantiv_transaction_logs, :created_at
  end
end
