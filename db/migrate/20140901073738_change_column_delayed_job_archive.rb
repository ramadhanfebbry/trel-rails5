class ChangeColumnDelayedJobArchive < ActiveRecord::Migration[5.0]
  def up
    remove_column :delayed_job_archives, :prioriy
    add_column :delayed_job_archives, :priority, :integer, :default => 0
  end

  def down
    remove_column :delayed_job_archives, :priority
    add_column :delayed_job_archives, :prioriy, :integer
  end
end
