class AddAttachmentToRevelLogs < ActiveRecord::Migration[5.0]
  def self.up
    add_column :revel_logs, :attachment_file_name, :string
    add_column :revel_logs, :attachment_content_type, :string
    add_column :revel_logs, :attachment_file_size, :integer
    add_column :revel_logs, :attachment_updated_at, :datetime
    remove_column :revel_logs, :file_name
  end

  def self.down
    remove_column :revel_logs, :attachment_file_name
    remove_column :revel_logs, :attachment_content_type
    remove_column :revel_logs, :attachment_file_size
    remove_column :revel_logs, :attachment_updated_at
    add_column :revel_logs, :file_name, :string
  end
end
