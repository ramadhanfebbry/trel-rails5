class AddDateSubmissionToRaffleUserInfor < ActiveRecord::Migration[5.0]
  def change
  	add_column :raffle_user_informations, :date_of_submission, :datetime
  end
end
