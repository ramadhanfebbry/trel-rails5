class AddIndexForEachTables < ActiveRecord::Migration[5.0]
  def up
    add_index :application_keys, :appkey
    add_index :application_keys, :application_id
    add_index :application_keys, [:appkey, :application_id]
    add_index :applications, :key    
    add_index :answers, :value_id
    add_index :chain_locales, :chain_id
    add_index :chain_locales, :locale_id
    add_index :chain_locales, [:locale_id, :chain_id]
    add_index :chain_reward_events, :reward_id
    add_index :chain_reward_events, :chain_id
    add_index :chain_reward_events, [:chain_id, :reward_id]
    add_index :chains_owners, [:chain_id, :owner_id]
    add_index :line_items, :receipt_id
    add_index :locales, :key
    add_index :memberships, [:user_id, :chain_id]
    add_index :milestone_points, :reward_id
    add_index :milestones, :chain_id
    add_index :notification_rewards, :reward_id
    add_index :perk_wallets, [:perk_id, :user_id]
    add_index :perks_restaurants, :perk_id
    add_index :perks_restaurants, :restaurant_id
    add_index :perks_restaurants, [:perk_id, :restaurant_id]
    add_index :promotions, :chain_id
    add_index :receipt_transactions, :receipt_id
    add_index :receipt_transactions, :reject_reason_id
    add_index :receipt_transactions, :admin_id
    add_index :receipt_transactions, :restaurant_id
    add_index :receipt_transactions, :restaurant_offer_id
    add_index :receipts, :chain_id
    add_index :receipts, :user_id
    add_index :receipts, :application_id
    add_index :rewards, :chain_id
    add_index :rewards, :survey_id
    add_index :surveys, :chain_id
    add_index :surveys_users, :receipt_id
    add_index :surveys_users, :reward_id
    add_index :surveys_users, :offer_id
    add_index :user_milestone_points, :user_id
    add_index :user_milestone_points, :milestone_id
  end

  def down
    remove_index :application_keys, :appkey
    remove_index :application_keys, :application_id
    remove_index :application_keys, [:appkey, :application_id]
    remove_index :applications, :key
    remove_index :answer_texts, :answer_id
    remove_index :answers, :value_id
    remove_index :answers, :user_id
    remove_index :answers, :question_id
    remove_index :chain_locales, :chain_id
    remove_index :chain_locales, :locale_id
    remove_index :chain_locales, [:locale_id, :chain_id]
    remove_index :chain_reward_events, :reward_id
    remove_index :chain_reward_events, :chain_id
    remove_index :chain_reward_events, [:chain_id, :reward_id]
    remove_index :chains_owners, [:chain_id, :owner_id]
    remove_index :line_items, :receipt_id
    remove_index :locales, :key
    remove_index :memberships, [:user_id, :chain_id]
    remove_index :milestone_points, :reward_id
    remove_index :milestones, :chain_id
    remove_index :notification_rewards, :reward_id
    remove_index :perk_wallets, [:perk_id, :user_id]
    remove_index :perks_restaurants, :perk_id
    remove_index :perks_restaurants, :restaurant_id
    remove_index :perks_restaurants, [:perk_id, :restaurant_id]
    remove_index :promotions, :chain_id
    remove_index :receipt_transactions, :receipt_id
    remove_index :receipt_transactions, :reject_reason_id
    remove_index :receipt_transactions, :admin_id
    remove_index :receipt_transactions, :restaurant_id
    remove_index :receipt_transactions, :restaurant_offer_id
    remove_index :receipts, :chain_id
    remove_index :receipts, :user_id
    remove_index :receipts, :application_id
    remove_index :rewards, :chain_id
    remove_index :rewards, :survey_id
    remove_index :surveys, :chain_id
    remove_index :surveys_users, :receipt_id
    remove_index :surveys_users, :reward_id
    remove_index :surveys_users, :offer_id
    remove_index :user_milestone_points, :user_id
    remove_index :user_milestone_points, :milestone_id
  end
end
