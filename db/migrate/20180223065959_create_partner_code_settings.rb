class CreatePartnerCodeSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_code_settings do |t|
      t.integer :chain_id
      t.string :barcode_format
      t.string :barcode_format_type
      t.string :pos_digit_barcode
      t.integer :appcode_type
      t.integer :timer, :default => 0
      t.integer :zero_padding, :default => 0
      t.string :prefix
      t.string :postfix

      t.timestamps
    end

    add_index :partner_code_settings, :chain_id
  end
end
