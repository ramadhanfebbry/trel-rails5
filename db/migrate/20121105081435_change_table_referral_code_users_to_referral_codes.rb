class ChangeTableReferralCodeUsersToReferralCodes < ActiveRecord::Migration[5.0]
  def up
    drop_table(:referral_code_users)
    
    create_table :referral_codes do |t|
      t.integer :referral_program_id, :user_id
      t.string :code
      t.timestamps
    end

    add_index :referral_codes, [:referral_program_id, :user_id, :code], :unique => true, :name => :uniq_referal_code
    add_index(:referral_codes, :referral_program_id)
    add_index(:referral_codes, :user_id)
  end

  def down
  end
end
