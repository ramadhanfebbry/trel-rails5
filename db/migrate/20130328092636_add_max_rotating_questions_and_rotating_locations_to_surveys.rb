class AddMaxRotatingQuestionsAndRotatingLocationsToSurveys < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :max_rotating_questions, :integer,  :default => 2
    add_column :surveys, :rotating_locations, :text
  end
end