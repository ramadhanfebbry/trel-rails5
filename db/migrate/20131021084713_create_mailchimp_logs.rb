class CreateMailchimpLogs < ActiveRecord::Migration[5.0]
  def change
    create_table :mailchimp_logs do |t|
      t.integer :delayed_job_id
      t.string :status
      t.text :success_users
      t.text :failed_users
      t.integer :chain_id
      t.string :log_type

      t.timestamps
    end
  end
end