class AddRestaurantIdToReceiptTransaction < ActiveRecord::Migration[5.0]
  def self.up
    add_column :receipt_transactions, :restaurant_id, :integer
  end

  def self.down
    remove_column :receipt_transactions, :restaurant_id
  end
end
