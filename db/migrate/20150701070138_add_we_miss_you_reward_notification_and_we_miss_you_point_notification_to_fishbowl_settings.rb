class AddWeMissYouRewardNotificationAndWeMissYouPointNotificationToFishbowlSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :fishbowl_settings, :we_miss_you_reward_notification, :string
    add_column :fishbowl_settings, :we_miss_you_point_notification, :string
  end
end
