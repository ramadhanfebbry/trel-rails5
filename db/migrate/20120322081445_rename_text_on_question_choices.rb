class RenameTextOnQuestionChoices < ActiveRecord::Migration[5.0]
  def change
    rename_column :question_choices, :text, :label
    add_column :question_choices, :value, :float, :default => 0
  end
end
