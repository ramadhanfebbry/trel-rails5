class AddIndexToPosMenuItems < ActiveRecord::Migration[5.0]
  def change
    add_index :pos_menu_items, :pos_menu_id
    add_index :pos_menu_items, :item_name
    add_index :pos_menu_items, :item_number
    add_index :pos_menu_items, :general_menu_item_id
    add_index :pos_menu_items, :chain_id
    add_index :pos_menu_items, :pos_location_id
    add_index :pos_menu_items, [:item_number, :chain_id, :pos_location_id], :name =>  :item_number_chain_and_pos_location
  end
end
