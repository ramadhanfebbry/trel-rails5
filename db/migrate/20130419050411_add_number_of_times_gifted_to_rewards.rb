class AddNumberOfTimesGiftedToRewards < ActiveRecord::Migration[5.0]
  def change
    add_column :rewards, :number_of_times_gifted, :integer, :default => 0
  end
end