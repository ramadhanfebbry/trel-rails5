class AddAdminPushPointNotifToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :admin_push_point_notif_to_email, :boolean, :default => false
    add_column :chains, :admin_push_point_notif_to_device, :boolean, :default => false
  end
end
