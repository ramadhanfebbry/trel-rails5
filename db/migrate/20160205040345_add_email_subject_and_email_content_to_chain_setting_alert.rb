class AddEmailSubjectAndEmailContentToChainSettingAlert < ActiveRecord::Migration[5.0]
  def change
    add_column :chain_setting_alerts, :email_subject, :string

    add_column :chain_setting_alerts, :email_content, :text

  end
end
