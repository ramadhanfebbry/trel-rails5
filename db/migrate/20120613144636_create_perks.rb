class CreatePerks < ActiveRecord::Migration[5.0]
  def change
    create_table :perks do |t|
      t.decimal :multiplier, :precision => 8, :scale => 2
      t.integer :bonus_points
      t.integer :type

      t.timestamps
    end
  end
end
