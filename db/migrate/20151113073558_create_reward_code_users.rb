class CreateRewardCodeUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_code_users do |t|
      t.string :staffcode
      t.integer :user_id
      t.integer :chain_id
      t.integer :restaurant_id

      t.timestamps
    end

    add_index :reward_code_users, :user_id
    add_index :reward_code_users, :chain_id
    add_index :reward_code_users, :staffcode
    add_index :reward_code_users, :restaurant_id
    add_index :reward_code_users, [:user_id, :staffcode ]
    add_index :reward_code_users, [:chain_id, :staffcode ], :unique => true
  end
end
