class CreateRewardTransactionDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :reward_transaction_details do |t|
      t.integer :reward_transaction_id
      t.integer :reward_information_id
      t.string :title
      t.text :description
      t.text :app_text
      t.text :answer
      t.timestamps
    end

    add_index :reward_transaction_details, :reward_transaction_id
    add_index :reward_transaction_details, [:reward_transaction_id, :reward_information_id] , :name => "by_transaction"
  end
end
