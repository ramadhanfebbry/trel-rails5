class AddSettingForRewardExpirationNotificationToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :ren_number_of_days, :integer , :default => 1
    add_column :chains, :ren_email_notification, :boolean, :default => true
    add_column :chains, :ren_device_notification, :boolean, :default => true
  end
end