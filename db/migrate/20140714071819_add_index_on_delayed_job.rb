class AddIndexOnDelayedJob < ActiveRecord::Migration[5.0]
  def up
    add_index :delayed_jobs ,[:delayable_id, :delayable_type]
  end

  def down
    remove_index :delayed_jobs ,[:delayable_id, :delayable_type]
  end
end
