class AddBarcodeAndCheckIdToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :barcode, :string
    add_column :pos_check_uploads, :check_id, :string
    add_column :pos_check_uploads, :status, :integer
  end
end