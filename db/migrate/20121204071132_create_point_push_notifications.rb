class CreatePointPushNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :point_push_notifications do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.text :email_content
      t.string :email_subject
      t.string :notification

      t.timestamps
    end
  end
end
