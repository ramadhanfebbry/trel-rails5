class AddProviderToOloConnectSettings < ActiveRecord::Migration[5.0]
  def change
    add_column :olo_connect_settings, :provider, :string
  end
end
