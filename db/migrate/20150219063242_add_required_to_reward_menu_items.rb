class AddRequiredToRewardMenuItems < ActiveRecord::Migration[5.0]
  def change
    add_column :reward_menu_items, :required, :boolean, :default => true
  end
end