class CreateUserGoalCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :user_goal_categories do |t|
      t.integer :user_id
      t.integer :category_id
      t.integer :chain_id

      t.timestamps
    end
  end
end
