class AddNewTextSettingToReferralNotifications < ActiveRecord::Migration[5.0]
  def change
    add_column :referral_notifications, :facebook_text, :text
    add_column :referral_notifications, :twitter_text, :text
    add_column :referral_notifications, :other_media_text, :text
  end
end
