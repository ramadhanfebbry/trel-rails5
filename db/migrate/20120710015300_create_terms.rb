class CreateTerms < ActiveRecord::Migration[5.0]
  def change
    create_table :terms do |t|
      t.text :content
      t.references :locale

      t.timestamps
    end
  end
end
