class AddColumnOnHistoryNotifications < ActiveRecord::Migration[5.0]
  def up
    add_column :history_notifications, :status, :integer
  end

  def down
    remove_column :history_notifications, :status, :integer
  end
end
