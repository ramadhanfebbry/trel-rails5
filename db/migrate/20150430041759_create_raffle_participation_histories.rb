class CreateRaffleParticipationHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :raffle_participation_histories do |t|
      t.integer :raffle_id
      t.integer :user_id
      t.integer :raffle_step_id
      t.datetime :date_of_submission
      t.string :user_email
      t.string :information_submited
      t.timestamps
    end
  end
end
