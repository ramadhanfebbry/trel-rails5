class CreateReceiptTransactionDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :receipt_transaction_details do |t|
      t.integer :receipt_transaction_id
      t.text :clientfeedback1
      t.text :clientfeedback2

      t.timestamps
    end
  end
end
