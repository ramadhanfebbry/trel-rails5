class AddColumnOnUserTotalMilestone < ActiveRecord::Migration[5.0]
  def up
    add_column :user_total_receipts , :last_receipt_id , :integer
  end

  def down
    remove_column :user_total_receipts , :last_receipt_id
  end


end
