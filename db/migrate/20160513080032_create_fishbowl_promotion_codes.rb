class CreateFishbowlPromotionCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :fishbowl_promotion_codes do |t|
      t.integer :chain_id
      t.integer :reward_id
      t.string :code
      t.integer :user_id
      t.datetime :used_at
      t.integer :restaurant_id

      t.timestamps
    end

    add_index :fishbowl_promotion_codes, :chain_id
    add_index :fishbowl_promotion_codes, :reward_id
    add_index :fishbowl_promotion_codes, :code
    add_index :fishbowl_promotion_codes, :user_id
    add_index :fishbowl_promotion_codes, :restaurant_id
  end
end
