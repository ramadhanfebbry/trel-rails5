class CreateNoScansTables < ActiveRecord::Migration[5.0]
  def up

    if table_exists?(:no_scan_scheduler)
      drop_table :no_scan_schedulers
    end

    
    execute "CREATE TABLE no_scan_schedulers
    (
      id serial NOT NULL,
      job_id integer,
      run_type character varying(255),
      start_at timestamp without time zone,
      executed_at timestamp without time zone,
      last_executed timestamp without time zone,
      next_run timestamp without time zone,
      end_at timestamp without time zone,
      created_at timestamp without time zone NOT NULL,
      updated_at timestamp without time zone NOT NULL,
      CONSTRAINT no_scan_schedulers_pkey PRIMARY KEY (id)
    )
    WITH (
      OIDS=FALSE
    );"

    # execute "CREATE INDEX index_no_scan_schedulers_on_job_id
    #   ON no_scan_schedulers
    #   USING btree
    #   (job_id);"

    if table_exists?(:no_scan_jobs)
      drop_table :no_scan_jobs
    end
    
        execute "CREATE TABLE no_scan_jobs
    (
      id serial NOT NULL,
      description text,
      job_type integer,
      reward_type integer,
      points integer,
      owner_id integer,
      status integer,
      amount_min integer,
      amount_max integer,
      total_users integer,
      chain_id integer,
      reward_id integer,
      activity_days integer,
      percentage integer,
      created_at timestamp without time zone NOT NULL,
      updated_at timestamp without time zone NOT NULL,
      executed_at timestamp without time zone,
      subject_email text,
      content_email text,
      push_email boolean DEFAULT true,
      push_phone boolean DEFAULT true,
      subject_phone character varying(255),
      content_phone text,
      schedule_type integer,
      notification boolean DEFAULT false,
      miss_you boolean DEFAULT false,
      individual boolean DEFAULT false,
      individual_user_id integer,
      individual_email character varying(255),
      task_definition_id integer,
      miss_you_reward_fishbowl_mailing_id character varying(255),
      miss_you_point_fishbowl_mailing_id character varying(255),
      is_fishbowl boolean DEFAULT false,
      is_standard_email boolean DEFAULT false,
      zone character varying(255),
      kind integer DEFAULT 0,
      no_scan_id integer,
      no_scan_definition_id integer,
      CONSTRAINT no_scan_jobs_pkey PRIMARY KEY (id)
    )
    WITH (
      OIDS=FALSE
    );"
  end

  def down
  end
end
