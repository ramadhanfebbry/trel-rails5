class AddStepableToSteps < ActiveRecord::Migration[5.0]
  def change
    add_column :steppingstone_steps, :stepable_id, :integer
    add_column :steppingstone_steps, :stepable_type, :string
  end
end