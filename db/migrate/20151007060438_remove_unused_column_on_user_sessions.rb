class RemoveUnusedColumnOnUserSessions < ActiveRecord::Migration[5.0]
  def up
    remove_column :user_sessions, :is_static
    remove_index :user_sessions, [:chain_id, :code]
    add_index :user_sessions, [:chain_id, :code]
  end

  def down
    add_column :user_sessions, :is_static, :boolean, :default => false
    remove_index :user_sessions, [:chain_id, :code]
    add_index :user_sessions, [:chain_id, :code], :unique => true
  end
end
