class AddClientSetStatusValueAndClientSetStatusLabelToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :client_set_status_label, :string

    add_column :users, :client_set_status_value, :string

  end
end
