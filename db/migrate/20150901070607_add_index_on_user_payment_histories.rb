class AddIndexOnUserPaymentHistories < ActiveRecord::Migration[5.0]
  def up
    execute "create index idx_pcu_id_uph on user_payment_histories (pos_check_upload_id);"
    execute "create index idx_user_id_uph on user_payment_histories (user_id);"
    execute "create index idx_success_uph on user_payment_histories (success);"
    execute "create index idx_user_id_pcu_id_uph on user_payment_histories (user_id, pos_check_upload_id);"
    execute "create index idx_chain_id_user_id_pay_code_status_action_ph on payment_histories (chain_id, user_id, pay_code, status, action);"
  end

  def down
    execute "DROP INDEX idx_pcu_id_uph;"
    execute "DROP INDEX idx_user_id_uph;"
    execute "DROP INDEX idx_success_uph;"
    execute "DROP INDEX idx_pcu_id_uph;"
    execute "DROP INDEX idx_user_id_pcu_id_uph;"
    execute "DROP INDEX idx_chain_id_user_id_pay_code_status_action_ph;"
  end
end
