class AddShowNewQuestionTypesToApplicationKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :application_keys, :show_new_question_types, :boolean, :default => false
  end
end
