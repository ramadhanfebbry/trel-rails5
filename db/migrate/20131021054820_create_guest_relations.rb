class CreateGuestRelations < ActiveRecord::Migration[5.0]
  def change
    create_table :guest_relations do |t|
      t.integer :surveys_user_id
      t.integer :owner_id
      t.integer :chain_id
      t.integer :job_type
      t.integer :points
      t.integer :reward_id
      t.integer :status
      t.integer :approved_owner_id
      t.timestamps
    end

    add_index :guest_relations,:surveys_user_id
    add_index :guest_relations,:owner_id
    add_index :guest_relations,:chain_id
    add_index :guest_relations,[:surveys_user_id,:owner_id]
  end
end
