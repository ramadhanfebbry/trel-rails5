class AddStatusToPosReceipts < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_receipts, :status, :integer, :default => 1
  end
end