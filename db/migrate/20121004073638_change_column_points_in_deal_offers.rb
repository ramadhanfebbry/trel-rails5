class ChangeColumnPointsInDealOffers < ActiveRecord::Migration[5.0]
  def up
    change_column :deal_offers, :points, :float
  end

  def down
    change_column :deal_offers, :points, :integer
  end
end
