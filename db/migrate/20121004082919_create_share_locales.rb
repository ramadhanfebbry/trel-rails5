class CreateShareLocales < ActiveRecord::Migration[5.0]
  def change
    create_table :chain_image_share_locales do |t|
      t.integer :chain_image_share_id, :locale_id
      t.string :title
      t.text :description
      t.timestamps
    end
  end
end
