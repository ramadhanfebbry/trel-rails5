class AddColumnRestaurantIdOnSurveysUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :surveys_users, :restaurant_id, :integer
    add_index :surveys_users, :restaurant_id
  end

  def down
    remove_column :surveys_users, :restaurant_id
  end
end
