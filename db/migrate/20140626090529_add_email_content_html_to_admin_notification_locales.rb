class AddEmailContentHtmlToAdminNotificationLocales < ActiveRecord::Migration[5.0]
  def change
    add_column :admin_notification_locales, :email_content_html, :text
    AdminNotificationLocale.all.each do |anl|
      anl.update_column(:email_content_html, anl.email_content)
    end
  end
end