class ChangeColumnOnAvgTable < ActiveRecord::Migration[5.0]
  def up
    change_column :average_chart_tables,:average , :float
  end

  def down
    change_column :average_chart_tables,:average , :integer
  end
end
