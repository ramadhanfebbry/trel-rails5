class CreateLaunchTexts < ActiveRecord::Migration[5.0]
  def change
    create_table :launch_texts do |t|
      t.integer :chain_id
      t.integer :locale_id
      t.string :title
      t.text :subtitle

      t.timestamps
    end
  end
end
