class AddRestauranAndEmailToOloWebhook < ActiveRecord::Migration[5.0]
  def change
    add_column :olo_webhooks, :restaurant, :string

    add_column :olo_webhooks, :email, :string

  end
end
