class CreateOpenCloseRestaurantDays < ActiveRecord::Migration[5.0]
  def change
    create_table :open_close_restaurant_days do |t|
      t.integer :restaurant_id
      t.date :day_time
      t.boolean :open, :default => true

      t.timestamps
    end
    add_index :open_close_restaurant_days, :restaurant_id
    add_index :open_close_restaurant_days, :day_time
    add_index :open_close_restaurant_days, :open
    add_index :open_close_restaurant_days, [:restaurant_id, :day_time]
  end
end