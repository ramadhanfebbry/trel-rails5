class AddRequiredUniqueDeviceInfoToChains < ActiveRecord::Migration[5.0]
  def change
    add_column :chains, :required_unique_device_info, :boolean, :default => false
  end
end
