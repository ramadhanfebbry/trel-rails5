class AddColumnLastRunOnOwner < ActiveRecord::Migration[5.0]
  def up
    add_column :owners, :last_report_run , :date
  end

  def down
    remove_column :owners, :last_report_run
  end
end
