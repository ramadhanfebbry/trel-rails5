class CreateSubCategoryMenuItems < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_sub_category_menu_items do |t|
      t.integer :partner_sub_category_id
      t.integer :partner_menu_item_id

      t.timestamps
    end
  end
end
