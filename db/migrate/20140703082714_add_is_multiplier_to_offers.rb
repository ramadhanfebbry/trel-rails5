class AddIsMultiplierToOffers < ActiveRecord::Migration[5.0]
  def change
    add_column :offers, :is_multiplier, :boolean, :default => true
  end
end
