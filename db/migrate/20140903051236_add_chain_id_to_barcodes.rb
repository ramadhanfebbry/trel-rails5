class AddChainIdToBarcodes < ActiveRecord::Migration[5.0]
  def change
    add_column :barcodes, :chain_id, :integer
    add_index :barcodes, :chain_id
    add_index :barcodes, [:code, :chain_id]
  end
end
