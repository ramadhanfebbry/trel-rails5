class ChangeTableAdminReportDefinitions < ActiveRecord::Migration[5.0]
  def up
    drop_table :admin_report_definitions

    create_table :report_definitions do |t|
      t.string :name
      t.text :jasper_path
      t.integer :report_kind
      t.text :query
      t.date :start_at
      t.date :end_at

      t.timestamps
    end
  end

  def down
  end
end
