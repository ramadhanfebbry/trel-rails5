class CreateOloConnectSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :olo_connect_settings do |t|
      t.string :client_id
      t.string :client_secret
      t.string :api_root
      t.string :apikey
      t.integer :chain_id
      t.timestamps
    end
  end
end
