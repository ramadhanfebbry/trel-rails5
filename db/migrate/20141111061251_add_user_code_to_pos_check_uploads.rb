class AddUserCodeToPosCheckUploads < ActiveRecord::Migration[5.0]
  def change
    add_column :pos_check_uploads, :user_code, :string
    add_index :pos_check_uploads, :user_code
  end
end
