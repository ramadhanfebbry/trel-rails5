class CreateFocusRewardChecks < ActiveRecord::Migration[5.0]
  def change
    create_table :focus_reward_checks do |t|
      t.integer :pos_location_id
      t.integer :chain_id
      t.integer :user_id
      t.string :reward_code
      t.integer :status
      t.integer :error_description
      t.timestamps
    end
  end
end
