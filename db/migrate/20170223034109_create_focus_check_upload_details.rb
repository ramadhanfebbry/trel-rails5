class CreateFocusCheckUploadDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :focus_check_upload_details do |t|
      t.integer :focus_check_upload_id
      t.string :ticket_id
      t.float :total
      t.string :check_date
      t.integer :employee_num
      t.timestamps
    end
    add_index :focus_check_upload_details, :focus_check_upload_id
  end
end
