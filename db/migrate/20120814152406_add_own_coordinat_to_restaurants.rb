class AddOwnCoordinatToRestaurants < ActiveRecord::Migration[5.0]
  def change
    add_column :restaurants, :own_coordinates, :boolean, :default => false
  end
end
