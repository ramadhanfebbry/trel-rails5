# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_05_04_064749) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_trgm"
  enable_extension "plpgsql"

  create_table "access_grants", id: :serial, force: :cascade do |t|
    t.string "code"
    t.string "access_token"
    t.string "refresh_token"
    t.datetime "access_token_expires_at"
    t.integer "user_id"
    t.string "client_id"
    t.string "state"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["access_token", "access_token_expires_at"], name: "index_access_grants_on_access_token_and_access_token_expires_at"
    t.index ["user_id"], name: "index_access_grants_on_user_id"
  end

  create_table "account_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.text "deactivation_confirmation_text"
    t.text "reactivation_confirmation_text"
    t.string "confirm_button_text"
    t.string "cancel_button_text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "active_reward_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.text "notification"
    t.text "cancel_button_text"
    t.text "continue_button_text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "activity_summaries", id: :serial, force: :cascade do |t|
    t.date "transaction_date"
    t.integer "total"
    t.integer "restaurant_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id", "restaurant_id"], name: "index_activity_summaries_on_chain_id_and_restaurant_id"
    t.index ["chain_id"], name: "index_activity_summaries_on_chain_id"
    t.index ["restaurant_id"], name: "index_activity_summaries_on_restaurant_id"
  end

  create_table "admin_notification_locales", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.string "notification"
    t.string "email_subject"
    t.text "email_content"
    t.integer "send_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "email_content_html"
  end

  create_table "admins", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "failed_attempts", default: 0
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role", default: "admin"
    t.index ["email"], name: "index_admins_on_email", unique: true
  end

  create_table "analytics_geodata", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "keychain"
    t.string "android_id"
    t.float "lat"
    t.float "long"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "device_time"
  end

  create_table "answer_texts", id: :serial, force: :cascade do |t|
    t.string "text"
    t.integer "answer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["answer_id"], name: "index_answer_texts_on_answer_id"
  end

  create_table "answers", id: :serial, force: :cascade do |t|
    t.integer "value_id"
    t.integer "answer_type"
    t.integer "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "surveys_user_id"
    t.datetime "deleted_at"
    t.index ["question_id"], name: "index_answers_on_question_id"
    t.index ["surveys_user_id"], name: "index_answers_on_surveys_user_id"
  end

  create_table "api_response_texts", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.text "message"
    t.integer "message_for"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "appcode_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "barcode_format"
    t.string "barcode_format_type"
    t.string "pos_digit_barcode"
    t.integer "appcode_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "code_type"
    t.integer "timer", default: 0
    t.integer "zero_padding", default: 0
    t.string "prefix"
    t.string "postfix"
    t.integer "user_metadata", default: 1
    t.string "default_code_number"
    t.boolean "default_code", default: false
    t.index ["chain_id"], name: "index_appcode_settings_on_chain_id"
  end

  create_table "application_key_details", id: :serial, force: :cascade do |t|
    t.integer "application_key_id"
    t.boolean "is_required", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "application_keys", id: :serial, force: :cascade do |t|
    t.string "appkey"
    t.integer "application_id"
    t.string "version"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status", default: "active"
    t.boolean "is_newest", default: false
    t.datetime "deleted_at"
    t.boolean "ocr_send_message", default: false
    t.boolean "show_new_question_types", default: false
    t.boolean "show_gifter", default: true
    t.boolean "show_new_question_slider", default: false
    t.boolean "show_expired_reward", default: true
    t.boolean "show_expired_reward_activity_listing", default: false
    t.boolean "show_new_question_multiple_dropdown", default: false
  end

  create_table "applications", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "device_type"
    t.datetime "deleted_at"
    t.string "key"
    t.string "status", default: "active"
    t.datetime "last_sent_notice"
    t.index ["chain_id"], name: "index_applications_on_chain_id"
    t.index ["device_type"], name: "index_applications_on_device_type"
  end

  create_table "archived_xml_receipts", id: :serial, force: :cascade do |t|
    t.integer "receipt_id"
    t.text "xml_data_receipt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "authentications", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "provider"
    t.string "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "average_chart_tables", id: :serial, force: :cascade do |t|
    t.date "receipt_date"
    t.float "average"
    t.integer "chain_id"
    t.integer "restaurant_id"
    t.integer "receipt_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id", "restaurant_id"], name: "index_average_chart_tables_on_chain_id_and_restaurant_id"
    t.index ["chain_id"], name: "index_average_chart_tables_on_chain_id"
    t.index ["restaurant_id"], name: "index_average_chart_tables_on_restaurant_id"
  end

  create_table "avg_visits", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "restaurant_id"
    t.float "average"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["restaurant_id"], name: "index_avg_visits_on_restaurant_id"
    t.index ["user_id"], name: "index_avg_visits_on_user_id"
  end

  create_table "barcode_batches", id: :serial, force: :cascade do |t|
    t.integer "pos_location_id"
    t.string "barcode"
    t.integer "size"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "barcodes", id: :serial, force: :cascade do |t|
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "barcode_batch_id"
    t.integer "chain_id"
    t.index ["code", "chain_id"], name: "index_barcodes_on_code_and_chain_id"
    t.index ["code"], name: "index_barcodes_on_code"
  end

  create_table "birthday_reward_notifications", id: :serial, force: :cascade do |t|
    t.integer "notifiable_id"
    t.string "notifiable_type"
    t.string "notification"
    t.string "email_subject"
    t.string "email_content"
    t.text "email_content_html"
    t.integer "locale_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["locale_id"], name: "index_birthday_reward_notifications_on_locale_id"
    t.index ["notifiable_id", "notifiable_type"], name: "birthday_reward_index"
  end

  create_table "bridg_webhooks", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.text "check"
    t.integer "reward_id"
    t.integer "restaurant_id"
    t.string "reward_code"
    t.string "discount"
    t.string "user_id"
    t.string "campaign_id"
    t.boolean "status"
    t.string "status_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "btree_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "public_key"
    t.string "private_key"
    t.string "environment"
    t.string "merchant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bucket_paths", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "name"
    t.string "define_as"
    t.boolean "active"
    t.boolean "resettable"
    t.float "percentage_of_user"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "tag_id"
    t.index ["chain_id"], name: "index_bucket_paths_on_chain_id"
  end

  create_table "chain_image_share_locales", id: :serial, force: :cascade do |t|
    t.integer "chain_image_share_id"
    t.integer "locale_id"
    t.string "title"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "chain_image_shares", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "title"
    t.date "start_date"
    t.date "expiry_date"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.integer "image_file_size"
    t.string "image_content_type"
    t.datetime "image_updated_at"
    t.text "fb_description"
    t.text "twitter_description"
    t.string "fb_title"
    t.string "twitter_title"
    t.index ["chain_id"], name: "index_chain_image_shares_on_chain_id"
  end

  create_table "chain_locales", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "chain_reward_events", id: :serial, force: :cascade do |t|
    t.integer "event"
    t.integer "reward_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["chain_id"], name: "index_chain_reward_events_on_chain_id"
    t.index ["reward_id"], name: "index_chain_reward_events_on_reward_id"
  end

  create_table "chain_setting_alerts", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "email"
    t.boolean "is_active", default: false
    t.string "send_time"
    t.string "send_zone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email_subject"
    t.text "email_content"
    t.index ["chain_id"], name: "index_chain_setting_alerts_on_chain_id"
  end

  create_table "chain_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.boolean "hybrid_milestone_regular_rewards_enabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "olo_restaurant_hour_sync", default: false
    t.boolean "domain_email_validation", default: true
    t.boolean "user_email_confirmation", default: false
    t.integer "external_partner"
    t.index ["chain_id"], name: "index_chain_settings_on_chain_id"
  end

  create_table "chain_urls", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "name"
    t.string "url"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_chain_urls_on_chain_id"
    t.index ["slug"], name: "index_chain_urls_on_slug"
  end

  create_table "chains", id: :serial, force: :cascade do |t|
    t.string "name"
    t.float "points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "status", default: "active"
    t.string "email"
    t.integer "timer"
    t.float "user_points", default: 1.0
    t.float "hours_delay_reward", default: 0.0
    t.boolean "is_push_to_phone", default: false
    t.boolean "is_push_to_email", default: false
    t.integer "report_period"
    t.date "report_start_date"
    t.date "report_end_date"
    t.boolean "is_send_push_point_notif_to_email", default: false
    t.boolean "is_send_push_point_notif_to_phone", default: false
    t.integer "user_limit_receipt_approved_per_day", default: 0
    t.string "survey_email"
    t.integer "php_chain_id"
    t.boolean "check_php_migration"
    t.float "max_subtotal", default: 0.0
    t.boolean "activate_milestone_receipt", default: true
    t.boolean "activate_stepping_stone", default: false
    t.boolean "use_generated_code", default: true
    t.datetime "last_set_tag_at"
    t.boolean "game_point_notif_send_to_device", default: false
    t.boolean "game_point_notif_send_to_email", default: false
    t.boolean "admin_push_reward_notif_to_email", default: false
    t.boolean "admin_push_reward_notif_to_device", default: false
    t.boolean "admin_push_point_notif_to_email", default: false
    t.boolean "admin_push_point_notif_to_device", default: false
    t.boolean "phone_number_is_required", default: false
    t.boolean "birthday_required", default: false
    t.boolean "check_age_required", default: false
    t.integer "check_age_value"
    t.boolean "mailchimp_sync", default: false
    t.string "mailchimp_api_key"
    t.string "mailchimp_list_id"
    t.integer "mailchimp_log_id"
    t.string "point_rounding_preference", default: "ROUND"
    t.boolean "required_unique_device_info", default: false
    t.boolean "require_point_signup_incentive", default: false
    t.integer "point_signup_incentive"
    t.integer "reward_timer", default: 24
    t.boolean "fb_signup_incentive", default: false
    t.integer "fb_signup_incentive_point"
    t.integer "survey_incentive_daily_cap", default: 0
    t.integer "survey_incentive_weekly_cap", default: 0
    t.integer "survey_incentive_monthly_cap", default: 0
    t.integer "pos_digit_barcode", default: 9
    t.integer "barcode_format", default: 1
    t.integer "barcode_format_type", default: 13
    t.integer "ren_number_of_days", default: 1
    t.boolean "ren_email_notification", default: true
    t.boolean "ren_device_notification", default: true
    t.string "email_parse_url"
    t.string "rewards_image_file_name"
    t.integer "rewards_image_file_size"
    t.string "rewards_image_content_type"
    t.datetime "rewards_image_updated_at"
    t.string "email_url"
    t.integer "user_reward_redeemption_flow", default: 1
    t.integer "pos_used_type", default: 1
    t.integer "pos_receipt_processing_delay", default: 0
    t.boolean "mailchimp_ignore_marketing_optin", default: false
    t.integer "max_number_of_locations", default: 10
    t.integer "no_of_days_micros_table_receipt_generated", default: 0
    t.float "maximum_offer_points_earned_per_day", default: 0.0
    t.integer "pos_discount_setting", default: 1
    t.string "ftp_url"
    t.integer "ftp_port"
    t.string "ftp_username"
    t.string "ftp_password"
    t.boolean "emma_ignore_marketing_optin", default: false
    t.decimal "code_scan_range", default: "0.5"
    t.string "olo_url"
    t.string "olo_signature"
    t.integer "netpos_batch_check_request", default: 60
    t.boolean "auto_loyalty", default: false
    t.bigint "user_code_count", default: 0
    t.bigint "gift_code_count", default: 0
    t.boolean "birthday_reward_push_to_phone", default: false
    t.boolean "birthday_reward_push_to_email", default: false
    t.integer "reward_code_generated_type", default: 1
    t.string "revel_to_relevant_ftp_url"
    t.integer "revel_to_relevant_ftp_port"
    t.string "revel_to_relevant_ftp_username"
    t.string "revel_to_relevant_ftp_password"
    t.string "relevant_to_revel_ftp_url"
    t.integer "relevant_to_revel_ftp_port"
    t.string "relevant_to_revel_ftp_username"
    t.string "relevant_to_revel_ftp_password"
    t.string "revel_to_relevant_ftp_root_dir"
    t.string "relevant_to_revel_ftp_root_dir"
    t.index ["name"], name: "index_chains_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["pos_receipt_processing_delay"], name: "index_chains_on_pos_receipt_processing_delay"
    t.index ["pos_used_type"], name: "index_chains_on_pos_used_type"
  end

  create_table "chains_owners", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["chain_id"], name: "index_chains_owners_on_chain_id"
    t.index ["owner_id"], name: "index_chains_owners_on_owner_id"
  end

  create_table "checkins", id: :serial, force: :cascade do |t|
    t.float "minimum_distance"
    t.text "fine_print"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "image_file_name"
    t.integer "image_file_size"
    t.string "image_content_type"
    t.datetime "image_updated_at"
  end

  create_table "cities", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "region_id"
    t.boolean "active", default: true
    t.datetime "deleted_at"
    t.index ["name"], name: "index_cities_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["region_id"], name: "index_cities_on_region_id"
  end

  create_table "ckeditor_assets", id: :serial, force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.integer "assetable_id"
    t.string "assetable_type", limit: 30
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
    t.index ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"
  end

  create_table "countries", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "abbreviation"
    t.string "ISO"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.integer "regions_count", default: 0
    t.datetime "deleted_at"
    t.index ["abbreviation"], name: "index_countries_abbreviation_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["name"], name: "index_countries_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
  end

  create_table "daily_users", id: :serial, force: :cascade do |t|
    t.integer "restaurant_id"
    t.integer "total"
    t.date "active_date"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["restaurant_id", "active_date"], name: "index_daily_users_on_restaurant_id_and_active_date"
    t.index ["restaurant_id", "chain_id"], name: "index_daily_users_on_restaurant_id_and_chain_id"
    t.index ["restaurant_id"], name: "index_daily_users_on_restaurant_id"
  end

  create_table "dashboard_choices", id: :serial, force: :cascade do |t|
    t.integer "dashboard_question_id"
    t.float "value"
    t.index ["dashboard_question_id"], name: "index_dashboard_choices_on_dashboard_question_id"
  end

  create_table "dashboard_questions", id: :serial, force: :cascade do |t|
    t.string "text"
    t.integer "order_number"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_dashboard_questions_on_chain_id"
  end

  create_table "day_visits", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "chain_id"
    t.integer "restaurant_id"
    t.date "visit_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id", "user_id", "restaurant_id"], name: "index_day_visits_on_chain_id_and_user_id_and_restaurant_id"
    t.index ["restaurant_id", "user_id", "visit_at"], name: "index_day_visits_on_restaurant_id_and_user_id_and_visit_at", unique: true
    t.index ["restaurant_id"], name: "index_day_visits_on_restaurant_id"
    t.index ["user_id"], name: "index_day_visits_on_user_id"
  end

  create_table "db_summaries", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "restaurant_id"
    t.integer "total_member"
    t.integer "member_this_month"
    t.integer "member_last_month"
    t.float "avg_general"
    t.float "avg_visit_month"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_db_summaries_on_chain_id"
    t.index ["restaurant_id", "chain_id"], name: "index_db_summaries_on_restaurant_id_and_chain_id"
    t.index ["restaurant_id"], name: "index_db_summaries_on_restaurant_id"
  end

  create_table "deal_assets", id: :serial, force: :cascade do |t|
    t.string "image_file_name"
    t.integer "image_file_size"
    t.string "image_content_type"
    t.datetime "image_updated_at"
    t.integer "activity_id"
    t.string "activity_type"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deal_offers", id: :serial, force: :cascade do |t|
    t.string "title"
    t.integer "kind"
    t.float "points"
    t.integer "multiplier"
    t.text "fine_print"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "deal_restaurants", id: :serial, force: :cascade do |t|
    t.integer "deal_id"
    t.integer "restaurant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "deals", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "title"
    t.integer "deal_type"
    t.integer "max_winner"
    t.date "start_date"
    t.date "end_date"
    t.integer "limit_user_per_day"
    t.integer "max_user"
    t.string "activity_type"
    t.integer "activity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "incentive_type"
    t.integer "incentive_id"
    t.datetime "deleted_at"
    t.index ["activity_type", "activity_id"], name: "index_deals_on_activity_type_and_activity_id"
    t.index ["chain_id"], name: "index_deals_on_chain_id"
  end

  create_table "delayed_job_archives", id: :serial, force: :cascade do |t|
    t.integer "delayed_job_id"
    t.integer "attempts"
    t.text "handler"
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "chain_id"
    t.integer "delayable_id"
    t.string "delayable_type"
    t.string "queue"
    t.string "locked_by"
    t.integer "priority", default: 0
  end

  create_table "delayed_jobs", id: :serial, force: :cascade do |t|
    t.integer "priority", default: 0
    t.integer "attempts", default: 0
    t.text "handler"
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "chain_id"
    t.string "delayable_type"
    t.integer "delayable_id"
    t.string "queue"
    t.index ["delayable_id", "delayable_type"], name: "index_delayed_jobs_on_delayable_id_and_delayable_type"
    t.index ["delayable_type", "delayable_id"], name: "index_delayed_jobs_on_delayable_type_and_delayable_id"
    t.index ["failed_at"], name: "index_delayed_jobs_on_failed_at"
    t.index ["locked_by"], name: "index_delayed_jobs_on_locked_by"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "email_marketings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "user_id"
    t.string "email"
    t.integer "restaurant_id"
    t.datetime "signup_date"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "leid"
    t.index ["chain_id"], name: "index_email_marketings_on_chain_id"
    t.index ["email", "chain_id"], name: "index_email_marketings_on_email_and_chain_id"
    t.index ["restaurant_id"], name: "index_email_marketings_on_restaurant_id"
    t.index ["status", "chain_id"], name: "index_email_marketings_on_status_and_chain_id"
    t.index ["user_id"], name: "index_email_marketings_on_user_id"
  end

  create_table "email_templates", id: :serial, force: :cascade do |t|
    t.string "template_name"
    t.integer "chain_id"
    t.string "mail_subject"
    t.text "mail_content_html"
    t.text "mail_content_text"
    t.integer "locale_id"
    t.string "send_as"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id", "locale_id", "template_name"], name: "ch_loc_name"
  end

  create_table "emma_logs", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "delayed_job_id"
    t.string "status"
    t.text "success_users"
    t.text "failed_users"
    t.string "log_type"
    t.datetime "initiate_at"
    t.datetime "completed_at"
    t.text "error_report"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_emma_logs_on_chain_id"
    t.index ["delayed_job_id"], name: "index_emma_logs_on_delayed_job_id"
  end

  create_table "emma_marketings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "user_id"
    t.string "email"
    t.integer "restaurant_id"
    t.integer "signup_id"
    t.string "member_identifier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.datetime "signup_date"
    t.index ["chain_id", "user_id"], name: "index_emma_marketings_on_chain_id_and_user_id"
    t.index ["chain_id"], name: "index_emma_marketings_on_chain_id"
    t.index ["restaurant_id"], name: "index_emma_marketings_on_restaurant_id"
    t.index ["user_id"], name: "index_emma_marketings_on_user_id"
  end

  create_table "emma_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "public_api_key"
    t.string "private_api_key"
    t.string "account_id"
    t.string "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sync_now_status"
    t.integer "emma_log_id"
    t.index ["chain_id"], name: "index_emma_settings_on_chain_id"
    t.index ["emma_log_id"], name: "index_emma_settings_on_emma_log_id"
  end

  create_table "external_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "basic_auth"
    t.string "basic_pwd"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "facebook_infos", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "user_id"
    t.string "fb_id"
    t.string "fb_name"
    t.string "fb_fname"
    t.string "fb_lname"
    t.string "fb_link"
    t.string "fb_gender"
    t.string "fb_locale"
    t.string "fb_age_range"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_facebook_infos_on_chain_id"
    t.index ["fb_id"], name: "index_facebook_infos_on_fb_id"
    t.index ["user_id"], name: "index_facebook_infos_on_user_id"
  end

  create_table "faqimages", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.integer "image_file_size"
    t.string "image_content_type"
    t.datetime "image_updated_at"
  end

  create_table "faqs", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.text "html_content"
    t.text "css_content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
    t.string "url"
    t.text "generated_html_content"
  end

  create_table "fishbowl_logs", id: :serial, force: :cascade do |t|
    t.integer "delayed_job_id"
    t.string "status"
    t.text "success_users"
    t.text "failed_users"
    t.integer "chain_id"
    t.string "log_type"
    t.datetime "initiate_at"
    t.datetime "completed_at"
    t.text "error_report"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fishbowl_promotion_codes", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "reward_id"
    t.string "code"
    t.integer "user_id"
    t.datetime "used_at"
    t.integer "restaurant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_fishbowl_promotion_codes_on_chain_id"
    t.index ["code"], name: "index_fishbowl_promotion_codes_on_code"
    t.index ["restaurant_id"], name: "index_fishbowl_promotion_codes_on_restaurant_id"
    t.index ["reward_id", "user_id"], name: "index_fishbowl_promotion_codes_on_reward_id_and_user_id"
    t.index ["reward_id"], name: "index_fishbowl_promotion_codes_on_reward_id"
    t.index ["user_id"], name: "index_fishbowl_promotion_codes_on_user_id"
  end

  create_table "fishbowl_promotion_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "username"
    t.string "password"
    t.string "client_id"
    t.string "client_secret"
    t.string "brand_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_fishbowl_promotion_settings_on_chain_id"
  end

  create_table "fishbowl_reward_promotion_settings", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.string "promotion_id"
    t.integer "code_batch_size"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reward_id"], name: "index_fishbowl_reward_promotion_settings_on_reward_id"
  end

  create_table "fishbowl_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "api_username"
    t.string "api_password"
    t.string "list_id"
    t.string "site_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "fishbowl_log_id"
    t.boolean "real_time_sync", default: false
    t.string "welcome_email_mailing_id"
    t.string "reward_redeem_mailing_id"
    t.string "reward_redeem_email_title"
    t.string "reward_redeem_expirations_date"
    t.boolean "ignore_marketing_optin", default: false
    t.string "we_miss_you_reward_notification"
    t.string "we_miss_you_point_notification"
    t.string "no_scans_7_days_mailing_id"
    t.string "we_miss_u_mailing_id"
    t.string "browser_welcome_email_mailing_id"
    t.string "ftp_url"
    t.string "ftp_port"
    t.string "ftp_username"
    t.string "ftp_password"
    t.string "ftp_path_directory"
    t.string "points_notification_mailing_id"
    t.string "payment_notification_mailing_id"
    t.text "list_ids"
  end

  create_table "focus_check_upload_details", id: :serial, force: :cascade do |t|
    t.integer "focus_check_upload_id"
    t.string "ticket_id"
    t.float "total"
    t.string "check_date"
    t.integer "employee_num"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["focus_check_upload_id"], name: "index_focus_check_upload_details_on_focus_check_upload_id"
  end

  create_table "focus_check_uploads", id: :serial, force: :cascade do |t|
    t.integer "pos_location_id"
    t.integer "chain_id"
    t.string "check_id"
    t.integer "status"
    t.integer "user_id"
    t.string "user_code"
    t.string "code_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "check_data"
    t.index ["pos_location_id", "check_id", "created_at"], name: "idx_pos_location_check_date"
  end

  create_table "focus_reward_checks", id: :serial, force: :cascade do |t|
    t.integer "pos_location_id"
    t.integer "chain_id"
    t.integer "user_id"
    t.string "reward_code"
    t.integer "status"
    t.integer "error_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "game_notifications", id: :serial, force: :cascade do |t|
    t.integer "locale_id"
    t.integer "chain_id"
    t.text "notification"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "game_point_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.text "email_content"
    t.string "email_subject"
    t.text "notification"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "email_content_html"
  end

  create_table "games", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.float "daily_cap"
    t.float "weekly_cap"
    t.float "monthly_cap"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gcm_unsent_histories", id: :serial, force: :cascade do |t|
    t.string "message_id"
    t.string "error_code"
    t.string "reg_id"
    t.datetime "failed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_id"], name: "index_gcm_unsent_histories_on_message_id"
    t.index ["reg_id"], name: "index_gcm_unsent_histories_on_reg_id"
  end

  create_table "general_menu_item_range_values", id: :serial, force: :cascade do |t|
    t.integer "general_menu_item_id"
    t.integer "start_value"
    t.integer "end_value"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_chain_id_general_menu_item_range_values"
    t.index ["general_menu_item_id"], name: "index_general_menu_item_id_general_menu_item_range_values"
  end

  create_table "general_menu_items", id: :serial, force: :cascade do |t|
    t.string "item_name"
    t.text "description"
    t.string "item_keywords"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "pos_location_id"
    t.index ["chain_id"], name: "index_general_menu_items_on_chain_id"
    t.index ["description"], name: "index_general_menu_items_description_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["item_keywords"], name: "index_general_menu_items_item_keywords_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["item_keywords"], name: "index_general_menu_items_on_item_keywords"
    t.index ["item_name"], name: "index_general_menu_items_item_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["item_name"], name: "index_general_menu_items_on_item_name"
    t.index ["pos_location_id"], name: "index_general_menu_items_on_pos_location_id"
  end

  create_table "general_pos_menu_items", id: :serial, force: :cascade do |t|
    t.integer "general_menu_item_id"
    t.integer "pos_menu_item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["general_menu_item_id"], name: "index_general_pos_menu_items_on_general_menu_item_id"
    t.index ["pos_menu_item_id"], name: "index_general_pos_menu_items_on_pos_menu_item_id"
  end

  create_table "geo_data_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.time "collect_start_time"
    t.time "collect_end_time"
    t.time "post_start_time"
    t.time "post_end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "n_days_to_post_from_today"
    t.integer "num_days_retry"
    t.index ["chain_id"], name: "index_geo_data_settings_on_chain_id"
  end

  create_table "gift_card_profiles", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "gift_card_number"
    t.float "amount"
    t.datetime "activate_at"
    t.string "expiration_month"
    t.string "expiration_year"
    t.string "cvv"
    t.string "cvv2"
    t.string "security_code"
    t.integer "vantiv_gift_card_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gift_card_number"], name: "index_gift_card_profiles_on_gift_card_number"
    t.index ["user_id"], name: "index_gift_card_profiles_on_user_id"
  end

  create_table "gift_card_redem_histories", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "receipt_number"
    t.float "amount"
    t.string "transaction_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "transaction_type", default: 0
    t.index ["chain_id"], name: "index_gift_card_redem_histories_on_chain_id"
    t.index ["receipt_number"], name: "index_gift_card_redem_histories_on_receipt_number"
    t.index ["transaction_id"], name: "index_gift_card_redem_histories_on_transaction_id"
    t.index ["transaction_type"], name: "index_gift_card_redem_histories_on_transaction_type"
    t.index ["user_id", "transaction_id"], name: "index_gift_card_redem_histories_on_user_id_and_transaction_id"
    t.index ["user_id"], name: "index_gift_card_redem_histories_on_user_id"
  end

  create_table "gift_code_users", id: :serial, force: :cascade do |t|
    t.string "gift_code"
    t.integer "user_id"
    t.integer "chain_id"
    t.datetime "expired_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id", "gift_code"], name: "index_gift_code_users_on_chain_id_and_gift_code", unique: true
    t.index ["chain_id"], name: "index_gift_code_users_on_chain_id"
    t.index ["gift_code"], name: "index_gift_code_users_on_gift_code"
    t.index ["user_id", "gift_code"], name: "index_gift_code_users_on_user_id_and_gift_code"
    t.index ["user_id"], name: "index_gift_code_users_on_user_id"
  end

  create_table "gift_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.string "email_subject"
    t.text "email_content"
    t.text "warning_message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "giftcard_notification_emails", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.string "subject_email"
    t.text "content_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "send_barcode", default: false
  end

  create_table "giftcard_skins", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "title"
    t.text "description"
    t.string "app_image_file_name"
    t.string "app_image_content_type"
    t.integer "app_image_file_size"
    t.datetime "app_image_updated_at"
    t.string "email_header_image_file_name"
    t.string "email_header_image_content_type"
    t.integer "email_header_image_file_size"
    t.datetime "email_header_image_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_active", default: false
    t.index ["chain_id"], name: "index_giftcard_skins_on_chain_id"
  end

  create_table "giftcard_transactions", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "barcode_file_name"
    t.string "barcode_content_type"
    t.string "barcode_file_size"
    t.string "barcode_updated_at"
    t.string "giftcard_amount"
    t.string "giftee_email"
    t.string "giftee_name"
    t.text "custom_message"
    t.string "gifter_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_giftcard_transactions_on_chain_id"
  end

  create_table "goal_categories", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 1
    t.index ["chain_id"], name: "index_goal_categories_on_chain_id"
  end

  create_table "goal_category_locales", id: :serial, force: :cascade do |t|
    t.integer "category_id"
    t.integer "locale_id"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id", "locale_id"], name: "index_goal_category_locales_on_category_id_and_locale_id"
    t.index ["category_id"], name: "index_goal_category_locales_on_category_id"
    t.index ["locale_id"], name: "index_goal_category_locales_on_locale_id"
  end

  create_table "goal_locales", id: :serial, force: :cascade do |t|
    t.integer "goal_id"
    t.integer "locale_id"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["goal_id", "locale_id"], name: "index_goal_locales_on_goal_id_and_locale_id"
    t.index ["goal_id"], name: "index_goal_locales_on_goal_id"
    t.index ["locale_id"], name: "index_goal_locales_on_locale_id"
  end

  create_table "goal_user_incentives", id: :serial, force: :cascade do |t|
    t.integer "incentive_id"
    t.string "incentive_type"
    t.integer "goal_id"
    t.integer "week_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["goal_id"], name: "index_goal_user_incentives_on_goal_id"
    t.index ["incentive_id", "incentive_type"], name: "index_goal_user_incentives_on_incentive_id_and_incentive_type"
    t.index ["incentive_id"], name: "index_goal_user_incentives_on_incentive_id"
    t.index ["incentive_type"], name: "index_goal_user_incentives_on_incentive_type"
    t.index ["user_id"], name: "index_goal_user_incentives_on_user_id"
    t.index ["week_id"], name: "index_goal_user_incentives_on_week_id"
  end

  create_table "goals", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "category_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "incentive_type"
    t.integer "incentive_id"
    t.integer "status", default: 1
    t.index ["category_id"], name: "index_goals_on_category_id"
    t.index ["chain_id", "category_id"], name: "index_goals_on_chain_id_and_category_id"
    t.index ["chain_id"], name: "index_goals_on_chain_id"
  end

# Could not dump table "guest_dna_users" because of following StandardError
#   Unknown type 'gender' for column 'gender'

  create_table "guest_relations", id: :serial, force: :cascade do |t|
    t.integer "surveys_user_id"
    t.integer "owner_id"
    t.integer "chain_id"
    t.integer "job_type"
    t.integer "points"
    t.integer "reward_id"
    t.integer "status"
    t.integer "approved_owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_guest_relations_on_chain_id"
    t.index ["owner_id"], name: "index_guest_relations_on_owner_id"
    t.index ["surveys_user_id", "owner_id"], name: "index_guest_relations_on_surveys_user_id_and_owner_id"
    t.index ["surveys_user_id"], name: "index_guest_relations_on_surveys_user_id"
  end

  create_table "history_notifications", id: :serial, force: :cascade do |t|
    t.integer "plain_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "plain_kind"
    t.integer "status"
    t.text "error_text"
    t.index ["plain_id"], name: "index_history_notifications_on_plain_id"
    t.index ["plain_kind"], name: "index_history_notifications_on_plain_kind"
    t.index ["user_id", "plain_id"], name: "index_history_notifications_on_user_id_and_plain_id"
    t.index ["user_id"], name: "index_history_notifications_on_user_id"
  end

# Could not dump table "hybrid_reward_redemption_flows" because of following StandardError
#   Unknown type 'hybrid_redemption_flow' for column 'flow'

  create_table "launch_texts", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.string "title"
    t.text "subtitle"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "levels", id: :serial, force: :cascade do |t|
    t.integer "game_id"
    t.integer "number"
    t.float "ratio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
  end

  create_table "levels_points", id: :serial, force: :cascade do |t|
    t.integer "level_number"
    t.float "points"
    t.integer "user_id"
    t.integer "game_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "game_point_submitted"
    t.index ["game_id", "user_id"], name: "index_levels_points_on_game_id_and_user_id"
    t.index ["user_id"], name: "index_levels_points_on_user_id"
  end

  create_table "line_items", id: :serial, force: :cascade do |t|
    t.string "item"
    t.decimal "price"
    t.integer "number_of_item"
    t.integer "receipt_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["receipt_id"], name: "index_line_items_on_receipt_id"
  end

  create_table "locales", id: :serial, force: :cascade do |t|
    t.string "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.datetime "deleted_at"
  end

  create_table "mailchimp_logs", id: :serial, force: :cascade do |t|
    t.integer "delayed_job_id"
    t.string "status"
    t.text "success_users"
    t.text "failed_users"
    t.integer "chain_id"
    t.string "log_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "initiate_at"
    t.datetime "completed_at"
    t.text "error_report"
  end

  create_table "memberships", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "chain_id"
    t.boolean "firstTime", default: false
    t.integer "pointBalance", default: 0
    t.integer "pointsLifetime"
    t.decimal "cashLifetime"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["chain_id"], name: "index_memberships_on_chain_id"
    t.index ["user_id"], name: "index_memberships_on_user_id"
  end

  create_table "message_incentive_locales", id: :serial, force: :cascade do |t|
    t.integer "goal_id"
    t.integer "locale_id"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["goal_id", "locale_id"], name: "index_message_incentive_locales_on_goal_id_and_locale_id"
    t.index ["goal_id"], name: "index_message_incentive_locales_on_goal_id"
    t.index ["locale_id"], name: "index_message_incentive_locales_on_locale_id"
  end

  create_table "micros_check_data", id: :serial, force: :cascade do |t|
    t.integer "pos_check_upload_id"
    t.integer "chain_id"
    t.integer "pos_location_id"
    t.string "rvc_num"
    t.string "emp_num"
    t.integer "check_state"
    t.datetime "created_at"
    t.string "check_id"
    t.string "check_num"
    t.string "seq_num"
    t.string "receipt_number"
    t.string "table_num"
    t.string "check_open_time"
    t.date "receipt_date"
    t.float "subtotal"
    t.float "sales_total"
    t.float "total"
    t.float "tax"
    t.float "discount_total"
    t.float "direct_tips"
    t.float "tip_total"
    t.float "due_total"
    t.index ["chain_id", "pos_location_id", "check_state", "rvc_num", "created_at"], name: "micros_check_index"
    t.index ["pos_check_upload_id"], name: "index_micros_check_data_on_pos_check_upload_id"
  end

  create_table "milestone_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.string "email_subject"
    t.text "email_content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_milestone_notifications_on_chain_id"
    t.index ["locale_id", "chain_id"], name: "index_milestone_notifications_on_locale_id_and_chain_id"
    t.index ["locale_id"], name: "index_milestone_notifications_on_locale_id"
  end

  create_table "milestone_points", id: :serial, force: :cascade do |t|
    t.integer "points"
    t.integer "reward_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["reward_id"], name: "index_milestone_points_on_reward_id"
  end

  create_table "milestones", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "type"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "reward_id"
    t.float "milestone_number"
    t.float "threshold_points"
    t.text "description"
    t.index ["chain_id"], name: "index_milestones_on_chain_id"
  end

  create_table "module_histories", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "keychain"
    t.string "android_id"
    t.float "lat"
    t.float "long"
    t.string "module_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "momentfeed_settings", id: :serial, force: :cascade do |t|
    t.string "api_key"
    t.string "base_url"
    t.string "update_type"
    t.integer "update_interval"
    t.string "update_interval_option"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "netpos_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "root_url"
    t.string "login"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_netpos_settings_on_chain_id"
  end

  create_table "no_scan_job_schedulers", id: :serial, force: :cascade do |t|
    t.integer "job_id"
    t.string "run_type", limit: 255
    t.datetime "start_at"
    t.datetime "executed_at"
    t.datetime "last_executed"
    t.datetime "next_run"
    t.datetime "end_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "no_scan_jobs", id: :serial, force: :cascade do |t|
    t.text "description"
    t.integer "job_type"
    t.integer "reward_type"
    t.integer "points"
    t.integer "owner_id"
    t.integer "status"
    t.integer "amount_min"
    t.integer "amount_max"
    t.integer "total_users"
    t.integer "chain_id"
    t.integer "reward_id"
    t.integer "activity_days"
    t.integer "percentage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "executed_at"
    t.text "subject_email"
    t.text "content_email"
    t.boolean "push_email", default: true
    t.boolean "push_phone", default: true
    t.string "subject_phone", limit: 255
    t.text "content_phone"
    t.integer "schedule_type"
    t.boolean "notification", default: false
    t.boolean "miss_you", default: false
    t.boolean "individual", default: false
    t.integer "individual_user_id"
    t.string "individual_email", limit: 255
    t.integer "task_definition_id"
    t.string "miss_you_reward_fishbowl_mailing_id", limit: 255
    t.string "miss_you_point_fishbowl_mailing_id", limit: 255
    t.boolean "is_fishbowl", default: false
    t.boolean "is_standard_email", default: false
    t.string "zone", limit: 255
    t.integer "kind", default: 0
    t.integer "no_scan_id"
    t.integer "no_scan_definition_id"
  end

  create_table "no_scan_schedulers", id: :serial, force: :cascade do |t|
    t.integer "job_id"
    t.string "run_type", limit: 255
    t.datetime "start_at"
    t.datetime "executed_at"
    t.datetime "last_executed"
    t.datetime "next_run"
    t.datetime "end_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "noscanjob_logs", id: :serial, force: :cascade do |t|
    t.integer "no_scan_job_id"
    t.integer "status"
    t.integer "user_id"
    t.string "email"
    t.date "date_sent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "notification_status"
    t.index ["no_scan_job_id", "status"], name: "index_noscanjob_logs_on_no_scan_job_id_and_status"
    t.index ["no_scan_job_id"], name: "index_noscanjob_logs_on_no_scan_job_id"
    t.index ["user_id"], name: "index_noscanjob_logs_on_user_id"
  end

  create_table "notification_locales", id: :serial, force: :cascade do |t|
    t.integer "notifiable_id"
    t.string "notifiable_type"
    t.string "notification"
    t.string "email_subject"
    t.text "email_content"
    t.integer "locale_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.text "email_content_html"
    t.index ["locale_id"], name: "index_notification_locales_on_locale_id"
    t.index ["notifiable_id", "notifiable_type"], name: "index_notification_locales_on_notifiable_id_and_notifiable_type"
  end

  create_table "notification_rewards", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "notification_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "notifiable_id"
    t.string "notifiable_type"
    t.integer "send_type"
    t.string "email_template"
    t.string "apikey"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "custom_email_template"
    t.index ["chain_id"], name: "index_notification_settings_on_chain_id"
    t.index ["notifiable_id", "notifiable_type"], name: "notification_setting_index"
  end

  create_table "nuorder_connect_settings", id: :serial, force: :cascade do |t|
    t.string "api_root"
    t.string "requester_id"
    t.string "license_code"
    t.string "passcode"
    t.string "subscriber_id"
    t.string "location_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offer_rule_menu_items", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "offer_rule_id"
    t.integer "general_menu_item_id"
    t.boolean "required", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["offer_rule_id", "general_menu_item_id"], name: "offer_general_offer_index"
    t.index ["offer_rule_id"], name: "offer_rule_index_id"
  end

  create_table "offer_rules", id: :serial, force: :cascade do |t|
    t.integer "offer_id"
    t.date "start_date"
    t.date "end_date"
    t.string "time_start"
    t.string "time_end"
    t.integer "days_of_week"
    t.integer "rule_type"
    t.integer "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "constant_value"
    t.float "multiplier"
    t.string "day_of_week_converter"
    t.string "name"
    t.text "description"
    t.boolean "status", default: true
    t.float "min_amount", default: 0.0
    t.float "max_amount", default: 0.0
    t.index ["offer_id"], name: "index_offer_rules_on_offer_id"
    t.index ["rule_type"], name: "index_offer_rules_on_rule_type"
  end

  create_table "offers", id: :serial, force: :cascade do |t|
    t.string "name"
    t.decimal "multiplier", precision: 8, scale: 2
    t.date "effectiveDate"
    t.date "expiryDate"
    t.string "timeStart"
    t.string "timeEnd"
    t.integer "daysOfWeek"
    t.boolean "isActive"
    t.text "fineprint"
    t.float "bonus_points"
    t.float "bonus_points_ftu"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "survey_id"
    t.datetime "deleted_at"
    t.boolean "is_multiplier", default: true
    t.decimal "constant_value"
    t.boolean "is_online_order", default: false
    t.boolean "having_rule", default: false
    t.index ["chain_id"], name: "index_offers_on_chain_id"
    t.index ["name"], name: "index_offers_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
  end

  create_table "olo_connect_settings", id: :serial, force: :cascade do |t|
    t.string "client_id"
    t.string "client_secret"
    t.string "api_root"
    t.string "apikey"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider"
    t.string "iphone_api_key"
    t.string "android_api_key"
  end

  create_table "olo_executions", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.date "executed_at"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["executed_at"], name: "index_olo_executions_on_executed_at"
  end

  create_table "olo_order_details", id: :serial, force: :cascade do |t|
    t.integer "olo_order_id"
    t.bigint "olo_ref"
    t.string "store_name"
    t.integer "store_ref"
    t.string "email"
    t.boolean "is_guest"
    t.float "subtotal"
    t.float "discount"
    t.float "tax"
    t.float "total"
    t.date "time_placed"
    t.date "time_closed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "issue_time"
    t.integer "receipt_id"
    t.boolean "processed", default: false
    t.date "time_wanted"
    t.index ["olo_order_id"], name: "index_olo_order_details_on_olo_order_id"
    t.index ["receipt_id"], name: "index_olo_order_details_on_receipt_id"
  end

  create_table "olo_orders", id: :serial, force: :cascade do |t|
    t.integer "batch_id"
    t.text "xml_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "chain_id"
    t.index ["chain_id", "batch_id"], name: "index_olo_orders_on_chain_id_and_batch_id"
    t.index ["chain_id"], name: "index_olo_orders_on_chain_id"
  end

  create_table "olo_profiles", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "account_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_olo_profiles_on_user_id"
  end

  create_table "olo_webhooks", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "receipt_id"
    t.string "restaurant"
    t.string "email"
    t.boolean "is_match_restauran", default: false
  end

  create_table "olorewards_settings", id: :serial, force: :cascade do |t|
    t.string "basic_auth"
    t.integer "chain_id"
    t.string "basic_pwd"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "online_offers", id: :serial, force: :cascade do |t|
    t.string "name"
    t.decimal "multiplier"
    t.date "effectiveDate"
    t.date "expiryDate"
    t.string "timeStart"
    t.string "timeEnd"
    t.integer "daysOfWeek"
    t.boolean "isActive"
    t.text "fineprint"
    t.float "bonus_points"
    t.float "bonus_points_ftu"
    t.integer "chain_id"
    t.integer "survey_id"
    t.datetime "deleted_at"
    t.boolean "is_multiplier", default: true
    t.decimal "constant_value"
    t.integer "smg_survey_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_online_offers_on_chain_id"
  end

  create_table "online_orders", id: :serial, force: :cascade do |t|
    t.string "email"
    t.text "body"
    t.text "parameter"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "subject"
    t.integer "receipt_id"
    t.integer "chain_id"
    t.index ["chain_id"], name: "index_online_orders_on_chain_id"
  end

  create_table "online_parse_results", id: :serial, force: :cascade do |t|
    t.integer "online_order_id"
    t.string "order_id"
    t.integer "user_id"
    t.integer "chain_id"
    t.integer "restaurant_id"
    t.string "location"
    t.string "order_no"
    t.string "customer_no"
    t.string "restaurant"
    t.string "chain"
    t.string "email"
    t.float "subtotal"
    t.float "total"
    t.float "tax"
    t.date "date_pickup"
    t.date "date_placed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "issue_time"
    t.index ["online_order_id", "chain_id"], name: "index_online_parse_results_on_online_order_id_and_chain_id"
  end

  create_table "onosys_connect_settings", id: :serial, force: :cascade do |t|
    t.string "api_root"
    t.string "api_key"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "onosys_online_order_details", id: :serial, force: :cascade do |t|
    t.integer "onosys_online_order_id"
    t.text "order_detail"
    t.decimal "sub_total"
    t.decimal "tax"
    t.decimal "total"
    t.decimal "points", default: "0.0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "discount"
    t.index ["onosys_online_order_id"], name: "index_onosys_online_order_details_on_onosys_online_order_id"
  end

  create_table "onosys_online_orders", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "restaurant_id"
    t.integer "user_id"
    t.string "online_order_identifier"
    t.integer "loyalty_status"
    t.integer "platform"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.index ["chain_id"], name: "index_onosys_online_orders_on_chain_id"
    t.index ["online_order_identifier"], name: "index_onosys_online_orders_on_online_order_identifier"
    t.index ["restaurant_id"], name: "index_onosys_online_orders_on_restaurant_id"
    t.index ["user_id"], name: "index_onosys_online_orders_on_user_id"
  end

  create_table "onosys_profiles", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "account_id"
    t.text "access_token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_onosys_profiles_on_account_id"
    t.index ["user_id"], name: "index_onosys_profiles_on_user_id"
  end

  create_table "onosys_receipt_online_orders", id: :serial, force: :cascade do |t|
    t.integer "receipt_id"
    t.integer "onosys_online_order_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "open_close_restaurant_days", id: :serial, force: :cascade do |t|
    t.integer "restaurant_id"
    t.date "day_time"
    t.boolean "open", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["day_time"], name: "index_open_close_restaurant_days_on_day_time"
    t.index ["open"], name: "index_open_close_restaurant_days_on_open"
    t.index ["restaurant_id", "day_time"], name: "index_open_close_restaurant_days_on_restaurant_id_and_day_time"
    t.index ["restaurant_id"], name: "index_open_close_restaurant_days_on_restaurant_id"
  end

  create_table "owner_job_histories", id: :serial, force: :cascade do |t|
    t.integer "owner_job_id"
    t.text "success"
    t.text "failed"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "owner_jobs", id: :serial, force: :cascade do |t|
    t.text "description"
    t.integer "job_type"
    t.integer "reward_type"
    t.integer "points"
    t.integer "owner_id"
    t.integer "status"
    t.integer "amount_min"
    t.integer "amount_max"
    t.integer "total_users"
    t.integer "chain_id"
    t.integer "reward_id"
    t.integer "activity_days"
    t.integer "percentage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "executed_at"
    t.text "subject_email"
    t.text "content_email"
    t.boolean "push_email", default: true
    t.boolean "push_phone", default: true
    t.string "subject_phone"
    t.text "content_phone"
    t.integer "schedule_type"
    t.boolean "notification", default: false
    t.boolean "miss_you", default: false
    t.boolean "individual", default: false
    t.integer "individual_user_id"
    t.string "individual_email"
    t.integer "task_definition_id"
    t.string "miss_you_reward_fishbowl_mailing_id"
    t.string "miss_you_point_fishbowl_mailing_id"
    t.boolean "is_fishbowl", default: false
    t.boolean "is_standard_email", default: false
    t.string "zone"
  end

  create_table "owner_schedulers", id: :serial, force: :cascade do |t|
    t.integer "job_id"
    t.string "run_type"
    t.datetime "start_at"
    t.datetime "executed_at"
    t.datetime "last_executed"
    t.datetime "next_run"
    t.datetime "end_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["job_id"], name: "index_owner_schedulers_on_job_id"
  end

  create_table "ownerjob_logs", id: :serial, force: :cascade do |t|
    t.integer "owner_job_id"
    t.integer "status"
    t.integer "user_id"
    t.string "email"
    t.date "date_sent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "notification_status"
    t.index ["owner_job_id", "status"], name: "index_ownerjob_logs_on_owner_job_id_and_status"
    t.index ["owner_job_id"], name: "index_ownerjob_logs_on_owner_job_id"
    t.index ["user_id"], name: "index_ownerjob_logs_on_user_id"
  end

  create_table "owners", id: :serial, force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "address"
    t.string "zipcode"
    t.integer "city_id"
    t.string "work_contact_number"
    t.string "cell_contact_number"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "parent_id"
    t.string "title"
    t.boolean "daily", default: false
    t.boolean "weekly", default: false
    t.boolean "monthly", default: false
    t.boolean "periodic", default: false
    t.integer "role_id"
    t.boolean "is_active", default: true
    t.date "last_report_run"
    t.string "role"
    t.index ["city_id"], name: "index_owners_on_city_id"
    t.index ["email"], name: "index_owners_on_email", unique: true
    t.index ["parent_id"], name: "index_owners_on_parent_id"
    t.index ["reset_password_token"], name: "index_owners_on_reset_password_token", unique: true
  end

  create_table "partner_categories", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_partner_categories_on_chain_id"
  end

  create_table "partner_code_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "barcode_format"
    t.string "barcode_format_type"
    t.string "pos_digit_barcode"
    t.integer "appcode_type"
    t.integer "timer", default: 0
    t.integer "zero_padding", default: 0
    t.string "prefix"
    t.string "postfix"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_partner_code_settings_on_chain_id"
  end

  create_table "partner_hours", id: :serial, force: :cascade do |t|
    t.integer "partner_reward_id"
    t.integer "day_of_week"
    t.time "start"
    t.time "end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_reward_id"], name: "index_partner_hours_on_partner_reward_id"
  end

  create_table "partner_menu_items", id: :serial, force: :cascade do |t|
    t.string "item_name"
    t.integer "item_number"
    t.text "description"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "partner_sub_category_id"
    t.index ["chain_id"], name: "index_partner_menu_items_on_chain_id"
    t.index ["item_name"], name: "index_partner_menu_items_on_item_name"
    t.index ["item_number"], name: "index_partner_menu_items_on_item_number"
    t.index ["partner_sub_category_id"], name: "index_partner_menu_items_on_partner_sub_category_id"
  end

  create_table "partner_menu_items_restaurants", id: :serial, force: :cascade do |t|
    t.integer "partner_menu_item_id"
    t.integer "restaurant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_menu_item_id"], name: "index_partner_menu_items_restaurants_on_partner_menu_item_id"
    t.index ["restaurant_id"], name: "index_partner_menu_items_restaurants_on_restaurant_id"
  end

  create_table "partner_reward_categories", id: :serial, force: :cascade do |t|
    t.integer "partner_reward_id"
    t.integer "category_id"
    t.boolean "mandatory"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "partner_reward_codes", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "code"
    t.integer "reward_id"
    t.string "user_id"
    t.string "champain_id"
    t.integer "number_of_valid_days"
    t.boolean "used", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "expiration_date"
    t.index ["chain_id"], name: "index_partner_reward_codes_on_chain_id"
    t.index ["champain_id"], name: "index_partner_reward_codes_on_champain_id"
    t.index ["code"], name: "index_partner_reward_codes_on_code"
    t.index ["used"], name: "index_partner_reward_codes_on_used"
    t.index ["user_id"], name: "index_partner_reward_codes_on_user_id"
  end

  create_table "partner_reward_discounts", id: :serial, force: :cascade do |t|
    t.integer "partner_reward_id"
    t.integer "category_id"
    t.boolean "mandatory"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "partner_rewards", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.boolean "valid_all_locations"
    t.integer "order_minimum"
    t.string "discount_type"
    t.string "discount_value"
    t.string "minimum_count_order_from_category"
    t.string "discount_count_from_category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "valid_locations", array: true
    t.integer "discount_order", default: 0
    t.integer "discount_source_price", default: 0
    t.index ["reward_id"], name: "index_partner_rewards_on_reward_id"
  end

  create_table "partner_sub_categories", id: :serial, force: :cascade do |t|
    t.integer "partner_category_id"
    t.string "name"
    t.text "description"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_partner_sub_categories_on_chain_id"
    t.index ["name", "chain_id", "partner_category_id"], name: "name_chain_and_partner_category"
    t.index ["name"], name: "index_partner_sub_categories_on_name"
    t.index ["partner_category_id"], name: "index_partner_sub_categories_on_partner_category_id"
  end

  create_table "partner_sub_category_menu_items", id: :serial, force: :cascade do |t|
    t.integer "partner_sub_category_id"
    t.integer "partner_menu_item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "restaurant_id"
    t.index ["restaurant_id"], name: "index_partner_sub_category_menu_items_on_restaurant_id"
  end

  create_table "partner_webhook_settings", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pay_code_users", id: :serial, force: :cascade do |t|
    t.string "pay_code"
    t.integer "user_id"
    t.integer "chain_id"
    t.datetime "expired_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id", "pay_code"], name: "index_pay_code_users_on_chain_id_and_pay_code", unique: true
    t.index ["chain_id"], name: "index_pay_code_users_on_chain_id"
    t.index ["pay_code"], name: "index_pay_code_users_on_pay_code"
    t.index ["user_id", "pay_code"], name: "index_pay_code_users_on_user_id_and_pay_code"
    t.index ["user_id"], name: "index_pay_code_users_on_user_id"
  end

  create_table "payment_histories", id: :serial, force: :cascade do |t|
    t.string "mp_payment_id"
    t.integer "user_id"
    t.integer "restaurant_id"
    t.float "amount"
    t.string "pay_code"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at"
    t.integer "user_session_id"
    t.integer "status"
    t.string "action"
    t.index ["action"], name: "index_payment_histories_on_action"
    t.index ["chain_id", "user_id", "pay_code", "status", "action"], name: "idx_chain_id_user_id_pay_code_status_action_ph"
    t.index ["chain_id", "user_id"], name: "index_payment_histories_on_chain_id_and_user_id"
    t.index ["chain_id"], name: "index_payment_histories_on_chain_id"
    t.index ["restaurant_id"], name: "index_payment_histories_on_restaurant_id"
    t.index ["status"], name: "index_payment_histories_on_status"
    t.index ["user_id", "restaurant_id"], name: "index_payment_histories_on_user_id_and_restaurant_id"
    t.index ["user_id"], name: "index_payment_histories_on_user_id"
    t.index ["user_session_id"], name: "index_payment_histories_on_user_session_id"
  end

  create_table "perk_wallets", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "perk_id"
    t.datetime "expiry_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status"
    t.datetime "deleted_at"
    t.index ["perk_id"], name: "index_perk_wallets_on_perk_id"
    t.index ["user_id"], name: "index_perk_wallets_on_user_id"
  end

  create_table "perks", id: :serial, force: :cascade do |t|
    t.decimal "multiplier", precision: 8, scale: 2
    t.float "bonus_points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "stackable", default: false
    t.integer "bonus_threshold"
    t.integer "perk_type"
    t.integer "chain_id"
    t.datetime "deleted_at"
    t.index ["chain_id"], name: "index_perks_on_chain_id"
  end

  create_table "perks_restaurants", id: :serial, force: :cascade do |t|
    t.integer "perk_id"
    t.integer "restaurant_id"
    t.datetime "deleted_at"
  end

  create_table "plain_push_notifications", id: :serial, force: :cascade do |t|
    t.text "email"
    t.text "content"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "chains"
    t.boolean "is_push_email", default: true
    t.boolean "is_push_phone", default: true
    t.datetime "executed_at"
    t.integer "admin_id"
    t.integer "testing_mode"
    t.boolean "is_all_user", default: false
    t.index ["chain_id"], name: "index_plain_push_notifications_on_chain_id"
  end

  create_table "point_histories", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "description"
    t.float "point"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "group_no", default: 0
    t.integer "assignment_id"
    t.index ["assignment_id"], name: "index_point_histories_on_assignment_id"
    t.index ["user_id", "group_no"], name: "index_point_histories_on_user_id_and_group_no"
  end

  create_table "point_push_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.text "email_content"
    t.string "email_subject"
    t.string "notification"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "email_content_html"
  end

  create_table "pos_check_uploads", id: :serial, force: :cascade do |t|
    t.integer "pos_location_id"
    t.integer "chain_id"
    t.text "xml_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "barcode"
    t.string "check_id"
    t.integer "status"
    t.string "sequence_number"
    t.string "user_code"
    t.integer "user_id"
    t.integer "error_code"
    t.integer "service"
    t.boolean "payment_included", default: false
    t.string "code_type"
    t.string "old_barcode"
    t.boolean "is_xpient_check", default: false
    t.integer "pos_used_type", default: 1
    t.string "rvc_num"
    t.string "tran_num"
    t.string "emp_num"
    t.string "tbl_num"
    t.index ["barcode"], name: "index_pos_check_uploads_barcode_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["barcode"], name: "index_pos_check_uploads_on_barcode"
    t.index ["chain_id", "check_id", "sequence_number", "rvc_num"], name: "idx_chain_id_seq_num_check_id_rvc_num_pcu"
    t.index ["chain_id"], name: "index_pos_check_uploads_on_chain_id"
    t.index ["check_id"], name: "index_pos_check_uploads_on_check_id"
    t.index ["emp_num"], name: "index_pos_check_uploads_on_emp_num"
    t.index ["old_barcode"], name: "index_pos_check_uploads_on_old_barcode"
    t.index ["pos_location_id"], name: "index_pos_check_uploads_on_pos_location_id"
    t.index ["rvc_num", "emp_num"], name: "index_pos_check_uploads_on_rvc_num_and_emp_num"
    t.index ["sequence_number"], name: "index_pos_check_uploads_on_sequence_number"
    t.index ["tbl_num"], name: "index_pos_check_uploads_on_tbl_num"
    t.index ["user_id"], name: "index_pos_check_uploads_on_user_id"
  end

  create_table "pos_discount_types", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.string "title"
    t.integer "discount_type"
    t.decimal "discount_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "min_no_items_ordered", default: 1
    t.integer "min_no_items_discounted", default: 1
    t.index ["reward_id"], name: "index_pos_discount_types_on_reward_id"
  end

  create_table "pos_locations", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "apikey"
    t.integer "restaurant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "menu_upload_status", default: true
    t.boolean "check_status", default: true
    t.string "POS_StoreID"
    t.string "POS_ChainID"
    t.string "netpos_timezone"
  end

  create_table "pos_menu_items", id: :serial, force: :cascade do |t|
    t.integer "pos_menu_id"
    t.string "item_name"
    t.integer "item_number"
    t.text "description"
    t.integer "general_menu_item_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "pos_location_id"
    t.integer "level_size_id"
    t.index ["chain_id"], name: "index_pos_menu_items_on_chain_id"
    t.index ["description"], name: "index_pos_menu_items_description_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["general_menu_item_id"], name: "index_pos_menu_items_on_general_menu_item_id"
    t.index ["item_name"], name: "index_pos_menu_items_item_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["item_name"], name: "index_pos_menu_items_on_item_name"
    t.index ["item_number", "chain_id", "pos_location_id"], name: "item_number_chain_and_pos_location"
    t.index ["item_number"], name: "index_pos_menu_items_on_item_number"
    t.index ["level_size_id"], name: "index_pos_menu_items_on_level_size_id"
    t.index ["pos_location_id"], name: "index_pos_menu_items_on_pos_location_id"
    t.index ["pos_menu_id"], name: "index_pos_menu_items_on_pos_menu_id"
  end

  create_table "pos_menu_uploads", id: :serial, force: :cascade do |t|
    t.integer "pos_location_id"
    t.integer "chain_id"
    t.text "xml_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pos_menus", id: :serial, force: :cascade do |t|
    t.string "pos_menu_type"
    t.integer "pos_location_id"
    t.text "menu_data"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pos_receipt_users", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "offer_id"
    t.integer "restaurant_id"
    t.integer "restaurant_offer_id"
    t.integer "chain_id"
    t.string "barcode"
    t.integer "status", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pos_receipts", id: :serial, force: :cascade do |t|
    t.datetime "receipt_time"
    t.float "total"
    t.float "tips"
    t.float "tax"
    t.float "item_sales"
    t.string "barcode"
    t.text "menu_items"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 1
  end

  create_table "pos_reward_checks", id: :serial, force: :cascade do |t|
    t.integer "pos_location_id"
    t.integer "chain_id"
    t.text "xml_data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.string "reward_code"
    t.integer "status"
    t.text "error_description"
    t.integer "pos_used_type", default: 1
    t.integer "reward_transaction_id"
    t.integer "transaction_type", default: 0
    t.index ["reward_code"], name: "index_pos_reward_checks_on_reward_code"
    t.index ["reward_transaction_id"], name: "index_pos_reward_checks_on_reward_transaction_id"
    t.index ["status"], name: "index_pos_reward_checks_on_status"
    t.index ["user_id"], name: "index_pos_reward_checks_on_user_id"
  end

  create_table "privacy_policies", id: :serial, force: :cascade do |t|
    t.text "content"
    t.integer "locale_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promo_codes", id: :serial, force: :cascade do |t|
    t.string "code"
    t.boolean "used", default: false
    t.boolean "shared", default: false
    t.integer "promotion_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "from_user_id"
    t.index ["chain_id"], name: "index_promo_codes_on_chain_id"
    t.index ["code"], name: "index_promo_codes_code_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["code"], name: "index_promo_codes_on_code"
    t.index ["promotion_id", "chain_id"], name: "index_promo_codes_on_promotion_id_and_chain_id"
    t.index ["promotion_id"], name: "index_promo_codes_on_promotion_id"
  end

  create_table "promotion_locales", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "locale"
    t.string "alert"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_promotion_locales_on_chain_id"
    t.index ["locale"], name: "index_promotion_locales_on_locale"
  end

  create_table "promotion_offers", id: :serial, force: :cascade do |t|
    t.string "title"
    t.integer "points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "chain_id"
    t.index ["chain_id"], name: "index_promotion_offers_on_chain_id"
  end

  create_table "promotions", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "promotable_id"
    t.string "promotable_type"
    t.datetime "effective_date"
    t.datetime "expiry_date"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "quantity"
    t.datetime "deleted_at"
    t.integer "limit", default: 0
    t.boolean "is_repeated", default: false
    t.integer "duration", default: 0
    t.index ["chain_id"], name: "index_promotions_on_chain_id"
    t.index ["promotable_id", "promotable_type"], name: "index_promotions_on_promotable_id_and_promotable_type"
  end

  create_table "pull_surveys", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "receipt_id"
    t.integer "survey_id"
    t.integer "user_id"
    t.boolean "skipped", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_chain_id_pull_surveys"
    t.index ["receipt_id"], name: "index_receipt_id_pull_surveys"
    t.index ["survey_id"], name: "index_survey_id_pull_surveys"
    t.index ["user_id"], name: "index_user_id_pull_surveys"
  end

  create_table "push_log_entries", id: :serial, force: :cascade do |t|
    t.string "pushable_type"
    t.integer "pushable_id"
    t.text "logs"
    t.integer "log_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pushable_id", "pushable_type"], name: "index_push_log_entries_on_pushable_id_and_pushable_type"
  end

  create_table "push_points", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "points"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "executed_at"
    t.integer "push_type", default: 1
    t.text "user_ids"
    t.text "success_logs"
    t.text "error_logs"
    t.integer "pusher_id"
    t.integer "testing_mode"
    t.boolean "is_all_user", default: false
    t.index ["chain_id"], name: "index_push_points_on_chain_id"
  end

  create_table "push_rewards", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "reward_id"
    t.integer "push_type"
    t.text "user_ids"
    t.text "success_logs"
    t.text "error_logs"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "pusher_id"
    t.datetime "executed_at"
    t.integer "testing_mode"
    t.boolean "is_all_user", default: false
  end

  create_table "push_service_notification_certificates", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.text "certificate"
    t.string "environment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_push_service_notification_certificates_on_chain_id"
  end

  create_table "push_service_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.text "notification"
    t.datetime "sent_at"
    t.integer "recipient_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "receiver_user_ids", array: true
    t.string "date_text_ttd"
    t.integer "status", default: 0
    t.integer "admin_id"
    t.string "notification_url"
  end

  create_table "push_service_points", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "point"
    t.datetime "sent_at"
    t.integer "recipient_count"
    t.integer "admin_id"
    t.integer "status", default: 0
    t.string "date_text_ttd"
    t.boolean "sent", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "receiver_user_ids", array: true
  end

  create_table "question_choices", id: :serial, force: :cascade do |t|
    t.string "label"
    t.integer "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "value", default: 0.0
    t.datetime "deleted_at"
    t.boolean "is_considered_for_negative_response", default: false
    t.string "non_app_display", limit: 8, default: "value"
    t.index ["question_id"], name: "index_question_choices_on_question_id"
  end

  create_table "questions", id: :serial, force: :cascade do |t|
    t.string "text"
    t.integer "question_type"
    t.integer "survey_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "order_number"
    t.integer "rotating_group_id"
    t.boolean "display_value_bar", default: true
    t.integer "orientation_choice_view", default: 1
    t.boolean "display_choice_text_next_to_radio", default: true
    t.boolean "display_opposing_icon", default: true
    t.integer "min_threshold", default: 0
    t.integer "max_threshold", default: 0
    t.boolean "is_considered_for_negative_response", default: false
    t.string "non_app_display", limit: 8, default: "value"
    t.index ["survey_id"], name: "index_questions_on_survey_id"
  end

  create_table "raffle_additional_informations", id: :serial, force: :cascade do |t|
    t.integer "raffle_id"
    t.string "field_name"
    t.string "app_display_text"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "raffle_manual_histories", id: :serial, force: :cascade do |t|
    t.string "group"
    t.integer "submitted_user_id"
    t.integer "user_id"
    t.integer "chain_id"
    t.integer "raffle_id"
    t.integer "raffle_step_id"
    t.boolean "success"
    t.string "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_raffle_manual_histories_on_chain_id"
    t.index ["group"], name: "index_raffle_manual_histories_on_group"
    t.index ["raffle_id"], name: "index_raffle_manual_histories_on_raffle_id"
    t.index ["raffle_step_id"], name: "index_raffle_manual_histories_on_raffle_step_id"
    t.index ["user_id"], name: "index_raffle_manual_histories_on_user_id"
  end

  create_table "raffle_participation_histories", id: :serial, force: :cascade do |t|
    t.integer "raffle_step_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "raffle_user_information_id"
    t.boolean "custom_api_executed", default: false
  end

  create_table "raffle_steps", id: :serial, force: :cascade do |t|
    t.integer "raffle_id"
    t.string "display_title"
    t.string "display_subtitle"
    t.string "display_description"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: false
    t.text "custom_api_response"
  end

  create_table "raffle_user_informations", id: :serial, force: :cascade do |t|
    t.integer "raffle_id"
    t.integer "user_id"
    t.string "user_email"
    t.string "information_submited"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "date_of_submission"
    t.index ["raffle_id"], name: "index_raffle_user_informations_on_raffle_id"
    t.index ["user_email"], name: "index_raffle_user_informations_on_user_email"
    t.index ["user_id"], name: "index_raffle_user_informations_on_user_id"
  end

  create_table "raffles", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "title"
    t.string "description"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: false
  end

  create_table "receipt_approved_details", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "restaurant_id"
    t.integer "receipt_id"
    t.float "subtotal"
    t.datetime "issue_date"
    t.string "time_stamp"
    t.string "receipt_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["chain_id", "restaurant_id", "subtotal", "issue_date", "time_stamp", "receipt_number"], name: "chain_idsubtotalrestaurantidx_new"
    t.index ["receipt_id"], name: "index_receipt_approved_details_on_receipt_id"
  end

  create_table "receipt_beacons", id: :serial, force: :cascade do |t|
    t.integer "receipt_id"
    t.string "beacon_info"
    t.string "beacon_uuid"
    t.string "beacon_serial_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["receipt_id"], name: "index_receipt_beacons_on_receipt_id"
  end

  create_table "receipt_details", id: :serial, force: :cascade do |t|
    t.integer "receipt_id"
    t.date "issue_date"
    t.time "issue_time"
    t.string "timezone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["receipt_id"], name: "index_receipt_details_on_receipt_id"
  end

  create_table "receipt_focus_check_uploads", id: :serial, force: :cascade do |t|
    t.integer "receipt_id"
    t.integer "focus_check_upload_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["focus_check_upload_id"], name: "index_receipt_focus_check_uploads_on_focus_check_upload_id"
    t.index ["receipt_id"], name: "index_receipt_focus_check_uploads_on_receipt_id"
  end

  create_table "receipt_statuses", id: :serial, force: :cascade do |t|
    t.integer "receipt_transaction_id"
    t.integer "status"
    t.integer "delayed_status"
    t.integer "delayed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["receipt_transaction_id", "delayed_status"], name: "receipt_dj_status_index"
  end

  create_table "receipt_transaction_details", id: :serial, force: :cascade do |t|
    t.integer "receipt_transaction_id"
    t.text "clientfeedback1"
    t.text "clientfeedback2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "receipt_transactions", id: :serial, force: :cascade do |t|
    t.datetime "issue_date"
    t.string "time_stamp"
    t.string "card_number"
    t.string "cashier"
    t.string "server_name"
    t.integer "status"
    t.integer "payment_method"
    t.integer "guests"
    t.string "receipt_number"
    t.decimal "tax"
    t.decimal "subtotal"
    t.decimal "food_total"
    t.decimal "beverage_total"
    t.decimal "preps_total"
    t.decimal "total_payment"
    t.decimal "base_points_earned"
    t.decimal "bonus_earned"
    t.decimal "total_points_earned"
    t.integer "receipt_id"
    t.integer "restaurant_offer_id"
    t.integer "reject_reason_id"
    t.integer "admin_id"
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "restaurant_id"
    t.datetime "deleted_at"
    t.date "receipt_date"
    t.time "receipt_time"
    t.string "review_reason"
    t.text "instructions"
    t.decimal "total_discount"
    t.index ["admin_id"], name: "index_receipt_transactions_on_admin_id"
    t.index ["cashier"], name: "index_receipt_transactions_cashier_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["receipt_id"], name: "index_receipt_transactions_on_receipt_id"
    t.index ["receipt_number"], name: "index_receipt_transactions_receipt_number_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["restaurant_offer_id"], name: "index_receipt_transactions_on_restaurant_offer_id"
    t.index ["server_name"], name: "index_receipt_transactions_server_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
  end

  create_table "receipts", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "user_id"
    t.integer "status"
    t.string "image_url"
    t.string "thumbnail_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.integer "application_id"
    t.datetime "deleted_at"
    t.string "order_kind"
    t.boolean "is_receipt_barcode", default: false
    t.integer "pos_check_upload_id"
    t.boolean "is_online_order", default: false
    t.boolean "is_olo", default: false
    t.string "beacons_info"
    t.index ["chain_id"], name: "index_receipts_on_chain_id"
    t.index ["created_at", "chain_id"], name: "idx_createdat_chaind_id", order: { created_at: :desc }
    t.index ["is_receipt_barcode"], name: "index_receipts_on_is_receipt_barcode"
    t.index ["pos_check_upload_id"], name: "index_receipts_on_pos_check_upload_id"
    t.index ["user_id"], name: "index_receipts_on_user_id"
  end

  create_table "referral_codes", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_referral_codes_on_code", unique: true
    t.index ["user_id"], name: "index_referral_codes_on_user_id", unique: true
  end

  create_table "referral_notifications", id: :serial, force: :cascade do |t|
    t.integer "notifiable_id"
    t.integer "locale_id"
    t.string "notifiable_type"
    t.text "email_content"
    t.string "email_subject"
    t.string "notification"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "facebook_text"
    t.text "twitter_text"
    t.text "other_media_text"
    t.index ["locale_id"], name: "index_referral_notifications_on_locale_id"
    t.index ["notifiable_id", "notifiable_type"], name: "polymorph_notif_referral"
  end

  create_table "referral_offers", id: :serial, force: :cascade do |t|
    t.string "title"
    t.integer "points"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer "chain_id"
    t.index ["chain_id"], name: "index_referral_offers_on_chain_id"
    t.index ["kind"], name: "index_referral_offers_on_kind"
  end

  create_table "referral_programs", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "chain_id"
    t.date "start_date"
    t.date "expiry_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "incentive_type"
    t.integer "incentive_id"
    t.boolean "is_active", default: false
    t.boolean "approve"
    t.string "incentive_referer_type"
    t.integer "incentive_referer_id"
    t.integer "min_approve", default: 0
    t.integer "max_incentive", default: 0
    t.boolean "uniq_device", default: false
    t.index ["chain_id"], name: "index_referral_programs_on_chain_id"
  end

  create_table "referral_users", id: :serial, force: :cascade do |t|
    t.integer "code_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "referral_program_id"
    t.boolean "is_get_incentive", default: true
    t.integer "referral_program_type"
    t.index ["code_id", "user_id"], name: "index_referral_users_on_code_id_and_user_id"
    t.index ["code_id"], name: "index_referral_users_on_code_id"
    t.index ["user_id"], name: "index_referral_users_on_user_id"
  end

  create_table "regions", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "abbreviation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "country_id"
    t.boolean "active", default: true
    t.integer "cities_count", default: 0
    t.datetime "deleted_at"
    t.index ["country_id"], name: "index_regions_on_country_id"
    t.index ["name"], name: "index_regions_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
  end

  create_table "reject_reasons", id: :serial, force: :cascade do |t|
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "reason_key"
    t.datetime "deleted_at"
  end

  create_table "remove_points", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "points"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "user_ids"
    t.text "success_logs"
    t.text "error_logs"
    t.integer "pusher_id"
    t.index ["chain_id"], name: "index_remove_points_on_chain_id"
  end

  create_table "removed_regids", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "reg_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reg_id"], name: "index_removed_regids_on_reg_id"
    t.index ["user_id"], name: "index_removed_regids_on_user_id"
  end

  create_table "report_definitions", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "jasper_path"
    t.text "query"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "asynchronous", default: false
    t.boolean "active", default: true
    t.string "email_subject"
    t.boolean "is_email_content", default: false
  end

  create_table "report_owners", id: :serial, force: :cascade do |t|
    t.integer "owner_id"
    t.integer "subscription_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_id"], name: "index_report_owners_on_owner_id"
    t.index ["subscription_id"], name: "index_report_owners_on_subscription_id"
  end

  create_table "report_results", id: :serial, force: :cascade do |t|
    t.integer "subscription_id"
    t.string "uuid"
    t.string "filename"
    t.text "jasper_session"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "emails"
    t.index ["subscription_id"], name: "index_report_results_on_subscription_id"
  end

  create_table "report_subscriptions", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "report_definition_id"
    t.text "custom_params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "report_kind"
    t.integer "sending_method"
    t.date "start_at"
    t.date "end_at"
    t.text "report_settings"
    t.time "run_at"
    t.string "file_type"
    t.date "last_executed_at"
    t.text "additional_emails"
    t.string "ftp_host"
    t.string "ftp_username"
    t.string "ftp_password"
    t.string "ftp_upload_path"
    t.string "date_format", default: "%Y-%m-%d"
  end

  create_table "reports", id: :serial, force: :cascade do |t|
    t.date "start_date"
    t.date "end_date"
    t.integer "chain_id"
    t.string "s3_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "kind"
    t.string "url"
    t.integer "restaurant_id"
    t.integer "subscription_id"
    t.index ["chain_id"], name: "index_reports_on_chain_id"
    t.index ["restaurant_id"], name: "index_reports_on_restaurant_id"
    t.index ["start_date", "end_date"], name: "index_reports_on_start_date_and_end_date"
  end

  create_table "reset_email_notifications", id: :serial, force: :cascade do |t|
    t.integer "locale_id"
    t.integer "chain_id"
    t.string "subject"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "restaurant_categories", id: :serial, force: :cascade do |t|
    t.integer "restaurant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "partner_sub_category_id"
    t.index ["partner_sub_category_id"], name: "index_restaurant_categories_on_partner_sub_category_id"
    t.index ["restaurant_id"], name: "index_restaurant_categories_on_restaurant_id"
  end

  create_table "restaurant_hours", id: :serial, force: :cascade do |t|
    t.integer "restaurant_id"
    t.integer "day_of_week"
    t.string "open_at"
    t.string "close_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["restaurant_id"], name: "index_restaurant_hours_on_restaurant_id"
  end

  create_table "restaurant_import_histories", id: :serial, force: :cascade do |t|
    t.integer "delayed_job_id"
    t.string "status"
    t.text "success_restaurants"
    t.text "failed_restaurants"
    t.integer "chain_id"
    t.string "log_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_modification"
    t.text "file_name"
    t.string "error_message"
  end

  create_table "restaurant_offers", id: :serial, force: :cascade do |t|
    t.integer "restaurant_id"
    t.integer "offer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["offer_id"], name: "index_restaurant_offers_on_offer_id"
    t.index ["restaurant_id"], name: "index_restaurant_offers_on_restaurant_id"
  end

  create_table "restaurant_online_offers", id: :serial, force: :cascade do |t|
    t.integer "restaurant_id"
    t.integer "online_offer_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["online_offer_id", "restaurant_id"], name: "by_restaurant_offer"
    t.index ["online_offer_id"], name: "index_restaurant_online_offers_on_online_offer_id"
    t.index ["restaurant_id"], name: "index_restaurant_online_offers_on_restaurant_id"
  end

  create_table "restaurant_rewards", id: :serial, force: :cascade do |t|
    t.integer "restaurant_id"
    t.integer "reward_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["restaurant_id"], name: "index_restaurant_rewards_on_restaurant_id"
    t.index ["reward_id"], name: "index_restaurant_rewards_on_reward_id"
  end

  create_table "restaurant_users", id: :serial, force: :cascade do |t|
    t.integer "restaurant_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "joined_date"
    t.datetime "last_active"
    t.float "total_transaction"
    t.date "miss_you_date"
    t.date "last_redeem"
    t.index ["restaurant_id", "miss_you_date", "last_redeem"], name: "miss_you_task_index"
    t.index ["restaurant_id", "user_id"], name: "index_restaurant_users_on_restaurant_id_and_user_id"
    t.index ["restaurant_id"], name: "index_restaurant_users_on_restaurant_id"
    t.index ["user_id"], name: "index_restaurant_users_on_user_id"
  end

  create_table "restaurants", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "app_display_text"
    t.string "dashboard_display_text"
    t.string "address"
    t.string "zipcode"
    t.string "phone_number"
    t.string "contact"
    t.integer "chain_id"
    t.integer "city_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "latitude"
    t.float "longitude"
    t.datetime "deleted_at"
    t.boolean "own_coordinates", default: false
    t.float "rest_tax", default: 0.0
    t.boolean "status", default: true
    t.string "region", limit: 64
    t.string "online_order_link"
    t.string "fishbowl_restaurant_identifier"
    t.boolean "online_order_support_status", default: true
    t.string "beacon_serial_number"
    t.string "beacon_uuid"
    t.string "location_qrcode_identifier"
    t.string "social_link"
    t.index "upper((location_qrcode_identifier)::text)", name: "idx_upper_qrcode_identifier_restaurants"
    t.index ["beacon_serial_number"], name: "index_restaurants_on_beacon_serial_number"
    t.index ["beacon_uuid"], name: "index_restaurants_on_beacon_uuid"
    t.index ["chain_id"], name: "index_restaurants_on_chain_id"
    t.index ["city_id"], name: "index_restaurants_on_city_id"
    t.index ["dashboard_display_text"], name: "index_restaurants_dashboard_display_text_gin_trgm", opclass: :gin_trgm_ops, using: :gin
    t.index ["location_qrcode_identifier"], name: "index_restaurants_on_location_qrcode_identifier"
    t.index ["name"], name: "index_restaurants_name_gin_trgm", opclass: :gin_trgm_ops, using: :gin
  end

  create_table "restaurants_owners", id: :serial, force: :cascade do |t|
    t.integer "owner_id"
    t.integer "restaurant_id"
    t.datetime "deleted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "daily", default: false
    t.boolean "weekly", default: false
    t.boolean "monthly", default: false
    t.boolean "periodic", default: false
    t.index ["owner_id"], name: "index_restaurants_owners_on_owner_id"
    t.index ["restaurant_id", "owner_id"], name: "index_restaurants_owners_on_restaurant_id_and_owner_id"
    t.index ["restaurant_id"], name: "index_restaurants_owners_on_restaurant_id"
  end

  create_table "revel_logs", id: :serial, force: :cascade do |t|
    t.integer "delayed_job_id"
    t.string "status"
    t.text "success_users"
    t.text "failed_users"
    t.integer "chain_id"
    t.string "log_type"
    t.datetime "initiate_at"
    t.datetime "completed_at"
    t.text "error_report"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.index ["chain_id"], name: "index_revel_logs_on_chain_id"
  end

  create_table "reward_code_users", id: :serial, force: :cascade do |t|
    t.string "staffcode"
    t.integer "user_id"
    t.integer "chain_id"
    t.integer "restaurant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active", default: true
    t.index ["chain_id", "staffcode"], name: "index_reward_code_users_on_chain_id_and_staffcode", unique: true
    t.index ["chain_id"], name: "index_reward_code_users_on_chain_id"
    t.index ["restaurant_id"], name: "index_reward_code_users_on_restaurant_id"
    t.index ["staffcode"], name: "index_reward_code_users_on_staffcode"
    t.index ["user_id", "staffcode"], name: "index_reward_code_users_on_user_id_and_staffcode"
    t.index ["user_id"], name: "index_reward_code_users_on_user_id"
  end

  create_table "reward_discounts", id: :serial, force: :cascade do |t|
    t.integer "reward_transaction_id"
    t.string "check_id"
    t.string "seq_num"
    t.decimal "discount"
    t.integer "restaurant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "menu_item_id"
    t.integer "level_size_id"
    t.index ["check_id", "seq_num", "restaurant_id"], name: "check_seq_num_location"
    t.index ["check_id"], name: "index_reward_discounts_on_check_id"
    t.index ["level_size_id"], name: "index_reward_discounts_on_level_size_id"
    t.index ["restaurant_id"], name: "index_reward_discounts_on_restaurant_id"
    t.index ["reward_transaction_id"], name: "index_reward_discounts_on_reward_transaction_id"
    t.index ["seq_num"], name: "index_reward_discounts_on_seq_num"
  end

  create_table "reward_exp_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.string "email_subject"
    t.text "email_body_html"
    t.text "email_body_text"
    t.text "notification"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reward_informations", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.string "title"
    t.text "description"
    t.string "app_text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reward_id"], name: "index_reward_informations_on_reward_id"
  end

  create_table "reward_menu_items", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "reward_id"
    t.integer "general_menu_item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "required", default: true
    t.index ["chain_id"], name: "index_reward_menu_items_on_chain_id"
    t.index ["general_menu_item_id"], name: "index_reward_menu_items_on_general_menu_item_id"
    t.index ["reward_id"], name: "index_reward_menu_items_on_reward_id"
  end

  create_table "reward_pos_codes", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.string "code"
    t.integer "user_id"
    t.datetime "used_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "restaurant_id"
  end

  create_table "reward_sent_histories", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.integer "user_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_reward_sent_histories_on_chain_id"
    t.index ["reward_id", "user_id"], name: "index_reward_sent_histories_on_reward_id_and_user_id"
    t.index ["reward_id"], name: "index_reward_sent_histories_on_reward_id"
    t.index ["user_id"], name: "index_reward_sent_histories_on_user_id"
  end

  create_table "reward_share_texts", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.text "share_text_twitter"
    t.text "share_text_facebook"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reward_summaries", id: :serial, force: :cascade do |t|
    t.date "transaction_date"
    t.integer "total"
    t.integer "restaurant_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id", "restaurant_id"], name: "index_reward_summaries_on_chain_id_and_restaurant_id"
    t.index ["chain_id"], name: "index_reward_summaries_on_chain_id"
    t.index ["restaurant_id"], name: "index_reward_summaries_on_restaurant_id"
  end

  create_table "reward_transaction_details", id: :serial, force: :cascade do |t|
    t.integer "reward_transaction_id"
    t.integer "reward_information_id"
    t.string "title"
    t.text "description"
    t.text "app_text"
    t.text "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["reward_transaction_id", "reward_information_id"], name: "by_transaction"
    t.index ["reward_transaction_id"], name: "index_reward_transaction_details_on_reward_transaction_id"
  end

  create_table "reward_transactions", id: :serial, force: :cascade do |t|
    t.decimal "longitude"
    t.decimal "latitude"
    t.float "points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "restaurant_reward_id"
    t.integer "user_id"
    t.datetime "deleted_at"
    t.string "staffcode"
    t.integer "restaurant_id"
    t.integer "reward_id"
    t.boolean "pos_used", default: false
    t.boolean "redeeming", default: false
    t.boolean "xpient_processed", default: false
    t.index ["pos_used"], name: "index_reward_transactions_on_pos_used"
    t.index ["redeeming"], name: "index_reward_transactions_on_redeeming"
    t.index ["restaurant_id"], name: "index_reward_transactions_on_restaurant_id"
    t.index ["restaurant_reward_id"], name: "index_reward_transactions_on_restaurant_reward_id"
    t.index ["reward_id"], name: "index_reward_transactions_on_reward_id"
    t.index ["staffcode", "reward_id", "pos_used"], name: "staffcode_reward_pos"
    t.index ["staffcode"], name: "index_reward_transactions_on_staffcode"
    t.index ["user_id"], name: "index_reward_transactions_on_user_id"
    t.index ["xpient_processed"], name: "index_reward_transactions_on_xpient_processed"
  end

  create_table "reward_wallets", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.integer "user_id"
    t.datetime "expiry_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 1
    t.datetime "deleted_at"
    t.boolean "gifter"
    t.string "description"
    t.float "rest_balance"
    t.index ["reward_id"], name: "index_reward_wallets_on_reward_id"
    t.index ["user_id"], name: "index_reward_wallets_on_user_id"
  end

  create_table "rewards", id: :serial, force: :cascade do |t|
    t.string "name"
    t.float "points"
    t.text "fineprint"
    t.datetime "effectiveDate"
    t.datetime "expiryDate"
    t.boolean "isActive"
    t.string "POSCode"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "survey_id"
    t.integer "reward_type"
    t.string "status", default: "inactive"
    t.datetime "executed_at"
    t.boolean "is_push_phone"
    t.boolean "is_push_email"
    t.datetime "deleted_at"
    t.float "hours_delay", default: 0.0
    t.integer "expired_day_user", default: 0
    t.integer "number_of_times_gifted", default: 0
    t.integer "priority_number", default: 0
    t.boolean "notify_sys_admin_for_pos_codes", default: false
    t.datetime "last_notification_sent_at"
    t.text "description"
    t.boolean "visible_dashboard", default: false
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.boolean "additional_information", default: false
    t.string "thumbnail_file_name"
    t.integer "thumbnail_file_size"
    t.string "thumbnail_content_type"
    t.datetime "thumbnail_updated_at"
    t.string "targetType"
    t.string "targetId"
    t.string "adj_type"
    t.float "adj_value", default: 0.0
    t.index ["chain_id"], name: "index_rewards_on_chain_id"
  end

  create_table "ritas_user_fav_items", id: :serial, force: :cascade do |t|
    t.string "email"
    t.integer "fid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_ritas_user_fav_items_on_email"
  end

  create_table "ritas_user_rewards", id: :serial, force: :cascade do |t|
    t.string "email"
    t.integer "current_redeemable_cards"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "pushed", default: false
    t.index ["email"], name: "index_ritas_user_rewards_on_email"
  end

  create_table "ritas_users", id: :serial, force: :cascade do |t|
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.datetime "member_sign_up_date"
    t.decimal "number_of_punches"
    t.boolean "migrated", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_ritas_users_on_email"
  end

  create_table "rotating_groups", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "group_type"
    t.boolean "active", default: true
    t.integer "priority"
    t.integer "max_renders"
    t.integer "survey_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rpush_apps", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.string "environment"
    t.text "certificate"
    t.string "password"
    t.integer "connections", default: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type", null: false
    t.string "auth_key"
    t.string "client_id"
    t.string "client_secret"
    t.string "access_token"
    t.datetime "access_token_expiration"
    t.index ["name"], name: "index_rpush_apps_on_name"
  end

  create_table "rpush_feedback", id: :serial, force: :cascade do |t|
    t.string "device_token", limit: 64, null: false
    t.datetime "failed_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "app"
    t.index ["device_token"], name: "index_rpush_feedback_on_device_token"
  end

  create_table "rpush_notifications", id: :serial, force: :cascade do |t|
    t.integer "badge"
    t.string "device_token", limit: 64
    t.string "sound", default: "default"
    t.string "alert"
    t.text "data"
    t.integer "expiry", default: 86400
    t.boolean "delivered", default: false, null: false
    t.datetime "delivered_at"
    t.boolean "failed", default: false, null: false
    t.datetime "failed_at"
    t.integer "error_code"
    t.text "error_description"
    t.datetime "deliver_after"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "alert_is_json", default: false
    t.string "type", null: false
    t.string "collapse_key"
    t.boolean "delay_while_idle", default: false, null: false
    t.text "registration_ids"
    t.integer "app_id"
    t.integer "retries", default: 0
    t.string "uri"
    t.datetime "fail_after"
    t.index ["app_id", "delivered", "failed", "deliver_after"], name: "index_rpush_notifications_multi"
    t.index ["delivered", "failed", "deliver_after"], name: "rpush_d_f_da_idx"
  end

  create_table "simple_captcha_data", id: :serial, force: :cascade do |t|
    t.string "key", limit: 40
    t.string "value", limit: 6
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["key"], name: "idx_key"
  end

  create_table "social_offers", id: :serial, force: :cascade do |t|
    t.string "title"
    t.integer "points"
    t.integer "kind"
    t.integer "chain_id"
  end

  create_table "social_share_participants", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "reward_id"
    t.integer "points"
    t.boolean "received"
    t.text "log"
    t.string "email"
    t.integer "medium_id"
    t.string "medium"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "social_share_id"
    t.index ["email"], name: "index_social_share_participants_on_email"
    t.index ["medium_id"], name: "index_social_share_participants_on_medium_id"
    t.index ["social_share_id"], name: "index_social_share_participants_on_social_share_id"
    t.index ["user_id"], name: "index_social_share_participants_on_user_id"
  end

  create_table "social_shares", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "chain_id"
    t.date "start_date"
    t.date "end_date"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "incentive_type"
    t.integer "incentive_id"
    t.text "fb_text"
    t.text "twitter_text"
    t.text "instagram_text"
    t.integer "daily_cap", default: 0
    t.integer "weekly_cap", default: 0
    t.integer "monthly_cap", default: 0
    t.boolean "is_default", default: false
    t.index ["chain_id"], name: "index_social_shares_on_chain_id"
  end

  create_table "steppingstone_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.string "subject"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "steppingstone_steps", id: :serial, force: :cascade do |t|
    t.integer "steppingstone_id"
    t.integer "property_one", default: 0
    t.float "property_two", default: 0.0
    t.integer "step_number", default: 0
    t.integer "reward_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "stepable_id"
    t.string "stepable_type"
    t.integer "point"
    t.integer "point_threshold"
    t.boolean "loop", default: true
    t.boolean "reset_milestone_value_after_qualifying", default: true
  end

  create_table "steppingstones", id: :serial, force: :cascade do |t|
    t.string "step_type"
    t.boolean "resetable", default: false
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "randomize", default: false
    t.integer "category_type"
    t.float "min_amount"
    t.float "max_amount"
    t.boolean "carry_over_points", default: false
  end

  create_table "stream_galleries", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.text "title"
    t.text "description"
    t.integer "animation_style"
    t.integer "transition_duration"
    t.integer "display_duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stream_gallery_images", id: :serial, force: :cascade do |t|
    t.text "hyperlink"
    t.text "title"
    t.text "description"
    t.text "feed_link"
    t.string "embed_file_name"
    t.integer "embed_file_size"
    t.string "embed_content_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "stream_gallery_id"
    t.integer "chain_id"
    t.integer "input_source"
    t.text "image_feed_lists"
    t.index ["stream_gallery_id"], name: "index_stream_gallery_images_on_stream_gallery_id"
  end

  create_table "submit_receipts", id: :serial, force: :cascade do |t|
    t.float "minimum_subtotal"
    t.text "fine_print"
    t.integer "survey_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  create_table "submit_surveys", id: :serial, force: :cascade do |t|
    t.integer "survey_id"
    t.text "fine_print"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "image_file_name"
    t.integer "image_file_size"
    t.string "image_content_type"
    t.datetime "image_updated_at"
  end

  create_table "subscription_formats", id: :serial, force: :cascade do |t|
    t.integer "subscription_id"
    t.string "date_format"
    t.string "file_name"
    t.string "email_subject"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["subscription_id"], name: "index_subscription_formats_on_subscription_id"
  end

  create_table "survey_response_alerts", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.boolean "send_email_alert", default: false
    t.boolean "send_alert_for_negative_review", default: false
    t.boolean "send_alert_for_comment", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "surveys", id: :serial, force: :cascade do |t|
    t.string "title"
    t.date "valid_from_date"
    t.date "valid_to_date"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "status", default: 0
    t.boolean "repeatable", default: true
    t.datetime "deleted_at"
    t.integer "max_rotating_questions", default: 0
    t.text "rotating_locations"
    t.integer "point"
    t.integer "reward_id"
    t.index ["chain_id"], name: "index_surveys_on_chain_id"
    t.index ["id", "deleted_at"], name: "index_surveys_on_id_and_deleted_at"
    t.index ["title"], name: "index_surveys_title_gin_trgm", opclass: :gin_trgm_ops, using: :gin
  end

  create_table "surveys_users", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "survey_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "receipt_id"
    t.integer "reward_id"
    t.integer "offer_id"
    t.float "scores"
    t.datetime "deleted_at"
    t.integer "restaurant_id"
    t.integer "guest_count", default: 0
    t.boolean "qualified_survey_incentive", default: false
    t.boolean "show_dashboard", default: true
    t.index ["receipt_id"], name: "index_surveys_users_on_receipt_id", unique: true
    t.index ["restaurant_id"], name: "index_surveys_users_on_restaurant_id"
    t.index ["survey_id"], name: "index_surveys_users_on_survey_id"
    t.index ["user_id"], name: "index_surveys_users_on_user_id"
  end

  create_table "taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id"
    t.string "taggable_type"
    t.integer "taggable_id"
    t.string "tagger_type"
    t.integer "tagger_id"
    t.string "context", limit: 128
    t.datetime "created_at"
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true
    t.index ["taggable_type", "taggable_id"], name: "index_taggings_on_taggable_type_and_taggable_id"
    t.index ["tagger_type", "tagger_id"], name: "index_taggings_on_tagger_type_and_tagger_id"
  end

  create_table "tags", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "category_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "min", default: 0
    t.integer "max", default: 0
    t.string "color"
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "task_definitions", id: :serial, force: :cascade do |t|
    t.integer "job_type"
    t.integer "reward_type"
    t.integer "points"
    t.integer "owner_id"
    t.integer "status"
    t.integer "total_users"
    t.integer "chain_id"
    t.integer "reward_id"
    t.integer "activity_days"
    t.integer "percentage"
    t.integer "schedule_type"
    t.datetime "executed_at"
    t.string "subject_email"
    t.string "subject_phone"
    t.text "content_email"
    t.text "content_phone"
    t.boolean "push_email"
    t.boolean "push_phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "notification", default: true
    t.string "zone"
    t.index ["chain_id", "reward_id"], name: "index_task_definitions_on_chain_id_and_reward_id"
    t.index ["chain_id"], name: "index_task_definitions_on_chain_id"
    t.index ["reward_id"], name: "index_task_definitions_on_reward_id"
  end

  create_table "task_schedulers", id: :serial, force: :cascade do |t|
    t.integer "job_id"
    t.string "run_type"
    t.datetime "start_at"
    t.datetime "executed_at"
    t.datetime "last_executed"
    t.datetime "next_run"
    t.datetime "end_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["job_id"], name: "index_task_schedulers_on_job_id"
  end

# Could not dump table "tech_studio_users" because of following StandardError
#   Unknown type 'optin' for column 'optin'

  create_table "terms", id: :serial, force: :cascade do |t|
    t.text "content"
    t.integer "locale_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["locale_id"], name: "index_terms_on_locale_id"
  end

  create_table "text_tile_contents", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "locale_id"
    t.text "content"
    t.string "content_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tmp_categories", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "category_ids"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transactions", id: :serial, force: :cascade do |t|
    t.integer "reward_id"
    t.integer "offer_id"
    t.integer "user_id"
    t.integer "chain_id"
    t.integer "balanceBefore"
    t.integer "balanceAfter"
    t.integer "balanceDiff"
    t.boolean "isUndo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["chain_id"], name: "index_transactions_on_chain_id"
    t.index ["offer_id"], name: "index_transactions_on_offer_id"
    t.index ["reward_id"], name: "index_transactions_on_reward_id"
    t.index ["user_id"], name: "index_transactions_on_user_id"
  end

  create_table "user_activity_notification_fields", id: :serial, force: :cascade do |t|
    t.integer "user_activity_notification_id"
    t.string "name"
    t.string "description"
    t.string "identifier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_activity_notification_id", "identifier"], name: "notif_by_data_key_index"
  end

  create_table "user_activity_notifications", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "description"
    t.text "notification"
    t.string "unique_identifier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "delay_time", default: 0
    t.index ["chain_id"], name: "index_user_activity_notifications_on_chain_id"
    t.index ["unique_identifier"], name: "index_user_activity_notifications_on_unique_identifier"
  end

  create_table "user_bucket_steps", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "bucket_path_id"
    t.integer "current_step", default: 0
    t.integer "current_count", default: 0
    t.integer "total_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_checkins", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "deal_id"
    t.integer "restaurant_id"
    t.datetime "deleted_at"
    t.float "user_latitude"
    t.float "user_longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_code_users", id: :serial, force: :cascade do |t|
    t.string "user_code"
    t.integer "user_id"
    t.integer "chain_id"
    t.datetime "expired_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id", "user_code"], name: "index_user_code_users_on_chain_id_and_user_code", unique: true
    t.index ["chain_id"], name: "index_user_code_users_on_chain_id"
    t.index ["user_code"], name: "index_user_code_users_on_user_code"
    t.index ["user_id", "user_code"], name: "index_user_code_users_on_user_id_and_user_code"
    t.index ["user_id"], name: "index_user_code_users_on_user_id"
  end

  create_table "user_confirmations", id: :serial, force: :cascade do |t|
    t.boolean "email_confirmed", default: false
    t.string "confirm_token"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_user_confirmations_on_user_id"
  end

  create_table "user_deal_surveys", id: :serial, force: :cascade do |t|
    t.integer "deal_id"
    t.integer "survey_id"
    t.integer "survey_user_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "latitude"
    t.float "longitude"
    t.integer "restaurant_id"
    t.index ["deal_id"], name: "index_user_deal_surveys_on_deal_id"
    t.index ["restaurant_id"], name: "index_user_deal_surveys_on_restaurant_id"
    t.index ["survey_id"], name: "index_user_deal_surveys_on_survey_id"
    t.index ["user_id"], name: "index_user_deal_surveys_on_user_id"
  end

  create_table "user_device_logs", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "keychain_value"
    t.string "device_type"
    t.string "access_type"
    t.datetime "access_date_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "device_id"
    t.integer "chain_id"
    t.index ["access_type"], name: "index_user_device_logs_on_access_type"
    t.index ["chain_id", "keychain_value", "access_type"], name: "user_device_log_cukeyacctype_index"
    t.index ["chain_id", "user_id"], name: "index_user_device_logs_on_chain_id_and_user_id"
    t.index ["chain_id"], name: "index_user_device_logs_on_chain_id"
    t.index ["user_id"], name: "index_user_device_logs_on_user_id"
  end

  create_table "user_devices", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "keychain_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["keychain_value"], name: "index_user_devices_on_keychain_value"
    t.index ["user_id"], name: "index_user_devices_on_user_id"
  end

  create_table "user_fishbowls", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "user_id"
    t.integer "restaurant_id"
    t.integer "status"
    t.string "member_id"
    t.datetime "signup_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
  end

  create_table "user_goal_categories", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "category_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_user_goal_categories_on_category_id"
    t.index ["user_id", "category_id"], name: "index_user_goal_categories_on_user_id_and_category_id"
    t.index ["user_id"], name: "index_user_goal_categories_on_user_id"
  end

  create_table "user_goals", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "goal_id"
    t.integer "week_id"
    t.boolean "completed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["goal_id"], name: "index_user_goals_on_goal_id"
    t.index ["user_id", "goal_id", "week_id"], name: "index_user_goals_on_user_id_and_goal_id_and_week_id"
    t.index ["user_id", "goal_id"], name: "index_user_goals_on_user_id_and_goal_id"
    t.index ["user_id"], name: "index_user_goals_on_user_id"
    t.index ["week_id"], name: "index_user_goals_on_week_id"
  end

  create_table "user_incentives", id: :serial, force: :cascade do |t|
    t.string "incentive_type"
    t.integer "incentive_id"
    t.integer "deal_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_activity_id"
    t.string "user_activity_type"
    t.index ["deal_id"], name: "index_user_incentives_on_deal_id"
    t.index ["incentive_type", "incentive_id"], name: "index_user_incentives_on_incentive_type_and_incentive_id"
    t.index ["user_id"], name: "index_user_incentives_on_user_id"
  end

  create_table "user_milestone_points", id: :serial, force: :cascade do |t|
    t.integer "current_redeem_at"
    t.integer "last_redeem_at"
    t.integer "user_id"
    t.integer "milestone_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["milestone_id"], name: "index_user_milestone_points_on_milestone_id"
    t.index ["user_id"], name: "index_user_milestone_points_on_user_id"
  end

  create_table "user_payment_histories", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "pos_check_upload_id"
    t.boolean "success", default: false
    t.integer "error_code"
    t.text "error_description"
    t.integer "payment_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "amount", default: 0.0
    t.float "tip", default: 0.0
    t.string "ncr_receipt_number"
    t.string "qrcode"
    t.string "signature_file_name"
    t.string "signature_content_type"
    t.integer "signature_file_size"
    t.datetime "signature_updated_at"
    t.integer "receipt_id"
    t.string "authorization_code"
    t.index ["authorization_code"], name: "index_user_payment_histories_on_authorization_code"
    t.index ["ncr_receipt_number"], name: "index_ncr_receipt_number_user_payment_histories"
    t.index ["pos_check_upload_id"], name: "idx_pcu_id_uph"
    t.index ["qrcode"], name: "index_qrcode_user_payment_histories"
    t.index ["success"], name: "idx_success_uph"
    t.index ["user_id", "pos_check_upload_id"], name: "idx_user_id_pcu_id_uph"
    t.index ["user_id"], name: "idx_user_id_uph"
  end

  create_table "user_profiles", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "avatar_file_name"
    t.integer "avatar_file_size"
    t.string "avatar_content_type"
    t.datetime "avatar_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "guest_dna_user_acc_key"
    t.string "patron_id"
    t.string "external_relevant_user_id"
    t.string "tech_studio_guest_identifier"
    t.boolean "mall_employee"
    t.string "retailer"
    t.index ["user_id"], name: "index_user_id_user_profiles"
  end

  create_table "user_push_points", id: :serial, force: :cascade do |t|
    t.integer "push_point_id"
    t.integer "user_id"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["push_point_id", "user_id"], name: "index_user_push_points_on_push_point_id_and_user_id"
    t.index ["push_point_id"], name: "index_user_push_points_on_push_point_id"
    t.index ["user_id"], name: "index_user_push_points_on_user_id"
  end

  create_table "user_remove_points", id: :serial, force: :cascade do |t|
    t.integer "remove_point_id"
    t.integer "user_id"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["remove_point_id", "user_id"], name: "index_user_remove_points_on_remove_point_id_and_user_id"
    t.index ["remove_point_id"], name: "index_user_remove_points_on_remove_point_id"
    t.index ["user_id"], name: "index_user_remove_points_on_user_id"
  end

  create_table "user_repeated_promotions", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "promotion_id"
    t.date "applied_at"
    t.boolean "expired", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "last_pushed_at"
  end

  create_table "user_rotating_questions", id: :serial, force: :cascade do |t|
    t.integer "render_count"
    t.integer "answer_id"
    t.boolean "completed"
    t.string "survey_user_ids"
    t.integer "user_id"
    t.integer "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id", "user_id"], name: "index_user_rotating_questions_on_question_id_and_user_id"
  end

  create_table "user_sessions", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "chain_id"
    t.integer "application_id"
    t.string "code"
    t.string "code_with_prefix"
    t.float "lat"
    t.float "long"
    t.string "city"
    t.string "postal_code"
    t.string "country_code"
    t.text "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "used", default: false
    t.integer "location_id"
    t.integer "code_type"
    t.datetime "expired_at"
    t.integer "receipt_id"
    t.boolean "executed", default: false
    t.boolean "active"
    t.boolean "mp_processed", default: false
    t.integer "code_sequence"
    t.index ["active"], name: "index_user_sessions_on_active"
    t.index ["chain_id", "code", "code_type"], name: "index_user_sessions_on_chain_id_and_code_and_code_type"
    t.index ["code"], name: "index_user_sessions_on_code"
    t.index ["code_type"], name: "index_user_sessions_on_code_type"
    t.index ["user_id", "code"], name: "index_user_sessions_on_user_id_and_code"
    t.index ["user_id", "used"], name: "index_user_sessions_on_user_id_and_used"
    t.index ["user_id"], name: "index_user_sessions_on_user_id"
  end

  create_table "user_steps", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "steppingstone_id"
    t.integer "current_step", default: 0
    t.integer "current_count", default: 0
    t.integer "total_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id", "steppingstone_id"], name: "index_user_steps_on_user_id_and_steppingstone_id"
  end

  create_table "user_tags", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "tag_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tag_id"], name: "index_user_tags_on_tag_id"
    t.index ["user_id"], name: "index_user_tags_on_user_id"
  end

  create_table "user_total_receipts", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "chain_id"
    t.integer "receipt_count"
    t.float "subtotal"
    t.float "average"
    t.date "last_milestone"
    t.integer "reward_pushed_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "last_receipt_id"
    t.index ["user_id", "chain_id"], name: "index_user_total_receipts_on_user_id_and_chain_id"
    t.index ["user_id"], name: "index_user_total_receipts_on_user_id"
  end

# Could not dump table "users" because of following StandardError
#   Unknown type 'new_gender' for column 'gender'

  create_table "users_promo_codes", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "promo_code_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["promo_code_id"], name: "index_users_promo_codes_on_promo_code_id"
    t.index ["user_id"], name: "index_users_promo_codes_on_user_id"
  end

  create_table "userstep_categories", id: :serial, force: :cascade do |t|
    t.integer "steppingstone_step_id"
    t.integer "user_type"
    t.integer "reward_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["steppingstone_step_id", "user_type"], name: "index_user_and_id"
    t.index ["steppingstone_step_id"], name: "index_userstep_categories_on_steppingstone_step_id"
  end

  create_table "vantiv_credentials", id: :serial, force: :cascade do |t|
    t.string "account_id"
    t.string "account_token"
    t.string "application_id"
    t.string "acceptor_id"
    t.integer "chain_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_vantiv_credentials_on_chain_id"
  end

  create_table "vantiv_gift_card_email_sent_logs", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.string "giftcard_notification_email"
    t.string "card_number"
    t.float "giftcard_amount"
    t.string "giftee_email"
    t.string "giftee_name"
    t.text "custom_message"
    t.string "gifter_name"
    t.text "image_header_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["chain_id"], name: "index_vantiv_gift_card_email_sent_logs_on_chain_id"
    t.index ["giftee_email"], name: "index_vantiv_gift_card_email_sent_logs_on_giftee_email"
  end

  create_table "vantiv_gift_cards", id: :serial, force: :cascade do |t|
    t.string "card_number"
    t.string "expiration_month"
    t.string "expiration_year"
    t.string "cvv"
    t.string "cvv2"
    t.string "security_code"
    t.integer "chain_id"
    t.boolean "used", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_number"], name: "index_vantiv_gift_cards_on_card_number"
    t.index ["chain_id"], name: "index_vantiv_gift_cards_on_chain_id"
  end

  create_table "vantiv_transaction_logs", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "user_id"
    t.integer "transaction_type"
    t.string "card_number"
    t.string "ref_num"
    t.string "transaction_id"
    t.float "amount"
    t.text "relevant_response"
    t.text "vantiv_response"
    t.text "mp_response"
    t.boolean "success", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "barcode_file_name"
    t.string "barcode_content_type"
    t.integer "barcode_file_size"
    t.datetime "barcode_updated_at"
    t.index ["card_number"], name: "index_vantiv_transaction_logs_on_card_number"
    t.index ["chain_id"], name: "index_vantiv_transaction_logs_on_chain_id"
    t.index ["created_at"], name: "index_vantiv_transaction_logs_on_created_at"
    t.index ["ref_num"], name: "index_vantiv_transaction_logs_on_ref_num"
    t.index ["transaction_id"], name: "index_vantiv_transaction_logs_on_transaction_id"
    t.index ["user_id"], name: "index_vantiv_transaction_logs_on_user_id"
  end

  create_table "vext_online_order_details", id: :serial, force: :cascade do |t|
    t.integer "vext_online_order_id"
    t.text "order_detail"
    t.decimal "sub_total"
    t.decimal "tax"
    t.decimal "total"
    t.decimal "points", default: "0.0"
    t.decimal "discount"
    t.index ["vext_online_order_id"], name: "index_vext_online_order_details_on_vext_online_order_id"
  end

  create_table "vext_online_orders", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "user_id"
    t.integer "loyalty_status"
    t.integer "platform"
    t.integer "restaurant_id"
    t.string "email"
    t.string "online_order_identifier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["chain_id"], name: "index_vext_online_orders_on_chain_id"
    t.index ["online_order_identifier"], name: "index_vext_online_orders_on_online_order_identifier"
    t.index ["restaurant_id"], name: "index_vext_online_orders_on_restaurant_id"
    t.index ["user_id"], name: "index_vext_online_orders_on_user_id"
  end

  create_table "vext_receipt_online_orders", id: :serial, force: :cascade do |t|
    t.integer "receipt_id"
    t.integer "vext_online_order_id"
    t.index ["receipt_id"], name: "index_vext_receipt_online_orders_on_receipt_id"
    t.index ["vext_online_order_id"], name: "index_vext_receipt_online_orders_on_vext_online_order_id"
  end

  create_table "vext_reward_claims", id: :serial, force: :cascade do |t|
    t.integer "chain_id"
    t.integer "user_id"
    t.integer "reward_id"
    t.string "reward_code"
    t.integer "location_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "weeks", id: :serial, force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
